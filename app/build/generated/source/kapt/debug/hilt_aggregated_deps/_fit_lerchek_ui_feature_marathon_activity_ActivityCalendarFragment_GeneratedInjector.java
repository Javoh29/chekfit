package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "fit.lerchek.ui.feature.marathon.activity.ActivityCalendarFragment_GeneratedInjector"
)
public class _fit_lerchek_ui_feature_marathon_activity_ActivityCalendarFragment_GeneratedInjector {
}
