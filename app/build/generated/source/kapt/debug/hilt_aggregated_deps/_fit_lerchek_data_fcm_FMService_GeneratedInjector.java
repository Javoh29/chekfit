package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ServiceComponent",
    entryPoints = "fit.lerchek.data.fcm.FMService_GeneratedInjector"
)
public class _fit_lerchek_data_fcm_FMService_GeneratedInjector {
}
