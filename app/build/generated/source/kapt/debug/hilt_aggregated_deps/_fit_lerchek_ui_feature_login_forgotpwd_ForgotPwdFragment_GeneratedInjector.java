package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "fit.lerchek.ui.feature.login.forgotpwd.ForgotPwdFragment_GeneratedInjector"
)
public class _fit_lerchek_ui_feature_login_forgotpwd_ForgotPwdFragment_GeneratedInjector {
}
