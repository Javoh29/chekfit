package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityRetainedComponent",
    modules = "fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel_HiltModules.KeyModule"
)
public class _fit_lerchek_ui_feature_marathon_progress_MakeMeasurementsViewModel_HiltModules_KeyModule {
}
