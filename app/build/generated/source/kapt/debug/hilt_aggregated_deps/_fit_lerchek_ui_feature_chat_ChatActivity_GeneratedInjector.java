package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityComponent",
    entryPoints = "fit.lerchek.ui.feature.chat.ChatActivity_GeneratedInjector"
)
public class _fit_lerchek_ui_feature_chat_ChatActivity_GeneratedInjector {
}
