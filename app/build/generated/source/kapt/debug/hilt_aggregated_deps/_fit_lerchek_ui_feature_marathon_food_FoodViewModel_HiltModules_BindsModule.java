package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "fit.lerchek.ui.feature.marathon.food.FoodViewModel_HiltModules.BindsModule"
)
public class _fit_lerchek_ui_feature_marathon_food_FoodViewModel_HiltModules_BindsModule {
}
