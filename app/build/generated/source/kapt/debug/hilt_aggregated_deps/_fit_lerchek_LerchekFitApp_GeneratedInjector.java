package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    entryPoints = "fit.lerchek.LerchekFitApp_GeneratedInjector"
)
public class _fit_lerchek_LerchekFitApp_GeneratedInjector {
}
