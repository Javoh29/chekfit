package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel_HiltModules.BindsModule"
)
public class _fit_lerchek_ui_feature_marathon_profile_personal_PersonalInfoViewModel_HiltModules_BindsModule {
}
