package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "fit.lerchek.ui.feature.marathon.voting.VotingViewModel_HiltModules.BindsModule"
)
public class _fit_lerchek_ui_feature_marathon_voting_VotingViewModel_HiltModules_BindsModule {
}
