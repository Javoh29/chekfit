package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemFoodFilterBindingImpl extends ItemFoodFilterBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(22);
        sIncludes.setIncludes(9, 
            new String[] {"item_food_day", "item_food_day", "item_food_day", "item_food_day", "item_food_day", "item_food_day", "item_food_day"},
            new int[] {11, 12, 13, 14, 15, 16, 17},
            new int[] {fit.lerchek.R.layout.item_food_day,
                fit.lerchek.R.layout.item_food_day,
                fit.lerchek.R.layout.item_food_day,
                fit.lerchek.R.layout.item_food_day,
                fit.lerchek.R.layout.item_food_day,
                fit.lerchek.R.layout.item_food_day,
                fit.lerchek.R.layout.item_food_day});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.iconSelect, 18);
        sViewsWithIds.put(R.id.imgArrow, 19);
        sViewsWithIds.put(R.id.imgNutritionPlan, 20);
        sViewsWithIds.put(R.id.llText, 21);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView10;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView5;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView7;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView8;
    @NonNull
    private final android.widget.LinearLayout mboundView9;
    @Nullable
    private final fit.lerchek.databinding.ItemFoodDayBinding mboundView91;
    @Nullable
    private final fit.lerchek.databinding.ItemFoodDayBinding mboundView92;
    @Nullable
    private final fit.lerchek.databinding.ItemFoodDayBinding mboundView93;
    @Nullable
    private final fit.lerchek.databinding.ItemFoodDayBinding mboundView94;
    @Nullable
    private final fit.lerchek.databinding.ItemFoodDayBinding mboundView95;
    @Nullable
    private final fit.lerchek.databinding.ItemFoodDayBinding mboundView96;
    @Nullable
    private final fit.lerchek.databinding.ItemFoodDayBinding mboundView97;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback33;
    @Nullable
    private final android.view.View.OnClickListener mCallback30;
    @Nullable
    private final android.view.View.OnClickListener mCallback34;
    @Nullable
    private final android.view.View.OnClickListener mCallback32;
    @Nullable
    private final android.view.View.OnClickListener mCallback31;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemFoodFilterBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private ItemFoodFilterBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[3]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (androidx.cardview.widget.CardView) bindings[6]
            , (android.widget.LinearLayout) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[18]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[19]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[20]
            , (android.widget.LinearLayout) bindings[21]
            );
        this.btnFAQ.setTag(null);
        this.btnFavorite.setTag(null);
        this.btnSelectPlan.setTag(null);
        this.btnSelectWeek.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView10 = (androidx.appcompat.widget.AppCompatTextView) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatImageView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView7 = (androidx.appcompat.widget.AppCompatTextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (androidx.appcompat.widget.AppCompatTextView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (android.widget.LinearLayout) bindings[9];
        this.mboundView9.setTag(null);
        this.mboundView91 = (fit.lerchek.databinding.ItemFoodDayBinding) bindings[11];
        setContainedBinding(this.mboundView91);
        this.mboundView92 = (fit.lerchek.databinding.ItemFoodDayBinding) bindings[12];
        setContainedBinding(this.mboundView92);
        this.mboundView93 = (fit.lerchek.databinding.ItemFoodDayBinding) bindings[13];
        setContainedBinding(this.mboundView93);
        this.mboundView94 = (fit.lerchek.databinding.ItemFoodDayBinding) bindings[14];
        setContainedBinding(this.mboundView94);
        this.mboundView95 = (fit.lerchek.databinding.ItemFoodDayBinding) bindings[15];
        setContainedBinding(this.mboundView95);
        this.mboundView96 = (fit.lerchek.databinding.ItemFoodDayBinding) bindings[16];
        setContainedBinding(this.mboundView96);
        this.mboundView97 = (fit.lerchek.databinding.ItemFoodDayBinding) bindings[17];
        setContainedBinding(this.mboundView97);
        setRootTag(root);
        // listeners
        mCallback33 = new fit.lerchek.generated.callback.OnClickListener(this, 4);
        mCallback30 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        mCallback34 = new fit.lerchek.generated.callback.OnClickListener(this, 5);
        mCallback32 = new fit.lerchek.generated.callback.OnClickListener(this, 3);
        mCallback31 = new fit.lerchek.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        mboundView91.invalidateAll();
        mboundView92.invalidateAll();
        mboundView93.invalidateAll();
        mboundView94.invalidateAll();
        mboundView95.invalidateAll();
        mboundView96.invalidateAll();
        mboundView97.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (mboundView91.hasPendingBindings()) {
            return true;
        }
        if (mboundView92.hasPendingBindings()) {
            return true;
        }
        if (mboundView93.hasPendingBindings()) {
            return true;
        }
        if (mboundView94.hasPendingBindings()) {
            return true;
        }
        if (mboundView95.hasPendingBindings()) {
            return true;
        }
        if (mboundView96.hasPendingBindings()) {
            return true;
        }
        if (mboundView97.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.FoodFilterUIModel) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.food.FoodCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.FoodFilterUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.food.FoodCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        mboundView91.setLifecycleOwner(lifecycleOwner);
        mboundView92.setLifecycleOwner(lifecycleOwner);
        mboundView93.setLifecycleOwner(lifecycleOwner);
        mboundView94.setLifecycleOwner(lifecycleOwner);
        mboundView95.setLifecycleOwner(lifecycleOwner);
        mboundView96.setLifecycleOwner(lifecycleOwner);
        mboundView97.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.FoodFilterUIModel model = mModel;
        java.lang.String modelNutritionPlanName = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt3 = null;
        java.util.List<fit.lerchek.data.domain.uimodel.marathon.DayUIModel> modelDays = null;
        java.lang.String modelSelectedWeekName = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt2 = null;
        java.lang.String modelTodayName = null;
        fit.lerchek.data.domain.uimodel.marathon.WeekUIModel modelSelectedWeek = null;
        java.lang.String modelNutritionPlanDescription = null;
        fit.lerchek.ui.feature.marathon.food.FoodCallback callback = mCallback;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt6 = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt1 = null;
        fit.lerchek.data.domain.uimodel.marathon.PlanUIModel modelNutritionPlan = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt5 = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt0 = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt4 = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (model != null) {
                    // read model.days
                    modelDays = model.getDays();
                    // read model.todayName
                    modelTodayName = model.getTodayName();
                    // read model.selectedWeek
                    modelSelectedWeek = model.getSelectedWeek();
                    // read model.nutritionPlan
                    modelNutritionPlan = model.getNutritionPlan();
                }


                if (modelDays != null) {
                    // read model.days.get(3)
                    modelDaysGetInt3 = modelDays.get(3);
                    // read model.days.get(2)
                    modelDaysGetInt2 = modelDays.get(2);
                    // read model.days.get(6)
                    modelDaysGetInt6 = modelDays.get(6);
                    // read model.days.get(1)
                    modelDaysGetInt1 = modelDays.get(1);
                    // read model.days.get(5)
                    modelDaysGetInt5 = modelDays.get(5);
                    // read model.days.get(0)
                    modelDaysGetInt0 = modelDays.get(0);
                    // read model.days.get(4)
                    modelDaysGetInt4 = modelDays.get(4);
                }
                if (modelSelectedWeek != null) {
                    // read model.selectedWeek.name
                    modelSelectedWeekName = modelSelectedWeek.getName();
                }
                if (modelNutritionPlan != null) {
                    // read model.nutritionPlan.name
                    modelNutritionPlanName = modelNutritionPlan.getName();
                    // read model.nutritionPlan.description
                    modelNutritionPlanDescription = modelNutritionPlan.getDescription();
                }
        }
        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.btnFAQ.setOnClickListener(mCallback31);
            this.btnFavorite.setOnClickListener(mCallback32);
            this.btnSelectPlan.setOnClickListener(mCallback34);
            this.btnSelectWeek.setOnClickListener(mCallback30);
            this.mboundView5.setOnClickListener(mCallback33);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView10, modelTodayName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, modelSelectedWeekName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, modelNutritionPlanName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, modelNutritionPlanDescription);
            this.mboundView91.setModel(modelDaysGetInt0);
            this.mboundView92.setModel(modelDaysGetInt1);
            this.mboundView93.setModel(modelDaysGetInt2);
            this.mboundView94.setModel(modelDaysGetInt3);
            this.mboundView95.setModel(modelDaysGetInt4);
            this.mboundView96.setModel(modelDaysGetInt5);
            this.mboundView97.setModel(modelDaysGetInt6);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            this.mboundView91.setCallback(callback);
            this.mboundView92.setCallback(callback);
            this.mboundView93.setCallback(callback);
            this.mboundView94.setCallback(callback);
            this.mboundView95.setCallback(callback);
            this.mboundView96.setCallback(callback);
            this.mboundView97.setCallback(callback);
        }
        executeBindingsOn(mboundView91);
        executeBindingsOn(mboundView92);
        executeBindingsOn(mboundView93);
        executeBindingsOn(mboundView94);
        executeBindingsOn(mboundView95);
        executeBindingsOn(mboundView96);
        executeBindingsOn(mboundView97);
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 4: {
                // localize variables for thread safety
                // callback
                fit.lerchek.ui.feature.marathon.food.FoodCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.onShoppingCartClick();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // model
                fit.lerchek.data.domain.uimodel.marathon.FoodFilterUIModel model = mModel;
                // model != null
                boolean modelJavaLangObjectNull = false;
                // model.weeks
                java.util.List<fit.lerchek.data.domain.uimodel.marathon.WeekUIModel> modelWeeks = null;
                // callback
                fit.lerchek.ui.feature.marathon.food.FoodCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {



                    modelJavaLangObjectNull = (model) != (null);
                    if (modelJavaLangObjectNull) {


                        modelWeeks = model.getWeeks();

                        callback.onSelectWeekClick(modelWeeks);
                    }
                }
                break;
            }
            case 5: {
                // localize variables for thread safety
                // model
                fit.lerchek.data.domain.uimodel.marathon.FoodFilterUIModel model = mModel;
                // model.plans
                java.util.List<fit.lerchek.data.domain.uimodel.marathon.PlanUIModel> modelPlans = null;
                // model != null
                boolean modelJavaLangObjectNull = false;
                // callback
                fit.lerchek.ui.feature.marathon.food.FoodCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {



                    modelJavaLangObjectNull = (model) != (null);
                    if (modelJavaLangObjectNull) {


                        modelPlans = model.getPlans();

                        callback.onSelectNutritionPlanClick(modelPlans);
                    }
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // callback
                fit.lerchek.ui.feature.marathon.food.FoodCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.onFavoriteClick();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // callback
                fit.lerchek.ui.feature.marathon.food.FoodCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.onFAQClick();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}