package fit.lerchek.ui.feature.marathon.profile.personal;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = TestFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface TestFragment_GeneratedInjector {
  void injectTestFragment(TestFragment testFragment);
}
