package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentActivityCalendarBindingImpl extends FragmentActivityCalendarBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 12);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView1;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView10;
    @NonNull
    private final android.widget.FrameLayout mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView7;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView8;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView9;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentActivityCalendarBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private FragmentActivityCalendarBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 10
            , (bindings[12] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[12]) : null
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.core.widget.NestedScrollView) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[5]
            );
        this.icProfile.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (bindings[11] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[11]) : null;
        this.mboundView10 = (androidx.recyclerview.widget.RecyclerView) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView3 = (android.widget.FrameLayout) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView7 = (androidx.appcompat.widget.AppCompatTextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (androidx.recyclerview.widget.RecyclerView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (androidx.appcompat.widget.AppCompatTextView) bindings[9];
        this.mboundView9.setTag(null);
        this.progressBlock.setTag(null);
        this.svCalendar.setTag(null);
        this.tvCurrentDay.setTag(null);
        this.tvHello.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2000L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.calendarCallback == variableId) {
            setCalendarCallback((fit.lerchek.ui.feature.marathon.activity.ActivityCalendarCallback) variable);
        }
        else if (BR.taskCallback == variableId) {
            setTaskCallback((fit.lerchek.ui.feature.marathon.today.callbacks.TaskCallback) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.activity.ActivityCalendarViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCalendarCallback(@Nullable fit.lerchek.ui.feature.marathon.activity.ActivityCalendarCallback CalendarCallback) {
        this.mCalendarCallback = CalendarCallback;
        synchronized(this) {
            mDirtyFlags |= 0x400L;
        }
        notifyPropertyChanged(BR.calendarCallback);
        super.requestRebind();
    }
    public void setTaskCallback(@Nullable fit.lerchek.ui.feature.marathon.today.callbacks.TaskCallback TaskCallback) {
        this.mTaskCallback = TaskCallback;
        synchronized(this) {
            mDirtyFlags |= 0x800L;
        }
        notifyPropertyChanged(BR.taskCallback);
        super.requestRebind();
    }
    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.activity.ActivityCalendarViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1000L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelProfileName((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelCurrentDate((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelCalendarItems((androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarItem>) object, fieldId);
            case 3 :
                return onChangeViewModelCurrentDay((androidx.databinding.ObservableInt) object, fieldId);
            case 4 :
                return onChangeViewModelCurrentTasks((androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.TaskUIModel>) object, fieldId);
            case 5 :
                return onChangeViewModelIsToday((androidx.databinding.ObservableBoolean) object, fieldId);
            case 6 :
                return onChangeViewModelIsMale((androidx.databinding.ObservableBoolean) object, fieldId);
            case 7 :
                return onChangeViewModelCurrentMarathonDay((androidx.databinding.ObservableInt) object, fieldId);
            case 8 :
                return onChangeViewModelCurrentTasksDate((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 9 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelProfileName(androidx.databinding.ObservableField<java.lang.String> ViewModelProfileName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentDate(androidx.databinding.ObservableField<java.lang.String> ViewModelCurrentDate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCalendarItems(androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarItem> ViewModelCalendarItems, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentDay(androidx.databinding.ObservableInt ViewModelCurrentDay, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentTasks(androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.TaskUIModel> ViewModelCurrentTasks, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsToday(androidx.databinding.ObservableBoolean ViewModelIsToday, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsMale(androidx.databinding.ObservableBoolean ViewModelIsMale, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentMarathonDay(androidx.databinding.ObservableInt ViewModelCurrentMarathonDay, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCurrentTasksDate(androidx.databinding.ObservableField<java.lang.String> ViewModelCurrentTasksDate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelIsTodayGet = false;
        java.lang.String viewModelIsTodayMboundView9AndroidStringMarathonTasksMboundView9AndroidStringMarathonTasksFormatViewModelCurrentTasksDate = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelProfileName = null;
        java.lang.String viewModelCurrentTasksDateGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelCurrentDate = null;
        android.graphics.drawable.Drawable viewModelIsMaleIcProfileAndroidDrawableIcProfileMaleIcProfileAndroidDrawableIcProfileFemale = null;
        java.lang.String viewModelCurrentDateGet = null;
        androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarItem> viewModelCalendarItems = null;
        androidx.databinding.ObservableInt viewModelCurrentDay = null;
        androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.TaskUIModel> viewModelCurrentTasks = null;
        int viewModelCurrentMarathonDayGet = 0;
        int viewModelIsProgressViewGONEViewVISIBLE = 0;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        java.lang.String tvHelloAndroidStringMarathonTasksHelloViewModelProfileName = null;
        java.lang.String mboundView9AndroidStringMarathonTasksFormatViewModelCurrentTasksDate = null;
        androidx.databinding.ObservableBoolean viewModelIsToday = null;
        java.lang.String viewModelProfileNameGet = null;
        fit.lerchek.ui.feature.marathon.activity.ActivityCalendarCallback calendarCallback = mCalendarCallback;
        androidx.databinding.ObservableBoolean viewModelIsMale = null;
        fit.lerchek.ui.feature.marathon.today.callbacks.TaskCallback taskCallback = mTaskCallback;
        androidx.databinding.ObservableInt viewModelCurrentMarathonDay = null;
        java.lang.String tvCurrentDayAndroidStringMarathonTasksTodayViewModelCurrentMarathonDay = null;
        boolean viewModelCurrentMarathonDayInt1 = false;
        java.lang.String viewModelCurrentMarathonDayInt1TvCurrentDayAndroidStringMarathonTasksPrepareWeekTvCurrentDayAndroidStringMarathonTasksTodayViewModelCurrentMarathonDay = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelCurrentTasksDate = null;
        int viewModelCurrentDayGet = 0;
        boolean viewModelIsMaleGet = false;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.activity.ActivityCalendarViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x3404L) != 0) {
        }
        if ((dirtyFlags & 0x3818L) != 0) {
        }
        if ((dirtyFlags & 0x3fffL) != 0) {


            if ((dirtyFlags & 0x3001L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.profileName
                        viewModelProfileName = viewModel.getProfileName();
                    }
                    updateRegistration(0, viewModelProfileName);


                    if (viewModelProfileName != null) {
                        // read viewModel.profileName.get()
                        viewModelProfileNameGet = viewModelProfileName.get();
                    }


                    // read @android:string/marathon_tasks_hello
                    tvHelloAndroidStringMarathonTasksHelloViewModelProfileName = tvHello.getResources().getString(R.string.marathon_tasks_hello, viewModelProfileNameGet);
            }
            if ((dirtyFlags & 0x3002L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentDate
                        viewModelCurrentDate = viewModel.getCurrentDate();
                    }
                    updateRegistration(1, viewModelCurrentDate);


                    if (viewModelCurrentDate != null) {
                        // read viewModel.currentDate.get()
                        viewModelCurrentDateGet = viewModelCurrentDate.get();
                    }
            }
            if ((dirtyFlags & 0x3404L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.calendarItems
                        viewModelCalendarItems = viewModel.getCalendarItems();
                    }
                    updateRegistration(2, viewModelCalendarItems);
            }
            if ((dirtyFlags & 0x3818L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentDay
                        viewModelCurrentDay = viewModel.getCurrentDay();
                        // read viewModel.currentTasks
                        viewModelCurrentTasks = viewModel.getCurrentTasks();
                    }
                    updateRegistration(3, viewModelCurrentDay);
                    updateRegistration(4, viewModelCurrentTasks);


                    if (viewModelCurrentDay != null) {
                        // read viewModel.currentDay.get()
                        viewModelCurrentDayGet = viewModelCurrentDay.get();
                    }
            }
            if ((dirtyFlags & 0x3120L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isToday()
                        viewModelIsToday = viewModel.isToday();
                    }
                    updateRegistration(5, viewModelIsToday);


                    if (viewModelIsToday != null) {
                        // read viewModel.isToday().get()
                        viewModelIsTodayGet = viewModelIsToday.get();
                    }
                if((dirtyFlags & 0x3120L) != 0) {
                    if(viewModelIsTodayGet) {
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x4000L;
                    }
                }
            }
            if ((dirtyFlags & 0x3040L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isMale
                        viewModelIsMale = viewModel.isMale();
                    }
                    updateRegistration(6, viewModelIsMale);


                    if (viewModelIsMale != null) {
                        // read viewModel.isMale.get()
                        viewModelIsMaleGet = viewModelIsMale.get();
                    }
                if((dirtyFlags & 0x3040L) != 0) {
                    if(viewModelIsMaleGet) {
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x10000L;
                    }
                }


                    // read viewModel.isMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                    viewModelIsMaleIcProfileAndroidDrawableIcProfileMaleIcProfileAndroidDrawableIcProfileFemale = ((viewModelIsMaleGet) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icProfile.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icProfile.getContext(), R.drawable.ic_profile_female)));
            }
            if ((dirtyFlags & 0x3080L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.currentMarathonDay
                        viewModelCurrentMarathonDay = viewModel.getCurrentMarathonDay();
                    }
                    updateRegistration(7, viewModelCurrentMarathonDay);


                    if (viewModelCurrentMarathonDay != null) {
                        // read viewModel.currentMarathonDay.get()
                        viewModelCurrentMarathonDayGet = viewModelCurrentMarathonDay.get();
                    }


                    // read viewModel.currentMarathonDay.get() < 1
                    viewModelCurrentMarathonDayInt1 = (viewModelCurrentMarathonDayGet) < (1);
                if((dirtyFlags & 0x3080L) != 0) {
                    if(viewModelCurrentMarathonDayInt1) {
                            dirtyFlags |= 0x800000L;
                    }
                    else {
                            dirtyFlags |= 0x400000L;
                    }
                }
            }
            if ((dirtyFlags & 0x3200L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(9, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x3200L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x80000L;
                            dirtyFlags |= 0x200000L;
                    }
                    else {
                            dirtyFlags |= 0x40000L;
                            dirtyFlags |= 0x100000L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.GONE : View.VISIBLE
                    viewModelIsProgressViewGONEViewVISIBLE = ((viewModelIsProgressGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished

        if ((dirtyFlags & 0x400000L) != 0) {

                // read @android:string/marathon_tasks_today
                tvCurrentDayAndroidStringMarathonTasksTodayViewModelCurrentMarathonDay = tvCurrentDay.getResources().getString(R.string.marathon_tasks_today, viewModelCurrentMarathonDayGet);
        }
        if ((dirtyFlags & 0x4000L) != 0) {

                if (viewModel != null) {
                    // read viewModel.currentTasksDate
                    viewModelCurrentTasksDate = viewModel.getCurrentTasksDate();
                }
                updateRegistration(8, viewModelCurrentTasksDate);


                if (viewModelCurrentTasksDate != null) {
                    // read viewModel.currentTasksDate.get()
                    viewModelCurrentTasksDateGet = viewModelCurrentTasksDate.get();
                }


                // read @android:string/marathon_tasks_format
                mboundView9AndroidStringMarathonTasksFormatViewModelCurrentTasksDate = mboundView9.getResources().getString(R.string.marathon_tasks_format, viewModelCurrentTasksDateGet);
        }

        if ((dirtyFlags & 0x3120L) != 0) {

                // read viewModel.isToday().get() ? @android:string/marathon_tasks : @android:string/marathon_tasks_format
                viewModelIsTodayMboundView9AndroidStringMarathonTasksMboundView9AndroidStringMarathonTasksFormatViewModelCurrentTasksDate = ((viewModelIsTodayGet) ? (mboundView9.getResources().getString(R.string.marathon_tasks)) : (mboundView9AndroidStringMarathonTasksFormatViewModelCurrentTasksDate));
        }
        if ((dirtyFlags & 0x3080L) != 0) {

                // read viewModel.currentMarathonDay.get() < 1 ? @android:string/marathon_tasks_prepare_week : @android:string/marathon_tasks_today
                viewModelCurrentMarathonDayInt1TvCurrentDayAndroidStringMarathonTasksPrepareWeekTvCurrentDayAndroidStringMarathonTasksTodayViewModelCurrentMarathonDay = ((viewModelCurrentMarathonDayInt1) ? (tvCurrentDay.getResources().getString(R.string.marathon_tasks_prepare_week)) : (tvCurrentDayAndroidStringMarathonTasksTodayViewModelCurrentMarathonDay));
        }
        // batch finished
        if ((dirtyFlags & 0x3040L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icProfile, viewModelIsMaleIcProfileAndroidDrawableIcProfileMaleIcProfileAndroidDrawableIcProfileFemale);
        }
        if ((dirtyFlags & 0x3818L) != 0) {
            // api target 1

            fit.lerchek.ui.feature.marathon.today.adapters.TodayTasksAdapterKt.setupTodayTasksAdapter(this.mboundView10, viewModelCurrentDayGet, viewModelCurrentTasks, taskCallback);
        }
        if ((dirtyFlags & 0x3002L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, viewModelCurrentDateGet);
        }
        if ((dirtyFlags & 0x3404L) != 0) {
            // api target 1

            fit.lerchek.ui.feature.marathon.activity.ActivityCalendarAdapterKt.setCalendarAdapter(this.mboundView8, viewModelCalendarItems, calendarCallback);
        }
        if ((dirtyFlags & 0x3120L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView9, viewModelIsTodayMboundView9AndroidStringMarathonTasksMboundView9AndroidStringMarathonTasksFormatViewModelCurrentTasksDate);
        }
        if ((dirtyFlags & 0x3200L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
            this.svCalendar.setVisibility(viewModelIsProgressViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x3080L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCurrentDay, viewModelCurrentMarathonDayInt1TvCurrentDayAndroidStringMarathonTasksPrepareWeekTvCurrentDayAndroidStringMarathonTasksTodayViewModelCurrentMarathonDay);
        }
        if ((dirtyFlags & 0x3001L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHello, tvHelloAndroidStringMarathonTasksHelloViewModelProfileName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.profileName
        flag 1 (0x2L): viewModel.currentDate
        flag 2 (0x3L): viewModel.calendarItems
        flag 3 (0x4L): viewModel.currentDay
        flag 4 (0x5L): viewModel.currentTasks
        flag 5 (0x6L): viewModel.isToday()
        flag 6 (0x7L): viewModel.isMale
        flag 7 (0x8L): viewModel.currentMarathonDay
        flag 8 (0x9L): viewModel.currentTasksDate
        flag 9 (0xaL): viewModel.isProgress()
        flag 10 (0xbL): calendarCallback
        flag 11 (0xcL): taskCallback
        flag 12 (0xdL): viewModel
        flag 13 (0xeL): null
        flag 14 (0xfL): viewModel.isToday().get() ? @android:string/marathon_tasks : @android:string/marathon_tasks_format
        flag 15 (0x10L): viewModel.isToday().get() ? @android:string/marathon_tasks : @android:string/marathon_tasks_format
        flag 16 (0x11L): viewModel.isMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 17 (0x12L): viewModel.isMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 18 (0x13L): viewModel.isProgress().get() ? View.GONE : View.VISIBLE
        flag 19 (0x14L): viewModel.isProgress().get() ? View.GONE : View.VISIBLE
        flag 20 (0x15L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 21 (0x16L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 22 (0x17L): viewModel.currentMarathonDay.get() < 1 ? @android:string/marathon_tasks_prepare_week : @android:string/marathon_tasks_today
        flag 23 (0x18L): viewModel.currentMarathonDay.get() < 1 ? @android:string/marathon_tasks_prepare_week : @android:string/marathon_tasks_today
    flag mapping end*/
    //end
}