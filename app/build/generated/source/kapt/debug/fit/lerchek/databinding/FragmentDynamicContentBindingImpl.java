package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDynamicContentBindingImpl extends FragmentDynamicContentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentDynamicContentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private FragmentDynamicContentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.FrameLayout) bindings[2]
            , (androidx.recyclerview.widget.RecyclerView) bindings[1]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (bindings[3] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[3]) : null;
        this.progressBlock.setTag(null);
        this.rvDynamicItems.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.lifecycleHolder == variableId) {
            setLifecycleHolder((fit.lerchek.ui.feature.marathon.LifecycleHolder) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.DynamicContentViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setLifecycleHolder(@Nullable fit.lerchek.ui.feature.marathon.LifecycleHolder LifecycleHolder) {
        this.mLifecycleHolder = LifecycleHolder;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.lifecycleHolder);
        super.requestRebind();
    }
    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.DynamicContentViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelContent((androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel>) object, fieldId);
            case 1 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelContent(androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel> ViewModelContent, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel> viewModelContent = null;
        fit.lerchek.ui.feature.marathon.LifecycleHolder lifecycleHolder = mLifecycleHolder;
        java.util.List<fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel.DynamicContentListItem> viewModelContentDisplayItems = null;
        fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel viewModelContentGetValue = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.DynamicContentViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x1dL) != 0) {
        }
        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x1dL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.content
                        viewModelContent = viewModel.getContent();
                    }
                    updateLiveDataRegistration(0, viewModelContent);


                    if (viewModelContent != null) {
                        // read viewModel.content.getValue()
                        viewModelContentGetValue = viewModelContent.getValue();
                    }


                    if (viewModelContentGetValue != null) {
                        // read viewModel.content.getValue().displayItems
                        viewModelContentDisplayItems = viewModelContentGetValue.getDisplayItems();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(1, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1aL) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x1dL) != 0) {
            // api target 1

            fit.lerchek.ui.feature.marathon.DynamicContentAdapterKt.setupDynamicContentAdapter(this.rvDynamicItems, viewModelContentDisplayItems, lifecycleHolder);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.content
        flag 1 (0x2L): viewModel.isProgress()
        flag 2 (0x3L): lifecycleHolder
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 6 (0x7L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}