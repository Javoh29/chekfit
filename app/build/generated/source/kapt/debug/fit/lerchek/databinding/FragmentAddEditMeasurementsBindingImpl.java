package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentAddEditMeasurementsBindingImpl extends FragmentAddEditMeasurementsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btn_close, 5);
        sViewsWithIds.put(R.id.constraintLayoutHeight, 6);
        sViewsWithIds.put(R.id.btn_save, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView3;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener heightandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.value.get()
            //         is viewModel.value.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(height);
            // localize variables for thread safety
            // viewModel.value != null
            boolean viewModelValueJavaLangObjectNull = false;
            // viewModel.value.get()
            java.lang.String viewModelValueGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.progress.AddEditMeasurementsViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.value
            androidx.databinding.ObservableField<java.lang.String> viewModelValue = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelValue = viewModel.getValue();

                viewModelValueJavaLangObjectNull = (viewModelValue) != (null);
                if (viewModelValueJavaLangObjectNull) {




                    viewModelValue.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentAddEditMeasurementsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private FragmentAddEditMeasurementsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[2]
            , (android.widget.FrameLayout) bindings[3]
            );
        this.height.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (bindings[4] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[4]) : null;
        this.progressBlock.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.progress.AddEditMeasurementsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.progress.AddEditMeasurementsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelData((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.AddEditMeasurementsUIModel>) object, fieldId);
            case 1 :
                return onChangeViewModelValue((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelData(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.AddEditMeasurementsUIModel> ViewModelData, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelValue(androidx.databinding.ObservableField<java.lang.String> ViewModelValue, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelDataTypeHintRes = 0;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.AddEditMeasurementsUIModel> viewModelData = null;
        fit.lerchek.data.domain.uimodel.marathon.AddEditMeasurementsUIModel viewModelDataGet = null;
        int viewModelDataTypeTitleRes = 0;
        java.lang.String viewModelValueGet = null;
        fit.lerchek.data.domain.uimodel.marathon.AddEditMeasurementsUIModel.Type viewModelDataType = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.progress.AddEditMeasurementsViewModel viewModel = mViewModel;
        androidx.databinding.ObservableField<java.lang.String> viewModelValue = null;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.data
                        viewModelData = viewModel.getData();
                    }
                    updateRegistration(0, viewModelData);


                    if (viewModelData != null) {
                        // read viewModel.data.get()
                        viewModelDataGet = viewModelData.get();
                    }


                    if (viewModelDataGet != null) {
                        // read viewModel.data.get().type
                        viewModelDataType = viewModelDataGet.getType();
                    }


                    if (viewModelDataType != null) {
                        // read viewModel.data.get().type.hintRes
                        viewModelDataTypeHintRes = viewModelDataType.getHintRes();
                        // read viewModel.data.get().type.titleRes
                        viewModelDataTypeTitleRes = viewModelDataType.getTitleRes();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.value
                        viewModelValue = viewModel.getValue();
                    }
                    updateRegistration(1, viewModelValue);


                    if (viewModelValue != null) {
                        // read viewModel.value.get()
                        viewModelValueGet = viewModelValue.get();
                    }
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(2, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1cL) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            this.height.setHint(viewModelDataTypeHintRes);
            this.mboundView1.setText(viewModelDataTypeTitleRes);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.height, viewModelValueGet);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.height, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, heightandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.data
        flag 1 (0x2L): viewModel.value
        flag 2 (0x3L): viewModel.isProgress()
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 6 (0x7L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}