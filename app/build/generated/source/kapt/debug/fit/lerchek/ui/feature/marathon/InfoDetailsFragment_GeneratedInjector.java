package fit.lerchek.ui.feature.marathon;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = InfoDetailsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface InfoDetailsFragment_GeneratedInjector {
  void injectInfoDetailsFragment(InfoDetailsFragment infoDetailsFragment);
}
