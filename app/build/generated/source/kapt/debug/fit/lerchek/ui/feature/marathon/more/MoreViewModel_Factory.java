// Generated by Dagger (https://dagger.dev).
package fit.lerchek.ui.feature.marathon.more;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import fit.lerchek.data.repository.user.UserRepository;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class MoreViewModel_Factory implements Factory<MoreViewModel> {
  private final Provider<UserRepository> userRepositoryProvider;

  public MoreViewModel_Factory(Provider<UserRepository> userRepositoryProvider) {
    this.userRepositoryProvider = userRepositoryProvider;
  }

  @Override
  public MoreViewModel get() {
    return newInstance(userRepositoryProvider.get());
  }

  public static MoreViewModel_Factory create(Provider<UserRepository> userRepositoryProvider) {
    return new MoreViewModel_Factory(userRepositoryProvider);
  }

  public static MoreViewModel newInstance(UserRepository userRepository) {
    return new MoreViewModel(userRepository);
  }
}
