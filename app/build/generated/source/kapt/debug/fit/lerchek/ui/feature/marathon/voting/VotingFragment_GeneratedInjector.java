package fit.lerchek.ui.feature.marathon.voting;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = VotingFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface VotingFragment_GeneratedInjector {
  void injectVotingFragment(VotingFragment votingFragment);
}
