package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemVotingUserBindingImpl extends ItemVotingUserBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imagesPager, 6);
        sViewsWithIds.put(R.id.btnImageNext, 7);
        sViewsWithIds.put(R.id.btnImagePrev, 8);
        sViewsWithIds.put(R.id.tabs, 9);
        sViewsWithIds.put(R.id.btnLike, 10);
        sViewsWithIds.put(R.id.ivLike, 11);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemVotingUserBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private ItemVotingUserBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[8]
            , (android.widget.FrameLayout) bindings[10]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.viewpager2.widget.ViewPager2) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[11]
            , (com.google.android.material.tabs.TabLayout) bindings[9]
            );
        this.errorWithdrawView.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.VotingItemUIModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.VotingItemUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.VotingItemUIModel model = mModel;
        int modelIsMaleErrorWithdrawViewAndroidColorMaleHintErrorWithdrawViewAndroidColorFemaleHint = 0;
        boolean modelIsMale = false;
        java.lang.String modelTitle = null;
        java.lang.String modelStartWeight = null;
        java.lang.String modelEndWeight = null;
        java.lang.String modelVotesString = null;
        java.lang.String modelWeightResult = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read model.isMale
                    modelIsMale = model.isMale();
                    // read model.title
                    modelTitle = model.getTitle();
                    // read model.startWeight
                    modelStartWeight = model.getStartWeight();
                    // read model.endWeight
                    modelEndWeight = model.getEndWeight();
                    // read model.votesString()
                    modelVotesString = model.votesString();
                    // read model.weightResult
                    modelWeightResult = model.getWeightResult();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelIsMale) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read model.isMale ? @android:color/male_hint : @android:color/female_hint
                modelIsMaleErrorWithdrawViewAndroidColorMaleHintErrorWithdrawViewAndroidColorFemaleHint = ((modelIsMale) ? (getColorFromResource(errorWithdrawView, R.color.male_hint)) : (getColorFromResource(errorWithdrawView, R.color.female_hint)));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 21
            if(getBuildSdkInt() >= 21) {

                this.errorWithdrawView.setBackgroundTintList(androidx.databinding.adapters.Converters.convertColorToColorStateList(modelIsMaleErrorWithdrawViewAndroidColorMaleHintErrorWithdrawViewAndroidColorFemaleHint));
            }
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.errorWithdrawView, modelWeightResult);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, modelTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, modelStartWeight);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, modelEndWeight);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, modelVotesString);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): null
        flag 2 (0x3L): model.isMale ? @android:color/male_hint : @android:color/female_hint
        flag 3 (0x4L): model.isMale ? @android:color/male_hint : @android:color/female_hint
    flag mapping end*/
    //end
}