package fit.lerchek.ui.feature.marathon.profile.personal;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = UserPersonalInfoFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface UserPersonalInfoFragment_GeneratedInjector {
  void injectUserPersonalInfoFragment(UserPersonalInfoFragment userPersonalInfoFragment);
}
