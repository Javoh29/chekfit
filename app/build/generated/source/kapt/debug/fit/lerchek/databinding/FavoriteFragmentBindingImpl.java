package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FavoriteFragmentBindingImpl extends FavoriteFragmentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 7);
        sViewsWithIds.put(R.id.imgArrow, 9);
        sViewsWithIds.put(R.id.imgNutritionPlan, 10);
        sViewsWithIds.put(R.id.llText, 11);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final android.widget.FrameLayout mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView4;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FavoriteFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private FavoriteFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (bindings[7] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[7]) : null
            , (androidx.cardview.widget.CardView) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[9]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[10]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.FrameLayout) bindings[6]
            , (androidx.recyclerview.widget.RecyclerView) bindings[5]
            );
        this.btnSelectMeal.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.FrameLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView6 = (bindings[8] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[8]) : null;
        this.progressBlock.setTag(null);
        this.rvFavorite.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x40L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.food.FoodCallback) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.food.FavoriteViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.food.FoodCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }
    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.food.FavoriteViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x20L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelFavoriteItem((androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel>) object, fieldId);
            case 1 :
                return onChangeViewModelIsEmpty((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelDisplayItems((androidx.lifecycle.MutableLiveData<fit.lerchek.data.api.model.Meal>) object, fieldId);
            case 3 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelFavoriteItem(androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel> ViewModelFavoriteItem, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsEmpty(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsEmpty, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelDisplayItems(androidx.lifecycle.MutableLiveData<fit.lerchek.data.api.model.Meal> ViewModelDisplayItems, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel> viewModelFavoriteItem = null;
        java.lang.String viewModelDisplayItemsName = null;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsEmpty = null;
        fit.lerchek.ui.feature.marathon.food.FoodCallback callback = mCallback;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        fit.lerchek.data.api.model.Meal viewModelDisplayItemsGetValue = null;
        java.lang.Boolean viewModelIsEmptyGetValue = null;
        androidx.lifecycle.MutableLiveData<fit.lerchek.data.api.model.Meal> viewModelDisplayItems = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsEmptyGetValue = false;
        int viewModelIsEmptyViewGONEViewVISIBLE = 0;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.food.FavoriteViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x71L) != 0) {
        }
        if ((dirtyFlags & 0x7fL) != 0) {


            if ((dirtyFlags & 0x71L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.favoriteItem
                        viewModelFavoriteItem = viewModel.getFavoriteItem();
                    }
                    updateRegistration(0, viewModelFavoriteItem);
            }
            if ((dirtyFlags & 0x62L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isEmpty
                        viewModelIsEmpty = viewModel.isEmpty();
                    }
                    updateLiveDataRegistration(1, viewModelIsEmpty);


                    if (viewModelIsEmpty != null) {
                        // read viewModel.isEmpty.getValue()
                        viewModelIsEmptyGetValue = viewModelIsEmpty.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEmpty.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsEmptyGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsEmptyGetValue);
                if((dirtyFlags & 0x62L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelIsEmptyGetValue) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEmpty.getValue()) ? View.GONE : View.VISIBLE
                    viewModelIsEmptyViewGONEViewVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelIsEmptyGetValue) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x64L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.displayItems
                        viewModelDisplayItems = viewModel.getDisplayItems();
                    }
                    updateLiveDataRegistration(2, viewModelDisplayItems);


                    if (viewModelDisplayItems != null) {
                        // read viewModel.displayItems.getValue()
                        viewModelDisplayItemsGetValue = viewModelDisplayItems.getValue();
                    }


                    if (viewModelDisplayItemsGetValue != null) {
                        // read viewModel.displayItems.getValue().name
                        viewModelDisplayItemsName = viewModelDisplayItemsGetValue.getName();
                    }
            }
            if ((dirtyFlags & 0x68L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(3, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x68L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x62L) != 0) {
            // api target 1

            this.btnSelectMeal.setVisibility(viewModelIsEmptyViewGONEViewVISIBLE);
            this.mboundView4.setVisibility(viewModelIsEmptyViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x64L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelDisplayItemsName);
        }
        if ((dirtyFlags & 0x68L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x71L) != 0) {
            // api target 1

            fit.lerchek.ui.feature.marathon.food.FavoriteAdapterKt.setupFavoriteItemsAdapter(this.rvFavorite, viewModelFavoriteItem, callback);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.favoriteItem
        flag 1 (0x2L): viewModel.isEmpty
        flag 2 (0x3L): viewModel.displayItems
        flag 3 (0x4L): viewModel.isProgress()
        flag 4 (0x5L): callback
        flag 5 (0x6L): viewModel
        flag 6 (0x7L): null
        flag 7 (0x8L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEmpty.getValue()) ? View.GONE : View.VISIBLE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isEmpty.getValue()) ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}