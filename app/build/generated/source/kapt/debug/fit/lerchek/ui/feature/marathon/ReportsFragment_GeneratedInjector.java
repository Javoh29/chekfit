package fit.lerchek.ui.feature.marathon;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ReportsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ReportsFragment_GeneratedInjector {
  void injectReportsFragment(ReportsFragment reportsFragment);
}
