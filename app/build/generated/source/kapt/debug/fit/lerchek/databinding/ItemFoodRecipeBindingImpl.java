package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemFoodRecipeBindingImpl extends ItemFoodRecipeBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.cardRecipe, 6);
        sViewsWithIds.put(R.id.imgRecipe, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemFoodRecipeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private ItemFoodRecipeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (androidx.cardview.widget.CardView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            );
        this.btnFavorite.setTag(null);
        this.btnReplace.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.tvCalories.setTag(null);
        this.tvName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel model = mModel;
        android.graphics.drawable.Drawable modelFavoriteBtnFavoriteAndroidDrawableIcLikeActiveBtnFavoriteAndroidDrawableIcLikeInactive = null;
        java.lang.String modelName = null;
        java.lang.String modelMealName = null;
        int modelMealId = 0;
        boolean modelHasReplacements = false;
        java.lang.String tvCaloriesAndroidStringTodayCcalModelCalories = null;
        boolean modelFavorite = false;
        int modelHasReplacementsViewVISIBLEViewGONE = 0;
        java.lang.String modelFavoriteBtnFavoriteAndroidStringFoodReplaceFavoriteBtnFavoriteAndroidStringFoodReplaceFavoriteInactive = null;
        java.lang.String modelCalories = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read model.name
                    modelName = model.getName();
                    // read model.mealName
                    modelMealName = model.getMealName();
                    // read model.mealId
                    modelMealId = model.getMealId();
                    // read model.hasReplacements
                    modelHasReplacements = model.getHasReplacements();
                    // read model.favorite
                    modelFavorite = model.isFavorite();
                    // read model.calories
                    modelCalories = model.getCalories();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelHasReplacements) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelFavorite) {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x4L;
                        dirtyFlags |= 0x40L;
                }
            }


                // read model.hasReplacements ? View.VISIBLE : View.GONE
                modelHasReplacementsViewVISIBLEViewGONE = ((modelHasReplacements) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read model.favorite ? @android:drawable/ic_like_active : @android:drawable/ic_like_inactive
                modelFavoriteBtnFavoriteAndroidDrawableIcLikeActiveBtnFavoriteAndroidDrawableIcLikeInactive = ((modelFavorite) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnFavorite.getContext(), R.drawable.ic_like_active)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnFavorite.getContext(), R.drawable.ic_like_inactive)));
                // read model.favorite ? @android:string/food_replace_favorite : @android:string/food_replace_favorite_inactive
                modelFavoriteBtnFavoriteAndroidStringFoodReplaceFavoriteBtnFavoriteAndroidStringFoodReplaceFavoriteInactive = ((modelFavorite) ? (btnFavorite.getResources().getString(R.string.food_replace_favorite)) : (btnFavorite.getResources().getString(R.string.food_replace_favorite_inactive)));
                // read @android:string/today_ccal
                tvCaloriesAndroidStringTodayCcalModelCalories = tvCalories.getResources().getString(R.string.today_ccal, modelCalories);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnFavorite, modelFavoriteBtnFavoriteAndroidStringFoodReplaceFavoriteBtnFavoriteAndroidStringFoodReplaceFavoriteInactive);
            androidx.databinding.adapters.TextViewBindingAdapter.setDrawableStart(this.btnFavorite, modelFavoriteBtnFavoriteAndroidDrawableIcLikeActiveBtnFavoriteAndroidDrawableIcLikeInactive);
            this.btnReplace.setVisibility(modelHasReplacementsViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, modelMealName);
            fit.lerchek.ui.feature.marathon.today.adapters.TodayRecipesAdapterKt.setMealName(this.mboundView5, modelMealId);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCalories, tvCaloriesAndroidStringTodayCcalModelCalories);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvName, modelName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): null
        flag 2 (0x3L): model.favorite ? @android:drawable/ic_like_active : @android:drawable/ic_like_inactive
        flag 3 (0x4L): model.favorite ? @android:drawable/ic_like_active : @android:drawable/ic_like_inactive
        flag 4 (0x5L): model.hasReplacements ? View.VISIBLE : View.GONE
        flag 5 (0x6L): model.hasReplacements ? View.VISIBLE : View.GONE
        flag 6 (0x7L): model.favorite ? @android:string/food_replace_favorite : @android:string/food_replace_favorite_inactive
        flag 7 (0x8L): model.favorite ? @android:string/food_replace_favorite : @android:string/food_replace_favorite_inactive
    flag mapping end*/
    //end
}