package fit.lerchek.ui.feature.marathon.workout;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = WorkoutDetailsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface WorkoutDetailsFragment_GeneratedInjector {
  void injectWorkoutDetailsFragment(WorkoutDetailsFragment workoutDetailsFragment);
}
