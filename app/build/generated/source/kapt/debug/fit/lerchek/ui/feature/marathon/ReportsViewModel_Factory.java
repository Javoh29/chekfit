// Generated by Dagger (https://dagger.dev).
package fit.lerchek.ui.feature.marathon;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import fit.lerchek.data.repository.user.UserRepository;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ReportsViewModel_Factory implements Factory<ReportsViewModel> {
  private final Provider<UserRepository> userRepositoryProvider;

  public ReportsViewModel_Factory(Provider<UserRepository> userRepositoryProvider) {
    this.userRepositoryProvider = userRepositoryProvider;
  }

  @Override
  public ReportsViewModel get() {
    return newInstance(userRepositoryProvider.get());
  }

  public static ReportsViewModel_Factory create(Provider<UserRepository> userRepositoryProvider) {
    return new ReportsViewModel_Factory(userRepositoryProvider);
  }

  public static ReportsViewModel newInstance(UserRepository userRepository) {
    return new ReportsViewModel(userRepository);
  }
}
