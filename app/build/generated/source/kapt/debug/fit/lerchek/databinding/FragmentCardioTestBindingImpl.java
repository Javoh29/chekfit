package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCardioTestBindingImpl extends FragmentCardioTestBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 10);
        sViewsWithIds.put(R.id.nsvCardioTest, 12);
        sViewsWithIds.put(R.id.imgCardioTest, 13);
        sViewsWithIds.put(R.id.constraintLayoutAge, 14);
        sViewsWithIds.put(R.id.imgHeartRateEmpty, 15);
        sViewsWithIds.put(R.id.heartRateLimitLowerTitle, 16);
        sViewsWithIds.put(R.id.imgHeartRateFill, 17);
        sViewsWithIds.put(R.id.heartRateLimitUpperTitle, 18);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView6;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView8;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback5;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener ageandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.age.get()
            //         is viewModel.age.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(age);
            // localize variables for thread safety
            // viewModel.age.get()
            java.lang.String viewModelAgeGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.cardiotest.CardioTestViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.age != null
            boolean viewModelAgeJavaLangObjectNull = false;
            // viewModel.age
            androidx.databinding.ObservableField<java.lang.String> viewModelAge = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAge = viewModel.getAge();

                viewModelAgeJavaLangObjectNull = (viewModelAge) != (null);
                if (viewModelAgeJavaLangObjectNull) {




                    viewModelAge.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentCardioTestBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private FragmentCardioTestBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (androidx.appcompat.widget.AppCompatEditText) bindings[2]
            , (bindings[10] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[10]) : null
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[14]
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[16]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[5]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[18]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[13]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[15]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[17]
            , (androidx.core.widget.NestedScrollView) bindings[12]
            , (android.widget.FrameLayout) bindings[9]
            );
        this.age.setTag(null);
        this.btnCalculate.setTag(null);
        this.frameLayout.setTag(null);
        this.heartRateLimitLowerValue.setTag(null);
        this.heartRateLimitUpperValue.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView8 = (androidx.appcompat.widget.AppCompatTextView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (bindings[11] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[11]) : null;
        this.progressBlock.setTag(null);
        setRootTag(root);
        // listeners
        mCallback5 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.more.cardiotest.CardioTestViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.more.cardiotest.CardioTestViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelAge((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelIsCalculate((androidx.databinding.ObservableBoolean) object, fieldId);
            case 2 :
                return onChangeViewModelHeartRateLimitUpper((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelHeartRateLimitUpperSpeedUnit((androidx.databinding.ObservableInt) object, fieldId);
            case 4 :
                return onChangeViewModelHeartRateLimitLowerSpeedUnit((androidx.databinding.ObservableInt) object, fieldId);
            case 5 :
                return onChangeViewModelHeartRateLimitLower((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelAge(androidx.databinding.ObservableField<java.lang.String> ViewModelAge, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsCalculate(androidx.databinding.ObservableBoolean ViewModelIsCalculate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHeartRateLimitUpper(androidx.databinding.ObservableField<java.lang.String> ViewModelHeartRateLimitUpper, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHeartRateLimitUpperSpeedUnit(androidx.databinding.ObservableInt ViewModelHeartRateLimitUpperSpeedUnit, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHeartRateLimitLowerSpeedUnit(androidx.databinding.ObservableInt ViewModelHeartRateLimitLowerSpeedUnit, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHeartRateLimitLower(androidx.databinding.ObservableField<java.lang.String> ViewModelHeartRateLimitLower, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelHeartRateLimitUpperSpeedUnitGet = 0;
        int viewModelIsCalculateViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelAge = null;
        androidx.databinding.ObservableBoolean viewModelIsCalculate = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelHeartRateLimitUpper = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableInt viewModelHeartRateLimitUpperSpeedUnit = null;
        boolean viewModelIsCalculateGet = false;
        java.lang.String viewModelHeartRateLimitUpperGet = null;
        java.lang.String viewModelAgeGet = null;
        androidx.databinding.ObservableInt viewModelHeartRateLimitLowerSpeedUnit = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelHeartRateLimitLower = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.more.cardiotest.CardioTestViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;
        int viewModelHeartRateLimitLowerSpeedUnitGet = 0;
        java.lang.String viewModelHeartRateLimitLowerGet = null;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.age
                        viewModelAge = viewModel.getAge();
                    }
                    updateRegistration(0, viewModelAge);


                    if (viewModelAge != null) {
                        // read viewModel.age.get()
                        viewModelAgeGet = viewModelAge.get();
                    }
            }
            if ((dirtyFlags & 0x182L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isCalculate()
                        viewModelIsCalculate = viewModel.isCalculate();
                    }
                    updateRegistration(1, viewModelIsCalculate);


                    if (viewModelIsCalculate != null) {
                        // read viewModel.isCalculate().get()
                        viewModelIsCalculateGet = viewModelIsCalculate.get();
                    }
                if((dirtyFlags & 0x182L) != 0) {
                    if(viewModelIsCalculateGet) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
                    viewModelIsCalculateViewVISIBLEViewGONE = ((viewModelIsCalculateGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x184L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.heartRateLimitUpper
                        viewModelHeartRateLimitUpper = viewModel.getHeartRateLimitUpper();
                    }
                    updateRegistration(2, viewModelHeartRateLimitUpper);


                    if (viewModelHeartRateLimitUpper != null) {
                        // read viewModel.heartRateLimitUpper.get()
                        viewModelHeartRateLimitUpperGet = viewModelHeartRateLimitUpper.get();
                    }
            }
            if ((dirtyFlags & 0x188L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.heartRateLimitUpperSpeedUnit
                        viewModelHeartRateLimitUpperSpeedUnit = viewModel.getHeartRateLimitUpperSpeedUnit();
                    }
                    updateRegistration(3, viewModelHeartRateLimitUpperSpeedUnit);


                    if (viewModelHeartRateLimitUpperSpeedUnit != null) {
                        // read viewModel.heartRateLimitUpperSpeedUnit.get()
                        viewModelHeartRateLimitUpperSpeedUnitGet = viewModelHeartRateLimitUpperSpeedUnit.get();
                    }
            }
            if ((dirtyFlags & 0x190L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.heartRateLimitLowerSpeedUnit
                        viewModelHeartRateLimitLowerSpeedUnit = viewModel.getHeartRateLimitLowerSpeedUnit();
                    }
                    updateRegistration(4, viewModelHeartRateLimitLowerSpeedUnit);


                    if (viewModelHeartRateLimitLowerSpeedUnit != null) {
                        // read viewModel.heartRateLimitLowerSpeedUnit.get()
                        viewModelHeartRateLimitLowerSpeedUnitGet = viewModelHeartRateLimitLowerSpeedUnit.get();
                    }
            }
            if ((dirtyFlags & 0x1a0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.heartRateLimitLower
                        viewModelHeartRateLimitLower = viewModel.getHeartRateLimitLower();
                    }
                    updateRegistration(5, viewModelHeartRateLimitLower);


                    if (viewModelHeartRateLimitLower != null) {
                        // read viewModel.heartRateLimitLower.get()
                        viewModelHeartRateLimitLowerGet = viewModelHeartRateLimitLower.get();
                    }
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(6, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1c0L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.age, viewModelAgeGet);
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.age, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, ageandroidTextAttrChanged);
            this.btnCalculate.setOnClickListener(mCallback5);
        }
        if ((dirtyFlags & 0x1a0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.heartRateLimitLowerValue, viewModelHeartRateLimitLowerGet);
        }
        if ((dirtyFlags & 0x184L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.heartRateLimitUpperValue, viewModelHeartRateLimitUpperGet);
        }
        if ((dirtyFlags & 0x182L) != 0) {
            // api target 1

            this.mboundView4.setVisibility(viewModelIsCalculateViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x190L) != 0) {
            // api target 1

            this.mboundView6.setText(viewModelHeartRateLimitLowerSpeedUnitGet);
        }
        if ((dirtyFlags & 0x188L) != 0) {
            // api target 1

            this.mboundView8.setText(viewModelHeartRateLimitUpperSpeedUnitGet);
        }
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        fit.lerchek.ui.feature.marathon.more.cardiotest.CardioTestViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.calculate();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.age
        flag 1 (0x2L): viewModel.isCalculate()
        flag 2 (0x3L): viewModel.heartRateLimitUpper
        flag 3 (0x4L): viewModel.heartRateLimitUpperSpeedUnit
        flag 4 (0x5L): viewModel.heartRateLimitLowerSpeedUnit
        flag 5 (0x6L): viewModel.heartRateLimitLower
        flag 6 (0x7L): viewModel.isProgress()
        flag 7 (0x8L): viewModel
        flag 8 (0x9L): null
        flag 9 (0xaL): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 11 (0xcL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 12 (0xdL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}