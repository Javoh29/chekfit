// Generated by Dagger (https://dagger.dev).
package fit.lerchek.ui.feature.marathon.progress;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import fit.lerchek.data.repository.user.UserRepository;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class MeasurementsViewModel_Factory implements Factory<MeasurementsViewModel> {
  private final Provider<UserRepository> userRepositoryProvider;

  public MeasurementsViewModel_Factory(Provider<UserRepository> userRepositoryProvider) {
    this.userRepositoryProvider = userRepositoryProvider;
  }

  @Override
  public MeasurementsViewModel get() {
    return newInstance(userRepositoryProvider.get());
  }

  public static MeasurementsViewModel_Factory create(
      Provider<UserRepository> userRepositoryProvider) {
    return new MeasurementsViewModel_Factory(userRepositoryProvider);
  }

  public static MeasurementsViewModel newInstance(UserRepository userRepository) {
    return new MeasurementsViewModel(userRepository);
  }
}
