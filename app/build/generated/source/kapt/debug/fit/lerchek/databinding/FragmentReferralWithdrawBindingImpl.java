package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentReferralWithdrawBindingImpl extends FragmentReferralWithdrawBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(18);
        sIncludes.setIncludes(9, 
            new String[] {"layout_ref_withdraw"},
            new int[] {13},
            new int[] {fit.lerchek.R.layout.layout_ref_withdraw});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.referralScrollView, 14);
        sViewsWithIds.put(R.id.phoneHint, 15);
        sViewsWithIds.put(R.id.sumHint, 16);
        sViewsWithIds.put(R.id.max, 17);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @Nullable
    private final fit.lerchek.databinding.IncludeBackBinding mboundView1;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView10;
    @NonNull
    private final android.widget.FrameLayout mboundView9;
    @Nullable
    private final fit.lerchek.databinding.LayoutRefWithdrawBinding mboundView91;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener phoneandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.phone.get()
            //         is viewModel.phone.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(phone);
            // localize variables for thread safety
            // viewModel.phone != null
            boolean viewModelPhoneJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.referral.ReferralWithdrawViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.phone
            androidx.databinding.ObservableField<java.lang.String> viewModelPhone = null;
            // viewModel.phone.get()
            java.lang.String viewModelPhoneGet = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPhone = viewModel.getPhone();

                viewModelPhoneJavaLangObjectNull = (viewModelPhone) != (null);
                if (viewModelPhoneJavaLangObjectNull) {




                    viewModelPhone.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener sumandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.amount.get()
            //         is viewModel.amount.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(sum);
            // localize variables for thread safety
            // viewModel.amount != null
            boolean viewModelAmountJavaLangObjectNull = false;
            // viewModel.amount
            androidx.databinding.ObservableField<java.lang.String> viewModelAmount = null;
            // viewModel.amount.get()
            java.lang.String viewModelAmountGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.referral.ReferralWithdrawViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAmount = viewModel.getAmount();

                viewModelAmountJavaLangObjectNull = (viewModelAmount) != (null);
                if (viewModelAmountJavaLangObjectNull) {




                    viewModelAmount.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentReferralWithdrawBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentReferralWithdrawBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[17]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[15]
            , (android.widget.FrameLayout) bindings[10]
            , (android.widget.ScrollView) bindings[14]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[16]
            );
        this.btnBack.setTag(null);
        this.constraintLayoutNewPwd.setTag(null);
        this.constraintLayoutSum.setTag(null);
        this.errorMessageAmount.setTag(null);
        this.errorMessagePhone.setTag(null);
        this.hintMessageAmount.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (bindings[11] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[11]) : null;
        this.mboundView10 = (bindings[12] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[12]) : null;
        this.mboundView9 = (android.widget.FrameLayout) bindings[9];
        this.mboundView9.setTag(null);
        this.mboundView91 = (fit.lerchek.databinding.LayoutRefWithdrawBinding) bindings[13];
        setContainedBinding(this.mboundView91);
        this.phone.setTag(null);
        this.progressBlock.setTag(null);
        this.sum.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x400L;
        }
        mboundView91.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (mboundView91.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.callback == variableId) {
            setCallback((fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback) variable);
        }
        else if (BR.price == variableId) {
            setPrice((java.lang.String) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.profile.referral.ReferralWithdrawViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCallback(@Nullable fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }
    public void setPrice(@Nullable java.lang.String Price) {
        this.mPrice = Price;
        synchronized(this) {
            mDirtyFlags |= 0x100L;
        }
        notifyPropertyChanged(BR.price);
        super.requestRebind();
    }
    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.profile.referral.ReferralWithdrawViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x200L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        mboundView91.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelErrorWithdraw((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeViewModelAmount((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelSuccessWithdraw((androidx.databinding.ObservableBoolean) object, fieldId);
            case 3 :
                return onChangeViewModelAmountError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelPhoneError((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeViewModelPhone((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelErrorWithdraw(androidx.databinding.ObservableBoolean ViewModelErrorWithdraw, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAmount(androidx.databinding.ObservableField<java.lang.String> ViewModelAmount, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelSuccessWithdraw(androidx.databinding.ObservableBoolean ViewModelSuccessWithdraw, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAmountError(androidx.databinding.ObservableField<java.lang.String> ViewModelAmountError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPhoneError(androidx.databinding.ObservableField<java.lang.String> ViewModelPhoneError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPhone(androidx.databinding.ObservableField<java.lang.String> ViewModelPhone, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelAmountErrorEmpty = false;
        java.lang.String viewModelPhoneGet = null;
        int viewModelAmountErrorEmptyViewVISIBLEViewGONE = 0;
        java.lang.String viewModelPhoneErrorGet = null;
        androidx.databinding.ObservableBoolean viewModelErrorWithdraw = null;
        fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback callback = mCallback;
        androidx.databinding.ObservableField<java.lang.String> viewModelAmount = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        java.lang.String price = mPrice;
        java.lang.String viewModelAmountErrorGet = null;
        androidx.databinding.ObservableBoolean viewModelSuccessWithdraw = null;
        boolean viewModelSuccessWithdrawGet = false;
        boolean viewModelErrorWithdrawGet = false;
        androidx.databinding.ObservableField<java.lang.String> viewModelAmountError = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPhoneError = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPhone = null;
        int viewModelAmountErrorEmptyViewGONEViewVISIBLE = 0;
        java.lang.String viewModelAmountGet = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.profile.referral.ReferralWithdrawViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x480L) != 0) {
        }
        if ((dirtyFlags & 0x500L) != 0) {
        }
        if ((dirtyFlags & 0x67fL) != 0) {


            if ((dirtyFlags & 0x601L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.errorWithdraw
                        viewModelErrorWithdraw = viewModel.getErrorWithdraw();
                    }
                    updateRegistration(0, viewModelErrorWithdraw);


                    if (viewModelErrorWithdraw != null) {
                        // read viewModel.errorWithdraw.get()
                        viewModelErrorWithdrawGet = viewModelErrorWithdraw.get();
                    }
            }
            if ((dirtyFlags & 0x602L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.amount
                        viewModelAmount = viewModel.getAmount();
                    }
                    updateRegistration(1, viewModelAmount);


                    if (viewModelAmount != null) {
                        // read viewModel.amount.get()
                        viewModelAmountGet = viewModelAmount.get();
                    }
            }
            if ((dirtyFlags & 0x604L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.successWithdraw
                        viewModelSuccessWithdraw = viewModel.getSuccessWithdraw();
                    }
                    updateRegistration(2, viewModelSuccessWithdraw);


                    if (viewModelSuccessWithdraw != null) {
                        // read viewModel.successWithdraw.get()
                        viewModelSuccessWithdrawGet = viewModelSuccessWithdraw.get();
                    }
            }
            if ((dirtyFlags & 0x608L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.amountError
                        viewModelAmountError = viewModel.getAmountError();
                    }
                    updateRegistration(3, viewModelAmountError);


                    if (viewModelAmountError != null) {
                        // read viewModel.amountError.get()
                        viewModelAmountErrorGet = viewModelAmountError.get();
                    }


                    if (viewModelAmountErrorGet != null) {
                        // read viewModel.amountError.get().empty
                        viewModelAmountErrorEmpty = viewModelAmountErrorGet.isEmpty();
                    }
                if((dirtyFlags & 0x608L) != 0) {
                    if(viewModelAmountErrorEmpty) {
                            dirtyFlags |= 0x1000L;
                            dirtyFlags |= 0x10000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                            dirtyFlags |= 0x8000L;
                    }
                }


                    // read viewModel.amountError.get().empty ? View.VISIBLE : View.GONE
                    viewModelAmountErrorEmptyViewVISIBLEViewGONE = ((viewModelAmountErrorEmpty) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.amountError.get().empty ? View.GONE : View.VISIBLE
                    viewModelAmountErrorEmptyViewGONEViewVISIBLE = ((viewModelAmountErrorEmpty) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x610L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.phoneError
                        viewModelPhoneError = viewModel.getPhoneError();
                    }
                    updateRegistration(4, viewModelPhoneError);


                    if (viewModelPhoneError != null) {
                        // read viewModel.phoneError.get()
                        viewModelPhoneErrorGet = viewModelPhoneError.get();
                    }
            }
            if ((dirtyFlags & 0x620L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.phone
                        viewModelPhone = viewModel.getPhone();
                    }
                    updateRegistration(5, viewModelPhone);


                    if (viewModelPhone != null) {
                        // read viewModel.phone.get()
                        viewModelPhoneGet = viewModelPhone.get();
                    }
            }
            if ((dirtyFlags & 0x640L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(6, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x640L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x610L) != 0) {
            // api target 1

            fit.lerchek.ui.util.BindingAdaptersKt.updateElevationOutline(this.constraintLayoutNewPwd, viewModelPhoneErrorGet);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.errorMessagePhone, viewModelPhoneErrorGet);
        }
        if ((dirtyFlags & 0x608L) != 0) {
            // api target 1

            fit.lerchek.ui.util.BindingAdaptersKt.updateElevationOutline(this.constraintLayoutSum, viewModelAmountErrorGet);
            this.errorMessageAmount.setVisibility(viewModelAmountErrorEmptyViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.errorMessageAmount, viewModelAmountErrorGet);
            this.hintMessageAmount.setVisibility(viewModelAmountErrorEmptyViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x480L) != 0) {
            // api target 1

            this.mboundView91.setCallback(callback);
        }
        if ((dirtyFlags & 0x604L) != 0) {
            // api target 1

            this.mboundView91.setShowSuccessHint(viewModelSuccessWithdrawGet);
        }
        if ((dirtyFlags & 0x601L) != 0) {
            // api target 1

            this.mboundView91.setShowErrorHint(viewModelErrorWithdrawGet);
        }
        if ((dirtyFlags & 0x500L) != 0) {
            // api target 1

            this.mboundView91.setPrice(price);
        }
        if ((dirtyFlags & 0x620L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.phone, viewModelPhoneGet);
        }
        if ((dirtyFlags & 0x400L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.phone, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, phoneandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.sum, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, sumandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x640L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x602L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.sum, viewModelAmountGet);
        }
        executeBindingsOn(mboundView91);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.errorWithdraw
        flag 1 (0x2L): viewModel.amount
        flag 2 (0x3L): viewModel.successWithdraw
        flag 3 (0x4L): viewModel.amountError
        flag 4 (0x5L): viewModel.phoneError
        flag 5 (0x6L): viewModel.phone
        flag 6 (0x7L): viewModel.isProgress()
        flag 7 (0x8L): callback
        flag 8 (0x9L): price
        flag 9 (0xaL): viewModel
        flag 10 (0xbL): null
        flag 11 (0xcL): viewModel.amountError.get().empty ? View.VISIBLE : View.GONE
        flag 12 (0xdL): viewModel.amountError.get().empty ? View.VISIBLE : View.GONE
        flag 13 (0xeL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 14 (0xfL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 15 (0x10L): viewModel.amountError.get().empty ? View.GONE : View.VISIBLE
        flag 16 (0x11L): viewModel.amountError.get().empty ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}