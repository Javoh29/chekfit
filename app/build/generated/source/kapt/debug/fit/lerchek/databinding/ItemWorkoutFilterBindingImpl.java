package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemWorkoutFilterBindingImpl extends ItemWorkoutFilterBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(20);
        sIncludes.setIncludes(8, 
            new String[] {"item_workout_day", "item_workout_day", "item_workout_day", "item_workout_day", "item_workout_day", "item_workout_day", "item_workout_day"},
            new int[] {10, 11, 12, 13, 14, 15, 16},
            new int[] {fit.lerchek.R.layout.item_workout_day,
                fit.lerchek.R.layout.item_workout_day,
                fit.lerchek.R.layout.item_workout_day,
                fit.lerchek.R.layout.item_workout_day,
                fit.lerchek.R.layout.item_workout_day,
                fit.lerchek.R.layout.item_workout_day,
                fit.lerchek.R.layout.item_workout_day});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.iconSelect, 17);
        sViewsWithIds.put(R.id.imgArrow, 18);
        sViewsWithIds.put(R.id.llText, 19);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView6;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView7;
    @NonNull
    private final android.widget.LinearLayout mboundView8;
    @Nullable
    private final fit.lerchek.databinding.ItemWorkoutDayBinding mboundView81;
    @Nullable
    private final fit.lerchek.databinding.ItemWorkoutDayBinding mboundView82;
    @Nullable
    private final fit.lerchek.databinding.ItemWorkoutDayBinding mboundView83;
    @Nullable
    private final fit.lerchek.databinding.ItemWorkoutDayBinding mboundView84;
    @Nullable
    private final fit.lerchek.databinding.ItemWorkoutDayBinding mboundView85;
    @Nullable
    private final fit.lerchek.databinding.ItemWorkoutDayBinding mboundView86;
    @Nullable
    private final fit.lerchek.databinding.ItemWorkoutDayBinding mboundView87;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemWorkoutFilterBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds));
    }
    private ItemWorkoutFilterBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[3]
            , (androidx.cardview.widget.CardView) bindings[4]
            , (android.widget.LinearLayout) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[17]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[18]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (android.widget.LinearLayout) bindings[19]
            );
        this.btnFAQ.setTag(null);
        this.btnSelectLevel.setTag(null);
        this.btnSelectWeek.setTag(null);
        this.imgLevel.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (androidx.appcompat.widget.AppCompatTextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (android.widget.LinearLayout) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView81 = (fit.lerchek.databinding.ItemWorkoutDayBinding) bindings[10];
        setContainedBinding(this.mboundView81);
        this.mboundView82 = (fit.lerchek.databinding.ItemWorkoutDayBinding) bindings[11];
        setContainedBinding(this.mboundView82);
        this.mboundView83 = (fit.lerchek.databinding.ItemWorkoutDayBinding) bindings[12];
        setContainedBinding(this.mboundView83);
        this.mboundView84 = (fit.lerchek.databinding.ItemWorkoutDayBinding) bindings[13];
        setContainedBinding(this.mboundView84);
        this.mboundView85 = (fit.lerchek.databinding.ItemWorkoutDayBinding) bindings[14];
        setContainedBinding(this.mboundView85);
        this.mboundView86 = (fit.lerchek.databinding.ItemWorkoutDayBinding) bindings[15];
        setContainedBinding(this.mboundView86);
        this.mboundView87 = (fit.lerchek.databinding.ItemWorkoutDayBinding) bindings[16];
        setContainedBinding(this.mboundView87);
        this.mboundView9 = (androidx.appcompat.widget.AppCompatTextView) bindings[9];
        this.mboundView9.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new fit.lerchek.generated.callback.OnClickListener(this, 2);
        mCallback3 = new fit.lerchek.generated.callback.OnClickListener(this, 3);
        mCallback1 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        mboundView81.invalidateAll();
        mboundView82.invalidateAll();
        mboundView83.invalidateAll();
        mboundView84.invalidateAll();
        mboundView85.invalidateAll();
        mboundView86.invalidateAll();
        mboundView87.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (mboundView81.hasPendingBindings()) {
            return true;
        }
        if (mboundView82.hasPendingBindings()) {
            return true;
        }
        if (mboundView83.hasPendingBindings()) {
            return true;
        }
        if (mboundView84.hasPendingBindings()) {
            return true;
        }
        if (mboundView85.hasPendingBindings()) {
            return true;
        }
        if (mboundView86.hasPendingBindings()) {
            return true;
        }
        if (mboundView87.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.WorkoutFilterUIModel) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.workout.WorkoutCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.WorkoutFilterUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.workout.WorkoutCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        mboundView81.setLifecycleOwner(lifecycleOwner);
        mboundView82.setLifecycleOwner(lifecycleOwner);
        mboundView83.setLifecycleOwner(lifecycleOwner);
        mboundView84.setLifecycleOwner(lifecycleOwner);
        mboundView85.setLifecycleOwner(lifecycleOwner);
        mboundView86.setLifecycleOwner(lifecycleOwner);
        mboundView87.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.WorkoutFilterUIModel model = mModel;
        boolean modelIsMale = false;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt3 = null;
        java.util.List<fit.lerchek.data.domain.uimodel.marathon.DayUIModel> modelDays = null;
        fit.lerchek.data.domain.uimodel.marathon.PlanUIModel modelLevel = null;
        java.lang.String modelSelectedWeekName = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt2 = null;
        java.lang.String modelTodayName = null;
        fit.lerchek.data.domain.uimodel.marathon.WeekUIModel modelSelectedWeek = null;
        fit.lerchek.ui.feature.marathon.workout.WorkoutCallback callback = mCallback;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt6 = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt1 = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt5 = null;
        java.lang.String modelLevelName = null;
        java.lang.String modelLevelDescription = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt0 = null;
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel modelDaysGetInt4 = null;
        android.graphics.drawable.Drawable modelIsMaleImgLevelAndroidDrawableIcProfileMaleImgLevelAndroidDrawableIcProfileFemale = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (model != null) {
                    // read model.isMale
                    modelIsMale = model.isMale();
                    // read model.days
                    modelDays = model.getDays();
                    // read model.level
                    modelLevel = model.getLevel();
                    // read model.todayName
                    modelTodayName = model.getTodayName();
                    // read model.selectedWeek
                    modelSelectedWeek = model.getSelectedWeek();
                }
            if((dirtyFlags & 0x5L) != 0) {
                if(modelIsMale) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                modelIsMaleImgLevelAndroidDrawableIcProfileMaleImgLevelAndroidDrawableIcProfileFemale = ((modelIsMale) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgLevel.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgLevel.getContext(), R.drawable.ic_profile_female)));
                if (modelDays != null) {
                    // read model.days.get(3)
                    modelDaysGetInt3 = modelDays.get(3);
                    // read model.days.get(2)
                    modelDaysGetInt2 = modelDays.get(2);
                    // read model.days.get(6)
                    modelDaysGetInt6 = modelDays.get(6);
                    // read model.days.get(1)
                    modelDaysGetInt1 = modelDays.get(1);
                    // read model.days.get(5)
                    modelDaysGetInt5 = modelDays.get(5);
                    // read model.days.get(0)
                    modelDaysGetInt0 = modelDays.get(0);
                    // read model.days.get(4)
                    modelDaysGetInt4 = modelDays.get(4);
                }
                if (modelLevel != null) {
                    // read model.level.name
                    modelLevelName = modelLevel.getName();
                    // read model.level.description
                    modelLevelDescription = modelLevel.getDescription();
                }
                if (modelSelectedWeek != null) {
                    // read model.selectedWeek.name
                    modelSelectedWeekName = modelSelectedWeek.getName();
                }
        }
        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.btnFAQ.setOnClickListener(mCallback2);
            this.btnSelectLevel.setOnClickListener(mCallback3);
            this.btnSelectWeek.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.imgLevel, modelIsMaleImgLevelAndroidDrawableIcProfileMaleImgLevelAndroidDrawableIcProfileFemale);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, modelSelectedWeekName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, modelLevelName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, modelLevelDescription);
            this.mboundView81.setModel(modelDaysGetInt0);
            this.mboundView82.setModel(modelDaysGetInt1);
            this.mboundView83.setModel(modelDaysGetInt2);
            this.mboundView84.setModel(modelDaysGetInt3);
            this.mboundView85.setModel(modelDaysGetInt4);
            this.mboundView86.setModel(modelDaysGetInt5);
            this.mboundView87.setModel(modelDaysGetInt6);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView9, modelTodayName);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            this.mboundView81.setCallback(callback);
            this.mboundView82.setCallback(callback);
            this.mboundView83.setCallback(callback);
            this.mboundView84.setCallback(callback);
            this.mboundView85.setCallback(callback);
            this.mboundView86.setCallback(callback);
            this.mboundView87.setCallback(callback);
        }
        executeBindingsOn(mboundView81);
        executeBindingsOn(mboundView82);
        executeBindingsOn(mboundView83);
        executeBindingsOn(mboundView84);
        executeBindingsOn(mboundView85);
        executeBindingsOn(mboundView86);
        executeBindingsOn(mboundView87);
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // callback
                fit.lerchek.ui.feature.marathon.workout.WorkoutCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.onFAQClick();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // model
                fit.lerchek.data.domain.uimodel.marathon.WorkoutFilterUIModel model = mModel;
                // model != null
                boolean modelJavaLangObjectNull = false;
                // callback
                fit.lerchek.ui.feature.marathon.workout.WorkoutCallback callback = mCallback;
                // model.levels
                java.util.List<fit.lerchek.data.domain.uimodel.marathon.PlanUIModel> modelLevels = null;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {



                    modelJavaLangObjectNull = (model) != (null);
                    if (modelJavaLangObjectNull) {


                        modelLevels = model.getLevels();

                        callback.onSelectLevelClick(modelLevels);
                    }
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // model
                fit.lerchek.data.domain.uimodel.marathon.WorkoutFilterUIModel model = mModel;
                // model != null
                boolean modelJavaLangObjectNull = false;
                // model.weeks
                java.util.List<fit.lerchek.data.domain.uimodel.marathon.WeekUIModel> modelWeeks = null;
                // callback
                fit.lerchek.ui.feature.marathon.workout.WorkoutCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {



                    modelJavaLangObjectNull = (model) != (null);
                    if (modelJavaLangObjectNull) {


                        modelWeeks = model.getWeeks();

                        callback.onSelectWeekClick(modelWeeks);
                    }
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
        flag 3 (0x4L): model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 4 (0x5L): model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
    flag mapping end*/
    //end
}