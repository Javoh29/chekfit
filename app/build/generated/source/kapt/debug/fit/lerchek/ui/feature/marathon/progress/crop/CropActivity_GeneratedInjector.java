package fit.lerchek.ui.feature.marathon.progress.crop;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = CropActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface CropActivity_GeneratedInjector {
  void injectCropActivity(CropActivity cropActivity);
}
