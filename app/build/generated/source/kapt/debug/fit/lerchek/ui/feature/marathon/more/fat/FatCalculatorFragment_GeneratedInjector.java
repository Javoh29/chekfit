package fit.lerchek.ui.feature.marathon.more.fat;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = FatCalculatorFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface FatCalculatorFragment_GeneratedInjector {
  void injectFatCalculatorFragment(FatCalculatorFragment fatCalculatorFragment);
}
