package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentUserPersonalInfoBindingImpl extends FragmentUserPersonalInfoBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.appCompatTextView3, 16);
        sViewsWithIds.put(R.id.message, 17);
        sViewsWithIds.put(R.id.save, 18);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView14;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener countOfChildrenandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().childrenCount.get()
            //         is viewModel.personalDetails.get().childrenCount.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(countOfChildren);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get().childrenCount
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetChildrenCount = null;
            // viewModel.personalDetails.get().childrenCount
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsChildrenCount = null;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel.personalDetails.get().childrenCount.get()
            java.lang.String viewModelPersonalDetailsChildrenCountGet = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().childrenCount != null
            boolean viewModelPersonalDetailsGetChildrenCountJavaLangObjectNull = false;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetChildrenCount = viewModelPersonalDetailsGet.getChildrenCount();

                        viewModelPersonalDetailsGetChildrenCountJavaLangObjectNull = (viewModelPersonalDetailsGetChildrenCount) != (null);
                        if (viewModelPersonalDetailsGetChildrenCountJavaLangObjectNull) {




                            viewModelPersonalDetailsGetChildrenCount.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener inputCityandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().city.get()
            //         is viewModel.personalDetails.get().city.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputCity);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get().city.get()
            java.lang.String viewModelPersonalDetailsCityGet = null;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails.get().city
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsCity = null;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().city != null
            boolean viewModelPersonalDetailsGetCityJavaLangObjectNull = false;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;
            // viewModel.personalDetails.get().city
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetCity = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetCity = viewModelPersonalDetailsGet.getCity();

                        viewModelPersonalDetailsGetCityJavaLangObjectNull = (viewModelPersonalDetailsGetCity) != (null);
                        if (viewModelPersonalDetailsGetCityJavaLangObjectNull) {




                            viewModelPersonalDetailsGetCity.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener inputInstagramandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().instagram.get()
            //         is viewModel.personalDetails.get().instagram.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputInstagram);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel.personalDetails.get().instagram
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetInstagram = null;
            // viewModel.personalDetails.get().instagram != null
            boolean viewModelPersonalDetailsGetInstagramJavaLangObjectNull = false;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().instagram
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsInstagram = null;
            // viewModel.personalDetails.get().instagram.get()
            java.lang.String viewModelPersonalDetailsInstagramGet = null;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetInstagram = viewModelPersonalDetailsGet.getInstagram();

                        viewModelPersonalDetailsGetInstagramJavaLangObjectNull = (viewModelPersonalDetailsGetInstagram) != (null);
                        if (viewModelPersonalDetailsGetInstagramJavaLangObjectNull) {




                            viewModelPersonalDetailsGetInstagram.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener inputNamesandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().name.get()
            //         is viewModel.personalDetails.get().name.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputNames);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get().name != null
            boolean viewModelPersonalDetailsGetNameJavaLangObjectNull = false;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel.personalDetails.get().name.get()
            java.lang.String viewModelPersonalDetailsNameGet = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().name
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetName = null;
            // viewModel.personalDetails.get().name
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsName = null;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetName = viewModelPersonalDetailsGet.getName();

                        viewModelPersonalDetailsGetNameJavaLangObjectNull = (viewModelPersonalDetailsGetName) != (null);
                        if (viewModelPersonalDetailsGetNameJavaLangObjectNull) {




                            viewModelPersonalDetailsGetName.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener inputPatronNameandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().middleName.get()
            //         is viewModel.personalDetails.get().middleName.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputPatronName);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel.personalDetails.get().middleName
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsMiddleName = null;
            // viewModel.personalDetails.get().middleName != null
            boolean viewModelPersonalDetailsGetMiddleNameJavaLangObjectNull = false;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().middleName.get()
            java.lang.String viewModelPersonalDetailsMiddleNameGet = null;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;
            // viewModel.personalDetails.get().middleName
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetMiddleName = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetMiddleName = viewModelPersonalDetailsGet.getMiddleName();

                        viewModelPersonalDetailsGetMiddleNameJavaLangObjectNull = (viewModelPersonalDetailsGetMiddleName) != (null);
                        if (viewModelPersonalDetailsGetMiddleNameJavaLangObjectNull) {




                            viewModelPersonalDetailsGetMiddleName.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener inputPhoneNumberandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().phone.get()
            //         is viewModel.personalDetails.get().phone.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputPhoneNumber);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel.personalDetails.get().phone != null
            boolean viewModelPersonalDetailsGetPhoneJavaLangObjectNull = false;
            // viewModel.personalDetails.get().phone.get()
            java.lang.String viewModelPersonalDetailsPhoneGet = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().phone
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetPhone = null;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;
            // viewModel.personalDetails.get().phone
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsPhone = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetPhone = viewModelPersonalDetailsGet.getPhone();

                        viewModelPersonalDetailsGetPhoneJavaLangObjectNull = (viewModelPersonalDetailsGetPhone) != (null);
                        if (viewModelPersonalDetailsGetPhoneJavaLangObjectNull) {




                            viewModelPersonalDetailsGetPhone.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener inputSecondNameandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().lastName.get()
            //         is viewModel.personalDetails.get().lastName.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputSecondName);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get().lastName.get()
            java.lang.String viewModelPersonalDetailsLastNameGet = null;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().lastName != null
            boolean viewModelPersonalDetailsGetLastNameJavaLangObjectNull = false;
            // viewModel.personalDetails.get().lastName
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsLastName = null;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel.personalDetails.get().lastName
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetLastName = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetLastName = viewModelPersonalDetailsGet.getLastName();

                        viewModelPersonalDetailsGetLastNameJavaLangObjectNull = (viewModelPersonalDetailsGetLastName) != (null);
                        if (viewModelPersonalDetailsGetLastNameJavaLangObjectNull) {




                            viewModelPersonalDetailsGetLastName.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener inputTelegramandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalDetails.get().telegram.get()
            //         is viewModel.personalDetails.get().telegram.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputTelegram);
            // localize variables for thread safety
            // viewModel.personalDetails
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
            // viewModel.personalDetails.get().telegram
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsTelegram = null;
            // viewModel.personalDetails.get().telegram != null
            boolean viewModelPersonalDetailsGetTelegramJavaLangObjectNull = false;
            // viewModel.personalDetails.get() != null
            boolean viewModelPersonalDetailsGetJavaLangObjectNull = false;
            // viewModel.personalDetails != null
            boolean viewModelPersonalDetailsJavaLangObjectNull = false;
            // viewModel.personalDetails.get().telegram.get()
            java.lang.String viewModelPersonalDetailsTelegramGet = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalDetails.get().telegram
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsGetTelegram = null;
            // viewModel.personalDetails.get()
            fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalDetails = viewModel.getPersonalDetails();

                viewModelPersonalDetailsJavaLangObjectNull = (viewModelPersonalDetails) != (null);
                if (viewModelPersonalDetailsJavaLangObjectNull) {


                    viewModelPersonalDetailsGet = viewModelPersonalDetails.get();

                    viewModelPersonalDetailsGetJavaLangObjectNull = (viewModelPersonalDetailsGet) != (null);
                    if (viewModelPersonalDetailsGetJavaLangObjectNull) {


                        viewModelPersonalDetailsGetTelegram = viewModelPersonalDetailsGet.getTelegram();

                        viewModelPersonalDetailsGetTelegramJavaLangObjectNull = (viewModelPersonalDetailsGetTelegram) != (null);
                        if (viewModelPersonalDetailsGetTelegramJavaLangObjectNull) {




                            viewModelPersonalDetailsGetTelegram.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };

    public FragmentUserPersonalInfoBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private FragmentUserPersonalInfoBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 12
            , (androidx.appcompat.widget.AppCompatTextView) bindings[16]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[9]
            , (android.widget.RadioGroup) bindings[6]
            , (androidx.appcompat.widget.AppCompatRadioButton) bindings[7]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[11]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[12]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[2]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[4]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[10]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[3]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[13]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[17]
            , (android.widget.FrameLayout) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[18]
            , (androidx.appcompat.widget.AppCompatRadioButton) bindings[8]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[5]
            );
        this.countOfChildren.setTag(null);
        this.familyState.setTag(null);
        this.first.setTag(null);
        this.inputCity.setTag(null);
        this.inputEmail.setTag(null);
        this.inputInstagram.setTag(null);
        this.inputNames.setTag(null);
        this.inputPatronName.setTag(null);
        this.inputPhoneNumber.setTag(null);
        this.inputSecondName.setTag(null);
        this.inputTelegram.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView14 = (bindings[15] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[15]) : null;
        this.progressBlock.setTag(null);
        this.second.setTag(null);
        this.selectDateOfBirth.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2000L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x1000L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelPersonalDetailsDateOfBirth((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelPersonalDetailsPhone((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelPersonalDetailsCity((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelPersonalDetailsMiddleName((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelPersonalDetailsInstagram((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeViewModelPersonalDetailsName((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewModelPersonalDetails((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel>) object, fieldId);
            case 7 :
                return onChangeViewModelPersonalDetailsTelegram((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 8 :
                return onChangeViewModelPersonalDetailsLastName((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 9 :
                return onChangeViewModelPersonalDetailsEmail((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 10 :
                return onChangeViewModelPersonalDetailsChildrenCount((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 11 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsDateOfBirth(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsDateOfBirth, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsPhone(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsPhone, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsCity(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsCity, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsMiddleName(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsMiddleName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsInstagram(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsInstagram, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsName(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetails(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> ViewModelPersonalDetails, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsTelegram(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsTelegram, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsLastName(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsLastName, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsEmail(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsEmail, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalDetailsChildrenCount(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalDetailsChildrenCount, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x400L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x800L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsDateOfBirth = null;
        java.lang.String viewModelPersonalDetailsChildrenCountGet = null;
        java.lang.String viewModelPersonalDetailsEmailGet = null;
        java.lang.String viewModelPersonalDetailsMiddleNameGet = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsPhone = null;
        java.lang.String viewModelPersonalDetailsLastNameGet = null;
        int viewModelPersonalDetailsSecondStateId = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsCity = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsMiddleName = null;
        java.lang.String viewModelPersonalDetailsPhoneGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsInstagram = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsName = null;
        boolean viewModelIsProgressGet = false;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel> viewModelPersonalDetails = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsTelegram = null;
        java.lang.String viewModelPersonalDetailsCityGet = null;
        java.lang.String viewModelPersonalDetailsFirstStateForFamily = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsLastName = null;
        int viewModelPersonalDetailsCheckedButton = 0;
        java.lang.String viewModelPersonalDetailsDateOfBirthGet = null;
        java.lang.String viewModelPersonalDetailsInstagramGet = null;
        fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel viewModelPersonalDetailsGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsEmail = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalDetailsChildrenCount = null;
        int viewModelPersonalDetailsFirstStateId = 0;
        java.lang.String viewModelPersonalDetailsNameGet = null;
        java.lang.String viewModelPersonalDetailsTelegramGet = null;
        java.lang.String viewModelPersonalDetailsSecondStateForFamily = null;
        fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x3fffL) != 0) {


            if ((dirtyFlags & 0x37ffL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.personalDetails
                        viewModelPersonalDetails = viewModel.getPersonalDetails();
                    }
                    updateRegistration(6, viewModelPersonalDetails);


                    if (viewModelPersonalDetails != null) {
                        // read viewModel.personalDetails.get()
                        viewModelPersonalDetailsGet = viewModelPersonalDetails.get();
                    }

                if ((dirtyFlags & 0x3041L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().dateOfBirth
                            viewModelPersonalDetailsDateOfBirth = viewModelPersonalDetailsGet.getDateOfBirth();
                        }
                        updateRegistration(0, viewModelPersonalDetailsDateOfBirth);


                        if (viewModelPersonalDetailsDateOfBirth != null) {
                            // read viewModel.personalDetails.get().dateOfBirth.get()
                            viewModelPersonalDetailsDateOfBirthGet = viewModelPersonalDetailsDateOfBirth.get();
                        }
                }
                if ((dirtyFlags & 0x3042L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().phone
                            viewModelPersonalDetailsPhone = viewModelPersonalDetailsGet.getPhone();
                        }
                        updateRegistration(1, viewModelPersonalDetailsPhone);


                        if (viewModelPersonalDetailsPhone != null) {
                            // read viewModel.personalDetails.get().phone.get()
                            viewModelPersonalDetailsPhoneGet = viewModelPersonalDetailsPhone.get();
                        }
                }
                if ((dirtyFlags & 0x3040L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().secondStateId()
                            viewModelPersonalDetailsSecondStateId = viewModelPersonalDetailsGet.secondStateId();
                            // read viewModel.personalDetails.get().firstStateForFamily()
                            viewModelPersonalDetailsFirstStateForFamily = viewModelPersonalDetailsGet.firstStateForFamily();
                            // read viewModel.personalDetails.get().checkedButton()
                            viewModelPersonalDetailsCheckedButton = viewModelPersonalDetailsGet.checkedButton();
                            // read viewModel.personalDetails.get().firstStateId()
                            viewModelPersonalDetailsFirstStateId = viewModelPersonalDetailsGet.firstStateId();
                            // read viewModel.personalDetails.get().secondStateForFamily()
                            viewModelPersonalDetailsSecondStateForFamily = viewModelPersonalDetailsGet.secondStateForFamily();
                        }
                }
                if ((dirtyFlags & 0x3044L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().city
                            viewModelPersonalDetailsCity = viewModelPersonalDetailsGet.getCity();
                        }
                        updateRegistration(2, viewModelPersonalDetailsCity);


                        if (viewModelPersonalDetailsCity != null) {
                            // read viewModel.personalDetails.get().city.get()
                            viewModelPersonalDetailsCityGet = viewModelPersonalDetailsCity.get();
                        }
                }
                if ((dirtyFlags & 0x3048L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().middleName
                            viewModelPersonalDetailsMiddleName = viewModelPersonalDetailsGet.getMiddleName();
                        }
                        updateRegistration(3, viewModelPersonalDetailsMiddleName);


                        if (viewModelPersonalDetailsMiddleName != null) {
                            // read viewModel.personalDetails.get().middleName.get()
                            viewModelPersonalDetailsMiddleNameGet = viewModelPersonalDetailsMiddleName.get();
                        }
                }
                if ((dirtyFlags & 0x3050L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().instagram
                            viewModelPersonalDetailsInstagram = viewModelPersonalDetailsGet.getInstagram();
                        }
                        updateRegistration(4, viewModelPersonalDetailsInstagram);


                        if (viewModelPersonalDetailsInstagram != null) {
                            // read viewModel.personalDetails.get().instagram.get()
                            viewModelPersonalDetailsInstagramGet = viewModelPersonalDetailsInstagram.get();
                        }
                }
                if ((dirtyFlags & 0x3060L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().name
                            viewModelPersonalDetailsName = viewModelPersonalDetailsGet.getName();
                        }
                        updateRegistration(5, viewModelPersonalDetailsName);


                        if (viewModelPersonalDetailsName != null) {
                            // read viewModel.personalDetails.get().name.get()
                            viewModelPersonalDetailsNameGet = viewModelPersonalDetailsName.get();
                        }
                }
                if ((dirtyFlags & 0x30c0L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().telegram
                            viewModelPersonalDetailsTelegram = viewModelPersonalDetailsGet.getTelegram();
                        }
                        updateRegistration(7, viewModelPersonalDetailsTelegram);


                        if (viewModelPersonalDetailsTelegram != null) {
                            // read viewModel.personalDetails.get().telegram.get()
                            viewModelPersonalDetailsTelegramGet = viewModelPersonalDetailsTelegram.get();
                        }
                }
                if ((dirtyFlags & 0x3140L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().lastName
                            viewModelPersonalDetailsLastName = viewModelPersonalDetailsGet.getLastName();
                        }
                        updateRegistration(8, viewModelPersonalDetailsLastName);


                        if (viewModelPersonalDetailsLastName != null) {
                            // read viewModel.personalDetails.get().lastName.get()
                            viewModelPersonalDetailsLastNameGet = viewModelPersonalDetailsLastName.get();
                        }
                }
                if ((dirtyFlags & 0x3240L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().email
                            viewModelPersonalDetailsEmail = viewModelPersonalDetailsGet.getEmail();
                        }
                        updateRegistration(9, viewModelPersonalDetailsEmail);


                        if (viewModelPersonalDetailsEmail != null) {
                            // read viewModel.personalDetails.get().email.get()
                            viewModelPersonalDetailsEmailGet = viewModelPersonalDetailsEmail.get();
                        }
                }
                if ((dirtyFlags & 0x3440L) != 0) {

                        if (viewModelPersonalDetailsGet != null) {
                            // read viewModel.personalDetails.get().childrenCount
                            viewModelPersonalDetailsChildrenCount = viewModelPersonalDetailsGet.getChildrenCount();
                        }
                        updateRegistration(10, viewModelPersonalDetailsChildrenCount);


                        if (viewModelPersonalDetailsChildrenCount != null) {
                            // read viewModel.personalDetails.get().childrenCount.get()
                            viewModelPersonalDetailsChildrenCountGet = viewModelPersonalDetailsChildrenCount.get();
                        }
                }
            }
            if ((dirtyFlags & 0x3800L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(11, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x3800L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x4000L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x3440L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.countOfChildren, viewModelPersonalDetailsChildrenCountGet);
        }
        if ((dirtyFlags & 0x2000L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.countOfChildren, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, countOfChildrenandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputCity, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputCityandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputInstagram, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputInstagramandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputNames, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputNamesandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputPatronName, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputPatronNameandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputPhoneNumber, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputPhoneNumberandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputSecondName, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputSecondNameandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputTelegram, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputTelegramandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x3040L) != 0) {
            // api target 1

            androidx.databinding.adapters.RadioGroupBindingAdapter.setCheckedButton(this.familyState, viewModelPersonalDetailsCheckedButton);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.first, viewModelPersonalDetailsFirstStateForFamily);
            this.first.setTag(viewModelPersonalDetailsFirstStateId);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.second, viewModelPersonalDetailsSecondStateForFamily);
            this.second.setTag(viewModelPersonalDetailsSecondStateId);
        }
        if ((dirtyFlags & 0x3044L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputCity, viewModelPersonalDetailsCityGet);
        }
        if ((dirtyFlags & 0x3240L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputEmail, viewModelPersonalDetailsEmailGet);
        }
        if ((dirtyFlags & 0x3050L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputInstagram, viewModelPersonalDetailsInstagramGet);
        }
        if ((dirtyFlags & 0x3060L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputNames, viewModelPersonalDetailsNameGet);
        }
        if ((dirtyFlags & 0x3048L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputPatronName, viewModelPersonalDetailsMiddleNameGet);
        }
        if ((dirtyFlags & 0x3042L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputPhoneNumber, viewModelPersonalDetailsPhoneGet);
        }
        if ((dirtyFlags & 0x3140L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputSecondName, viewModelPersonalDetailsLastNameGet);
        }
        if ((dirtyFlags & 0x30c0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputTelegram, viewModelPersonalDetailsTelegramGet);
        }
        if ((dirtyFlags & 0x3800L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x3041L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.selectDateOfBirth, viewModelPersonalDetailsDateOfBirthGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.personalDetails.get().dateOfBirth
        flag 1 (0x2L): viewModel.personalDetails.get().phone
        flag 2 (0x3L): viewModel.personalDetails.get().city
        flag 3 (0x4L): viewModel.personalDetails.get().middleName
        flag 4 (0x5L): viewModel.personalDetails.get().instagram
        flag 5 (0x6L): viewModel.personalDetails.get().name
        flag 6 (0x7L): viewModel.personalDetails
        flag 7 (0x8L): viewModel.personalDetails.get().telegram
        flag 8 (0x9L): viewModel.personalDetails.get().lastName
        flag 9 (0xaL): viewModel.personalDetails.get().email
        flag 10 (0xbL): viewModel.personalDetails.get().childrenCount
        flag 11 (0xcL): viewModel.isProgress()
        flag 12 (0xdL): viewModel
        flag 13 (0xeL): null
        flag 14 (0xfL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 15 (0x10L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}