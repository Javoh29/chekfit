package fit.lerchek.ui.feature.marathon.workout;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = WorkoutFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface WorkoutFragment_GeneratedInjector {
  void injectWorkoutFragment(WorkoutFragment workoutFragment);
}
