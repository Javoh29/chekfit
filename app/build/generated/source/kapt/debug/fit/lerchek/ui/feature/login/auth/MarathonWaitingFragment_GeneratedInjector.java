package fit.lerchek.ui.feature.login.auth;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = MarathonWaitingFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface MarathonWaitingFragment_GeneratedInjector {
  void injectMarathonWaitingFragment(MarathonWaitingFragment marathonWaitingFragment);
}
