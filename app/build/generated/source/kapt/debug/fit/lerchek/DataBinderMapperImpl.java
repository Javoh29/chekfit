package fit.lerchek;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import fit.lerchek.databinding.ActivityChatBindingImpl;
import fit.lerchek.databinding.ActivityCropBindingImpl;
import fit.lerchek.databinding.ActivityMarathonBindingImpl;
import fit.lerchek.databinding.FavoriteFragmentBindingImpl;
import fit.lerchek.databinding.FragmentActivityCalendarBindingImpl;
import fit.lerchek.databinding.FragmentAddEditMeasurementsBindingImpl;
import fit.lerchek.databinding.FragmentBmiCalculatorBindingImpl;
import fit.lerchek.databinding.FragmentCalorieCalculatorBindingImpl;
import fit.lerchek.databinding.FragmentCardioTestBindingImpl;
import fit.lerchek.databinding.FragmentDynamicContentBindingImpl;
import fit.lerchek.databinding.FragmentExtraRecipesBindingImpl;
import fit.lerchek.databinding.FragmentFaqBindingImpl;
import fit.lerchek.databinding.FragmentFatCalculatorBindingImpl;
import fit.lerchek.databinding.FragmentFoodBindingImpl;
import fit.lerchek.databinding.FragmentFoodIngredientsBindingImpl;
import fit.lerchek.databinding.FragmentForgotPwdBindingImpl;
import fit.lerchek.databinding.FragmentInfoDetailsBindingImpl;
import fit.lerchek.databinding.FragmentMakeMeasurementsBindingImpl;
import fit.lerchek.databinding.FragmentMarathonListBindingImpl;
import fit.lerchek.databinding.FragmentMeasurementsBindingImpl;
import fit.lerchek.databinding.FragmentMenuBindingImpl;
import fit.lerchek.databinding.FragmentMoreBindingImpl;
import fit.lerchek.databinding.FragmentProductsBindingImpl;
import fit.lerchek.databinding.FragmentProfileBindingImpl;
import fit.lerchek.databinding.FragmentProfileTestBindingImpl;
import fit.lerchek.databinding.FragmentProgressBindingImpl;
import fit.lerchek.databinding.FragmentRecipeDetailsBindingImpl;
import fit.lerchek.databinding.FragmentRecipeReplacementBindingImpl;
import fit.lerchek.databinding.FragmentReferralBindingImpl;
import fit.lerchek.databinding.FragmentReferralWithdrawBindingImpl;
import fit.lerchek.databinding.FragmentReportsBindingImpl;
import fit.lerchek.databinding.FragmentSetNewPwdBindingImpl;
import fit.lerchek.databinding.FragmentSignInBindingImpl;
import fit.lerchek.databinding.FragmentTodayBindingImpl;
import fit.lerchek.databinding.FragmentTrackerBindingImpl;
import fit.lerchek.databinding.FragmentTrackerSettingBindingImpl;
import fit.lerchek.databinding.FragmentUserPersonalBodyParametersBindingImpl;
import fit.lerchek.databinding.FragmentUserPersonalInfoBindingImpl;
import fit.lerchek.databinding.FragmentVotingBindingImpl;
import fit.lerchek.databinding.FragmentWorkoutBindingImpl;
import fit.lerchek.databinding.FragmentWorkoutDetailsBindingImpl;
import fit.lerchek.databinding.ItemCalorieItemBindingImpl;
import fit.lerchek.databinding.ItemCalorieOptionItemBindingImpl;
import fit.lerchek.databinding.ItemChatFooterBindingImpl;
import fit.lerchek.databinding.ItemChatMessageBindingImpl;
import fit.lerchek.databinding.ItemDynamicHeaderBindingImpl;
import fit.lerchek.databinding.ItemDynamicImageBindingImpl;
import fit.lerchek.databinding.ItemDynamicSpannedBindingImpl;
import fit.lerchek.databinding.ItemDynamicTitleBindingImpl;
import fit.lerchek.databinding.ItemDynamicVideoBindingImpl;
import fit.lerchek.databinding.ItemDynamicWhiteBlockBindingImpl;
import fit.lerchek.databinding.ItemExtraRecipeFilterBindingImpl;
import fit.lerchek.databinding.ItemFaqHeaderBindingImpl;
import fit.lerchek.databinding.ItemFaqItemBindingImpl;
import fit.lerchek.databinding.ItemFoodDayBindingImpl;
import fit.lerchek.databinding.ItemFoodEmptyBindingImpl;
import fit.lerchek.databinding.ItemFoodFilterBindingImpl;
import fit.lerchek.databinding.ItemFoodRecipeBindingImpl;
import fit.lerchek.databinding.ItemLineTrackerBindingImpl;
import fit.lerchek.databinding.ItemMarathonsListBindingImpl;
import fit.lerchek.databinding.ItemMarathonsListHeaderBindingImpl;
import fit.lerchek.databinding.ItemMenuButtonBindingImpl;
import fit.lerchek.databinding.ItemMenuLabelBindingImpl;
import fit.lerchek.databinding.ItemProductBindingImpl;
import fit.lerchek.databinding.ItemProductsFilterBindingImpl;
import fit.lerchek.databinding.ItemPurpleBtnBindingImpl;
import fit.lerchek.databinding.ItemRecipeIngredientBindingImpl;
import fit.lerchek.databinding.ItemRecipeIngredientHeaderBindingImpl;
import fit.lerchek.databinding.ItemRecipeReplacementFilterBindingImpl;
import fit.lerchek.databinding.ItemRefTransactionBindingImpl;
import fit.lerchek.databinding.ItemSettingSleepAddBindingImpl;
import fit.lerchek.databinding.ItemSettingSleepSetBindingImpl;
import fit.lerchek.databinding.ItemSettingStepsAddBindingImpl;
import fit.lerchek.databinding.ItemSettingStepsSetBindingImpl;
import fit.lerchek.databinding.ItemSettingTrackerBindingImpl;
import fit.lerchek.databinding.ItemSettingWaterSetBindingImpl;
import fit.lerchek.databinding.ItemSleepTrackerBindingImpl;
import fit.lerchek.databinding.ItemStepsTrackerBindingImpl;
import fit.lerchek.databinding.ItemSwitchFitBindingImpl;
import fit.lerchek.databinding.ItemTestFooterBindingImpl;
import fit.lerchek.databinding.ItemTestHeaderBindingImpl;
import fit.lerchek.databinding.ItemTestItemBindingImpl;
import fit.lerchek.databinding.ItemTestOptionItemBindingImpl;
import fit.lerchek.databinding.ItemTodayErrorBindingImpl;
import fit.lerchek.databinding.ItemTodayExpiredBindingImpl;
import fit.lerchek.databinding.ItemTodayRecipeBindingImpl;
import fit.lerchek.databinding.ItemTodayRecipesBindingImpl;
import fit.lerchek.databinding.ItemTodayRefBindingImpl;
import fit.lerchek.databinding.ItemTodaySaleBindingImpl;
import fit.lerchek.databinding.ItemTodaySaleOfferBindingImpl;
import fit.lerchek.databinding.ItemTodayTaskBindingImpl;
import fit.lerchek.databinding.ItemTodayTasksBindingImpl;
import fit.lerchek.databinding.ItemTodayTelegramBindingImpl;
import fit.lerchek.databinding.ItemTodayVoteBindingImpl;
import fit.lerchek.databinding.ItemTodayWaitingBindingImpl;
import fit.lerchek.databinding.ItemTodayWorkoutBindingImpl;
import fit.lerchek.databinding.ItemTodayWorkoutsBindingImpl;
import fit.lerchek.databinding.ItemVotingHeaderBindingImpl;
import fit.lerchek.databinding.ItemVotingImageBindingImpl;
import fit.lerchek.databinding.ItemVotingUserBindingImpl;
import fit.lerchek.databinding.ItemWaterTrackerBindingImpl;
import fit.lerchek.databinding.ItemWorkoutBindingImpl;
import fit.lerchek.databinding.ItemWorkoutDayBindingImpl;
import fit.lerchek.databinding.ItemWorkoutEmptyBindingImpl;
import fit.lerchek.databinding.ItemWorkoutFilterBindingImpl;
import fit.lerchek.databinding.LayoutProgressEmptyBindingImpl;
import fit.lerchek.databinding.LayoutProgressMakePhotosBindingImpl;
import fit.lerchek.databinding.LayoutProgressMakeVideoBindingImpl;
import fit.lerchek.databinding.LayoutProgressPhotoErrorBindingImpl;
import fit.lerchek.databinding.LayoutProgressPhotoSucceedBindingImpl;
import fit.lerchek.databinding.LayoutProgressPhotosBindingImpl;
import fit.lerchek.databinding.LayoutProgressResultBindingImpl;
import fit.lerchek.databinding.LayoutProgressVideoErrorBindingImpl;
import fit.lerchek.databinding.LayoutProgressWeekResultBindingImpl;
import fit.lerchek.databinding.LayoutProgressYourProgressBindingImpl;
import fit.lerchek.databinding.LayoutRefWithdrawBindingImpl;
import fit.lerchek.databinding.ViewInputBindingImpl;
import fit.lerchek.databinding.ViewRecipeDetailsBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYCHAT = 1;

  private static final int LAYOUT_ACTIVITYCROP = 2;

  private static final int LAYOUT_ACTIVITYMARATHON = 3;

  private static final int LAYOUT_FAVORITEFRAGMENT = 4;

  private static final int LAYOUT_FRAGMENTACTIVITYCALENDAR = 5;

  private static final int LAYOUT_FRAGMENTADDEDITMEASUREMENTS = 6;

  private static final int LAYOUT_FRAGMENTBMICALCULATOR = 7;

  private static final int LAYOUT_FRAGMENTCALORIECALCULATOR = 8;

  private static final int LAYOUT_FRAGMENTCARDIOTEST = 9;

  private static final int LAYOUT_FRAGMENTDYNAMICCONTENT = 10;

  private static final int LAYOUT_FRAGMENTEXTRARECIPES = 11;

  private static final int LAYOUT_FRAGMENTFAQ = 12;

  private static final int LAYOUT_FRAGMENTFATCALCULATOR = 13;

  private static final int LAYOUT_FRAGMENTFOOD = 14;

  private static final int LAYOUT_FRAGMENTFOODINGREDIENTS = 15;

  private static final int LAYOUT_FRAGMENTFORGOTPWD = 16;

  private static final int LAYOUT_FRAGMENTINFODETAILS = 17;

  private static final int LAYOUT_FRAGMENTMAKEMEASUREMENTS = 18;

  private static final int LAYOUT_FRAGMENTMARATHONLIST = 19;

  private static final int LAYOUT_FRAGMENTMEASUREMENTS = 20;

  private static final int LAYOUT_FRAGMENTMENU = 21;

  private static final int LAYOUT_FRAGMENTMORE = 22;

  private static final int LAYOUT_FRAGMENTPRODUCTS = 23;

  private static final int LAYOUT_FRAGMENTPROFILE = 24;

  private static final int LAYOUT_FRAGMENTPROFILETEST = 25;

  private static final int LAYOUT_FRAGMENTPROGRESS = 26;

  private static final int LAYOUT_FRAGMENTRECIPEDETAILS = 27;

  private static final int LAYOUT_FRAGMENTRECIPEREPLACEMENT = 28;

  private static final int LAYOUT_FRAGMENTREFERRAL = 29;

  private static final int LAYOUT_FRAGMENTREFERRALWITHDRAW = 30;

  private static final int LAYOUT_FRAGMENTREPORTS = 31;

  private static final int LAYOUT_FRAGMENTSETNEWPWD = 32;

  private static final int LAYOUT_FRAGMENTSIGNIN = 33;

  private static final int LAYOUT_FRAGMENTTODAY = 34;

  private static final int LAYOUT_FRAGMENTTRACKER = 35;

  private static final int LAYOUT_FRAGMENTTRACKERSETTING = 36;

  private static final int LAYOUT_FRAGMENTUSERPERSONALBODYPARAMETERS = 37;

  private static final int LAYOUT_FRAGMENTUSERPERSONALINFO = 38;

  private static final int LAYOUT_FRAGMENTVOTING = 39;

  private static final int LAYOUT_FRAGMENTWORKOUT = 40;

  private static final int LAYOUT_FRAGMENTWORKOUTDETAILS = 41;

  private static final int LAYOUT_ITEMCALORIEITEM = 42;

  private static final int LAYOUT_ITEMCALORIEOPTIONITEM = 43;

  private static final int LAYOUT_ITEMCHATFOOTER = 44;

  private static final int LAYOUT_ITEMCHATMESSAGE = 45;

  private static final int LAYOUT_ITEMDYNAMICHEADER = 46;

  private static final int LAYOUT_ITEMDYNAMICIMAGE = 47;

  private static final int LAYOUT_ITEMDYNAMICSPANNED = 48;

  private static final int LAYOUT_ITEMDYNAMICTITLE = 49;

  private static final int LAYOUT_ITEMDYNAMICVIDEO = 50;

  private static final int LAYOUT_ITEMDYNAMICWHITEBLOCK = 51;

  private static final int LAYOUT_ITEMEXTRARECIPEFILTER = 52;

  private static final int LAYOUT_ITEMFAQHEADER = 53;

  private static final int LAYOUT_ITEMFAQITEM = 54;

  private static final int LAYOUT_ITEMFOODDAY = 55;

  private static final int LAYOUT_ITEMFOODEMPTY = 56;

  private static final int LAYOUT_ITEMFOODFILTER = 57;

  private static final int LAYOUT_ITEMFOODRECIPE = 58;

  private static final int LAYOUT_ITEMLINETRACKER = 59;

  private static final int LAYOUT_ITEMMARATHONSLIST = 60;

  private static final int LAYOUT_ITEMMARATHONSLISTHEADER = 61;

  private static final int LAYOUT_ITEMMENUBUTTON = 62;

  private static final int LAYOUT_ITEMMENULABEL = 63;

  private static final int LAYOUT_ITEMPRODUCT = 64;

  private static final int LAYOUT_ITEMPRODUCTSFILTER = 65;

  private static final int LAYOUT_ITEMPURPLEBTN = 66;

  private static final int LAYOUT_ITEMRECIPEINGREDIENT = 67;

  private static final int LAYOUT_ITEMRECIPEINGREDIENTHEADER = 68;

  private static final int LAYOUT_ITEMRECIPEREPLACEMENTFILTER = 69;

  private static final int LAYOUT_ITEMREFTRANSACTION = 70;

  private static final int LAYOUT_ITEMSETTINGSLEEPADD = 71;

  private static final int LAYOUT_ITEMSETTINGSLEEPSET = 72;

  private static final int LAYOUT_ITEMSETTINGSTEPSADD = 73;

  private static final int LAYOUT_ITEMSETTINGSTEPSSET = 74;

  private static final int LAYOUT_ITEMSETTINGTRACKER = 75;

  private static final int LAYOUT_ITEMSETTINGWATERSET = 76;

  private static final int LAYOUT_ITEMSLEEPTRACKER = 77;

  private static final int LAYOUT_ITEMSTEPSTRACKER = 78;

  private static final int LAYOUT_ITEMSWITCHFIT = 79;

  private static final int LAYOUT_ITEMTESTFOOTER = 80;

  private static final int LAYOUT_ITEMTESTHEADER = 81;

  private static final int LAYOUT_ITEMTESTITEM = 82;

  private static final int LAYOUT_ITEMTESTOPTIONITEM = 83;

  private static final int LAYOUT_ITEMTODAYERROR = 84;

  private static final int LAYOUT_ITEMTODAYEXPIRED = 85;

  private static final int LAYOUT_ITEMTODAYRECIPE = 86;

  private static final int LAYOUT_ITEMTODAYRECIPES = 87;

  private static final int LAYOUT_ITEMTODAYREF = 88;

  private static final int LAYOUT_ITEMTODAYSALE = 89;

  private static final int LAYOUT_ITEMTODAYSALEOFFER = 90;

  private static final int LAYOUT_ITEMTODAYTASK = 91;

  private static final int LAYOUT_ITEMTODAYTASKS = 92;

  private static final int LAYOUT_ITEMTODAYTELEGRAM = 93;

  private static final int LAYOUT_ITEMTODAYVOTE = 94;

  private static final int LAYOUT_ITEMTODAYWAITING = 95;

  private static final int LAYOUT_ITEMTODAYWORKOUT = 96;

  private static final int LAYOUT_ITEMTODAYWORKOUTS = 97;

  private static final int LAYOUT_ITEMVOTINGHEADER = 98;

  private static final int LAYOUT_ITEMVOTINGIMAGE = 99;

  private static final int LAYOUT_ITEMVOTINGUSER = 100;

  private static final int LAYOUT_ITEMWATERTRACKER = 101;

  private static final int LAYOUT_ITEMWORKOUT = 102;

  private static final int LAYOUT_ITEMWORKOUTDAY = 103;

  private static final int LAYOUT_ITEMWORKOUTEMPTY = 104;

  private static final int LAYOUT_ITEMWORKOUTFILTER = 105;

  private static final int LAYOUT_LAYOUTPROGRESSEMPTY = 106;

  private static final int LAYOUT_LAYOUTPROGRESSMAKEPHOTOS = 107;

  private static final int LAYOUT_LAYOUTPROGRESSMAKEVIDEO = 108;

  private static final int LAYOUT_LAYOUTPROGRESSPHOTOERROR = 109;

  private static final int LAYOUT_LAYOUTPROGRESSPHOTOSUCCEED = 110;

  private static final int LAYOUT_LAYOUTPROGRESSPHOTOS = 111;

  private static final int LAYOUT_LAYOUTPROGRESSRESULT = 112;

  private static final int LAYOUT_LAYOUTPROGRESSVIDEOERROR = 113;

  private static final int LAYOUT_LAYOUTPROGRESSWEEKRESULT = 114;

  private static final int LAYOUT_LAYOUTPROGRESSYOURPROGRESS = 115;

  private static final int LAYOUT_LAYOUTREFWITHDRAW = 116;

  private static final int LAYOUT_VIEWINPUT = 117;

  private static final int LAYOUT_VIEWRECIPEDETAILS = 118;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(118);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.activity_chat, LAYOUT_ACTIVITYCHAT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.activity_crop, LAYOUT_ACTIVITYCROP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.activity_marathon, LAYOUT_ACTIVITYMARATHON);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.favorite_fragment, LAYOUT_FAVORITEFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_activity_calendar, LAYOUT_FRAGMENTACTIVITYCALENDAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_add_edit_measurements, LAYOUT_FRAGMENTADDEDITMEASUREMENTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_bmi_calculator, LAYOUT_FRAGMENTBMICALCULATOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_calorie_calculator, LAYOUT_FRAGMENTCALORIECALCULATOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_cardio_test, LAYOUT_FRAGMENTCARDIOTEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_dynamic_content, LAYOUT_FRAGMENTDYNAMICCONTENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_extra_recipes, LAYOUT_FRAGMENTEXTRARECIPES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_faq, LAYOUT_FRAGMENTFAQ);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_fat_calculator, LAYOUT_FRAGMENTFATCALCULATOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_food, LAYOUT_FRAGMENTFOOD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_food_ingredients, LAYOUT_FRAGMENTFOODINGREDIENTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_forgot_pwd, LAYOUT_FRAGMENTFORGOTPWD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_info_details, LAYOUT_FRAGMENTINFODETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_make_measurements, LAYOUT_FRAGMENTMAKEMEASUREMENTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_marathon_list, LAYOUT_FRAGMENTMARATHONLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_measurements, LAYOUT_FRAGMENTMEASUREMENTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_menu, LAYOUT_FRAGMENTMENU);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_more, LAYOUT_FRAGMENTMORE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_products, LAYOUT_FRAGMENTPRODUCTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_profile_test, LAYOUT_FRAGMENTPROFILETEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_progress, LAYOUT_FRAGMENTPROGRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_recipe_details, LAYOUT_FRAGMENTRECIPEDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_recipe_replacement, LAYOUT_FRAGMENTRECIPEREPLACEMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_referral, LAYOUT_FRAGMENTREFERRAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_referral_withdraw, LAYOUT_FRAGMENTREFERRALWITHDRAW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_reports, LAYOUT_FRAGMENTREPORTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_set_new_pwd, LAYOUT_FRAGMENTSETNEWPWD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_sign_in, LAYOUT_FRAGMENTSIGNIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_today, LAYOUT_FRAGMENTTODAY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_tracker, LAYOUT_FRAGMENTTRACKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_tracker_setting, LAYOUT_FRAGMENTTRACKERSETTING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_user_personal_body_parameters, LAYOUT_FRAGMENTUSERPERSONALBODYPARAMETERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_user_personal_info, LAYOUT_FRAGMENTUSERPERSONALINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_voting, LAYOUT_FRAGMENTVOTING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_workout, LAYOUT_FRAGMENTWORKOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.fragment_workout_details, LAYOUT_FRAGMENTWORKOUTDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_calorie_item, LAYOUT_ITEMCALORIEITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_calorie_option_item, LAYOUT_ITEMCALORIEOPTIONITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_chat_footer, LAYOUT_ITEMCHATFOOTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_chat_message, LAYOUT_ITEMCHATMESSAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_dynamic_header, LAYOUT_ITEMDYNAMICHEADER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_dynamic_image, LAYOUT_ITEMDYNAMICIMAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_dynamic_spanned, LAYOUT_ITEMDYNAMICSPANNED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_dynamic_title, LAYOUT_ITEMDYNAMICTITLE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_dynamic_video, LAYOUT_ITEMDYNAMICVIDEO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_dynamic_white_block, LAYOUT_ITEMDYNAMICWHITEBLOCK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_extra_recipe_filter, LAYOUT_ITEMEXTRARECIPEFILTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_faq_header, LAYOUT_ITEMFAQHEADER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_faq_item, LAYOUT_ITEMFAQITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_food_day, LAYOUT_ITEMFOODDAY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_food_empty, LAYOUT_ITEMFOODEMPTY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_food_filter, LAYOUT_ITEMFOODFILTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_food_recipe, LAYOUT_ITEMFOODRECIPE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_line_tracker, LAYOUT_ITEMLINETRACKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_marathons_list, LAYOUT_ITEMMARATHONSLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_marathons_list_header, LAYOUT_ITEMMARATHONSLISTHEADER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_menu_button, LAYOUT_ITEMMENUBUTTON);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_menu_label, LAYOUT_ITEMMENULABEL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_product, LAYOUT_ITEMPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_products_filter, LAYOUT_ITEMPRODUCTSFILTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_purple_btn, LAYOUT_ITEMPURPLEBTN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_recipe_ingredient, LAYOUT_ITEMRECIPEINGREDIENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_recipe_ingredient_header, LAYOUT_ITEMRECIPEINGREDIENTHEADER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_recipe_replacement_filter, LAYOUT_ITEMRECIPEREPLACEMENTFILTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_ref_transaction, LAYOUT_ITEMREFTRANSACTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_setting_sleep_add, LAYOUT_ITEMSETTINGSLEEPADD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_setting_sleep_set, LAYOUT_ITEMSETTINGSLEEPSET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_setting_steps_add, LAYOUT_ITEMSETTINGSTEPSADD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_setting_steps_set, LAYOUT_ITEMSETTINGSTEPSSET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_setting_tracker, LAYOUT_ITEMSETTINGTRACKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_setting_water_set, LAYOUT_ITEMSETTINGWATERSET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_sleep_tracker, LAYOUT_ITEMSLEEPTRACKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_steps_tracker, LAYOUT_ITEMSTEPSTRACKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_switch_fit, LAYOUT_ITEMSWITCHFIT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_test_footer, LAYOUT_ITEMTESTFOOTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_test_header, LAYOUT_ITEMTESTHEADER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_test_item, LAYOUT_ITEMTESTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_test_option_item, LAYOUT_ITEMTESTOPTIONITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_error, LAYOUT_ITEMTODAYERROR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_expired, LAYOUT_ITEMTODAYEXPIRED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_recipe, LAYOUT_ITEMTODAYRECIPE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_recipes, LAYOUT_ITEMTODAYRECIPES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_ref, LAYOUT_ITEMTODAYREF);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_sale, LAYOUT_ITEMTODAYSALE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_sale_offer, LAYOUT_ITEMTODAYSALEOFFER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_task, LAYOUT_ITEMTODAYTASK);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_tasks, LAYOUT_ITEMTODAYTASKS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_telegram, LAYOUT_ITEMTODAYTELEGRAM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_vote, LAYOUT_ITEMTODAYVOTE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_waiting, LAYOUT_ITEMTODAYWAITING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_workout, LAYOUT_ITEMTODAYWORKOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_today_workouts, LAYOUT_ITEMTODAYWORKOUTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_voting_header, LAYOUT_ITEMVOTINGHEADER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_voting_image, LAYOUT_ITEMVOTINGIMAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_voting_user, LAYOUT_ITEMVOTINGUSER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_water_tracker, LAYOUT_ITEMWATERTRACKER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_workout, LAYOUT_ITEMWORKOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_workout_day, LAYOUT_ITEMWORKOUTDAY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_workout_empty, LAYOUT_ITEMWORKOUTEMPTY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.item_workout_filter, LAYOUT_ITEMWORKOUTFILTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_empty, LAYOUT_LAYOUTPROGRESSEMPTY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_make_photos, LAYOUT_LAYOUTPROGRESSMAKEPHOTOS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_make_video, LAYOUT_LAYOUTPROGRESSMAKEVIDEO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_photo_error, LAYOUT_LAYOUTPROGRESSPHOTOERROR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_photo_succeed, LAYOUT_LAYOUTPROGRESSPHOTOSUCCEED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_photos, LAYOUT_LAYOUTPROGRESSPHOTOS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_result, LAYOUT_LAYOUTPROGRESSRESULT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_video_error, LAYOUT_LAYOUTPROGRESSVIDEOERROR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_week_result, LAYOUT_LAYOUTPROGRESSWEEKRESULT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_progress_your_progress, LAYOUT_LAYOUTPROGRESSYOURPROGRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.layout_ref_withdraw, LAYOUT_LAYOUTREFWITHDRAW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.view_input, LAYOUT_VIEWINPUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(fit.lerchek.R.layout.view_recipe_details, LAYOUT_VIEWRECIPEDETAILS);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYCHAT: {
        if ("layout/activity_chat_0".equals(tag)) {
          return new ActivityChatBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_chat is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYCROP: {
        if ("layout/activity_crop_0".equals(tag)) {
          return new ActivityCropBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_crop is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYMARATHON: {
        if ("layout/activity_marathon_0".equals(tag)) {
          return new ActivityMarathonBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_marathon is invalid. Received: " + tag);
      }
      case  LAYOUT_FAVORITEFRAGMENT: {
        if ("layout/favorite_fragment_0".equals(tag)) {
          return new FavoriteFragmentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for favorite_fragment is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTACTIVITYCALENDAR: {
        if ("layout/fragment_activity_calendar_0".equals(tag)) {
          return new FragmentActivityCalendarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_activity_calendar is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDEDITMEASUREMENTS: {
        if ("layout/fragment_add_edit_measurements_0".equals(tag)) {
          return new FragmentAddEditMeasurementsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_edit_measurements is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTBMICALCULATOR: {
        if ("layout/fragment_bmi_calculator_0".equals(tag)) {
          return new FragmentBmiCalculatorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_bmi_calculator is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCALORIECALCULATOR: {
        if ("layout/fragment_calorie_calculator_0".equals(tag)) {
          return new FragmentCalorieCalculatorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_calorie_calculator is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCARDIOTEST: {
        if ("layout/fragment_cardio_test_0".equals(tag)) {
          return new FragmentCardioTestBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_cardio_test is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDYNAMICCONTENT: {
        if ("layout/fragment_dynamic_content_0".equals(tag)) {
          return new FragmentDynamicContentBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_dynamic_content is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTEXTRARECIPES: {
        if ("layout/fragment_extra_recipes_0".equals(tag)) {
          return new FragmentExtraRecipesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_extra_recipes is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFAQ: {
        if ("layout/fragment_faq_0".equals(tag)) {
          return new FragmentFaqBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_faq is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFATCALCULATOR: {
        if ("layout/fragment_fat_calculator_0".equals(tag)) {
          return new FragmentFatCalculatorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_fat_calculator is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFOOD: {
        if ("layout/fragment_food_0".equals(tag)) {
          return new FragmentFoodBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_food is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFOODINGREDIENTS: {
        if ("layout/fragment_food_ingredients_0".equals(tag)) {
          return new FragmentFoodIngredientsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_food_ingredients is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPWD: {
        if ("layout/fragment_forgot_pwd_0".equals(tag)) {
          return new FragmentForgotPwdBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_pwd is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTINFODETAILS: {
        if ("layout/fragment_info_details_0".equals(tag)) {
          return new FragmentInfoDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_info_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMAKEMEASUREMENTS: {
        if ("layout/fragment_make_measurements_0".equals(tag)) {
          return new FragmentMakeMeasurementsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_make_measurements is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMARATHONLIST: {
        if ("layout/fragment_marathon_list_0".equals(tag)) {
          return new FragmentMarathonListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_marathon_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMEASUREMENTS: {
        if ("layout/fragment_measurements_0".equals(tag)) {
          return new FragmentMeasurementsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_measurements is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMENU: {
        if ("layout/fragment_menu_0".equals(tag)) {
          return new FragmentMenuBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_menu is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMORE: {
        if ("layout/fragment_more_0".equals(tag)) {
          return new FragmentMoreBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_more is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPRODUCTS: {
        if ("layout/fragment_products_0".equals(tag)) {
          return new FragmentProductsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_products is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROFILE: {
        if ("layout/fragment_profile_0".equals(tag)) {
          return new FragmentProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROFILETEST: {
        if ("layout/fragment_profile_test_0".equals(tag)) {
          return new FragmentProfileTestBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_profile_test is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROGRESS: {
        if ("layout/fragment_progress_0".equals(tag)) {
          return new FragmentProgressBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_progress is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTRECIPEDETAILS: {
        if ("layout/fragment_recipe_details_0".equals(tag)) {
          return new FragmentRecipeDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_recipe_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTRECIPEREPLACEMENT: {
        if ("layout/fragment_recipe_replacement_0".equals(tag)) {
          return new FragmentRecipeReplacementBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_recipe_replacement is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREFERRAL: {
        if ("layout/fragment_referral_0".equals(tag)) {
          return new FragmentReferralBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_referral is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREFERRALWITHDRAW: {
        if ("layout/fragment_referral_withdraw_0".equals(tag)) {
          return new FragmentReferralWithdrawBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_referral_withdraw is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREPORTS: {
        if ("layout/fragment_reports_0".equals(tag)) {
          return new FragmentReportsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_reports is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSETNEWPWD: {
        if ("layout/fragment_set_new_pwd_0".equals(tag)) {
          return new FragmentSetNewPwdBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_set_new_pwd is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNIN: {
        if ("layout/fragment_sign_in_0".equals(tag)) {
          return new FragmentSignInBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_in is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTODAY: {
        if ("layout/fragment_today_0".equals(tag)) {
          return new FragmentTodayBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_today is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRACKER: {
        if ("layout/fragment_tracker_0".equals(tag)) {
          return new FragmentTrackerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_tracker is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRACKERSETTING: {
        if ("layout/fragment_tracker_setting_0".equals(tag)) {
          return new FragmentTrackerSettingBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_tracker_setting is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTUSERPERSONALBODYPARAMETERS: {
        if ("layout/fragment_user_personal_body_parameters_0".equals(tag)) {
          return new FragmentUserPersonalBodyParametersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_user_personal_body_parameters is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTUSERPERSONALINFO: {
        if ("layout/fragment_user_personal_info_0".equals(tag)) {
          return new FragmentUserPersonalInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_user_personal_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTVOTING: {
        if ("layout/fragment_voting_0".equals(tag)) {
          return new FragmentVotingBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_voting is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWORKOUT: {
        if ("layout/fragment_workout_0".equals(tag)) {
          return new FragmentWorkoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_workout is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWORKOUTDETAILS: {
        if ("layout/fragment_workout_details_0".equals(tag)) {
          return new FragmentWorkoutDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_workout_details is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCALORIEITEM: {
        if ("layout/item_calorie_item_0".equals(tag)) {
          return new ItemCalorieItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_calorie_item is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCALORIEOPTIONITEM: {
        if ("layout/item_calorie_option_item_0".equals(tag)) {
          return new ItemCalorieOptionItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_calorie_option_item is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCHATFOOTER: {
        if ("layout/item_chat_footer_0".equals(tag)) {
          return new ItemChatFooterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat_footer is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCHATMESSAGE: {
        if ("layout/item_chat_message_0".equals(tag)) {
          return new ItemChatMessageBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_chat_message is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMDYNAMICHEADER: {
        if ("layout/item_dynamic_header_0".equals(tag)) {
          return new ItemDynamicHeaderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_dynamic_header is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMDYNAMICIMAGE: {
        if ("layout/item_dynamic_image_0".equals(tag)) {
          return new ItemDynamicImageBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_dynamic_image is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMDYNAMICSPANNED: {
        if ("layout/item_dynamic_spanned_0".equals(tag)) {
          return new ItemDynamicSpannedBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_dynamic_spanned is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMDYNAMICTITLE: {
        if ("layout/item_dynamic_title_0".equals(tag)) {
          return new ItemDynamicTitleBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_dynamic_title is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMDYNAMICVIDEO: {
        if ("layout/item_dynamic_video_0".equals(tag)) {
          return new ItemDynamicVideoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_dynamic_video is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ITEMDYNAMICWHITEBLOCK: {
        if ("layout/item_dynamic_white_block_0".equals(tag)) {
          return new ItemDynamicWhiteBlockBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_dynamic_white_block is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMEXTRARECIPEFILTER: {
        if ("layout/item_extra_recipe_filter_0".equals(tag)) {
          return new ItemExtraRecipeFilterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_extra_recipe_filter is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFAQHEADER: {
        if ("layout/item_faq_header_0".equals(tag)) {
          return new ItemFaqHeaderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_faq_header is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFAQITEM: {
        if ("layout/item_faq_item_0".equals(tag)) {
          return new ItemFaqItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_faq_item is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFOODDAY: {
        if ("layout/item_food_day_0".equals(tag)) {
          return new ItemFoodDayBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_food_day is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFOODEMPTY: {
        if ("layout/item_food_empty_0".equals(tag)) {
          return new ItemFoodEmptyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_food_empty is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFOODFILTER: {
        if ("layout/item_food_filter_0".equals(tag)) {
          return new ItemFoodFilterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_food_filter is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFOODRECIPE: {
        if ("layout/item_food_recipe_0".equals(tag)) {
          return new ItemFoodRecipeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_food_recipe is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMLINETRACKER: {
        if ("layout/item_line_tracker_0".equals(tag)) {
          return new ItemLineTrackerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_line_tracker is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMARATHONSLIST: {
        if ("layout/item_marathons_list_0".equals(tag)) {
          return new ItemMarathonsListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_marathons_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMARATHONSLISTHEADER: {
        if ("layout/item_marathons_list_header_0".equals(tag)) {
          return new ItemMarathonsListHeaderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_marathons_list_header is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMENUBUTTON: {
        if ("layout/item_menu_button_0".equals(tag)) {
          return new ItemMenuButtonBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_menu_button is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMENULABEL: {
        if ("layout/item_menu_label_0".equals(tag)) {
          return new ItemMenuLabelBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_menu_label is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMPRODUCT: {
        if ("layout/item_product_0".equals(tag)) {
          return new ItemProductBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_product is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMPRODUCTSFILTER: {
        if ("layout/item_products_filter_0".equals(tag)) {
          return new ItemProductsFilterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_products_filter is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMPURPLEBTN: {
        if ("layout/item_purple_btn_0".equals(tag)) {
          return new ItemPurpleBtnBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_purple_btn is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMRECIPEINGREDIENT: {
        if ("layout/item_recipe_ingredient_0".equals(tag)) {
          return new ItemRecipeIngredientBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_recipe_ingredient is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMRECIPEINGREDIENTHEADER: {
        if ("layout/item_recipe_ingredient_header_0".equals(tag)) {
          return new ItemRecipeIngredientHeaderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_recipe_ingredient_header is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMRECIPEREPLACEMENTFILTER: {
        if ("layout/item_recipe_replacement_filter_0".equals(tag)) {
          return new ItemRecipeReplacementFilterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_recipe_replacement_filter is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMREFTRANSACTION: {
        if ("layout/item_ref_transaction_0".equals(tag)) {
          return new ItemRefTransactionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_ref_transaction is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSETTINGSLEEPADD: {
        if ("layout/item_setting_sleep_add_0".equals(tag)) {
          return new ItemSettingSleepAddBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_setting_sleep_add is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSETTINGSLEEPSET: {
        if ("layout/item_setting_sleep_set_0".equals(tag)) {
          return new ItemSettingSleepSetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_setting_sleep_set is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSETTINGSTEPSADD: {
        if ("layout/item_setting_steps_add_0".equals(tag)) {
          return new ItemSettingStepsAddBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_setting_steps_add is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSETTINGSTEPSSET: {
        if ("layout/item_setting_steps_set_0".equals(tag)) {
          return new ItemSettingStepsSetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_setting_steps_set is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSETTINGTRACKER: {
        if ("layout/item_setting_tracker_0".equals(tag)) {
          return new ItemSettingTrackerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_setting_tracker is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSETTINGWATERSET: {
        if ("layout/item_setting_water_set_0".equals(tag)) {
          return new ItemSettingWaterSetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_setting_water_set is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSLEEPTRACKER: {
        if ("layout/item_sleep_tracker_0".equals(tag)) {
          return new ItemSleepTrackerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_sleep_tracker is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSTEPSTRACKER: {
        if ("layout/item_steps_tracker_0".equals(tag)) {
          return new ItemStepsTrackerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_steps_tracker is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSWITCHFIT: {
        if ("layout/item_switch_fit_0".equals(tag)) {
          return new ItemSwitchFitBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_switch_fit is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTESTFOOTER: {
        if ("layout/item_test_footer_0".equals(tag)) {
          return new ItemTestFooterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_test_footer is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTESTHEADER: {
        if ("layout/item_test_header_0".equals(tag)) {
          return new ItemTestHeaderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_test_header is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTESTITEM: {
        if ("layout/item_test_item_0".equals(tag)) {
          return new ItemTestItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_test_item is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTESTOPTIONITEM: {
        if ("layout/item_test_option_item_0".equals(tag)) {
          return new ItemTestOptionItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_test_option_item is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYERROR: {
        if ("layout/item_today_error_0".equals(tag)) {
          return new ItemTodayErrorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_error is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYEXPIRED: {
        if ("layout/item_today_expired_0".equals(tag)) {
          return new ItemTodayExpiredBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_expired is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYRECIPE: {
        if ("layout/item_today_recipe_0".equals(tag)) {
          return new ItemTodayRecipeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_recipe is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYRECIPES: {
        if ("layout/item_today_recipes_0".equals(tag)) {
          return new ItemTodayRecipesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_recipes is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYREF: {
        if ("layout/item_today_ref_0".equals(tag)) {
          return new ItemTodayRefBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_ref is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYSALE: {
        if ("layout/item_today_sale_0".equals(tag)) {
          return new ItemTodaySaleBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_sale is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYSALEOFFER: {
        if ("layout/item_today_sale_offer_0".equals(tag)) {
          return new ItemTodaySaleOfferBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_sale_offer is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYTASK: {
        if ("layout/item_today_task_0".equals(tag)) {
          return new ItemTodayTaskBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_task is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYTASKS: {
        if ("layout/item_today_tasks_0".equals(tag)) {
          return new ItemTodayTasksBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_tasks is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYTELEGRAM: {
        if ("layout/item_today_telegram_0".equals(tag)) {
          return new ItemTodayTelegramBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_telegram is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYVOTE: {
        if ("layout/item_today_vote_0".equals(tag)) {
          return new ItemTodayVoteBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_vote is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYWAITING: {
        if ("layout/item_today_waiting_0".equals(tag)) {
          return new ItemTodayWaitingBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_waiting is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYWORKOUT: {
        if ("layout/item_today_workout_0".equals(tag)) {
          return new ItemTodayWorkoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_workout is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMTODAYWORKOUTS: {
        if ("layout/item_today_workouts_0".equals(tag)) {
          return new ItemTodayWorkoutsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_today_workouts is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMVOTINGHEADER: {
        if ("layout/item_voting_header_0".equals(tag)) {
          return new ItemVotingHeaderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_voting_header is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMVOTINGIMAGE: {
        if ("layout/item_voting_image_0".equals(tag)) {
          return new ItemVotingImageBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_voting_image is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMVOTINGUSER: {
        if ("layout/item_voting_user_0".equals(tag)) {
          return new ItemVotingUserBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_voting_user is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding2(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ITEMWATERTRACKER: {
        if ("layout/item_water_tracker_0".equals(tag)) {
          return new ItemWaterTrackerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_water_tracker is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMWORKOUT: {
        if ("layout/item_workout_0".equals(tag)) {
          return new ItemWorkoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_workout is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMWORKOUTDAY: {
        if ("layout/item_workout_day_0".equals(tag)) {
          return new ItemWorkoutDayBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_workout_day is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMWORKOUTEMPTY: {
        if ("layout/item_workout_empty_0".equals(tag)) {
          return new ItemWorkoutEmptyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_workout_empty is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMWORKOUTFILTER: {
        if ("layout/item_workout_filter_0".equals(tag)) {
          return new ItemWorkoutFilterBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_workout_filter is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSEMPTY: {
        if ("layout/layout_progress_empty_0".equals(tag)) {
          return new LayoutProgressEmptyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_empty is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSMAKEPHOTOS: {
        if ("layout/layout_progress_make_photos_0".equals(tag)) {
          return new LayoutProgressMakePhotosBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_make_photos is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSMAKEVIDEO: {
        if ("layout/layout_progress_make_video_0".equals(tag)) {
          return new LayoutProgressMakeVideoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_make_video is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSPHOTOERROR: {
        if ("layout/layout_progress_photo_error_0".equals(tag)) {
          return new LayoutProgressPhotoErrorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_photo_error is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSPHOTOSUCCEED: {
        if ("layout/layout_progress_photo_succeed_0".equals(tag)) {
          return new LayoutProgressPhotoSucceedBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_photo_succeed is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSPHOTOS: {
        if ("layout/layout_progress_photos_0".equals(tag)) {
          return new LayoutProgressPhotosBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_photos is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSRESULT: {
        if ("layout/layout_progress_result_0".equals(tag)) {
          return new LayoutProgressResultBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_result is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSVIDEOERROR: {
        if ("layout/layout_progress_video_error_0".equals(tag)) {
          return new LayoutProgressVideoErrorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_video_error is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSWEEKRESULT: {
        if ("layout/layout_progress_week_result_0".equals(tag)) {
          return new LayoutProgressWeekResultBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_week_result is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTPROGRESSYOURPROGRESS: {
        if ("layout/layout_progress_your_progress_0".equals(tag)) {
          return new LayoutProgressYourProgressBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_progress_your_progress is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTREFWITHDRAW: {
        if ("layout/layout_ref_withdraw_0".equals(tag)) {
          return new LayoutRefWithdrawBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_ref_withdraw is invalid. Received: " + tag);
      }
      case  LAYOUT_VIEWINPUT: {
        if ("layout/view_input_0".equals(tag)) {
          return new ViewInputBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for view_input is invalid. Received: " + tag);
      }
      case  LAYOUT_VIEWRECIPEDETAILS: {
        if ("layout/view_recipe_details_0".equals(tag)) {
          return new ViewRecipeDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for view_recipe_details is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
        case 2: {
          return internalGetViewDataBinding2(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(22);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "calendarCallback");
      sKeys.put(2, "callback");
      sKeys.put(3, "context");
      sKeys.put(4, "description");
      sKeys.put(5, "errorMessage");
      sKeys.put(6, "faqBackCallback");
      sKeys.put(7, "headerText");
      sKeys.put(8, "isReplacement");
      sKeys.put(9, "item");
      sKeys.put(10, "lifecycleHolder");
      sKeys.put(11, "link");
      sKeys.put(12, "message");
      sKeys.put(13, "model");
      sKeys.put(14, "price");
      sKeys.put(15, "progress");
      sKeys.put(16, "showErrorHint");
      sKeys.put(17, "showSuccessHint");
      sKeys.put(18, "taskCallback");
      sKeys.put(19, "text");
      sKeys.put(20, "title");
      sKeys.put(21, "viewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(118);

    static {
      sKeys.put("layout/activity_chat_0", fit.lerchek.R.layout.activity_chat);
      sKeys.put("layout/activity_crop_0", fit.lerchek.R.layout.activity_crop);
      sKeys.put("layout/activity_marathon_0", fit.lerchek.R.layout.activity_marathon);
      sKeys.put("layout/favorite_fragment_0", fit.lerchek.R.layout.favorite_fragment);
      sKeys.put("layout/fragment_activity_calendar_0", fit.lerchek.R.layout.fragment_activity_calendar);
      sKeys.put("layout/fragment_add_edit_measurements_0", fit.lerchek.R.layout.fragment_add_edit_measurements);
      sKeys.put("layout/fragment_bmi_calculator_0", fit.lerchek.R.layout.fragment_bmi_calculator);
      sKeys.put("layout/fragment_calorie_calculator_0", fit.lerchek.R.layout.fragment_calorie_calculator);
      sKeys.put("layout/fragment_cardio_test_0", fit.lerchek.R.layout.fragment_cardio_test);
      sKeys.put("layout/fragment_dynamic_content_0", fit.lerchek.R.layout.fragment_dynamic_content);
      sKeys.put("layout/fragment_extra_recipes_0", fit.lerchek.R.layout.fragment_extra_recipes);
      sKeys.put("layout/fragment_faq_0", fit.lerchek.R.layout.fragment_faq);
      sKeys.put("layout/fragment_fat_calculator_0", fit.lerchek.R.layout.fragment_fat_calculator);
      sKeys.put("layout/fragment_food_0", fit.lerchek.R.layout.fragment_food);
      sKeys.put("layout/fragment_food_ingredients_0", fit.lerchek.R.layout.fragment_food_ingredients);
      sKeys.put("layout/fragment_forgot_pwd_0", fit.lerchek.R.layout.fragment_forgot_pwd);
      sKeys.put("layout/fragment_info_details_0", fit.lerchek.R.layout.fragment_info_details);
      sKeys.put("layout/fragment_make_measurements_0", fit.lerchek.R.layout.fragment_make_measurements);
      sKeys.put("layout/fragment_marathon_list_0", fit.lerchek.R.layout.fragment_marathon_list);
      sKeys.put("layout/fragment_measurements_0", fit.lerchek.R.layout.fragment_measurements);
      sKeys.put("layout/fragment_menu_0", fit.lerchek.R.layout.fragment_menu);
      sKeys.put("layout/fragment_more_0", fit.lerchek.R.layout.fragment_more);
      sKeys.put("layout/fragment_products_0", fit.lerchek.R.layout.fragment_products);
      sKeys.put("layout/fragment_profile_0", fit.lerchek.R.layout.fragment_profile);
      sKeys.put("layout/fragment_profile_test_0", fit.lerchek.R.layout.fragment_profile_test);
      sKeys.put("layout/fragment_progress_0", fit.lerchek.R.layout.fragment_progress);
      sKeys.put("layout/fragment_recipe_details_0", fit.lerchek.R.layout.fragment_recipe_details);
      sKeys.put("layout/fragment_recipe_replacement_0", fit.lerchek.R.layout.fragment_recipe_replacement);
      sKeys.put("layout/fragment_referral_0", fit.lerchek.R.layout.fragment_referral);
      sKeys.put("layout/fragment_referral_withdraw_0", fit.lerchek.R.layout.fragment_referral_withdraw);
      sKeys.put("layout/fragment_reports_0", fit.lerchek.R.layout.fragment_reports);
      sKeys.put("layout/fragment_set_new_pwd_0", fit.lerchek.R.layout.fragment_set_new_pwd);
      sKeys.put("layout/fragment_sign_in_0", fit.lerchek.R.layout.fragment_sign_in);
      sKeys.put("layout/fragment_today_0", fit.lerchek.R.layout.fragment_today);
      sKeys.put("layout/fragment_tracker_0", fit.lerchek.R.layout.fragment_tracker);
      sKeys.put("layout/fragment_tracker_setting_0", fit.lerchek.R.layout.fragment_tracker_setting);
      sKeys.put("layout/fragment_user_personal_body_parameters_0", fit.lerchek.R.layout.fragment_user_personal_body_parameters);
      sKeys.put("layout/fragment_user_personal_info_0", fit.lerchek.R.layout.fragment_user_personal_info);
      sKeys.put("layout/fragment_voting_0", fit.lerchek.R.layout.fragment_voting);
      sKeys.put("layout/fragment_workout_0", fit.lerchek.R.layout.fragment_workout);
      sKeys.put("layout/fragment_workout_details_0", fit.lerchek.R.layout.fragment_workout_details);
      sKeys.put("layout/item_calorie_item_0", fit.lerchek.R.layout.item_calorie_item);
      sKeys.put("layout/item_calorie_option_item_0", fit.lerchek.R.layout.item_calorie_option_item);
      sKeys.put("layout/item_chat_footer_0", fit.lerchek.R.layout.item_chat_footer);
      sKeys.put("layout/item_chat_message_0", fit.lerchek.R.layout.item_chat_message);
      sKeys.put("layout/item_dynamic_header_0", fit.lerchek.R.layout.item_dynamic_header);
      sKeys.put("layout/item_dynamic_image_0", fit.lerchek.R.layout.item_dynamic_image);
      sKeys.put("layout/item_dynamic_spanned_0", fit.lerchek.R.layout.item_dynamic_spanned);
      sKeys.put("layout/item_dynamic_title_0", fit.lerchek.R.layout.item_dynamic_title);
      sKeys.put("layout/item_dynamic_video_0", fit.lerchek.R.layout.item_dynamic_video);
      sKeys.put("layout/item_dynamic_white_block_0", fit.lerchek.R.layout.item_dynamic_white_block);
      sKeys.put("layout/item_extra_recipe_filter_0", fit.lerchek.R.layout.item_extra_recipe_filter);
      sKeys.put("layout/item_faq_header_0", fit.lerchek.R.layout.item_faq_header);
      sKeys.put("layout/item_faq_item_0", fit.lerchek.R.layout.item_faq_item);
      sKeys.put("layout/item_food_day_0", fit.lerchek.R.layout.item_food_day);
      sKeys.put("layout/item_food_empty_0", fit.lerchek.R.layout.item_food_empty);
      sKeys.put("layout/item_food_filter_0", fit.lerchek.R.layout.item_food_filter);
      sKeys.put("layout/item_food_recipe_0", fit.lerchek.R.layout.item_food_recipe);
      sKeys.put("layout/item_line_tracker_0", fit.lerchek.R.layout.item_line_tracker);
      sKeys.put("layout/item_marathons_list_0", fit.lerchek.R.layout.item_marathons_list);
      sKeys.put("layout/item_marathons_list_header_0", fit.lerchek.R.layout.item_marathons_list_header);
      sKeys.put("layout/item_menu_button_0", fit.lerchek.R.layout.item_menu_button);
      sKeys.put("layout/item_menu_label_0", fit.lerchek.R.layout.item_menu_label);
      sKeys.put("layout/item_product_0", fit.lerchek.R.layout.item_product);
      sKeys.put("layout/item_products_filter_0", fit.lerchek.R.layout.item_products_filter);
      sKeys.put("layout/item_purple_btn_0", fit.lerchek.R.layout.item_purple_btn);
      sKeys.put("layout/item_recipe_ingredient_0", fit.lerchek.R.layout.item_recipe_ingredient);
      sKeys.put("layout/item_recipe_ingredient_header_0", fit.lerchek.R.layout.item_recipe_ingredient_header);
      sKeys.put("layout/item_recipe_replacement_filter_0", fit.lerchek.R.layout.item_recipe_replacement_filter);
      sKeys.put("layout/item_ref_transaction_0", fit.lerchek.R.layout.item_ref_transaction);
      sKeys.put("layout/item_setting_sleep_add_0", fit.lerchek.R.layout.item_setting_sleep_add);
      sKeys.put("layout/item_setting_sleep_set_0", fit.lerchek.R.layout.item_setting_sleep_set);
      sKeys.put("layout/item_setting_steps_add_0", fit.lerchek.R.layout.item_setting_steps_add);
      sKeys.put("layout/item_setting_steps_set_0", fit.lerchek.R.layout.item_setting_steps_set);
      sKeys.put("layout/item_setting_tracker_0", fit.lerchek.R.layout.item_setting_tracker);
      sKeys.put("layout/item_setting_water_set_0", fit.lerchek.R.layout.item_setting_water_set);
      sKeys.put("layout/item_sleep_tracker_0", fit.lerchek.R.layout.item_sleep_tracker);
      sKeys.put("layout/item_steps_tracker_0", fit.lerchek.R.layout.item_steps_tracker);
      sKeys.put("layout/item_switch_fit_0", fit.lerchek.R.layout.item_switch_fit);
      sKeys.put("layout/item_test_footer_0", fit.lerchek.R.layout.item_test_footer);
      sKeys.put("layout/item_test_header_0", fit.lerchek.R.layout.item_test_header);
      sKeys.put("layout/item_test_item_0", fit.lerchek.R.layout.item_test_item);
      sKeys.put("layout/item_test_option_item_0", fit.lerchek.R.layout.item_test_option_item);
      sKeys.put("layout/item_today_error_0", fit.lerchek.R.layout.item_today_error);
      sKeys.put("layout/item_today_expired_0", fit.lerchek.R.layout.item_today_expired);
      sKeys.put("layout/item_today_recipe_0", fit.lerchek.R.layout.item_today_recipe);
      sKeys.put("layout/item_today_recipes_0", fit.lerchek.R.layout.item_today_recipes);
      sKeys.put("layout/item_today_ref_0", fit.lerchek.R.layout.item_today_ref);
      sKeys.put("layout/item_today_sale_0", fit.lerchek.R.layout.item_today_sale);
      sKeys.put("layout/item_today_sale_offer_0", fit.lerchek.R.layout.item_today_sale_offer);
      sKeys.put("layout/item_today_task_0", fit.lerchek.R.layout.item_today_task);
      sKeys.put("layout/item_today_tasks_0", fit.lerchek.R.layout.item_today_tasks);
      sKeys.put("layout/item_today_telegram_0", fit.lerchek.R.layout.item_today_telegram);
      sKeys.put("layout/item_today_vote_0", fit.lerchek.R.layout.item_today_vote);
      sKeys.put("layout/item_today_waiting_0", fit.lerchek.R.layout.item_today_waiting);
      sKeys.put("layout/item_today_workout_0", fit.lerchek.R.layout.item_today_workout);
      sKeys.put("layout/item_today_workouts_0", fit.lerchek.R.layout.item_today_workouts);
      sKeys.put("layout/item_voting_header_0", fit.lerchek.R.layout.item_voting_header);
      sKeys.put("layout/item_voting_image_0", fit.lerchek.R.layout.item_voting_image);
      sKeys.put("layout/item_voting_user_0", fit.lerchek.R.layout.item_voting_user);
      sKeys.put("layout/item_water_tracker_0", fit.lerchek.R.layout.item_water_tracker);
      sKeys.put("layout/item_workout_0", fit.lerchek.R.layout.item_workout);
      sKeys.put("layout/item_workout_day_0", fit.lerchek.R.layout.item_workout_day);
      sKeys.put("layout/item_workout_empty_0", fit.lerchek.R.layout.item_workout_empty);
      sKeys.put("layout/item_workout_filter_0", fit.lerchek.R.layout.item_workout_filter);
      sKeys.put("layout/layout_progress_empty_0", fit.lerchek.R.layout.layout_progress_empty);
      sKeys.put("layout/layout_progress_make_photos_0", fit.lerchek.R.layout.layout_progress_make_photos);
      sKeys.put("layout/layout_progress_make_video_0", fit.lerchek.R.layout.layout_progress_make_video);
      sKeys.put("layout/layout_progress_photo_error_0", fit.lerchek.R.layout.layout_progress_photo_error);
      sKeys.put("layout/layout_progress_photo_succeed_0", fit.lerchek.R.layout.layout_progress_photo_succeed);
      sKeys.put("layout/layout_progress_photos_0", fit.lerchek.R.layout.layout_progress_photos);
      sKeys.put("layout/layout_progress_result_0", fit.lerchek.R.layout.layout_progress_result);
      sKeys.put("layout/layout_progress_video_error_0", fit.lerchek.R.layout.layout_progress_video_error);
      sKeys.put("layout/layout_progress_week_result_0", fit.lerchek.R.layout.layout_progress_week_result);
      sKeys.put("layout/layout_progress_your_progress_0", fit.lerchek.R.layout.layout_progress_your_progress);
      sKeys.put("layout/layout_ref_withdraw_0", fit.lerchek.R.layout.layout_ref_withdraw);
      sKeys.put("layout/view_input_0", fit.lerchek.R.layout.view_input);
      sKeys.put("layout/view_recipe_details_0", fit.lerchek.R.layout.view_recipe_details);
    }
  }
}
