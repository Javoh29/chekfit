package fit.lerchek.data.fcm;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ServiceComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = FMService.class
)
@GeneratedEntryPoint
@InstallIn(ServiceComponent.class)
public interface FMService_GeneratedInjector {
  void injectFMService(FMService fMService);
}
