package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentFoodIngredientsBindingImpl extends FragmentFoodIngredientsBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.nsvIngredients, 6);
        sViewsWithIds.put(R.id.iconSelect, 7);
        sViewsWithIds.put(R.id.btnCheckAll, 8);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView3;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback6;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentFoodIngredientsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds));
    }
    private FragmentFoodIngredientsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (android.widget.LinearLayout) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (androidx.core.widget.NestedScrollView) bindings[6]
            , (android.widget.FrameLayout) bindings[4]
            );
        this.btnSelectWeek.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.recyclerview.widget.RecyclerView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (bindings[5] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[5]) : null;
        this.progressBlock.setTag(null);
        setRootTag(root);
        // listeners
        mCallback6 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.food.FoodIngredientsCallback) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.food.FoodIngredientsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.food.FoodIngredientsCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }
    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.food.FoodIngredientsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelSelectedWeek((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.WeekUIModel>) object, fieldId);
            case 1 :
                return onChangeViewModelIngredients((androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.TaskUIModel>) object, fieldId);
            case 2 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelSelectedWeek(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.WeekUIModel> ViewModelSelectedWeek, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIngredients(androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.TaskUIModel> ViewModelIngredients, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.WeekUIModel viewModelSelectedWeekGet = null;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.WeekUIModel> viewModelSelectedWeek = null;
        java.lang.String viewModelSelectedWeekName = null;
        androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.TaskUIModel> viewModelIngredients = null;
        fit.lerchek.ui.feature.marathon.food.FoodIngredientsCallback callback = mCallback;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.food.FoodIngredientsViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x3aL) != 0) {
        }
        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.selectedWeek
                        viewModelSelectedWeek = viewModel.getSelectedWeek();
                    }
                    updateRegistration(0, viewModelSelectedWeek);


                    if (viewModelSelectedWeek != null) {
                        // read viewModel.selectedWeek.get()
                        viewModelSelectedWeekGet = viewModelSelectedWeek.get();
                    }


                    if (viewModelSelectedWeekGet != null) {
                        // read viewModel.selectedWeek.get().name
                        viewModelSelectedWeekName = viewModelSelectedWeekGet.getName();
                    }
            }
            if ((dirtyFlags & 0x3aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.ingredients
                        viewModelIngredients = viewModel.getIngredients();
                    }
                    updateRegistration(1, viewModelIngredients);
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(2, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x34L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x80L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.btnSelectWeek.setOnClickListener(mCallback6);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelSelectedWeekName);
        }
        if ((dirtyFlags & 0x3aL) != 0) {
            // api target 1

            fit.lerchek.ui.feature.marathon.food.FoodIngredientsAdapterKt.setupIngredientsAdapter(this.mboundView3, viewModelIngredients, callback);
        }
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel.weeks
        androidx.databinding.ObservableArrayList<fit.lerchek.data.domain.uimodel.marathon.WeekUIModel> viewModelWeeks = null;
        // callback
        fit.lerchek.ui.feature.marathon.food.FoodIngredientsCallback callback = mCallback;
        // viewModel
        fit.lerchek.ui.feature.marathon.food.FoodIngredientsViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelWeeks = viewModel.getWeeks();

                callback.onSelectWeekClick(viewModelWeeks);
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.selectedWeek
        flag 1 (0x2L): viewModel.ingredients
        flag 2 (0x3L): viewModel.isProgress()
        flag 3 (0x4L): callback
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
        flag 6 (0x7L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 7 (0x8L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}