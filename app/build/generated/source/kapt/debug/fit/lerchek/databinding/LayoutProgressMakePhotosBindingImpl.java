package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutProgressMakePhotosBindingImpl extends LayoutProgressMakePhotosBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imgPhoto, 5);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback11;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutProgressMakePhotosBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private LayoutProgressMakePhotosBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (androidx.appcompat.widget.AppCompatCheckedTextView) bindings[2]
            , (androidx.appcompat.widget.AppCompatCheckedTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatCheckedTextView) bindings[3]
            );
        this.btnUploadPhoto.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.textBackPhoto.setTag(null);
        this.textFrontPhoto.setTag(null);
        this.textSidePhoto.setTag(null);
        setRootTag(root);
        // listeners
        mCallback11 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeModelCheckBackPhoto((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeModelSelectedWeek((androidx.databinding.ObservableInt) object, fieldId);
            case 2 :
                return onChangeModelUploadPhotoText((androidx.databinding.ObservableInt) object, fieldId);
            case 3 :
                return onChangeModelCheckFrontPhoto((androidx.databinding.ObservableBoolean) object, fieldId);
            case 4 :
                return onChangeModelProgress((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel>) object, fieldId);
            case 5 :
                return onChangeModelCheckSidePhoto((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeModelCheckBackPhoto(androidx.databinding.ObservableBoolean ModelCheckBackPhoto, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelSelectedWeek(androidx.databinding.ObservableInt ModelSelectedWeek, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelUploadPhotoText(androidx.databinding.ObservableInt ModelUploadPhotoText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelCheckFrontPhoto(androidx.databinding.ObservableBoolean ModelCheckFrontPhoto, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelProgress(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> ModelProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelCheckSidePhoto(androidx.databinding.ObservableBoolean ModelCheckSidePhoto, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel model = mModel;
        int modelSelectedWeekGet = 0;
        androidx.databinding.ObservableBoolean modelCheckBackPhoto = null;
        int modelProgressPhotoSucceedModelSelectedWeekViewGONEViewVISIBLE = 0;
        int modelUploadPhotoTextGet = 0;
        androidx.databinding.ObservableInt modelSelectedWeek = null;
        fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback callback = mCallback;
        fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel modelProgressGet = null;
        androidx.databinding.ObservableInt modelUploadPhotoText = null;
        androidx.databinding.ObservableBoolean modelCheckFrontPhoto = null;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> modelProgress = null;
        boolean modelCheckFrontPhotoGet = false;
        boolean modelCheckBackPhotoGet = false;
        androidx.databinding.ObservableBoolean modelCheckSidePhoto = null;
        boolean modelProgressPhotoSucceedModelSelectedWeek = false;
        boolean modelCheckSidePhotoGet = false;

        if ((dirtyFlags & 0x17fL) != 0) {


            if ((dirtyFlags & 0x141L) != 0) {

                    if (model != null) {
                        // read model.checkBackPhoto
                        modelCheckBackPhoto = model.getCheckBackPhoto();
                    }
                    updateRegistration(0, modelCheckBackPhoto);


                    if (modelCheckBackPhoto != null) {
                        // read model.checkBackPhoto.get()
                        modelCheckBackPhotoGet = modelCheckBackPhoto.get();
                    }
            }
            if ((dirtyFlags & 0x152L) != 0) {

                    if (model != null) {
                        // read model.selectedWeek
                        modelSelectedWeek = model.getSelectedWeek();
                        // read model.progress
                        modelProgress = model.getProgress();
                    }
                    updateRegistration(1, modelSelectedWeek);
                    updateRegistration(4, modelProgress);


                    if (modelSelectedWeek != null) {
                        // read model.selectedWeek.get()
                        modelSelectedWeekGet = modelSelectedWeek.get();
                    }
                    if (modelProgress != null) {
                        // read model.progress.get()
                        modelProgressGet = modelProgress.get();
                    }


                    if (modelProgressGet != null) {
                        // read model.progress.get().photoSucceed(model.selectedWeek.get())
                        modelProgressPhotoSucceedModelSelectedWeek = modelProgressGet.photoSucceed(modelSelectedWeekGet);
                    }
                if((dirtyFlags & 0x152L) != 0) {
                    if(modelProgressPhotoSucceedModelSelectedWeek) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read model.progress.get().photoSucceed(model.selectedWeek.get()) ? View.GONE : View.VISIBLE
                    modelProgressPhotoSucceedModelSelectedWeekViewGONEViewVISIBLE = ((modelProgressPhotoSucceedModelSelectedWeek) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x144L) != 0) {

                    if (model != null) {
                        // read model.uploadPhotoText
                        modelUploadPhotoText = model.getUploadPhotoText();
                    }
                    updateRegistration(2, modelUploadPhotoText);


                    if (modelUploadPhotoText != null) {
                        // read model.uploadPhotoText.get()
                        modelUploadPhotoTextGet = modelUploadPhotoText.get();
                    }
            }
            if ((dirtyFlags & 0x148L) != 0) {

                    if (model != null) {
                        // read model.checkFrontPhoto
                        modelCheckFrontPhoto = model.getCheckFrontPhoto();
                    }
                    updateRegistration(3, modelCheckFrontPhoto);


                    if (modelCheckFrontPhoto != null) {
                        // read model.checkFrontPhoto.get()
                        modelCheckFrontPhotoGet = modelCheckFrontPhoto.get();
                    }
            }
            if ((dirtyFlags & 0x160L) != 0) {

                    if (model != null) {
                        // read model.checkSidePhoto
                        modelCheckSidePhoto = model.getCheckSidePhoto();
                    }
                    updateRegistration(5, modelCheckSidePhoto);


                    if (modelCheckSidePhoto != null) {
                        // read model.checkSidePhoto.get()
                        modelCheckSidePhotoGet = modelCheckSidePhoto.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            this.btnUploadPhoto.setOnClickListener(mCallback11);
        }
        if ((dirtyFlags & 0x144L) != 0) {
            // api target 1

            this.btnUploadPhoto.setText(modelUploadPhotoTextGet);
        }
        if ((dirtyFlags & 0x152L) != 0) {
            // api target 1

            this.btnUploadPhoto.setVisibility(modelProgressPhotoSucceedModelSelectedWeekViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x141L) != 0) {
            // api target 1

            this.textBackPhoto.setChecked(modelCheckBackPhotoGet);
        }
        if ((dirtyFlags & 0x148L) != 0) {
            // api target 1

            this.textFrontPhoto.setChecked(modelCheckFrontPhotoGet);
        }
        if ((dirtyFlags & 0x160L) != 0) {
            // api target 1

            this.textSidePhoto.setChecked(modelCheckSidePhotoGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // model
        fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel model = mModel;
        // model.selectedWeek.get()
        int modelSelectedWeekGet = 0;
        // model != null
        boolean modelJavaLangObjectNull = false;
        // model.selectedWeek != null
        boolean modelSelectedWeekJavaLangObjectNull = false;
        // model.selectedWeek
        androidx.databinding.ObservableInt modelSelectedWeek = null;
        // callback
        fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {



            modelJavaLangObjectNull = (model) != (null);
            if (modelJavaLangObjectNull) {


                modelSelectedWeek = model.getSelectedWeek();

                modelSelectedWeekJavaLangObjectNull = (modelSelectedWeek) != (null);
                if (modelSelectedWeekJavaLangObjectNull) {


                    modelSelectedWeekGet = modelSelectedWeek.get();

                    callback.onUploadPhoto(modelSelectedWeekGet);
                }
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model.checkBackPhoto
        flag 1 (0x2L): model.selectedWeek
        flag 2 (0x3L): model.uploadPhotoText
        flag 3 (0x4L): model.checkFrontPhoto
        flag 4 (0x5L): model.progress
        flag 5 (0x6L): model.checkSidePhoto
        flag 6 (0x7L): model
        flag 7 (0x8L): callback
        flag 8 (0x9L): null
        flag 9 (0xaL): model.progress.get().photoSucceed(model.selectedWeek.get()) ? View.GONE : View.VISIBLE
        flag 10 (0xbL): model.progress.get().photoSucceed(model.selectedWeek.get()) ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}