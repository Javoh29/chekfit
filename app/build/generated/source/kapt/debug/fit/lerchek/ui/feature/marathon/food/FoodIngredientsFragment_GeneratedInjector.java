package fit.lerchek.ui.feature.marathon.food;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = FoodIngredientsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface FoodIngredientsFragment_GeneratedInjector {
  void injectFoodIngredientsFragment(FoodIngredientsFragment foodIngredientsFragment);
}
