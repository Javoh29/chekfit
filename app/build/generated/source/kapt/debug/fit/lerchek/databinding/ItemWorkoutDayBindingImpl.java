package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemWorkoutDayBindingImpl extends ItemWorkoutDayBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback27;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemWorkoutDayBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private ItemWorkoutDayBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        mCallback27 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.DayUIModel) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.workout.WorkoutCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.DayUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.workout.WorkoutCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel model = mModel;
        int modelSelectedMboundView2AndroidColorWhiteMboundView2AndroidColorTextPrimary = 0;
        int modelSelectedMboundView1AndroidColorWhiteMboundView1AndroidColorTextPrimary = 0;
        float modelSelectedMboundView1AndroidDimenTextSize15MboundView1AndroidDimenTextSize14 = 0f;
        java.lang.String modelName = null;
        android.graphics.drawable.Drawable modelSelectedMboundView0AndroidDrawableBgSelectedWorkoutDayMboundView0AndroidDrawableBgUnselectedFoodDay = null;
        fit.lerchek.ui.feature.marathon.workout.WorkoutCallback callback = mCallback;
        boolean modelSelected = false;
        java.lang.String modelDayName = null;
        float modelSelectedMboundView2AndroidDimenTextSize22MboundView2AndroidDimenTextSize18 = 0f;

        if ((dirtyFlags & 0x5L) != 0) {



                if (model != null) {
                    // read model.name
                    modelName = model.getName();
                    // read model.selected
                    modelSelected = model.getSelected();
                    // read model.dayName
                    modelDayName = model.getDayName();
                }
            if((dirtyFlags & 0x5L) != 0) {
                if(modelSelected) {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x40L;
                        dirtyFlags |= 0x100L;
                        dirtyFlags |= 0x400L;
                        dirtyFlags |= 0x1000L;
                }
                else {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x80L;
                        dirtyFlags |= 0x200L;
                        dirtyFlags |= 0x800L;
                }
            }


                // read model.selected ? @android:color/white : @android:color/text_primary
                modelSelectedMboundView2AndroidColorWhiteMboundView2AndroidColorTextPrimary = ((modelSelected) ? (getColorFromResource(mboundView2, R.color.white)) : (getColorFromResource(mboundView2, R.color.text_primary)));
                // read model.selected ? @android:color/white : @android:color/text_primary
                modelSelectedMboundView1AndroidColorWhiteMboundView1AndroidColorTextPrimary = ((modelSelected) ? (getColorFromResource(mboundView1, R.color.white)) : (getColorFromResource(mboundView1, R.color.text_primary)));
                // read model.selected ? @android:dimen/text_size_15 : @android:dimen/text_size_14
                modelSelectedMboundView1AndroidDimenTextSize15MboundView1AndroidDimenTextSize14 = ((modelSelected) ? (mboundView1.getResources().getDimension(R.dimen.text_size_15)) : (mboundView1.getResources().getDimension(R.dimen.text_size_14)));
                // read model.selected ? @android:drawable/bg_selected_workout_day : @android:drawable/bg_unselected_food_day
                modelSelectedMboundView0AndroidDrawableBgSelectedWorkoutDayMboundView0AndroidDrawableBgUnselectedFoodDay = ((modelSelected) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView0.getContext(), R.drawable.bg_selected_workout_day)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView0.getContext(), R.drawable.bg_unselected_food_day)));
                // read model.selected ? @android:dimen/text_size_22 : @android:dimen/text_size_18
                modelSelectedMboundView2AndroidDimenTextSize22MboundView2AndroidDimenTextSize18 = ((modelSelected) ? (mboundView2.getResources().getDimension(R.dimen.text_size_22)) : (mboundView2.getResources().getDimension(R.dimen.text_size_18)));
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.mboundView0.setOnClickListener(mCallback27);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView0, modelSelectedMboundView0AndroidDrawableBgSelectedWorkoutDayMboundView0AndroidDrawableBgUnselectedFoodDay);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, modelName);
            this.mboundView1.setTextColor(modelSelectedMboundView1AndroidColorWhiteMboundView1AndroidColorTextPrimary);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextSize(this.mboundView1, modelSelectedMboundView1AndroidDimenTextSize15MboundView1AndroidDimenTextSize14);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, modelDayName);
            this.mboundView2.setTextColor(modelSelectedMboundView2AndroidColorWhiteMboundView2AndroidColorTextPrimary);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextSize(this.mboundView2, modelSelectedMboundView2AndroidDimenTextSize22MboundView2AndroidDimenTextSize18);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // model
        fit.lerchek.data.domain.uimodel.marathon.DayUIModel model = mModel;
        // callback
        fit.lerchek.ui.feature.marathon.workout.WorkoutCallback callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {



            callback.onSelectDayClick(model);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
        flag 3 (0x4L): model.selected ? @android:color/white : @android:color/text_primary
        flag 4 (0x5L): model.selected ? @android:color/white : @android:color/text_primary
        flag 5 (0x6L): model.selected ? @android:color/white : @android:color/text_primary
        flag 6 (0x7L): model.selected ? @android:color/white : @android:color/text_primary
        flag 7 (0x8L): model.selected ? @android:dimen/text_size_15 : @android:dimen/text_size_14
        flag 8 (0x9L): model.selected ? @android:dimen/text_size_15 : @android:dimen/text_size_14
        flag 9 (0xaL): model.selected ? @android:drawable/bg_selected_workout_day : @android:drawable/bg_unselected_food_day
        flag 10 (0xbL): model.selected ? @android:drawable/bg_selected_workout_day : @android:drawable/bg_unselected_food_day
        flag 11 (0xcL): model.selected ? @android:dimen/text_size_22 : @android:dimen/text_size_18
        flag 12 (0xdL): model.selected ? @android:dimen/text_size_22 : @android:dimen/text_size_18
    flag mapping end*/
    //end
}