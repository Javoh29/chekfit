// Generated by Dagger (https://dagger.dev).
package fit.lerchek.data.api;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.inject.Provider;
import retrofit2.Retrofit;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class UserRestImpl_Factory implements Factory<UserRestImpl> {
  private final Provider<Retrofit> retrofitProvider;

  public UserRestImpl_Factory(Provider<Retrofit> retrofitProvider) {
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public UserRestImpl get() {
    return newInstance(retrofitProvider.get());
  }

  public static UserRestImpl_Factory create(Provider<Retrofit> retrofitProvider) {
    return new UserRestImpl_Factory(retrofitProvider);
  }

  public static UserRestImpl newInstance(Retrofit retrofit) {
    return new UserRestImpl(retrofit);
  }
}
