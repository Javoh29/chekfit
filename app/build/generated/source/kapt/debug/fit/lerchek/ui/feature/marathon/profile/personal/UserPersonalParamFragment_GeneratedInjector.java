package fit.lerchek.ui.feature.marathon.profile.personal;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = UserPersonalParamFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface UserPersonalParamFragment_GeneratedInjector {
  void injectUserPersonalParamFragment(UserPersonalParamFragment userPersonalParamFragment);
}
