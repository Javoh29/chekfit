package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemVotingHeaderBindingImpl extends ItemVotingHeaderBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnChooseMeal, 5);
        sViewsWithIds.put(R.id.llText, 6);
        sViewsWithIds.put(R.id.tvLink, 7);
        sViewsWithIds.put(R.id.imgLike, 8);
        sViewsWithIds.put(R.id.llLikePower, 9);
        sViewsWithIds.put(R.id.tvLikesCount, 10);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemVotingHeaderBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private ItemVotingHeaderBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[5]
            , (androidx.appcompat.widget.AppCompatCheckBox) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[8]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.SeekBar) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            );
        this.cbAgree.setTag(null);
        this.errorWithdrawView.setTag(null);
        this.imgProfile.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.sbLikes.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.VotingHeaderUIModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.VotingHeaderUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.VotingHeaderUIModel model = mModel;
        boolean modelIsMale = false;
        int modelIsActiveViewGONEViewVISIBLE = 0;
        int modelAvailableVotes = 0;
        int modelMaxVotes = 0;
        android.graphics.drawable.Drawable modelIsMaleImgProfileAndroidDrawableIcProfileMaleImgProfileAndroidDrawableIcProfileFemale = null;
        boolean modelIsActive = false;
        boolean modelAgreed = false;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read model.isMale
                    modelIsMale = model.isMale();
                    // read model.availableVotes
                    modelAvailableVotes = model.getAvailableVotes();
                    // read model.maxVotes
                    modelMaxVotes = model.getMaxVotes();
                    // read model.isActive
                    modelIsActive = model.isActive();
                    // read model.agreed
                    modelAgreed = model.isAgreed();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelIsMale) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelIsActive) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                modelIsMaleImgProfileAndroidDrawableIcProfileMaleImgProfileAndroidDrawableIcProfileFemale = ((modelIsMale) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgProfile.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgProfile.getContext(), R.drawable.ic_profile_female)));
                // read model.isActive ? View.GONE : View.VISIBLE
                modelIsActiveViewGONEViewVISIBLE = ((modelIsActive) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.CompoundButtonBindingAdapter.setChecked(this.cbAgree, modelAgreed);
            this.errorWithdrawView.setVisibility(modelIsActiveViewGONEViewVISIBLE);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.imgProfile, modelIsMaleImgProfileAndroidDrawableIcProfileMaleImgProfileAndroidDrawableIcProfileFemale);
            androidx.databinding.adapters.SeekBarBindingAdapter.setProgress(this.sbLikes, modelAvailableVotes);
            this.sbLikes.setMax(modelMaxVotes);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): null
        flag 2 (0x3L): model.isActive ? View.GONE : View.VISIBLE
        flag 3 (0x4L): model.isActive ? View.GONE : View.VISIBLE
        flag 4 (0x5L): model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 5 (0x6L): model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
    flag mapping end*/
    //end
}