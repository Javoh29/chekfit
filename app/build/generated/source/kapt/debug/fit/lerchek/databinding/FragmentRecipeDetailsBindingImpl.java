package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentRecipeDetailsBindingImpl extends FragmentRecipeDetailsBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(11);
        sIncludes.setIncludes(1, 
            new String[] {"view_recipe_details"},
            new int[] {9},
            new int[] {fit.lerchek.R.layout.view_recipe_details});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 7);
        sViewsWithIds.put(R.id.nsvRecipe, 10);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.LinearLayout mboundView2;
    @NonNull
    private final androidx.cardview.widget.CardView mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView6;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback24;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentRecipeDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private FragmentRecipeDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (bindings[7] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[7]) : null
            , (androidx.appcompat.widget.AppCompatImageView) bindings[3]
            , (androidx.core.widget.NestedScrollView) bindings[10]
            , (android.widget.FrameLayout) bindings[6]
            , (fit.lerchek.databinding.ViewRecipeDetailsBinding) bindings[9]
            );
        this.btnFavorite.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.LinearLayout) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView4 = (androidx.cardview.widget.CardView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (bindings[8] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[8]) : null;
        this.progressBlock.setTag(null);
        setContainedBinding(this.viewRecipeDetails);
        setRootTag(root);
        // listeners
        mCallback24 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        viewRecipeDetails.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (viewRecipeDetails.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.food.RecipeDetailsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.food.RecipeDetailsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        viewRecipeDetails.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelRecipe((androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel>) object, fieldId);
            case 1 :
                return onChangeViewRecipeDetails((fit.lerchek.databinding.ViewRecipeDetailsBinding) object, fieldId);
            case 2 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelRecipe(androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel> ViewModelRecipe, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewRecipeDetails(fit.lerchek.databinding.ViewRecipeDetailsBinding ViewRecipeDetails, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelRecipeFavorite = false;
        fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel viewModelRecipeGetValue = null;
        androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel> viewModelRecipe = null;
        int viewModelRecipeDescriptionSpanLength = 0;
        boolean viewModelRecipeDescriptionSpanLengthInt0 = false;
        int viewModelRecipeDescriptionSpanLengthInt0ViewGONEViewVISIBLE = 0;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        android.text.SpannableStringBuilder viewModelRecipeDescriptionSpan = null;
        android.graphics.drawable.Drawable viewModelRecipeFavoriteBtnFavoriteAndroidDrawableIcLikeActiveBtnFavoriteAndroidDrawableIcLikeInactive = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.food.RecipeDetailsViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x1dL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.recipe
                        viewModelRecipe = viewModel.getRecipe();
                    }
                    updateLiveDataRegistration(0, viewModelRecipe);


                    if (viewModelRecipe != null) {
                        // read viewModel.recipe.getValue()
                        viewModelRecipeGetValue = viewModelRecipe.getValue();
                    }


                    if (viewModelRecipeGetValue != null) {
                        // read viewModel.recipe.getValue().favorite
                        viewModelRecipeFavorite = viewModelRecipeGetValue.isFavorite();
                        // read viewModel.recipe.getValue().descriptionSpan
                        viewModelRecipeDescriptionSpan = viewModelRecipeGetValue.getDescriptionSpan();
                    }
                if((dirtyFlags & 0x19L) != 0) {
                    if(viewModelRecipeFavorite) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read viewModel.recipe.getValue().favorite ? @android:drawable/ic_like_active : @android:drawable/ic_like_inactive
                    viewModelRecipeFavoriteBtnFavoriteAndroidDrawableIcLikeActiveBtnFavoriteAndroidDrawableIcLikeInactive = ((viewModelRecipeFavorite) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnFavorite.getContext(), R.drawable.ic_like_active)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnFavorite.getContext(), R.drawable.ic_like_inactive)));
                    if (viewModelRecipeDescriptionSpan != null) {
                        // read viewModel.recipe.getValue().descriptionSpan.length()
                        viewModelRecipeDescriptionSpanLength = viewModelRecipeDescriptionSpan.length();
                    }


                    // read viewModel.recipe.getValue().descriptionSpan.length() == 0
                    viewModelRecipeDescriptionSpanLengthInt0 = (viewModelRecipeDescriptionSpanLength) == (0);
                if((dirtyFlags & 0x19L) != 0) {
                    if(viewModelRecipeDescriptionSpanLengthInt0) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read viewModel.recipe.getValue().descriptionSpan.length() == 0 ? View.GONE : View.VISIBLE
                    viewModelRecipeDescriptionSpanLengthInt0ViewGONEViewVISIBLE = ((viewModelRecipeDescriptionSpanLengthInt0) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(2, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1cL) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.btnFavorite.setOnClickListener(mCallback24);
            this.viewRecipeDetails.setIsReplacement(false);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.btnFavorite, viewModelRecipeFavoriteBtnFavoriteAndroidDrawableIcLikeActiveBtnFavoriteAndroidDrawableIcLikeInactive);
            this.mboundView4.setVisibility(viewModelRecipeDescriptionSpanLengthInt0ViewGONEViewVISIBLE);
            fit.lerchek.ui.util.BindingAdaptersKt.setSpannableText(this.mboundView5, viewModelRecipeDescriptionSpan);
            this.viewRecipeDetails.setModel(viewModelRecipeGetValue);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        executeBindingsOn(viewRecipeDetails);
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        fit.lerchek.ui.feature.marathon.food.RecipeDetailsViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.favoriteChange();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.recipe
        flag 1 (0x2L): viewRecipeDetails
        flag 2 (0x3L): viewModel.isProgress()
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): viewModel.recipe.getValue().descriptionSpan.length() == 0 ? View.GONE : View.VISIBLE
        flag 6 (0x7L): viewModel.recipe.getValue().descriptionSpan.length() == 0 ? View.GONE : View.VISIBLE
        flag 7 (0x8L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 9 (0xaL): viewModel.recipe.getValue().favorite ? @android:drawable/ic_like_active : @android:drawable/ic_like_inactive
        flag 10 (0xbL): viewModel.recipe.getValue().favorite ? @android:drawable/ic_like_active : @android:drawable/ic_like_inactive
    flag mapping end*/
    //end
}