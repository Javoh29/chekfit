package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentCalorieCalculatorBindingImpl extends FragmentCalorieCalculatorBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 10);
        sViewsWithIds.put(R.id.nsvCaloriesCalculator, 12);
        sViewsWithIds.put(R.id.weight, 13);
        sViewsWithIds.put(R.id.yourCalorieTitle, 14);
        sViewsWithIds.put(R.id.cardCalorie, 15);
        sViewsWithIds.put(R.id.calorieTitle, 16);
        sViewsWithIds.put(R.id.calorieDescription, 17);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView4;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView7;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback22;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.age.get()
            //         is viewModel.age.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // viewModel.age.get()
            java.lang.String viewModelAgeGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.age != null
            boolean viewModelAgeJavaLangObjectNull = false;
            // viewModel.age
            androidx.databinding.ObservableField<java.lang.String> viewModelAge = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAge = viewModel.getAge();

                viewModelAgeJavaLangObjectNull = (viewModelAge) != (null);
                if (viewModelAgeJavaLangObjectNull) {




                    viewModelAge.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.weight.get()
            //         is viewModel.weight.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // viewModel.weight.get()
            java.lang.String viewModelWeightGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel viewModel = mViewModel;
            // viewModel.weight
            androidx.databinding.ObservableField<java.lang.String> viewModelWeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.weight != null
            boolean viewModelWeightJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelWeight = viewModel.getWeight();

                viewModelWeightJavaLangObjectNull = (viewModelWeight) != (null);
                if (viewModelWeightJavaLangObjectNull) {




                    viewModelWeight.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.height.get()
            //         is viewModel.height.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewModel.height.get()
            java.lang.String viewModelHeightGet = null;
            // viewModel.height != null
            boolean viewModelHeightJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel viewModel = mViewModel;
            // viewModel.height
            androidx.databinding.ObservableField<java.lang.String> viewModelHeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelHeight = viewModel.getHeight();

                viewModelHeightJavaLangObjectNull = (viewModelHeight) != (null);
                if (viewModelHeightJavaLangObjectNull) {




                    viewModelHeight.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentCalorieCalculatorBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentCalorieCalculatorBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (bindings[10] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[10]) : null
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[17]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[16]
            , (androidx.cardview.widget.CardView) bindings[15]
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.core.widget.NestedScrollView) bindings[12]
            , (android.widget.FrameLayout) bindings[9]
            , (androidx.recyclerview.widget.RecyclerView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            );
        this.btnCalculate.setTag(null);
        this.frameLayout.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatEditText) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView7 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView9 = (bindings[11] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[11]) : null;
        this.progressBlock.setTag(null);
        this.rvCalorie.setTag(null);
        this.yourCalorieValue.setTag(null);
        setRootTag(root);
        // listeners
        mCallback22 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelYourCalorie((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelAge((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelIsCalculate((androidx.databinding.ObservableBoolean) object, fieldId);
            case 3 :
                return onChangeViewModelWeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelHeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelYourCalorie(androidx.databinding.ObservableField<java.lang.String> ViewModelYourCalorie, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAge(androidx.databinding.ObservableField<java.lang.String> ViewModelAge, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsCalculate(androidx.databinding.ObservableBoolean ViewModelIsCalculate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelWeight(androidx.databinding.ObservableField<java.lang.String> ViewModelWeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHeight(androidx.databinding.ObservableField<java.lang.String> ViewModelHeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<java.lang.String> viewModelYourCalorie = null;
        int viewModelIsCalculateViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelAge = null;
        androidx.databinding.ObservableBoolean viewModelIsCalculate = null;
        java.lang.String viewModelHeightGet = null;
        int viewModelIsProgressViewGONEViewVISIBLE = 0;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelWeight = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelHeight = null;
        boolean viewModelIsCalculateGet = false;
        java.lang.String viewModelWeightGet = null;
        java.lang.String viewModelYourCalorieGet = null;
        java.lang.String yourCalorieValueAndroidStringTodayCcalViewModelYourCalorie = null;
        java.lang.String viewModelAgeGet = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0xffL) != 0) {


            if ((dirtyFlags & 0xc1L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.yourCalorie
                        viewModelYourCalorie = viewModel.getYourCalorie();
                    }
                    updateRegistration(0, viewModelYourCalorie);


                    if (viewModelYourCalorie != null) {
                        // read viewModel.yourCalorie.get()
                        viewModelYourCalorieGet = viewModelYourCalorie.get();
                    }


                    // read @android:string/today_ccal
                    yourCalorieValueAndroidStringTodayCcalViewModelYourCalorie = yourCalorieValue.getResources().getString(R.string.today_ccal, viewModelYourCalorieGet);
            }
            if ((dirtyFlags & 0xc2L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.age
                        viewModelAge = viewModel.getAge();
                    }
                    updateRegistration(1, viewModelAge);


                    if (viewModelAge != null) {
                        // read viewModel.age.get()
                        viewModelAgeGet = viewModelAge.get();
                    }
            }
            if ((dirtyFlags & 0xc4L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isCalculate()
                        viewModelIsCalculate = viewModel.isCalculate();
                    }
                    updateRegistration(2, viewModelIsCalculate);


                    if (viewModelIsCalculate != null) {
                        // read viewModel.isCalculate().get()
                        viewModelIsCalculateGet = viewModelIsCalculate.get();
                    }
                if((dirtyFlags & 0xc4L) != 0) {
                    if(viewModelIsCalculateGet) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
                    viewModelIsCalculateViewVISIBLEViewGONE = ((viewModelIsCalculateGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xc8L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.weight
                        viewModelWeight = viewModel.getWeight();
                    }
                    updateRegistration(3, viewModelWeight);


                    if (viewModelWeight != null) {
                        // read viewModel.weight.get()
                        viewModelWeightGet = viewModelWeight.get();
                    }
            }
            if ((dirtyFlags & 0xd0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.height
                        viewModelHeight = viewModel.getHeight();
                    }
                    updateRegistration(4, viewModelHeight);


                    if (viewModelHeight != null) {
                        // read viewModel.height.get()
                        viewModelHeightGet = viewModelHeight.get();
                    }
            }
            if ((dirtyFlags & 0xe0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(5, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0xe0L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x800L;
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                            dirtyFlags |= 0x1000L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.GONE : View.VISIBLE
                    viewModelIsProgressViewGONEViewVISIBLE = ((viewModelIsProgressGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x80L) != 0) {
            // api target 1

            this.btnCalculate.setOnClickListener(mCallback22);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
        }
        if ((dirtyFlags & 0xc2L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelAgeGet);
        }
        if ((dirtyFlags & 0xc8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelWeightGet);
        }
        if ((dirtyFlags & 0xd0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewModelHeightGet);
        }
        if ((dirtyFlags & 0xc4L) != 0) {
            // api target 1

            this.mboundView7.setVisibility(viewModelIsCalculateViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xe0L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
            this.rvCalorie.setVisibility(viewModelIsProgressViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0xc1L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.yourCalorieValue, yourCalorieValueAndroidStringTodayCcalViewModelYourCalorie);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.calculate();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.yourCalorie
        flag 1 (0x2L): viewModel.age
        flag 2 (0x3L): viewModel.isCalculate()
        flag 3 (0x4L): viewModel.weight
        flag 4 (0x5L): viewModel.height
        flag 5 (0x6L): viewModel.isProgress()
        flag 6 (0x7L): viewModel
        flag 7 (0x8L): null
        flag 8 (0x9L): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 9 (0xaL): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.isProgress().get() ? View.GONE : View.VISIBLE
        flag 11 (0xcL): viewModel.isProgress().get() ? View.GONE : View.VISIBLE
        flag 12 (0xdL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 13 (0xeL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}