package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemTestFooterBindingImpl extends ItemTestFooterBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.constraintLayoutInput, 5);
        sViewsWithIds.put(R.id.btnSave, 6);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener ageandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of model.textValue.get()
            //         is model.textValue.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(age);
            // localize variables for thread safety
            // model
            fit.lerchek.data.domain.uimodel.marathon.TestFooterItem model = mModel;
            // model != null
            boolean modelJavaLangObjectNull = false;
            // model.textValue.get()
            java.lang.String modelTextValueGet = null;
            // model.textValue != null
            boolean modelTextValueJavaLangObjectNull = false;
            // model.textValue
            androidx.databinding.ObservableField<java.lang.String> modelTextValue = null;



            modelJavaLangObjectNull = (model) != (null);
            if (modelJavaLangObjectNull) {


                modelTextValue = model.getTextValue();

                modelTextValueJavaLangObjectNull = (modelTextValue) != (null);
                if (modelTextValueJavaLangObjectNull) {




                    modelTextValue.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ItemTestFooterBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ItemTestFooterBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatEditText) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            , (android.widget.FrameLayout) bindings[5]
            );
        this.age.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.TestFooterItem) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.TestFooterItem Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeModelTextValue((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeModelTextValue(androidx.databinding.ObservableField<java.lang.String> ModelTextValue, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.TestFooterItem model = mModel;
        java.lang.String modelTitle = null;
        java.lang.String modelTextValueGet = null;
        boolean modelHintTextEmpty = false;
        java.lang.String modelDescription = null;
        int modelHintTextEmptyViewGONEViewVISIBLE = 0;
        java.lang.String modelFieldHint = null;
        androidx.databinding.ObservableField<java.lang.String> modelTextValue = null;
        java.lang.String modelHintText = null;

        if ((dirtyFlags & 0x7L) != 0) {


            if ((dirtyFlags & 0x6L) != 0) {

                    if (model != null) {
                        // read model.title
                        modelTitle = model.getTitle();
                        // read model.description
                        modelDescription = model.getDescription();
                        // read model.fieldHint
                        modelFieldHint = model.getFieldHint();
                        // read model.hintText
                        modelHintText = model.getHintText();
                    }


                    if (modelHintText != null) {
                        // read model.hintText.empty
                        modelHintTextEmpty = modelHintText.isEmpty();
                    }
                if((dirtyFlags & 0x6L) != 0) {
                    if(modelHintTextEmpty) {
                            dirtyFlags |= 0x10L;
                    }
                    else {
                            dirtyFlags |= 0x8L;
                    }
                }


                    // read model.hintText.empty ? View.GONE : View.VISIBLE
                    modelHintTextEmptyViewGONEViewVISIBLE = ((modelHintTextEmpty) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }

                if (model != null) {
                    // read model.textValue
                    modelTextValue = model.getTextValue();
                }
                updateRegistration(0, modelTextValue);


                if (modelTextValue != null) {
                    // read model.textValue.get()
                    modelTextValueGet = modelTextValue.get();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            this.age.setHint(modelFieldHint);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, modelTitle);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, modelDescription);
            this.mboundView4.setVisibility(modelHintTextEmptyViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, modelHintText);
        }
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.age, modelTextValueGet);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.age, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, ageandroidTextAttrChanged);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model.textValue
        flag 1 (0x2L): model
        flag 2 (0x3L): null
        flag 3 (0x4L): model.hintText.empty ? View.GONE : View.VISIBLE
        flag 4 (0x5L): model.hintText.empty ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}