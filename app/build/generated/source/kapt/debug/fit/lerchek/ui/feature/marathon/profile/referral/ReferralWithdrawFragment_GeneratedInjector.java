package fit.lerchek.ui.feature.marathon.profile.referral;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ReferralWithdrawFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ReferralWithdrawFragment_GeneratedInjector {
  void injectReferralWithdrawFragment(ReferralWithdrawFragment referralWithdrawFragment);
}
