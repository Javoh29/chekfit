package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemFaqItemBindingImpl extends ItemFaqItemBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemFaqItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private ItemFaqItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            );
        this.ivExpand.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.tvAnswer.setTag(null);
        this.tvQuestion.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.FaqItemUIModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.FaqItemUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.FaqItemUIModel model = mModel;
        boolean modelExpanded = false;
        java.lang.String modelQuestion = null;
        java.lang.String modelAnswer = null;
        int modelExpandedViewVISIBLEViewGONE = 0;
        int modelExpandedInt90Int270 = 0;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read model.expanded
                    modelExpanded = model.getExpanded();
                    // read model.question
                    modelQuestion = model.getQuestion();
                    // read model.answer
                    modelAnswer = model.getAnswer();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelExpanded) {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x4L;
                        dirtyFlags |= 0x10L;
                }
            }


                // read model.expanded ? View.VISIBLE : View.GONE
                modelExpandedViewVISIBLEViewGONE = ((modelExpanded) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read model.expanded ? 90 : 270
                modelExpandedInt90Int270 = ((modelExpanded) ? (90) : (270));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 11
            if(getBuildSdkInt() >= 11) {

                this.ivExpand.setRotation(modelExpandedInt90Int270);
            }
            // api target 1

            this.tvAnswer.setVisibility(modelExpandedViewVISIBLEViewGONE);
            fit.lerchek.ui.util.BindingAdaptersKt.setHtmlText(this.tvAnswer, modelAnswer);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvQuestion, modelQuestion);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): null
        flag 2 (0x3L): model.expanded ? View.VISIBLE : View.GONE
        flag 3 (0x4L): model.expanded ? View.VISIBLE : View.GONE
        flag 4 (0x5L): model.expanded ? 90 : 270
        flag 5 (0x6L): model.expanded ? 90 : 270
    flag mapping end*/
    //end
}