package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutProgressWeekResultBindingImpl extends LayoutProgressWeekResultBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.itemWeight, 6);
        sViewsWithIds.put(R.id.weekFull1, 7);
        sViewsWithIds.put(R.id.imgArrowWeek1, 8);
        sViewsWithIds.put(R.id.itemWaist, 9);
        sViewsWithIds.put(R.id.weekFull2, 10);
        sViewsWithIds.put(R.id.imgArrowWeek2, 11);
        sViewsWithIds.put(R.id.itemBreast, 12);
        sViewsWithIds.put(R.id.weekFull3, 13);
        sViewsWithIds.put(R.id.imgArrowWeek3, 14);
        sViewsWithIds.put(R.id.itemHips, 15);
        sViewsWithIds.put(R.id.weekFull4, 16);
        sViewsWithIds.put(R.id.imgArrowWeek4, 17);
        sViewsWithIds.put(R.id.itemReports, 18);
        sViewsWithIds.put(R.id.weekReports, 19);
        sViewsWithIds.put(R.id.imgArrowReports, 20);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutProgressWeekResultBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private LayoutProgressWeekResultBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.ImageView) bindings[20]
            , (android.widget.ImageView) bindings[8]
            , (android.widget.ImageView) bindings[11]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.ImageView) bindings[17]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[12]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[15]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[18]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[13]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[16]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[19]
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback Callback) {
        this.mCallback = Callback;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeModelSelectedWeek((androidx.databinding.ObservableInt) object, fieldId);
            case 1 :
                return onChangeModelProgress((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeModelSelectedWeek(androidx.databinding.ObservableInt ModelSelectedWeek, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelProgress(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> ModelProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel model = mModel;
        java.lang.String modelProgressMeasurementStrTypeWEIGHTModelSelectedWeek = null;
        boolean modelProgressIsEmptyMeasurementTypeBREASTModelSelectedWeek = false;
        java.lang.String modelProgressMeasurementStrTypeHIPSModelSelectedWeek = null;
        java.lang.String modelProgressIsEmptyMeasurementTypeWEIGHTModelSelectedWeekMboundView2AndroidStringProgressEmptyMboundView2AndroidStringKilogramsModelProgressMeasurementStrTypeWEIGHTModelSelectedWeek = null;
        androidx.databinding.ObservableInt modelSelectedWeek = null;
        java.lang.String mboundView5AndroidStringCentimetersModelProgressMeasurementStrTypeHIPSModelSelectedWeek = null;
        java.lang.String mboundView2AndroidStringKilogramsModelProgressMeasurementStrTypeWEIGHTModelSelectedWeek = null;
        java.lang.String modelProgressIsEmptyMeasurementTypeHIPSModelSelectedWeekMboundView5AndroidStringProgressEmptyMboundView5AndroidStringCentimetersModelProgressMeasurementStrTypeHIPSModelSelectedWeek = null;
        boolean modelSelectedWeekInt0 = false;
        boolean modelProgressIsEmptyMeasurementTypeWAISTModelSelectedWeek = false;
        java.lang.String modelProgressMeasurementStrTypeBREASTModelSelectedWeek = null;
        int modelSelectedWeekInt0ViewVISIBLEViewGONE = 0;
        int modelSelectedWeekGet = 0;
        java.lang.String modelProgressIsEmptyMeasurementTypeBREASTModelSelectedWeekMboundView4AndroidStringProgressEmptyMboundView4AndroidStringCentimetersModelProgressMeasurementStrTypeBREASTModelSelectedWeek = null;
        boolean modelProgressIsEmptyMeasurementTypeWEIGHTModelSelectedWeek = false;
        java.lang.String mboundView4AndroidStringCentimetersModelProgressMeasurementStrTypeBREASTModelSelectedWeek = null;
        boolean modelProgressIsEmptyMeasurementTypeHIPSModelSelectedWeek = false;
        fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel modelProgressGet = null;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> modelProgress = null;
        java.lang.String modelProgressWeekModelSelectedWeekName = null;
        java.lang.String modelProgressMeasurementStrTypeWAISTModelSelectedWeek = null;
        java.lang.String modelProgressIsEmptyMeasurementTypeWAISTModelSelectedWeekMboundView3AndroidStringProgressEmptyMboundView3AndroidStringCentimetersModelProgressMeasurementStrTypeWAISTModelSelectedWeek = null;
        java.lang.String mboundView3AndroidStringCentimetersModelProgressMeasurementStrTypeWAISTModelSelectedWeek = null;
        fit.lerchek.data.api.model.Week modelProgressWeekModelSelectedWeek = null;

        if ((dirtyFlags & 0x17L) != 0) {



                if (model != null) {
                    // read model.selectedWeek
                    modelSelectedWeek = model.getSelectedWeek();
                    // read model.progress
                    modelProgress = model.getProgress();
                }
                updateRegistration(0, modelSelectedWeek);
                updateRegistration(1, modelProgress);


                if (modelSelectedWeek != null) {
                    // read model.selectedWeek.get()
                    modelSelectedWeekGet = modelSelectedWeek.get();
                }
                if (modelProgress != null) {
                    // read model.progress.get()
                    modelProgressGet = modelProgress.get();
                }

            if ((dirtyFlags & 0x15L) != 0) {

                    // read model.selectedWeek.get() == 0
                    modelSelectedWeekInt0 = (modelSelectedWeekGet) == (0);
                if((dirtyFlags & 0x15L) != 0) {
                    if(modelSelectedWeekInt0) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read model.selectedWeek.get() == 0 ? View.VISIBLE : View.GONE
                    modelSelectedWeekInt0ViewVISIBLEViewGONE = ((modelSelectedWeekInt0) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }

                if (modelProgressGet != null) {
                    // read model.progress.get().isEmptyMeasurement(Type.BREAST, model.selectedWeek.get())
                    modelProgressIsEmptyMeasurementTypeBREASTModelSelectedWeek = modelProgressGet.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, modelSelectedWeekGet);
                    // read model.progress.get().isEmptyMeasurement(Type.WAIST, model.selectedWeek.get())
                    modelProgressIsEmptyMeasurementTypeWAISTModelSelectedWeek = modelProgressGet.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, modelSelectedWeekGet);
                    // read model.progress.get().isEmptyMeasurement(Type.WEIGHT, model.selectedWeek.get())
                    modelProgressIsEmptyMeasurementTypeWEIGHTModelSelectedWeek = modelProgressGet.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, modelSelectedWeekGet);
                    // read model.progress.get().isEmptyMeasurement(Type.HIPS, model.selectedWeek.get())
                    modelProgressIsEmptyMeasurementTypeHIPSModelSelectedWeek = modelProgressGet.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, modelSelectedWeekGet);
                    // read model.progress.get().week(model.selectedWeek.get())
                    modelProgressWeekModelSelectedWeek = modelProgressGet.week(modelSelectedWeekGet);
                }
            if((dirtyFlags & 0x17L) != 0) {
                if(modelProgressIsEmptyMeasurementTypeBREASTModelSelectedWeek) {
                        dirtyFlags |= 0x1000L;
                }
                else {
                        dirtyFlags |= 0x800L;
                }
            }
            if((dirtyFlags & 0x17L) != 0) {
                if(modelProgressIsEmptyMeasurementTypeWAISTModelSelectedWeek) {
                        dirtyFlags |= 0x4000L;
                }
                else {
                        dirtyFlags |= 0x2000L;
                }
            }
            if((dirtyFlags & 0x17L) != 0) {
                if(modelProgressIsEmptyMeasurementTypeWEIGHTModelSelectedWeek) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }
            if((dirtyFlags & 0x17L) != 0) {
                if(modelProgressIsEmptyMeasurementTypeHIPSModelSelectedWeek) {
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x80L;
                }
            }


                if (modelProgressWeekModelSelectedWeek != null) {
                    // read model.progress.get().week(model.selectedWeek.get()).name
                    modelProgressWeekModelSelectedWeekName = modelProgressWeekModelSelectedWeek.getName();
                }
        }
        // batch finished

        if ((dirtyFlags & 0x20L) != 0) {

                if (modelProgressGet != null) {
                    // read model.progress.get().measurementStr(Type.WEIGHT, model.selectedWeek.get())
                    modelProgressMeasurementStrTypeWEIGHTModelSelectedWeek = modelProgressGet.measurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, modelSelectedWeekGet);
                }


                // read @android:string/kilograms
                mboundView2AndroidStringKilogramsModelProgressMeasurementStrTypeWEIGHTModelSelectedWeek = mboundView2.getResources().getString(R.string.kilograms, modelProgressMeasurementStrTypeWEIGHTModelSelectedWeek);
        }
        if ((dirtyFlags & 0x80L) != 0) {

                if (modelProgressGet != null) {
                    // read model.progress.get().measurementStr(Type.HIPS, model.selectedWeek.get())
                    modelProgressMeasurementStrTypeHIPSModelSelectedWeek = modelProgressGet.measurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, modelSelectedWeekGet);
                }


                // read @android:string/centimeters
                mboundView5AndroidStringCentimetersModelProgressMeasurementStrTypeHIPSModelSelectedWeek = mboundView5.getResources().getString(R.string.centimeters, modelProgressMeasurementStrTypeHIPSModelSelectedWeek);
        }
        if ((dirtyFlags & 0x800L) != 0) {

                if (modelProgressGet != null) {
                    // read model.progress.get().measurementStr(Type.BREAST, model.selectedWeek.get())
                    modelProgressMeasurementStrTypeBREASTModelSelectedWeek = modelProgressGet.measurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, modelSelectedWeekGet);
                }


                // read @android:string/centimeters
                mboundView4AndroidStringCentimetersModelProgressMeasurementStrTypeBREASTModelSelectedWeek = mboundView4.getResources().getString(R.string.centimeters, modelProgressMeasurementStrTypeBREASTModelSelectedWeek);
        }
        if ((dirtyFlags & 0x2000L) != 0) {

                if (modelProgressGet != null) {
                    // read model.progress.get().measurementStr(Type.WAIST, model.selectedWeek.get())
                    modelProgressMeasurementStrTypeWAISTModelSelectedWeek = modelProgressGet.measurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, modelSelectedWeekGet);
                }


                // read @android:string/centimeters
                mboundView3AndroidStringCentimetersModelProgressMeasurementStrTypeWAISTModelSelectedWeek = mboundView3.getResources().getString(R.string.centimeters, modelProgressMeasurementStrTypeWAISTModelSelectedWeek);
        }

        if ((dirtyFlags & 0x17L) != 0) {

                // read model.progress.get().isEmptyMeasurement(Type.WEIGHT, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/kilograms
                modelProgressIsEmptyMeasurementTypeWEIGHTModelSelectedWeekMboundView2AndroidStringProgressEmptyMboundView2AndroidStringKilogramsModelProgressMeasurementStrTypeWEIGHTModelSelectedWeek = ((modelProgressIsEmptyMeasurementTypeWEIGHTModelSelectedWeek) ? (mboundView2.getResources().getString(R.string.progress_empty)) : (mboundView2AndroidStringKilogramsModelProgressMeasurementStrTypeWEIGHTModelSelectedWeek));
                // read model.progress.get().isEmptyMeasurement(Type.HIPS, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
                modelProgressIsEmptyMeasurementTypeHIPSModelSelectedWeekMboundView5AndroidStringProgressEmptyMboundView5AndroidStringCentimetersModelProgressMeasurementStrTypeHIPSModelSelectedWeek = ((modelProgressIsEmptyMeasurementTypeHIPSModelSelectedWeek) ? (mboundView5.getResources().getString(R.string.progress_empty)) : (mboundView5AndroidStringCentimetersModelProgressMeasurementStrTypeHIPSModelSelectedWeek));
                // read model.progress.get().isEmptyMeasurement(Type.BREAST, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
                modelProgressIsEmptyMeasurementTypeBREASTModelSelectedWeekMboundView4AndroidStringProgressEmptyMboundView4AndroidStringCentimetersModelProgressMeasurementStrTypeBREASTModelSelectedWeek = ((modelProgressIsEmptyMeasurementTypeBREASTModelSelectedWeek) ? (mboundView4.getResources().getString(R.string.progress_empty)) : (mboundView4AndroidStringCentimetersModelProgressMeasurementStrTypeBREASTModelSelectedWeek));
                // read model.progress.get().isEmptyMeasurement(Type.WAIST, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
                modelProgressIsEmptyMeasurementTypeWAISTModelSelectedWeekMboundView3AndroidStringProgressEmptyMboundView3AndroidStringCentimetersModelProgressMeasurementStrTypeWAISTModelSelectedWeek = ((modelProgressIsEmptyMeasurementTypeWAISTModelSelectedWeek) ? (mboundView3.getResources().getString(R.string.progress_empty)) : (mboundView3AndroidStringCentimetersModelProgressMeasurementStrTypeWAISTModelSelectedWeek));
        }
        // batch finished
        if ((dirtyFlags & 0x17L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, modelProgressWeekModelSelectedWeekName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, modelProgressIsEmptyMeasurementTypeWEIGHTModelSelectedWeekMboundView2AndroidStringProgressEmptyMboundView2AndroidStringKilogramsModelProgressMeasurementStrTypeWEIGHTModelSelectedWeek);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, modelProgressIsEmptyMeasurementTypeWAISTModelSelectedWeekMboundView3AndroidStringProgressEmptyMboundView3AndroidStringCentimetersModelProgressMeasurementStrTypeWAISTModelSelectedWeek);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, modelProgressIsEmptyMeasurementTypeBREASTModelSelectedWeekMboundView4AndroidStringProgressEmptyMboundView4AndroidStringCentimetersModelProgressMeasurementStrTypeBREASTModelSelectedWeek);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, modelProgressIsEmptyMeasurementTypeHIPSModelSelectedWeekMboundView5AndroidStringProgressEmptyMboundView5AndroidStringCentimetersModelProgressMeasurementStrTypeHIPSModelSelectedWeek);
        }
        if ((dirtyFlags & 0x15L) != 0) {
            // api target 1

            this.mboundView1.setVisibility(modelSelectedWeekInt0ViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model.selectedWeek
        flag 1 (0x2L): model.progress
        flag 2 (0x3L): model
        flag 3 (0x4L): callback
        flag 4 (0x5L): null
        flag 5 (0x6L): model.progress.get().isEmptyMeasurement(Type.WEIGHT, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/kilograms
        flag 6 (0x7L): model.progress.get().isEmptyMeasurement(Type.WEIGHT, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/kilograms
        flag 7 (0x8L): model.progress.get().isEmptyMeasurement(Type.HIPS, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
        flag 8 (0x9L): model.progress.get().isEmptyMeasurement(Type.HIPS, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
        flag 9 (0xaL): model.selectedWeek.get() == 0 ? View.VISIBLE : View.GONE
        flag 10 (0xbL): model.selectedWeek.get() == 0 ? View.VISIBLE : View.GONE
        flag 11 (0xcL): model.progress.get().isEmptyMeasurement(Type.BREAST, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
        flag 12 (0xdL): model.progress.get().isEmptyMeasurement(Type.BREAST, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
        flag 13 (0xeL): model.progress.get().isEmptyMeasurement(Type.WAIST, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
        flag 14 (0xfL): model.progress.get().isEmptyMeasurement(Type.WAIST, model.selectedWeek.get()) ? @android:string/progress_empty : @android:string/centimeters
    flag mapping end*/
    //end
}