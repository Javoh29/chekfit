package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMeasurementsBindingImpl extends FragmentMeasurementsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 29);
        sViewsWithIds.put(R.id.nsvMeasurements, 30);
        sViewsWithIds.put(R.id.measurementsBarrier, 31);
        sViewsWithIds.put(R.id.measurements1, 32);
        sViewsWithIds.put(R.id.measurements2, 33);
        sViewsWithIds.put(R.id.measurements3, 34);
        sViewsWithIds.put(R.id.measurements4, 35);
        sViewsWithIds.put(R.id.weeksBarrier, 36);
        sViewsWithIds.put(R.id.itemWeek1, 37);
        sViewsWithIds.put(R.id.imgArrowWeek1, 38);
        sViewsWithIds.put(R.id.itemWeek2, 39);
        sViewsWithIds.put(R.id.imgArrowWeek2, 40);
        sViewsWithIds.put(R.id.itemWeek3, 41);
        sViewsWithIds.put(R.id.imgArrowWeek3, 42);
        sViewsWithIds.put(R.id.itemWeek4, 43);
        sViewsWithIds.put(R.id.imgArrowWeek4, 44);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.view.View mboundView10;
    @NonNull
    private final android.view.View mboundView11;
    @NonNull
    private final android.view.View mboundView12;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView18;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView19;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView21;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView22;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView24;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView25;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView27;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView28;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    @NonNull
    private final android.view.View mboundView9;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMeasurementsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 45, sIncludes, sViewsWithIds));
    }
    private FragmentMeasurementsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (bindings[29] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[29]) : null
            , (android.widget.FrameLayout) bindings[1]
            , (android.widget.ImageView) bindings[38]
            , (android.widget.ImageView) bindings[40]
            , (android.widget.ImageView) bindings[42]
            , (android.widget.ImageView) bindings[44]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[37]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[39]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[41]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[43]
            , (android.widget.LinearLayout) bindings[32]
            , (android.widget.LinearLayout) bindings[33]
            , (android.widget.LinearLayout) bindings[34]
            , (android.widget.LinearLayout) bindings[35]
            , (androidx.constraintlayout.widget.Barrier) bindings[31]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (androidx.core.widget.NestedScrollView) bindings[30]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[13]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[15]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[16]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[17]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[20]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[23]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[26]
            , (androidx.constraintlayout.widget.Barrier) bindings[36]
            );
        this.frameLayout.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView10 = (android.view.View) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView11 = (android.view.View) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView12 = (android.view.View) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView18 = (androidx.appcompat.widget.AppCompatTextView) bindings[18];
        this.mboundView18.setTag(null);
        this.mboundView19 = (androidx.appcompat.widget.AppCompatTextView) bindings[19];
        this.mboundView19.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView21 = (androidx.appcompat.widget.AppCompatTextView) bindings[21];
        this.mboundView21.setTag(null);
        this.mboundView22 = (androidx.appcompat.widget.AppCompatTextView) bindings[22];
        this.mboundView22.setTag(null);
        this.mboundView24 = (androidx.appcompat.widget.AppCompatTextView) bindings[24];
        this.mboundView24.setTag(null);
        this.mboundView25 = (androidx.appcompat.widget.AppCompatTextView) bindings[25];
        this.mboundView25.setTag(null);
        this.mboundView27 = (androidx.appcompat.widget.AppCompatTextView) bindings[27];
        this.mboundView27.setTag(null);
        this.mboundView28 = (androidx.appcompat.widget.AppCompatTextView) bindings[28];
        this.mboundView28.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView9 = (android.view.View) bindings[9];
        this.mboundView9.setTag(null);
        this.measurementsMax.setTag(null);
        this.measurementsMid.setTag(null);
        this.measurementsMin.setTag(null);
        this.week1.setTag(null);
        this.week2.setTag(null);
        this.week3.setTag(null);
        this.week4.setTag(null);
        this.weekFull1.setTag(null);
        this.weekFull2.setTag(null);
        this.weekFull3.setTag(null);
        this.weekFull4.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.context == variableId) {
            setContext((android.content.Context) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.progress.MeasurementsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setContext(@Nullable android.content.Context Context) {
        this.mContext = Context;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.context);
        super.requestRebind();
    }
    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.progress.MeasurementsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelData((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelData(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel> ViewModelData, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelDataProgressMeasurementStrViewModelDataTypeInt1 = null;
        java.lang.String viewModelDataProgressStatWeeksFullDaysInt2 = null;
        java.lang.String viewModelDataUnitContextViewModelDataProgressLastMeasurementStrViewModelDataType = null;
        android.graphics.drawable.Drawable viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4MboundView12AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = null;
        java.lang.String viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1MboundView19AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt1 = null;
        fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel viewModelDataGet = null;
        java.lang.String viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3MboundView25AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt3 = null;
        java.lang.String viewModelDataProgressStatWeeksDaysInMonthInt4 = null;
        int viewModelDataTypeProgressRes = 0;
        java.lang.String viewModelDataUnitDiffContextViewModelDataProgressDiffMeasurementFloatViewModelDataType = null;
        java.lang.String viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent3ToString = null;
        java.lang.String viewModelDataProgressStatWeeksFullDaysInt4 = null;
        float viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt4 = 0f;
        java.lang.Float viewModelDataProgressDiffMeasurementFloatViewModelDataType = null;
        java.lang.String viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent1ToString = null;
        boolean viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1 = false;
        java.lang.Integer viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent1 = null;
        float viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt1 = 0f;
        android.graphics.drawable.Drawable viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2MboundView10AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = null;
        java.lang.String viewModelDataProgressLastMeasurementStrViewModelDataType = null;
        android.graphics.drawable.Drawable contextGetDrawableViewModelDataTypeProgressRes = null;
        java.lang.String viewModelDataProgressMeasurementStrViewModelDataTypeInt4 = null;
        boolean viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3 = false;
        float viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt2 = 0f;
        boolean viewModelDataShowFireViewModelDataProgressDiffMeasurementFloatViewModelDataType = false;
        java.lang.Float viewModelDataProgressMeasurementGraphViewModelDataTypeInt4 = null;
        java.lang.Float viewModelDataProgressMeasurementGraphViewModelDataTypeInt2 = null;
        java.lang.String viewModelDataProgressMeasurementStrViewModelDataTypeInt2 = null;
        int viewModelDataTypeTitleRes = 0;
        java.lang.String viewModelDataProgressStatWeeksDaysInMonthInt2 = null;
        java.lang.Integer viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent3 = null;
        boolean viewModelDataProgressIsEmptyDiffMeasurementViewModelDataType = false;
        float androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt2 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt1 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt3 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt4 = 0f;
        float viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt3 = 0f;
        fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type viewModelDataType = null;
        java.lang.String viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt1 = null;
        java.lang.String viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt2 = null;
        java.lang.String viewModelDataProgressStatWeeksFullDaysInt3 = null;
        kotlin.Triple<java.lang.Integer,java.lang.Integer,java.lang.Integer> viewModelDataProgressMeasurementsMinMidMaxViewModelDataType = null;
        java.lang.String viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt3 = null;
        java.lang.String viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt4 = null;
        fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel viewModelDataProgress = null;
        android.graphics.drawable.Drawable viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1MboundView9AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = null;
        android.graphics.drawable.Drawable viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3MboundView11AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = null;
        boolean viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2 = false;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel> viewModelData = null;
        int viewModelDataShowFireViewModelDataProgressDiffMeasurementFloatViewModelDataTypeViewVISIBLEViewGONE = 0;
        java.lang.String viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2MboundView22AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt2 = null;
        java.lang.Float viewModelDataProgressMeasurementGraphViewModelDataTypeInt1 = null;
        java.lang.String viewModelDataProgressStatWeeksDaysInMonthInt1 = null;
        int viewModelDataProgressIsEmptyDiffMeasurementViewModelDataTypeViewGONEViewVISIBLE = 0;
        boolean viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4 = false;
        android.content.Context context = mContext;
        java.lang.Integer viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent2 = null;
        java.lang.String viewModelDataProgressStatWeeksDaysInMonthInt3 = null;
        java.lang.String viewModelDataProgressMeasurementStrViewModelDataTypeInt3 = null;
        java.lang.String viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4MboundView28AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt4 = null;
        java.lang.String viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent2ToString = null;
        java.lang.String viewModelDataProgressStatWeeksFullDaysInt1 = null;
        fit.lerchek.ui.feature.marathon.progress.MeasurementsViewModel viewModel = mViewModel;
        java.lang.Float viewModelDataProgressMeasurementGraphViewModelDataTypeInt3 = null;

        if ((dirtyFlags & 0xfL) != 0) {



                if (viewModel != null) {
                    // read viewModel.data
                    viewModelData = viewModel.getData();
                }
                updateRegistration(0, viewModelData);


                if (viewModelData != null) {
                    // read viewModel.data.get()
                    viewModelDataGet = viewModelData.get();
                }


                if (viewModelDataGet != null) {
                    // read viewModel.data.get().type
                    viewModelDataType = viewModelDataGet.getType();
                    // read viewModel.data.get().progress
                    viewModelDataProgress = viewModelDataGet.getProgress();
                }

            if ((dirtyFlags & 0xdL) != 0) {

                    if (viewModelDataType != null) {
                        // read viewModel.data.get().type.titleRes
                        viewModelDataTypeTitleRes = viewModelDataType.getTitleRes();
                    }
                    if (viewModelDataProgress != null) {
                        // read viewModel.data.get().progress.statWeeksFullDays(2)
                        viewModelDataProgressStatWeeksFullDaysInt2 = viewModelDataProgress.statWeeksFullDays(2);
                        // read viewModel.data.get().progress.statWeeksDaysInMonth(4)
                        viewModelDataProgressStatWeeksDaysInMonthInt4 = viewModelDataProgress.statWeeksDaysInMonth(4);
                        // read viewModel.data.get().progress.statWeeksFullDays(4)
                        viewModelDataProgressStatWeeksFullDaysInt4 = viewModelDataProgress.statWeeksFullDays(4);
                        // read viewModel.data.get().progress.statWeeksDaysInMonth(2)
                        viewModelDataProgressStatWeeksDaysInMonthInt2 = viewModelDataProgress.statWeeksDaysInMonth(2);
                        // read viewModel.data.get().progress.isEmptyDiffMeasurement(viewModel.data.get().type)
                        viewModelDataProgressIsEmptyDiffMeasurementViewModelDataType = viewModelDataProgress.isEmptyDiffMeasurement(viewModelDataType);
                        // read viewModel.data.get().progress.statWeeksFullDays(3)
                        viewModelDataProgressStatWeeksFullDaysInt3 = viewModelDataProgress.statWeeksFullDays(3);
                        // read viewModel.data.get().progress.measurementsMinMidMax(viewModel.data.get().type)
                        viewModelDataProgressMeasurementsMinMidMaxViewModelDataType = viewModelDataProgress.measurementsMinMidMax(viewModelDataType);
                        // read viewModel.data.get().progress.statWeeksDaysInMonth(1)
                        viewModelDataProgressStatWeeksDaysInMonthInt1 = viewModelDataProgress.statWeeksDaysInMonth(1);
                        // read viewModel.data.get().progress.statWeeksDaysInMonth(3)
                        viewModelDataProgressStatWeeksDaysInMonthInt3 = viewModelDataProgress.statWeeksDaysInMonth(3);
                        // read viewModel.data.get().progress.statWeeksFullDays(1)
                        viewModelDataProgressStatWeeksFullDaysInt1 = viewModelDataProgress.statWeeksFullDays(1);
                    }
                if((dirtyFlags & 0xdL) != 0) {
                    if(viewModelDataProgressIsEmptyDiffMeasurementViewModelDataType) {
                            dirtyFlags |= 0x20000000L;
                    }
                    else {
                            dirtyFlags |= 0x10000000L;
                    }
                }


                    // read viewModel.data.get().progress.isEmptyDiffMeasurement(viewModel.data.get().type) ? View.GONE : View.VISIBLE
                    viewModelDataProgressIsEmptyDiffMeasurementViewModelDataTypeViewGONEViewVISIBLE = ((viewModelDataProgressIsEmptyDiffMeasurementViewModelDataType) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    if (viewModelDataProgressMeasurementsMinMidMaxViewModelDataType != null) {
                        // read viewModel.data.get().progress.measurementsMinMidMax(viewModel.data.get().type).component1()
                        viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent1 = viewModelDataProgressMeasurementsMinMidMaxViewModelDataType.component1();
                        // read viewModel.data.get().progress.measurementsMinMidMax(viewModel.data.get().type).component3()
                        viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent3 = viewModelDataProgressMeasurementsMinMidMaxViewModelDataType.component3();
                        // read viewModel.data.get().progress.measurementsMinMidMax(viewModel.data.get().type).component2()
                        viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent2 = viewModelDataProgressMeasurementsMinMidMaxViewModelDataType.component2();
                    }


                    if (viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent1 != null) {
                        // read viewModel.data.get().progress.measurementsMinMidMax(viewModel.data.get().type).component1().toString()
                        viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent1ToString = viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent1.toString();
                    }
                    if (viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent3 != null) {
                        // read viewModel.data.get().progress.measurementsMinMidMax(viewModel.data.get().type).component3().toString()
                        viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent3ToString = viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent3.toString();
                    }
                    if (viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent2 != null) {
                        // read viewModel.data.get().progress.measurementsMinMidMax(viewModel.data.get().type).component2().toString()
                        viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent2ToString = viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent2.toString();
                    }
            }

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.diffMeasurementFloat(viewModel.data.get().type)
                    viewModelDataProgressDiffMeasurementFloatViewModelDataType = viewModelDataProgress.diffMeasurementFloat(viewModelDataType);
                    // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1)
                    viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1 = viewModelDataProgress.isEmptyMeasurement(viewModelDataType, 1);
                    // read viewModel.data.get().progress.lastMeasurementStr(viewModel.data.get().type)
                    viewModelDataProgressLastMeasurementStrViewModelDataType = viewModelDataProgress.lastMeasurementStr(viewModelDataType);
                    // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3)
                    viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3 = viewModelDataProgress.isEmptyMeasurement(viewModelDataType, 3);
                    // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2)
                    viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2 = viewModelDataProgress.isEmptyMeasurement(viewModelDataType, 2);
                    // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4)
                    viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4 = viewModelDataProgress.isEmptyMeasurement(viewModelDataType, 4);
                }
            if((dirtyFlags & 0xfL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1) {
                        dirtyFlags |= 0x80L;
                        dirtyFlags |= 0x200000L;
                }
                else {
                        dirtyFlags |= 0x40L;
                        dirtyFlags |= 0x100000L;
                }
            }
            if((dirtyFlags & 0xdL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1) {
                        dirtyFlags |= 0x2000L;
                }
                else {
                        dirtyFlags |= 0x1000L;
                }
            }
            if((dirtyFlags & 0xfL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3) {
                        dirtyFlags |= 0x200L;
                        dirtyFlags |= 0x800000L;
                }
                else {
                        dirtyFlags |= 0x100L;
                        dirtyFlags |= 0x400000L;
                }
            }
            if((dirtyFlags & 0xdL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3) {
                        dirtyFlags |= 0x80000L;
                }
                else {
                        dirtyFlags |= 0x40000L;
                }
            }
            if((dirtyFlags & 0xfL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2) {
                        dirtyFlags |= 0x8000L;
                        dirtyFlags |= 0x8000000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                        dirtyFlags |= 0x4000000L;
                }
            }
            if((dirtyFlags & 0xdL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2) {
                        dirtyFlags |= 0x20000L;
                }
                else {
                        dirtyFlags |= 0x10000L;
                }
            }
            if((dirtyFlags & 0xfL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4) {
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x80000000L;
                }
                else {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x40000000L;
                }
            }
            if((dirtyFlags & 0xdL) != 0) {
                if(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4) {
                        dirtyFlags |= 0x800L;
                }
                else {
                        dirtyFlags |= 0x400L;
                }
            }


                if (viewModelDataGet != null) {
                    // read viewModel.data.get().unitDiff(context, viewModel.data.get().progress.diffMeasurementFloat(viewModel.data.get().type))
                    viewModelDataUnitDiffContextViewModelDataProgressDiffMeasurementFloatViewModelDataType = viewModelDataGet.unitDiff(context, viewModelDataProgressDiffMeasurementFloatViewModelDataType);
                    // read viewModel.data.get().unit(context, viewModel.data.get().progress.lastMeasurementStr(viewModel.data.get().type))
                    viewModelDataUnitContextViewModelDataProgressLastMeasurementStrViewModelDataType = viewModelDataGet.unit(context, viewModelDataProgressLastMeasurementStrViewModelDataType);
                }
            if ((dirtyFlags & 0xdL) != 0) {

                    if (viewModelDataGet != null) {
                        // read viewModel.data.get().showFire(viewModel.data.get().progress.diffMeasurementFloat(viewModel.data.get().type))
                        viewModelDataShowFireViewModelDataProgressDiffMeasurementFloatViewModelDataType = viewModelDataGet.showFire(viewModelDataProgressDiffMeasurementFloatViewModelDataType);
                    }
                if((dirtyFlags & 0xdL) != 0) {
                    if(viewModelDataShowFireViewModelDataProgressDiffMeasurementFloatViewModelDataType) {
                            dirtyFlags |= 0x2000000L;
                    }
                    else {
                            dirtyFlags |= 0x1000000L;
                    }
                }


                    // read viewModel.data.get().showFire(viewModel.data.get().progress.diffMeasurementFloat(viewModel.data.get().type)) ? View.VISIBLE : View.GONE
                    viewModelDataShowFireViewModelDataProgressDiffMeasurementFloatViewModelDataTypeViewVISIBLEViewGONE = ((viewModelDataShowFireViewModelDataProgressDiffMeasurementFloatViewModelDataType) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished

        if ((dirtyFlags & 0x40L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 1)
                    viewModelDataProgressMeasurementStrViewModelDataTypeInt1 = viewModelDataProgress.measurementStr(viewModelDataType, 1);
                }


                if (viewModelDataGet != null) {
                    // read viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 1))
                    viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt1 = viewModelDataGet.unit(context, viewModelDataProgressMeasurementStrViewModelDataTypeInt1);
                }
        }
        if ((dirtyFlags & 0x504010L) != 0) {

                if (viewModelDataType != null) {
                    // read viewModel.data.get().type.progressRes
                    viewModelDataTypeProgressRes = viewModelDataType.getProgressRes();
                }


                if (context != null) {
                    // read context.getDrawable(viewModel.data.get().type.progressRes)
                    contextGetDrawableViewModelDataTypeProgressRes = context.getDrawable(viewModelDataTypeProgressRes);
                }
        }
        if ((dirtyFlags & 0x40000000L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 4)
                    viewModelDataProgressMeasurementStrViewModelDataTypeInt4 = viewModelDataProgress.measurementStr(viewModelDataType, 4);
                }


                if (viewModelDataGet != null) {
                    // read viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 4))
                    viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt4 = viewModelDataGet.unit(context, viewModelDataProgressMeasurementStrViewModelDataTypeInt4);
                }
        }
        if ((dirtyFlags & 0x400L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 4)
                    viewModelDataProgressMeasurementGraphViewModelDataTypeInt4 = viewModelDataProgress.measurementGraph(viewModelDataType, 4);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 4))
                androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt4 = androidx.databinding.ViewDataBinding.safeUnbox(viewModelDataProgressMeasurementGraphViewModelDataTypeInt4);
        }
        if ((dirtyFlags & 0x10000L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 2)
                    viewModelDataProgressMeasurementGraphViewModelDataTypeInt2 = viewModelDataProgress.measurementGraph(viewModelDataType, 2);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 2))
                androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt2 = androidx.databinding.ViewDataBinding.safeUnbox(viewModelDataProgressMeasurementGraphViewModelDataTypeInt2);
        }
        if ((dirtyFlags & 0x4000000L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 2)
                    viewModelDataProgressMeasurementStrViewModelDataTypeInt2 = viewModelDataProgress.measurementStr(viewModelDataType, 2);
                }


                if (viewModelDataGet != null) {
                    // read viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 2))
                    viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt2 = viewModelDataGet.unit(context, viewModelDataProgressMeasurementStrViewModelDataTypeInt2);
                }
        }
        if ((dirtyFlags & 0x1000L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 1)
                    viewModelDataProgressMeasurementGraphViewModelDataTypeInt1 = viewModelDataProgress.measurementGraph(viewModelDataType, 1);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 1))
                androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt1 = androidx.databinding.ViewDataBinding.safeUnbox(viewModelDataProgressMeasurementGraphViewModelDataTypeInt1);
        }
        if ((dirtyFlags & 0x100L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 3)
                    viewModelDataProgressMeasurementStrViewModelDataTypeInt3 = viewModelDataProgress.measurementStr(viewModelDataType, 3);
                }


                if (viewModelDataGet != null) {
                    // read viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 3))
                    viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt3 = viewModelDataGet.unit(context, viewModelDataProgressMeasurementStrViewModelDataTypeInt3);
                }
        }
        if ((dirtyFlags & 0x40000L) != 0) {

                if (viewModelDataProgress != null) {
                    // read viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 3)
                    viewModelDataProgressMeasurementGraphViewModelDataTypeInt3 = viewModelDataProgress.measurementGraph(viewModelDataType, 3);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 3))
                androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt3 = androidx.databinding.ViewDataBinding.safeUnbox(viewModelDataProgressMeasurementGraphViewModelDataTypeInt3);
        }

        if ((dirtyFlags & 0xfL) != 0) {

                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4MboundView12AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView12.getContext(), R.drawable.bg_progress_empty)) : (contextGetDrawableViewModelDataTypeProgressRes));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 1))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1MboundView19AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt1 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1) ? (mboundView19.getResources().getString(R.string.progress_empty)) : (viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt1));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 3))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3MboundView25AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt3 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3) ? (mboundView25.getResources().getString(R.string.progress_empty)) : (viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt3));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2MboundView10AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView10.getContext(), R.drawable.bg_progress_empty)) : (contextGetDrawableViewModelDataTypeProgressRes));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1MboundView9AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView9.getContext(), R.drawable.bg_progress_empty)) : (contextGetDrawableViewModelDataTypeProgressRes));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3MboundView11AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView11.getContext(), R.drawable.bg_progress_empty)) : (contextGetDrawableViewModelDataTypeProgressRes));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 2))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2MboundView22AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt2 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2) ? (mboundView22.getResources().getString(R.string.progress_empty)) : (viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt2));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 4))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4MboundView28AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt4 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4) ? (mboundView28.getResources().getString(R.string.progress_empty)) : (viewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt4));
        }
        if ((dirtyFlags & 0xdL) != 0) {

                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 4))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt4 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt4));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 1))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt1 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt1));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 2))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt2 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt2));
                // read viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 3))
                viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt3 = ((viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxViewModelDataProgressMeasurementGraphViewModelDataTypeInt3));
        }
        // batch finished
        if ((dirtyFlags & 0xfL) != 0) {
            // api target 1

            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView10, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2MboundView10AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView11, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3MboundView11AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView12, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4MboundView12AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView19, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1MboundView19AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt1);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView22, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2MboundView22AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt2);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView25, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3MboundView25AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt3);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView28, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4MboundView28AndroidStringProgressEmptyViewModelDataUnitContextViewModelDataProgressMeasurementStrViewModelDataTypeInt4);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelDataUnitContextViewModelDataProgressLastMeasurementStrViewModelDataType);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, viewModelDataUnitDiffContextViewModelDataProgressDiffMeasurementFloatViewModelDataType);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView9, viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1MboundView9AndroidDrawableBgProgressEmptyContextGetDrawableViewModelDataTypeProgressRes);
        }
        if ((dirtyFlags & 0xdL) != 0) {
            // api target 11
            if(getBuildSdkInt() >= 11) {

                this.mboundView10.setScaleY(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt2Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt2);
                this.mboundView11.setScaleY(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt3Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt3);
                this.mboundView12.setScaleY(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt4Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt4);
                this.mboundView9.setScaleY(viewModelDataProgressIsEmptyMeasurementViewModelDataTypeInt1Float05FViewModelDataProgressMeasurementGraphViewModelDataTypeInt1);
            }
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView18, viewModelDataProgressStatWeeksFullDaysInt1);
            this.mboundView2.setText(viewModelDataTypeTitleRes);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView21, viewModelDataProgressStatWeeksFullDaysInt2);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView24, viewModelDataProgressStatWeeksFullDaysInt3);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView27, viewModelDataProgressStatWeeksFullDaysInt4);
            this.mboundView4.setVisibility(viewModelDataShowFireViewModelDataProgressDiffMeasurementFloatViewModelDataTypeViewVISIBLEViewGONE);
            this.mboundView5.setVisibility(viewModelDataProgressIsEmptyDiffMeasurementViewModelDataTypeViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.measurementsMax, viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent3ToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.measurementsMid, viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent2ToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.measurementsMin, viewModelDataProgressMeasurementsMinMidMaxViewModelDataTypeComponent1ToString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.week1, viewModelDataProgressStatWeeksDaysInMonthInt1);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.week2, viewModelDataProgressStatWeeksDaysInMonthInt2);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.week3, viewModelDataProgressStatWeeksDaysInMonthInt3);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.week4, viewModelDataProgressStatWeeksDaysInMonthInt4);
        }
        if ((dirtyFlags & 0x8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.weekFull1, weekFull1.getResources().getString(R.string.progress_week, 1));
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.weekFull2, weekFull2.getResources().getString(R.string.progress_week, 2));
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.weekFull3, weekFull3.getResources().getString(R.string.progress_week, 3));
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.weekFull4, weekFull4.getResources().getString(R.string.progress_week, 4));
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.data
        flag 1 (0x2L): context
        flag 2 (0x3L): viewModel
        flag 3 (0x4L): null
        flag 4 (0x5L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 5 (0x6L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 6 (0x7L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 1))
        flag 7 (0x8L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 1))
        flag 8 (0x9L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 3))
        flag 9 (0xaL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 3))
        flag 10 (0xbL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 4))
        flag 11 (0xcL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 4))
        flag 12 (0xdL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 1))
        flag 13 (0xeL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 1))
        flag 14 (0xfL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 15 (0x10L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 16 (0x11L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 2))
        flag 17 (0x12L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 2))
        flag 18 (0x13L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 3))
        flag 19 (0x14L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(viewModel.data.get().progress.measurementGraph(viewModel.data.get().type, 3))
        flag 20 (0x15L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 21 (0x16L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 1) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 22 (0x17L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 23 (0x18L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 3) ? @android:drawable/bg_progress_empty : context.getDrawable(viewModel.data.get().type.progressRes)
        flag 24 (0x19L): viewModel.data.get().showFire(viewModel.data.get().progress.diffMeasurementFloat(viewModel.data.get().type)) ? View.VISIBLE : View.GONE
        flag 25 (0x1aL): viewModel.data.get().showFire(viewModel.data.get().progress.diffMeasurementFloat(viewModel.data.get().type)) ? View.VISIBLE : View.GONE
        flag 26 (0x1bL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 2))
        flag 27 (0x1cL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 2) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 2))
        flag 28 (0x1dL): viewModel.data.get().progress.isEmptyDiffMeasurement(viewModel.data.get().type) ? View.GONE : View.VISIBLE
        flag 29 (0x1eL): viewModel.data.get().progress.isEmptyDiffMeasurement(viewModel.data.get().type) ? View.GONE : View.VISIBLE
        flag 30 (0x1fL): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 4))
        flag 31 (0x20L): viewModel.data.get().progress.isEmptyMeasurement(viewModel.data.get().type, 4) ? @android:string/progress_empty : viewModel.data.get().unit(context, viewModel.data.get().progress.measurementStr(viewModel.data.get().type, 4))
    flag mapping end*/
    //end
}