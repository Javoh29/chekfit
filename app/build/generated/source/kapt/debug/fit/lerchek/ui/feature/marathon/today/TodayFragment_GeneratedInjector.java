package fit.lerchek.ui.feature.marathon.today;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = TodayFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface TodayFragment_GeneratedInjector {
  void injectTodayFragment(TodayFragment todayFragment);
}
