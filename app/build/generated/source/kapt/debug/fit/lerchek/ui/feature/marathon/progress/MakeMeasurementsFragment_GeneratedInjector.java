package fit.lerchek.ui.feature.marathon.progress;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = MakeMeasurementsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface MakeMeasurementsFragment_GeneratedInjector {
  void injectMakeMeasurementsFragment(MakeMeasurementsFragment makeMeasurementsFragment);
}
