package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMoreBindingImpl extends FragmentMoreBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.svMore, 2);
        sViewsWithIds.put(R.id.itemNutrients, 3);
        sViewsWithIds.put(R.id.itemRecipes, 4);
        sViewsWithIds.put(R.id.itemYam, 5);
        sViewsWithIds.put(R.id.itemProductsCheckList, 6);
        sViewsWithIds.put(R.id.itemHealthChekUp, 7);
        sViewsWithIds.put(R.id.itemCalorieCalculator, 8);
        sViewsWithIds.put(R.id.itemFatCalculator, 9);
        sViewsWithIds.put(R.id.itemBMIcalculator, 10);
        sViewsWithIds.put(R.id.itemWaterTracker, 11);
        sViewsWithIds.put(R.id.itemSleepTracker, 12);
        sViewsWithIds.put(R.id.itemSelfCareTracker, 13);
        sViewsWithIds.put(R.id.itemCardio, 14);
        sViewsWithIds.put(R.id.itemVacuum, 15);
        sViewsWithIds.put(R.id.itemStepTracker, 16);
        sViewsWithIds.put(R.id.itemCardioTest, 17);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMoreBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentMoreBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[10]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[8]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[14]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[17]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[9]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[7]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[3]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[6]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[4]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[13]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[1]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[12]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[16]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[15]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[11]
            , (fit.lerchek.ui.widget.ClickableTextView) bindings[5]
            , (android.widget.ScrollView) bindings[2]
            );
        this.itemSkinCare.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.more.MoreViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.more.MoreViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelProfileIsMale((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelProfileIsMale(androidx.databinding.ObservableBoolean ViewModelProfileIsMale, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableBoolean viewModelProfileIsMale = null;
        boolean viewModelProfileIsMaleGet = false;
        fit.lerchek.ui.feature.marathon.more.MoreViewModel viewModel = mViewModel;
        int viewModelProfileIsMaleViewGONEViewVISIBLE = 0;

        if ((dirtyFlags & 0x7L) != 0) {



                if (viewModel != null) {
                    // read viewModel.profileIsMale
                    viewModelProfileIsMale = viewModel.getProfileIsMale();
                }
                updateRegistration(0, viewModelProfileIsMale);


                if (viewModelProfileIsMale != null) {
                    // read viewModel.profileIsMale.get()
                    viewModelProfileIsMaleGet = viewModelProfileIsMale.get();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(viewModelProfileIsMaleGet) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read viewModel.profileIsMale.get() ? View.GONE : View.VISIBLE
                viewModelProfileIsMaleViewGONEViewVISIBLE = ((viewModelProfileIsMaleGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            this.itemSkinCare.setVisibility(viewModelProfileIsMaleViewGONEViewVISIBLE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.profileIsMale
        flag 1 (0x2L): viewModel
        flag 2 (0x3L): null
        flag 3 (0x4L): viewModel.profileIsMale.get() ? View.GONE : View.VISIBLE
        flag 4 (0x5L): viewModel.profileIsMale.get() ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}