// Generated by Dagger (https://dagger.dev).
package fit.lerchek.ui.feature.marathon.food;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import fit.lerchek.data.repository.user.UserRepository;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class RecipeDetailsViewModel_Factory implements Factory<RecipeDetailsViewModel> {
  private final Provider<UserRepository> userRepositoryProvider;

  public RecipeDetailsViewModel_Factory(Provider<UserRepository> userRepositoryProvider) {
    this.userRepositoryProvider = userRepositoryProvider;
  }

  @Override
  public RecipeDetailsViewModel get() {
    return newInstance(userRepositoryProvider.get());
  }

  public static RecipeDetailsViewModel_Factory create(
      Provider<UserRepository> userRepositoryProvider) {
    return new RecipeDetailsViewModel_Factory(userRepositoryProvider);
  }

  public static RecipeDetailsViewModel newInstance(UserRepository userRepository) {
    return new RecipeDetailsViewModel(userRepository);
  }
}
