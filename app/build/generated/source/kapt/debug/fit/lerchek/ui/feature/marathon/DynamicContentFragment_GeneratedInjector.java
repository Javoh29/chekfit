package fit.lerchek.ui.feature.marathon;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = DynamicContentFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface DynamicContentFragment_GeneratedInjector {
  void injectDynamicContentFragment(DynamicContentFragment dynamicContentFragment);
}
