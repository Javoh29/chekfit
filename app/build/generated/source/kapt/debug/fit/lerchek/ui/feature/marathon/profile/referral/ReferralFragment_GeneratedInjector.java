package fit.lerchek.ui.feature.marathon.profile.referral;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ReferralFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ReferralFragment_GeneratedInjector {
  void injectReferralFragment(ReferralFragment referralFragment);
}
