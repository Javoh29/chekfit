package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemTestOptionItemBindingImpl extends ItemTestOptionItemBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.ivCheck, 3);
        sViewsWithIds.put(R.id.llText, 4);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemTestOptionItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ItemTestOptionItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[3]
            , (android.widget.LinearLayout) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            );
        this.btnOption.setTag(null);
        this.tvDescription.setTag(null);
        this.tvTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.TestItemOptionUIModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.TestItemOptionUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.TestItemOptionUIModel model = mModel;
        int modelDescriptionEmptyViewGONEViewVISIBLE = 0;
        java.lang.String modelTitle = null;
        int modelTitleEmptyViewGONEViewVISIBLE = 0;
        boolean modelDescriptionEmpty = false;
        java.lang.String modelDescription = null;
        boolean modelTitleEmpty = false;

        if ((dirtyFlags & 0x3L) != 0) {



                if (model != null) {
                    // read model.title
                    modelTitle = model.getTitle();
                    // read model.description
                    modelDescription = model.getDescription();
                }


                if (modelTitle != null) {
                    // read model.title.empty
                    modelTitleEmpty = modelTitle.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelTitleEmpty) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
                if (modelDescription != null) {
                    // read model.description.empty
                    modelDescriptionEmpty = modelDescription.isEmpty();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(modelDescriptionEmpty) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read model.title.empty ? View.GONE : View.VISIBLE
                modelTitleEmptyViewGONEViewVISIBLE = ((modelTitleEmpty) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                // read model.description.empty ? View.GONE : View.VISIBLE
                modelDescriptionEmptyViewGONEViewVISIBLE = ((modelDescriptionEmpty) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.tvDescription.setVisibility(modelDescriptionEmptyViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDescription, modelDescription);
            this.tvTitle.setVisibility(modelTitleEmptyViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTitle, modelTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): null
        flag 2 (0x3L): model.description.empty ? View.GONE : View.VISIBLE
        flag 3 (0x4L): model.description.empty ? View.GONE : View.VISIBLE
        flag 4 (0x5L): model.title.empty ? View.GONE : View.VISIBLE
        flag 5 (0x6L): model.title.empty ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}