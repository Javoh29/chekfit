// Generated by Dagger (https://dagger.dev).
package fit.lerchek.ui.feature.marathon.profile.personal;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PersonalParamViewModel_HiltModules_KeyModule_ProvideFactory implements Factory<String> {
  @Override
  public String get() {
    return provide();
  }

  public static PersonalParamViewModel_HiltModules_KeyModule_ProvideFactory create() {
    return InstanceHolder.INSTANCE;
  }

  public static String provide() {
    return Preconditions.checkNotNullFromProvides(PersonalParamViewModel_HiltModules.KeyModule.provide());
  }

  private static final class InstanceHolder {
    private static final PersonalParamViewModel_HiltModules_KeyModule_ProvideFactory INSTANCE = new PersonalParamViewModel_HiltModules_KeyModule_ProvideFactory();
  }
}
