package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentUserPersonalBodyParametersBindingImpl extends FragmentUserPersonalBodyParametersBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.appCompatTextView3, 8);
        sViewsWithIds.put(R.id.message, 9);
        sViewsWithIds.put(R.id.save, 10);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener breastVolumeandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalParams.get().breast.get()
            //         is viewModel.personalParams.get().breast.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(breastVolume);
            // localize variables for thread safety
            // viewModel.personalParams.get().breast
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsGetBreast = null;
            // viewModel.personalParams.get() != null
            boolean viewModelPersonalParamsGetJavaLangObjectNull = false;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalParams
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel> viewModelPersonalParams = null;
            // viewModel.personalParams.get().breast
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsBreast = null;
            // viewModel.personalParams.get().breast.get()
            java.lang.String viewModelPersonalParamsBreastGet = null;
            // viewModel.personalParams != null
            boolean viewModelPersonalParamsJavaLangObjectNull = false;
            // viewModel.personalParams.get().breast != null
            boolean viewModelPersonalParamsGetBreastJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel viewModel = mViewModel;
            // viewModel.personalParams.get()
            fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel viewModelPersonalParamsGet = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalParams = viewModel.getPersonalParams();

                viewModelPersonalParamsJavaLangObjectNull = (viewModelPersonalParams) != (null);
                if (viewModelPersonalParamsJavaLangObjectNull) {


                    viewModelPersonalParamsGet = viewModelPersonalParams.get();

                    viewModelPersonalParamsGetJavaLangObjectNull = (viewModelPersonalParamsGet) != (null);
                    if (viewModelPersonalParamsGetJavaLangObjectNull) {


                        viewModelPersonalParamsGetBreast = viewModelPersonalParamsGet.getBreast();

                        viewModelPersonalParamsGetBreastJavaLangObjectNull = (viewModelPersonalParamsGetBreast) != (null);
                        if (viewModelPersonalParamsGetBreastJavaLangObjectNull) {




                            viewModelPersonalParamsGetBreast.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener heightandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalParams.get().height.get()
            //         is viewModel.personalParams.get().height.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(height);
            // localize variables for thread safety
            // viewModel.personalParams.get().height.get()
            java.lang.String viewModelPersonalParamsHeightGet = null;
            // viewModel.personalParams.get().height
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsGetHeight = null;
            // viewModel.personalParams.get() != null
            boolean viewModelPersonalParamsGetJavaLangObjectNull = false;
            // viewModel.personalParams.get().height
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsHeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalParams.get().height != null
            boolean viewModelPersonalParamsGetHeightJavaLangObjectNull = false;
            // viewModel.personalParams
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel> viewModelPersonalParams = null;
            // viewModel.personalParams != null
            boolean viewModelPersonalParamsJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel viewModel = mViewModel;
            // viewModel.personalParams.get()
            fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel viewModelPersonalParamsGet = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalParams = viewModel.getPersonalParams();

                viewModelPersonalParamsJavaLangObjectNull = (viewModelPersonalParams) != (null);
                if (viewModelPersonalParamsJavaLangObjectNull) {


                    viewModelPersonalParamsGet = viewModelPersonalParams.get();

                    viewModelPersonalParamsGetJavaLangObjectNull = (viewModelPersonalParamsGet) != (null);
                    if (viewModelPersonalParamsGetJavaLangObjectNull) {


                        viewModelPersonalParamsGetHeight = viewModelPersonalParamsGet.getHeight();

                        viewModelPersonalParamsGetHeightJavaLangObjectNull = (viewModelPersonalParamsGetHeight) != (null);
                        if (viewModelPersonalParamsGetHeightJavaLangObjectNull) {




                            viewModelPersonalParamsGetHeight.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener hipsandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalParams.get().hips.get()
            //         is viewModel.personalParams.get().hips.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(hips);
            // localize variables for thread safety
            // viewModel.personalParams.get() != null
            boolean viewModelPersonalParamsGetJavaLangObjectNull = false;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalParams
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel> viewModelPersonalParams = null;
            // viewModel.personalParams.get().hips
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsHips = null;
            // viewModel.personalParams != null
            boolean viewModelPersonalParamsJavaLangObjectNull = false;
            // viewModel.personalParams.get().hips.get()
            java.lang.String viewModelPersonalParamsHipsGet = null;
            // viewModel.personalParams.get().hips
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsGetHips = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel viewModel = mViewModel;
            // viewModel.personalParams.get().hips != null
            boolean viewModelPersonalParamsGetHipsJavaLangObjectNull = false;
            // viewModel.personalParams.get()
            fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel viewModelPersonalParamsGet = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalParams = viewModel.getPersonalParams();

                viewModelPersonalParamsJavaLangObjectNull = (viewModelPersonalParams) != (null);
                if (viewModelPersonalParamsJavaLangObjectNull) {


                    viewModelPersonalParamsGet = viewModelPersonalParams.get();

                    viewModelPersonalParamsGetJavaLangObjectNull = (viewModelPersonalParamsGet) != (null);
                    if (viewModelPersonalParamsGetJavaLangObjectNull) {


                        viewModelPersonalParamsGetHips = viewModelPersonalParamsGet.getHips();

                        viewModelPersonalParamsGetHipsJavaLangObjectNull = (viewModelPersonalParamsGetHips) != (null);
                        if (viewModelPersonalParamsGetHipsJavaLangObjectNull) {




                            viewModelPersonalParamsGetHips.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener waistandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalParams.get().waist.get()
            //         is viewModel.personalParams.get().waist.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(waist);
            // localize variables for thread safety
            // viewModel.personalParams.get() != null
            boolean viewModelPersonalParamsGetJavaLangObjectNull = false;
            // viewModel.personalParams.get().waist != null
            boolean viewModelPersonalParamsGetWaistJavaLangObjectNull = false;
            // viewModel.personalParams.get().waist
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsGetWaist = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalParams
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel> viewModelPersonalParams = null;
            // viewModel.personalParams != null
            boolean viewModelPersonalParamsJavaLangObjectNull = false;
            // viewModel.personalParams.get().waist.get()
            java.lang.String viewModelPersonalParamsWaistGet = null;
            // viewModel.personalParams.get().waist
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsWaist = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel viewModel = mViewModel;
            // viewModel.personalParams.get()
            fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel viewModelPersonalParamsGet = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalParams = viewModel.getPersonalParams();

                viewModelPersonalParamsJavaLangObjectNull = (viewModelPersonalParams) != (null);
                if (viewModelPersonalParamsJavaLangObjectNull) {


                    viewModelPersonalParamsGet = viewModelPersonalParams.get();

                    viewModelPersonalParamsGetJavaLangObjectNull = (viewModelPersonalParamsGet) != (null);
                    if (viewModelPersonalParamsGetJavaLangObjectNull) {


                        viewModelPersonalParamsGetWaist = viewModelPersonalParamsGet.getWaist();

                        viewModelPersonalParamsGetWaistJavaLangObjectNull = (viewModelPersonalParamsGetWaist) != (null);
                        if (viewModelPersonalParamsGetWaistJavaLangObjectNull) {




                            viewModelPersonalParamsGetWaist.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener weightandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.personalParams.get().weight.get()
            //         is viewModel.personalParams.get().weight.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(weight);
            // localize variables for thread safety
            // viewModel.personalParams.get().weight
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsGetWeight = null;
            // viewModel.personalParams.get().weight != null
            boolean viewModelPersonalParamsGetWeightJavaLangObjectNull = false;
            // viewModel.personalParams.get() != null
            boolean viewModelPersonalParamsGetJavaLangObjectNull = false;
            // viewModel.personalParams.get().weight.get()
            java.lang.String viewModelPersonalParamsWeightGet = null;
            // viewModel.personalParams.get().weight
            androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsWeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.personalParams
            androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel> viewModelPersonalParams = null;
            // viewModel.personalParams != null
            boolean viewModelPersonalParamsJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel viewModel = mViewModel;
            // viewModel.personalParams.get()
            fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel viewModelPersonalParamsGet = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelPersonalParams = viewModel.getPersonalParams();

                viewModelPersonalParamsJavaLangObjectNull = (viewModelPersonalParams) != (null);
                if (viewModelPersonalParamsJavaLangObjectNull) {


                    viewModelPersonalParamsGet = viewModelPersonalParams.get();

                    viewModelPersonalParamsGetJavaLangObjectNull = (viewModelPersonalParamsGet) != (null);
                    if (viewModelPersonalParamsGetJavaLangObjectNull) {


                        viewModelPersonalParamsGetWeight = viewModelPersonalParamsGet.getWeight();

                        viewModelPersonalParamsGetWeightJavaLangObjectNull = (viewModelPersonalParamsGetWeight) != (null);
                        if (viewModelPersonalParamsGetWeightJavaLangObjectNull) {




                            viewModelPersonalParamsGetWeight.set(((java.lang.String) (callbackArg_0)));
                        }
                    }
                }
            }
        }
    };

    public FragmentUserPersonalBodyParametersBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private FragmentUserPersonalBodyParametersBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[3]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[2]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[5]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[9]
            , (android.widget.FrameLayout) bindings[6]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[4]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[1]
            );
        this.breastVolume.setTag(null);
        this.height.setTag(null);
        this.hips.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView6 = (bindings[7] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[7]) : null;
        this.progressBlock.setTag(null);
        this.waist.setTag(null);
        this.weight.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelPersonalParamsWeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelPersonalParamsHeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelPersonalParams((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel>) object, fieldId);
            case 3 :
                return onChangeViewModelPersonalParamsBreast((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelPersonalParamsHips((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 5 :
                return onChangeViewModelPersonalParamsWaist((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelPersonalParamsWeight(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalParamsWeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalParamsHeight(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalParamsHeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalParams(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel> ViewModelPersonalParams, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalParamsBreast(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalParamsBreast, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalParamsHips(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalParamsHips, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelPersonalParamsWaist(androidx.databinding.ObservableField<java.lang.String> ViewModelPersonalParamsWaist, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelPersonalParamsWeightGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsWeight = null;
        java.lang.String viewModelPersonalParamsWaistGet = null;
        java.lang.String viewModelPersonalParamsHipsGet = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        java.lang.String viewModelPersonalParamsHeightGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsHeight = null;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel> viewModelPersonalParams = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsBreast = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsHips = null;
        java.lang.String viewModelPersonalParamsBreastGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelPersonalParamsWaist = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;
        fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel viewModelPersonalParamsGet = null;

        if ((dirtyFlags & 0x1ffL) != 0) {


            if ((dirtyFlags & 0x1bfL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.personalParams
                        viewModelPersonalParams = viewModel.getPersonalParams();
                    }
                    updateRegistration(2, viewModelPersonalParams);


                    if (viewModelPersonalParams != null) {
                        // read viewModel.personalParams.get()
                        viewModelPersonalParamsGet = viewModelPersonalParams.get();
                    }

                if ((dirtyFlags & 0x185L) != 0) {

                        if (viewModelPersonalParamsGet != null) {
                            // read viewModel.personalParams.get().weight
                            viewModelPersonalParamsWeight = viewModelPersonalParamsGet.getWeight();
                        }
                        updateRegistration(0, viewModelPersonalParamsWeight);


                        if (viewModelPersonalParamsWeight != null) {
                            // read viewModel.personalParams.get().weight.get()
                            viewModelPersonalParamsWeightGet = viewModelPersonalParamsWeight.get();
                        }
                }
                if ((dirtyFlags & 0x186L) != 0) {

                        if (viewModelPersonalParamsGet != null) {
                            // read viewModel.personalParams.get().height
                            viewModelPersonalParamsHeight = viewModelPersonalParamsGet.getHeight();
                        }
                        updateRegistration(1, viewModelPersonalParamsHeight);


                        if (viewModelPersonalParamsHeight != null) {
                            // read viewModel.personalParams.get().height.get()
                            viewModelPersonalParamsHeightGet = viewModelPersonalParamsHeight.get();
                        }
                }
                if ((dirtyFlags & 0x18cL) != 0) {

                        if (viewModelPersonalParamsGet != null) {
                            // read viewModel.personalParams.get().breast
                            viewModelPersonalParamsBreast = viewModelPersonalParamsGet.getBreast();
                        }
                        updateRegistration(3, viewModelPersonalParamsBreast);


                        if (viewModelPersonalParamsBreast != null) {
                            // read viewModel.personalParams.get().breast.get()
                            viewModelPersonalParamsBreastGet = viewModelPersonalParamsBreast.get();
                        }
                }
                if ((dirtyFlags & 0x194L) != 0) {

                        if (viewModelPersonalParamsGet != null) {
                            // read viewModel.personalParams.get().hips
                            viewModelPersonalParamsHips = viewModelPersonalParamsGet.getHips();
                        }
                        updateRegistration(4, viewModelPersonalParamsHips);


                        if (viewModelPersonalParamsHips != null) {
                            // read viewModel.personalParams.get().hips.get()
                            viewModelPersonalParamsHipsGet = viewModelPersonalParamsHips.get();
                        }
                }
                if ((dirtyFlags & 0x1a4L) != 0) {

                        if (viewModelPersonalParamsGet != null) {
                            // read viewModel.personalParams.get().waist
                            viewModelPersonalParamsWaist = viewModelPersonalParamsGet.getWaist();
                        }
                        updateRegistration(5, viewModelPersonalParamsWaist);


                        if (viewModelPersonalParamsWaist != null) {
                            // read viewModel.personalParams.get().waist.get()
                            viewModelPersonalParamsWaistGet = viewModelPersonalParamsWaist.get();
                        }
                }
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(6, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1c0L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x18cL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.breastVolume, viewModelPersonalParamsBreastGet);
        }
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.breastVolume, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, breastVolumeandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.height, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, heightandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.hips, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, hipsandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.waist, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, waistandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.weight, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, weightandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x186L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.height, viewModelPersonalParamsHeightGet);
        }
        if ((dirtyFlags & 0x194L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.hips, viewModelPersonalParamsHipsGet);
        }
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x1a4L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.waist, viewModelPersonalParamsWaistGet);
        }
        if ((dirtyFlags & 0x185L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.weight, viewModelPersonalParamsWeightGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.personalParams.get().weight
        flag 1 (0x2L): viewModel.personalParams.get().height
        flag 2 (0x3L): viewModel.personalParams
        flag 3 (0x4L): viewModel.personalParams.get().breast
        flag 4 (0x5L): viewModel.personalParams.get().hips
        flag 5 (0x6L): viewModel.personalParams.get().waist
        flag 6 (0x7L): viewModel.isProgress()
        flag 7 (0x8L): viewModel
        flag 8 (0x9L): null
        flag 9 (0xaL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}