package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemMarathonsListBindingImpl extends ItemMarathonsListBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.imgMarathon, 5);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback12;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemMarathonsListBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ItemMarathonsListBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            );
        this.btnSignIn.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        setRootTag(root);
        // listeners
        mCallback12 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.MarathonUIModel) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.MarathonsAdapter.SelectMarathonCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.MarathonUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.MarathonsAdapter.SelectMarathonCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.MarathonUIModel model = mModel;
        java.lang.String modelStartDateCharCharChar = null;
        java.lang.String modelStartDate = null;
        java.lang.String modelStartDateCharChar = null;
        java.lang.String modelName = null;
        java.lang.String modelStartDateCharCharCharModelEndDate = null;
        java.lang.String modelStartDateChar = null;
        java.lang.String modelEndDate = null;
        java.lang.String modelStatus = null;
        fit.lerchek.ui.feature.marathon.MarathonsAdapter.SelectMarathonCallback callback = mCallback;

        if ((dirtyFlags & 0x5L) != 0) {



                if (model != null) {
                    // read model.startDate
                    modelStartDate = model.getStartDate();
                    // read model.name
                    modelName = model.getName();
                    // read model.endDate
                    modelEndDate = model.getEndDate();
                    // read model.status
                    modelStatus = model.getStatus();
                }


                // read (model.startDate) + (' ')
                modelStartDateChar = (modelStartDate) + (' ');


                // read ((model.startDate) + (' ')) + ('-')
                modelStartDateCharChar = (modelStartDateChar) + ('-');


                // read (((model.startDate) + (' ')) + ('-')) + (' ')
                modelStartDateCharCharChar = (modelStartDateCharChar) + (' ');


                // read ((((model.startDate) + (' ')) + ('-')) + (' ')) + (model.endDate)
                modelStartDateCharCharCharModelEndDate = (modelStartDateCharCharChar) + (modelEndDate);
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.btnSignIn.setOnClickListener(mCallback12);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, modelName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, modelStartDateCharCharCharModelEndDate);
            fit.lerchek.ui.feature.marathon.MarathonsAdapterKt.setMarathonStatus(this.mboundView4, modelStatus);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // model
        fit.lerchek.data.domain.uimodel.marathon.MarathonUIModel model = mModel;
        // callback
        fit.lerchek.ui.feature.marathon.MarathonsAdapter.SelectMarathonCallback callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {



            callback.onSelectItem(model);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}