package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProfileBindingImpl extends FragmentProfileBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.profileScrollView, 10);
        sViewsWithIds.put(R.id.btnPersonalData, 11);
        sViewsWithIds.put(R.id.imgPersonalData, 12);
        sViewsWithIds.put(R.id.imgBodyParams, 13);
        sViewsWithIds.put(R.id.btnTest, 14);
        sViewsWithIds.put(R.id.imgTest, 15);
        sViewsWithIds.put(R.id.viewNightMode, 16);
        sViewsWithIds.put(R.id.themeSwitch, 17);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView6;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView8;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (androidx.cardview.widget.CardView) bindings[7]
            , (androidx.cardview.widget.CardView) bindings[11]
            , (androidx.cardview.widget.CardView) bindings[14]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[13]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[12]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[15]
            , (android.widget.ScrollView) bindings[10]
            , (android.widget.FrameLayout) bindings[8]
            , (androidx.appcompat.widget.SwitchCompat) bindings[17]
            , (androidx.cardview.widget.CardView) bindings[16]
            );
        this.btnBodyParams.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView8 = (bindings[9] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[9]) : null;
        this.progressBlock.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.profile.ProfileViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.profile.ProfileViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelProfileDetails((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileDetailsUIModel>) object, fieldId);
            case 1 :
                return onChangeViewModelIsMarathonLocalItemsVisible((androidx.databinding.ObservableBoolean) object, fieldId);
            case 2 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelProfileDetails(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileDetailsUIModel> ViewModelProfileDetails, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsMarathonLocalItemsVisible(androidx.databinding.ObservableBoolean ViewModelIsMarathonLocalItemsVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.graphics.drawable.Drawable viewModelProfileDetailsIsMaleMboundView1AndroidDrawableIcProfileMaleMboundView1AndroidDrawableIcProfileFemale = null;
        boolean viewModelIsMarathonLocalItemsVisibleGet = false;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ProfileDetailsUIModel> viewModelProfileDetails = null;
        int viewModelIsMarathonLocalItemsVisibleViewVISIBLEViewGONE = 0;
        java.lang.String viewModelProfileDetailsGetPrizesString = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        java.lang.String viewModelProfileDetailsName = null;
        java.lang.String viewModelProfileDetailsNick = null;
        androidx.databinding.ObservableBoolean viewModelIsMarathonLocalItemsVisible = null;
        boolean viewModelProfileDetailsIsMale = false;
        java.lang.String viewModelProfileDetailsGetActiveDaysString = null;
        fit.lerchek.data.domain.uimodel.profile.ProfileDetailsUIModel viewModelProfileDetailsGet = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.profile.ProfileViewModel viewModel = mViewModel;
        java.lang.String viewModelProfileDetailsGetMarathonCountString = null;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.profileDetails
                        viewModelProfileDetails = viewModel.getProfileDetails();
                    }
                    updateRegistration(0, viewModelProfileDetails);


                    if (viewModelProfileDetails != null) {
                        // read viewModel.profileDetails.get()
                        viewModelProfileDetailsGet = viewModelProfileDetails.get();
                    }


                    if (viewModelProfileDetailsGet != null) {
                        // read viewModel.profileDetails.get().getPrizesString()
                        viewModelProfileDetailsGetPrizesString = viewModelProfileDetailsGet.getPrizesString();
                        // read viewModel.profileDetails.get().name
                        viewModelProfileDetailsName = viewModelProfileDetailsGet.getName();
                        // read viewModel.profileDetails.get().nick
                        viewModelProfileDetailsNick = viewModelProfileDetailsGet.getNick();
                        // read viewModel.profileDetails.get().isMale
                        viewModelProfileDetailsIsMale = viewModelProfileDetailsGet.isMale();
                        // read viewModel.profileDetails.get().getActiveDaysString()
                        viewModelProfileDetailsGetActiveDaysString = viewModelProfileDetailsGet.getActiveDaysString();
                        // read viewModel.profileDetails.get().getMarathonCountString()
                        viewModelProfileDetailsGetMarathonCountString = viewModelProfileDetailsGet.getMarathonCountString();
                    }
                if((dirtyFlags & 0x19L) != 0) {
                    if(viewModelProfileDetailsIsMale) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read viewModel.profileDetails.get().isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                    viewModelProfileDetailsIsMaleMboundView1AndroidDrawableIcProfileMaleMboundView1AndroidDrawableIcProfileFemale = ((viewModelProfileDetailsIsMale) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView1.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView1.getContext(), R.drawable.ic_profile_female)));
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isMarathonLocalItemsVisible()
                        viewModelIsMarathonLocalItemsVisible = viewModel.isMarathonLocalItemsVisible();
                    }
                    updateRegistration(1, viewModelIsMarathonLocalItemsVisible);


                    if (viewModelIsMarathonLocalItemsVisible != null) {
                        // read viewModel.isMarathonLocalItemsVisible().get()
                        viewModelIsMarathonLocalItemsVisibleGet = viewModelIsMarathonLocalItemsVisible.get();
                    }
                if((dirtyFlags & 0x1aL) != 0) {
                    if(viewModelIsMarathonLocalItemsVisibleGet) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read viewModel.isMarathonLocalItemsVisible().get() ? View.VISIBLE : View.GONE
                    viewModelIsMarathonLocalItemsVisibleViewVISIBLEViewGONE = ((viewModelIsMarathonLocalItemsVisibleGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(2, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1cL) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            this.btnBodyParams.setVisibility(viewModelIsMarathonLocalItemsVisibleViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.mboundView1, viewModelProfileDetailsIsMaleMboundView1AndroidDrawableIcProfileMaleMboundView1AndroidDrawableIcProfileFemale);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelProfileDetailsName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelProfileDetailsNick);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewModelProfileDetailsGetMarathonCountString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, viewModelProfileDetailsGetActiveDaysString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, viewModelProfileDetailsGetPrizesString);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.profileDetails
        flag 1 (0x2L): viewModel.isMarathonLocalItemsVisible()
        flag 2 (0x3L): viewModel.isProgress()
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): viewModel.profileDetails.get().isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 6 (0x7L): viewModel.profileDetails.get().isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 7 (0x8L): viewModel.isMarathonLocalItemsVisible().get() ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.isMarathonLocalItemsVisible().get() ? View.VISIBLE : View.GONE
        flag 9 (0xaL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}