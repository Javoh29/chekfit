package fit.lerchek.ui.feature.marathon.food;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = FoodFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface FoodFragment_GeneratedInjector {
  void injectFoodFragment(FoodFragment foodFragment);
}
