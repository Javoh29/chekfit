package fit.lerchek.ui.feature.marathon.more.cardiotest;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = CardioTestFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface CardioTestFragment_GeneratedInjector {
  void injectCardioTestFragment(CardioTestFragment cardioTestFragment);
}
