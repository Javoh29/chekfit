package fit.lerchek.ui.feature.marathon.progress;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ProgressFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ProgressFragment_GeneratedInjector {
  void injectProgressFragment(ProgressFragment progressFragment);
}
