package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityChatBindingImpl extends ActivityChatBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.container, 10);
        sViewsWithIds.put(R.id.toolbar_delimeter, 11);
        sViewsWithIds.put(R.id.clInput, 12);
        sViewsWithIds.put(R.id.toolbar_view, 13);
        sViewsWithIds.put(R.id.btn_back, 14);
        sViewsWithIds.put(R.id.tv_support_title, 15);
        sViewsWithIds.put(R.id.rvChat, 16);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView1;
    @NonNull
    private final android.view.View mboundView3;
    @NonNull
    private final android.view.View mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener inputMessageandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.message.get()
            //         is viewModel.message.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(inputMessage);
            // localize variables for thread safety
            // viewModel.message
            androidx.databinding.ObservableField<java.lang.String> viewModelMessage = null;
            // viewModel
            fit.lerchek.ui.feature.chat.ChatActivityViewModel viewModel = mViewModel;
            // viewModel.message != null
            boolean viewModelMessageJavaLangObjectNull = false;
            // viewModel.message.get()
            java.lang.String viewModelMessageGet = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelMessage = viewModel.getMessage();

                viewModelMessageJavaLangObjectNull = (viewModelMessage) != (null);
                if (viewModelMessageJavaLangObjectNull) {




                    viewModelMessage.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public ActivityChatBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private ActivityChatBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            , (android.widget.FrameLayout) bindings[14]
            , (android.widget.FrameLayout) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[12]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[10]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[5]
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.recyclerview.widget.RecyclerView) bindings[16]
            , (android.view.View) bindings[11]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[15]
            );
        this.blockedFooter.setTag(null);
        this.btnSend.setTag(null);
        this.icProfile.setTag(null);
        this.inputMessage.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (bindings[9] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[9]) : null;
        this.mboundView3 = (android.view.View) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.view.View) bindings[4];
        this.mboundView4.setTag(null);
        this.progressBlock.setTag(null);
        this.tvSupportName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.chat.ChatActivityViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.chat.ChatActivityViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelMessage((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeViewModelProfileIsMale((androidx.databinding.ObservableBoolean) object, fieldId);
            case 2 :
                return onChangeViewModelCanEdit((androidx.databinding.ObservableBoolean) object, fieldId);
            case 3 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelMessage(androidx.databinding.ObservableField<java.lang.String> ViewModelMessage, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelProfileIsMale(androidx.databinding.ObservableBoolean ViewModelProfileIsMale, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelCanEdit(androidx.databinding.ObservableBoolean ViewModelCanEdit, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelCanEditGet = false;
        androidx.databinding.ObservableField<java.lang.String> viewModelMessage = null;
        androidx.databinding.ObservableBoolean viewModelProfileIsMale = null;
        int viewModelCanEditViewGONEViewVISIBLE = 0;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        java.lang.String viewModelProfileIsMaleTvSupportNameAndroidStringChatSupportArtemchekTvSupportNameAndroidStringChatSupportLercheck = null;
        java.lang.String viewModelMessageGet = null;
        android.graphics.drawable.Drawable viewModelProfileIsMaleIcProfileAndroidDrawableIcChatMaleIcProfileAndroidDrawableIcChatFemale = null;
        boolean viewModelProfileIsMaleGet = false;
        androidx.databinding.ObservableBoolean viewModelCanEdit = null;
        int viewModelCanEditViewVISIBLEViewGONE = 0;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.chat.ChatActivityViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.message
                        viewModelMessage = viewModel.getMessage();
                    }
                    updateRegistration(0, viewModelMessage);


                    if (viewModelMessage != null) {
                        // read viewModel.message.get()
                        viewModelMessageGet = viewModelMessage.get();
                    }
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.profileIsMale
                        viewModelProfileIsMale = viewModel.getProfileIsMale();
                    }
                    updateRegistration(1, viewModelProfileIsMale);


                    if (viewModelProfileIsMale != null) {
                        // read viewModel.profileIsMale.get()
                        viewModelProfileIsMaleGet = viewModelProfileIsMale.get();
                    }
                if((dirtyFlags & 0x32L) != 0) {
                    if(viewModelProfileIsMaleGet) {
                            dirtyFlags |= 0x800L;
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                            dirtyFlags |= 0x1000L;
                    }
                }


                    // read viewModel.profileIsMale.get() ? @android:string/chat_support_artemchek : @android:string/chat_support_lercheck
                    viewModelProfileIsMaleTvSupportNameAndroidStringChatSupportArtemchekTvSupportNameAndroidStringChatSupportLercheck = ((viewModelProfileIsMaleGet) ? (tvSupportName.getResources().getString(R.string.chat_support_artemchek)) : (tvSupportName.getResources().getString(R.string.chat_support_lercheck)));
                    // read viewModel.profileIsMale.get() ? @android:drawable/ic_chat_male : @android:drawable/ic_chat_female
                    viewModelProfileIsMaleIcProfileAndroidDrawableIcChatMaleIcProfileAndroidDrawableIcChatFemale = ((viewModelProfileIsMaleGet) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icProfile.getContext(), R.drawable.ic_chat_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icProfile.getContext(), R.drawable.ic_chat_female)));
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.canEdit
                        viewModelCanEdit = viewModel.getCanEdit();
                    }
                    updateRegistration(2, viewModelCanEdit);


                    if (viewModelCanEdit != null) {
                        // read viewModel.canEdit.get()
                        viewModelCanEditGet = viewModelCanEdit.get();
                    }
                if((dirtyFlags & 0x34L) != 0) {
                    if(viewModelCanEditGet) {
                            dirtyFlags |= 0x80L;
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                            dirtyFlags |= 0x4000L;
                    }
                }


                    // read viewModel.canEdit.get() ? View.GONE : View.VISIBLE
                    viewModelCanEditViewGONEViewVISIBLE = ((viewModelCanEditGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewModel.canEdit.get() ? View.VISIBLE : View.GONE
                    viewModelCanEditViewVISIBLEViewGONE = ((viewModelCanEditGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(3, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x38L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 1

            this.blockedFooter.setVisibility(viewModelCanEditViewGONEViewVISIBLE);
            this.btnSend.setVisibility(viewModelCanEditViewVISIBLEViewGONE);
            this.inputMessage.setVisibility(viewModelCanEditViewVISIBLEViewGONE);
            this.mboundView3.setVisibility(viewModelCanEditViewGONEViewVISIBLE);
            this.mboundView4.setVisibility(viewModelCanEditViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icProfile, viewModelProfileIsMaleIcProfileAndroidDrawableIcChatMaleIcProfileAndroidDrawableIcChatFemale);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSupportName, viewModelProfileIsMaleTvSupportNameAndroidStringChatSupportArtemchekTvSupportNameAndroidStringChatSupportLercheck);
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.inputMessage, viewModelMessageGet);
        }
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.inputMessage, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, inputMessageandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.message
        flag 1 (0x2L): viewModel.profileIsMale
        flag 2 (0x3L): viewModel.canEdit
        flag 3 (0x4L): viewModel.isProgress()
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
        flag 6 (0x7L): viewModel.canEdit.get() ? View.GONE : View.VISIBLE
        flag 7 (0x8L): viewModel.canEdit.get() ? View.GONE : View.VISIBLE
        flag 8 (0x9L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 9 (0xaL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.profileIsMale.get() ? @android:string/chat_support_artemchek : @android:string/chat_support_lercheck
        flag 11 (0xcL): viewModel.profileIsMale.get() ? @android:string/chat_support_artemchek : @android:string/chat_support_lercheck
        flag 12 (0xdL): viewModel.profileIsMale.get() ? @android:drawable/ic_chat_male : @android:drawable/ic_chat_female
        flag 13 (0xeL): viewModel.profileIsMale.get() ? @android:drawable/ic_chat_male : @android:drawable/ic_chat_female
        flag 14 (0xfL): viewModel.canEdit.get() ? View.VISIBLE : View.GONE
        flag 15 (0x10L): viewModel.canEdit.get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}