// Generated by Dagger (https://dagger.dev).
package fit.lerchek.common.di;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import fit.lerchek.data.managers.DataManager;
import javax.inject.Provider;
import okhttp3.OkHttpClient;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NetworkModule_ProvideHttpClientFactory implements Factory<OkHttpClient> {
  private final Provider<DataManager> dataManagerProvider;

  public NetworkModule_ProvideHttpClientFactory(Provider<DataManager> dataManagerProvider) {
    this.dataManagerProvider = dataManagerProvider;
  }

  @Override
  public OkHttpClient get() {
    return provideHttpClient(dataManagerProvider.get());
  }

  public static NetworkModule_ProvideHttpClientFactory create(
      Provider<DataManager> dataManagerProvider) {
    return new NetworkModule_ProvideHttpClientFactory(dataManagerProvider);
  }

  public static OkHttpClient provideHttpClient(DataManager dataManager) {
    return Preconditions.checkNotNullFromProvides(NetworkModule.INSTANCE.provideHttpClient(dataManager));
  }
}
