package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutRefWithdrawBindingImpl extends LayoutRefWithdrawBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback15;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutRefWithdrawBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private LayoutRefWithdrawBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            );
        this.errorWithdrawView.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.successWithdrawView.setTag(null);
        setRootTag(root);
        // listeners
        mCallback15 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.showErrorHint == variableId) {
            setShowErrorHint((java.lang.Boolean) variable);
        }
        else if (BR.showSuccessHint == variableId) {
            setShowSuccessHint((java.lang.Boolean) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback) variable);
        }
        else if (BR.price == variableId) {
            setPrice((java.lang.String) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setShowErrorHint(@Nullable java.lang.Boolean ShowErrorHint) {
        this.mShowErrorHint = ShowErrorHint;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.showErrorHint);
        super.requestRebind();
    }
    public void setShowSuccessHint(@Nullable java.lang.Boolean ShowSuccessHint) {
        this.mShowSuccessHint = ShowSuccessHint;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.showSuccessHint);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }
    public void setPrice(@Nullable java.lang.String Price) {
        this.mPrice = Price;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.price);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int showSuccessHintViewVISIBLEViewGONE = 0;
        java.lang.Boolean showErrorHint = mShowErrorHint;
        boolean androidxDatabindingViewDataBindingSafeUnboxShowSuccessHint = false;
        java.lang.Boolean showSuccessHint = mShowSuccessHint;
        java.lang.String mboundView1AndroidStringRefPriceFormatPrice = null;
        fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback callback = mCallback;
        boolean androidxDatabindingViewDataBindingSafeUnboxShowErrorHint = false;
        int showErrorHintViewVISIBLEViewGONE = 0;
        java.lang.String price = mPrice;

        if ((dirtyFlags & 0x11L) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(showErrorHint)
                androidxDatabindingViewDataBindingSafeUnboxShowErrorHint = androidx.databinding.ViewDataBinding.safeUnbox(showErrorHint);
            if((dirtyFlags & 0x11L) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxShowErrorHint) {
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x80L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(showErrorHint) ? View.VISIBLE : View.GONE
                showErrorHintViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxShowErrorHint) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        if ((dirtyFlags & 0x12L) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(showSuccessHint)
                androidxDatabindingViewDataBindingSafeUnboxShowSuccessHint = androidx.databinding.ViewDataBinding.safeUnbox(showSuccessHint);
            if((dirtyFlags & 0x12L) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxShowSuccessHint) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(showSuccessHint) ? View.VISIBLE : View.GONE
                showSuccessHintViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxShowSuccessHint) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        if ((dirtyFlags & 0x18L) != 0) {



                // read @android:string/ref_price_format
                mboundView1AndroidStringRefPriceFormatPrice = mboundView1.getResources().getString(R.string.ref_price_format, price);
        }
        // batch finished
        if ((dirtyFlags & 0x11L) != 0) {
            // api target 1

            this.errorWithdrawView.setVisibility(showErrorHintViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, mboundView1AndroidStringRefPriceFormatPrice);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.mboundView2.setOnClickListener(mCallback15);
        }
        if ((dirtyFlags & 0x12L) != 0) {
            // api target 1

            this.successWithdrawView.setVisibility(showSuccessHintViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // callback
        fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {


            callback.onWithdraw();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): showErrorHint
        flag 1 (0x2L): showSuccessHint
        flag 2 (0x3L): callback
        flag 3 (0x4L): price
        flag 4 (0x5L): null
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(showSuccessHint) ? View.VISIBLE : View.GONE
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(showSuccessHint) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(showErrorHint) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): androidx.databinding.ViewDataBinding.safeUnbox(showErrorHint) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}