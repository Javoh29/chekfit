package fit.lerchek.ui.feature.marathon.progress;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = AddEditMeasurementsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface AddEditMeasurementsFragment_GeneratedInjector {
  void injectAddEditMeasurementsFragment(AddEditMeasurementsFragment addEditMeasurementsFragment);
}
