package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProgressBindingImpl extends FragmentProgressBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(10);
        sIncludes.setIncludes(1, 
            new String[] {"layout_progress_empty", "layout_progress_photo_error", "layout_progress_your_progress", "layout_progress_photos", "layout_progress_result"},
            new int[] {4, 5, 6, 7, 8},
            new int[] {fit.lerchek.R.layout.layout_progress_empty,
                fit.lerchek.R.layout.layout_progress_photo_error,
                fit.lerchek.R.layout.layout_progress_your_progress,
                fit.lerchek.R.layout.layout_progress_photos,
                fit.lerchek.R.layout.layout_progress_result});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.svMore, 9);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProgressBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private FragmentProgressBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 7
            , (android.widget.FrameLayout) bindings[2]
            , (fit.lerchek.databinding.LayoutProgressEmptyBinding) bindings[4]
            , (fit.lerchek.databinding.LayoutProgressPhotoErrorBinding) bindings[5]
            , (fit.lerchek.databinding.LayoutProgressPhotosBinding) bindings[7]
            , (fit.lerchek.databinding.LayoutProgressResultBinding) bindings[8]
            , (fit.lerchek.databinding.LayoutProgressYourProgressBinding) bindings[6]
            , (android.widget.ScrollView) bindings[9]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (bindings[3] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[3]) : null;
        this.progressBlock.setTag(null);
        setContainedBinding(this.progressEmpty);
        setContainedBinding(this.progressError);
        setContainedBinding(this.progressPhotos);
        setContainedBinding(this.progressResult);
        setContainedBinding(this.progressYourProgress);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        progressEmpty.invalidateAll();
        progressError.invalidateAll();
        progressYourProgress.invalidateAll();
        progressPhotos.invalidateAll();
        progressResult.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (progressEmpty.hasPendingBindings()) {
            return true;
        }
        if (progressError.hasPendingBindings()) {
            return true;
        }
        if (progressYourProgress.hasPendingBindings()) {
            return true;
        }
        if (progressPhotos.hasPendingBindings()) {
            return true;
        }
        if (progressResult.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.progress.ProgressViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.progress.ProgressViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        progressEmpty.setLifecycleOwner(lifecycleOwner);
        progressError.setLifecycleOwner(lifecycleOwner);
        progressYourProgress.setLifecycleOwner(lifecycleOwner);
        progressPhotos.setLifecycleOwner(lifecycleOwner);
        progressResult.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelProgress((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel>) object, fieldId);
            case 1 :
                return onChangeProgressEmpty((fit.lerchek.databinding.LayoutProgressEmptyBinding) object, fieldId);
            case 2 :
                return onChangeProgressYourProgress((fit.lerchek.databinding.LayoutProgressYourProgressBinding) object, fieldId);
            case 3 :
                return onChangeProgressError((fit.lerchek.databinding.LayoutProgressPhotoErrorBinding) object, fieldId);
            case 4 :
                return onChangeProgressPhotos((fit.lerchek.databinding.LayoutProgressPhotosBinding) object, fieldId);
            case 5 :
                return onChangeProgressResult((fit.lerchek.databinding.LayoutProgressResultBinding) object, fieldId);
            case 6 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelProgress(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> ViewModelProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressEmpty(fit.lerchek.databinding.LayoutProgressEmptyBinding ProgressEmpty, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressYourProgress(fit.lerchek.databinding.LayoutProgressYourProgressBinding ProgressYourProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressError(fit.lerchek.databinding.LayoutProgressPhotoErrorBinding ProgressError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressPhotos(fit.lerchek.databinding.LayoutProgressPhotosBinding ProgressPhotos, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressResult(fit.lerchek.databinding.LayoutProgressResultBinding ProgressResult, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> viewModelProgress = null;
        boolean viewModelProgressPhotosEmpty = false;
        fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel viewModelProgressGet = null;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        int viewModelProgressPhotosEmptyViewGONEViewVISIBLE = 0;
        int viewModelProgressProgressEmptyViewVISIBLEViewGONE = 0;
        int viewModelProgressPhotosErrorViewVISIBLEViewGONE = 0;
        boolean viewModelProgressPhotosError = false;
        int viewModelProgressProgressEmptyViewGONEViewVISIBLE = 0;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.progress.ProgressViewModel viewModel = mViewModel;
        boolean viewModelProgressProgressEmpty = false;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;
        java.lang.String viewModelProgressPhotosReviewComment = null;

        if ((dirtyFlags & 0x1c1L) != 0) {


            if ((dirtyFlags & 0x181L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.progress
                        viewModelProgress = viewModel.getProgress();
                    }
                    updateRegistration(0, viewModelProgress);


                    if (viewModelProgress != null) {
                        // read viewModel.progress.get()
                        viewModelProgressGet = viewModelProgress.get();
                    }


                    if (viewModelProgressGet != null) {
                        // read viewModel.progress.get().photosEmpty
                        viewModelProgressPhotosEmpty = viewModelProgressGet.getPhotosEmpty();
                        // read viewModel.progress.get().photosError
                        viewModelProgressPhotosError = viewModelProgressGet.getPhotosError();
                        // read viewModel.progress.get().progressEmpty
                        viewModelProgressProgressEmpty = viewModelProgressGet.getProgressEmpty();
                        // read viewModel.progress.get().photosReviewComment
                        viewModelProgressPhotosReviewComment = viewModelProgressGet.getPhotosReviewComment();
                    }
                if((dirtyFlags & 0x181L) != 0) {
                    if(viewModelProgressPhotosEmpty) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }
                if((dirtyFlags & 0x181L) != 0) {
                    if(viewModelProgressPhotosError) {
                            dirtyFlags |= 0x10000L;
                    }
                    else {
                            dirtyFlags |= 0x8000L;
                    }
                }
                if((dirtyFlags & 0x181L) != 0) {
                    if(viewModelProgressProgressEmpty) {
                            dirtyFlags |= 0x4000L;
                            dirtyFlags |= 0x40000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                            dirtyFlags |= 0x20000L;
                    }
                }


                    // read viewModel.progress.get().photosEmpty ? View.GONE : View.VISIBLE
                    viewModelProgressPhotosEmptyViewGONEViewVISIBLE = ((viewModelProgressPhotosEmpty) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewModel.progress.get().photosError ? View.VISIBLE : View.GONE
                    viewModelProgressPhotosErrorViewVISIBLEViewGONE = ((viewModelProgressPhotosError) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.progress.get().progressEmpty ? View.VISIBLE : View.GONE
                    viewModelProgressProgressEmptyViewVISIBLEViewGONE = ((viewModelProgressProgressEmpty) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.progress.get().progressEmpty ? View.GONE : View.VISIBLE
                    viewModelProgressProgressEmptyViewGONEViewVISIBLE = ((viewModelProgressProgressEmpty) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x1c0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(6, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1c0L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1c0L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x181L) != 0) {
            // api target 1

            this.progressEmpty.getRoot().setVisibility(viewModelProgressProgressEmptyViewVISIBLEViewGONE);
            this.progressError.getRoot().setVisibility(viewModelProgressPhotosErrorViewVISIBLEViewGONE);
            this.progressError.setErrorMessage(viewModelProgressPhotosReviewComment);
            this.progressPhotos.getRoot().setVisibility(viewModelProgressPhotosEmptyViewGONEViewVISIBLE);
            this.progressResult.getRoot().setVisibility(viewModelProgressProgressEmptyViewGONEViewVISIBLE);
            this.progressResult.setProgress(viewModelProgressGet);
            this.progressYourProgress.getRoot().setVisibility(viewModelProgressProgressEmptyViewGONEViewVISIBLE);
            this.progressYourProgress.setProgress(viewModelProgressGet);
        }
        executeBindingsOn(progressEmpty);
        executeBindingsOn(progressError);
        executeBindingsOn(progressYourProgress);
        executeBindingsOn(progressPhotos);
        executeBindingsOn(progressResult);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.progress
        flag 1 (0x2L): progressEmpty
        flag 2 (0x3L): progressYourProgress
        flag 3 (0x4L): progressError
        flag 4 (0x5L): progressPhotos
        flag 5 (0x6L): progressResult
        flag 6 (0x7L): viewModel.isProgress()
        flag 7 (0x8L): viewModel
        flag 8 (0x9L): null
        flag 9 (0xaL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 11 (0xcL): viewModel.progress.get().photosEmpty ? View.GONE : View.VISIBLE
        flag 12 (0xdL): viewModel.progress.get().photosEmpty ? View.GONE : View.VISIBLE
        flag 13 (0xeL): viewModel.progress.get().progressEmpty ? View.VISIBLE : View.GONE
        flag 14 (0xfL): viewModel.progress.get().progressEmpty ? View.VISIBLE : View.GONE
        flag 15 (0x10L): viewModel.progress.get().photosError ? View.VISIBLE : View.GONE
        flag 16 (0x11L): viewModel.progress.get().photosError ? View.VISIBLE : View.GONE
        flag 17 (0x12L): viewModel.progress.get().progressEmpty ? View.GONE : View.VISIBLE
        flag 18 (0x13L): viewModel.progress.get().progressEmpty ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}