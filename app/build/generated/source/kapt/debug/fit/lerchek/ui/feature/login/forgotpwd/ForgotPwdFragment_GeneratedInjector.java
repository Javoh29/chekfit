package fit.lerchek.ui.feature.login.forgotpwd;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ForgotPwdFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ForgotPwdFragment_GeneratedInjector {
  void injectForgotPwdFragment(ForgotPwdFragment forgotPwdFragment);
}
