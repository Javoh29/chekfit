package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemTodayTasksBindingImpl extends ItemTodayTasksBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tvTasks, 7);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback23;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemTodayTasksBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private ItemTodayTasksBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            );
        this.btnActivityCalendar.setTag(null);
        this.icProfile.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView5 = (androidx.recyclerview.widget.RecyclerView) bindings[5];
        this.mboundView5.setTag(null);
        this.tvCurrentDay.setTag(null);
        this.tvHello.setTag(null);
        setRootTag(root);
        // listeners
        mCallback23 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.TodayTasksItem) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.TodayTasksItem Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.TodayTasksItem model = mModel;
        boolean modelIsMale = false;
        java.lang.String tvHelloAndroidStringMarathonTasksHelloModelProfileName = null;
        int modelActivityCalendarVisibleViewVISIBLEViewGONE = 0;
        java.util.List<fit.lerchek.data.domain.uimodel.marathon.TaskUIModel> modelTasks = null;
        java.lang.String modelMarathonName = null;
        int modelCurrentMarathonDay = 0;
        fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback callback = mCallback;
        java.lang.String tvCurrentDayAndroidStringMarathonTasksTodayModelCurrentMarathonDay = null;
        boolean modelActivityCalendarVisible = false;
        boolean modelCurrentMarathonDayInt1 = false;
        java.lang.String modelProfileName = null;
        int modelCurrentDay = 0;
        android.graphics.drawable.Drawable modelIsMaleIcProfileAndroidDrawableIcProfileMaleIcProfileAndroidDrawableIcProfileFemale = null;
        java.lang.String modelCurrentMarathonDayInt1TvCurrentDayAndroidStringMarathonTasksPrepareWeekTvCurrentDayAndroidStringMarathonTasksTodayModelCurrentMarathonDay = null;

        if ((dirtyFlags & 0x7L) != 0) {


            if ((dirtyFlags & 0x5L) != 0) {

                    if (model != null) {
                        // read model.isMale
                        modelIsMale = model.isMale();
                        // read model.marathonName
                        modelMarathonName = model.getMarathonName();
                        // read model.currentMarathonDay
                        modelCurrentMarathonDay = model.getCurrentMarathonDay();
                        // read model.activityCalendarVisible
                        modelActivityCalendarVisible = model.getActivityCalendarVisible();
                        // read model.profileName
                        modelProfileName = model.getProfileName();
                    }
                if((dirtyFlags & 0x5L) != 0) {
                    if(modelIsMale) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }
                if((dirtyFlags & 0x5L) != 0) {
                    if(modelActivityCalendarVisible) {
                            dirtyFlags |= 0x10L;
                    }
                    else {
                            dirtyFlags |= 0x8L;
                    }
                }


                    // read model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                    modelIsMaleIcProfileAndroidDrawableIcProfileMaleIcProfileAndroidDrawableIcProfileFemale = ((modelIsMale) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(icProfile.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(icProfile.getContext(), R.drawable.ic_profile_female)));
                    // read model.currentMarathonDay < 1
                    modelCurrentMarathonDayInt1 = (modelCurrentMarathonDay) < (1);
                    // read model.activityCalendarVisible ? View.VISIBLE : View.GONE
                    modelActivityCalendarVisibleViewVISIBLEViewGONE = ((modelActivityCalendarVisible) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read @android:string/marathon_tasks_hello
                    tvHelloAndroidStringMarathonTasksHelloModelProfileName = tvHello.getResources().getString(R.string.marathon_tasks_hello, modelProfileName);
                if((dirtyFlags & 0x5L) != 0) {
                    if(modelCurrentMarathonDayInt1) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }
            }

                if (model != null) {
                    // read model.tasks
                    modelTasks = model.getTasks();
                    // read model.currentDay
                    modelCurrentDay = model.getCurrentDay();
                }
        }
        // batch finished

        if ((dirtyFlags & 0x80L) != 0) {

                // read @android:string/marathon_tasks_today
                tvCurrentDayAndroidStringMarathonTasksTodayModelCurrentMarathonDay = tvCurrentDay.getResources().getString(R.string.marathon_tasks_today, modelCurrentMarathonDay);
        }

        if ((dirtyFlags & 0x5L) != 0) {

                // read model.currentMarathonDay < 1 ? @android:string/marathon_tasks_prepare_week : @android:string/marathon_tasks_today
                modelCurrentMarathonDayInt1TvCurrentDayAndroidStringMarathonTasksPrepareWeekTvCurrentDayAndroidStringMarathonTasksTodayModelCurrentMarathonDay = ((modelCurrentMarathonDayInt1) ? (tvCurrentDay.getResources().getString(R.string.marathon_tasks_prepare_week)) : (tvCurrentDayAndroidStringMarathonTasksTodayModelCurrentMarathonDay));
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.btnActivityCalendar.setOnClickListener(mCallback23);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            this.btnActivityCalendar.setVisibility(modelActivityCalendarVisibleViewVISIBLEViewGONE);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.icProfile, modelIsMaleIcProfileAndroidDrawableIcProfileMaleIcProfileAndroidDrawableIcProfileFemale);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, modelMarathonName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCurrentDay, modelCurrentMarathonDayInt1TvCurrentDayAndroidStringMarathonTasksPrepareWeekTvCurrentDayAndroidStringMarathonTasksTodayModelCurrentMarathonDay);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHello, tvHelloAndroidStringMarathonTasksHelloModelProfileName);
        }
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            fit.lerchek.ui.feature.marathon.today.adapters.TodayTasksAdapterKt.setupTodayTasksAdapter(this.mboundView5, modelCurrentDay, modelTasks, callback);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // callback
        fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback callback = mCallback;
        // callback != null
        boolean callbackJavaLangObjectNull = false;



        callbackJavaLangObjectNull = (callback) != (null);
        if (callbackJavaLangObjectNull) {


            callback.onActivityCalendarClick();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
        flag 3 (0x4L): model.activityCalendarVisible ? View.VISIBLE : View.GONE
        flag 4 (0x5L): model.activityCalendarVisible ? View.VISIBLE : View.GONE
        flag 5 (0x6L): model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 6 (0x7L): model.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 7 (0x8L): model.currentMarathonDay < 1 ? @android:string/marathon_tasks_prepare_week : @android:string/marathon_tasks_today
        flag 8 (0x9L): model.currentMarathonDay < 1 ? @android:string/marathon_tasks_prepare_week : @android:string/marathon_tasks_today
    flag mapping end*/
    //end
}