package fit.lerchek;

import dagger.Binds;
import dagger.Component;
import dagger.Module;
import dagger.Subcomponent;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.android.components.ServiceComponent;
import dagger.hilt.android.components.ViewComponent;
import dagger.hilt.android.components.ViewModelComponent;
import dagger.hilt.android.components.ViewWithFragmentComponent;
import dagger.hilt.android.internal.builders.ActivityComponentBuilder;
import dagger.hilt.android.internal.builders.ActivityRetainedComponentBuilder;
import dagger.hilt.android.internal.builders.FragmentComponentBuilder;
import dagger.hilt.android.internal.builders.ServiceComponentBuilder;
import dagger.hilt.android.internal.builders.ViewComponentBuilder;
import dagger.hilt.android.internal.builders.ViewModelComponentBuilder;
import dagger.hilt.android.internal.builders.ViewWithFragmentComponentBuilder;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories;
import dagger.hilt.android.internal.lifecycle.HiltViewModelFactory;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_DefaultViewModelFactories_ActivityModule;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_HiltViewModelFactory_ActivityCreatorEntryPoint;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_HiltViewModelFactory_ViewModelModule;
import dagger.hilt.android.internal.managers.ActivityComponentManager;
import dagger.hilt.android.internal.managers.FragmentComponentManager;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedComponentBuilderEntryPoint;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedLifecycleEntryPoint;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_LifecycleModule;
import dagger.hilt.android.internal.managers.ServiceComponentManager;
import dagger.hilt.android.internal.managers.ViewComponentManager;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.modules.HiltWrapper_ActivityModule;
import dagger.hilt.android.scopes.ActivityRetainedScoped;
import dagger.hilt.android.scopes.ActivityScoped;
import dagger.hilt.android.scopes.FragmentScoped;
import dagger.hilt.android.scopes.ServiceScoped;
import dagger.hilt.android.scopes.ViewModelScoped;
import dagger.hilt.android.scopes.ViewScoped;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedComponent;
import dagger.hilt.migration.DisableInstallInCheck;
import fit.lerchek.common.di.BindingModule;
import fit.lerchek.common.di.DataModule;
import fit.lerchek.common.di.NetworkModule;
import fit.lerchek.data.fcm.FMService_GeneratedInjector;
import fit.lerchek.ui.feature.chat.ChatActivityViewModel_HiltModules;
import fit.lerchek.ui.feature.chat.ChatActivity_GeneratedInjector;
import fit.lerchek.ui.feature.login.LoginActivity_GeneratedInjector;
import fit.lerchek.ui.feature.login.auth.MarathonWaitingFragment_GeneratedInjector;
import fit.lerchek.ui.feature.login.auth.SignInFragment_GeneratedInjector;
import fit.lerchek.ui.feature.login.auth.SignInViewModel_HiltModules;
import fit.lerchek.ui.feature.login.forgotpwd.ForgotPwdFragment_GeneratedInjector;
import fit.lerchek.ui.feature.login.forgotpwd.ForgotPwdViewModel_HiltModules;
import fit.lerchek.ui.feature.login.forgotpwd.SetNewPwdFragment_GeneratedInjector;
import fit.lerchek.ui.feature.login.forgotpwd.SetNewPwdViewModel_HiltModules;
import fit.lerchek.ui.feature.login.splash.SplashViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.DynamicContentFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.DynamicContentViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.FaqFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.FaqViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.InfoDetailsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.InfoDetailsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.MarathonActivityViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.MarathonActivity_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.MarathonListFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.MarathonListViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.ReportsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.ReportsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.activity.ActivityCalendarFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.activity.ActivityCalendarViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.food.FavoriteFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.food.FavoriteViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.food.FoodFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.food.FoodIngredientsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.food.FoodIngredientsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.food.FoodViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.food.RecipeDetailsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.food.RecipeDetailsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.food.RecipeReplacementFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.food.RecipeReplacementViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.menu.MenuFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.menu.MenuViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.more.ExtraRecipesFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.more.ExtraRecipesViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.more.MoreFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.more.MoreViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.more.ProductsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.more.ProductsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.more.calories.CalorieCalculatorViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.more.cardiotest.CardioTestFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.more.cardiotest.CardioTestViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.profile.ProfileFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.profile.ProfileViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.profile.personal.PersonalInfoViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.profile.personal.PersonalParamViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.profile.personal.TestFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.profile.personal.TestViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.profile.personal.UserPersonalInfoFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.profile.personal.UserPersonalParamFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.profile.referral.ReferralFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.profile.referral.ReferralViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.profile.referral.ReferralWithdrawFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.profile.referral.ReferralWithdrawViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.progress.AddEditMeasurementsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.progress.AddEditMeasurementsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.progress.MeasurementsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.progress.MeasurementsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.progress.ProgressFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.progress.ProgressViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.progress.crop.CropActivityViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.progress.crop.CropActivity_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.today.TodayFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.today.TodayViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.today.TrackerFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.today.TrackerSettingFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.today.TrackerSettingViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.today.TrackerViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.voting.VotingFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.voting.VotingViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.workout.WorkoutDetailsFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.workout.WorkoutDetailsViewModel_HiltModules;
import fit.lerchek.ui.feature.marathon.workout.WorkoutFragment_GeneratedInjector;
import fit.lerchek.ui.feature.marathon.workout.WorkoutViewModel_HiltModules;
import javax.inject.Singleton;

public final class LerchekFitApp_HiltComponents {
  private LerchekFitApp_HiltComponents() {
  }

  @Module(
      subcomponents = ServiceC.class
  )
  @DisableInstallInCheck
  abstract interface ServiceCBuilderModule {
    @Binds
    ServiceComponentBuilder bind(ServiceC.Builder builder);
  }

  @Module(
      subcomponents = ActivityRetainedC.class
  )
  @DisableInstallInCheck
  abstract interface ActivityRetainedCBuilderModule {
    @Binds
    ActivityRetainedComponentBuilder bind(ActivityRetainedC.Builder builder);
  }

  @Module(
      subcomponents = ActivityC.class
  )
  @DisableInstallInCheck
  abstract interface ActivityCBuilderModule {
    @Binds
    ActivityComponentBuilder bind(ActivityC.Builder builder);
  }

  @Module(
      subcomponents = ViewModelC.class
  )
  @DisableInstallInCheck
  abstract interface ViewModelCBuilderModule {
    @Binds
    ViewModelComponentBuilder bind(ViewModelC.Builder builder);
  }

  @Module(
      subcomponents = ViewC.class
  )
  @DisableInstallInCheck
  abstract interface ViewCBuilderModule {
    @Binds
    ViewComponentBuilder bind(ViewC.Builder builder);
  }

  @Module(
      subcomponents = FragmentC.class
  )
  @DisableInstallInCheck
  abstract interface FragmentCBuilderModule {
    @Binds
    FragmentComponentBuilder bind(FragmentC.Builder builder);
  }

  @Module(
      subcomponents = ViewWithFragmentC.class
  )
  @DisableInstallInCheck
  abstract interface ViewWithFragmentCBuilderModule {
    @Binds
    ViewWithFragmentComponentBuilder bind(ViewWithFragmentC.Builder builder);
  }

  @Component(
      modules = {
          ApplicationContextModule.class,
          BindingModule.class,
          DataModule.class,
          ActivityRetainedCBuilderModule.class,
          ServiceCBuilderModule.class,
          NetworkModule.class
      }
  )
  @Singleton
  public abstract static class SingletonC implements HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedComponentBuilderEntryPoint,
      ServiceComponentManager.ServiceComponentBuilderEntryPoint,
      SingletonComponent,
      GeneratedComponent,
      LerchekFitApp_GeneratedInjector {
  }

  @Subcomponent
  @ServiceScoped
  public abstract static class ServiceC implements ServiceComponent,
      GeneratedComponent,
      FMService_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends ServiceComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          ActivityCalendarViewModel_HiltModules.KeyModule.class,
          AddEditMeasurementsViewModel_HiltModules.KeyModule.class,
          BMICalculatorViewModel_HiltModules.KeyModule.class,
          CalorieCalculatorViewModel_HiltModules.KeyModule.class,
          CardioTestViewModel_HiltModules.KeyModule.class,
          ChatActivityViewModel_HiltModules.KeyModule.class,
          CropActivityViewModel_HiltModules.KeyModule.class,
          DynamicContentViewModel_HiltModules.KeyModule.class,
          ExtraRecipesViewModel_HiltModules.KeyModule.class,
          FaqViewModel_HiltModules.KeyModule.class,
          FatCalculatorViewModel_HiltModules.KeyModule.class,
          FavoriteViewModel_HiltModules.KeyModule.class,
          FoodIngredientsViewModel_HiltModules.KeyModule.class,
          FoodViewModel_HiltModules.KeyModule.class,
          ForgotPwdViewModel_HiltModules.KeyModule.class,
          HiltWrapper_ActivityRetainedComponentManager_LifecycleModule.class,
          InfoDetailsViewModel_HiltModules.KeyModule.class,
          ActivityCBuilderModule.class,
          ViewModelCBuilderModule.class,
          MakeMeasurementsViewModel_HiltModules.KeyModule.class,
          MarathonActivityViewModel_HiltModules.KeyModule.class,
          MarathonListViewModel_HiltModules.KeyModule.class,
          MeasurementsViewModel_HiltModules.KeyModule.class,
          MenuViewModel_HiltModules.KeyModule.class,
          MoreViewModel_HiltModules.KeyModule.class,
          PersonalInfoViewModel_HiltModules.KeyModule.class,
          PersonalParamViewModel_HiltModules.KeyModule.class,
          ProductsViewModel_HiltModules.KeyModule.class,
          ProfileViewModel_HiltModules.KeyModule.class,
          ProgressViewModel_HiltModules.KeyModule.class,
          RecipeDetailsViewModel_HiltModules.KeyModule.class,
          RecipeReplacementViewModel_HiltModules.KeyModule.class,
          ReferralViewModel_HiltModules.KeyModule.class,
          ReferralWithdrawViewModel_HiltModules.KeyModule.class,
          ReportsViewModel_HiltModules.KeyModule.class,
          SetNewPwdViewModel_HiltModules.KeyModule.class,
          SignInViewModel_HiltModules.KeyModule.class,
          SplashViewModel_HiltModules.KeyModule.class,
          TestViewModel_HiltModules.KeyModule.class,
          TodayViewModel_HiltModules.KeyModule.class,
          TrackerSettingViewModel_HiltModules.KeyModule.class,
          TrackerViewModel_HiltModules.KeyModule.class,
          VotingViewModel_HiltModules.KeyModule.class,
          WorkoutDetailsViewModel_HiltModules.KeyModule.class,
          WorkoutViewModel_HiltModules.KeyModule.class
      }
  )
  @ActivityRetainedScoped
  public abstract static class ActivityRetainedC implements ActivityRetainedComponent,
      ActivityComponentManager.ActivityComponentBuilderEntryPoint,
      HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedLifecycleEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityRetainedComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          HiltWrapper_ActivityModule.class,
          HiltWrapper_DefaultViewModelFactories_ActivityModule.class,
          FragmentCBuilderModule.class,
          ViewCBuilderModule.class
      }
  )
  @ActivityScoped
  public abstract static class ActivityC implements ActivityComponent,
      DefaultViewModelFactories.ActivityEntryPoint,
      HiltWrapper_HiltViewModelFactory_ActivityCreatorEntryPoint,
      FragmentComponentManager.FragmentComponentBuilderEntryPoint,
      ViewComponentManager.ViewComponentBuilderEntryPoint,
      GeneratedComponent,
      ChatActivity_GeneratedInjector,
      LoginActivity_GeneratedInjector,
      MarathonActivity_GeneratedInjector,
      CropActivity_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          ActivityCalendarViewModel_HiltModules.BindsModule.class,
          AddEditMeasurementsViewModel_HiltModules.BindsModule.class,
          BMICalculatorViewModel_HiltModules.BindsModule.class,
          CalorieCalculatorViewModel_HiltModules.BindsModule.class,
          CardioTestViewModel_HiltModules.BindsModule.class,
          ChatActivityViewModel_HiltModules.BindsModule.class,
          CropActivityViewModel_HiltModules.BindsModule.class,
          DynamicContentViewModel_HiltModules.BindsModule.class,
          ExtraRecipesViewModel_HiltModules.BindsModule.class,
          FaqViewModel_HiltModules.BindsModule.class,
          FatCalculatorViewModel_HiltModules.BindsModule.class,
          FavoriteViewModel_HiltModules.BindsModule.class,
          FoodIngredientsViewModel_HiltModules.BindsModule.class,
          FoodViewModel_HiltModules.BindsModule.class,
          ForgotPwdViewModel_HiltModules.BindsModule.class,
          HiltWrapper_HiltViewModelFactory_ViewModelModule.class,
          InfoDetailsViewModel_HiltModules.BindsModule.class,
          MakeMeasurementsViewModel_HiltModules.BindsModule.class,
          MarathonActivityViewModel_HiltModules.BindsModule.class,
          MarathonListViewModel_HiltModules.BindsModule.class,
          MeasurementsViewModel_HiltModules.BindsModule.class,
          MenuViewModel_HiltModules.BindsModule.class,
          MoreViewModel_HiltModules.BindsModule.class,
          PersonalInfoViewModel_HiltModules.BindsModule.class,
          PersonalParamViewModel_HiltModules.BindsModule.class,
          ProductsViewModel_HiltModules.BindsModule.class,
          ProfileViewModel_HiltModules.BindsModule.class,
          ProgressViewModel_HiltModules.BindsModule.class,
          RecipeDetailsViewModel_HiltModules.BindsModule.class,
          RecipeReplacementViewModel_HiltModules.BindsModule.class,
          ReferralViewModel_HiltModules.BindsModule.class,
          ReferralWithdrawViewModel_HiltModules.BindsModule.class,
          ReportsViewModel_HiltModules.BindsModule.class,
          SetNewPwdViewModel_HiltModules.BindsModule.class,
          SignInViewModel_HiltModules.BindsModule.class,
          SplashViewModel_HiltModules.BindsModule.class,
          TestViewModel_HiltModules.BindsModule.class,
          TodayViewModel_HiltModules.BindsModule.class,
          TrackerSettingViewModel_HiltModules.BindsModule.class,
          TrackerViewModel_HiltModules.BindsModule.class,
          VotingViewModel_HiltModules.BindsModule.class,
          WorkoutDetailsViewModel_HiltModules.BindsModule.class,
          WorkoutViewModel_HiltModules.BindsModule.class
      }
  )
  @ViewModelScoped
  public abstract static class ViewModelC implements ViewModelComponent,
      HiltViewModelFactory.ViewModelFactoriesEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewModelComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewC implements ViewComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewComponentBuilder {
    }
  }

  @Subcomponent(
      modules = ViewWithFragmentCBuilderModule.class
  )
  @FragmentScoped
  public abstract static class FragmentC implements FragmentComponent,
      DefaultViewModelFactories.FragmentEntryPoint,
      ViewComponentManager.ViewWithFragmentComponentBuilderEntryPoint,
      GeneratedComponent,
      MarathonWaitingFragment_GeneratedInjector,
      SignInFragment_GeneratedInjector,
      ForgotPwdFragment_GeneratedInjector,
      SetNewPwdFragment_GeneratedInjector,
      DynamicContentFragment_GeneratedInjector,
      FaqFragment_GeneratedInjector,
      InfoDetailsFragment_GeneratedInjector,
      MarathonListFragment_GeneratedInjector,
      ReportsFragment_GeneratedInjector,
      ActivityCalendarFragment_GeneratedInjector,
      FavoriteFragment_GeneratedInjector,
      FoodFragment_GeneratedInjector,
      FoodIngredientsFragment_GeneratedInjector,
      RecipeDetailsFragment_GeneratedInjector,
      RecipeReplacementFragment_GeneratedInjector,
      MenuFragment_GeneratedInjector,
      ExtraRecipesFragment_GeneratedInjector,
      MoreFragment_GeneratedInjector,
      ProductsFragment_GeneratedInjector,
      BMICalculatorFragment_GeneratedInjector,
      CalorieCalculatorFragment_GeneratedInjector,
      CardioTestFragment_GeneratedInjector,
      FatCalculatorFragment_GeneratedInjector,
      ProfileFragment_GeneratedInjector,
      TestFragment_GeneratedInjector,
      UserPersonalInfoFragment_GeneratedInjector,
      UserPersonalParamFragment_GeneratedInjector,
      ReferralFragment_GeneratedInjector,
      ReferralWithdrawFragment_GeneratedInjector,
      AddEditMeasurementsFragment_GeneratedInjector,
      MakeMeasurementsFragment_GeneratedInjector,
      MeasurementsFragment_GeneratedInjector,
      ProgressFragment_GeneratedInjector,
      TodayFragment_GeneratedInjector,
      TrackerFragment_GeneratedInjector,
      TrackerSettingFragment_GeneratedInjector,
      VotingFragment_GeneratedInjector,
      WorkoutDetailsFragment_GeneratedInjector,
      WorkoutFragment_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends FragmentComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewWithFragmentC implements ViewWithFragmentComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewWithFragmentComponentBuilder {
    }
  }
}
