package fit.lerchek.ui.feature.marathon;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = MarathonActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface MarathonActivity_GeneratedInjector {
  void injectMarathonActivity(MarathonActivity marathonActivity);
}
