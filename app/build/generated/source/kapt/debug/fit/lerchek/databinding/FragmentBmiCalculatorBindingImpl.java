package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentBmiCalculatorBindingImpl extends FragmentBmiCalculatorBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 13);
        sViewsWithIds.put(R.id.nsvBMICalculator, 15);
        sViewsWithIds.put(R.id.constraintLayoutHeight, 16);
        sViewsWithIds.put(R.id.constraintLayoutWeight, 17);
        sViewsWithIds.put(R.id.yourBMITitle, 18);
        sViewsWithIds.put(R.id.cardBMI, 19);
        sViewsWithIds.put(R.id.imgBMI, 20);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView11;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView12;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView5;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView7;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView8;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback26;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener heightandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.height.get()
            //         is viewModel.height.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(height);
            // localize variables for thread safety
            // viewModel.height.get()
            java.lang.String viewModelHeightGet = null;
            // viewModel.height != null
            boolean viewModelHeightJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorViewModel viewModel = mViewModel;
            // viewModel.height
            androidx.databinding.ObservableField<java.lang.String> viewModelHeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelHeight = viewModel.getHeight();

                viewModelHeightJavaLangObjectNull = (viewModelHeight) != (null);
                if (viewModelHeightJavaLangObjectNull) {




                    viewModelHeight.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener weightandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.weight.get()
            //         is viewModel.weight.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(weight);
            // localize variables for thread safety
            // viewModel.weight.get()
            java.lang.String viewModelWeightGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorViewModel viewModel = mViewModel;
            // viewModel.weight
            androidx.databinding.ObservableField<java.lang.String> viewModelWeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.weight != null
            boolean viewModelWeightJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelWeight = viewModel.getWeight();

                viewModelWeightJavaLangObjectNull = (viewModelWeight) != (null);
                if (viewModelWeightJavaLangObjectNull) {




                    viewModelWeight.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentBmiCalculatorBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentBmiCalculatorBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (bindings[13] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[13]) : null
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.cardview.widget.CardView) bindings[19]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[16]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[17]
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[20]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.core.widget.NestedScrollView) bindings[15]
            , (android.widget.FrameLayout) bindings[12]
            , (androidx.appcompat.widget.AppCompatEditText) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[18]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[6]
            );
        this.btnCalculate.setTag(null);
        this.frameLayout.setTag(null);
        this.height.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView11 = (androidx.appcompat.widget.AppCompatTextView) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView12 = (bindings[14] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[14]) : null;
        this.mboundView5 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView7 = (androidx.appcompat.widget.AppCompatTextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (androidx.appcompat.widget.AppCompatImageView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (androidx.appcompat.widget.AppCompatTextView) bindings[9];
        this.mboundView9.setTag(null);
        this.nameBMI.setTag(null);
        this.progressBlock.setTag(null);
        this.weight.setTag(null);
        this.yourBMIValue.setTag(null);
        setRootTag(root);
        // listeners
        mCallback26 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsCalculate((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeViewModelHeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelWeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelYourBMI((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelValueBMI((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.BMIItemUIModel>) object, fieldId);
            case 5 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsCalculate(androidx.databinding.ObservableBoolean ViewModelIsCalculate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHeight(androidx.databinding.ObservableField<java.lang.String> ViewModelHeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelWeight(androidx.databinding.ObservableField<java.lang.String> ViewModelWeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelYourBMI(androidx.databinding.ObservableField<java.lang.String> ViewModelYourBMI, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelValueBMI(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.BMIItemUIModel> ViewModelValueBMI, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelValueBMIValue = null;
        int viewModelIsCalculateViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableBoolean viewModelIsCalculate = null;
        java.lang.String viewModelHeightGet = null;
        int viewModelValueBMITypeDescriptionRes = 0;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelHeight = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelWeight = null;
        android.graphics.drawable.Drawable contextCompatGetDrawableContextViewModelValueBMITypeImgRes = null;
        boolean viewModelIsCalculateGet = false;
        java.lang.String viewModelValueBMINumber = null;
        java.lang.String viewModelWeightGet = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelYourBMI = null;
        fit.lerchek.data.domain.uimodel.marathon.BMIItemUIModel viewModelValueBMIGet = null;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.BMIItemUIModel> viewModelValueBMI = null;
        int viewModelValueBMITypeNameRes = 0;
        java.lang.String viewModelYourBMIGet = null;
        int viewModelValueBMITypeImgRes = 0;
        fit.lerchek.data.domain.uimodel.marathon.BMIItemUIModel.Type viewModelValueBMIType = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;

        if ((dirtyFlags & 0xffL) != 0) {


            if ((dirtyFlags & 0xc1L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isCalculate()
                        viewModelIsCalculate = viewModel.isCalculate();
                    }
                    updateRegistration(0, viewModelIsCalculate);


                    if (viewModelIsCalculate != null) {
                        // read viewModel.isCalculate().get()
                        viewModelIsCalculateGet = viewModelIsCalculate.get();
                    }
                if((dirtyFlags & 0xc1L) != 0) {
                    if(viewModelIsCalculateGet) {
                            dirtyFlags |= 0x200L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                    }
                }


                    // read viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
                    viewModelIsCalculateViewVISIBLEViewGONE = ((viewModelIsCalculateGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xc2L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.height
                        viewModelHeight = viewModel.getHeight();
                    }
                    updateRegistration(1, viewModelHeight);


                    if (viewModelHeight != null) {
                        // read viewModel.height.get()
                        viewModelHeightGet = viewModelHeight.get();
                    }
            }
            if ((dirtyFlags & 0xc4L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.weight
                        viewModelWeight = viewModel.getWeight();
                    }
                    updateRegistration(2, viewModelWeight);


                    if (viewModelWeight != null) {
                        // read viewModel.weight.get()
                        viewModelWeightGet = viewModelWeight.get();
                    }
            }
            if ((dirtyFlags & 0xc8L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.yourBMI
                        viewModelYourBMI = viewModel.getYourBMI();
                    }
                    updateRegistration(3, viewModelYourBMI);


                    if (viewModelYourBMI != null) {
                        // read viewModel.yourBMI.get()
                        viewModelYourBMIGet = viewModelYourBMI.get();
                    }
            }
            if ((dirtyFlags & 0xd0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.valueBMI
                        viewModelValueBMI = viewModel.getValueBMI();
                    }
                    updateRegistration(4, viewModelValueBMI);


                    if (viewModelValueBMI != null) {
                        // read viewModel.valueBMI.get()
                        viewModelValueBMIGet = viewModelValueBMI.get();
                    }


                    if (viewModelValueBMIGet != null) {
                        // read viewModel.valueBMI.get().value
                        viewModelValueBMIValue = viewModelValueBMIGet.getValue();
                        // read viewModel.valueBMI.get().number
                        viewModelValueBMINumber = viewModelValueBMIGet.getNumber();
                        // read viewModel.valueBMI.get().type
                        viewModelValueBMIType = viewModelValueBMIGet.getType();
                    }


                    if (viewModelValueBMIType != null) {
                        // read viewModel.valueBMI.get().type.descriptionRes
                        viewModelValueBMITypeDescriptionRes = viewModelValueBMIType.getDescriptionRes();
                        // read viewModel.valueBMI.get().type.nameRes
                        viewModelValueBMITypeNameRes = viewModelValueBMIType.getNameRes();
                        // read viewModel.valueBMI.get().type.imgRes
                        viewModelValueBMITypeImgRes = viewModelValueBMIType.getImgRes();
                    }


                    // read ContextCompat.getDrawable(context, viewModel.valueBMI.get().type.imgRes)
                    contextCompatGetDrawableContextViewModelValueBMITypeImgRes = androidx.core.content.ContextCompat.getDrawable(getRoot().getContext(), viewModelValueBMITypeImgRes);
            }
            if ((dirtyFlags & 0xe0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(5, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0xe0L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x80L) != 0) {
            // api target 1

            this.btnCalculate.setOnClickListener(mCallback26);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.height, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, heightandroidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.weight, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, weightandroidTextAttrChanged);
        }
        if ((dirtyFlags & 0xc2L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.height, viewModelHeightGet);
        }
        if ((dirtyFlags & 0xd0L) != 0) {
            // api target 1

            this.mboundView11.setText(viewModelValueBMITypeDescriptionRes);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, viewModelValueBMINumber);
            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.mboundView8, contextCompatGetDrawableContextViewModelValueBMITypeImgRes);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView9, viewModelValueBMIValue);
            this.nameBMI.setText(viewModelValueBMITypeNameRes);
        }
        if ((dirtyFlags & 0xc1L) != 0) {
            // api target 1

            this.mboundView5.setVisibility(viewModelIsCalculateViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xe0L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xc4L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.weight, viewModelWeightGet);
        }
        if ((dirtyFlags & 0xc8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.yourBMIValue, viewModelYourBMIGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        fit.lerchek.ui.feature.marathon.more.bmi.BMICalculatorViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.calculate();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isCalculate()
        flag 1 (0x2L): viewModel.height
        flag 2 (0x3L): viewModel.weight
        flag 3 (0x4L): viewModel.yourBMI
        flag 4 (0x5L): viewModel.valueBMI
        flag 5 (0x6L): viewModel.isProgress()
        flag 6 (0x7L): viewModel
        flag 7 (0x8L): null
        flag 8 (0x9L): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 9 (0xaL): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 11 (0xcL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}