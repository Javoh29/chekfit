package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentReferralBindingImpl extends FragmentReferralBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(11);
        sIncludes.setIncludes(1, 
            new String[] {"item_today_ref"},
            new int[] {8},
            new int[] {fit.lerchek.R.layout.item_today_ref});
        sIncludes.setIncludes(2, 
            new String[] {"layout_ref_withdraw"},
            new int[] {9},
            new int[] {fit.lerchek.R.layout.layout_ref_withdraw});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.referralScrollView, 10);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @Nullable
    private final fit.lerchek.databinding.ItemTodayRefBinding mboundView11;
    @NonNull
    private final android.widget.FrameLayout mboundView2;
    @Nullable
    private final fit.lerchek.databinding.LayoutRefWithdrawBinding mboundView21;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.cardview.widget.CardView mboundView4;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView5;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentReferralBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private FragmentReferralBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.FrameLayout) bindings[6]
            , (android.widget.ScrollView) bindings[10]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView11 = (fit.lerchek.databinding.ItemTodayRefBinding) bindings[8];
        setContainedBinding(this.mboundView11);
        this.mboundView2 = (android.widget.FrameLayout) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView21 = (fit.lerchek.databinding.LayoutRefWithdrawBinding) bindings[9];
        setContainedBinding(this.mboundView21);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.cardview.widget.CardView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.recyclerview.widget.RecyclerView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (bindings[7] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[7]) : null;
        this.progressBlock.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        mboundView11.invalidateAll();
        mboundView21.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (mboundView11.hasPendingBindings()) {
            return true;
        }
        if (mboundView21.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.callback == variableId) {
            setCallback((fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.profile.referral.ReferralViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCallback(@Nullable fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }
    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.profile.referral.ReferralViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        mboundView11.setLifecycleOwner(lifecycleOwner);
        mboundView21.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelReferralDetails((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ReferralDetailsUIModel>) object, fieldId);
            case 1 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelReferralDetails(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ReferralDetailsUIModel> ViewModelReferralDetails, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.util.List<fit.lerchek.data.domain.uimodel.profile.TransactionUIModel> viewModelReferralDetailsTransactions = null;
        java.lang.String viewModelReferralDetailsLink = null;
        java.lang.String viewModelReferralDetailsBalance = null;
        boolean viewModelReferralDetailsTransactionsEmpty = false;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.profile.ReferralDetailsUIModel> viewModelReferralDetails = null;
        fit.lerchek.data.domain.uimodel.profile.ReferralDetailsUIModel viewModelReferralDetailsGet = null;
        fit.lerchek.data.domain.uimodel.marathon.TodayRefItem.Callback callback = mCallback;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.profile.referral.ReferralViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;
        int viewModelReferralDetailsTransactionsEmptyViewGONEViewVISIBLE = 0;

        if ((dirtyFlags & 0x14L) != 0) {
        }
        if ((dirtyFlags & 0x1bL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.referralDetails
                        viewModelReferralDetails = viewModel.getReferralDetails();
                    }
                    updateRegistration(0, viewModelReferralDetails);


                    if (viewModelReferralDetails != null) {
                        // read viewModel.referralDetails.get()
                        viewModelReferralDetailsGet = viewModelReferralDetails.get();
                    }


                    if (viewModelReferralDetailsGet != null) {
                        // read viewModel.referralDetails.get().transactions
                        viewModelReferralDetailsTransactions = viewModelReferralDetailsGet.getTransactions();
                        // read viewModel.referralDetails.get().link
                        viewModelReferralDetailsLink = viewModelReferralDetailsGet.getLink();
                        // read viewModel.referralDetails.get().balance
                        viewModelReferralDetailsBalance = viewModelReferralDetailsGet.getBalance();
                    }


                    if (viewModelReferralDetailsTransactions != null) {
                        // read viewModel.referralDetails.get().transactions.empty
                        viewModelReferralDetailsTransactionsEmpty = viewModelReferralDetailsTransactions.isEmpty();
                    }
                if((dirtyFlags & 0x19L) != 0) {
                    if(viewModelReferralDetailsTransactionsEmpty) {
                            dirtyFlags |= 0x100L;
                    }
                    else {
                            dirtyFlags |= 0x80L;
                    }
                }


                    // read viewModel.referralDetails.get().transactions.empty ? View.GONE : View.VISIBLE
                    viewModelReferralDetailsTransactionsEmptyViewGONEViewVISIBLE = ((viewModelReferralDetailsTransactionsEmpty) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(1, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0x1aL) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x14L) != 0) {
            // api target 1

            this.mboundView11.setCallback(callback);
            this.mboundView21.setCallback(callback);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            this.mboundView11.setLink(viewModelReferralDetailsLink);
            this.mboundView21.setPrice(viewModelReferralDetailsBalance);
            this.mboundView3.setVisibility(viewModelReferralDetailsTransactionsEmptyViewGONEViewVISIBLE);
            this.mboundView4.setVisibility(viewModelReferralDetailsTransactionsEmptyViewGONEViewVISIBLE);
            fit.lerchek.ui.feature.marathon.profile.referral.RefTransactionsAdapterKt.setupRefAdapter(this.mboundView5, viewModelReferralDetailsTransactions);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.mboundView21.setShowSuccessHint(false);
            this.mboundView21.setShowErrorHint(false);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        executeBindingsOn(mboundView11);
        executeBindingsOn(mboundView21);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.referralDetails
        flag 1 (0x2L): viewModel.isProgress()
        flag 2 (0x3L): callback
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 6 (0x7L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 7 (0x8L): viewModel.referralDetails.get().transactions.empty ? View.GONE : View.VISIBLE
        flag 8 (0x9L): viewModel.referralDetails.get().transactions.empty ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}