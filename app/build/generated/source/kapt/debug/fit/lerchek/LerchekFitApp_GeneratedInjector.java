package fit.lerchek;

import dagger.hilt.InstallIn;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = LerchekFitApp.class
)
@GeneratedEntryPoint
@InstallIn(SingletonComponent.class)
public interface LerchekFitApp_GeneratedInjector {
  void injectLerchekFitApp(LerchekFitApp lerchekFitApp);
}
