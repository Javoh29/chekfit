package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutProgressResultBindingImpl extends LayoutProgressResultBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.cardWeight, 29);
        sViewsWithIds.put(R.id.cardWaist, 30);
        sViewsWithIds.put(R.id.cardBreast, 31);
        sViewsWithIds.put(R.id.cardHips, 32);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final android.view.View mboundView10;
    @NonNull
    private final android.view.View mboundView11;
    @NonNull
    private final android.view.View mboundView12;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView13;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView15;
    @NonNull
    private final android.view.View mboundView16;
    @NonNull
    private final android.view.View mboundView17;
    @NonNull
    private final android.view.View mboundView18;
    @NonNull
    private final android.view.View mboundView19;
    @NonNull
    private final android.view.View mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView20;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView22;
    @NonNull
    private final android.view.View mboundView23;
    @NonNull
    private final android.view.View mboundView24;
    @NonNull
    private final android.view.View mboundView25;
    @NonNull
    private final android.view.View mboundView26;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView27;
    @NonNull
    private final android.view.View mboundView3;
    @NonNull
    private final android.view.View mboundView4;
    @NonNull
    private final android.view.View mboundView5;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView6;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView8;
    @NonNull
    private final android.view.View mboundView9;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutProgressResultBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 33, sIncludes, sViewsWithIds));
    }
    private LayoutProgressResultBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[21]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[28]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (androidx.cardview.widget.CardView) bindings[31]
            , (androidx.cardview.widget.CardView) bindings[32]
            , (androidx.cardview.widget.CardView) bindings[30]
            , (androidx.cardview.widget.CardView) bindings[29]
            );
        this.btnMoreDetailsBreast.setTag(null);
        this.btnMoreDetailsHips.setTag(null);
        this.btnMoreDetailsWaist.setTag(null);
        this.btnMoreDetailsWeight.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView10 = (android.view.View) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView11 = (android.view.View) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView12 = (android.view.View) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView13 = (androidx.appcompat.widget.AppCompatTextView) bindings[13];
        this.mboundView13.setTag(null);
        this.mboundView15 = (androidx.appcompat.widget.AppCompatTextView) bindings[15];
        this.mboundView15.setTag(null);
        this.mboundView16 = (android.view.View) bindings[16];
        this.mboundView16.setTag(null);
        this.mboundView17 = (android.view.View) bindings[17];
        this.mboundView17.setTag(null);
        this.mboundView18 = (android.view.View) bindings[18];
        this.mboundView18.setTag(null);
        this.mboundView19 = (android.view.View) bindings[19];
        this.mboundView19.setTag(null);
        this.mboundView2 = (android.view.View) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView20 = (androidx.appcompat.widget.AppCompatTextView) bindings[20];
        this.mboundView20.setTag(null);
        this.mboundView22 = (androidx.appcompat.widget.AppCompatTextView) bindings[22];
        this.mboundView22.setTag(null);
        this.mboundView23 = (android.view.View) bindings[23];
        this.mboundView23.setTag(null);
        this.mboundView24 = (android.view.View) bindings[24];
        this.mboundView24.setTag(null);
        this.mboundView25 = (android.view.View) bindings[25];
        this.mboundView25.setTag(null);
        this.mboundView26 = (android.view.View) bindings[26];
        this.mboundView26.setTag(null);
        this.mboundView27 = (androidx.appcompat.widget.AppCompatTextView) bindings[27];
        this.mboundView27.setTag(null);
        this.mboundView3 = (android.view.View) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.view.View) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (android.view.View) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView8 = (androidx.appcompat.widget.AppCompatTextView) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (android.view.View) bindings[9];
        this.mboundView9.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
                mDirtyFlags_1 = 0x0L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0 || mDirtyFlags_1 != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.progress == variableId) {
            setProgress((fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setProgress(@Nullable fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel Progress) {
        this.mProgress = Progress;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.progress);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        long dirtyFlags_1 = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
            dirtyFlags_1 = mDirtyFlags_1;
            mDirtyFlags_1 = 0;
        }
        float progressIsEmptyMeasurementTypeWEIGHTInt3Float05FProgressMeasurementGraphTypeWEIGHTInt3 = 0f;
        float progressIsEmptyMeasurementTypeWAISTInt3Float05FProgressMeasurementGraphTypeWAISTInt3 = 0f;
        java.lang.Float progressMeasurementGraphTypeBREASTInt4 = null;
        java.lang.String progressDiffMeasurementTypeWEIGHT = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeBREASTInt3MboundView18AndroidDrawableBgProgressEmptyMboundView18AndroidDrawableBgProgressBreast = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeBREASTInt1MboundView16AndroidDrawableBgProgressEmptyMboundView16AndroidDrawableBgProgressBreast = null;
        float progressIsEmptyMeasurementTypeHIPSInt4Float05FProgressMeasurementGraphTypeHIPSInt4 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt1 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt2 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt3 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt4 = 0f;
        java.lang.Float progressMeasurementGraphTypeWEIGHTInt3 = null;
        java.lang.Float progressMeasurementGraphTypeWAISTInt4 = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeHIPSInt2MboundView24AndroidDrawableBgProgressEmptyMboundView24AndroidDrawableBgProgressHips = null;
        float progressIsEmptyMeasurementTypeBREASTInt4Float05FProgressMeasurementGraphTypeBREASTInt4 = 0f;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeBREASTInt2MboundView17AndroidDrawableBgProgressEmptyMboundView17AndroidDrawableBgProgressBreast = null;
        java.lang.String mboundView22AndroidStringCentimetersProgressDiffMeasurementTypeHIPS = null;
        java.lang.Float progressMeasurementGraphTypeHIPSInt2 = null;
        boolean progressIsEmptyDiffMeasurementTypeWAIST = false;
        boolean progressIsEmptyMeasurementTypeWEIGHTInt1 = false;
        float progressIsEmptyMeasurementTypeWAISTInt4Float05FProgressMeasurementGraphTypeWAISTInt4 = 0f;
        float progressIsEmptyMeasurementTypeWEIGHTInt2Float05FProgressMeasurementGraphTypeWEIGHTInt2 = 0f;
        java.lang.String progressLastMeasurementStrTypeWEIGHT = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeBREASTInt4MboundView19AndroidDrawableBgProgressEmptyMboundView19AndroidDrawableBgProgressBreast = null;
        java.lang.Float progressMeasurementGraphTypeWAISTInt2 = null;
        float progressIsEmptyMeasurementTypeBREASTInt3Float05FProgressMeasurementGraphTypeBREASTInt3 = 0f;
        float progressIsEmptyMeasurementTypeHIPSInt1Float05FProgressMeasurementGraphTypeHIPSInt1 = 0f;
        java.lang.String progressDiffMeasurementTypeHIPS = null;
        java.lang.String progressLastMeasurementStrTypeHIPS = null;
        int progressIsEmptyDiffMeasurementTypeWEIGHTViewINVISIBLEViewVISIBLE = 0;
        boolean progressIsEmptyDiffMeasurementTypeBREAST = false;
        boolean progressIsEmptyMeasurementTypeHIPSInt2 = false;
        boolean progressIsEmptyMeasurementTypeWAISTInt2 = false;
        boolean progressIsEmptyMeasurementTypeBREASTInt4 = false;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeHIPSInt1MboundView23AndroidDrawableBgProgressEmptyMboundView23AndroidDrawableBgProgressHips = null;
        int progressIsEmptyWeeksViewGONEViewVISIBLE = 0;
        java.lang.String progressLastMeasurementStrTypeBREAST = null;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt4 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt1 = 0f;
        boolean progressIsEmptyMeasurementTypeWEIGHTInt2 = false;
        float progressIsEmptyMeasurementTypeHIPSInt2Float05FProgressMeasurementGraphTypeHIPSInt2 = 0f;
        java.lang.String mboundView27AndroidStringCentimetersProgressLastMeasurementStrTypeHIPS = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWAISTInt1MboundView9AndroidDrawableBgProgressEmptyMboundView9AndroidDrawableBgProgressWaist = null;
        boolean progressIsEmptyMeasurementTypeWAISTInt4 = false;
        boolean progressIsEmptyMeasurementTypeHIPSInt4 = false;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeHIPSInt3MboundView25AndroidDrawableBgProgressEmptyMboundView25AndroidDrawableBgProgressHips = null;
        java.lang.Float progressMeasurementGraphTypeWEIGHTInt1 = null;
        float progressIsEmptyMeasurementTypeWAISTInt2Float05FProgressMeasurementGraphTypeWAISTInt2 = 0f;
        boolean progressIsEmptyMeasurementTypeBREASTInt2 = false;
        java.lang.String progressLastMeasurementStrTypeWAIST = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeHIPSInt4MboundView26AndroidDrawableBgProgressEmptyMboundView26AndroidDrawableBgProgressHips = null;
        float progressIsEmptyMeasurementTypeBREASTInt1Float05FProgressMeasurementGraphTypeBREASTInt1 = 0f;
        int progressIsEmptyDiffMeasurementTypeBREASTViewINVISIBLEViewVISIBLE = 0;
        java.lang.Float progressMeasurementGraphTypeBREASTInt1 = null;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt1 = 0f;
        boolean progressIsEmptyMeasurementTypeWEIGHTInt4 = false;
        float progressIsEmptyMeasurementTypeWAISTInt1Float05FProgressMeasurementGraphTypeWAISTInt1 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt3 = 0f;
        java.lang.Float progressMeasurementGraphTypeWAISTInt3 = null;
        java.lang.Float progressMeasurementGraphTypeWEIGHTInt4 = null;
        int progressIsEmptyDiffMeasurementTypeWAISTViewINVISIBLEViewVISIBLE = 0;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWEIGHTInt3MboundView4AndroidDrawableBgProgressEmptyMboundView4AndroidDrawableBgProgressWeight = null;
        boolean progressIsEmptyDiffMeasurementTypeWEIGHT = false;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWEIGHTInt4MboundView5AndroidDrawableBgProgressEmptyMboundView5AndroidDrawableBgProgressWeight = null;
        java.lang.Float progressMeasurementGraphTypeBREASTInt3 = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWEIGHTInt1MboundView2AndroidDrawableBgProgressEmptyMboundView2AndroidDrawableBgProgressWeight = null;
        java.lang.Float progressMeasurementGraphTypeHIPSInt1 = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWEIGHTInt2MboundView3AndroidDrawableBgProgressEmptyMboundView3AndroidDrawableBgProgressWeight = null;
        boolean progressIsEmptyWeeks = false;
        java.lang.Float progressMeasurementGraphTypeWAISTInt1 = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWAISTInt4MboundView12AndroidDrawableBgProgressEmptyMboundView12AndroidDrawableBgProgressWaist = null;
        java.lang.String mboundView15AndroidStringCentimetersProgressDiffMeasurementTypeBREAST = null;
        java.lang.String mboundView20AndroidStringCentimetersProgressLastMeasurementStrTypeBREAST = null;
        java.lang.Float progressMeasurementGraphTypeHIPSInt3 = null;
        boolean progressIsEmptyMeasurementTypeHIPSInt1 = false;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt2 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt4 = 0f;
        boolean progressIsEmptyMeasurementTypeWAISTInt1 = false;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt2 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt4 = 0f;
        java.lang.String mboundView6AndroidStringKilogramsProgressLastMeasurementStrTypeWEIGHT = null;
        java.lang.Float progressMeasurementGraphTypeHIPSInt4 = null;
        boolean progressIsEmptyMeasurementTypeWEIGHTInt3 = false;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWAISTInt3MboundView11AndroidDrawableBgProgressEmptyMboundView11AndroidDrawableBgProgressWaist = null;
        fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel progress = mProgress;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt3 = 0f;
        float progressIsEmptyMeasurementTypeWEIGHTInt1Float05FProgressMeasurementGraphTypeWEIGHTInt1 = 0f;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt2 = 0f;
        boolean progressIsEmptyMeasurementTypeHIPSInt3 = false;
        float progressIsEmptyMeasurementTypeBREASTInt2Float05FProgressMeasurementGraphTypeBREASTInt2 = 0f;
        java.lang.String progressDiffMeasurementTypeBREAST = null;
        java.lang.String progressDiffMeasurementTypeWAIST = null;
        java.lang.String mboundView8AndroidStringCentimetersProgressDiffMeasurementTypeWAIST = null;
        boolean progressIsEmptyMeasurementTypeWAISTInt3 = false;
        int progressIsEmptyDiffMeasurementTypeHIPSViewINVISIBLEViewVISIBLE = 0;
        boolean progressIsEmptyMeasurementTypeBREASTInt1 = false;
        float progressIsEmptyMeasurementTypeWEIGHTInt4Float05FProgressMeasurementGraphTypeWEIGHTInt4 = 0f;
        java.lang.Float progressMeasurementGraphTypeBREASTInt2 = null;
        android.graphics.drawable.Drawable progressIsEmptyMeasurementTypeWAISTInt2MboundView10AndroidDrawableBgProgressEmptyMboundView10AndroidDrawableBgProgressWaist = null;
        float progressIsEmptyMeasurementTypeHIPSInt3Float05FProgressMeasurementGraphTypeHIPSInt3 = 0f;
        java.lang.String mboundView1AndroidStringKilogramsProgressDiffMeasurementTypeWEIGHT = null;
        java.lang.String mboundView13AndroidStringCentimetersProgressLastMeasurementStrTypeWAIST = null;
        boolean progressIsEmptyDiffMeasurementTypeHIPS = false;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt3 = 0f;
        java.lang.Float progressMeasurementGraphTypeWEIGHTInt2 = null;
        boolean progressIsEmptyMeasurementTypeBREASTInt3 = false;
        float androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt1 = 0f;

        if ((dirtyFlags & 0x3L) != 0) {



                if (progress != null) {
                    // read progress.diffMeasurement(Type.WEIGHT)
                    progressDiffMeasurementTypeWEIGHT = progress.diffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT);
                    // read progress.isEmptyDiffMeasurement(Type.WAIST)
                    progressIsEmptyDiffMeasurementTypeWAIST = progress.isEmptyDiffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST);
                    // read progress.isEmptyMeasurement(Type.WEIGHT, 1)
                    progressIsEmptyMeasurementTypeWEIGHTInt1 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 1);
                    // read progress.lastMeasurementStr(Type.WEIGHT)
                    progressLastMeasurementStrTypeWEIGHT = progress.lastMeasurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT);
                    // read progress.diffMeasurement(Type.HIPS)
                    progressDiffMeasurementTypeHIPS = progress.diffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS);
                    // read progress.lastMeasurementStr(Type.HIPS)
                    progressLastMeasurementStrTypeHIPS = progress.lastMeasurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS);
                    // read progress.isEmptyDiffMeasurement(Type.BREAST)
                    progressIsEmptyDiffMeasurementTypeBREAST = progress.isEmptyDiffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST);
                    // read progress.isEmptyMeasurement(Type.HIPS, 2)
                    progressIsEmptyMeasurementTypeHIPSInt2 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 2);
                    // read progress.isEmptyMeasurement(Type.WAIST, 2)
                    progressIsEmptyMeasurementTypeWAISTInt2 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 2);
                    // read progress.isEmptyMeasurement(Type.BREAST, 4)
                    progressIsEmptyMeasurementTypeBREASTInt4 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 4);
                    // read progress.lastMeasurementStr(Type.BREAST)
                    progressLastMeasurementStrTypeBREAST = progress.lastMeasurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST);
                    // read progress.isEmptyMeasurement(Type.WEIGHT, 2)
                    progressIsEmptyMeasurementTypeWEIGHTInt2 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 2);
                    // read progress.isEmptyMeasurement(Type.WAIST, 4)
                    progressIsEmptyMeasurementTypeWAISTInt4 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 4);
                    // read progress.isEmptyMeasurement(Type.HIPS, 4)
                    progressIsEmptyMeasurementTypeHIPSInt4 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 4);
                    // read progress.isEmptyMeasurement(Type.BREAST, 2)
                    progressIsEmptyMeasurementTypeBREASTInt2 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 2);
                    // read progress.lastMeasurementStr(Type.WAIST)
                    progressLastMeasurementStrTypeWAIST = progress.lastMeasurementStr(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST);
                    // read progress.isEmptyMeasurement(Type.WEIGHT, 4)
                    progressIsEmptyMeasurementTypeWEIGHTInt4 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 4);
                    // read progress.isEmptyDiffMeasurement(Type.WEIGHT)
                    progressIsEmptyDiffMeasurementTypeWEIGHT = progress.isEmptyDiffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT);
                    // read progress.isEmptyWeeks
                    progressIsEmptyWeeks = progress.isEmptyWeeks();
                    // read progress.isEmptyMeasurement(Type.HIPS, 1)
                    progressIsEmptyMeasurementTypeHIPSInt1 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 1);
                    // read progress.isEmptyMeasurement(Type.WAIST, 1)
                    progressIsEmptyMeasurementTypeWAISTInt1 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 1);
                    // read progress.isEmptyMeasurement(Type.WEIGHT, 3)
                    progressIsEmptyMeasurementTypeWEIGHTInt3 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 3);
                    // read progress.isEmptyMeasurement(Type.HIPS, 3)
                    progressIsEmptyMeasurementTypeHIPSInt3 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 3);
                    // read progress.diffMeasurement(Type.BREAST)
                    progressDiffMeasurementTypeBREAST = progress.diffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST);
                    // read progress.diffMeasurement(Type.WAIST)
                    progressDiffMeasurementTypeWAIST = progress.diffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST);
                    // read progress.isEmptyMeasurement(Type.WAIST, 3)
                    progressIsEmptyMeasurementTypeWAISTInt3 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 3);
                    // read progress.isEmptyMeasurement(Type.BREAST, 1)
                    progressIsEmptyMeasurementTypeBREASTInt1 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 1);
                    // read progress.isEmptyDiffMeasurement(Type.HIPS)
                    progressIsEmptyDiffMeasurementTypeHIPS = progress.isEmptyDiffMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS);
                    // read progress.isEmptyMeasurement(Type.BREAST, 3)
                    progressIsEmptyMeasurementTypeBREASTInt3 = progress.isEmptyMeasurement(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 3);
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyDiffMeasurementTypeWAIST) {
                        dirtyFlags |= 0x8000000000000L;
                }
                else {
                        dirtyFlags |= 0x4000000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWEIGHTInt1) {
                        dirtyFlags |= 0x200000000000000L;
                        dirtyFlags_1 |= 0x2L;
                }
                else {
                        dirtyFlags |= 0x100000000000000L;
                        dirtyFlags_1 |= 0x1L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyDiffMeasurementTypeBREAST) {
                        dirtyFlags |= 0x800000000000L;
                }
                else {
                        dirtyFlags |= 0x400000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeHIPSInt2) {
                        dirtyFlags |= 0x2000L;
                        dirtyFlags |= 0x800000000L;
                }
                else {
                        dirtyFlags |= 0x1000L;
                        dirtyFlags |= 0x400000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWAISTInt2) {
                        dirtyFlags |= 0x20000000000L;
                        dirtyFlags_1 |= 0x200L;
                }
                else {
                        dirtyFlags |= 0x10000000000L;
                        dirtyFlags_1 |= 0x100L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeBREASTInt4) {
                        dirtyFlags |= 0x8000L;
                        dirtyFlags |= 0x800000L;
                }
                else {
                        dirtyFlags |= 0x4000L;
                        dirtyFlags |= 0x400000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWEIGHTInt2) {
                        dirtyFlags |= 0x200000L;
                        dirtyFlags |= 0x800000000000000L;
                }
                else {
                        dirtyFlags |= 0x100000L;
                        dirtyFlags |= 0x400000000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWAISTInt4) {
                        dirtyFlags |= 0x80000L;
                        dirtyFlags |= 0x2000000000000000L;
                }
                else {
                        dirtyFlags |= 0x40000L;
                        dirtyFlags |= 0x1000000000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeHIPSInt4) {
                        dirtyFlags |= 0x800L;
                        dirtyFlags |= 0x80000000000L;
                }
                else {
                        dirtyFlags |= 0x400L;
                        dirtyFlags |= 0x40000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeBREASTInt2) {
                        dirtyFlags |= 0x20000L;
                        dirtyFlags_1 |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x10000L;
                        dirtyFlags_1 |= 0x4L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWEIGHTInt4) {
                        dirtyFlags |= 0x80000000000000L;
                        dirtyFlags_1 |= 0x80L;
                }
                else {
                        dirtyFlags |= 0x40000000000000L;
                        dirtyFlags_1 |= 0x40L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyDiffMeasurementTypeWEIGHT) {
                        dirtyFlags |= 0x20000000L;
                }
                else {
                        dirtyFlags |= 0x10000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyWeeks) {
                        dirtyFlags |= 0x200000000L;
                }
                else {
                        dirtyFlags |= 0x100000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeHIPSInt1) {
                        dirtyFlags |= 0x8000000L;
                        dirtyFlags |= 0x80000000L;
                }
                else {
                        dirtyFlags |= 0x4000000L;
                        dirtyFlags |= 0x40000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWAISTInt1) {
                        dirtyFlags |= 0x2000000000L;
                        dirtyFlags |= 0x2000000000000L;
                }
                else {
                        dirtyFlags |= 0x1000000000L;
                        dirtyFlags |= 0x1000000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWEIGHTInt3) {
                        dirtyFlags |= 0x8L;
                        dirtyFlags |= 0x20000000000000L;
                }
                else {
                        dirtyFlags |= 0x4L;
                        dirtyFlags |= 0x10000000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeHIPSInt3) {
                        dirtyFlags |= 0x8000000000L;
                        dirtyFlags_1 |= 0x800L;
                }
                else {
                        dirtyFlags |= 0x4000000000L;
                        dirtyFlags_1 |= 0x400L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeWAISTInt3) {
                        dirtyFlags |= 0x20L;
                        dirtyFlags |= 0x8000000000000000L;
                }
                else {
                        dirtyFlags |= 0x10L;
                        dirtyFlags |= 0x4000000000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeBREASTInt1) {
                        dirtyFlags |= 0x200L;
                        dirtyFlags |= 0x200000000000L;
                }
                else {
                        dirtyFlags |= 0x100L;
                        dirtyFlags |= 0x100000000000L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyDiffMeasurementTypeHIPS) {
                        dirtyFlags_1 |= 0x20L;
                }
                else {
                        dirtyFlags_1 |= 0x10L;
                }
            }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsEmptyMeasurementTypeBREASTInt3) {
                        dirtyFlags |= 0x80L;
                        dirtyFlags |= 0x2000000L;
                }
                else {
                        dirtyFlags |= 0x40L;
                        dirtyFlags |= 0x1000000L;
                }
            }


                // read @android:string/kilograms
                mboundView1AndroidStringKilogramsProgressDiffMeasurementTypeWEIGHT = mboundView1.getResources().getString(R.string.kilograms, progressDiffMeasurementTypeWEIGHT);
                // read progress.isEmptyDiffMeasurement(Type.WAIST) ? View.INVISIBLE : View.VISIBLE
                progressIsEmptyDiffMeasurementTypeWAISTViewINVISIBLEViewVISIBLE = ((progressIsEmptyDiffMeasurementTypeWAIST) ? (android.view.View.INVISIBLE) : (android.view.View.VISIBLE));
                // read progress.isEmptyMeasurement(Type.WEIGHT, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
                progressIsEmptyMeasurementTypeWEIGHTInt1MboundView2AndroidDrawableBgProgressEmptyMboundView2AndroidDrawableBgProgressWeight = ((progressIsEmptyMeasurementTypeWEIGHTInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView2.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView2.getContext(), R.drawable.bg_progress_weight)));
                // read @android:string/kilograms
                mboundView6AndroidStringKilogramsProgressLastMeasurementStrTypeWEIGHT = mboundView6.getResources().getString(R.string.kilograms, progressLastMeasurementStrTypeWEIGHT);
                // read @android:string/centimeters
                mboundView22AndroidStringCentimetersProgressDiffMeasurementTypeHIPS = mboundView22.getResources().getString(R.string.centimeters, progressDiffMeasurementTypeHIPS);
                // read @android:string/centimeters
                mboundView27AndroidStringCentimetersProgressLastMeasurementStrTypeHIPS = mboundView27.getResources().getString(R.string.centimeters, progressLastMeasurementStrTypeHIPS);
                // read progress.isEmptyDiffMeasurement(Type.BREAST) ? View.INVISIBLE : View.VISIBLE
                progressIsEmptyDiffMeasurementTypeBREASTViewINVISIBLEViewVISIBLE = ((progressIsEmptyDiffMeasurementTypeBREAST) ? (android.view.View.INVISIBLE) : (android.view.View.VISIBLE));
                // read progress.isEmptyMeasurement(Type.HIPS, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
                progressIsEmptyMeasurementTypeHIPSInt2MboundView24AndroidDrawableBgProgressEmptyMboundView24AndroidDrawableBgProgressHips = ((progressIsEmptyMeasurementTypeHIPSInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView24.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView24.getContext(), R.drawable.bg_progress_hips)));
                // read progress.isEmptyMeasurement(Type.WAIST, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
                progressIsEmptyMeasurementTypeWAISTInt2MboundView10AndroidDrawableBgProgressEmptyMboundView10AndroidDrawableBgProgressWaist = ((progressIsEmptyMeasurementTypeWAISTInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView10.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView10.getContext(), R.drawable.bg_progress_waist)));
                // read progress.isEmptyMeasurement(Type.BREAST, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
                progressIsEmptyMeasurementTypeBREASTInt4MboundView19AndroidDrawableBgProgressEmptyMboundView19AndroidDrawableBgProgressBreast = ((progressIsEmptyMeasurementTypeBREASTInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView19.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView19.getContext(), R.drawable.bg_progress_breast)));
                // read @android:string/centimeters
                mboundView20AndroidStringCentimetersProgressLastMeasurementStrTypeBREAST = mboundView20.getResources().getString(R.string.centimeters, progressLastMeasurementStrTypeBREAST);
                // read progress.isEmptyMeasurement(Type.WEIGHT, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
                progressIsEmptyMeasurementTypeWEIGHTInt2MboundView3AndroidDrawableBgProgressEmptyMboundView3AndroidDrawableBgProgressWeight = ((progressIsEmptyMeasurementTypeWEIGHTInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView3.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView3.getContext(), R.drawable.bg_progress_weight)));
                // read progress.isEmptyMeasurement(Type.WAIST, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
                progressIsEmptyMeasurementTypeWAISTInt4MboundView12AndroidDrawableBgProgressEmptyMboundView12AndroidDrawableBgProgressWaist = ((progressIsEmptyMeasurementTypeWAISTInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView12.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView12.getContext(), R.drawable.bg_progress_waist)));
                // read progress.isEmptyMeasurement(Type.HIPS, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
                progressIsEmptyMeasurementTypeHIPSInt4MboundView26AndroidDrawableBgProgressEmptyMboundView26AndroidDrawableBgProgressHips = ((progressIsEmptyMeasurementTypeHIPSInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView26.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView26.getContext(), R.drawable.bg_progress_hips)));
                // read progress.isEmptyMeasurement(Type.BREAST, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
                progressIsEmptyMeasurementTypeBREASTInt2MboundView17AndroidDrawableBgProgressEmptyMboundView17AndroidDrawableBgProgressBreast = ((progressIsEmptyMeasurementTypeBREASTInt2) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView17.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView17.getContext(), R.drawable.bg_progress_breast)));
                // read @android:string/centimeters
                mboundView13AndroidStringCentimetersProgressLastMeasurementStrTypeWAIST = mboundView13.getResources().getString(R.string.centimeters, progressLastMeasurementStrTypeWAIST);
                // read progress.isEmptyMeasurement(Type.WEIGHT, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
                progressIsEmptyMeasurementTypeWEIGHTInt4MboundView5AndroidDrawableBgProgressEmptyMboundView5AndroidDrawableBgProgressWeight = ((progressIsEmptyMeasurementTypeWEIGHTInt4) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView5.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView5.getContext(), R.drawable.bg_progress_weight)));
                // read progress.isEmptyDiffMeasurement(Type.WEIGHT) ? View.INVISIBLE : View.VISIBLE
                progressIsEmptyDiffMeasurementTypeWEIGHTViewINVISIBLEViewVISIBLE = ((progressIsEmptyDiffMeasurementTypeWEIGHT) ? (android.view.View.INVISIBLE) : (android.view.View.VISIBLE));
                // read progress.isEmptyWeeks ? View.GONE : View.VISIBLE
                progressIsEmptyWeeksViewGONEViewVISIBLE = ((progressIsEmptyWeeks) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                // read progress.isEmptyMeasurement(Type.HIPS, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
                progressIsEmptyMeasurementTypeHIPSInt1MboundView23AndroidDrawableBgProgressEmptyMboundView23AndroidDrawableBgProgressHips = ((progressIsEmptyMeasurementTypeHIPSInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView23.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView23.getContext(), R.drawable.bg_progress_hips)));
                // read progress.isEmptyMeasurement(Type.WAIST, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
                progressIsEmptyMeasurementTypeWAISTInt1MboundView9AndroidDrawableBgProgressEmptyMboundView9AndroidDrawableBgProgressWaist = ((progressIsEmptyMeasurementTypeWAISTInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView9.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView9.getContext(), R.drawable.bg_progress_waist)));
                // read progress.isEmptyMeasurement(Type.WEIGHT, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
                progressIsEmptyMeasurementTypeWEIGHTInt3MboundView4AndroidDrawableBgProgressEmptyMboundView4AndroidDrawableBgProgressWeight = ((progressIsEmptyMeasurementTypeWEIGHTInt3) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView4.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView4.getContext(), R.drawable.bg_progress_weight)));
                // read progress.isEmptyMeasurement(Type.HIPS, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
                progressIsEmptyMeasurementTypeHIPSInt3MboundView25AndroidDrawableBgProgressEmptyMboundView25AndroidDrawableBgProgressHips = ((progressIsEmptyMeasurementTypeHIPSInt3) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView25.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView25.getContext(), R.drawable.bg_progress_hips)));
                // read @android:string/centimeters
                mboundView15AndroidStringCentimetersProgressDiffMeasurementTypeBREAST = mboundView15.getResources().getString(R.string.centimeters, progressDiffMeasurementTypeBREAST);
                // read @android:string/centimeters
                mboundView8AndroidStringCentimetersProgressDiffMeasurementTypeWAIST = mboundView8.getResources().getString(R.string.centimeters, progressDiffMeasurementTypeWAIST);
                // read progress.isEmptyMeasurement(Type.WAIST, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
                progressIsEmptyMeasurementTypeWAISTInt3MboundView11AndroidDrawableBgProgressEmptyMboundView11AndroidDrawableBgProgressWaist = ((progressIsEmptyMeasurementTypeWAISTInt3) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView11.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView11.getContext(), R.drawable.bg_progress_waist)));
                // read progress.isEmptyMeasurement(Type.BREAST, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
                progressIsEmptyMeasurementTypeBREASTInt1MboundView16AndroidDrawableBgProgressEmptyMboundView16AndroidDrawableBgProgressBreast = ((progressIsEmptyMeasurementTypeBREASTInt1) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView16.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView16.getContext(), R.drawable.bg_progress_breast)));
                // read progress.isEmptyDiffMeasurement(Type.HIPS) ? View.INVISIBLE : View.VISIBLE
                progressIsEmptyDiffMeasurementTypeHIPSViewINVISIBLEViewVISIBLE = ((progressIsEmptyDiffMeasurementTypeHIPS) ? (android.view.View.INVISIBLE) : (android.view.View.VISIBLE));
                // read progress.isEmptyMeasurement(Type.BREAST, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
                progressIsEmptyMeasurementTypeBREASTInt3MboundView18AndroidDrawableBgProgressEmptyMboundView18AndroidDrawableBgProgressBreast = ((progressIsEmptyMeasurementTypeBREASTInt3) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView18.getContext(), R.drawable.bg_progress_empty)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(mboundView18.getContext(), R.drawable.bg_progress_breast)));
        }
        // batch finished

        if ((dirtyFlags & 0x4000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.BREAST, 4)
                    progressMeasurementGraphTypeBREASTInt4 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 4);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 4))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt4 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeBREASTInt4);
        }
        if ((dirtyFlags & 0x4L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WEIGHT, 3)
                    progressMeasurementGraphTypeWEIGHTInt3 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 3);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 3))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt3 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWEIGHTInt3);
        }
        if ((dirtyFlags & 0x40000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WAIST, 4)
                    progressMeasurementGraphTypeWAISTInt4 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 4);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 4))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt4 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWAISTInt4);
        }
        if ((dirtyFlags & 0x400000000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.HIPS, 2)
                    progressMeasurementGraphTypeHIPSInt2 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 2);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 2))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt2 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeHIPSInt2);
        }
        if ((dirtyFlags & 0x10000000000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WAIST, 2)
                    progressMeasurementGraphTypeWAISTInt2 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 2);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 2))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt2 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWAISTInt2);
        }
        if ((dirtyFlags_1 & 0x1L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WEIGHT, 1)
                    progressMeasurementGraphTypeWEIGHTInt1 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 1);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 1))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt1 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWEIGHTInt1);
        }
        if ((dirtyFlags & 0x100000000000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.BREAST, 1)
                    progressMeasurementGraphTypeBREASTInt1 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 1);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 1))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt1 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeBREASTInt1);
        }
        if ((dirtyFlags & 0x10L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WAIST, 3)
                    progressMeasurementGraphTypeWAISTInt3 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 3);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 3))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt3 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWAISTInt3);
        }
        if ((dirtyFlags_1 & 0x40L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WEIGHT, 4)
                    progressMeasurementGraphTypeWEIGHTInt4 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 4);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 4))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt4 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWEIGHTInt4);
        }
        if ((dirtyFlags & 0x1000000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.BREAST, 3)
                    progressMeasurementGraphTypeBREASTInt3 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 3);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 3))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt3 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeBREASTInt3);
        }
        if ((dirtyFlags & 0x4000000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.HIPS, 1)
                    progressMeasurementGraphTypeHIPSInt1 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 1);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 1))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt1 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeHIPSInt1);
        }
        if ((dirtyFlags & 0x1000000000000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WAIST, 1)
                    progressMeasurementGraphTypeWAISTInt1 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WAIST, 1);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 1))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt1 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWAISTInt1);
        }
        if ((dirtyFlags_1 & 0x400L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.HIPS, 3)
                    progressMeasurementGraphTypeHIPSInt3 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 3);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 3))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt3 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeHIPSInt3);
        }
        if ((dirtyFlags & 0x400L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.HIPS, 4)
                    progressMeasurementGraphTypeHIPSInt4 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.HIPS, 4);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 4))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt4 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeHIPSInt4);
        }
        if ((dirtyFlags_1 & 0x4L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.BREAST, 2)
                    progressMeasurementGraphTypeBREASTInt2 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.BREAST, 2);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 2))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt2 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeBREASTInt2);
        }
        if ((dirtyFlags & 0x100000L) != 0) {

                if (progress != null) {
                    // read progress.measurementGraph(Type.WEIGHT, 2)
                    progressMeasurementGraphTypeWEIGHTInt2 = progress.measurementGraph(fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel.Type.WEIGHT, 2);
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 2))
                androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt2 = androidx.databinding.ViewDataBinding.safeUnbox(progressMeasurementGraphTypeWEIGHTInt2);
        }

        if ((dirtyFlags & 0x3L) != 0) {

                // read progress.isEmptyMeasurement(Type.WEIGHT, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 3))
                progressIsEmptyMeasurementTypeWEIGHTInt3Float05FProgressMeasurementGraphTypeWEIGHTInt3 = ((progressIsEmptyMeasurementTypeWEIGHTInt3) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt3));
                // read progress.isEmptyMeasurement(Type.WAIST, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 3))
                progressIsEmptyMeasurementTypeWAISTInt3Float05FProgressMeasurementGraphTypeWAISTInt3 = ((progressIsEmptyMeasurementTypeWAISTInt3) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt3));
                // read progress.isEmptyMeasurement(Type.HIPS, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 4))
                progressIsEmptyMeasurementTypeHIPSInt4Float05FProgressMeasurementGraphTypeHIPSInt4 = ((progressIsEmptyMeasurementTypeHIPSInt4) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt4));
                // read progress.isEmptyMeasurement(Type.BREAST, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 4))
                progressIsEmptyMeasurementTypeBREASTInt4Float05FProgressMeasurementGraphTypeBREASTInt4 = ((progressIsEmptyMeasurementTypeBREASTInt4) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt4));
                // read progress.isEmptyMeasurement(Type.WAIST, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 4))
                progressIsEmptyMeasurementTypeWAISTInt4Float05FProgressMeasurementGraphTypeWAISTInt4 = ((progressIsEmptyMeasurementTypeWAISTInt4) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt4));
                // read progress.isEmptyMeasurement(Type.WEIGHT, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 2))
                progressIsEmptyMeasurementTypeWEIGHTInt2Float05FProgressMeasurementGraphTypeWEIGHTInt2 = ((progressIsEmptyMeasurementTypeWEIGHTInt2) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt2));
                // read progress.isEmptyMeasurement(Type.BREAST, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 3))
                progressIsEmptyMeasurementTypeBREASTInt3Float05FProgressMeasurementGraphTypeBREASTInt3 = ((progressIsEmptyMeasurementTypeBREASTInt3) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt3));
                // read progress.isEmptyMeasurement(Type.HIPS, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 1))
                progressIsEmptyMeasurementTypeHIPSInt1Float05FProgressMeasurementGraphTypeHIPSInt1 = ((progressIsEmptyMeasurementTypeHIPSInt1) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt1));
                // read progress.isEmptyMeasurement(Type.HIPS, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 2))
                progressIsEmptyMeasurementTypeHIPSInt2Float05FProgressMeasurementGraphTypeHIPSInt2 = ((progressIsEmptyMeasurementTypeHIPSInt2) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt2));
                // read progress.isEmptyMeasurement(Type.WAIST, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 2))
                progressIsEmptyMeasurementTypeWAISTInt2Float05FProgressMeasurementGraphTypeWAISTInt2 = ((progressIsEmptyMeasurementTypeWAISTInt2) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt2));
                // read progress.isEmptyMeasurement(Type.BREAST, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 1))
                progressIsEmptyMeasurementTypeBREASTInt1Float05FProgressMeasurementGraphTypeBREASTInt1 = ((progressIsEmptyMeasurementTypeBREASTInt1) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt1));
                // read progress.isEmptyMeasurement(Type.WAIST, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 1))
                progressIsEmptyMeasurementTypeWAISTInt1Float05FProgressMeasurementGraphTypeWAISTInt1 = ((progressIsEmptyMeasurementTypeWAISTInt1) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWAISTInt1));
                // read progress.isEmptyMeasurement(Type.WEIGHT, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 1))
                progressIsEmptyMeasurementTypeWEIGHTInt1Float05FProgressMeasurementGraphTypeWEIGHTInt1 = ((progressIsEmptyMeasurementTypeWEIGHTInt1) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt1));
                // read progress.isEmptyMeasurement(Type.BREAST, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 2))
                progressIsEmptyMeasurementTypeBREASTInt2Float05FProgressMeasurementGraphTypeBREASTInt2 = ((progressIsEmptyMeasurementTypeBREASTInt2) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeBREASTInt2));
                // read progress.isEmptyMeasurement(Type.WEIGHT, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 4))
                progressIsEmptyMeasurementTypeWEIGHTInt4Float05FProgressMeasurementGraphTypeWEIGHTInt4 = ((progressIsEmptyMeasurementTypeWEIGHTInt4) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeWEIGHTInt4));
                // read progress.isEmptyMeasurement(Type.HIPS, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 3))
                progressIsEmptyMeasurementTypeHIPSInt3Float05FProgressMeasurementGraphTypeHIPSInt3 = ((progressIsEmptyMeasurementTypeHIPSInt3) ? (0.5F) : (androidxDatabindingViewDataBindingSafeUnboxProgressMeasurementGraphTypeHIPSInt3));
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.btnMoreDetailsBreast.setVisibility(progressIsEmptyWeeksViewGONEViewVISIBLE);
            this.btnMoreDetailsHips.setVisibility(progressIsEmptyWeeksViewGONEViewVISIBLE);
            this.btnMoreDetailsWaist.setVisibility(progressIsEmptyWeeksViewGONEViewVISIBLE);
            this.btnMoreDetailsWeight.setVisibility(progressIsEmptyWeeksViewGONEViewVISIBLE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, mboundView1AndroidStringKilogramsProgressDiffMeasurementTypeWEIGHT);
            this.mboundView1.setVisibility(progressIsEmptyDiffMeasurementTypeWEIGHTViewINVISIBLEViewVISIBLE);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView10, progressIsEmptyMeasurementTypeWAISTInt2MboundView10AndroidDrawableBgProgressEmptyMboundView10AndroidDrawableBgProgressWaist);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView11, progressIsEmptyMeasurementTypeWAISTInt3MboundView11AndroidDrawableBgProgressEmptyMboundView11AndroidDrawableBgProgressWaist);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView12, progressIsEmptyMeasurementTypeWAISTInt4MboundView12AndroidDrawableBgProgressEmptyMboundView12AndroidDrawableBgProgressWaist);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView13, mboundView13AndroidStringCentimetersProgressLastMeasurementStrTypeWAIST);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView15, mboundView15AndroidStringCentimetersProgressDiffMeasurementTypeBREAST);
            this.mboundView15.setVisibility(progressIsEmptyDiffMeasurementTypeBREASTViewINVISIBLEViewVISIBLE);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView16, progressIsEmptyMeasurementTypeBREASTInt1MboundView16AndroidDrawableBgProgressEmptyMboundView16AndroidDrawableBgProgressBreast);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView17, progressIsEmptyMeasurementTypeBREASTInt2MboundView17AndroidDrawableBgProgressEmptyMboundView17AndroidDrawableBgProgressBreast);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView18, progressIsEmptyMeasurementTypeBREASTInt3MboundView18AndroidDrawableBgProgressEmptyMboundView18AndroidDrawableBgProgressBreast);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView19, progressIsEmptyMeasurementTypeBREASTInt4MboundView19AndroidDrawableBgProgressEmptyMboundView19AndroidDrawableBgProgressBreast);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView2, progressIsEmptyMeasurementTypeWEIGHTInt1MboundView2AndroidDrawableBgProgressEmptyMboundView2AndroidDrawableBgProgressWeight);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView20, mboundView20AndroidStringCentimetersProgressLastMeasurementStrTypeBREAST);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView22, mboundView22AndroidStringCentimetersProgressDiffMeasurementTypeHIPS);
            this.mboundView22.setVisibility(progressIsEmptyDiffMeasurementTypeHIPSViewINVISIBLEViewVISIBLE);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView23, progressIsEmptyMeasurementTypeHIPSInt1MboundView23AndroidDrawableBgProgressEmptyMboundView23AndroidDrawableBgProgressHips);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView24, progressIsEmptyMeasurementTypeHIPSInt2MboundView24AndroidDrawableBgProgressEmptyMboundView24AndroidDrawableBgProgressHips);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView25, progressIsEmptyMeasurementTypeHIPSInt3MboundView25AndroidDrawableBgProgressEmptyMboundView25AndroidDrawableBgProgressHips);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView26, progressIsEmptyMeasurementTypeHIPSInt4MboundView26AndroidDrawableBgProgressEmptyMboundView26AndroidDrawableBgProgressHips);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView27, mboundView27AndroidStringCentimetersProgressLastMeasurementStrTypeHIPS);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView3, progressIsEmptyMeasurementTypeWEIGHTInt2MboundView3AndroidDrawableBgProgressEmptyMboundView3AndroidDrawableBgProgressWeight);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView4, progressIsEmptyMeasurementTypeWEIGHTInt3MboundView4AndroidDrawableBgProgressEmptyMboundView4AndroidDrawableBgProgressWeight);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView5, progressIsEmptyMeasurementTypeWEIGHTInt4MboundView5AndroidDrawableBgProgressEmptyMboundView5AndroidDrawableBgProgressWeight);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, mboundView6AndroidStringKilogramsProgressLastMeasurementStrTypeWEIGHT);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, mboundView8AndroidStringCentimetersProgressDiffMeasurementTypeWAIST);
            this.mboundView8.setVisibility(progressIsEmptyDiffMeasurementTypeWAISTViewINVISIBLEViewVISIBLE);
            androidx.databinding.adapters.ViewBindingAdapter.setBackground(this.mboundView9, progressIsEmptyMeasurementTypeWAISTInt1MboundView9AndroidDrawableBgProgressEmptyMboundView9AndroidDrawableBgProgressWaist);
            // api target 11
            if(getBuildSdkInt() >= 11) {

                this.mboundView10.setScaleY(progressIsEmptyMeasurementTypeWAISTInt2Float05FProgressMeasurementGraphTypeWAISTInt2);
                this.mboundView11.setScaleY(progressIsEmptyMeasurementTypeWAISTInt3Float05FProgressMeasurementGraphTypeWAISTInt3);
                this.mboundView12.setScaleY(progressIsEmptyMeasurementTypeWAISTInt4Float05FProgressMeasurementGraphTypeWAISTInt4);
                this.mboundView16.setScaleY(progressIsEmptyMeasurementTypeBREASTInt1Float05FProgressMeasurementGraphTypeBREASTInt1);
                this.mboundView17.setScaleY(progressIsEmptyMeasurementTypeBREASTInt2Float05FProgressMeasurementGraphTypeBREASTInt2);
                this.mboundView18.setScaleY(progressIsEmptyMeasurementTypeBREASTInt3Float05FProgressMeasurementGraphTypeBREASTInt3);
                this.mboundView19.setScaleY(progressIsEmptyMeasurementTypeBREASTInt4Float05FProgressMeasurementGraphTypeBREASTInt4);
                this.mboundView2.setScaleY(progressIsEmptyMeasurementTypeWEIGHTInt1Float05FProgressMeasurementGraphTypeWEIGHTInt1);
                this.mboundView23.setScaleY(progressIsEmptyMeasurementTypeHIPSInt1Float05FProgressMeasurementGraphTypeHIPSInt1);
                this.mboundView24.setScaleY(progressIsEmptyMeasurementTypeHIPSInt2Float05FProgressMeasurementGraphTypeHIPSInt2);
                this.mboundView25.setScaleY(progressIsEmptyMeasurementTypeHIPSInt3Float05FProgressMeasurementGraphTypeHIPSInt3);
                this.mboundView26.setScaleY(progressIsEmptyMeasurementTypeHIPSInt4Float05FProgressMeasurementGraphTypeHIPSInt4);
                this.mboundView3.setScaleY(progressIsEmptyMeasurementTypeWEIGHTInt2Float05FProgressMeasurementGraphTypeWEIGHTInt2);
                this.mboundView4.setScaleY(progressIsEmptyMeasurementTypeWEIGHTInt3Float05FProgressMeasurementGraphTypeWEIGHTInt3);
                this.mboundView5.setScaleY(progressIsEmptyMeasurementTypeWEIGHTInt4Float05FProgressMeasurementGraphTypeWEIGHTInt4);
                this.mboundView9.setScaleY(progressIsEmptyMeasurementTypeWAISTInt1Float05FProgressMeasurementGraphTypeWAISTInt1);
            }
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    private  long mDirtyFlags_1 = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): progress
        flag 1 (0x2L): null
        flag 2 (0x3L): progress.isEmptyMeasurement(Type.WEIGHT, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 3))
        flag 3 (0x4L): progress.isEmptyMeasurement(Type.WEIGHT, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 3))
        flag 4 (0x5L): progress.isEmptyMeasurement(Type.WAIST, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 3))
        flag 5 (0x6L): progress.isEmptyMeasurement(Type.WAIST, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 3))
        flag 6 (0x7L): progress.isEmptyMeasurement(Type.BREAST, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 7 (0x8L): progress.isEmptyMeasurement(Type.BREAST, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 8 (0x9L): progress.isEmptyMeasurement(Type.BREAST, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 9 (0xaL): progress.isEmptyMeasurement(Type.BREAST, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 10 (0xbL): progress.isEmptyMeasurement(Type.HIPS, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 4))
        flag 11 (0xcL): progress.isEmptyMeasurement(Type.HIPS, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 4))
        flag 12 (0xdL): progress.isEmptyMeasurement(Type.HIPS, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 13 (0xeL): progress.isEmptyMeasurement(Type.HIPS, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 14 (0xfL): progress.isEmptyMeasurement(Type.BREAST, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 4))
        flag 15 (0x10L): progress.isEmptyMeasurement(Type.BREAST, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 4))
        flag 16 (0x11L): progress.isEmptyMeasurement(Type.BREAST, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 17 (0x12L): progress.isEmptyMeasurement(Type.BREAST, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 18 (0x13L): progress.isEmptyMeasurement(Type.WAIST, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 4))
        flag 19 (0x14L): progress.isEmptyMeasurement(Type.WAIST, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 4))
        flag 20 (0x15L): progress.isEmptyMeasurement(Type.WEIGHT, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 2))
        flag 21 (0x16L): progress.isEmptyMeasurement(Type.WEIGHT, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 2))
        flag 22 (0x17L): progress.isEmptyMeasurement(Type.BREAST, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 23 (0x18L): progress.isEmptyMeasurement(Type.BREAST, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_breast
        flag 24 (0x19L): progress.isEmptyMeasurement(Type.BREAST, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 3))
        flag 25 (0x1aL): progress.isEmptyMeasurement(Type.BREAST, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 3))
        flag 26 (0x1bL): progress.isEmptyMeasurement(Type.HIPS, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 1))
        flag 27 (0x1cL): progress.isEmptyMeasurement(Type.HIPS, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 1))
        flag 28 (0x1dL): progress.isEmptyDiffMeasurement(Type.WEIGHT) ? View.INVISIBLE : View.VISIBLE
        flag 29 (0x1eL): progress.isEmptyDiffMeasurement(Type.WEIGHT) ? View.INVISIBLE : View.VISIBLE
        flag 30 (0x1fL): progress.isEmptyMeasurement(Type.HIPS, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 31 (0x20L): progress.isEmptyMeasurement(Type.HIPS, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 32 (0x21L): progress.isEmptyWeeks ? View.GONE : View.VISIBLE
        flag 33 (0x22L): progress.isEmptyWeeks ? View.GONE : View.VISIBLE
        flag 34 (0x23L): progress.isEmptyMeasurement(Type.HIPS, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 2))
        flag 35 (0x24L): progress.isEmptyMeasurement(Type.HIPS, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 2))
        flag 36 (0x25L): progress.isEmptyMeasurement(Type.WAIST, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 37 (0x26L): progress.isEmptyMeasurement(Type.WAIST, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 38 (0x27L): progress.isEmptyMeasurement(Type.HIPS, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 39 (0x28L): progress.isEmptyMeasurement(Type.HIPS, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 40 (0x29L): progress.isEmptyMeasurement(Type.WAIST, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 2))
        flag 41 (0x2aL): progress.isEmptyMeasurement(Type.WAIST, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 2))
        flag 42 (0x2bL): progress.isEmptyMeasurement(Type.HIPS, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 43 (0x2cL): progress.isEmptyMeasurement(Type.HIPS, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_hips
        flag 44 (0x2dL): progress.isEmptyMeasurement(Type.BREAST, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 1))
        flag 45 (0x2eL): progress.isEmptyMeasurement(Type.BREAST, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 1))
        flag 46 (0x2fL): progress.isEmptyDiffMeasurement(Type.BREAST) ? View.INVISIBLE : View.VISIBLE
        flag 47 (0x30L): progress.isEmptyDiffMeasurement(Type.BREAST) ? View.INVISIBLE : View.VISIBLE
        flag 48 (0x31L): progress.isEmptyMeasurement(Type.WAIST, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 1))
        flag 49 (0x32L): progress.isEmptyMeasurement(Type.WAIST, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WAIST, 1))
        flag 50 (0x33L): progress.isEmptyDiffMeasurement(Type.WAIST) ? View.INVISIBLE : View.VISIBLE
        flag 51 (0x34L): progress.isEmptyDiffMeasurement(Type.WAIST) ? View.INVISIBLE : View.VISIBLE
        flag 52 (0x35L): progress.isEmptyMeasurement(Type.WEIGHT, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 53 (0x36L): progress.isEmptyMeasurement(Type.WEIGHT, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 54 (0x37L): progress.isEmptyMeasurement(Type.WEIGHT, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 55 (0x38L): progress.isEmptyMeasurement(Type.WEIGHT, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 56 (0x39L): progress.isEmptyMeasurement(Type.WEIGHT, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 57 (0x3aL): progress.isEmptyMeasurement(Type.WEIGHT, 1) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 58 (0x3bL): progress.isEmptyMeasurement(Type.WEIGHT, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 59 (0x3cL): progress.isEmptyMeasurement(Type.WEIGHT, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_weight
        flag 60 (0x3dL): progress.isEmptyMeasurement(Type.WAIST, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 61 (0x3eL): progress.isEmptyMeasurement(Type.WAIST, 4) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 62 (0x3fL): progress.isEmptyMeasurement(Type.WAIST, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 63 (0x40L): progress.isEmptyMeasurement(Type.WAIST, 3) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 64 (0x41L): progress.isEmptyMeasurement(Type.WEIGHT, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 1))
        flag 65 (0x42L): progress.isEmptyMeasurement(Type.WEIGHT, 1) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 1))
        flag 66 (0x43L): progress.isEmptyMeasurement(Type.BREAST, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 2))
        flag 67 (0x44L): progress.isEmptyMeasurement(Type.BREAST, 2) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.BREAST, 2))
        flag 68 (0x45L): progress.isEmptyDiffMeasurement(Type.HIPS) ? View.INVISIBLE : View.VISIBLE
        flag 69 (0x46L): progress.isEmptyDiffMeasurement(Type.HIPS) ? View.INVISIBLE : View.VISIBLE
        flag 70 (0x47L): progress.isEmptyMeasurement(Type.WEIGHT, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 4))
        flag 71 (0x48L): progress.isEmptyMeasurement(Type.WEIGHT, 4) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.WEIGHT, 4))
        flag 72 (0x49L): progress.isEmptyMeasurement(Type.WAIST, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 73 (0x4aL): progress.isEmptyMeasurement(Type.WAIST, 2) ? @android:drawable/bg_progress_empty : @android:drawable/bg_progress_waist
        flag 74 (0x4bL): progress.isEmptyMeasurement(Type.HIPS, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 3))
        flag 75 (0x4cL): progress.isEmptyMeasurement(Type.HIPS, 3) ? 0.5F : androidx.databinding.ViewDataBinding.safeUnbox(progress.measurementGraph(Type.HIPS, 3))
    flag mapping end*/
    //end
}