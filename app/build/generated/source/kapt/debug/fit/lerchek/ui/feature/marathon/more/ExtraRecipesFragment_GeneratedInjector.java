package fit.lerchek.ui.feature.marathon.more;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ExtraRecipesFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ExtraRecipesFragment_GeneratedInjector {
  void injectExtraRecipesFragment(ExtraRecipesFragment extraRecipesFragment);
}
