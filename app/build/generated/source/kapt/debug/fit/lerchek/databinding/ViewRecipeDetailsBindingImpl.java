package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ViewRecipeDetailsBindingImpl extends ViewRecipeDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.cardRecipe, 11);
        sViewsWithIds.put(R.id.imgRecipe, 12);
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView6;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView7;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView9;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ViewRecipeDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 13, sIncludes, sViewsWithIds));
    }
    private ViewRecipeDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.cardview.widget.CardView) bindings[11]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[12]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            );
        this.btnReplace.setTag(null);
        this.btnUseThis.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatTextView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (androidx.appcompat.widget.AppCompatTextView) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView9 = (androidx.recyclerview.widget.RecyclerView) bindings[9];
        this.mboundView9.setTag(null);
        this.mealType.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel) variable);
        }
        else if (BR.isReplacement == variableId) {
            setIsReplacement((java.lang.Boolean) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setIsReplacement(@Nullable java.lang.Boolean IsReplacement) {
        this.mIsReplacement = IsReplacement;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.isReplacement);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel model = mModel;
        boolean androidxDatabindingViewDataBindingSafeUnboxIsReplacement = false;
        java.lang.String modelName = null;
        java.lang.String modelMealName = null;
        int modelShowWithoutOvenViewVISIBLEViewGONE = 0;
        int modelMealId = 0;
        boolean modelHasReplacements = false;
        java.lang.String modelFats = null;
        java.util.List<fit.lerchek.data.domain.uimodel.marathon.RecipeIngredientUIModel> modelIngredients = null;
        boolean modelHasReplacementsIsReplacementBooleanFalse = false;
        java.lang.Boolean isReplacement = mIsReplacement;
        int modelHasReplacementsIsReplacementBooleanFalseViewVISIBLEViewGONE = 0;
        boolean IsReplacement1 = false;
        int isReplacementViewVISIBLEViewGONE = 0;
        boolean modelShowWithoutOven = false;
        java.lang.String modelCarbohydrates = null;
        java.lang.String modelProteins = null;
        java.lang.String modelCalories = null;

        if ((dirtyFlags & 0x7L) != 0) {


            if ((dirtyFlags & 0x5L) != 0) {

                    if (model != null) {
                        // read model.name
                        modelName = model.getName();
                        // read model.mealName
                        modelMealName = model.getMealName();
                        // read model.mealId
                        modelMealId = model.getMealId();
                        // read model.fats
                        modelFats = model.getFats();
                        // read model.ingredients
                        modelIngredients = model.getIngredients();
                        // read model.showWithoutOven
                        modelShowWithoutOven = model.getShowWithoutOven();
                        // read model.carbohydrates
                        modelCarbohydrates = model.getCarbohydrates();
                        // read model.proteins
                        modelProteins = model.getProteins();
                        // read model.calories
                        modelCalories = model.getCalories();
                    }
                if((dirtyFlags & 0x5L) != 0) {
                    if(modelShowWithoutOven) {
                            dirtyFlags |= 0x10L;
                    }
                    else {
                            dirtyFlags |= 0x8L;
                    }
                }


                    // read model.showWithoutOven ? View.VISIBLE : View.GONE
                    modelShowWithoutOvenViewVISIBLEViewGONE = ((modelShowWithoutOven) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }

                if (model != null) {
                    // read model.hasReplacements
                    modelHasReplacements = model.getHasReplacements();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(modelHasReplacements) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }
        }
        if ((dirtyFlags & 0x6L) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(isReplacement)
                androidxDatabindingViewDataBindingSafeUnboxIsReplacement = androidx.databinding.ViewDataBinding.safeUnbox(isReplacement);
            if((dirtyFlags & 0x6L) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxIsReplacement) {
                        dirtyFlags |= 0x400L;
                }
                else {
                        dirtyFlags |= 0x200L;
                }
            }


                // read androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) ? View.VISIBLE : View.GONE
                isReplacementViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxIsReplacement) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished

        if ((dirtyFlags & 0x40L) != 0) {



                // read androidx.databinding.ViewDataBinding.safeUnbox(isReplacement)
                androidxDatabindingViewDataBindingSafeUnboxIsReplacement = androidx.databinding.ViewDataBinding.safeUnbox(isReplacement);
            if((dirtyFlags & 0x6L) != 0) {
                if(androidxDatabindingViewDataBindingSafeUnboxIsReplacement) {
                        dirtyFlags |= 0x400L;
                }
                else {
                        dirtyFlags |= 0x200L;
                }
            }


                // read !androidx.databinding.ViewDataBinding.safeUnbox(isReplacement)
                IsReplacement1 = !androidxDatabindingViewDataBindingSafeUnboxIsReplacement;
        }

        if ((dirtyFlags & 0x7L) != 0) {

                // read model.hasReplacements ? !androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) : false
                modelHasReplacementsIsReplacementBooleanFalse = ((modelHasReplacements) ? (IsReplacement1) : (false));
            if((dirtyFlags & 0x7L) != 0) {
                if(modelHasReplacementsIsReplacementBooleanFalse) {
                        dirtyFlags |= 0x100L;
                }
                else {
                        dirtyFlags |= 0x80L;
                }
            }


                // read model.hasReplacements ? !androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) : false ? View.VISIBLE : View.GONE
                modelHasReplacementsIsReplacementBooleanFalseViewVISIBLEViewGONE = ((modelHasReplacementsIsReplacementBooleanFalse) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            this.btnReplace.setVisibility(modelHasReplacementsIsReplacementBooleanFalseViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            this.btnUseThis.setVisibility(isReplacementViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, modelName);
            this.mboundView2.setVisibility(modelShowWithoutOvenViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, modelCalories);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, modelProteins);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, modelFats);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, modelCarbohydrates);
            fit.lerchek.ui.feature.marathon.food.RecipeIngredientsAdapterKt.setRecipeIngredients(this.mboundView9, modelIngredients);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mealType, modelMealName);
            fit.lerchek.ui.feature.marathon.today.adapters.TodayRecipesAdapterKt.setMealName(this.mealType, modelMealId);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): isReplacement
        flag 2 (0x3L): null
        flag 3 (0x4L): model.showWithoutOven ? View.VISIBLE : View.GONE
        flag 4 (0x5L): model.showWithoutOven ? View.VISIBLE : View.GONE
        flag 5 (0x6L): model.hasReplacements ? !androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) : false
        flag 6 (0x7L): model.hasReplacements ? !androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) : false
        flag 7 (0x8L): model.hasReplacements ? !androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) : false ? View.VISIBLE : View.GONE
        flag 8 (0x9L): model.hasReplacements ? !androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) : false ? View.VISIBLE : View.GONE
        flag 9 (0xaL): androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) ? View.VISIBLE : View.GONE
        flag 10 (0xbL): androidx.databinding.ViewDataBinding.safeUnbox(isReplacement) ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}