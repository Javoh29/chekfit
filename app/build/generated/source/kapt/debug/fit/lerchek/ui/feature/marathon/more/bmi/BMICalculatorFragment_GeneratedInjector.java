package fit.lerchek.ui.feature.marathon.more.bmi;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = BMICalculatorFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface BMICalculatorFragment_GeneratedInjector {
  void injectBMICalculatorFragment(BMICalculatorFragment bMICalculatorFragment);
}
