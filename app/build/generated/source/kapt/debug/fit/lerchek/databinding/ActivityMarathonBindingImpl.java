package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMarathonBindingImpl extends ActivityMarathonBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.container, 2);
        sViewsWithIds.put(R.id.toolbar_view, 3);
        sViewsWithIds.put(R.id.btn_menu, 4);
        sViewsWithIds.put(R.id.btn_select_marathon, 5);
        sViewsWithIds.put(R.id.iv_logo, 6);
        sViewsWithIds.put(R.id.btn_close, 7);
        sViewsWithIds.put(R.id.tv_title, 8);
        sViewsWithIds.put(R.id.nav_view, 9);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMarathonBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private ActivityMarathonBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.appcompat.widget.AppCompatImageView) bindings[7]
            , (android.widget.FrameLayout) bindings[4]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[5]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[6]
            , (com.google.android.material.bottomnavigation.BottomNavigationView) bindings[9]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[3]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            );
        this.btnProfile.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.MarathonActivityViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.MarathonActivityViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelProfileIsMale((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelProfileIsMale(androidx.databinding.ObservableBoolean ViewModelProfileIsMale, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean viewModelProfileIsMaleGet = false;
        android.graphics.drawable.Drawable viewModelProfileIsMaleBtnProfileAndroidDrawableIcProfileMaleBtnProfileAndroidDrawableIcProfileFemale = null;
        androidx.databinding.ObservableBoolean viewModelProfileIsMale = null;
        fit.lerchek.ui.feature.marathon.MarathonActivityViewModel viewModel = mViewModel;

        if ((dirtyFlags & 0x7L) != 0) {



                if (viewModel != null) {
                    // read viewModel.profileIsMale
                    viewModelProfileIsMale = viewModel.getProfileIsMale();
                }
                updateRegistration(0, viewModelProfileIsMale);


                if (viewModelProfileIsMale != null) {
                    // read viewModel.profileIsMale.get()
                    viewModelProfileIsMaleGet = viewModelProfileIsMale.get();
                }
            if((dirtyFlags & 0x7L) != 0) {
                if(viewModelProfileIsMaleGet) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read viewModel.profileIsMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                viewModelProfileIsMaleBtnProfileAndroidDrawableIcProfileMaleBtnProfileAndroidDrawableIcProfileFemale = ((viewModelProfileIsMaleGet) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnProfile.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(btnProfile.getContext(), R.drawable.ic_profile_female)));
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.btnProfile, viewModelProfileIsMaleBtnProfileAndroidDrawableIcProfileMaleBtnProfileAndroidDrawableIcProfileFemale);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.profileIsMale
        flag 1 (0x2L): viewModel
        flag 2 (0x3L): null
        flag 3 (0x4L): viewModel.profileIsMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 4 (0x5L): viewModel.profileIsMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
    flag mapping end*/
    //end
}