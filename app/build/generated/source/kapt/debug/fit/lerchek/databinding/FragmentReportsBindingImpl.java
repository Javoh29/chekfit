package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentReportsBindingImpl extends FragmentReportsBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 10);
        sViewsWithIds.put(R.id.btnChooseWeek, 11);
        sViewsWithIds.put(R.id.imgChoose, 12);
        sViewsWithIds.put(R.id.llText, 13);
        sViewsWithIds.put(R.id.tvWeekName, 14);
        sViewsWithIds.put(R.id.tvWeekSubtitle, 15);
        sViewsWithIds.put(R.id.tvWelcomeText, 16);
        sViewsWithIds.put(R.id.cvInside, 17);
        sViewsWithIds.put(R.id.tvReport, 18);
        sViewsWithIds.put(R.id.cvText, 19);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView1;
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView2;
    @NonNull
    private final android.widget.FrameLayout mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback8;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener ageandroidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.reportText.get()
            //         is viewModel.reportText.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(age);
            // localize variables for thread safety
            // viewModel.reportText != null
            boolean viewModelReportTextJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.ReportsViewModel viewModel = mViewModel;
            // viewModel.reportText
            androidx.databinding.ObservableField<java.lang.String> viewModelReportText = null;
            // viewModel.reportText.get()
            java.lang.String viewModelReportTextGet = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelReportText = viewModel.getReportText();

                viewModelReportTextJavaLangObjectNull = (viewModelReportText) != (null);
                if (viewModelReportTextJavaLangObjectNull) {




                    viewModelReportText.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentReportsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds));
    }
    private FragmentReportsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (androidx.appcompat.widget.AppCompatEditText) bindings[7]
            , (bindings[10] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[10]) : null
            , (androidx.cardview.widget.CardView) bindings[11]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[8]
            , (android.widget.FrameLayout) bindings[6]
            , (androidx.cardview.widget.CardView) bindings[17]
            , (androidx.cardview.widget.CardView) bindings[19]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[12]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[4]
            , (android.widget.LinearLayout) bindings[13]
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[18]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[15]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[16]
            );
        this.age.setTag(null);
        this.btnSave.setTag(null);
        this.constraintLayoutInput.setTag(null);
        this.imgMeal.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (bindings[9] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[9]) : null;
        this.mboundView2 = (androidx.core.widget.NestedScrollView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.FrameLayout) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.progressBlock.setTag(null);
        setRootTag(root);
        // listeners
        mCallback8 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.ReportsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.ReportsViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelProfileIsMale((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeViewModelIsEditMode((androidx.databinding.ObservableBoolean) object, fieldId);
            case 2 :
                return onChangeViewModelReports((androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.ReportsUIModel>) object, fieldId);
            case 3 :
                return onChangeViewModelReportText((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelIsEditMode1((androidx.databinding.ObservableBoolean) object, fieldId);
            case 5 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelProfileIsMale(androidx.databinding.ObservableBoolean ViewModelProfileIsMale, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsEditMode(androidx.databinding.ObservableBoolean ViewModelIsEditMode, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelReports(androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.ReportsUIModel> ViewModelReports, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelReportText(androidx.databinding.ObservableField<java.lang.String> ViewModelReportText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsEditMode1(androidx.databinding.ObservableBoolean ViewModelIsEditMode, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.ReportsUIModel viewModelReportsGetValue = null;
        androidx.databinding.ObservableBoolean viewModelProfileIsMale = null;
        java.lang.String viewModelReportTextGet = null;
        androidx.databinding.ObservableBoolean viewModelIsEditMode = null;
        androidx.lifecycle.LiveData<fit.lerchek.data.domain.uimodel.marathon.ReportsUIModel> viewModelReports = null;
        int viewModelIsProgressViewGONEViewVISIBLE = 0;
        boolean viewModelIsEditModeGet = false;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        android.graphics.drawable.Drawable viewModelProfileIsMaleImgMealAndroidDrawableIcProfileMaleImgMealAndroidDrawableIcProfileFemale = null;
        java.lang.String viewModelIsEditModeBtnSaveAndroidStringReportSendBtnSaveAndroidStringReportEdit = null;
        boolean viewModelProfileIsMaleGet = false;
        int viewModelIsEditModeViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelReportText = null;
        androidx.databinding.ObservableBoolean ViewModelIsEditMode1 = null;
        boolean viewModelIsProgressGet = false;
        fit.lerchek.ui.feature.marathon.ReportsViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;
        boolean viewModelReportsCanEdit = false;
        boolean ViewModelIsEditModeGet1 = false;

        if ((dirtyFlags & 0xffL) != 0) {


            if ((dirtyFlags & 0xc1L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.profileIsMale
                        viewModelProfileIsMale = viewModel.getProfileIsMale();
                    }
                    updateRegistration(0, viewModelProfileIsMale);


                    if (viewModelProfileIsMale != null) {
                        // read viewModel.profileIsMale.get()
                        viewModelProfileIsMaleGet = viewModelProfileIsMale.get();
                    }
                if((dirtyFlags & 0xc1L) != 0) {
                    if(viewModelProfileIsMaleGet) {
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                    }
                }


                    // read viewModel.profileIsMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                    viewModelProfileIsMaleImgMealAndroidDrawableIcProfileMaleImgMealAndroidDrawableIcProfileFemale = ((viewModelProfileIsMaleGet) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgMeal.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgMeal.getContext(), R.drawable.ic_profile_female)));
            }
            if ((dirtyFlags & 0xc2L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isEditMode()
                        viewModelIsEditMode = viewModel.isEditMode();
                    }
                    updateRegistration(1, viewModelIsEditMode);


                    if (viewModelIsEditMode != null) {
                        // read viewModel.isEditMode().get()
                        viewModelIsEditModeGet = viewModelIsEditMode.get();
                    }
                if((dirtyFlags & 0xc2L) != 0) {
                    if(viewModelIsEditModeGet) {
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x10000L;
                    }
                }


                    // read viewModel.isEditMode().get() ? View.VISIBLE : View.GONE
                    viewModelIsEditModeViewVISIBLEViewGONE = ((viewModelIsEditModeGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xc4L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.reports
                        viewModelReports = viewModel.getReports();
                    }
                    updateLiveDataRegistration(2, viewModelReports);


                    if (viewModelReports != null) {
                        // read viewModel.reports.getValue()
                        viewModelReportsGetValue = viewModelReports.getValue();
                    }


                    if (viewModelReportsGetValue != null) {
                        // read viewModel.reports.getValue().canEdit
                        viewModelReportsCanEdit = viewModelReportsGetValue.getCanEdit();
                    }
            }
            if ((dirtyFlags & 0xc8L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.reportText
                        viewModelReportText = viewModel.getReportText();
                    }
                    updateRegistration(3, viewModelReportText);


                    if (viewModelReportText != null) {
                        // read viewModel.reportText.get()
                        viewModelReportTextGet = viewModelReportText.get();
                    }
            }
            if ((dirtyFlags & 0xd0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isEditMode
                        ViewModelIsEditMode1 = viewModel.isEditMode();
                    }
                    updateRegistration(4, ViewModelIsEditMode1);


                    if (ViewModelIsEditMode1 != null) {
                        // read viewModel.isEditMode.get()
                        ViewModelIsEditModeGet1 = ViewModelIsEditMode1.get();
                    }
                if((dirtyFlags & 0xd0L) != 0) {
                    if(ViewModelIsEditModeGet1) {
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x4000L;
                    }
                }


                    // read viewModel.isEditMode.get() ? @android:string/report_send : @android:string/report_edit
                    viewModelIsEditModeBtnSaveAndroidStringReportSendBtnSaveAndroidStringReportEdit = ((ViewModelIsEditModeGet1) ? (btnSave.getResources().getString(R.string.report_send)) : (btnSave.getResources().getString(R.string.report_edit)));
            }
            if ((dirtyFlags & 0xe0L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(5, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0xe0L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x200L;
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.GONE : View.VISIBLE
                    viewModelIsProgressViewGONEViewVISIBLE = ((viewModelIsProgressGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0xc8L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.age, viewModelReportTextGet);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, viewModelReportTextGet);
        }
        if ((dirtyFlags & 0x80L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.age, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, ageandroidTextAttrChanged);
            this.btnSave.setOnClickListener(mCallback8);
        }
        if ((dirtyFlags & 0xc4L) != 0) {
            // api target 1

            this.btnSave.setEnabled(viewModelReportsCanEdit);
        }
        if ((dirtyFlags & 0xd0L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.btnSave, viewModelIsEditModeBtnSaveAndroidStringReportSendBtnSaveAndroidStringReportEdit);
        }
        if ((dirtyFlags & 0xc2L) != 0) {
            // api target 1

            this.constraintLayoutInput.setVisibility(viewModelIsEditModeViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xc1L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.imgMeal, viewModelProfileIsMaleImgMealAndroidDrawableIcProfileMaleImgMealAndroidDrawableIcProfileFemale);
        }
        if ((dirtyFlags & 0xe0L) != 0) {
            // api target 1

            this.mboundView2.setVisibility(viewModelIsProgressViewGONEViewVISIBLE);
            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        fit.lerchek.ui.feature.marathon.ReportsViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.toggleSendEdit();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.profileIsMale
        flag 1 (0x2L): viewModel.isEditMode()
        flag 2 (0x3L): viewModel.reports
        flag 3 (0x4L): viewModel.reportText
        flag 4 (0x5L): viewModel.isEditMode
        flag 5 (0x6L): viewModel.isProgress()
        flag 6 (0x7L): viewModel
        flag 7 (0x8L): null
        flag 8 (0x9L): viewModel.isProgress().get() ? View.GONE : View.VISIBLE
        flag 9 (0xaL): viewModel.isProgress().get() ? View.GONE : View.VISIBLE
        flag 10 (0xbL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 11 (0xcL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 12 (0xdL): viewModel.profileIsMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 13 (0xeL): viewModel.profileIsMale.get() ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 14 (0xfL): viewModel.isEditMode.get() ? @android:string/report_send : @android:string/report_edit
        flag 15 (0x10L): viewModel.isEditMode.get() ? @android:string/report_send : @android:string/report_edit
        flag 16 (0x11L): viewModel.isEditMode().get() ? View.VISIBLE : View.GONE
        flag 17 (0x12L): viewModel.isEditMode().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}