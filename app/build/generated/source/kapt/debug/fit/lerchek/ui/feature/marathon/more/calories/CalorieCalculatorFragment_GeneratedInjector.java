package fit.lerchek.ui.feature.marathon.more.calories;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = CalorieCalculatorFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface CalorieCalculatorFragment_GeneratedInjector {
  void injectCalorieCalculatorFragment(CalorieCalculatorFragment calorieCalculatorFragment);
}
