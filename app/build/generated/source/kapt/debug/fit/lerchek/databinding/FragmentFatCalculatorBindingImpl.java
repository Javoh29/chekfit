package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentFatCalculatorBindingImpl extends FragmentFatCalculatorBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.btnBack, 16);
        sViewsWithIds.put(R.id.nsvFatCalculator, 18);
        sViewsWithIds.put(R.id.weight, 19);
        sViewsWithIds.put(R.id.yourFatTitle, 20);
        sViewsWithIds.put(R.id.cardFat, 21);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView11;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView15;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView5;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView6;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView7;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView8;
    @NonNull
    private final androidx.appcompat.widget.AppCompatEditText mboundView9;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback16;
    // values
    // listeners
    // Inverse Binding Event Handlers
    private androidx.databinding.InverseBindingListener mboundView2androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.age.get()
            //         is viewModel.age.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView2);
            // localize variables for thread safety
            // viewModel.age.get()
            java.lang.String viewModelAgeGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.age != null
            boolean viewModelAgeJavaLangObjectNull = false;
            // viewModel.age
            androidx.databinding.ObservableField<java.lang.String> viewModelAge = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelAge = viewModel.getAge();

                viewModelAgeJavaLangObjectNull = (viewModelAge) != (null);
                if (viewModelAgeJavaLangObjectNull) {




                    viewModelAge.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView3androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.height.get()
            //         is viewModel.height.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView3);
            // localize variables for thread safety
            // viewModel.height.get()
            java.lang.String viewModelHeightGet = null;
            // viewModel.height != null
            boolean viewModelHeightJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
            // viewModel.height
            androidx.databinding.ObservableField<java.lang.String> viewModelHeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelHeight = viewModel.getHeight();

                viewModelHeightJavaLangObjectNull = (viewModelHeight) != (null);
                if (viewModelHeightJavaLangObjectNull) {




                    viewModelHeight.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView4androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.weight.get()
            //         is viewModel.weight.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView4);
            // localize variables for thread safety
            // viewModel.weight.get()
            java.lang.String viewModelWeightGet = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
            // viewModel.weight
            androidx.databinding.ObservableField<java.lang.String> viewModelWeight = null;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.weight != null
            boolean viewModelWeightJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelWeight = viewModel.getWeight();

                viewModelWeightJavaLangObjectNull = (viewModelWeight) != (null);
                if (viewModelWeightJavaLangObjectNull) {




                    viewModelWeight.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView5androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.waist.get()
            //         is viewModel.waist.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView5);
            // localize variables for thread safety
            // viewModel.waist
            androidx.databinding.ObservableField<java.lang.String> viewModelWaist = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.waist.get()
            java.lang.String viewModelWaistGet = null;
            // viewModel.waist != null
            boolean viewModelWaistJavaLangObjectNull = false;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelWaist = viewModel.getWaist();

                viewModelWaistJavaLangObjectNull = (viewModelWaist) != (null);
                if (viewModelWaistJavaLangObjectNull) {




                    viewModelWaist.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView8androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.hips.get()
            //         is viewModel.hips.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView8);
            // localize variables for thread safety
            // viewModel.hips != null
            boolean viewModelHipsJavaLangObjectNull = false;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.hips.get()
            java.lang.String viewModelHipsGet = null;
            // viewModel.hips
            androidx.databinding.ObservableField<java.lang.String> viewModelHips = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelHips = viewModel.getHips();

                viewModelHipsJavaLangObjectNull = (viewModelHips) != (null);
                if (viewModelHipsJavaLangObjectNull) {




                    viewModelHips.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };
    private androidx.databinding.InverseBindingListener mboundView9androidTextAttrChanged = new androidx.databinding.InverseBindingListener() {
        @Override
        public void onChange() {
            // Inverse of viewModel.neck.get()
            //         is viewModel.neck.set((java.lang.String) callbackArg_0)
            java.lang.String callbackArg_0 = androidx.databinding.adapters.TextViewBindingAdapter.getTextString(mboundView9);
            // localize variables for thread safety
            // viewModel.neck != null
            boolean viewModelNeckJavaLangObjectNull = false;
            // viewModel.neck
            androidx.databinding.ObservableField<java.lang.String> viewModelNeck = null;
            // viewModel
            fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
            // viewModel != null
            boolean viewModelJavaLangObjectNull = false;
            // viewModel.neck.get()
            java.lang.String viewModelNeckGet = null;



            viewModelJavaLangObjectNull = (viewModel) != (null);
            if (viewModelJavaLangObjectNull) {


                viewModelNeck = viewModel.getNeck();

                viewModelNeckJavaLangObjectNull = (viewModelNeck) != (null);
                if (viewModelNeckJavaLangObjectNull) {




                    viewModelNeck.set(((java.lang.String) (callbackArg_0)));
                }
            }
        }
    };

    public FragmentFatCalculatorBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private FragmentFatCalculatorBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 10
            , (bindings[16] != null) ? fit.lerchek.databinding.IncludeBackBinding.bind((android.view.View) bindings[16]) : null
            , (androidx.appcompat.widget.AppCompatTextView) bindings[10]
            , (androidx.cardview.widget.CardView) bindings[21]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[14]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[13]
            , (android.widget.FrameLayout) bindings[1]
            , (androidx.core.widget.NestedScrollView) bindings[18]
            , (android.widget.FrameLayout) bindings[15]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[19]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[20]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[12]
            );
        this.btnCalculate.setTag(null);
        this.fatDescription.setTag(null);
        this.fatTitle.setTag(null);
        this.frameLayout.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView11 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[11];
        this.mboundView11.setTag(null);
        this.mboundView15 = (bindings[17] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[17]) : null;
        this.mboundView2 = (androidx.appcompat.widget.AppCompatEditText) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatEditText) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatEditText) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatEditText) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView8 = (androidx.appcompat.widget.AppCompatEditText) bindings[8];
        this.mboundView8.setTag(null);
        this.mboundView9 = (androidx.appcompat.widget.AppCompatEditText) bindings[9];
        this.mboundView9.setTag(null);
        this.progressBlock.setTag(null);
        this.yourFatValue.setTag(null);
        setRootTag(root);
        // listeners
        mCallback16 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x800L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x400L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelIsMale((androidx.databinding.ObservableBoolean) object, fieldId);
            case 1 :
                return onChangeViewModelHips((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeViewModelYourFat((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeViewModelAge((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 4 :
                return onChangeViewModelIsCalculate((androidx.databinding.ObservableBoolean) object, fieldId);
            case 5 :
                return onChangeViewModelHeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 6 :
                return onChangeViewModelWeight((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 7 :
                return onChangeViewModelWaist((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 8 :
                return onChangeViewModelNeck((androidx.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 9 :
                return onChangeViewModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelIsMale(androidx.databinding.ObservableBoolean ViewModelIsMale, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHips(androidx.databinding.ObservableField<java.lang.String> ViewModelHips, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelYourFat(androidx.databinding.ObservableField<java.lang.String> ViewModelYourFat, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelAge(androidx.databinding.ObservableField<java.lang.String> ViewModelAge, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsCalculate(androidx.databinding.ObservableBoolean ViewModelIsCalculate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelHeight(androidx.databinding.ObservableField<java.lang.String> ViewModelHeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelWeight(androidx.databinding.ObservableField<java.lang.String> ViewModelWeight, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelWaist(androidx.databinding.ObservableField<java.lang.String> ViewModelWaist, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x80L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelNeck(androidx.databinding.ObservableField<java.lang.String> ViewModelNeck, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x100L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsProgress(androidx.databinding.ObservableBoolean ViewModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x200L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String viewModelIsMaleFatDescriptionAndroidStringFatCalculatorNormalMaleDescriptionFatDescriptionAndroidStringFatCalculatorNormalFemaleDescription = null;
        androidx.databinding.ObservableBoolean viewModelIsMale = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelHips = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelYourFat = null;
        java.lang.String viewModelHeightGet = null;
        boolean viewModelIsMaleGet = false;
        int viewModelIsProgressViewVISIBLEViewGONE = 0;
        boolean viewModelIsCalculateGet = false;
        java.lang.String viewModelWeightGet = null;
        boolean viewModelIsProgressGet = false;
        java.lang.String viewModelWaistGet = null;
        java.lang.String viewModelYourFatGet = null;
        int viewModelIsCalculateViewVISIBLEViewGONE = 0;
        androidx.databinding.ObservableField<java.lang.String> viewModelAge = null;
        androidx.databinding.ObservableBoolean viewModelIsCalculate = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelHeight = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelWeight = null;
        java.lang.String viewModelIsMaleFatTitleAndroidStringFatCalculatorNormalMaleTitleFatTitleAndroidStringFatCalculatorNormalFemaleTitle = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelWaist = null;
        androidx.databinding.ObservableField<java.lang.String> viewModelNeck = null;
        int viewModelIsMaleViewGONEViewVISIBLE = 0;
        java.lang.String viewModelAgeGet = null;
        fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
        androidx.databinding.ObservableBoolean viewModelIsProgress = null;
        java.lang.String viewModelHipsGet = null;
        java.lang.String viewModelNeckGet = null;

        if ((dirtyFlags & 0xfffL) != 0) {


            if ((dirtyFlags & 0xc01L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isMale()
                        viewModelIsMale = viewModel.isMale();
                    }
                    updateRegistration(0, viewModelIsMale);


                    if (viewModelIsMale != null) {
                        // read viewModel.isMale().get()
                        viewModelIsMaleGet = viewModelIsMale.get();
                    }
                if((dirtyFlags & 0xc01L) != 0) {
                    if(viewModelIsMaleGet) {
                            dirtyFlags |= 0x2000L;
                            dirtyFlags |= 0x80000L;
                            dirtyFlags |= 0x200000L;
                    }
                    else {
                            dirtyFlags |= 0x1000L;
                            dirtyFlags |= 0x40000L;
                            dirtyFlags |= 0x100000L;
                    }
                }


                    // read viewModel.isMale().get() ? @android:string/fat_calculator_normal_male_description : @android:string/fat_calculator_normal_female_description
                    viewModelIsMaleFatDescriptionAndroidStringFatCalculatorNormalMaleDescriptionFatDescriptionAndroidStringFatCalculatorNormalFemaleDescription = ((viewModelIsMaleGet) ? (fatDescription.getResources().getString(R.string.fat_calculator_normal_male_description)) : (fatDescription.getResources().getString(R.string.fat_calculator_normal_female_description)));
                    // read viewModel.isMale().get() ? @android:string/fat_calculator_normal_male_title : @android:string/fat_calculator_normal_female_title
                    viewModelIsMaleFatTitleAndroidStringFatCalculatorNormalMaleTitleFatTitleAndroidStringFatCalculatorNormalFemaleTitle = ((viewModelIsMaleGet) ? (fatTitle.getResources().getString(R.string.fat_calculator_normal_male_title)) : (fatTitle.getResources().getString(R.string.fat_calculator_normal_female_title)));
                    // read viewModel.isMale().get() ? View.GONE : View.VISIBLE
                    viewModelIsMaleViewGONEViewVISIBLE = ((viewModelIsMaleGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0xc02L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.hips
                        viewModelHips = viewModel.getHips();
                    }
                    updateRegistration(1, viewModelHips);


                    if (viewModelHips != null) {
                        // read viewModel.hips.get()
                        viewModelHipsGet = viewModelHips.get();
                    }
            }
            if ((dirtyFlags & 0xc04L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.yourFat
                        viewModelYourFat = viewModel.getYourFat();
                    }
                    updateRegistration(2, viewModelYourFat);


                    if (viewModelYourFat != null) {
                        // read viewModel.yourFat.get()
                        viewModelYourFatGet = viewModelYourFat.get();
                    }
            }
            if ((dirtyFlags & 0xc08L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.age
                        viewModelAge = viewModel.getAge();
                    }
                    updateRegistration(3, viewModelAge);


                    if (viewModelAge != null) {
                        // read viewModel.age.get()
                        viewModelAgeGet = viewModelAge.get();
                    }
            }
            if ((dirtyFlags & 0xc10L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isCalculate()
                        viewModelIsCalculate = viewModel.isCalculate();
                    }
                    updateRegistration(4, viewModelIsCalculate);


                    if (viewModelIsCalculate != null) {
                        // read viewModel.isCalculate().get()
                        viewModelIsCalculateGet = viewModelIsCalculate.get();
                    }
                if((dirtyFlags & 0xc10L) != 0) {
                    if(viewModelIsCalculateGet) {
                            dirtyFlags |= 0x20000L;
                    }
                    else {
                            dirtyFlags |= 0x10000L;
                    }
                }


                    // read viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
                    viewModelIsCalculateViewVISIBLEViewGONE = ((viewModelIsCalculateGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0xc20L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.height
                        viewModelHeight = viewModel.getHeight();
                    }
                    updateRegistration(5, viewModelHeight);


                    if (viewModelHeight != null) {
                        // read viewModel.height.get()
                        viewModelHeightGet = viewModelHeight.get();
                    }
            }
            if ((dirtyFlags & 0xc40L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.weight
                        viewModelWeight = viewModel.getWeight();
                    }
                    updateRegistration(6, viewModelWeight);


                    if (viewModelWeight != null) {
                        // read viewModel.weight.get()
                        viewModelWeightGet = viewModelWeight.get();
                    }
            }
            if ((dirtyFlags & 0xc80L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.waist
                        viewModelWaist = viewModel.getWaist();
                    }
                    updateRegistration(7, viewModelWaist);


                    if (viewModelWaist != null) {
                        // read viewModel.waist.get()
                        viewModelWaistGet = viewModelWaist.get();
                    }
            }
            if ((dirtyFlags & 0xd00L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.neck
                        viewModelNeck = viewModel.getNeck();
                    }
                    updateRegistration(8, viewModelNeck);


                    if (viewModelNeck != null) {
                        // read viewModel.neck.get()
                        viewModelNeckGet = viewModelNeck.get();
                    }
            }
            if ((dirtyFlags & 0xe00L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isProgress()
                        viewModelIsProgress = viewModel.isProgress();
                    }
                    updateRegistration(9, viewModelIsProgress);


                    if (viewModelIsProgress != null) {
                        // read viewModel.isProgress().get()
                        viewModelIsProgressGet = viewModelIsProgress.get();
                    }
                if((dirtyFlags & 0xe00L) != 0) {
                    if(viewModelIsProgressGet) {
                            dirtyFlags |= 0x8000L;
                    }
                    else {
                            dirtyFlags |= 0x4000L;
                    }
                }


                    // read viewModel.isProgress().get() ? View.VISIBLE : View.GONE
                    viewModelIsProgressViewVISIBLEViewGONE = ((viewModelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x800L) != 0) {
            // api target 1

            this.btnCalculate.setOnClickListener(mCallback16);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView2, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView2androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView3, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView3androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView4, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView4androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView5, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView5androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView8, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView8androidTextAttrChanged);
            androidx.databinding.adapters.TextViewBindingAdapter.setTextWatcher(this.mboundView9, (androidx.databinding.adapters.TextViewBindingAdapter.BeforeTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.OnTextChanged)null, (androidx.databinding.adapters.TextViewBindingAdapter.AfterTextChanged)null, mboundView9androidTextAttrChanged);
        }
        if ((dirtyFlags & 0xc01L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.fatDescription, viewModelIsMaleFatDescriptionAndroidStringFatCalculatorNormalMaleDescriptionFatDescriptionAndroidStringFatCalculatorNormalFemaleDescription);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.fatTitle, viewModelIsMaleFatTitleAndroidStringFatCalculatorNormalMaleTitleFatTitleAndroidStringFatCalculatorNormalFemaleTitle);
            this.mboundView6.setVisibility(viewModelIsMaleViewGONEViewVISIBLE);
            this.mboundView7.setVisibility(viewModelIsMaleViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0xc10L) != 0) {
            // api target 1

            this.mboundView11.setVisibility(viewModelIsCalculateViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xc08L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, viewModelAgeGet);
        }
        if ((dirtyFlags & 0xc20L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, viewModelHeightGet);
        }
        if ((dirtyFlags & 0xc40L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, viewModelWeightGet);
        }
        if ((dirtyFlags & 0xc80L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, viewModelWaistGet);
        }
        if ((dirtyFlags & 0xc02L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView8, viewModelHipsGet);
        }
        if ((dirtyFlags & 0xd00L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView9, viewModelNeckGet);
        }
        if ((dirtyFlags & 0xe00L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(viewModelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0xc04L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.yourFatValue, viewModelYourFatGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        fit.lerchek.ui.feature.marathon.more.fat.FatCalculatorViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.calculate();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.isMale()
        flag 1 (0x2L): viewModel.hips
        flag 2 (0x3L): viewModel.yourFat
        flag 3 (0x4L): viewModel.age
        flag 4 (0x5L): viewModel.isCalculate()
        flag 5 (0x6L): viewModel.height
        flag 6 (0x7L): viewModel.weight
        flag 7 (0x8L): viewModel.waist
        flag 8 (0x9L): viewModel.neck
        flag 9 (0xaL): viewModel.isProgress()
        flag 10 (0xbL): viewModel
        flag 11 (0xcL): null
        flag 12 (0xdL): viewModel.isMale().get() ? @android:string/fat_calculator_normal_male_description : @android:string/fat_calculator_normal_female_description
        flag 13 (0xeL): viewModel.isMale().get() ? @android:string/fat_calculator_normal_male_description : @android:string/fat_calculator_normal_female_description
        flag 14 (0xfL): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 15 (0x10L): viewModel.isProgress().get() ? View.VISIBLE : View.GONE
        flag 16 (0x11L): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 17 (0x12L): viewModel.isCalculate().get() ? View.VISIBLE : View.GONE
        flag 18 (0x13L): viewModel.isMale().get() ? @android:string/fat_calculator_normal_male_title : @android:string/fat_calculator_normal_female_title
        flag 19 (0x14L): viewModel.isMale().get() ? @android:string/fat_calculator_normal_male_title : @android:string/fat_calculator_normal_female_title
        flag 20 (0x15L): viewModel.isMale().get() ? View.GONE : View.VISIBLE
        flag 21 (0x16L): viewModel.isMale().get() ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}