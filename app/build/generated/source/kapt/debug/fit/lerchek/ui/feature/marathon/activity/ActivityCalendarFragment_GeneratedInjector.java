package fit.lerchek.ui.feature.marathon.activity;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ActivityCalendarFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ActivityCalendarFragment_GeneratedInjector {
  void injectActivityCalendarFragment(ActivityCalendarFragment activityCalendarFragment);
}
