package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemTodayTelegramBindingImpl extends ItemTodayTelegramBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback10;
    @Nullable
    private final android.view.View.OnClickListener mCallback9;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemTodayTelegramBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private ItemTodayTelegramBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            );
        this.btnTgChannel.setTag(null);
        this.btnTgGroup.setTag(null);
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        mCallback10 = new fit.lerchek.generated.callback.OnClickListener(this, 2);
        mCallback9 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.data.domain.uimodel.marathon.TodayTelegramItem) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.data.domain.uimodel.marathon.TodayTelegramItem Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.data.domain.uimodel.marathon.TodayTelegramItem model = mModel;
        boolean modelChannelJavaLangObjectNull = false;
        int modelGroupJavaLangObjectNullViewVISIBLEViewGONE = 0;
        java.lang.String modelChannel = null;
        fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback callback = mCallback;
        java.lang.String modelGroup = null;
        boolean modelGroupJavaLangObjectNull = false;
        int modelChannelJavaLangObjectNullViewVISIBLEViewGONE = 0;

        if ((dirtyFlags & 0x5L) != 0) {



                if (model != null) {
                    // read model.channel
                    modelChannel = model.getChannel();
                    // read model.group
                    modelGroup = model.getGroup();
                }


                // read model.channel != null
                modelChannelJavaLangObjectNull = (modelChannel) != (null);
                // read model.group != null
                modelGroupJavaLangObjectNull = (modelGroup) != (null);
            if((dirtyFlags & 0x5L) != 0) {
                if(modelChannelJavaLangObjectNull) {
                        dirtyFlags |= 0x40L;
                }
                else {
                        dirtyFlags |= 0x20L;
                }
            }
            if((dirtyFlags & 0x5L) != 0) {
                if(modelGroupJavaLangObjectNull) {
                        dirtyFlags |= 0x10L;
                }
                else {
                        dirtyFlags |= 0x8L;
                }
            }


                // read model.channel != null ? View.VISIBLE : View.GONE
                modelChannelJavaLangObjectNullViewVISIBLEViewGONE = ((modelChannelJavaLangObjectNull) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                // read model.group != null ? View.VISIBLE : View.GONE
                modelGroupJavaLangObjectNullViewVISIBLEViewGONE = ((modelGroupJavaLangObjectNull) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
        }
        // batch finished
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.btnTgChannel.setOnClickListener(mCallback9);
            this.btnTgGroup.setOnClickListener(mCallback10);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            this.btnTgChannel.setVisibility(modelChannelJavaLangObjectNullViewVISIBLEViewGONE);
            this.btnTgGroup.setVisibility(modelGroupJavaLangObjectNullViewVISIBLEViewGONE);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // model
                fit.lerchek.data.domain.uimodel.marathon.TodayTelegramItem model = mModel;
                // model != null
                boolean modelJavaLangObjectNull = false;
                // model.group
                java.lang.String modelGroup = null;
                // callback
                fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {



                    modelJavaLangObjectNull = (model) != (null);
                    if (modelJavaLangObjectNull) {


                        modelGroup = model.getGroup();

                        callback.onTgGroupClick(modelGroup);
                    }
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // model
                fit.lerchek.data.domain.uimodel.marathon.TodayTelegramItem model = mModel;
                // model != null
                boolean modelJavaLangObjectNull = false;
                // model.channel
                java.lang.String modelChannel = null;
                // callback
                fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {



                    modelJavaLangObjectNull = (model) != (null);
                    if (modelJavaLangObjectNull) {


                        modelChannel = model.getChannel();

                        callback.onTgChannelClick(modelChannel);
                    }
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): callback
        flag 2 (0x3L): null
        flag 3 (0x4L): model.group != null ? View.VISIBLE : View.GONE
        flag 4 (0x5L): model.group != null ? View.VISIBLE : View.GONE
        flag 5 (0x6L): model.channel != null ? View.VISIBLE : View.GONE
        flag 6 (0x7L): model.channel != null ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}