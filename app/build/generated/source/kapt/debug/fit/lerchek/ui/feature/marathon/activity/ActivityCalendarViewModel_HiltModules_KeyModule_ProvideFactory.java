// Generated by Dagger (https://dagger.dev).
package fit.lerchek.ui.feature.marathon.activity;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ActivityCalendarViewModel_HiltModules_KeyModule_ProvideFactory implements Factory<String> {
  @Override
  public String get() {
    return provide();
  }

  public static ActivityCalendarViewModel_HiltModules_KeyModule_ProvideFactory create() {
    return InstanceHolder.INSTANCE;
  }

  public static String provide() {
    return Preconditions.checkNotNullFromProvides(ActivityCalendarViewModel_HiltModules.KeyModule.provide());
  }

  private static final class InstanceHolder {
    private static final ActivityCalendarViewModel_HiltModules_KeyModule_ProvideFactory INSTANCE = new ActivityCalendarViewModel_HiltModules_KeyModule_ProvideFactory();
  }
}
