package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutProgressYourProgressBindingImpl extends LayoutProgressYourProgressBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tvYourProgress, 3);
        sViewsWithIds.put(R.id.btnAddMeasurements, 4);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutProgressYourProgressBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private LayoutProgressYourProgressBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatTextView) bindings[4]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[1]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[2]
            , (androidx.appcompat.widget.AppCompatTextView) bindings[3]
            );
        this.imgGender.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvHello.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.progress == variableId) {
            setProgress((fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setProgress(@Nullable fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel Progress) {
        this.mProgress = Progress;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.progress);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        boolean progressIsMale = false;
        java.lang.String progressProfileName = null;
        fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel progress = mProgress;
        java.lang.String tvHelloAndroidStringMarathonTasksHelloProgressProfileName = null;
        android.graphics.drawable.Drawable progressIsMaleImgGenderAndroidDrawableIcProfileMaleImgGenderAndroidDrawableIcProfileFemale = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (progress != null) {
                    // read progress.isMale
                    progressIsMale = progress.isMale();
                    // read progress.profileName
                    progressProfileName = progress.getProfileName();
                }
            if((dirtyFlags & 0x3L) != 0) {
                if(progressIsMale) {
                        dirtyFlags |= 0x8L;
                }
                else {
                        dirtyFlags |= 0x4L;
                }
            }


                // read progress.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
                progressIsMaleImgGenderAndroidDrawableIcProfileMaleImgGenderAndroidDrawableIcProfileFemale = ((progressIsMale) ? (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgGender.getContext(), R.drawable.ic_profile_male)) : (androidx.appcompat.content.res.AppCompatResources.getDrawable(imgGender.getContext(), R.drawable.ic_profile_female)));
                // read @android:string/marathon_tasks_hello
                tvHelloAndroidStringMarathonTasksHelloProgressProfileName = tvHello.getResources().getString(R.string.marathon_tasks_hello, progressProfileName);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.ImageViewBindingAdapter.setImageDrawable(this.imgGender, progressIsMaleImgGenderAndroidDrawableIcProfileMaleImgGenderAndroidDrawableIcProfileFemale);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvHello, tvHelloAndroidStringMarathonTasksHelloProgressProfileName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): progress
        flag 1 (0x2L): null
        flag 2 (0x3L): progress.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
        flag 3 (0x4L): progress.isMale ? @android:drawable/ic_profile_male : @android:drawable/ic_profile_female
    flag mapping end*/
    //end
}