package fit.lerchek.databinding;
import fit.lerchek.R;
import fit.lerchek.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMakeMeasurementsBindingImpl extends FragmentMakeMeasurementsBinding implements fit.lerchek.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(15);
        sIncludes.setIncludes(1, 
            new String[] {"layout_progress_photo_error", "layout_progress_photo_succeed", "layout_progress_make_photos", "layout_progress_video_error", "layout_progress_make_video", "layout_progress_week_result"},
            new int[] {7, 8, 9, 10, 11, 12},
            new int[] {fit.lerchek.R.layout.layout_progress_photo_error,
                fit.lerchek.R.layout.layout_progress_photo_succeed,
                fit.lerchek.R.layout.layout_progress_make_photos,
                fit.lerchek.R.layout.layout_progress_video_error,
                fit.lerchek.R.layout.layout_progress_make_video,
                fit.lerchek.R.layout.layout_progress_week_result});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.nsvMakeMeasurements, 13);
        sViewsWithIds.put(R.id.iconSelect, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressPhotoErrorBinding mboundView11;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressPhotoSucceedBinding mboundView12;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressVideoErrorBinding mboundView13;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @Nullable
    private final fit.lerchek.databinding.LayoutProgressBinding mboundView5;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback19;
    @Nullable
    private final android.view.View.OnClickListener mCallback20;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMakeMeasurementsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentMakeMeasurementsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 6
            , (android.widget.LinearLayout) bindings[4]
            , (android.widget.LinearLayout) bindings[2]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[14]
            , (androidx.core.widget.NestedScrollView) bindings[13]
            , (android.widget.FrameLayout) bindings[5]
            , (fit.lerchek.databinding.LayoutProgressMakePhotosBinding) bindings[9]
            , (fit.lerchek.databinding.LayoutProgressMakeVideoBinding) bindings[11]
            , (fit.lerchek.databinding.LayoutProgressWeekResultBinding) bindings[12]
            );
        this.btnFAQ.setTag(null);
        this.btnSelectWeek.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView11 = (fit.lerchek.databinding.LayoutProgressPhotoErrorBinding) bindings[7];
        setContainedBinding(this.mboundView11);
        this.mboundView12 = (fit.lerchek.databinding.LayoutProgressPhotoSucceedBinding) bindings[8];
        setContainedBinding(this.mboundView12);
        this.mboundView13 = (fit.lerchek.databinding.LayoutProgressVideoErrorBinding) bindings[10];
        setContainedBinding(this.mboundView13);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView5 = (bindings[6] != null) ? fit.lerchek.databinding.LayoutProgressBinding.bind((android.view.View) bindings[6]) : null;
        this.progressBlock.setTag(null);
        setContainedBinding(this.progressMakePhotos);
        setContainedBinding(this.progressMakeVideo);
        setContainedBinding(this.progressResult);
        setRootTag(root);
        // listeners
        mCallback19 = new fit.lerchek.generated.callback.OnClickListener(this, 1);
        mCallback20 = new fit.lerchek.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x100L;
        }
        mboundView11.invalidateAll();
        mboundView12.invalidateAll();
        progressMakePhotos.invalidateAll();
        mboundView13.invalidateAll();
        progressMakeVideo.invalidateAll();
        progressResult.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (mboundView11.hasPendingBindings()) {
            return true;
        }
        if (mboundView12.hasPendingBindings()) {
            return true;
        }
        if (progressMakePhotos.hasPendingBindings()) {
            return true;
        }
        if (mboundView13.hasPendingBindings()) {
            return true;
        }
        if (progressMakeVideo.hasPendingBindings()) {
            return true;
        }
        if (progressResult.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel) variable);
        }
        else if (BR.callback == variableId) {
            setCallback((fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel Model) {
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x40L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }
    public void setCallback(@Nullable fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback Callback) {
        this.mCallback = Callback;
        synchronized(this) {
            mDirtyFlags |= 0x80L;
        }
        notifyPropertyChanged(BR.callback);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        mboundView11.setLifecycleOwner(lifecycleOwner);
        mboundView12.setLifecycleOwner(lifecycleOwner);
        progressMakePhotos.setLifecycleOwner(lifecycleOwner);
        mboundView13.setLifecycleOwner(lifecycleOwner);
        progressMakeVideo.setLifecycleOwner(lifecycleOwner);
        progressResult.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeModelSelectedWeek((androidx.databinding.ObservableInt) object, fieldId);
            case 1 :
                return onChangeModelIsProgress((androidx.databinding.ObservableBoolean) object, fieldId);
            case 2 :
                return onChangeProgressResult((fit.lerchek.databinding.LayoutProgressWeekResultBinding) object, fieldId);
            case 3 :
                return onChangeModelProgress((androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel>) object, fieldId);
            case 4 :
                return onChangeProgressMakeVideo((fit.lerchek.databinding.LayoutProgressMakeVideoBinding) object, fieldId);
            case 5 :
                return onChangeProgressMakePhotos((fit.lerchek.databinding.LayoutProgressMakePhotosBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeModelSelectedWeek(androidx.databinding.ObservableInt ModelSelectedWeek, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelIsProgress(androidx.databinding.ObservableBoolean ModelIsProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressResult(fit.lerchek.databinding.LayoutProgressWeekResultBinding ProgressResult, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeModelProgress(androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> ModelProgress, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressMakeVideo(fit.lerchek.databinding.LayoutProgressMakeVideoBinding ProgressMakeVideo, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeProgressMakePhotos(fit.lerchek.databinding.LayoutProgressMakePhotosBinding ProgressMakePhotos, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel model = mModel;
        int modelSelectedWeekGet = 0;
        int modelProgressVideoErrorModelSelectedWeekViewVISIBLEViewGONE = 0;
        boolean modelProgressVideoErrorModelSelectedWeek = false;
        boolean modelProgressStatisticWeekModelSelectedWeekShowVideo = false;
        int modelProgressStatisticWeekModelSelectedWeekShowPhotoViewVISIBLEViewGONE = 0;
        int modelProgressPhotosErrorModelSelectedWeekViewVISIBLEViewGONE = 0;
        int modelProgressStatisticWeekModelSelectedWeekShowVideoViewVISIBLEViewGONE = 0;
        boolean modelIsProgressGet = false;
        androidx.databinding.ObservableInt modelSelectedWeek = null;
        java.lang.String modelProgressPhotosReviewCommentModelSelectedWeek = null;
        fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback callback = mCallback;
        fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel modelProgressGet = null;
        boolean modelProgressPhotosErrorModelSelectedWeek = false;
        java.lang.String modelProgressVideoReviewCommentModelSelectedWeek = null;
        androidx.databinding.ObservableBoolean modelIsProgress = null;
        androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> modelProgress = null;
        int modelProgressPhotoSucceedModelSelectedWeekViewVISIBLEViewGONE = 0;
        java.lang.String modelProgressWeekModelSelectedWeekName = null;
        boolean modelProgressPhotoSucceedModelSelectedWeek = false;
        fit.lerchek.data.api.model.StatisticsWeek modelProgressStatisticWeekModelSelectedWeek = null;
        boolean modelProgressStatisticWeekModelSelectedWeekShowPhoto = false;
        fit.lerchek.data.api.model.Week modelProgressWeekModelSelectedWeek = null;
        int modelIsProgressViewVISIBLEViewGONE = 0;

        if ((dirtyFlags & 0x14bL) != 0) {


            if ((dirtyFlags & 0x149L) != 0) {

                    if (model != null) {
                        // read model.selectedWeek
                        modelSelectedWeek = model.getSelectedWeek();
                        // read model.progress
                        modelProgress = model.getProgress();
                    }
                    updateRegistration(0, modelSelectedWeek);
                    updateRegistration(3, modelProgress);


                    if (modelSelectedWeek != null) {
                        // read model.selectedWeek.get()
                        modelSelectedWeekGet = modelSelectedWeek.get();
                    }
                    if (modelProgress != null) {
                        // read model.progress.get()
                        modelProgressGet = modelProgress.get();
                    }


                    if (modelProgressGet != null) {
                        // read model.progress.get().videoError(model.selectedWeek.get())
                        modelProgressVideoErrorModelSelectedWeek = modelProgressGet.videoError(modelSelectedWeekGet);
                        // read model.progress.get().photosReviewComment(model.selectedWeek.get())
                        modelProgressPhotosReviewCommentModelSelectedWeek = modelProgressGet.photosReviewComment(modelSelectedWeekGet);
                        // read model.progress.get().photosError(model.selectedWeek.get())
                        modelProgressPhotosErrorModelSelectedWeek = modelProgressGet.photosError(modelSelectedWeekGet);
                        // read model.progress.get().videoReviewComment(model.selectedWeek.get())
                        modelProgressVideoReviewCommentModelSelectedWeek = modelProgressGet.videoReviewComment(modelSelectedWeekGet);
                        // read model.progress.get().photoSucceed(model.selectedWeek.get())
                        modelProgressPhotoSucceedModelSelectedWeek = modelProgressGet.photoSucceed(modelSelectedWeekGet);
                        // read model.progress.get().statisticWeek(model.selectedWeek.get())
                        modelProgressStatisticWeekModelSelectedWeek = modelProgressGet.statisticWeek(modelSelectedWeekGet);
                        // read model.progress.get().week(model.selectedWeek.get())
                        modelProgressWeekModelSelectedWeek = modelProgressGet.week(modelSelectedWeekGet);
                    }
                if((dirtyFlags & 0x149L) != 0) {
                    if(modelProgressVideoErrorModelSelectedWeek) {
                            dirtyFlags |= 0x400L;
                    }
                    else {
                            dirtyFlags |= 0x200L;
                    }
                }
                if((dirtyFlags & 0x149L) != 0) {
                    if(modelProgressPhotosErrorModelSelectedWeek) {
                            dirtyFlags |= 0x4000L;
                    }
                    else {
                            dirtyFlags |= 0x2000L;
                    }
                }
                if((dirtyFlags & 0x149L) != 0) {
                    if(modelProgressPhotoSucceedModelSelectedWeek) {
                            dirtyFlags |= 0x40000L;
                    }
                    else {
                            dirtyFlags |= 0x20000L;
                    }
                }


                    // read model.progress.get().videoError(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
                    modelProgressVideoErrorModelSelectedWeekViewVISIBLEViewGONE = ((modelProgressVideoErrorModelSelectedWeek) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read model.progress.get().photosError(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
                    modelProgressPhotosErrorModelSelectedWeekViewVISIBLEViewGONE = ((modelProgressPhotosErrorModelSelectedWeek) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read model.progress.get().photoSucceed(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
                    modelProgressPhotoSucceedModelSelectedWeekViewVISIBLEViewGONE = ((modelProgressPhotoSucceedModelSelectedWeek) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    if (modelProgressStatisticWeekModelSelectedWeek != null) {
                        // read model.progress.get().statisticWeek(model.selectedWeek.get()).show_video
                        modelProgressStatisticWeekModelSelectedWeekShowVideo = modelProgressStatisticWeekModelSelectedWeek.getShow_video();
                        // read model.progress.get().statisticWeek(model.selectedWeek.get()).show_photo
                        modelProgressStatisticWeekModelSelectedWeekShowPhoto = modelProgressStatisticWeekModelSelectedWeek.getShow_photo();
                    }
                if((dirtyFlags & 0x149L) != 0) {
                    if(modelProgressStatisticWeekModelSelectedWeekShowVideo) {
                            dirtyFlags |= 0x10000L;
                    }
                    else {
                            dirtyFlags |= 0x8000L;
                    }
                }
                if((dirtyFlags & 0x149L) != 0) {
                    if(modelProgressStatisticWeekModelSelectedWeekShowPhoto) {
                            dirtyFlags |= 0x1000L;
                    }
                    else {
                            dirtyFlags |= 0x800L;
                    }
                }
                    if (modelProgressWeekModelSelectedWeek != null) {
                        // read model.progress.get().week(model.selectedWeek.get()).name
                        modelProgressWeekModelSelectedWeekName = modelProgressWeekModelSelectedWeek.getName();
                    }


                    // read model.progress.get().statisticWeek(model.selectedWeek.get()).show_video ? View.VISIBLE : View.GONE
                    modelProgressStatisticWeekModelSelectedWeekShowVideoViewVISIBLEViewGONE = ((modelProgressStatisticWeekModelSelectedWeekShowVideo) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read model.progress.get().statisticWeek(model.selectedWeek.get()).show_photo ? View.VISIBLE : View.GONE
                    modelProgressStatisticWeekModelSelectedWeekShowPhotoViewVISIBLEViewGONE = ((modelProgressStatisticWeekModelSelectedWeekShowPhoto) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
            if ((dirtyFlags & 0x142L) != 0) {

                    if (model != null) {
                        // read model.isProgress()
                        modelIsProgress = model.isProgress();
                    }
                    updateRegistration(1, modelIsProgress);


                    if (modelIsProgress != null) {
                        // read model.isProgress().get()
                        modelIsProgressGet = modelIsProgress.get();
                    }
                if((dirtyFlags & 0x142L) != 0) {
                    if(modelIsProgressGet) {
                            dirtyFlags |= 0x100000L;
                    }
                    else {
                            dirtyFlags |= 0x80000L;
                    }
                }


                    // read model.isProgress().get() ? View.VISIBLE : View.GONE
                    modelIsProgressViewVISIBLEViewGONE = ((modelIsProgressGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
            }
        }
        if ((dirtyFlags & 0x180L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x100L) != 0) {
            // api target 1

            this.btnFAQ.setOnClickListener(mCallback20);
            this.btnSelectWeek.setOnClickListener(mCallback19);
        }
        if ((dirtyFlags & 0x149L) != 0) {
            // api target 1

            this.mboundView11.getRoot().setVisibility(modelProgressPhotosErrorModelSelectedWeekViewVISIBLEViewGONE);
            this.mboundView11.setErrorMessage(modelProgressPhotosReviewCommentModelSelectedWeek);
            this.mboundView12.getRoot().setVisibility(modelProgressPhotoSucceedModelSelectedWeekViewVISIBLEViewGONE);
            this.mboundView13.setErrorMessage(modelProgressVideoReviewCommentModelSelectedWeek);
            this.mboundView13.getRoot().setVisibility(modelProgressVideoErrorModelSelectedWeekViewVISIBLEViewGONE);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, modelProgressWeekModelSelectedWeekName);
            this.progressMakePhotos.getRoot().setVisibility(modelProgressStatisticWeekModelSelectedWeekShowPhotoViewVISIBLEViewGONE);
            this.progressMakeVideo.getRoot().setVisibility(modelProgressStatisticWeekModelSelectedWeekShowVideoViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x142L) != 0) {
            // api target 1

            this.progressBlock.setVisibility(modelIsProgressViewVISIBLEViewGONE);
        }
        if ((dirtyFlags & 0x180L) != 0) {
            // api target 1

            this.progressMakePhotos.setCallback(callback);
            this.progressResult.setCallback(callback);
        }
        if ((dirtyFlags & 0x140L) != 0) {
            // api target 1

            this.progressMakePhotos.setModel(model);
            this.progressMakeVideo.setModel(model);
            this.progressResult.setModel(model);
        }
        executeBindingsOn(mboundView11);
        executeBindingsOn(mboundView12);
        executeBindingsOn(progressMakePhotos);
        executeBindingsOn(mboundView13);
        executeBindingsOn(progressMakeVideo);
        executeBindingsOn(progressResult);
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // model
                fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsViewModel model = mModel;
                // model.progress != null
                boolean modelProgressJavaLangObjectNull = false;
                // model != null
                boolean modelJavaLangObjectNull = false;
                // model.progress
                androidx.databinding.ObservableField<fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel> modelProgress = null;
                // model.progress.get() != null
                boolean modelProgressGetJavaLangObjectNull = false;
                // callback
                fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback callback = mCallback;
                // model.progress.get()
                fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel modelProgressGet = null;
                // model.progress.get().weeks
                java.util.List<fit.lerchek.data.api.model.Week> modelProgressWeeks = null;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {



                    modelJavaLangObjectNull = (model) != (null);
                    if (modelJavaLangObjectNull) {


                        modelProgress = model.getProgress();

                        modelProgressJavaLangObjectNull = (modelProgress) != (null);
                        if (modelProgressJavaLangObjectNull) {


                            modelProgressGet = modelProgress.get();

                            modelProgressGetJavaLangObjectNull = (modelProgressGet) != (null);
                            if (modelProgressGetJavaLangObjectNull) {


                                modelProgressWeeks = modelProgressGet.getWeeks();

                                callback.onSelectWeekClick(modelProgressWeeks);
                            }
                        }
                    }
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // callback
                fit.lerchek.ui.feature.marathon.progress.MakeMeasurementsCallback callback = mCallback;
                // callback != null
                boolean callbackJavaLangObjectNull = false;



                callbackJavaLangObjectNull = (callback) != (null);
                if (callbackJavaLangObjectNull) {


                    callback.onFAQClick();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model.selectedWeek
        flag 1 (0x2L): model.isProgress()
        flag 2 (0x3L): progressResult
        flag 3 (0x4L): model.progress
        flag 4 (0x5L): progressMakeVideo
        flag 5 (0x6L): progressMakePhotos
        flag 6 (0x7L): model
        flag 7 (0x8L): callback
        flag 8 (0x9L): null
        flag 9 (0xaL): model.progress.get().videoError(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
        flag 10 (0xbL): model.progress.get().videoError(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
        flag 11 (0xcL): model.progress.get().statisticWeek(model.selectedWeek.get()).show_photo ? View.VISIBLE : View.GONE
        flag 12 (0xdL): model.progress.get().statisticWeek(model.selectedWeek.get()).show_photo ? View.VISIBLE : View.GONE
        flag 13 (0xeL): model.progress.get().photosError(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
        flag 14 (0xfL): model.progress.get().photosError(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
        flag 15 (0x10L): model.progress.get().statisticWeek(model.selectedWeek.get()).show_video ? View.VISIBLE : View.GONE
        flag 16 (0x11L): model.progress.get().statisticWeek(model.selectedWeek.get()).show_video ? View.VISIBLE : View.GONE
        flag 17 (0x12L): model.progress.get().photoSucceed(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
        flag 18 (0x13L): model.progress.get().photoSucceed(model.selectedWeek.get()) ? View.VISIBLE : View.GONE
        flag 19 (0x14L): model.isProgress().get() ? View.VISIBLE : View.GONE
        flag 20 (0x15L): model.isProgress().get() ? View.VISIBLE : View.GONE
    flag mapping end*/
    //end
}