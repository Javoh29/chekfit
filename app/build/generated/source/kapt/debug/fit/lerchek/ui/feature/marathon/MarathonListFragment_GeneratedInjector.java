package fit.lerchek.ui.feature.marathon;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = MarathonListFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface MarathonListFragment_GeneratedInjector {
  void injectMarathonListFragment(MarathonListFragment marathonListFragment);
}
