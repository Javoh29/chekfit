package androidx.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int calendarCallback = 1;

  public static final int callback = 2;

  public static final int context = 3;

  public static final int description = 4;

  public static final int errorMessage = 5;

  public static final int faqBackCallback = 6;

  public static final int headerText = 7;

  public static final int isReplacement = 8;

  public static final int item = 9;

  public static final int lifecycleHolder = 10;

  public static final int link = 11;

  public static final int message = 12;

  public static final int model = 13;

  public static final int price = 14;

  public static final int progress = 15;

  public static final int showErrorHint = 16;

  public static final int showSuccessHint = 17;

  public static final int taskCallback = 18;

  public static final int text = 19;

  public static final int title = 20;

  public static final int viewModel = 21;
}
