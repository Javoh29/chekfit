package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "fit.lerchek.LerchekFitApp",
    originatingRoot = "fit.lerchek.LerchekFitApp",
    rootAnnotation = HiltAndroidApp.class
)
public class _fit_lerchek_LerchekFitApp {
}
