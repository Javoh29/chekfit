package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@ProcessedRootSentinel(
    roots = "fit.lerchek.LerchekFitApp"
)
public class _fit_lerchek_LerchekFitApp {
}
