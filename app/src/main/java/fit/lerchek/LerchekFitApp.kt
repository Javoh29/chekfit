package fit.lerchek

import android.app.Application
import android.util.Log
import dagger.hilt.android.HiltAndroidApp
import fit.lerchek.data.fcm.FMService.Companion.initNotificationChannel
import fit.lerchek.data.fcm.FMService.Companion.prepareFCMToken
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.util.ThemeUtil
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject

@HiltAndroidApp
class LerchekFitApp : Application() {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var userRepository: UserRepository

    companion object {
        lateinit var instance: LerchekFitApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        setupTheme()

        initNotificationChannel(this)
        prepareFCMToken { new ->
            Log.w("TAG_TOKEN", new ?: "")
            dataManager.getFCMToken().let { stored ->
                if (new.isNullOrEmpty().not() && new != stored) {
                    userRepository.updateFCMToken(new!!)
                }
            }
        }
        /*override default undelivered exceptions handler for Rx to make it not call uncaught exceptions to runtime*/
        RxJavaPlugins.setErrorHandler {  }
    }

    private fun setupTheme() {
        if (ThemeUtil.isNightModeSupported()) {
            dataManager.isStoredNightMode().let { storedMode ->
                if (storedMode == null) {
                    ThemeUtil.setNightMode(ThemeUtil.isGlobalNightMode(resources))
                    dataManager.saveIsNightMode(ThemeUtil.isNightMode())
                } else {
                    ThemeUtil.setNightMode(storedMode)
                }
            }
        }
    }
}