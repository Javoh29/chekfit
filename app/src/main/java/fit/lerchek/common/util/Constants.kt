package fit.lerchek.common.util

object Constants {
    const val WEB_HOME = "https://lerchek.fit/"
    const val DEV_URL = "https://lf.letique.ru/"
    const val PROD_URL = "https://lerchek.fit/"
    //"2021-06-27T21:00:00.000000Z"
    const val TIME_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
    const val TIME_FORMAT_PATTERN_SIMPLE = "yyyy-MM-dd"
    const val DEFAULT_DELAY = 500L
    const val LABEL_REF_CLIP_DATA = "referral_link"
    const val LONG_DELAY = 1000L
    const val UNKNOWN_ERROR = "Произошла ошибка"
}