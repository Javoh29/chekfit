package fit.lerchek.common.util

import java.text.SimpleDateFormat
import java.util.*

object DateFormatUtils {

    fun formatServerDate(date: Date?): String? {
        if (date == null) {
            return null
        }
        return SimpleDateFormat(Constants.TIME_FORMAT_PATTERN, Locale.ENGLISH).format(date)
    }

    fun parseServerDate(input: String): Date? {
        return try {
            SimpleDateFormat(Constants.TIME_FORMAT_PATTERN, Locale.ENGLISH).parse(input)
        } catch (ex: Exception) {
            null
        }
    }
    fun parseServerSimpleDate(input: String): Date? {
        return try {
            SimpleDateFormat(Constants.TIME_FORMAT_PATTERN_SIMPLE, Locale.ENGLISH).parse(input)
        } catch (ex: Exception) {
            null
        }
    }

    fun formatDateSimple(date: Date?): String? {
        date?.let {
            return SimpleDateFormat("yyyy.MM.dd", Locale("RU")).format(it)
        }
        return null
    }

    fun formatDateSimpleWithDOW(date: Date?): String? {
        date?.let {
            return SimpleDateFormat("EEEE, d MMMM", Locale("RU")).format(it)
        }
        return null
    }

    fun formatChatDateTime(date: Date?): String? {
        date?.let {
            return SimpleDateFormat("HH:mm, dd.MM.yyyy", Locale("RU")).format(it)
        }
        return null
    }

    fun formatDateTech(date: Date?): String? {
        date?.let {
            return SimpleDateFormat("dd/MM/yyy, HH:mm", Locale("RU")).format(it)
        }
        return null
    }

    fun formatDateDayFullMonth(date: Date?): String? {
        date?.let {
            return SimpleDateFormat("dd MMMM", Locale("RU")).format(it)
        }
        return null
    }
}


fun String.toDate(dateFormat: String = "yyyy-MM-dd HH:mm:ss", timeZone: TimeZone = TimeZone.getTimeZone("UTC")): Date {
    val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
    parser.timeZone = timeZone
    return parser.parse(this)
}

fun Date.toString(dateFormat: String): String {
    val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    formatter.timeZone  = TimeZone.getDefault()
    return formatter.format(this)
}
