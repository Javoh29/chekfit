package fit.lerchek.ui.util

fun <E> MutableCollection<E>.clearAndAddAll(items: Collection<E>) {
    clear()
    addAll(items)
}