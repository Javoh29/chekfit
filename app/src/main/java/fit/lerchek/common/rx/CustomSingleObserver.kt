package fit.lerchek.common.rx

import androidx.databinding.ObservableBoolean
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.exceptions.CommonException
import io.reactivex.observers.DisposableSingleObserver

abstract class CustomSingleObserver<T>(
    private var isProgress: ObservableBoolean? = null,
    private var onErrorListener: OnErrorListener? = null
) : DisposableSingleObserver<T>() {

    override fun onStart() {
        super.onStart()
        isProgress?.set(true)
    }

    override fun onSuccess(result: T) {
        isProgress?.set(false)
    }

    override fun onError(e: Throwable) {
        if (onErrorListener != null) {
            if (e is CommonException)
                onErrorListener?.onError(code = e.code.toString(), message = e.message)
            else
                onErrorListener?.onError(
                    message = e.message ?: Constants.UNKNOWN_ERROR
                )
        } else {
            isProgress?.set(false)
        }
    }
}