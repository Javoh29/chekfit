package fit.lerchek.common.rx

import androidx.databinding.ObservableBoolean
import fit.lerchek.common.util.Constants
import io.reactivex.observers.DisposableCompletableObserver

abstract class CustomCompletableObserver(
    private var isProgress: ObservableBoolean? = null,
    private var onErrorListener: OnErrorListener? = null
) : DisposableCompletableObserver() {

    override fun onStart() {
        super.onStart()
        isProgress?.set(true)
    }

    override fun onComplete() {
        isProgress?.set(false)
    }

    override fun onError(e: Throwable) {
        if (onErrorListener != null) {
            onErrorListener?.onError(
                message = e.message?: Constants.UNKNOWN_ERROR
            )
        } else {
            isProgress?.set(false)
        }
    }
}