package fit.lerchek.common.di

import android.util.Log
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fit.lerchek.BuildConfig
import fit.lerchek.common.util.Constants.PROD_URL
import fit.lerchek.data.managers.DataManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private const val TIMEOUT = 2L
    private const val HEADER_AUTH = "Authorization"
    private const val HEADER_AUTH_BEARER = "Bearer"
    private const val MOBILE = "mobile"
    private const val ANDROID = "android"


    @Provides
    @Singleton
    fun provideHttpClient(dataManager: DataManager): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(TIMEOUT, TimeUnit.MINUTES)
            .writeTimeout(TIMEOUT, TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT, TimeUnit.MINUTES)
            .retryOnConnectionFailure(true)
            .addInterceptor { original ->
                val requestBuilder = original.request().newBuilder()
                dataManager.getAuthToken()?.let {
                    requestBuilder.addHeader(HEADER_AUTH, "$HEADER_AUTH_BEARER $it")
                    Log.d("TOKEN", it)
                }

                requestBuilder.addHeader(MOBILE , ANDROID)

                original.proceed(requestBuilder.build())
            }
            .addInterceptor(HttpLoggingInterceptor().apply {
                if (BuildConfig.DEBUG)
                    level = HttpLoggingInterceptor.Level.BODY
            })
            .addInterceptor(HttpLoggingInterceptor().apply {
                if (BuildConfig.DEBUG)
                    level = HttpLoggingInterceptor.Level.HEADERS
            })
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(PROD_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }
}