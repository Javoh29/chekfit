package fit.lerchek.common.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fit.lerchek.data.api.UserRest
import fit.lerchek.data.api.UserRestImpl
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.data.repository.user.UserRepositoryImpl

@Module
@InstallIn(SingletonComponent::class)
abstract class BindingModule {
    @Binds
    abstract fun userRepository(repository : UserRepositoryImpl) : UserRepository
    @Binds
    abstract fun userService(rest: UserRestImpl): UserRest
}