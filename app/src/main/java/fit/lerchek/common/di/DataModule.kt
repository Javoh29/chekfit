package fit.lerchek.common.di

import android.content.Context
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import fit.lerchek.common.rx.SchedulerProvider
import fit.lerchek.common.rx.SchedulerProviderImpl
import fit.lerchek.data.domain.converter.UserDataConverter
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.managers.DataManagerImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideDataManager(@ApplicationContext context: Context, gson: Gson): DataManager {
        return DataManagerImpl(context, gson)
    }

    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider {
        return SchedulerProviderImpl()
    }

    @Provides
    @Singleton
    fun provideUserDataConverter() = UserDataConverter()

    @Provides
    fun provideGson() = Gson()
}