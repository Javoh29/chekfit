package fit.lerchek.ui.util

import android.content.Context
import android.text.TextUtils
import android.util.DisplayMetrics
import java.util.regex.Pattern

class SupportUtils {
    companion object{
        fun isValidEmail(target: String?): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun convertDpToPixel(dp: Float, context: Context): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            return dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
        }

        private const val PATTERN_PASSWORD = "(?=.*[A-Za-zА-Яа-я])(?=.*\\d)[A-Za-zА-Яа-я\\d]{8,255}"

        fun isValidPassword(password: String?): Boolean {
            return !TextUtils.isEmpty(password) && Pattern.compile(PATTERN_PASSWORD).matcher(password).matches()
        }
    }
}