package fit.lerchek.ui.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import fit.lerchek.R
import fit.lerchek.ui.base.BaseActivity

object BrowserUtils {

    fun openBrowser(context: BaseActivity, link: String, requestCode: Int? = null) {
        openLink(context, link, requestCode)
    }

    fun shareLink(context: Context, link: String, description: String = "") {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.putExtra(Intent.EXTRA_TEXT, link)
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, description)
        shareIntent.type = "text/plain"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        try {
            context.startActivity(
                Intent.createChooser(
                    shareIntent,
                    context.getString(R.string.today_ref_share)
                )
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.show(
                context,
                "Подходящих приложений не найдено"
            )
        }
    }

    fun getVideoEmbedUrl(videoId: String): String {
        return "https://www.youtube.com/embed/$videoId?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=1&frameborder=0&fs=0"
    }

    fun getVideoSettingsSetupJs(): String {
        return """javascript:(function f() {
                    document.querySelector('.ytp-show-cards-title').style.display = 'none';
                  })()"""
    }

    private fun openLink(context: BaseActivity, link: String, requestCode: Int?) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        startActivity(intent, context, requestCode)
    }

    private fun startActivity(intent: Intent, context: BaseActivity, requestCode: Int?) {
        if (intent.resolveActivity(context.packageManager) != null) {
            if (requestCode == null) {
                context.startActivity(intent)
            } else {
                context.getActivityResultLauncher(requestCode)?.launch(intent)
            }
            return
        }
    }
}