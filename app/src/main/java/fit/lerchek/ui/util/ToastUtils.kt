package fit.lerchek.ui.util

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.WindowManager
import android.widget.Toast

object ToastUtils {

    fun show(context: Context, idString: Int) {
        Handler(Looper.getMainLooper()).post {
            try {
                Toast.makeText(context, idString, Toast.LENGTH_SHORT).show()
            } catch (e: WindowManager.BadTokenException) {
                Log.e(ToastUtils::class.java.simpleName, "Activity is not running")
            }
        }
    }

    fun show(context: Context, str: String) {
        Handler(Looper.getMainLooper()).post{
            try {
                Toast.makeText(context, str, Toast.LENGTH_SHORT).show()
            } catch (e: WindowManager.BadTokenException) {
                Log.e(ToastUtils::class.java.simpleName, "Activity is not running")
            }
        }

    }

    fun show(context: Context, idString: Int, duration: Int) {
        Handler(Looper.getMainLooper()).post {
            try {
                Toast.makeText(context, idString, duration).show()
            } catch (e: WindowManager.BadTokenException) {
                Log.e(ToastUtils::class.java.simpleName, "Activity is not running")
            }
        }
    }

    fun show(context: Context, str: String, duration: Int) {
        Handler(Looper.getMainLooper()).post {
            try {
                Toast.makeText(context, str, duration).show()
            } catch (e: WindowManager.BadTokenException) {
                Log.e(ToastUtils::class.java.simpleName, "Activity is not running")
            }
        }

    }
}