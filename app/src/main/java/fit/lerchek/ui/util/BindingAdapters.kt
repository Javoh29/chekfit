package fit.lerchek.ui.util

import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.text.Html
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter

@BindingAdapter("elevationOutline")
fun updateElevationOutline(view: View, errorString: String?) {
    val elevation = if (errorString.isNullOrEmpty()) {
        1f
    } else {
        SupportUtils.convertDpToPixel(12f, view.context)
    }

    view.elevation = elevation
    view.invalidateOutline()
    view.invalidate()
}

@Suppress("DEPRECATION")
@BindingAdapter("htmlText")
fun setHtmlText(textView: AppCompatTextView, htmlText: String?) {
    textView.text = when {
        htmlText.isNullOrEmpty() -> ""
        SDK_INT >= Build.VERSION_CODES.N -> Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY)
        else -> Html.fromHtml(htmlText)
    }
}

@BindingAdapter("spannableText")
fun setSpannableText(textView: AppCompatTextView, ssb: SpannableStringBuilder?) {
    textView.setText(ssb, TextView.BufferType.SPANNABLE)
}