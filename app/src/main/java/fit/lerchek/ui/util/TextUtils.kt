package fit.lerchek.ui.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import kotlin.math.roundToInt


object TextUtils {
    fun Context.copyTextToBuffer(label: String, text: String) {
        val clipboard: ClipboardManager? =
            getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
        val clip = ClipData.newPlainText(label, text)
        clipboard?.setPrimaryClip(clip)
    }

    fun Double.convertWithTwoDecimal(): String {
        return if (this * 100 % 100 == 0.0) "${this.roundToInt()}"
        else String.format("%.2f", this).replace(",", ".")
    }

    fun Float.convertWithOneDecimal(): String {
        return if (this * 10 % 10 == 0F) "${this.roundToInt()}"
        else String.format("%.1f", this).replace(",", ".")
    }

    /**
     * Searches for all URLSpans in current text replaces them with our own ClickableSpans
     * forwards clicks to provided function.
     */
    fun AppCompatTextView.handleUrlClicks(onClicked: ((String) -> Unit)? = null) {
        //create span builder and replaces current text with it
        text = SpannableStringBuilder.valueOf(text).apply {
            //search for all URL spans and replace all spans with our own clickable spans
            getSpans(0, length, URLSpan::class.java).forEach {
                //add new clickable span at the same position
                setSpan(
                    object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            onClicked?.invoke(it.url)
                        }
                    },
                    getSpanStart(it),
                    getSpanEnd(it),
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE
                )
                //remove old URLSpan
                removeSpan(it)
            }
        }
        //make sure movement method is set
        movementMethod = LinkMovementMethod.getInstance()
    }
}