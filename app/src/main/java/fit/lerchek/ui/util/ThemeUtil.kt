package fit.lerchek.ui.util

import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.Build.VERSION
import androidx.appcompat.app.AppCompatDelegate

object ThemeUtil {

    fun isNightModeSupported() = VERSION.SDK_INT >= Build.VERSION_CODES.Q

    fun isNightMode() = if (isNightModeSupported()) {
        AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
    } else {
        false
    }

    fun setNightMode(isNightMode: Boolean) {
        if (isNightModeSupported()) {
            AppCompatDelegate.setDefaultNightMode(
                if (isNightMode) {
                    AppCompatDelegate.MODE_NIGHT_YES
                } else {
                    AppCompatDelegate.MODE_NIGHT_NO
                }
            )
        }
    }

    fun isGlobalNightMode(resources: Resources) =
        isNightModeSupported() && (resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES)
}