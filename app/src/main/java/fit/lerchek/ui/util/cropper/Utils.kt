package fit.lerchek.ui.util.cropper

import fit.lerchek.R

object Utils {
    fun getBodyOverlayByType(type: CropImageView.CropShape): Int {
        return when (type) {
            CropImageView.CropShape.BODY_FRONT_MALE -> R.drawable.male_front
            CropImageView.CropShape.BODY_FRONT_FEMALE -> R.drawable.female_front
            CropImageView.CropShape.BODY_BACK_MALE -> R.drawable.male_back
            CropImageView.CropShape.BODY_BACK_FEMALE -> R.drawable.female_back
            CropImageView.CropShape.BODY_SIDE_MALE -> R.drawable.male_left
            CropImageView.CropShape.BODY_SIDE_FEMALE -> R.drawable.female_left
            else -> R.drawable.male_front
        }
    }
}