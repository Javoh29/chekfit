package fit.lerchek.ui.util

import android.graphics.Bitmap
import androidx.exifinterface.media.ExifInterface
import fit.lerchek.LerchekFitApp
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.*

object FileUtils {
    fun buildImageBodyPart(name:String, fileName: String, bitmap: Bitmap): MultipartBody.Part {
        val imageFile = convertBitmapToFile(fileName, bitmap).apply {
            ExifInterface(this).apply {
                getAttributeInt(ExifInterface.TAG_ORIENTATION, 1).apply {

                }
            }
        }
        val reqFile = imageFile.asRequestBody("image/jpeg".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData(name, imageFile.name, reqFile)
    }

    fun convertBitmapToFile(fileName: String, bitmap: Bitmap): File {
        //create a file to write bitmap data
        val file = File(LerchekFitApp.instance.cacheDir, fileName)
        file.createNewFile()

        //Convert bitmap to byte array
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        val bitMapData = bos.toByteArray()

        //write the bytes in file
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        try {
            fos?.write(bitMapData)
            fos?.flush()
            fos?.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }
}