package fit.lerchek.ui.widget.linechart;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;

import fit.lerchek.R;

public class ChartProgressBar extends FrameLayout {

    private int mMaxValue;
    private int mBarWidth;
    private int mBarHeight;
    private int mProgressColor;
    private int mProgressDrawable;
    private int mProgressDrawable2;
    private Context mContext;
    private int mBarTitleColor;
    private float mBarTitleTxtSize;
    private DisplayMetrics mMetrics;
    private FrameLayout oldFrameLayout;
    private boolean isBarCanBeClick;
    private ArrayList<BarData> mDataList;
    private boolean isOldBarClicked;
    private boolean isBarsEmpty;
    private int mBarTitleMarginTop;
    private int mBarTitleSelectedColor;
    private int selectBarIndex = 0;
    private int mGoal = 0;
    private int mProgressDisableColor;
    private OnBarClickedListener listener;
    private boolean mBarCanBeToggle;

    public ChartProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        setAttrs(attrs, 0);
        mMetrics = Resources.getSystem().getDisplayMetrics();
    }

    public ChartProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        setAttrs(attrs, defStyleAttr);
        mMetrics = Resources.getSystem().getDisplayMetrics();
    }

    private void setAttrs(AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = mContext.obtainStyledAttributes(attrs, R.styleable.ChartProgressBar, defStyleAttr, 0);
        mBarWidth = typedArray.getDimensionPixelSize(R.styleable.ChartProgressBar_hdBarWidth, 0);
        mBarHeight = typedArray.getDimensionPixelSize(R.styleable.ChartProgressBar_hdBarHeight, 0);
        mProgressDrawable = typedArray.getResourceId(R.styleable.ChartProgressBar_hdProgressDrawable, R.drawable.water_progress_bar_shape);
        mProgressDrawable2 = typedArray.getResourceId(R.styleable.ChartProgressBar_hdProgressDrawable2, R.drawable.water_progress_bar_shape2);
        mProgressColor = typedArray.getResourceId(R.styleable.ChartProgressBar_hdProgressColor, ContextCompat.getColor(mContext, R.color.progress));
        mProgressDisableColor = typedArray.getResourceId(R.styleable.ChartProgressBar_hdProgressDisableColor, ContextCompat.getColor(mContext, android.R.color.darker_gray));
        mBarTitleSelectedColor = typedArray.getResourceId(R.styleable.ChartProgressBar_hdBarTitleSelectedColor, ContextCompat.getColor(mContext, R.color.progress_click));
        isBarCanBeClick = typedArray.getBoolean(R.styleable.ChartProgressBar_hdBarCanBeClick, false);
        mBarTitleColor = typedArray.getResourceId(R.styleable.ChartProgressBar_hdBarTitleColor, ContextCompat.getColor(mContext, R.color.bar_title_color));
        mMaxValue = typedArray.getInt(R.styleable.ChartProgressBar_hdMaxValue, 10);
        mBarTitleTxtSize = typedArray.getDimension(R.styleable.ChartProgressBar_hdBarTitleTxtSize, 14);
        mBarTitleMarginTop = typedArray.getDimensionPixelSize(R.styleable.ChartProgressBar_hdBarTitleMarginTop, 0);
        mBarCanBeToggle = typedArray.getBoolean(R.styleable.ChartProgressBar_hdBarCanBeToggle, false);
        typedArray.recycle();
    }


    public void setDataList(ArrayList<BarData> dataList) {
        mDataList = dataList;
    }


    public void setOnBarClickedListener(OnBarClickedListener listener) {
        this.listener = listener;
    }

    public void build() {

        removeAllViews();

        LinearLayout linearLayout = new LinearLayout(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
        );
        linearLayout.setLayoutParams(params);

        addView(linearLayout);
        int i = 0;
        for (BarData data : mDataList) {
            int barValue = (int) (data.getBarValue() * 100);
            FrameLayout bar = getBar(data.getBarTitle(), barValue, i);
            linearLayout.addView(bar);
            if (selectBarIndex == i) {
                clickBarOn(bar);
                oldFrameLayout = bar;
            }
            i++;
        }

        getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressWarnings("deprecation")
                    @Override
                    public void onGlobalLayout() {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            getViewTreeObserver()
                                    .removeOnGlobalLayoutListener(this);
                        } else {
                            getViewTreeObserver()
                                    .removeGlobalOnLayoutListener(this);
                        }
                    }
                });
    }

    private FrameLayout getBar(final String title, int value, final int index) {

        int maxValue = (int) (mMaxValue * 100);
        int maxGoalValue = (int) (mGoal * 100);

        LinearLayout linearLayout = new LinearLayout(mContext);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                125,
                LayoutParams.MATCH_PARENT
        );

        params.gravity = Gravity.CENTER;
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER);

        //Adding bar
        Bar bar = new Bar(mContext, null, android.R.attr.progressBarStyleHorizontal);
        bar.setVisibility(View.VISIBLE);
        bar.setIndeterminate(false);

        bar.setMax(maxValue);
        LayoutParams progressParams = new LayoutParams(
                mBarWidth,
                mBarHeight
        );

        progressParams.gravity = Gravity.CENTER;
        bar.setLayoutParams(progressParams);

        LayerDrawable layerDrawable = (LayerDrawable) bar.getProgressDrawable();
        layerDrawable.mutate();
        if (value > maxGoalValue) {
            bar.setProgressDrawable(ContextCompat.getDrawable(mContext, mProgressDrawable2));
            bar.setProgress(value);
            bar.setSecondaryProgress(maxGoalValue);
        } else {
            if (value == 0) {
                value = maxGoalValue / 2;
                bar.setProgressDrawable(ContextCompat.getDrawable(mContext, R.drawable.grey_progress_bar_shape));
            } else {
                bar.setProgressDrawable(ContextCompat.getDrawable(mContext, mProgressDrawable));
            }
            bar.setProgress(value);
        }
        ObjectAnimator.ofInt(bar, "progress", 0, value).setDuration(1000).start();

        linearLayout.addView(bar);

        //Adding txt below bar
        TextView txtBar = new TextView(mContext);
        LayoutParams txtParams = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
        );

        txtBar.setTextSize(getSP(mBarTitleTxtSize));
        txtBar.setText(title);
        txtBar.setGravity(Gravity.CENTER);
        txtBar.setTextColor(ContextCompat.getColor(mContext, mBarTitleColor));
        txtBar.setPadding(0, mBarTitleMarginTop, 0, 0);

        txtBar.setLayoutParams(txtParams);

        linearLayout.addView(txtBar);

        FrameLayout rootFrameLayout = new FrameLayout(mContext);
        LinearLayout.LayoutParams rootParams = new LinearLayout.LayoutParams(
                0,
                LayoutParams.MATCH_PARENT,
                1f
        );

        rootParams.gravity = Gravity.CENTER;


        //rootParams.setMargins(0, h, 0, h);
        rootFrameLayout.setLayoutParams(rootParams);


        //Adding bar + title
        rootFrameLayout.addView(linearLayout);

        if (isBarCanBeClick)
            rootFrameLayout.setOnClickListener(barClickListener);

        rootFrameLayout.setTag(index);
        return rootFrameLayout;
    }

    public void setMaxValue(int mMaxValue) {
        this.mMaxValue = mMaxValue;
    }

    public void setGoalValue(int value) {
        this.mGoal = value;
    }

    public void setIsBarCanBeClick(boolean value) {
        this.isBarCanBeClick = value;
    }

    public void setSelectBarIndex(int value) {
        this.selectBarIndex = value;
    }

    public void setProgressDrawable(int value) {
        this.mProgressDrawable = value;
    }

    public void setProgressDrawable2(int value) {
        this.mProgressDrawable2 = value;
    }

    private float getSP(float size) {
        return size / mMetrics.scaledDensity;
    }

    public boolean isBarsEmpty() {
        return isBarsEmpty;
    }

    private FrameLayout.OnClickListener barClickListener = new FrameLayout.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (isBarsEmpty)
                return;

            FrameLayout frameLayout = (FrameLayout) view;


            if (oldFrameLayout == frameLayout && mBarCanBeToggle) {
                if (isOldBarClicked)
                    clickBarOff(frameLayout);
                else
                    clickBarOn(frameLayout);
            } else {

                if (oldFrameLayout != null)
                    clickBarOff(oldFrameLayout);

                clickBarOn(frameLayout);
            }


            oldFrameLayout = frameLayout;

            if (listener != null)
                listener.onBarClicked((int) frameLayout.getTag());

        }
    };

    private void clickBarOn(FrameLayout frameLayout) {

        isOldBarClicked = true;

        int childCount = frameLayout.getChildCount();

        for (int i = 0; i < childCount; i++) {

            View childView = frameLayout.getChildAt(i);
            if (childView instanceof LinearLayout) {

                LinearLayout linearLayout = (LinearLayout) childView;
                Bar bar = (Bar) linearLayout.getChildAt(0);

                LayerDrawable layerDrawable = (LayerDrawable) bar.getProgressDrawable();
                layerDrawable.mutate();

                LinearLayout.LayoutParams progressParams = new LinearLayout.LayoutParams(
                        mBarWidth + 40,
                        mBarHeight
                );
                progressParams.gravity = Gravity.CENTER;
                bar.setLayoutParams(progressParams);

                linearLayout.updateViewLayout(bar, progressParams);

            }
        }
    }

    private void clickBarOff(FrameLayout frameLayout) {

        isOldBarClicked = false;

        int childCount = frameLayout.getChildCount();

        for (int i = 0; i < childCount; i++) {

            View childView = frameLayout.getChildAt(i);
            if (childView instanceof LinearLayout) {

                LinearLayout linearLayout = (LinearLayout) childView;
                Bar bar = (Bar) linearLayout.getChildAt(0);

                LayerDrawable layerDrawable = (LayerDrawable) bar.getProgressDrawable();
                layerDrawable.mutate();

                LinearLayout.LayoutParams progressParams = new LinearLayout.LayoutParams(
                        mBarWidth,
                        mBarHeight
                );
                progressParams.gravity = Gravity.CENTER;
                bar.setLayoutParams(progressParams);
                linearLayout.updateViewLayout(bar, progressParams);
            }
        }
    }


    public ArrayList<BarData> getData() {
        return mDataList;
    }

    public void removeBarValues() {
        if (this.getChildAt(0) != null) {
            if (oldFrameLayout != null)
                removeClickedBar();

            final int barsCount = ((LinearLayout) this.getChildAt(0)).getChildCount();

            for (int i = 0; i < barsCount; i++) {

                FrameLayout rootFrame = (FrameLayout) ((LinearLayout) this.getChildAt(0)).getChildAt(i);
                int rootChildCount = rootFrame.getChildCount();

                for (int j = 0; j < rootChildCount; j++) {

                    View childView = rootFrame.getChildAt(j);

                    if (childView instanceof LinearLayout) {
                        //bar
                        LinearLayout barContainerLinear = ((LinearLayout) childView);
                        int barContainerCount = barContainerLinear.getChildCount();

                        for (int k = 0; k < barContainerCount; k++) {

                            View view = barContainerLinear.getChildAt(k);

                            if (view instanceof Bar) {
                                BarAnimation anim = new BarAnimation(((Bar) view), (int) (mDataList.get(i).getBarValue() * 100), 0);
                                anim.setDuration(250);
                                ((Bar) view).startAnimation(anim);
                            }
                        }
                    }
                }


            }
            isBarsEmpty = true;
        }
    }

    public void removeClickedBar() {

        clickBarOff(oldFrameLayout);

    }

    public void resetBarValues() {

        if (oldFrameLayout != null)
            removeClickedBar();

        final int barsCount = ((LinearLayout) this.getChildAt(0)).getChildCount();

        for (int i = 0; i < barsCount; i++) {

            FrameLayout rootFrame = (FrameLayout) ((LinearLayout) this.getChildAt(0)).getChildAt(i);
            int rootChildCount = rootFrame.getChildCount();

            for (int j = 0; j < rootChildCount; j++) {

                View childView = rootFrame.getChildAt(j);

                if (childView instanceof LinearLayout) {
                    //bar
                    LinearLayout barContainerLinear = ((LinearLayout) childView);
                    int barContainerCount = barContainerLinear.getChildCount();

                    for (int k = 0; k < barContainerCount; k++) {

                        View view = barContainerLinear.getChildAt(k);

                        if (view instanceof Bar) {
                            BarAnimation anim = new BarAnimation(((Bar) view), 0, (int) (mDataList.get(i).getBarValue() * 100));
                            anim.setDuration(250);
                            ((Bar) view).startAnimation(anim);
                        }
                    }
                }


            }
        }
        isBarsEmpty = false;
    }

    public void disableBar(int index) {

        final int barsCount = ((LinearLayout) this.getChildAt(0)).getChildCount();

        for (int i = 0; i < barsCount; i++) {

            FrameLayout rootFrame = (FrameLayout) ((LinearLayout) this.getChildAt(0)).getChildAt(i);

            int rootChildCount = rootFrame.getChildCount();

            for (int j = 0; j < rootChildCount; j++) {

                if ((int) rootFrame.getTag() != index)
                    continue;

                rootFrame.setEnabled(false);
                rootFrame.setClickable(false);

                View childView = rootFrame.getChildAt(j);
                if (childView instanceof LinearLayout) {
                    //bar
                    LinearLayout barContainerLinear = ((LinearLayout) childView);
                    int barContainerCount = barContainerLinear.getChildCount();

                    for (int k = 0; k < barContainerCount; k++) {

                        View view = barContainerLinear.getChildAt(k);

                        if (view instanceof Bar) {

                            Bar bar = (Bar) view;

                            LayerDrawable layerDrawable = (LayerDrawable) bar.getProgressDrawable();
                            layerDrawable.mutate();

                            ScaleDrawable scaleDrawable = (ScaleDrawable) layerDrawable.getDrawable(1);

                            GradientDrawable progressLayer = (GradientDrawable) scaleDrawable.getDrawable();

                            if (progressLayer != null) {

                                if (mProgressDisableColor > 0)
                                    progressLayer.setColor(ContextCompat.getColor(mContext, mProgressDisableColor));
                                else
                                    progressLayer.setColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                            }
                        } else {
                            TextView titleTxtView = (TextView) view;
                            if (mProgressDisableColor > 0)
                                titleTxtView.setTextColor(ContextCompat.getColor(mContext, mProgressDisableColor));
                            else
                                titleTxtView.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                        }
                    }
                }
            }
        }
    }


    public void enableBar(int index) {

        final int barsCount = ((LinearLayout) this.getChildAt(0)).getChildCount();

        for (int i = 0; i < barsCount; i++) {

            FrameLayout rootFrame = (FrameLayout) ((LinearLayout) this.getChildAt(0)).getChildAt(i);

            int rootChildCount = rootFrame.getChildCount();

            for (int j = 0; j < rootChildCount; j++) {

                if ((int) rootFrame.getTag() != index)
                    continue;

                rootFrame.setEnabled(true);
                rootFrame.setClickable(true);

                View childView = rootFrame.getChildAt(j);
                if (childView instanceof LinearLayout) {
                    //bar
                    LinearLayout barContainerLinear = ((LinearLayout) childView);
                    int barContainerCount = barContainerLinear.getChildCount();

                    for (int k = 0; k < barContainerCount; k++) {

                        View view = barContainerLinear.getChildAt(k);

                        if (view instanceof Bar) {

                            Bar bar = (Bar) view;

                            LayerDrawable layerDrawable = (LayerDrawable) bar.getProgressDrawable();
                            layerDrawable.mutate();

                            ScaleDrawable scaleDrawable = (ScaleDrawable) layerDrawable.getDrawable(1);

                            GradientDrawable progressLayer = (GradientDrawable) scaleDrawable.getDrawable();

                            if (progressLayer != null) {

                                if (mProgressColor > 0)
                                    progressLayer.setColor(ContextCompat.getColor(mContext, mProgressColor));
                                else
                                    progressLayer.setColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                            }
                        } else {
                            TextView titleTxtView = (TextView) view;
                            if (mProgressDisableColor > 0)
                                titleTxtView.setTextColor(ContextCompat.getColor(mContext, mBarTitleColor));
                            else
                                titleTxtView.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray));
                        }
                    }
                }
            }
        }
    }

    public void selectBar(int index) {

        final int barsCount = ((LinearLayout) this.getChildAt(0)).getChildCount();

        for (int i = 0; i < barsCount; i++) {

            FrameLayout rootFrame = (FrameLayout) ((LinearLayout) this.getChildAt(0)).getChildAt(i);

            int rootChildCount = rootFrame.getChildCount();

            for (int j = 0; j < rootChildCount; j++) {

                if ((int) rootFrame.getTag() != index)
                    continue;

                if (oldFrameLayout != null)
                    clickBarOff(oldFrameLayout);

                clickBarOn(rootFrame);
                oldFrameLayout = rootFrame;
            }
        }
    }

    public void deselectBar(int index) {
        final int barsCount = ((LinearLayout) this.getChildAt(0)).getChildCount();

        for (int i = 0; i < barsCount; i++) {

            FrameLayout rootFrame = (FrameLayout) ((LinearLayout) this.getChildAt(0)).getChildAt(i);

            int rootChildCount = rootFrame.getChildCount();

            for (int j = 0; j < rootChildCount; j++) {

                if ((int) rootFrame.getTag() != index)
                    continue;

                clickBarOff(rootFrame);
            }
        }
    }
}
