package fit.lerchek.ui.widget

import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

class InputWidgetViewModel : BaseViewModel() {
    var obsString: ObservableField<String> = ObservableField()
}