package fit.lerchek.ui.widget.linechart;

public interface OnBarClickedListener {
    void onBarClicked(int index);
}
