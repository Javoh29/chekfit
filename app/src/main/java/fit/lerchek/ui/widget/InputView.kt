package fit.lerchek.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.ObservableField
import fit.lerchek.databinding.ViewInputBinding

internal class InputView : ConstraintLayout {

    private lateinit var binding: ViewInputBinding
    private lateinit var model: InputWidgetViewModel

    constructor(context: Context) : super(context) {
        initLayout()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initLayout()
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initLayout()
    }

    private fun initLayout() {
        binding = ViewInputBinding.inflate(LayoutInflater.from(context))
        addView(binding.root)
        model = InputWidgetViewModel()
        binding.viewModel = model

        binding.inputEmail.doAfterTextChanged {
            Log.d("!!" , it.toString())
        }

    }
}


