package fit.lerchek.ui.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_SP
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import fit.lerchek.R

class ClickableTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    init {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER_VERTICAL
        setPadding(
            context.resources.getDimensionPixelSize(R.dimen.size_16dp),
            context.resources.getDimensionPixelSize(R.dimen.size_24dp),
            context.resources.getDimensionPixelSize(R.dimen.size_16dp),
            context.resources.getDimensionPixelSize(R.dimen.size_24dp)
        )
        setBackgroundResource(
            TypedValue().apply {
                context.theme.resolveAttribute(
                    android.R.attr.selectableItemBackground,
                    this, true
                )
            }.resourceId
        )
        context.obtainStyledAttributes(attrs, R.styleable.ClickableTextView).apply {
            addView(createIcon(getDrawable(R.styleable.ClickableTextView_icon)))
            addView(createLabel(getString(R.styleable.ClickableTextView_label)))
            addView(createChevron())
        }.recycle()
    }

    private fun createIcon(drawable: Drawable?): AppCompatImageView {
        return AppCompatImageView(context).apply {
            layoutParams = LayoutParams(
                context.resources.getDimensionPixelSize(R.dimen.size_24dp),
                context.resources.getDimensionPixelSize(R.dimen.size_24dp)
            ).apply { marginEnd = context.resources.getDimensionPixelSize(R.dimen.size_16dp) }
            setImageDrawable(drawable ?: ContextCompat.getDrawable(context, R.drawable.ic_fag))
        }
    }

    private fun createLabel(label: String?): AppCompatTextView {
        return AppCompatTextView(context).apply {
            layoutParams = LayoutParams(
                context.resources.getDimensionPixelSize(R.dimen.size_0dp),
                WRAP_CONTENT
            ).apply { weight = 1f }
            setTextSize(COMPLEX_UNIT_SP, 16f)
            setTextColor(ContextCompat.getColor(context, R.color.text_primary))
            typeface = ResourcesCompat.getFont(context, R.font.gilroy_semi_bold)
            text = label
        }
    }

    private fun createChevron(): AppCompatImageView {
        return AppCompatImageView(context).apply {
            layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
            setImageResource(R.drawable.ic_chevron)
        }
    }
}