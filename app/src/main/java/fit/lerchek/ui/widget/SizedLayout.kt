package fit.lerchek.ui.widget

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout


class SizedLayout : ConstraintLayout {

    companion object {
        const val COUNT_IN_LIST = 2
    }

    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {

    }


    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val layoutParamsView = layoutParams

        val width = Resources.getSystem().displayMetrics.widthPixels / COUNT_IN_LIST
        layoutParamsView.width = width
        layoutParams = layoutParamsView
    }
}
