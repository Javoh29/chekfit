package fit.lerchek.ui.widget.linechart;

public class BarData {

    private String barTitle;
    private int barValue;
    private String pinText = "";
    private String date = "";
    private String dateFormat = "";

    public String getPinText() {
        return pinText;
    }

    public void setPinText(String pinText) {
        this.pinText = pinText;
    }

    public String getBarTitle() {
        return barTitle;
    }

    public void setBarTitle(String barTitle) {
        this.barTitle = barTitle;
    }

    public int getBarValue() {
        return barValue;
    }

    public void setBarValue(int barValue) {
        this.barValue = barValue;
    }

    public void setDate(String date) { this.date = date; }

    public String getDate() { return date; }

    public void setDateFormat(String dateFormat) { this.date = dateFormat; }

    public String getDateFormat() { return dateFormat; }

    public BarData(String barTitle, int barValue, String date, String dateFormat) {
        this.barTitle = barTitle;
        this.barValue = barValue;
        this.date = date;
        this.dateFormat = dateFormat;
    }

    public BarData() {
    }
}
