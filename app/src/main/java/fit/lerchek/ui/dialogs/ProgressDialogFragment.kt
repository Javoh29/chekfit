package fit.lerchek.ui.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import fit.lerchek.R
import fit.lerchek.ui.base.BaseStandardFragmentDialog

class ProgressDialogFragment : BaseStandardFragmentDialog() {

    private var mText: String? = null

    companion object {
        private const val TEXT_TAG = "title_tag"

        fun newInstance(
            text: String?
        ): ProgressDialogFragment {

            val fragment = ProgressDialogFragment()
            text?.let {
                val args = Bundle()
                args.putString(TEXT_TAG, it)
                fragment.arguments = args
            }

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mText = if (arguments?.containsKey(TEXT_TAG)==true) {
            requireArguments().getString(TEXT_TAG)
        } else mContext.getString(R.string.progress_please_wait)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(mContext)
        val dialogView = View.inflate(context, R.layout.layout_progress, null)
        dialogView.findViewById<AppCompatTextView>(R.id.title)?.let {
            it.text = mText
        }
        builder.setView(dialogView)
        return builder.create().apply { show() }
    }
}
