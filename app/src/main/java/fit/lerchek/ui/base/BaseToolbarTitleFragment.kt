package fit.lerchek.ui.base

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.annotation.LayoutRes
import fit.lerchek.ui.feature.marathon.MarathonActivity

abstract class BaseToolbarTitleFragment(@LayoutRes layoutResId : Int)  : BaseToolbarDependentFragment(layoutResId) {

    abstract fun getToolbarTitle() : String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler(Looper.getMainLooper()).post {
            (mContext as MarathonActivity).apply {
                updateTitle(getToolbarTitle())
            }
        }
    }

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Title,
        ToolbarContentItem.Close
    )

}