package fit.lerchek.ui.base

enum class ToolbarContentItem {
    Logo,
    Dropdown,
    Title,
    Profile,
    Menu,
    Close
}