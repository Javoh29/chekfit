package fit.lerchek.ui.base

import androidx.annotation.IdRes
import androidx.navigation.findNavController

abstract class BaseNavigationActivity(@IdRes private val navHostId : Int) : BaseActivity(){

    protected val navController by lazy { findNavController(navHostId) }

    fun goBack(steps: Int) {
        repeat(steps) {
            try {
                navController.navigateUp()
            } catch (t:Throwable){
                t.printStackTrace()
            }
        }
    }
}