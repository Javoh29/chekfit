package fit.lerchek.ui.base

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.annotation.LayoutRes
import fit.lerchek.ui.feature.marathon.MarathonActivity
import fit.lerchek.ui.util.ToastUtils

abstract class BaseToolbarDependentFragment(@LayoutRes layoutResId: Int) :
    BaseFragment(layoutResId) {

    companion object {
        private const val TAG_SCROLL_POSITION = "tag_scroll_position"
    }

    abstract fun getToolbarContent(): Set<ToolbarContentItem>

    protected open var isBottomNavigationVisible: Boolean = false

    protected open var rootScrollingView: View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler(Looper.getMainLooper()).post {
            (mContext as MarathonActivity).let {
                it.updateToolbar(getToolbarContent())
                it.showHideBottomNavigation(isBottomNavigationVisible)
            }
        }
    }

    protected fun updateBottomNavigation() {
        Handler(Looper.getMainLooper()).post {
            (mContext as MarathonActivity).showHideBottomNavigation(
                isBottomNavigationVisible
            )
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(
            outState.apply {
                rootScrollingView?.let {
                    putInt(TAG_SCROLL_POSITION, it.scrollY)
                }
            }
        )
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.let { bundle ->
            bundle.getInt(TAG_SCROLL_POSITION).let { position ->
                rootScrollingView?.let {
                    it.post { it.scrollTo(0, position) }
                }
            }
        }
    }
}