package fit.lerchek.ui.base

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import fit.lerchek.common.rx.CustomCompletableObserver
import fit.lerchek.common.rx.CustomSingleObserver
import fit.lerchek.common.rx.OnErrorListener
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver

abstract class BaseViewModel : ViewModel() {

    private val disposables = CompositeDisposable()

    var isProgress: ObservableBoolean = ObservableBoolean()

    var onErrorListener: OnErrorListener? = null

    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    protected fun processTask(disposable: Disposable) {
        disposables.add(disposable)
    }

    fun <T> simpleSingleObserver(onSuccess: (result: T) -> Unit): DisposableSingleObserver<T> {
        return object : CustomSingleObserver<T>(isProgress, onErrorListener) {
            override fun onSuccess(result: T) {
                onSuccess(result)
            }
        }
    }

    fun simpleCompletableObserver(onComplete: () -> Unit): DisposableCompletableObserver {
        return object : CustomCompletableObserver(isProgress, onErrorListener) {
            override fun onComplete() {
                onComplete()
            }
        }
    }
}