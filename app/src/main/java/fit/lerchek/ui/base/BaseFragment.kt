package fit.lerchek.ui.base

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import fit.lerchek.common.rx.OnErrorListener
import fit.lerchek.ui.util.ToastUtils

abstract class BaseFragment(@LayoutRes layoutResId: Int) : Fragment(layoutResId), OnErrorListener {

    private val navController by lazy { findNavController() }

    protected lateinit var mContext: BaseActivity


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context as BaseActivity
        Log.d("TAG", javaClass.name)
    }

    fun navigateTo(@IdRes resId: Int, popBackStack: Boolean = false) {
        try {
            if (popBackStack) {
                navController.popBackStack(resId, true)
            }
            navController.navigate(resId)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    fun navigateTo(directions: NavDirections, popBackStack: Boolean = false) {
        try {
            if (popBackStack) {
                navController.popBackStack(directions.actionId, true)
            }
            navController.navigate(directions)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    fun navigateTo(@IdRes resId: Int, bundle: Bundle, popBackStack: Boolean = false) {
        try {
            if (popBackStack) {
                navController.popBackStack(resId, true)
            }
            navController.navigate(resId, bundle)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    fun navigateUp() {
        try {
            navController.navigateUp()
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    override fun onError(code: String, message: String, goBack: Boolean) {
        ToastUtils.show(mContext, message)
        if (goBack) {
            Handler(Looper.getMainLooper()).post {
                navigateUp()
            }
        }
    }

}