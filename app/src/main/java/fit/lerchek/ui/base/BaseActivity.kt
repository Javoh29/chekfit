package fit.lerchek.ui.base

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import fit.lerchek.ui.dialogs.ProgressDialogFragment

abstract class BaseActivity : AppCompatActivity(){

    private var mDialogFragment: DialogFragment? = null
    private var needFragmentDialogShowOnResume = false
    private var resumed = false

    fun showSimpleOptionsDialog(options: Array<String>, callback: (index: Int) ->Unit){
        AlertDialog.Builder(this).apply {
            setItems(options) { _, which ->
                callback(which)
            }
            create().apply {
                show()
            }
        }
    }

    fun showProgressDialog(text: String? = null) {
        if (!isFinishing) {
            closeExistingAlert()
            mDialogFragment = ProgressDialogFragment.newInstance(text)
            mDialogFragment!!.isCancelable = false
            showDialog()
        }
    }

    private fun showDialog() {
        if (!isFinishing) {
            if (isVisible()) {
                mDialogFragment?.let {
                    if (!it.isAdded) {
                        try {
                            it.showNow(supportFragmentManager, javaClass.simpleName)
                        } catch (t: Throwable) {
                            t.printStackTrace()
                        }
                    }
                }
            } else {
                needFragmentDialogShowOnResume = true
            }
        }
    }

    fun closeExistingAlert() {
        if (mDialogFragment != null && mDialogFragment!!.isAdded) {
            try {
                mDialogFragment!!.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        mDialogFragment = null
    }

    protected fun isVisible(): Boolean {
        return resumed
    }

    override fun onResume() {
        super.onResume()
        resumed = true
        if (needFragmentDialogShowOnResume) {
            if (mDialogFragment != null && !mDialogFragment!!.isAdded) {
                try {
                    showDialog()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            }
            needFragmentDialogShowOnResume = false
        }
    }

    fun showToast(text:String){
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()

        resumed = false
    }

    open fun getActivityResultLauncher(requestCode: Int): ActivityResultLauncher<Intent>?{
        return null
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val view = currentFocus
        var ret = false
        try {
            ret = super.dispatchTouchEvent(event)
        } catch (e: Exception) {
            return ret
        }

        if(view?.id == unclosedIdForKeyboard()){
            return ret
        }

        if (view is EditText) {
            val w = currentFocus
            val scrCoordinates = IntArray(2)
            w!!.getLocationOnScreen(scrCoordinates)
            val x = event.rawX + w.left - scrCoordinates[0]
            val y = event.rawY + w.top - scrCoordinates[1]

            if (event.action == MotionEvent.ACTION_UP && (x < w.left || x >= w.right || y < w.top || y > w.bottom)) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(window.currentFocus!!.windowToken, 0)
            }
        }

        return ret
    }

    open fun unclosedIdForKeyboard(): Int {
        return -1
    }

    override fun attachBaseContext(newBase: Context) {
        val override = Configuration(newBase.resources.configuration)
        override.fontScale = 1.0f
        applyOverrideConfiguration(override)
        super.attachBaseContext(newBase)
    }
}