package fit.lerchek.ui.base

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment

abstract class BaseStandardFragmentDialog : DialogFragment() {

    protected lateinit var mContext: AppCompatActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = activity as AppCompatActivity
    }

    override fun onDestroyView() {
        dialog?.setDismissMessage(null)
        super.onDestroyView()
    }

    protected open fun dismissDialog() {
        if (isAdded) {
            try {
                dismiss()
            } catch (t: Throwable) {
                t.printStackTrace()
            }
        }
    }
}