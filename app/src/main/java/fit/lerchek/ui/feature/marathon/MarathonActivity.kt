package fit.lerchek.ui.feature.marathon

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.navigation.ui.setupWithNavController
import cn.jzvd.Jzvd
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.MenuItemUIModel
import fit.lerchek.data.domain.uimodel.marathon.MenuItemType
import fit.lerchek.data.fcm.FMService
import fit.lerchek.databinding.ActivityMarathonBinding
import fit.lerchek.ui.base.BaseNavigationActivity
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.chat.ChatActivity
import fit.lerchek.ui.feature.login.LoginActivity
import fit.lerchek.ui.feature.marathon.menu.MenuItemSelectListener
import fit.lerchek.ui.feature.marathon.progress.crop.UploadPhotoListener
import fit.lerchek.ui.feature.marathon.today.TodayFragmentArgs
import fit.lerchek.ui.feature.marathon.workout.WorkoutFragmentArgs
import fit.lerchek.ui.util.BrowserUtils
import fit.lerchek.ui.util.viewBinding
import fit.lerchek.util.EventObserver

@AndroidEntryPoint
class MarathonActivity : BaseNavigationActivity(R.id.nav_host_fragment_marathon),
    MenuItemSelectListener {

    private val binding by viewBinding(ActivityMarathonBinding::inflate)
    private val viewModel by viewModels<MarathonActivityViewModel>()

    companion object {
        const val REQUEST_CODE_UPLOAD_PROGRESS_PHOTO = 102
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setUpNavigation()
        initListeners()
        binding.viewModel = viewModel
        viewModel.successLogout.observe(this, EventObserver { success ->
            closeExistingAlert()
            if (success) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        })
        intent?.extras?.let {
            processDeepLink(it)
        }
        appUpdate()
    }

    private fun appUpdate() {
        val appUpdateManager = AppUpdateManagerFactory.create(this)
        appUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    this,
                    100
                )
            }

            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                Snackbar.make(
                    window.decorView.rootView,
                    "Только что было загружено обновление из Google Play",
                    Snackbar.LENGTH_INDEFINITE
                ).apply {
                    setAction("ОБНОВИТЬ") { appUpdateManager.completeUpdate() }
                    show()
                }
            }
        }.addOnFailureListener { e ->
            Log.e("BUG", e.message!!)
        }
    }

    private fun processDeepLink(extras: Bundle) {
        extras.getString(FMService.KEY_TYPE)?.let { deepLinkType ->
            extras.getString(FMService.KEY_MARATHON_ID, null)?.toIntOrNull()?.let { marathonId ->
                onChooseMarathon()
                navController.navigate(
                    R.id.navigation_today,
                    TodayFragmentArgs.Builder()
                        .setMarathonId(marathonId)
                        .setDeepLinkType(deepLinkType)
                        .build().toBundle()
                )
                extras.remove(FMService.KEY_TYPE)
                extras.remove(FMService.KEY_MARATHON_ID)
            }
        }
    }

    private fun initListeners() {
        binding.btnSelectMarathon.setOnClickListener {
            onChooseMarathon()
        }
        binding.btnProfile.setOnClickListener {
            onProfile()
        }
        binding.btnMenu.setOnClickListener {
            navController.navigate(R.id.navigation_menu)
        }
        binding.btnClose.setOnClickListener {
            navController.navigateUp()
        }
    }

    private fun setUpNavigation() {
        binding.navView.setupWithNavController(navController)
    }

    fun updateToolbar(toolbarContent: Set<ToolbarContentItem>) {
        binding.ivLogo.isVisible = toolbarContent.contains(ToolbarContentItem.Logo)
        binding.btnSelectMarathon.isVisible = toolbarContent.contains(ToolbarContentItem.Dropdown)
        binding.tvTitle.isVisible = toolbarContent.contains(ToolbarContentItem.Title)
        binding.btnProfile.visibility =
            if (toolbarContent.contains(ToolbarContentItem.Profile)) View.VISIBLE else View.INVISIBLE
        binding.btnMenu.isVisible = toolbarContent.contains(ToolbarContentItem.Menu)
        binding.btnClose.isVisible = toolbarContent.contains(ToolbarContentItem.Close)
    }

    fun showHideBottomNavigation(show: Boolean) {
        binding.navView.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun updateTitle(text: String) {
        binding.tvTitle.text = text
    }

    override fun onMenuItemSelect(item: MenuItemUIModel) {
        if (item.type == MenuItemType.Logout) {
            showProgressDialog()
            viewModel.logout()
        } else {
            navController.navigateUp()
            when (item.type) {
                MenuItemType.MarathonSelect -> {
                    onChooseMarathon()
                }
                MenuItemType.ReferralProgram -> {
                    navController.navigate(R.id.navigation_referral)
                }
                MenuItemType.AskQuestion -> {
                    startActivity(Intent(this, ChatActivity::class.java))
                }
                MenuItemType.TelegramChannel -> {
                    item.link?.let {
                        BrowserUtils.openBrowser(this, it)
                    }
                }
                MenuItemType.Instruction -> {
                    navController.navigate(
                        R.id.navigation_dynamic_content_page,
                        DynamicContentFragmentArgs.Builder()
                            .setAlias(DynamicContentFragment.TAG_ALIAS_INSTRUCTION)
                            .build().toBundle()
                    )
                }
                MenuItemType.MarathonReports -> {
                    navController.navigate(R.id.navigation_reports)
                }
                MenuItemType.ActivityCalendar -> {
                    navController.navigate(R.id.navigation_activity_calendar)
                }
                MenuItemType.TelegramGroup -> {
                    item.link?.let {
                        BrowserUtils.openBrowser(this, it)
                    }
                }
                MenuItemType.Profile -> {
                    onProfile()
                }
                MenuItemType.ExtraWorkout -> {
                    navController.navigate(
                        R.id.navigation_today_workout,
                        WorkoutFragmentArgs.Builder()
                            .setMarathonId(item.extraMarathonId ?: -1)
                            .build().toBundle()
                    )
                }
                else -> {
                }
            }
        }
    }

    private fun onChooseMarathon() {
        viewModel.resetAppMenu()
        navController.popBackStack(R.id.navigation_marathon_list, true)
        navController.navigate(R.id.navigation_marathon_list)
    }

    private fun onProfile() {
        navController.popBackStack(R.id.navigation_profile, true)
        navController.navigate(R.id.navigation_profile)
    }

    private val uploadProgressPhotoActivityResultListener =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                supportFragmentManager.primaryNavigationFragment?.childFragmentManager?.fragments?.firstOrNull()
                    ?.let {
                        if (it is UploadPhotoListener) {
                            it.onPhotoUploaded()
                        }
                    }
            }
        }

    override fun getActivityResultLauncher(requestCode: Int): ActivityResultLauncher<Intent>? {
        return when (requestCode) {
            REQUEST_CODE_UPLOAD_PROGRESS_PHOTO -> uploadProgressPhotoActivityResultListener
            else -> null
        }
    }

    override fun onBackPressed() {
        if (Jzvd.backPress()) {
            return
        }
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        Jzvd.releaseAllVideos()
    }

}