package fit.lerchek.ui.feature.login

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.ActivityAuthBinding
import fit.lerchek.ui.base.BaseActivity
import fit.lerchek.ui.base.BaseNavigationActivity
import fit.lerchek.ui.feature.login.splash.SplashViewModel
import fit.lerchek.ui.feature.marathon.MarathonActivity
import fit.lerchek.ui.util.viewBinding

@AndroidEntryPoint
class LoginActivity : BaseNavigationActivity(R.id.nav_host_fragment_auth) {

    private val viewModel by viewModels<SplashViewModel>()

    private val binding by viewBinding (ActivityAuthBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (viewModel.shouldShowAuthFlow()){
            setContentView(binding.root)
        } else {
            startActivity(Intent(this, MarathonActivity::class.java).apply {
                intent.extras?.let {
                    putExtras(it)
                }
            })
            finish()
        }
    }
}