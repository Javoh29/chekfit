package fit.lerchek.ui.feature.marathon.today.callbacks

import fit.lerchek.data.api.message.marathon.UpdateTrackerGoalRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest

interface TrackerSettingCallback {
    fun changeCountTracker(request: UpdateTrackerRequest, type: String)

    fun changeGoalTracker(request: UpdateTrackerGoalRequest, type: String)
}