package fit.lerchek.ui.feature.marathon.more.calories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.databinding.ItemCalorieItemBinding
import fit.lerchek.databinding.ItemTestFooterBinding
import fit.lerchek.databinding.ItemTestHeaderBinding
import fit.lerchek.databinding.ItemTestItemBinding


class CalorieAdapter(
    private var data: List<CalorieItem>,
    private var onChange: (CalorieItem) -> Unit?
) : RecyclerView.Adapter<CalorieAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_ITEM = 1
    }

    fun update(content: List<CalorieItem>) {
        data = content
        notifyItemRangeChanged(0, data.size)
    }

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class ItemViewHolder(val itemBinding: ItemCalorieItemBinding) :
        ViewHolder(itemBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_ITEM -> ItemViewHolder(
                ItemCalorieItemBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            else -> ITEM_VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is ItemViewHolder -> {
                (data[position] as CalorieItemUIModel).let { item ->
                    holder.itemBinding.apply {
                        model = item
                        rvTestOptions.adapter =
                            CalorieOptionsAdapter(item.options ?: arrayListOf()) {
                                onChange(item)
                            }
                    }
                }
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}