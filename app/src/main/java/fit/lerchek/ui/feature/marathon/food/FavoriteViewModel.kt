package fit.lerchek.ui.feature.marathon.food

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.api.model.Meal
import fit.lerchek.data.domain.uimodel.marathon.FavoriteUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {
    var favoriteItem: ObservableArrayList<RecipeUIModel> = ObservableArrayList()
    var listRecipe: ArrayList<RecipeUIModel> = arrayListOf()
    var displayItems = MutableLiveData<Meal>()
    val errorItemPosition = MutableLiveData<Int>()
    val isEmpty = MutableLiveData<Boolean>()
    val mealList = ArrayList<Meal>()

    init {
        isEmpty.value = false
    }

    fun loadFoodDetails() {
        processTask(
            userRepository.loadFavorites().subscribeWith(
                simpleSingleObserver<FavoriteUIModel> {
                    if (it.recipes.isNotEmpty()) {
                        mealList.clear()
                        listRecipe.clearAndAddAll(it.recipes)
                        it.meals.forEach { (_, m) ->  mealList.add(m)}
                        displayItems.value = mealList.first()
                        setMealFilter()
                    } else {
                        isProgress.set(false)
                        isEmpty.value = true
                    }
                }
            )
        )
    }

    fun setMealFilter() {
        processTask(
            userRepository.mealFilter(displayItems.value!!.id, listRecipe).subscribeWith(
                simpleSingleObserver<List<RecipeUIModel>> {
                    favoriteItem.clearAndAddAll(it)
                    isProgress.set(false)
                }
            )
        )
    }

    fun favoriteChange(isDelete: Boolean, recipeId: Int, position: Int) {
        processTask(
            userRepository.toggleFavorite(isDelete, recipeId)
                .subscribeWith(object : DisposableSingleObserver<Boolean>() {
                    override fun onSuccess(t: Boolean) {
                        FoodFragment.onUpdate.value = true
                        listRecipe.remove(favoriteItem[position])
                        favoriteItem.removeAt(position)
                        if (favoriteItem.isEmpty()) {
                            loadFoodDetails()
                        }
                    }

                    override fun onError(e: Throwable) {
                        Log.d("BUG", "Error: $e")
                        errorItemPosition.value = position
                    }

                })
        )
    }
}