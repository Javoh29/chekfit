package fit.lerchek.ui.feature.marathon.activity

import fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarItem

interface ActivityCalendarCallback {
    fun onDaySelected(day: ActivityCalendarItem.Day)
}