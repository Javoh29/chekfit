package fit.lerchek.ui.feature.marathon.more

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.data.domain.uimodel.marathon.ExtraRecipesFilterUIModel
import fit.lerchek.data.domain.uimodel.marathon.MealUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.ItemExtraRecipeFilterBinding
import fit.lerchek.databinding.ItemFoodRecipeBinding

class ExtraRecipesAdapter(
    private var data: List<ExtraRecipeItem>,
    private var callback: ExtraRecipesCallback?
) : RecyclerView.Adapter<ExtraRecipesAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_FILTER = 0
        private const val ITEM_VIEW_TYPE_RECIPE = 1
    }

    interface ExtraRecipesCallback {
        fun onSelectRecipe(recipeUIModel: RecipeUIModel)
        fun onSelectMeals(meals: List<MealUIModel>)
        fun onBack()
    }

    interface ExtraRecipeItem

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class FilterViewHolder(val itemFilterBinding: ItemExtraRecipeFilterBinding) :
        ViewHolder(itemFilterBinding)

    class RecipeItemViewHolder(val itemRecipeBinding: ItemFoodRecipeBinding) :
        ViewHolder(itemRecipeBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_FILTER -> FilterViewHolder(
                ItemExtraRecipeFilterBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_RECIPE -> RecipeItemViewHolder(
                ItemFoodRecipeBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is RecipeUIModel) ITEM_VIEW_TYPE_RECIPE else ITEM_VIEW_TYPE_FILTER
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is FilterViewHolder -> {
                holder.itemFilterBinding.apply {
                    val filter = data[position] as ExtraRecipesFilterUIModel
                    this.model = filter
                    imgMeal.let {
                        Glide.with(it.context).load(filter.getSelectedMeal()?.imageUrl).into(it)
                    }
                    btnChooseMeal.setOnClickListener {
                        callback?.onSelectMeals(filter.meals)
                    }
                    btnBack.root.setOnClickListener {
                        callback?.onBack()
                    }
                }
            }
            is RecipeItemViewHolder -> {
                val recipe = (data[position] as RecipeUIModel).apply { hasReplacements = false }
                holder.itemRecipeBinding.apply {
                    model = recipe
                    Glide.with(imgRecipe.context).load(recipe.imageUrl).into(imgRecipe)
                    btnFavorite.visibility = View.GONE
                }
                holder.itemView.setOnClickListener {
                    callback?.onSelectRecipe(recipe)
                }
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}

@BindingAdapter("extra_recipes", "extra_recipes_callback")
fun setupExtraRecipesAdapter(
    list: RecyclerView,
    items: List<ExtraRecipesAdapter.ExtraRecipeItem>?,
    callback: ExtraRecipesAdapter.ExtraRecipesCallback?
) {
    items?.let {
        list.adapter = ExtraRecipesAdapter(it, callback)
    }
}