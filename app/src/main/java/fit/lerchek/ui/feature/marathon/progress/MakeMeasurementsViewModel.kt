package fit.lerchek.ui.feature.marathon.progress

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.R
import fit.lerchek.common.util.Constants
import fit.lerchek.data.api.message.marathon.TaskToggleResponse
import fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class MakeMeasurementsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val progress: ObservableField<MarathonProgressUIModel> = ObservableField()
    val checkFrontPhoto: ObservableBoolean = ObservableBoolean(true)
    val checkBackPhoto: ObservableBoolean = ObservableBoolean(false)
    val checkSidePhoto: ObservableBoolean = ObservableBoolean(false)
    val selectedWeek: ObservableInt = ObservableInt()
    val uploadPhotoText: ObservableInt = ObservableInt(R.string.progress_upload_photo)
    val videoLink: ObservableField<String> = ObservableField()

    private var week: Int? = null

    fun loadMeasurements() {
        isProgress.set(true)
        userRepository.getProgressForMarathon()
            ?.subscribeWith(simpleSingleObserver<MarathonProgressUIModel> {
                this.progress.set(it)
                this.selectedWeek.set(week ?: it.current_week)
                val textRes =
                    if (it.emptyPhoto(it.current_week)) R.string.progress_upload_photo else R.string.progress_change_photo
                this.uploadPhotoText.set(textRes)
                isProgress.set(false)
            })?.let { processTask(it) }
    }

    fun selectWeek(week: Int) {
        this.selectedWeek.set(week)
        this.week = week
        this.videoLink.set(progress.get()?.videoUrl(week))
    }

    fun onSaveLink() {
        val url = videoLink.get() ?: return
        if (url.isEmpty()) return
        val week = selectedWeek.get()

        isProgress.set(true)

        processTask(userRepository.saveVideoLinkForMarathon(url = url, week = week).subscribeWith(
            object : DisposableSingleObserver<TaskToggleResponse>() {
                override fun onStart() {
                }

                override fun onSuccess(items: TaskToggleResponse) {
                    isProgress.set(false)
                }

                override fun onError(e: Throwable) {
                    isProgress.set(false)
                    onErrorListener?.onError(
                        message = e.message ?: Constants.UNKNOWN_ERROR,
                        goBack = false
                    )
                }
            }
        ))
    }

}