package fit.lerchek.ui.feature.marathon.progress

import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel
import fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class MeasurementsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val data: ObservableField<MeasurementsUIModel> = ObservableField()

    fun loadMeasurements(type: String) {
        data.set(
            MeasurementsUIModel(
                MeasurementsUIModel.Type.valueOf(type),
                MarathonProgressUIModel()
            )
        )
        isProgress.set(true)
        userRepository.getProgressForMarathon()
            ?.subscribeWith(simpleSingleObserver<MarathonProgressUIModel> {
                data.set(
                    MeasurementsUIModel(
                        MeasurementsUIModel.Type.valueOf(type),
                        it
                    )
                )
                isProgress.set(false)
            })?.let { processTask(it) }
    }

}