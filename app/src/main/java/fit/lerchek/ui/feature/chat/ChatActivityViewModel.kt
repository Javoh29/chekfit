package fit.lerchek.ui.feature.chat

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.chat.ChatItem
import fit.lerchek.data.domain.uimodel.chat.ChatUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class ChatActivityViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var profileIsMale = ObservableBoolean()

    val message = ObservableField<String>()

    private val mMessageList = MutableLiveData<List<ChatItem>>()

    val messageList: LiveData<List<ChatItem>>
        get() = mMessageList

    val canEdit = ObservableBoolean()

    private var userId = -1

    init {
        checkGender()
    }

    private fun checkGender() {
        processTask(userRepository.isCurrentProfileMale().subscribeWith(
            object : DisposableSingleObserver<Boolean>() {
                override fun onSuccess(isMale: Boolean) {
                    profileIsMale.set(isMale)
                }

                override fun onError(e: Throwable) {}
            }
        ))
    }

    fun getChat() {
        processTask(userRepository.getChat().subscribeWith(simpleSingleObserver<ChatUIModel> {
            userId = it.userId
            message.set(null)
            canEdit.set(it.canEdit)
            mMessageList.postValue(it.messages)
            isProgress.set(false)
        }))
    }

    fun sendMessage() {
        val text = message.get()
        if (text.isNullOrEmpty().not()) {
            processTask(userRepository.sendMessage(userId, text!!).subscribeWith(
                object : DisposableSingleObserver<List<ChatItem>>() {
                    override fun onStart() {
                        message.set(null)
                    }

                    override fun onSuccess(items: List<ChatItem>) {
                        mMessageList.postValue(items)
                    }

                    override fun onError(e: Throwable) {
                        onErrorListener?.onError(
                            message = e.message ?: Constants.UNKNOWN_ERROR,
                            goBack = false
                        )
                        message.set(text)
                    }
                }
            ))
        }
    }


}