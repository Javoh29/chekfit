package fit.lerchek.ui.feature.marathon.voting

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.VotingItemUIModel
import fit.lerchek.databinding.FragmentVotingBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class VotingFragment : BaseToolbarDependentFragment(R.layout.fragment_voting), VotingCallback {

    private val binding by viewBindings(FragmentVotingBinding::bind)
    private val viewModel by viewModels<VotingViewModel>()
    private var adapter: VotingAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadVoting()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        viewModel.votingItems.apply {
            removeObservers(viewLifecycleOwner)
            observe(viewLifecycleOwner, { items ->
                if (items.isNullOrEmpty().not()) {
                    if (adapter == null) {
                        adapter = VotingAdapter(this@VotingFragment)
                        binding.rvVoting.adapter = adapter
                    }
                    if (binding.rvVoting.adapter == null) {
                        binding.rvVoting.adapter = adapter
                    }
                    adapter!!.update(items)
                }
            })
        }
    }

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun vote(user: VotingItemUIModel) {
        viewModel.toggleLike(user)
    }
}