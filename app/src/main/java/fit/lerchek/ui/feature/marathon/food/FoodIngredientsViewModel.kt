package fit.lerchek.ui.feature.marathon.food

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.FoodIngredientsUIModel
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.data.domain.uimodel.marathon.WeekUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import javax.inject.Inject

@HiltViewModel
class FoodIngredientsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var ingredients: ObservableArrayList<TaskUIModel> = ObservableArrayList()

    var weeks: ObservableArrayList<WeekUIModel> = ObservableArrayList()

    var selectedWeek: ObservableField<WeekUIModel> = ObservableField()

    fun selectWeek(weekUIModel: WeekUIModel) {
        selectedWeek.set(weekUIModel)
        loadIngredients(weekUIModel.id)
    }

    fun loadIngredients(weekId: Int? = null) {
        processTask(
            userRepository.loadFoodIngredients(weekId)
                .subscribeWith(simpleSingleObserver<FoodIngredientsUIModel> { food ->
                    ingredients.clearAndAddAll(food.ingredients)
                    weeks.clearAndAddAll(food.weeks)
                    selectedWeek.set(food.weeks.firstOrNull { it.selected })
                    isProgress.set(false)
                })
        )
    }

    fun checkAllIngredients() {
        ingredients.any { !it.checked }.not().let { allChecked ->
            if (!allChecked) {
                val updated = ingredients.onEach {
                    if (!it.checked) {
                        it.checked = true
                    }
                }.map { it }
                ingredients.clearAndAddAll(updated)
                updateIngredients()
            }
        }
    }

    fun updateIngredientState(taskUIModel: TaskUIModel) {
        ingredients.find { it.id == taskUIModel.id }?.checked = taskUIModel.checked
        updateIngredient(taskUIModel)
    }

    private fun updateIngredients() {
        processTask(
            userRepository.updateIngredientsList(
                weekId = selectedWeek.get()?.id ?: -1,
                ingredients = ingredients
            ).subscribe()
        )
    }

    private fun updateIngredient(ingredient: TaskUIModel) {
        processTask(
            userRepository.toggleFoodIngredient(
                weekId = selectedWeek.get()?.id ?: -1,
                ingredient = ingredient
            ).subscribe()
        )
    }

}