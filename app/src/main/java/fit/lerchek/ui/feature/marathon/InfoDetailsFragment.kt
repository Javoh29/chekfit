package fit.lerchek.ui.feature.marathon

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.Constants.WEB_HOME
import fit.lerchek.databinding.FragmentInfoDetailsBinding
import fit.lerchek.ui.base.BaseToolbarTitleFragment
import fit.lerchek.ui.util.BrowserUtils
import fit.lerchek.ui.util.TextUtils.handleUrlClicks
import fit.lerchek.ui.util.setHtmlText
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class InfoDetailsFragment : BaseToolbarTitleFragment(R.layout.fragment_info_details) {

    private val binding by viewBindings(FragmentInfoDetailsBinding::bind)
    private val viewModel by viewModels<InfoDetailsViewModel>()
    private val args: InfoDetailsFragmentArgs by navArgs()

    override fun getToolbarTitle() = args.title

    companion object {
        private const val LINK_WORKOUT = "/lk/faq/${FaqFragment.TAG_ALIAS_WORKOUT}/"
        private const val LINK_FOOD = "/lk/faq/${FaqFragment.TAG_ALIAS_FOOD}/"
        private const val LINK_PROGRESS = "/lk/faq/${FaqFragment.TAG_ALIAS_PROGRESS}/"
        private const val LINK_MAP = "/lk/marathon/page/${DynamicContentFragment.TAG_ALIAS_MAP}/"
        private const val LINK_CHECK_LIST =
            "/lk/marathon/page/${DynamicContentFragment.TAG_ALIAS_CHECK_LIST}/"
        private const val LINK_CHECK_UP_WOMEN =
            "/lk/marathon/page/${DynamicContentFragment.TAG_ALIAS_CHECK_UP_WOMEN}/"
        private const val LINK_CHECK_UP_MEN =
            "/lk/marathon/page/${DynamicContentFragment.TAG_ALIAS_CHECK_UP_MEN}/"
        private const val LINK_KCAL = "/lk/marathon/calc/kcal/"
        private const val LINK_BMI = "/lk/marathon/calc/bmi/"
        private const val LINK_FAT = "/lk/marathon/calc/fat/"
        private const val LINK_CARDIO = "/lk/marathon/calc/cardio/"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.setupContent(args.content)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        viewModel.content.apply {
            removeObservers(viewLifecycleOwner)
            observe(viewLifecycleOwner) {
                binding.tvContent.apply {
                    setHtmlText(this, it)
                    handleUrlClicks { url ->
                        when (url) {
                            LINK_WORKOUT -> {
                                navigateTo(
                                    R.id.navigation_faq, FaqFragmentArgs.Builder()
                                        .setAlias(FaqFragment.TAG_ALIAS_WORKOUT)
                                        .build().toBundle()
                                )
                            }
                            LINK_FOOD -> {
                                navigateTo(
                                    R.id.navigation_faq, FaqFragmentArgs.Builder()
                                        .setAlias(FaqFragment.TAG_ALIAS_FOOD)
                                        .build().toBundle()
                                )
                            }
                            LINK_PROGRESS -> {
                                navigateTo(
                                    R.id.navigation_faq, FaqFragmentArgs.Builder()
                                        .setAlias(FaqFragment.TAG_ALIAS_PROGRESS)
                                        .build().toBundle()
                                )
                            }
                            LINK_MAP -> {
                                navigateTo(
                                    R.id.navigation_dynamic_content_page,
                                    DynamicContentFragmentArgs.Builder()
                                        .setAlias(DynamicContentFragment.TAG_ALIAS_MAP)
                                        .build().toBundle()
                                )
                            }
                            LINK_CHECK_LIST -> {
                                navigateTo(
                                    R.id.navigation_dynamic_content_page,
                                    DynamicContentFragmentArgs.Builder()
                                        .setAlias(DynamicContentFragment.TAG_ALIAS_CHECK_LIST)
                                        .build().toBundle()
                                )
                            }
                            LINK_CHECK_UP_WOMEN -> {
                                navigateTo(
                                    R.id.navigation_dynamic_content_page,
                                    DynamicContentFragmentArgs.Builder()
                                        .setAlias(DynamicContentFragment.TAG_ALIAS_CHECK_UP_WOMEN)
                                        .build().toBundle()
                                )
                            }
                            LINK_CHECK_UP_MEN -> {
                                navigateTo(
                                    R.id.navigation_dynamic_content_page,
                                    DynamicContentFragmentArgs.Builder()
                                        .setAlias(DynamicContentFragment.TAG_ALIAS_CHECK_UP_MEN)
                                        .build().toBundle()
                                )
                            }
                            LINK_KCAL -> navigateTo(R.id.navigation_calorie_calculator)
                            LINK_BMI -> navigateTo(R.id.navigation_bmi_calculator)
                            LINK_FAT -> navigateTo(R.id.navigation_fat_calculator)
                            LINK_CARDIO -> navigateTo(R.id.navigation_cardio_test)
                            else -> {
                                if (url.contains("http")){
                                    BrowserUtils.openBrowser(mContext, url)
                                }else {
                                    (if (url.startsWith(WEB_HOME)) url else WEB_HOME + url).let {
                                        BrowserUtils.openBrowser(mContext, it)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}