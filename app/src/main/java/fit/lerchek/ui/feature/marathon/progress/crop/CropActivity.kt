package fit.lerchek.ui.feature.marathon.progress.crop

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.PersistableBundle
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.rx.OnErrorListener
import fit.lerchek.common.util.Constants.UNKNOWN_ERROR
import fit.lerchek.databinding.ActivityCropBinding
import fit.lerchek.ui.base.BaseActivity
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.cropper.CropImageView
import fit.lerchek.ui.util.viewBinding
import fit.lerchek.util.EventObserver
import java.io.FileNotFoundException

@AndroidEntryPoint
class CropActivity : BaseActivity(), OnErrorListener, CropImageView.OnCropImageCompleteListener {

    private val binding by viewBinding(ActivityCropBinding::inflate)
    private val viewModel by viewModels<CropActivityViewModel>()

    private lateinit var photoType: String
    private var photoWeek: Int = -1

    companion object {
        const val REQUEST_CODE_PICK_PHOTO = 101
        const val EXTRA_TYPE = "extra_photo_type"
        const val EXTRA_WEEK = "extra_photo_week"
        const val TYPE_FRONT = "front"
        const val TYPE_BACK = "back"
        const val TYPE_SIDE = "side"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        photoType = intent.getStringExtra(EXTRA_TYPE) ?: TYPE_FRONT
        photoWeek = intent.getIntExtra(EXTRA_WEEK, -1)
        binding.btnUpload.setOnClickListener {
            binding.imgOrigin.setOnCropImageCompleteListener(this)
            binding.imgOrigin.getCroppedImageAsync()
        }
        viewModel.onErrorListener = this
        viewModel.success.observe(this, EventObserver { success ->
            if (success) {
                setResult(RESULT_OK)
                finish()
            }
        })
        binding.viewModel = viewModel
        binding.btnClose.setOnClickListener {
            onBackPressed()
        }
        binding.tvType.text = getString(
            when (photoType) {
                TYPE_BACK -> R.string.crop_back
                TYPE_SIDE -> R.string.crop_side
                else -> R.string.crop_front
            }
        )
        choosePhoto()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {}

    private fun choosePhoto() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        getActivityResultLauncher(REQUEST_CODE_PICK_PHOTO)?.launch(photoPickerIntent)
    }

    private val mPickPhotoActivityResultPhotoLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val selectedImage = result.data!!.data
                if (selectedImage != null) {
                    try {
                        val isMale = viewModel.profileIsMale.get()
                        binding.imgOrigin.cropShape = when (photoType) {
                            TYPE_BACK -> if (isMale) CropImageView.CropShape.BODY_BACK_MALE else CropImageView.CropShape.BODY_BACK_FEMALE
                            TYPE_SIDE -> if (isMale) CropImageView.CropShape.BODY_SIDE_MALE else CropImageView.CropShape.BODY_SIDE_FEMALE
                            else -> if (isMale) CropImageView.CropShape.BODY_FRONT_MALE else CropImageView.CropShape.BODY_FRONT_FEMALE
                        }
                        binding.imgOrigin.setImageUriAsync(selectedImage)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    } catch (e: OutOfMemoryError) {
                        e.printStackTrace()
                        ToastUtils.show(this, e.message!!)
                    }

                } else {
                    ToastUtils.show(this, "Не удалось открыть изображение")
                }
            }
        }

    override fun onError(code: String, message: String, goBack: Boolean) {
        ToastUtils.show(this, message)
        if (goBack) {
            Handler(Looper.getMainLooper()).post {
                onBackPressed()
            }
        }
    }

    override fun onCropImageComplete(view: CropImageView?, result: CropImageView.CropResult?) {
        if (result?.isSuccessful == true) {
            viewModel.uploadImages(
                type = photoType,
                week = photoWeek,
                cropped = result.bitmap,
                originalBitmap = result.originalBitmap
            )
        } else {
            Handler(Looper.getMainLooper()).post {
                ToastUtils.show(this, UNKNOWN_ERROR)
            }
        }
    }

    override fun getActivityResultLauncher(requestCode: Int): ActivityResultLauncher<Intent>? {
        return when (requestCode) {
            REQUEST_CODE_PICK_PHOTO -> mPickPhotoActivityResultPhotoLauncher
            else -> null
        }
    }


}