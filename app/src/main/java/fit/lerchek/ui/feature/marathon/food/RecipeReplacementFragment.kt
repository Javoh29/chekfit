package fit.lerchek.ui.feature.marathon.food

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.FragmentRecipeReplacementBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.today.TodayFragmentArgs
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class RecipeReplacementFragment :
    BaseToolbarDependentFragment(R.layout.fragment_recipe_replacement) {

    private val binding by viewBindings(FragmentRecipeReplacementBinding::bind)
    private val viewModel by viewModels<RecipeReplacementViewModel>()
    private val args: RecipeReplacementFragmentArgs by navArgs()


    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadReplacements(args.recipeId, args.baseRecipeId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = object : RecipeReplacementAdapter.RecipeReplacementCallback {
            override fun onReplace(recipeUIModel: RecipeUIModel) {
                viewModel.replaceRecipe(
                    recipeId = args.recipeId,
                    baseRecipeId = args.baseRecipeId,
                    replacement = recipeUIModel,
                    nutritionPlanId = args.nutritionPlanId,
                    dayNumber = args.dayNumber,
                    onComplete = ::navigateToOrigin
                )
            }

            override fun onCancel() {
                navigateUp()
            }
        }
    }

    private fun navigateToOrigin() {
        if (args.isFromToday) {
            navigateTo(
                resId = R.id.navigation_today,
                bundle = TodayFragmentArgs.Builder()
                    .setMarathonId(viewModel.getMarathonId())
                    .build()
                    .toBundle(),
                popBackStack = true
            )
        } else {
            navigateTo(
                resId = R.id.navigation_food,
                popBackStack = true
            )
        }

    }
}