package fit.lerchek.ui.feature.chat

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.rx.OnErrorListener
import fit.lerchek.databinding.ActivityChatBinding
import fit.lerchek.ui.base.BaseActivity
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.viewBinding

@AndroidEntryPoint
class ChatActivity : BaseActivity(), OnErrorListener {

    private val binding by viewBinding(ActivityChatBinding::inflate)
    private val viewModel by viewModels<ChatActivityViewModel>()
    private var adapter: ChatAdapter? = null

    private var onRVLayoutChangeListener =
        View.OnLayoutChangeListener { _, _, _, _, bottom, _, _, _, oldBottom ->
            if (bottom < oldBottom) {
                adapter?.itemCount?.let { itemCount ->
                    binding.rvChat.post {
                        if (itemCount > 0) {
                            binding.rvChat.smoothScrollToPosition(itemCount - 1)
                        }
                    }
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initListeners()
        binding.viewModel = viewModel
        viewModel.onErrorListener = this
        viewModel.getChat()
        viewModel.messageList.apply {
            removeObservers(this@ChatActivity)
            observe(this@ChatActivity) { messages ->
                if (adapter == null) {
                    adapter = ChatAdapter(messages.toMutableList())
                    binding.rvChat.adapter = adapter
                } else {
                    adapter!!.update(messages)
                }
                binding.rvChat.apply {
                    removeOnLayoutChangeListener(onRVLayoutChangeListener)
                    addOnLayoutChangeListener(onRVLayoutChangeListener)
                    if (messages.isNotEmpty()) {
                        scrollToPosition(messages.size - 1)
                    }
                }
            }
        }
    }

    private fun initListeners() {
        binding.btnBack.setOnClickListener {
            onBackPressed()
        }
        binding.btnSend.setOnClickListener {
            viewModel.sendMessage()
        }
    }

    override fun onError(code: String, message: String, goBack: Boolean) {
        ToastUtils.show(this, message)
        if (goBack) {
            Handler(Looper.getMainLooper()).post {
                onBackPressed()
            }
        }
    }

    override fun unclosedIdForKeyboard(): Int {
        return R.id.inputMessage
    }
}