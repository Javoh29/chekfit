package fit.lerchek.ui.feature.marathon.food

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.data.domain.uimodel.marathon.FoodFilterUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.*

class FoodAdapter(
    private var data: List<FoodItem>,
    private var callback: FoodCallback?
) : RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE_FILTER = 0
        const val ITEM_VIEW_TYPE_RECIPE = 1
        const val ITEM_VIEW_TYPE_EMPTY = 3
    }

    interface FoodItem

    sealed class FoodViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class EmptyViewHolder(itemFoodEmptyBinding: ItemFoodEmptyBinding) :
        FoodViewHolder(itemFoodEmptyBinding)

    class FilterViewHolder(val itemFoodFilterBinding: ItemFoodFilterBinding) :
        FoodViewHolder(itemFoodFilterBinding)

    class RecipeItemViewHolder(val itemFoodRecipeBinding: ItemFoodRecipeBinding) :
        FoodViewHolder(itemFoodRecipeBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_FILTER -> FilterViewHolder(
                ItemFoodFilterBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_RECIPE -> RecipeItemViewHolder(
                ItemFoodRecipeBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_EMPTY -> EmptyViewHolder(
                ItemFoodEmptyBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is FoodFilterUIModel -> ITEM_VIEW_TYPE_FILTER
            is RecipeUIModel -> ITEM_VIEW_TYPE_RECIPE
            else -> ITEM_VIEW_TYPE_EMPTY
        }
    }

    fun update(position: Int) {
        (data[position] as RecipeUIModel).isFavorite = !(data[position] as RecipeUIModel).isFavorite
        notifyItemChanged(position)
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        when (holder) {
            is FilterViewHolder -> {
                holder.itemFoodFilterBinding.apply {
                    val filter = data[position] as FoodFilterUIModel
                    this.callback = this@FoodAdapter.callback
                    this.model = filter
                    filter.getNutritionPlan()?.let { plan->
                        imgNutritionPlan.let{ img ->
                            Glide.with(img.context).load(plan.imageUrl).into(img)
                        }
                    }

                }
            }
            is RecipeItemViewHolder -> {
                val recipe = data[position] as RecipeUIModel
                holder.itemFoodRecipeBinding.apply {
                    model = recipe
                    Glide.with(imgRecipe.context).load(recipe.imageUrl).into(imgRecipe)
                    btnReplace.setOnClickListener {
                        callback?.onReplaceRecipe(recipe)
                    }
                    btnFavorite.setOnClickListener {
                        callback?.onFavToggleClick(recipe.isFavorite, recipe.id, position)
                        (data[position] as RecipeUIModel).isFavorite = !recipe.isFavorite
                        notifyItemChanged(position)
                    }
                    cardRecipe.setOnClickListener {
                        callback?.onSelectRecipe(recipe)
                    }
                }

            }
            else -> {
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}

@BindingAdapter("food_items", "food_adapter_callback")
fun setupFoodItemsAdapter(
    list: RecyclerView,
    items: List<FoodAdapter.FoodItem>?,
    callback: FoodCallback?
) {
    items?.let {
        list.adapter = FoodAdapter(it, callback)
    }
}