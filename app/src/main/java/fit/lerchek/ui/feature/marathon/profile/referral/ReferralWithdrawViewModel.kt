package fit.lerchek.ui.feature.marathon.profile.referral

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.exceptions.WithdrawError
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class ReferralWithdrawViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val phone = ObservableField<String>()
    val phoneError = ObservableField<String>()
    val amount = ObservableField<String>()
    val amountError = ObservableField<String>()
    val successWithdraw = ObservableBoolean()
    val errorWithdraw = ObservableBoolean()

    fun processWithdraw(balance: Int) {
        processTask(
            userRepository.processWithdraw(
                amount = amount.get()?.toIntOrNull() ?: 0,
                phone = phone.get() ?: "",
                balance = balance
            ).subscribeWith(object : DisposableSingleObserver<Boolean>() {
                override fun onStart() {
                    isProgress.set(true)
                    successWithdraw.set(false)
                    errorWithdraw.set(false)
                    phoneError.set("")
                    amountError.set("")
                }

                override fun onSuccess(success: Boolean) {
                    successWithdraw.set(success)
                    errorWithdraw.set(false)
                    isProgress.set(false)
                }

                override fun onError(e: Throwable) {
                    successWithdraw.set(false)
                    errorWithdraw.set(true)
                    if (e is WithdrawError) {
                        amountError.set(e.amountError)
                        phoneError.set(e.phoneError)
                    }
                    isProgress.set(false)
                }
            })
        )
    }

}