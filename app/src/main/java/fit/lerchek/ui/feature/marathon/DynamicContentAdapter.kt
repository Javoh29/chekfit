package fit.lerchek.ui.feature.marathon

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.marginStart
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel
import fit.lerchek.databinding.*
import fit.lerchek.ui.util.SupportUtils


class DynamicContentAdapter(
    private var data: List<DynamicContentUIModel.DynamicContentListItem>,
    private var lifecycleHolder: LifecycleHolder?
) : RecyclerView.Adapter<DynamicContentAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_HEADER = 0
        private const val ITEM_VIEW_TYPE_TITLE = 1
        private const val ITEM_VIEW_TYPE_WHITE_BLOCK = 2
        private const val ITEM_VIEW_TYPE_VIDEO = 3
        private const val ITEM_VIEW_TYPE_IMAGE = 4
        private const val ITEM_VIEW_TYPE_SPANNED = 5
    }


    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class HeaderViewHolder(val itemHeaderBinding: ItemDynamicHeaderBinding) :
        ViewHolder(itemHeaderBinding)

    class WhiteBlockViewHolder(val itemWhiteBlockBinding: ItemDynamicWhiteBlockBinding) :
        ViewHolder(itemWhiteBlockBinding)

    class TitleViewHolder(val itemTitleBlockBinding: ItemDynamicTitleBinding) :
        ViewHolder(itemTitleBlockBinding)

    class SpannedViewHolder(val itemSpannedBinding: ItemDynamicSpannedBinding) :
        ViewHolder(itemSpannedBinding)

    class VideoViewHolder(itemVideoBinding: ItemDynamicVideoBinding) :
        ViewHolder(itemVideoBinding) {

        private var currentVideoId: String? = null
        private var youTubePlayer: YouTubePlayer? = null

        init {
            itemVideoBinding.youtubePlayerView.apply {
                addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                    override fun onReady(youTubePlayer: YouTubePlayer) {
                        this@VideoViewHolder.youTubePlayer = youTubePlayer
                        cueVideo(currentVideoId)
                    }
                })
                getPlayerUiController().apply {
                    //не работает сокрытие. но кнопки не кликабельны
                    //https://github.com/PierfrancescoSoffritti/android-youtube-player/issues/513
                    showMenuButton(false)
                    showVideoTitle(false)
                }
            }
        }

        fun cueVideo(videoId: String?) {
            currentVideoId = videoId
            currentVideoId?.let {
                youTubePlayer?.cueVideo(it, 0f)
            }
        }

    }

    class ImageViewHolder(val itemImageBinding: ItemDynamicImageBinding) :
        ViewHolder(itemImageBinding)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder(
                ItemDynamicHeaderBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_VIDEO -> {
                ItemDynamicVideoBinding.inflate(inflater, parent, false).let {
                    lifecycleHolder?.getMyLifecycle()?.addObserver(it.youtubePlayerView)
                    VideoViewHolder(it)
                }
            }
            ITEM_VIEW_TYPE_TITLE -> TitleViewHolder(
                ItemDynamicTitleBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_IMAGE -> ImageViewHolder(
                ItemDynamicImageBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_WHITE_BLOCK -> WhiteBlockViewHolder(
                ItemDynamicWhiteBlockBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SPANNED -> SpannedViewHolder(
                ItemDynamicSpannedBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is DynamicContentUIModel.Title -> ITEM_VIEW_TYPE_TITLE
            is DynamicContentUIModel.WhiteBlock -> ITEM_VIEW_TYPE_WHITE_BLOCK
            is DynamicContentUIModel.Image -> ITEM_VIEW_TYPE_IMAGE
            is DynamicContentUIModel.Video -> ITEM_VIEW_TYPE_VIDEO
            is DynamicContentUIModel.SpannedContent -> ITEM_VIEW_TYPE_SPANNED
            else -> ITEM_VIEW_TYPE_HEADER
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                holder.itemHeaderBinding.apply {
                    btnBack.root.setOnClickListener {
                        lifecycleHolder?.onBack()
                    }
                }
            }
            is VideoViewHolder -> {
                (data[position] as DynamicContentUIModel.Video).let { video ->
                    holder.apply {
                        cueVideo(video.videoUrl)
                    }
                }
            }
            is TitleViewHolder -> {
                (data[position] as DynamicContentUIModel.Title).let { title ->
                    holder.itemTitleBlockBinding.tvTitle.apply {
                        text = title.text
                        val isInner = title.isInner
                        val _4dp = SupportUtils.convertDpToPixel(4f, context)
                        val _24dp = SupportUtils.convertDpToPixel(24f, context)
                        (layoutParams as ViewGroup.MarginLayoutParams).setMargins(
                            (if (isInner) 0 else _4dp).toInt(),
                            (if (isInner) 0 else _24dp).toInt(),
                            0,
                            0
                        )
                    }
                }
            }
            is SpannedViewHolder -> {
                (data[position] as DynamicContentUIModel.SpannedContent).let { span ->
                    holder.itemSpannedBinding.tvContent.setText(
                        span.spannableString,
                        TextView.BufferType.SPANNABLE
                    )
                }
            }

            is ImageViewHolder -> {
                (data[position] as DynamicContentUIModel.Image).let { imageModel ->
                    holder.itemImageBinding.ivImage.let { img ->
                        Glide.with(img.context).load(imageModel.imageUrl).into(img)
                    }
                }
            }
            is WhiteBlockViewHolder -> {
                (data[position] as DynamicContentUIModel.WhiteBlock).displayItems.let { displayItems ->
                    holder.itemWhiteBlockBinding.rvDynamicItems.let { rv ->
                        setupDynamicContentAdapter(rv, displayItems, lifecycleHolder)
                    }
                }
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}

@BindingAdapter("dynamic_content", "video_view_lifecycle_holder")
fun setupDynamicContentAdapter(
    list: RecyclerView,
    items: List<DynamicContentUIModel.DynamicContentListItem>?,
    lifecycleHolder: LifecycleHolder?
) {
    items?.let {
        list.adapter = DynamicContentAdapter(it, lifecycleHolder)
    }
}