package fit.lerchek.ui.feature.marathon.profile.personal

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentUserPersonalBodyParametersBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.viewBindings
import fit.lerchek.util.EventObserver


@AndroidEntryPoint
class UserPersonalParamFragment : BaseToolbarDependentFragment(R.layout.fragment_user_personal_body_parameters){

    private val binding by viewBindings(FragmentUserPersonalBodyParametersBinding::bind)
    private val viewModel by viewModels<PersonalParamViewModel>()

    private var selfEditFiled = false

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Close
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this

        viewModel.success.observe(this, EventObserver { success ->
            mContext.closeExistingAlert()
            if (success) {
                mContext.apply {
                    ToastUtils.show(this , R.string.data_saved)
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        binding.save.setOnClickListener {
            viewModel.savePersonalParam()
        }

        checkValue(binding.waist)
        checkValue(binding.weight)
        checkValue(binding.height)
        checkValue(binding.breastVolume)
        checkValue(binding.hips)

    }

    private fun checkValue(fieldEditText: AppCompatEditText) {
        fieldEditText.doAfterTextChanged {
            if(selfEditFiled){
                selfEditFiled = false
                return@doAfterTextChanged
            }
            val min = 1
            val max = 400
            var num = min

            try{
                num = it.toString().toInt()
            } catch (e:NumberFormatException){}

            if(num < min){
                selfEditFiled = true
                fieldEditText.setText(min.toString())

            }else if(num > max){
                selfEditFiled = true
                fieldEditText.setText(max.toString())
            }
        }

    }
}

