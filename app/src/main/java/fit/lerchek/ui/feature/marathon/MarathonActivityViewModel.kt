package fit.lerchek.ui.feature.marathon

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class MarathonActivityViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var profileIsMale = ObservableBoolean()

    init {
        checkGender()
    }

    private val mSuccessLogout = MutableLiveData<Event<Boolean>>()
    val successLogout: LiveData<Event<Boolean>>
        get() = mSuccessLogout

    fun logout() {
        processTask(
            userRepository.logout().subscribeWith(object : DisposableSingleObserver<Boolean>() {
                override fun onSuccess(success: Boolean) {
                    mSuccessLogout.postValue(Event(success))
                }

                override fun onError(e: Throwable) {
                    mSuccessLogout.postValue(Event(false))
                }
            })
        )
    }

    private fun checkGender() {
        processTask(userRepository.isCurrentProfileMale().subscribeWith(
            object : DisposableSingleObserver<Boolean>() {
                override fun onSuccess(isMale: Boolean) {
                    profileIsMale.set(isMale)
                }

                override fun onError(e: Throwable) {}
            }
        ))
    }

    fun resetAppMenu() {
        userRepository.clearAppMenuCache()
    }

}