package fit.lerchek.ui.feature.login.forgotpwd

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentSetNewPwdBinding
import fit.lerchek.ui.base.BaseFragment
import fit.lerchek.ui.feature.marathon.MarathonActivity
import fit.lerchek.ui.util.viewBindings
import fit.lerchek.util.EventObserver

@AndroidEntryPoint
class SetNewPwdFragment : BaseFragment(R.layout.fragment_set_new_pwd) {

    private val binding by viewBindings(FragmentSetNewPwdBinding::bind)

    private val viewModel by viewModels<SetNewPwdViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.success.observe(this, EventObserver { success ->
            mContext.closeExistingAlert()
            if (success) {
                mContext.apply {
                    startActivity(Intent(this, MarathonActivity::class.java))
                    finish()
                }
            }
        })

        val value = navArgs<SetNewPwdFragmentArgs>().value
        viewModel.code = value.code
        viewModel.email = value.email
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        binding.inputNewPwd.doAfterTextChanged {
            binding.sendNewPwd.isEnabled = viewModel.canSendNewPwd()
        }
//        binding.confirmPwd.doAfterTextChanged {
//            binding.sendNewPwd.isEnabled = viewModel.canSendNewPwd()
//        }

        binding.sendNewPwd.setOnClickListener {
            viewModel.sendNewPwd()
        }

    }
}