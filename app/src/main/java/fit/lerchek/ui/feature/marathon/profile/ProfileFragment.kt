package fit.lerchek.ui.feature.marathon.profile

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.Constants
import fit.lerchek.databinding.FragmentProfileBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.ThemeUtil
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class ProfileFragment : BaseToolbarDependentFragment(R.layout.fragment_profile) {

    private val binding by viewBindings(FragmentProfileBinding::bind)
    private val viewModel by viewModels<ProfileViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadProfileDetails(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.profileScrollView
        binding.viewModel = viewModel
        binding.viewNightMode.apply {
            if (ThemeUtil.isNightModeSupported()) {
                visibility = View.VISIBLE
                binding.themeSwitch.apply {
                    isChecked = viewModel.isNightTheme()
                    setOnCheckedChangeListener { _, isChecked ->
                        Handler(Looper.getMainLooper()).postDelayed({
                            viewModel.switchTheme(isChecked)
                            requireActivity().recreate()
                        }, Constants.DEFAULT_DELAY)
                    }
                }
            } else {
                visibility = View.GONE
            }
        }
        binding.btnPersonalData.setOnClickListener {
            navigateTo(R.id.action_navigation_profile_to_userPersonalInfoFragment)
        }
        binding.btnBodyParams.setOnClickListener {
            navigateTo(R.id.action_navigation_profile_to_userPersonalParamFragment)
        }
        binding.btnTest.setOnClickListener {
            navigateTo(R.id.navigation_test)
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadProfileDetails(false)
    }
}