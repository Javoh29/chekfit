package fit.lerchek.ui.feature.marathon.more.fat

import android.os.Handler
import android.os.Looper
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.Constants
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.TextUtils.convertWithTwoDecimal
import javax.inject.Inject
import kotlin.math.log10
import kotlin.math.pow

@HiltViewModel
class FatCalculatorViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val isMale: ObservableBoolean = ObservableBoolean()
    var age: ObservableField<String> = ObservableField("")
    var height: ObservableField<String> = ObservableField("")
    var weight: ObservableField<String> = ObservableField("")
    var waist: ObservableField<String> = ObservableField("")
    var hips: ObservableField<String> = ObservableField("")
    var neck: ObservableField<String> = ObservableField("")

    var yourFat: ObservableField<String> = ObservableField("")

    val isCalculate: ObservableBoolean = ObservableBoolean()

    init {
        isProgress.set(true)
        checkGender()
    }

    private fun checkGender() {
        processTask(userRepository.isCurrentProfileMale().subscribeWith(
            simpleSingleObserver<Boolean> {
                isMale.set(it)
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        isProgress.set(false)
                    },
                    Constants.DEFAULT_DELAY
                )
            }
        ))
    }

    fun calculate() {
        try {
            val age = this.age.get()?.toInt() ?: throw NumberFormatException()
            val height = this.height.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()
            val weight = this.weight.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()
            val waist = this.waist.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()
            val neck = this.neck.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()

            if (age <= 0 || height <= 0 || weight <= 0 || waist <= 0 || neck <= 0) throw NumberFormatException()

            val isMale = this.isMale.get()
            val fat = if (isMale) {

                val v1 = 86.01 * log10(waist - neck) - 70.041 * log10(height) + 30.3
                val v2 =
                    (-98.42 + (4.15 * waist) / 2.54 - 0.082 * (weight / 0.454)) / (weight / 0.454) * 100
                val v3 =
                    (1.2 * weight) / (((height / 100) * height) / 100) + 0.23 * age - 10.8 - 5.4

                (v1 * v2 * v3).pow(1.0 / 3)
            } else {
                val hips = this.hips.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()
                if (hips <= 0) throw NumberFormatException()

                val v1 =
                    163.205 * log10(waist + hips - neck) - 97.684 * log10(height) - 104.912
                val v2 =
                    (-76.76 + (4.15 * waist) / 2.54 - 0.082 * (weight / 0.454)) / (weight / 0.454) * 100
                val v3 =
                    (1.2 * weight) / (((height / 100) * height) / 100) + 0.23 * age - 5.4

                (v1 * v2 * v3).pow(1.0 / 3)
            }
            update(fat)
        } catch (e: NumberFormatException) {
            update(0.0)
        }
    }

    private fun update(fat: Double) {
        this.yourFat.set(fat.convertWithTwoDecimal().plus("%"))
        this.isCalculate.set(fat > 0)
    }

}