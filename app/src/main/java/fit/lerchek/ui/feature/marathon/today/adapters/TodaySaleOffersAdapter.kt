package fit.lerchek.ui.feature.marathon.today.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.data.domain.uimodel.marathon.MarathonSaleOfferUIModel
import fit.lerchek.databinding.ItemTodaySaleOfferBinding
import fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback

class TodaySaleOffersAdapter(
    private var data: List<MarathonSaleOfferUIModel>,
    private var callback: TodayCallback?
) :
    RecyclerView.Adapter<TodaySaleOffersAdapter.OfferViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {
        return OfferViewHolder(
            ItemTodaySaleOfferBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) {
        holder.binding.model = data[position]
        holder.binding.callback = callback
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class OfferViewHolder(val binding: ItemTodaySaleOfferBinding) :
        RecyclerView.ViewHolder(binding.root)

}

@BindingAdapter("marathon_sale_offers", "marathon_details_callback")
fun setupSaleOffersAdapter(
    list: RecyclerView,
    items: List<MarathonSaleOfferUIModel>?,
    callback: TodayCallback?
) {
    items?.let {
        list.adapter = TodaySaleOffersAdapter(it, callback)
    }
}