package fit.lerchek.ui.feature.marathon.progress

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel
import fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel
import fit.lerchek.databinding.FragmentProgressBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.voting.VotingImageAdapter
import fit.lerchek.ui.util.viewBindings
import java.util.*

@AndroidEntryPoint
class ProgressFragment : BaseToolbarDependentFragment(R.layout.fragment_progress) {
    private val binding by viewBindings(FragmentProgressBinding::bind)
    private val viewModel by viewModels<ProgressViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override var isBottomNavigationVisible = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModel.loadProgress()
        binding.apply {
            val makeMeasurements = View.OnClickListener {
                if (viewModel?.progress?.get()?.isEmptyWeeks != false) {
                    AlertDialog.Builder(requireContext())
                        .setMessage(R.string.progress_error_title)
                        .setPositiveButton(android.R.string.ok, null)
                        .create()
                        .show()
                } else navigateTo(
                    ProgressFragmentDirections.actionNavigationProgressToNavigationMakeMeasurements()
                )
            }
            progressEmpty.btnTakeMeasurements.setOnClickListener(makeMeasurements)
            progressYourProgress.btnAddMeasurements.setOnClickListener(makeMeasurements)

            progressResult.btnMoreDetailsWeight.setOnClickListener {
                navigateTo(
                    ProgressFragmentDirections.actionNavigationProgressToNavigationMeasurements(
                        MeasurementsUIModel.Type.WEIGHT.name
                    )
                )
            }
            progressResult.btnMoreDetailsWaist.setOnClickListener {
                navigateTo(
                    ProgressFragmentDirections.actionNavigationProgressToNavigationMeasurements(
                        MeasurementsUIModel.Type.WAIST.name
                    )
                )
            }
            progressResult.btnMoreDetailsBreast.setOnClickListener {
                navigateTo(
                    ProgressFragmentDirections.actionNavigationProgressToNavigationMeasurements(
                        MeasurementsUIModel.Type.BREAST.name
                    )
                )
            }
            progressResult.btnMoreDetailsHips.setOnClickListener {
                navigateTo(
                    ProgressFragmentDirections.actionNavigationProgressToNavigationMeasurements(
                        MeasurementsUIModel.Type.HIPS.name
                    )
                )
            }

            viewModel?.progress?.addOnPropertyChangedCallback(
                object : Observable.OnPropertyChangedCallback() {
                    override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                        (sender as ObservableField<*>).get()?.let { progress ->
                            progress as MarathonProgressUIModel

                            if(getView()!=null) {
                                progress.marathonImageUrl?.let { url ->
                                    progressEmpty.imgMarathon.apply {
                                        Glide.with(this.context).load(url).into(this)
                                    }
                                }

                                progress.progress.photos?.let photos@{ photos ->
                                    val photo0 = photos.getOrNull(0) ?: return@photos
                                    val photo1 = photos.getOrNull(1) ?: return@photos
                                    val images = arrayListOf<Pair<String, String>>().apply {
                                        add(Pair(photo0.front?:"", photo1.front?:""))
                                        add(Pair(photo0.back?:"", photo1.back?:""))
                                        add(Pair(photo0.side?:"", photo1.side?:""))
                                    }
                                    progressPhotos.imagesPager.adapter = VotingImageAdapter(images)
                                    TabLayoutMediator(progressPhotos.tabs, progressPhotos.imagesPager) { _, _ -> }.attach()
                                    progressPhotos.imagesPager.currentItem = progress.progress.selectedImagePosition
                                    progressPhotos.btnImagePrev.setOnClickListener {
                                        if (progressPhotos.imagesPager.currentItem > 0) {
                                            progressPhotos.imagesPager.currentItem--
                                        }
                                    }
                                    progressPhotos.btnImageNext.setOnClickListener {
                                        if (progressPhotos.imagesPager.currentItem < images.size - 1) {
                                            progressPhotos.imagesPager.currentItem++
                                        }
                                    }

                                    progressPhotos.imagesPager.registerOnPageChangeCallback(object :
                                        ViewPager2.OnPageChangeCallback() {
                                        override fun onPageSelected(position: Int) {
                                            progress.progress.selectedImagePosition = position
                                        }
                                    })
//                                    progressPhotos.imgProgressFront0.apply {
//                                        Glide.with(this.context).load(photo0.front).into(this)
//                                    }
//                                    progressPhotos.imgProgressBack0.apply {
//                                        Glide.with(this.context).load(photo0.back).into(this)
//                                    }
//                                    progressPhotos.imgProgressSide0.apply {
//                                        Glide.with(this.context).load(photo0.side).into(this)
//                                    }
//                                    progressPhotos.imgProgressFront1.apply {
//                                        Glide.with(this.context).load(photo1.front).into(this)
//                                    }
//                                    progressPhotos.imgProgressBack1.apply {
//                                        Glide.with(this.context).load(photo1.back).into(this)
//                                    }
//                                    progressPhotos.imgProgressSide1.apply {
//                                        Glide.with(this.context).load(photo1.side).into(this)
//                                    }
                                }
                            }
                        }
                    }
                })
        }
    }
}