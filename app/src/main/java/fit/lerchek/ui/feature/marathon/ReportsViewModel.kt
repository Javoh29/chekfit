package fit.lerchek.ui.feature.marathon

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.marathon.ReportsUIModel
import fit.lerchek.data.domain.uimodel.marathon.WeekUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class ReportsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var isEditMode = ObservableBoolean(false)

    var profileIsMale = ObservableBoolean()

    private val mReports = MutableLiveData<ReportsUIModel>()

    val reports: LiveData<ReportsUIModel>
        get() = mReports

    val reportText = ObservableField<String>()

    init {
        checkGender()
    }

    private fun checkGender() {
        processTask(userRepository.isCurrentProfileMale().subscribeWith(
            object : DisposableSingleObserver<Boolean>() {
                override fun onSuccess(isMale: Boolean) {
                    profileIsMale.set(isMale)
                }

                override fun onError(e: Throwable) {}
            }
        ))
    }

    fun loadReports() {
        processTask(
            userRepository.loadReports()
                .subscribeWith(simpleSingleObserver<ReportsUIModel> {
                    mReports.postValue(it)
                    updateEditMode(it)
                    isProgress.set(false)
                })
        )
    }

    private fun saveReport() {
        processTask(
            userRepository.saveReport(reports.value!!, reportText.get() ?: "")
                .subscribeWith(object : DisposableSingleObserver<ReportsUIModel>() {
                    override fun onStart() {
                        super.onStart()
                        isProgress.set(true)
                    }

                    override fun onSuccess(result: ReportsUIModel) {
                        mReports.postValue(result)
                        updateEditMode(result)
                        isProgress.set(false)
                    }

                    override fun onError(e: Throwable) {
                        onErrorListener?.onError(
                            message = e.message ?: Constants.UNKNOWN_ERROR,
                            goBack = false
                        )
                    }
                })
        )
    }

    fun selectWeek(week: WeekUIModel) {
        reports.value?.let {
            it.selectWeek(week.id)
            updateEditMode(it)
            mReports.postValue(it)
        }
    }

    fun toggleSendEdit() {
        if (isEditMode.get().not()) {
            isEditMode.set(true)
        } else {
            isEditMode.set(false)
            saveReport()
        }
    }

    private fun updateEditMode(reports: ReportsUIModel){
        reports.getReport()?.reportText.let { text->
            if(text.isNullOrEmpty()){
                isEditMode.set(true)
            } else {
                isEditMode.set(false)
            }
        }
    }
}