package fit.lerchek.ui.feature.marathon

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class DynamicContentViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    private var mContent = MutableLiveData<DynamicContentUIModel>()

    val content: LiveData<DynamicContentUIModel>
        get() = mContent

    fun loadContent(alias: String) {
        processTask(
            userRepository.loadDynamicContent(alias)
                .subscribeWith(simpleSingleObserver<DynamicContentUIModel> {
                    mContent.postValue(it)
                    isProgress.set(false)
                })
        )
    }
}