@file:Suppress("DEPRECATION")

package fit.lerchek.ui.feature.marathon.today

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataSource
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.request.SessionReadRequest
import com.google.android.gms.fitness.result.SessionReadResponse
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.api.message.marathon.FitData
import fit.lerchek.data.api.message.marathon.FitDataRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerGoalRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.databinding.FragmentTrackerBindingImpl
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.today.callbacks.TrackerCallBack
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.viewBindings
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


@AndroidEntryPoint
class TrackerFragment : BaseToolbarDependentFragment(R.layout.fragment_tracker), TrackerCallBack {
    private val binding by viewBindings(FragmentTrackerBindingImpl::bind)
    private val viewModel by viewModels<TrackerViewModel>()
    private var trackerType: String = "water"
    private val recognitionRequestCode: Int = 1122
    private val googleSignInRequestCode: Int = 2211
    private var mGoogleApiClient: GoogleApiClient? = null
    private val googleFitId = "com.google.android.apps.fitness"

    override var isBottomNavigationVisible = false

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackerType = arguments?.getString("type", "water")!!
        viewModel.onErrorListener = this
        if (viewModel.getSwitchState()) getGoogleFitData()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadData(type = trackerType, period = "week")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }
    }

    override fun onChangeBtn() {

    }

    override fun getSelectParam() = viewModel.selectParam

    override fun getSelectPeriod(): String {
        return viewModel.selectPeriod
    }

    override fun getTrackerType(): String {
        return trackerType
    }

    override fun onClickSetting(
        settingType: String,
        date: String,
        dimension: Int,
        goal: Int,
        value: Int
    ) {
//        if (settingType == "steps_ad" && viewModel.getSwitchState())
//            AlertDialog.Builder(requireContext())
//                .setMessage(R.string.text_google_fit_error)
//                .setPositiveButton(android.R.string.ok, null)
//                .create()
//                .show()
//        else
        navigateTo(
            TrackerFragmentDirections.actionNavigationTrackerToNavigationSetting(
                settingType,
                date,
                dimension,
                goal,
                value
            )
        )
    }

    override fun onClickPeriod(period: String) {
        viewModel.loadData(type = trackerType, period = period)
    }

    override fun changeCountTracker(request: UpdateTrackerRequest, type: String) {
        viewModel.changeCountTracker(request, type, mGoogleApiClient)
    }

    override fun changeSwitchFit(index: Int) {
        val fitnessOptions = FitnessOptions.builder()
            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_WRITE)
            .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE, FitnessOptions.ACCESS_WRITE)
            .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_HYDRATION, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_SLEEP_SEGMENT, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_SLEEP_SEGMENT, FitnessOptions.ACCESS_WRITE)
            .accessSleepSessions(FitnessOptions.ACCESS_WRITE)
            .accessSleepSessions(FitnessOptions.ACCESS_READ)
            .build()
        if (viewModel.getSwitchState()) {
            Fitness.getConfigClient(
                requireContext(),
                GoogleSignIn.getAccountForExtension(requireContext(), fitnessOptions)
            )
                .disableFit()
                .addOnSuccessListener {
                    ToastUtils.show(requireContext(), "Google Fit отключен")
                    viewModel.setSwitchFitValue(false)
                }
                .addOnFailureListener {
                    binding.rvTackers.adapter!!.notifyItemChanged(index)
                    ToastUtils.show(requireContext(), "Произошла ошибка при отключении Google Fit")
                }
        } else {
            if (checkAppGoogleFit()) {
                val acc = GoogleSignIn.getAccountForExtension(requireContext(), fitnessOptions)
                GoogleSignIn.requestPermissions(
                    this,
                    googleSignInRequestCode,
                    acc,
                    fitnessOptions
                )
            } else {
                binding.rvTackers.adapter!!.notifyItemChanged(index)
                AlertDialog.Builder(requireContext())
                    .setMessage(R.string.text_google_fit_install)
                    .setNegativeButton(R.string.text_btn_cancel, null)
                    .setPositiveButton(
                        R.string.text_btn_install
                    ) { _, _ ->
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse("market://details?id=$googleFitId")
                        startActivity(intent)
                    }
                    .create()
                    .show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == googleSignInRequestCode) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACTIVITY_RECOGNITION
                )
                != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
            ) {
                requestPermissions(
                    arrayOf(Manifest.permission.ACTIVITY_RECOGNITION),
                    recognitionRequestCode
                )
            } else {
                getGoogleFitData()
                GlobalScope.launch(Dispatchers.IO) {
                    viewModel.changeGoalTracker(UpdateTrackerGoalRequest(800, 1), "water_set_fit")
                }
            }
            viewModel.setSwitchFitValue(true)
        } else viewModel.setSwitchFitValue(false)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == recognitionRequestCode && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            getGoogleFitData()
            GlobalScope.launch(Dispatchers.IO) {
                viewModel.changeGoalTracker(UpdateTrackerGoalRequest(800, 1), "water_set_fit")
            }
        } else viewModel.setSwitchFitValue(false)
    }

    override fun isCheckFit(): Boolean {
        return viewModel.getSwitchState()
    }

    private fun checkAppGoogleFit(): Boolean {
        val pm = requireActivity().packageManager
        return try {
            pm?.getPackageInfo(googleFitId, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: Exception) {
            false
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getGoogleFitData() {
        viewModel.isProgress.set(true)
        val cal = Calendar.getInstance()
        val endDate = cal.time.time
        cal.add(Calendar.DATE, -31)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        val startDate = cal.time.time
        val listFitData = ArrayList<FitData>()
        when (trackerType) {
            "water" -> {
                listFitData.clear()
                try {
                    mGoogleApiClient = GoogleApiClient.Builder(requireContext().applicationContext)
                        .addApi(Fitness.HISTORY_API)
                        .addConnectionCallbacks(
                            object : GoogleApiClient.ConnectionCallbacks {
                                override fun onConnected(p0: Bundle?) {
                                    val readRequest: DataReadRequest = DataReadRequest.Builder()
                                        .read(DataType.TYPE_HYDRATION)
                                        .setTimeRange(
                                            startDate / 1000,
                                            endDate / 1000,
                                            TimeUnit.SECONDS
                                        ).build()

                                    Fitness.HistoryApi.readData(mGoogleApiClient!!, readRequest)
                                        .setResultCallback {
                                            if (it.status.isSuccess) {
                                                if (it.dataSets.size > 0) {
                                                    val map = HashMap<String, FitData>()
                                                    for (dataSet in it.dataSets) {
                                                        for (dp in dataSet.dataPoints) {
                                                            val model = FitData(
                                                                date = SimpleDateFormat("yyyy-MM-dd").format(
                                                                    Date(
                                                                        dp.getTimestamp(TimeUnit.MILLISECONDS)
                                                                    )
                                                                ),
                                                                value = (dp.getValue(Field.FIELD_VOLUME)
                                                                    .asFloat() * 1000).toInt()
                                                            )
                                                            if (map.containsKey(model.date)) {
                                                                map[model.date]!!.value += model.value
                                                            } else map[model.date] = model
                                                        }
                                                    }
                                                    if (map.isNotEmpty()) {
                                                        map.forEach { fit ->
                                                            listFitData.add(fit.value)
                                                        }
                                                        viewModel.uploadGoogleFitData(
                                                            FitDataRequest(
                                                                "water",
                                                                listFitData
                                                            )
                                                        )
                                                    } else viewModel.isProgress.set(false)
                                                }
                                            }
                                        }
                                }

                                override fun onConnectionSuspended(p0: Int) {

                                }
                            }
                        )
                        .addOnConnectionFailedListener { connectionResult ->
                            Log.i(
                                "BUG",
                                "Authorization - Failed Authorization Mgr:$connectionResult"
                            )
                        }
                        .build()
                    mGoogleApiClient?.connect()
                } catch (e: Exception) {
                    Log.e("BUG", e.message ?: "")
                }
            }
            "step" -> {
                listFitData.clear()
                val fitnessOptions = FitnessOptions.builder()
                    .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                    .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
                    .build()

                val datasource = DataSource.Builder()
                    .setAppPackageName("com.google.android.gms")
                    .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                    .setType(DataSource.TYPE_DERIVED)
                    .setStreamName("estimated_steps")
                    .build()

                val request = DataReadRequest.Builder()
                    .aggregate(datasource)
                    .bucketByTime(1, TimeUnit.DAYS)
                    .setTimeRange(startDate / 1000, endDate / 1000, TimeUnit.SECONDS)
                    .build()

                Fitness.getHistoryClient(
                    requireActivity(),
                    GoogleSignIn.getAccountForExtension(requireContext(), fitnessOptions)
                )
                    .readData(request)
                    .addOnSuccessListener { response ->
                        response.buckets
                            .flatMap { it.dataSets }
                            .forEach {
                                if (it.dataPoints.isNotEmpty()) {
                                    var totalSteps = 0
                                    it.dataPoints.forEach { dp ->
                                        totalSteps += dp.getValue(Field.FIELD_STEPS).asInt()
                                    }
                                    listFitData.add(
                                        FitData(
                                            date = SimpleDateFormat("yyyy-MM-dd").format(
                                                Date(
                                                    it.dataPoints.first()
                                                        .getTimestamp(TimeUnit.MILLISECONDS)
                                                )
                                            ), value = totalSteps
                                        )
                                    )
                                }
                            }
                        viewModel.uploadGoogleFitData(FitDataRequest("step", listFitData))
                    }
            }
            "dream" -> {
                listFitData.clear()
                val fitnessOptions = FitnessOptions.builder()
                    .addDataType(DataType.TYPE_SLEEP_SEGMENT, FitnessOptions.ACCESS_READ)
                    .build()

                val request = SessionReadRequest.Builder()
                    .readSessionsFromAllApps()
                    .includeSleepSessions()
                    .read(DataType.TYPE_SLEEP_SEGMENT)
                    .setTimeInterval(startDate / 1000, endDate / 1000, TimeUnit.SECONDS)
                    .build()

                Fitness.getSessionsClient(
                    requireActivity(),
                    GoogleSignIn.getAccountForExtension(requireContext(), fitnessOptions)
                ).readSession(request)
                    .addOnSuccessListener { sessionReadResponse: SessionReadResponse ->
                        val map = HashMap<String, FitData>()
                        for (ses in sessionReadResponse.sessions) {
                            val sessionStart = ses.getStartTime(TimeUnit.MILLISECONDS)
                            val sessionEnd = ses.getEndTime(TimeUnit.MILLISECONDS)
                            val sessionSleepTime = sessionEnd - sessionStart
                            val model = FitData(
                                date = SimpleDateFormat("yyyy-MM-dd").format(
                                    Date(sessionStart)
                                ),
                                value = TimeUnit.MILLISECONDS.toMinutes(sessionSleepTime)
                                    .toInt()
                            )
                            map[model.date] = model
                        }
                        if (map.isNotEmpty()) {
                            map.forEach { fit ->
                                listFitData.add(fit.value)
                            }
                            viewModel.uploadGoogleFitData(FitDataRequest("dream", listFitData))
                        } else viewModel.isProgress.set(false)
                    }.addOnFailureListener { err: Exception ->
                        Log.i(
                            "BUG",
                            err.message!!
                        )
                    }
            }
        }
    }
}