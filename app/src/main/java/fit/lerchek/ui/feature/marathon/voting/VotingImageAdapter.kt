package fit.lerchek.ui.feature.marathon.voting

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.databinding.ItemVotingImageBinding

class VotingImageAdapter(
    private var data: List<Pair<String,String>>
) : RecyclerView.Adapter<VotingImageAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            ItemVotingImageBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pair = data[position]
        holder.imageBinding.apply{
            img1.apply {
                Glide.with(this.context).load(pair.first).into(this)
            }
            img2.apply {
                Glide.with(this.context).load(pair.second).into(this)
            }
            holder.imageBinding.executePendingBindings()
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder(val imageBinding: ItemVotingImageBinding) :
        RecyclerView.ViewHolder(imageBinding.root)
}