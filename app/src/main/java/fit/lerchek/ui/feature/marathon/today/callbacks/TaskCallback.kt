package fit.lerchek.ui.feature.marathon.today.callbacks

import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel

interface TaskCallback {
    fun updateTaskCompletion(task: TaskUIModel, currentDay: Int)
    fun onTaskInfoClick(task: TaskUIModel)
}