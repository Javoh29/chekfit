package fit.lerchek.ui.feature.marathon.more.fat

import android.os.Bundle
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.databinding.Observable
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentFatCalculatorBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class FatCalculatorFragment :
    BaseToolbarDependentFragment(R.layout.fragment_fat_calculator) {

    private val binding by viewBindings(FragmentFatCalculatorBinding::bind)
    private val viewModel by viewModels<FatCalculatorViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.nsvFatCalculator
        binding.viewModel = viewModel
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }

        viewModel.yourFat.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if(getView()!=null) {
                    binding.nsvFatCalculator.postDelayed({
                        val child = binding.btnCalculate
                        binding.nsvFatCalculator.smoothScrollTo(child.x.toInt(), child.y.toInt())
                    }, 100)
                }
            }
        })
    }
}