package fit.lerchek.ui.feature.marathon.more.calories

import android.os.Handler
import android.os.Looper
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.TextUtils.convertWithTwoDecimal
import javax.inject.Inject
import kotlin.math.log10
import kotlin.math.pow

@HiltViewModel
class CalorieCalculatorViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    private var mContent = MutableLiveData<List<CalorieItem>>()

    val content: LiveData<List<CalorieItem>>
        get() = mContent

    val isMale: ObservableBoolean = ObservableBoolean()
    var age: ObservableField<String> = ObservableField("")
    var height: ObservableField<String> = ObservableField("")
    var weight: ObservableField<String> = ObservableField("")

    private val questions: HashMap<String, CalorieItemUIModel> = hashMapOf()

    var yourCalorie: ObservableField<String> = ObservableField("")

    val isCalculate: ObservableBoolean = ObservableBoolean()

    init {
        isProgress.set(true)
        checkGender()
    }

    private fun checkGender() {
        processTask(userRepository.isCurrentProfileMale().subscribeWith(
            simpleSingleObserver<Boolean> {
                isMale.set(it)
                mContent.postValue(userRepository.getCalorieDetails())
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        isProgress.set(false)
                    },
                    Constants.DEFAULT_DELAY
                )
            }
        ))
    }

    fun onChange(item: CalorieItem) {
        item as CalorieItemUIModel
        questions[item.type] = item
    }

    fun calculate() {
        try {
            val age = this.age.get()?.toInt() ?: throw NumberFormatException()
            val height = this.height.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()
            val weight = this.weight.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()

            if (age <= 0 || height <= 0 || weight <= 0) throw NumberFormatException()

            val goal =
                this.questions.getOrElse(CALORIE_ITEM_GOAL) { null }?.getSelectedOptionCoefficient()
                    ?: throw NumberFormatException()
            val activity = this.questions.getOrElse(CALORIE_ITEM_ACTIVITY) { null }
                ?.getSelectedOptionCoefficient() ?: throw NumberFormatException()

            val isMale = this.isMale.get()
            val calories = if (isMale) {

                (((10 * weight) + (6.25 * height) - (5 * age) + 5) * activity) - goal
            } else {
                val lactation =
                    this.questions.getOrElse(CALORIE_ITEM_LACTATION) { null }
                        ?.getSelectedOptionCoefficient()
                        ?: throw NumberFormatException()

                (((655 + (9.6 * weight) + (1.8 * height) - (4.7 * age)) * activity) * goal) + lactation
            }
            update(calories)
        } catch (e: NumberFormatException) {
            update(0.0)
        }
    }

    private fun update(calories: Double) {
        this.yourCalorie.set(calories.convertWithTwoDecimal())
        this.isCalculate.set(calories > 0)
    }

}