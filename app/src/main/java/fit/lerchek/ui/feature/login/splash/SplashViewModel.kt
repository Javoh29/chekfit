package fit.lerchek.ui.feature.login.splash

import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    fun shouldShowAuthFlow() = !userRepository.isLoggedIn()
}