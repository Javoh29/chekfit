package fit.lerchek.ui.feature.marathon.food

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.ItemFoodRecipeBinding

class FavoriteAdapter(
    private var data: List<RecipeUIModel>,
    private var callback: FoodCallback?
) : RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder>() {

    class FavoriteViewHolder(val dataBinding: ItemFoodRecipeBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder(
            ItemFoodRecipeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun update(position: Int) {
        data[position].isFavorite = !data[position].isFavorite
        notifyItemChanged(position)
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        val recipe = data[position]
        holder.apply {
            holder.dataBinding.model = recipe
            Glide.with(holder.dataBinding.imgRecipe.context).load(recipe.imageUrl)
                .into(holder.dataBinding.imgRecipe)
            holder.dataBinding.btnReplace.setOnClickListener {
                callback?.onReplaceRecipe(recipe)
            }
            holder.dataBinding.btnFavorite.setOnClickListener {
                callback?.onFavToggleClick(recipe.isFavorite, recipe.id, position)
                data[position].isFavorite = !recipe.isFavorite
                notifyItemChanged(position)
            }
            holder.dataBinding.cardRecipe.setOnClickListener {
                callback?.onSelectRecipe(recipe)
            }
        }
        holder.dataBinding.executePendingBindings()
    }
}

@BindingAdapter("favorite_items", "food_adapter_callback")
fun setupFavoriteItemsAdapter(
    list: RecyclerView,
    items: List<RecipeUIModel>?,
    callback: FoodCallback?
) {
    items?.let {
        list.adapter = FavoriteAdapter(it, callback)
    }
}