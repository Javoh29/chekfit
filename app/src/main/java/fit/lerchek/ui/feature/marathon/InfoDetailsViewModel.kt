package fit.lerchek.ui.feature.marathon

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class InfoDetailsViewModel @Inject constructor() : BaseViewModel() {

    private val mContent = MutableLiveData<String>()

    val content: LiveData<String>
        get() = mContent

    init {
        isProgress.set(true)
    }

    fun setupContent(content: String) {
        mContent.postValue(content)
        isProgress.set(false)
    }

}