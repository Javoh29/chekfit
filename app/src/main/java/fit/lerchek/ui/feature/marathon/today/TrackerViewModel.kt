@file:Suppress("DEPRECATION")

package fit.lerchek.ui.feature.marathon.today

import android.annotation.SuppressLint
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.data.*
import com.google.android.gms.fitness.request.DataDeleteRequest
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.BuildConfig
import fit.lerchek.data.api.message.marathon.FitDataRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerGoalRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class TrackerViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val dataManager: DataManager
) : BaseViewModel() {
    val trackerItems: ObservableArrayList<TrackerItem> = ObservableArrayList()
    val title = ObservableField<String>()
    val selectParam = ObservableField(1)
    var selectPeriod: String = "week"

    fun loadData(type: String, period: String) {
        selectPeriod = period
        when (period) {
            "week" -> selectParam.set(1)
            "month" -> selectParam.set(2)
            else -> selectParam.set(3)
        }
        processTask(
            userRepository.getDetailInfoTracker(type, period).subscribeWith(simpleSingleObserver {
                trackerItems.clearAndAddAll(it.list)
                isProgress.set(false)
            })
        )
    }

    fun changeCountTracker(
        request: UpdateTrackerRequest,
        type: String,
        googleApiClient: GoogleApiClient?
    ) {
        if (googleApiClient != null && getSwitchState())
            deleteOrWriteWater(request, googleApiClient, type)
        else sendChangeCountTracker(request, type)
    }

    @SuppressLint("SimpleDateFormat")
    private fun deleteOrWriteWater(
        request: UpdateTrackerRequest,
        googleApiClient: GoogleApiClient,
        type: String
    ) {
        processTask(
            Single.create<Boolean> {
                try {
                    val calendar = Calendar.getInstance()
                    calendar.time =
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.date!! + " 23:59:00")!!
                    val endDate = calendar.time
                    calendar.set(Calendar.HOUR_OF_DAY, 0)
                    calendar.set(Calendar.MINUTE, 0)
                    calendar.set(Calendar.SECOND, 0)
                    calendar.set(Calendar.MILLISECOND, 0)
                    val startDate = calendar.time

                    val requestDelete = DataDeleteRequest.Builder()
                        .setTimeInterval(
                            startDate.time / 1000,
                            endDate.time / 1000,
                            TimeUnit.SECONDS
                        )
                        .addDataType(DataType.TYPE_HYDRATION)
                        .build()

                    Fitness.HistoryApi.deleteData(
                        googleApiClient,
                        requestDelete
                    ).setResultCallback { _ ->
                        val mDataSourceWater = DataSource.Builder()
                            .setAppPackageName(BuildConfig.APPLICATION_ID)
                            .setDataType(DataType.TYPE_HYDRATION)
                            .setStreamName("hydrationSource")
                            .setType(DataSource.TYPE_RAW)
                            .build()

                        val dataPoint: DataPoint = DataPoint.builder(mDataSourceWater)
                            .setTimestamp(
                                SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(
                                    request.date + " ${
                                        SimpleDateFormat(
                                            "HH:mm:ss"
                                        ).format(Date())
                                    }"
                                )!!.time / 1000,
                                TimeUnit.SECONDS
                            )
                            .setField(
                                Field.FIELD_VOLUME,
                                request.value / 1000f
                            )
                            .build()

                        val dataSet =
                            DataSet.builder(mDataSourceWater).addAll(arrayListOf(dataPoint)).build()

                        Fitness.HistoryApi.insertData(googleApiClient, dataSet)
                            .setResultCallback { _ ->
                                it.onSuccess(true)
                            }
                    }

                } catch (e: Exception) {
                    it.onError(e)
                }
            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(simpleSingleObserver {
                    sendChangeCountTracker(request, type)
                })
        )
    }

    private fun sendChangeCountTracker(request: UpdateTrackerRequest, type: String) {
        processTask(
            userRepository.changeCountTracker(request, type).subscribeWith(simpleSingleObserver {
                if (!it.success) onErrorListener?.onError(
                    code = it.code.toString(),
                    message = it.message ?: "",
                    goBack = false
                )
                isProgress.set(false)
            })
        )
    }

    fun changeGoalTracker(request: UpdateTrackerGoalRequest, type: String) {
        processTask(
            userRepository.changeGoalTracker(request, type).subscribeWith(simpleSingleObserver {
                if (!it.success) onErrorListener?.onError(
                    code = it.code.toString(),
                    message = it.message ?: "",
                    goBack = false
                )
                isProgress.set(false)
            })
        )
    }

    fun uploadGoogleFitData(request: FitDataRequest) {
        processTask(
            userRepository.setGoogleFitData(request).subscribeWith(simpleSingleObserver {
                if (!it.success) {
                    onErrorListener?.onError(
                        code = it.code.toString(),
                        message = it.message ?: "",
                        goBack = false
                    )
                    isProgress.set(false)
                } else loadData(request.tracker, selectPeriod)
            })
        )
    }

    fun setSwitchFitValue(value: Boolean) {
        dataManager.saveSwitchFit(value)
    }

    fun getSwitchState() = dataManager.getSwitchFit()
}