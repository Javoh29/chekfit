package fit.lerchek.ui.feature.marathon.profile.personal

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.toString
import fit.lerchek.databinding.FragmentUserPersonalInfoBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.MarathonActivity
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.viewBindings
import fit.lerchek.util.EventObserver
import java.util.*

@AndroidEntryPoint
class UserPersonalInfoFragment : BaseToolbarDependentFragment(R.layout.fragment_user_personal_info) {


    private val binding by viewBindings(FragmentUserPersonalInfoBinding::bind)
    private val viewModel by viewModels<PersonalInfoViewModel>()


    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Close
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this

        viewModel.success.observe(this, EventObserver { success ->
            mContext.closeExistingAlert()
            if (success) {
                mContext.apply {
                   ToastUtils.show(this , R.string.data_saved)
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        binding.save.setOnClickListener {
            viewModel.savePersonalData()
        }

        binding.selectDateOfBirth.setOnClickListener {
            val dobDialog = DatePickerDialog(it.context)
            dobDialog.setOnDateSetListener { view, year, month, dayOfMonth ->
                val calendar = Calendar.getInstance()
                calendar.set(year, month, dayOfMonth)
                viewModel.personalDetails.get()?.dateOfBirth?.set(calendar.time.toString("dd/MM/yyyy"))
            }
            dobDialog.show()
        }

        binding.familyState.setOnCheckedChangeListener { group, checkedId ->
            val viewItem : View = group.findViewById(checkedId) ?: return@setOnCheckedChangeListener
            try {
                Log.d("tag" , viewItem.tag?.toString()?:"null")
                viewModel.personalDetails.get()?.familyStatus?.set(viewItem.tag as Int)
            }catch (e : Exception){
                e.printStackTrace()
            }
        }
    }
}