package fit.lerchek.ui.feature.marathon.food

import fit.lerchek.data.domain.uimodel.marathon.*

interface FoodCallback {
    fun onShoppingCartClick()
    fun onFAQClick()
    fun onFavoriteClick()
    fun onSelectWeekClick(weeks: List<WeekUIModel>)
    fun onSelectNutritionPlanClick(plans: List<PlanUIModel>)
    fun onSelectDayClick(dayUIModel: DayUIModel)
    fun onSelectRecipe(recipeUIModel: RecipeUIModel)
    fun onReplaceRecipe(recipeUIModel: RecipeUIModel)
    fun onFavToggleClick(isDelete: Boolean, recipeId: Int, position: Int)
}