package fit.lerchek.ui.feature.marathon.menu

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentMenuBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class MenuFragment : BaseToolbarDependentFragment(R.layout.fragment_menu) {

    private val binding by viewBindings(FragmentMenuBinding::bind)
    private val viewModel by viewModels<MenuViewModel>()

    private var callback: MenuItemSelectListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (mContext is MenuItemSelectListener) {
            callback = mContext as MenuItemSelectListener
        }
    }

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Close
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        binding.callback = callback
    }
}