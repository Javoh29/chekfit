package fit.lerchek.ui.feature.marathon.today.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.ItemTodayRecipeBinding
import fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback

class TodayRecipesAdapter(
    private var recipes: List<RecipeUIModel>,
    private var callback: TodayCallback?
) : RecyclerView.Adapter<TodayRecipesAdapter.RecipeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        return RecipeViewHolder(
            ItemTodayRecipeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val recipe = recipes[position]
        holder.binding.model = recipe
        holder.binding.executePendingBindings()
        holder.bind(recipe, callback)
    }

    override fun getItemCount(): Int {
        return recipes.size
    }

    class RecipeViewHolder(val binding: ItemTodayRecipeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(recipe: RecipeUIModel, callback: TodayCallback?) {
            itemView.setOnClickListener {
                callback?.onRecipeClick(recipe)
            }
            binding.imgRecipe.apply {
                Glide.with(this.context).load(recipe.imageUrl).into(this)
            }
        }

    }

}


@BindingAdapter("today_recipes", "today_recipes_callback")
fun setupTodayMealsAdapter(
    list: RecyclerView,
    items: List<RecipeUIModel>?,
    callback: TodayCallback?
) {
    items?.let {
        list.layoutManager = GridLayoutManager(list.context, 2)
        list.adapter = TodayRecipesAdapter(it, callback)
    }
}

@BindingAdapter("mealId")
fun setMealName(textView: AppCompatTextView, mealId: Int?) {
    if (mealId != null && mealId>-1) {
        textView.visibility = View.VISIBLE
    } else {
        textView.visibility = View.GONE
    }
    mealId?.let {
        val context = textView.context

        textView.background.setTint(
            ContextCompat.getColor(
                context, when (it) {
                    1 -> R.color.meal_name_1
                    2 -> R.color.meal_name_2
                    3 -> R.color.meal_name_3
                    4 -> R.color.meal_name_4
                    5 -> R.color.meal_name_5
                    else -> R.color.status_grey
                }
            )
        )
    }
}