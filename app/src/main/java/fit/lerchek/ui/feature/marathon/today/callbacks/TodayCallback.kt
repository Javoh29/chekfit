package fit.lerchek.ui.feature.marathon.today.callbacks

import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.data.domain.uimodel.marathon.TodayVoteItem
import fit.lerchek.data.domain.uimodel.marathon.WorkoutUIModel

interface TodayCallback : TaskCallback {
    fun onTgChannelClick(channel: String)
    fun onTgGroupClick(group: String)
    fun onSaleOfferClick(link: String)
    fun onVoteClick(vote: TodayVoteItem)
    fun onActivityCalendarClick()
    fun onReferralLinkCopied(link: String)
    fun onReferralLinkShare(link: String)
    fun onRecipeClick(recipeUIModel: RecipeUIModel)
    fun onWorkoutClick(workoutUIModel: WorkoutUIModel)
    fun onWaitingClick()
    fun onRenewClick(link: String)
    fun onTrackerClick(type: String)
    fun changeCountTracker(request: UpdateTrackerRequest, type: String)
}