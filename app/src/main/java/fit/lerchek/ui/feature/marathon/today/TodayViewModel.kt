@file:Suppress("DEPRECATION")

package fit.lerchek.ui.feature.marathon.today

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.data.*
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.BuildConfig
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.MarathonDetailsUIModel
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.data.domain.uimodel.marathon.TodayItem
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.HashMap
import com.google.android.gms.fitness.request.DataDeleteRequest


@HiltViewModel
class TodayViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val dataManager: DataManager
) : BaseViewModel() {

    val currentDay = ObservableInt()
    val todayItems: ObservableArrayList<TodayItem> = ObservableArrayList()

    var deepLinkProcessed = false

    private val mBottomBarAvailable = MutableLiveData<Boolean>()

    val bottomBarAvailable: LiveData<Boolean>
        get() = mBottomBarAvailable

    fun loadMarathonDetails(marathonId: Int, mapFit: HashMap<String, Int>?) {
        processTask(
            userRepository.loadMarathonDetails(marathonId, mapFit)
                .subscribeWith(simpleSingleObserver<MarathonDetailsUIModel> {
                    todayItems.clearAndAddAll(it.todayItems)
                    mBottomBarAvailable.postValue(it.bottomNavigationVisible)
                    isProgress.set(false)
                })
        )
    }

    fun updateTaskCompletion(task: TaskUIModel, currentDay: Int) {
        processTask(userRepository.toggleTaskCompletion(task, currentDay).subscribe())
    }

    fun changeCountTracker(
        request: UpdateTrackerRequest,
        type: String,
        googleApiClient: GoogleApiClient?
    ) {
        if (googleApiClient != null && getSwitchState()) {
            deleteOrWriteWaterData(request, googleApiClient, type)
        } else sendChangeCountTracker(request, type)

    }

    private fun deleteOrWriteWaterData(
        request: UpdateTrackerRequest,
        googleApiClient: GoogleApiClient, type: String
    ) {
        processTask(
            Single.create<Boolean> {
                try {
                    val calendar = Calendar.getInstance()
                    val endDate = calendar.time
                    calendar.set(Calendar.HOUR_OF_DAY, 0)
                    calendar.set(Calendar.MINUTE, 0)
                    calendar.set(Calendar.SECOND, 0)
                    calendar.set(Calendar.MILLISECOND, 0)
                    val startDate = calendar.time

                    val requestDelete = DataDeleteRequest.Builder()
                        .setTimeInterval(
                            startDate.time / 1000,
                            endDate.time / 1000,
                            TimeUnit.SECONDS
                        )
                        .addDataType(DataType.TYPE_HYDRATION)
                        .build()

                    Fitness.HistoryApi.deleteData(
                        googleApiClient,
                        requestDelete
                    ).setResultCallback { _ ->
                        val mDataSourceWater = DataSource.Builder()
                            .setAppPackageName(BuildConfig.APPLICATION_ID)
                            .setDataType(DataType.TYPE_HYDRATION)
                            .setStreamName("hydrationSource")
                            .setType(DataSource.TYPE_RAW)
                            .build()

                        val dataPoint: DataPoint = DataPoint.builder(mDataSourceWater)
                            .setTimestamp(
                                Date().time / 1000,
                                TimeUnit.SECONDS
                            )
                            .setField(
                                Field.FIELD_VOLUME,
                                request.value / 1000f
                            )
                            .build()

                        val dataSet =
                            DataSet.builder(mDataSourceWater).addAll(arrayListOf(dataPoint))
                                .build()

                        Fitness.HistoryApi.insertData(googleApiClient, dataSet)
                            .setResultCallback { _ ->
                                it.onSuccess(true)
                            }
                    }
                } catch (e: Exception) {
                    it.onError(e)
                }
            }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(simpleSingleObserver {
                    sendChangeCountTracker(request, type)
                })
        )
    }

    private fun sendChangeCountTracker(request: UpdateTrackerRequest, type: String) {
        processTask(
            userRepository.changeCountTracker(request, type).subscribeWith(simpleSingleObserver {
                if (!it.success) onErrorListener?.onError(
                    code = it.code.toString(),
                    message = it.message ?: "",
                    goBack = false
                )
                isProgress.set(false)
            })
        )
    }

    fun getSwitchState() = dataManager.getSwitchFit()
}