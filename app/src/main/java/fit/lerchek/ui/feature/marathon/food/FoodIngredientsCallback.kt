package fit.lerchek.ui.feature.marathon.food

import fit.lerchek.data.domain.uimodel.marathon.*

interface FoodIngredientsCallback {
    fun updateIngredientState(ingredient: TaskUIModel)
    fun onSelectWeekClick(weeks: List<WeekUIModel>)
}