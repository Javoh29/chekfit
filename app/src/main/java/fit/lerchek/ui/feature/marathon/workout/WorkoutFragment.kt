package fit.lerchek.ui.feature.marathon.workout

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.api.model.VimeoModel
import fit.lerchek.data.domain.uimodel.marathon.DayUIModel
import fit.lerchek.data.domain.uimodel.marathon.PlanUIModel
import fit.lerchek.data.domain.uimodel.marathon.WeekUIModel
import fit.lerchek.databinding.FragmentWorkoutBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.FaqFragment
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class WorkoutFragment : BaseToolbarDependentFragment(R.layout.fragment_workout), WorkoutCallback {
    private val binding by viewBindings(FragmentWorkoutBinding::bind)
    private val viewModel by viewModels<WorkoutViewModel>()
    private val args: WorkoutFragmentArgs by navArgs()


    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override var isBottomNavigationVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadWorkoutDetails(needSync = true, marathonId = args.marathonId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
    }

    override fun onFAQClick() {
        navigateTo(
            WorkoutFragmentDirections.actionNavigationWorkoutToNavigationFaq(
                FaqFragment.TAG_ALIAS_WORKOUT
            )
        )
    }

    override fun onSelectWeekClick(weeks: List<WeekUIModel>) {
        mContext.showSimpleOptionsDialog(weeks.map { it.name }.toTypedArray()) {
            viewModel.selectWeek(weeks[it])
        }
    }

    override fun onSelectLevelClick(levels: List<PlanUIModel>) {
        mContext.showSimpleOptionsDialog(levels.map { it.name }.toTypedArray()) {
            viewModel.selectLevel(levels[it])
        }
    }

    override fun onSelectDayClick(dayUIModel: DayUIModel) {
        viewModel.selectDay(dayUIModel)
    }

    override fun getVimeoData(videoId: String, h: String?, call: (vimeoModel: VimeoModel) -> Unit) {
        viewModel.getVimeoData(videoId, h, call)
    }

    override fun getMyLifecycle() = activity?.lifecycle ?: lifecycle

    override fun onBack() {
        navigateUp()
    }
}