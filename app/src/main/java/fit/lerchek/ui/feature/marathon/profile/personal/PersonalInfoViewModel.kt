package fit.lerchek.ui.feature.marathon.profile.personal

import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject


@HiltViewModel
class PersonalInfoViewModel @Inject constructor( private val dataManager: DataManager,
                                                 private val userRepository: UserRepository) : BaseViewModel() {
    init {
        loadUserProfile()
    }

    val personalDetails: ObservableField<ProfilePersonalInfoUIModel> = ObservableField()
    val success = MutableLiveData<Event<Boolean>>()

    private fun loadUserProfile(){
        processTask(userRepository.loadProfilePersonalInfo().subscribeWith(object : DisposableSingleObserver<ProfilePersonalInfoUIModel>() {
            override fun onError(e: Throwable) {
                isProgress.set(false)
            }

            override fun onSuccess(profileDetailsUIModel: ProfilePersonalInfoUIModel) {
                profileDetailsUIModel.familyStatusParams = dataManager.getProfileParam()
                personalDetails.set(profileDetailsUIModel)
                isProgress.set(false)
            }
        }))
    }

    fun savePersonalData() {
        if (personalDetails.get() == null)return

        if(personalDetails.get()?.phone?.get().isNullOrEmpty()){
            onErrorListener?.onError(message = "Номер телефона обязателен", goBack = false)
            return
        }

        processTask(userRepository.updateProfileDetails(personalDetails.get()!!.prepareRequestForUpdate()).subscribeWith(object : DisposableSingleObserver<ProfilePersonalInfoUIModel>() {
            override fun onStart() {
                super.onStart()
                isProgress.set(true)
            }

            override fun onError(e: Throwable) {
                isProgress.set(false)
            }

            override fun onSuccess(profileDetailsUIModel: ProfilePersonalInfoUIModel) {
                personalDetails.set(profileDetailsUIModel)
                isProgress.set(false)
                success.value = Event(true)
            }
        }))


    }
}