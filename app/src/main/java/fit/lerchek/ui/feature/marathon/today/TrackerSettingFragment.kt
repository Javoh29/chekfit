package fit.lerchek.ui.feature.marathon.today

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.api.message.marathon.UpdateTrackerGoalRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.databinding.FragmentTrackerSettingBinding
import fit.lerchek.ui.base.BaseToolbarTitleFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.today.callbacks.TrackerSettingCallback
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class TrackerSettingFragment : BaseToolbarTitleFragment(R.layout.fragment_tracker_setting),
    TrackerSettingCallback {
    private val binding by viewBindings(FragmentTrackerSettingBinding::bind)
    private val viewModel by viewModels<TrackerSettingViewModel>()
    private var settingType: String = ""

    override fun getToolbarContent() =
        if (settingType.isNotEmpty() && settingType.split("_").last() == "set") setOf(
            ToolbarContentItem.Logo,
            ToolbarContentItem.Dropdown,
            ToolbarContentItem.Profile,
            ToolbarContentItem.Menu
        ) else setOf(
            ToolbarContentItem.Title,
            ToolbarContentItem.Close
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        settingType = arguments?.getString("settingType", "water_set")!!
        viewModel.onErrorListener = this
        viewModel.loadData(
            settingType,
            arguments?.getString("settingDate", "")!!,
            arguments?.getInt("settingDimension", 0)!!,
            arguments?.getInt("settingGoal", 0)!!,
            arguments?.getInt("settingValue", 0)!!
        )
    }

    override fun getToolbarTitle(): String {
        return if (settingType == "steps_add") getString(R.string.text_your_progress_active) else getString(
            R.string.text_time_sleep
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        viewModel.onBack.observe(viewLifecycleOwner) {
            navigateUp()
        }
    }

    override fun changeCountTracker(request: UpdateTrackerRequest, type: String) {
        if (viewModel.getSwitchState()) {
            if (type == "steps_add")
                viewModel.deleteOrWriteStepsData(
                    request, Fitness.getHistoryClient(
                        requireActivity(), GoogleSignIn.getAccountForExtension(
                            requireContext(), FitnessOptions.builder()
                                .addDataType(
                                    DataType.TYPE_STEP_COUNT_DELTA,
                                    FitnessOptions.ACCESS_WRITE
                                )
                                .build()
                        )
                    ), type
                ) else viewModel.deleteOrWriteSleepData(
                request, Fitness.getSessionsClient(
                    requireActivity(), GoogleSignIn.getAccountForExtension(
                        requireContext(), FitnessOptions.builder()
                            .accessSleepSessions(FitnessOptions.ACCESS_WRITE)
                            .addDataType(DataType.TYPE_SLEEP_SEGMENT, FitnessOptions.ACCESS_WRITE)
                            .build()
                    )
                ), Fitness.getHistoryClient(
                    requireActivity(), GoogleSignIn.getAccountForExtension(
                        requireContext(), FitnessOptions.builder()
                            .addDataType(
                                DataType.TYPE_SLEEP_SEGMENT,
                                FitnessOptions.ACCESS_READ
                            )
                            .build()
                    )
                ), type
            )
        } else viewModel.changeCountTracker(request, type)
    }

    override fun changeGoalTracker(request: UpdateTrackerGoalRequest, type: String) {
        viewModel.changeGoalTracker(request, type)
    }

}