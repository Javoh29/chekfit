package fit.lerchek.ui.feature.marathon.more.calories

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.databinding.*

class CalorieOptionsAdapter(
    private var data: List<CalorieItemOptionUIModel>,
    private var onChange: (CalorieItemOptionUIModel) -> Unit?
) : RecyclerView.Adapter<CalorieOptionsAdapter.ViewHolder>() {


    class ViewHolder(val dataBinding: ItemCalorieOptionItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            ItemCalorieOptionItemBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val option = data[position]
        holder.dataBinding.apply {
            model = option
            ivCheck.apply {
                if (option.selected.get()) {
                    setBackgroundResource(R.drawable.calendar_completed_day_bg)
                    setImageResource(R.drawable.ic_check_mark)
                    imageTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(context, R.color.black)
                    )
                } else {
                    setBackgroundResource(R.drawable.calendar_enabled_day_bg)
                    setImageDrawable(null)
                }
            }

            btnOption.setOnClickListener {
                if (option.selected.get().not()) {
                    val selectedIndex = data.indexOfFirst { it.selected.get() }
                    if (selectedIndex != -1) {
                        data[selectedIndex].selected.set(false)
                        notifyItemChanged(selectedIndex)
                    }
                    option.selected.set(true)
                    notifyItemChanged(position)
                    onChange(option)
                }
            }
            executePendingBindings()
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}