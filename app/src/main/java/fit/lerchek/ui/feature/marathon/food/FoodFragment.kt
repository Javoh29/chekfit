package fit.lerchek.ui.feature.marathon.food

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.marathon.DayUIModel
import fit.lerchek.data.domain.uimodel.marathon.PlanUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.data.domain.uimodel.marathon.WeekUIModel
import fit.lerchek.databinding.FragmentFoodBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.FaqFragment
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.viewBindings


@AndroidEntryPoint
class FoodFragment : BaseToolbarDependentFragment(R.layout.fragment_food),
    FoodCallback {

    private val binding by viewBindings(FragmentFoodBinding::bind)
    private val viewModel by viewModels<FoodViewModel>()

    companion object {
        val onUpdate: MutableLiveData<Boolean> = MutableLiveData()
    }

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override var isBottomNavigationVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadFoodDetails(needSync = true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        viewModel.errorItemPosition.observe(viewLifecycleOwner, {
            if (it != null) {
                (binding.rvFood.adapter as FoodAdapter).update(it)
                ToastUtils.show(requireContext(), Constants.UNKNOWN_ERROR)
            }
        })
        onUpdate.observe(viewLifecycleOwner, {
            if (it != null && it) {
                viewModel.loadFoodDetails(needSync = true)
                onUpdate.value = false
            }
        })
    }

    override fun onShoppingCartClick() {
        navigateTo(
            FoodFragmentDirections.actionNavigationFoodToNavigationFoodIngredients()
        )
    }

    override fun onFAQClick() {
        navigateTo(
            FoodFragmentDirections.actionNavigationFoodToNavigationFaq(
                FaqFragment.TAG_ALIAS_FOOD
            )
        )
    }

    override fun onFavoriteClick() {
        navigateTo(FoodFragmentDirections.actionNavigationFoodToNavigationFavorite())
    }

    override fun onSelectWeekClick(weeks: List<WeekUIModel>) {
        mContext.showSimpleOptionsDialog(weeks.map { it.name }.toTypedArray()) {
            viewModel.selectWeek(weeks[it])
        }
    }

    override fun onSelectNutritionPlanClick(plans: List<PlanUIModel>) {
        mContext.showSimpleOptionsDialog(plans.map { it.name }.toTypedArray()) {
            viewModel.selectNutritionPlan(plans[it])
        }
    }

    override fun onSelectDayClick(dayUIModel: DayUIModel) {
        viewModel.selectDay(dayUIModel)
    }

    override fun onSelectRecipe(recipeUIModel: RecipeUIModel) {
        navigateTo(
            FoodFragmentDirections.actionNavigationFoodToNavigationRecipeDetails(
                false,
                recipeUIModel.id,
                recipeUIModel.baseId ?: -1,
                true,
                false,
                recipeUIModel.dayNumber ?: -1,
            )
        )
    }

    override fun onReplaceRecipe(recipeUIModel: RecipeUIModel) {
        navigateTo(
            FoodFragmentDirections.actionNavigationFoodToNavigationRecipeReplacement(
                false,
                recipeUIModel.id,
                recipeUIModel.baseId ?: -1,
                recipeUIModel.planId ?: -1,
                recipeUIModel.dayNumber ?: -1
            )
        )
    }

    override fun onFavToggleClick(isDelete: Boolean, recipeId: Int, position: Int) {
        viewModel.favoriteChange(isDelete, recipeId, position)
    }
}