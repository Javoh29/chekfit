package fit.lerchek.ui.feature.marathon.today

import android.annotation.SuppressLint
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.fitness.data.*
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.BuildConfig
import fit.lerchek.data.api.message.marathon.UpdateTrackerGoalRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.FitnessActivities
import com.google.android.gms.fitness.HistoryClient
import com.google.android.gms.fitness.SessionsClient
import com.google.android.gms.fitness.request.DataDeleteRequest
import com.google.android.gms.fitness.request.DataUpdateRequest
import com.google.android.gms.fitness.request.SessionInsertRequest
import java.text.SimpleDateFormat


@Suppress("DEPRECATION")
@HiltViewModel
@SuppressLint("SimpleDateFormat")
class TrackerSettingViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val dataManager: DataManager
) : BaseViewModel() {
    val settingItems: ObservableArrayList<TrackerSettingItem> = ObservableArrayList()
    val onBack = MutableLiveData<Boolean>()

    fun loadData(type: String, date: String, dimension: Int, goal: Int, value: Int) {
        isProgress.set(false)
        when (type) {
            "water_set" -> {
                val t =
                    if (getSwitchState()) 1 else if (dimension == 250) 0 else if (dimension > 1) 2 else 1
                settingItems.add(
                    WaterTrackerSetItem(
                        t,
                        dimension.toString(),
                        goal.toString(),
                        !getSwitchState()
                    )
                )
            }
            "steps_add" -> {
                settingItems.add(
                    StepsTrackerAddItem(
                        ObservableField(""),
                        date
                    )
                )
            }
            "steps_set" -> {
                settingItems.add(
                    StepsTrackerSetItem(
                        goal.toString()
                    )
                )
            }
            "sleep_add" -> {
                val h = value / 60
                val m = value % 60
                settingItems.add(
                    SleepTrackerAddItem(
                        h.toString(),
                        m.toString(),
                        date
                    )
                )
            }
            "sleep_set" -> {
                val h = goal / 60
                val m = goal % 60
                settingItems.add(
                    SleepTrackerSetItem(
                        h.toString(),
                        m.toString()
                    )
                )
            }
        }
    }

    fun changeCountTracker(
        request: UpdateTrackerRequest,
        type: String
    ) {
        processTask(
            userRepository.changeCountTracker(request, type).subscribeWith(simpleSingleObserver {
                if (!it.success) onErrorListener?.onError(
                    code = it.code.toString(),
                    message = it.message ?: "",
                    goBack = false
                ) else onBack.value = true
                isProgress.set(false)
            })
        )
    }

    fun deleteOrWriteStepsData(
        request: UpdateTrackerRequest,
        historyClient: HistoryClient?,
        type: String
    ) {
        val calendar = Calendar.getInstance()
        calendar.time =
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.date!! + " ${SimpleDateFormat("HH:mm:ss").format(Date())}")!!
        val endDate = calendar.time
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val startDate = calendar.time
        val dataSource = DataSource.Builder()
            .setAppPackageName(BuildConfig.APPLICATION_ID)
            .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
            .setStreamName("step count")
            .setType(DataSource.TYPE_RAW)
            .build()

        val dataPoint =
            DataPoint.builder(dataSource)
                .setField(Field.FIELD_STEPS, request.value)
                .setTimeInterval(
                    startDate.time,
                    endDate.time,
                    TimeUnit.MILLISECONDS
                )
                .build()

        val dataSet = DataSet.builder(dataSource)
            .add(dataPoint)
            .build()

        val requestUpdate = DataUpdateRequest.Builder()
            .setDataSet(dataSet)
            .setTimeInterval(
                startDate.time / 1000,
                endDate.time / 1000,
                TimeUnit.SECONDS
            )
            .build()

        historyClient!!.updateData(requestUpdate)
            .addOnSuccessListener {
                changeCountTracker(request, type)
            }
            .addOnFailureListener { e ->
                onErrorListener?.onError(
                    code = "111",
                    message = e.message ?: "Unknown error",
                    goBack = false
                )
            }
    }

    fun deleteOrWriteSleepData(
        request: UpdateTrackerRequest,
        sessionsClient: SessionsClient?,
        historyClient: HistoryClient?,
        type: String
    ) {
        try {
            val calendar = Calendar.getInstance()
            calendar.time =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.date!! + " 23:59:59")!!
            val endTime = calendar.time
            calendar.time =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.date + " 00:00:00")!!

            val requestDelete = DataDeleteRequest.Builder()
                .setTimeInterval(
                    calendar.time.time / 1000,
                    endTime.time / 1000,
                    TimeUnit.SECONDS
                )
                .addDataType(DataType.TYPE_SLEEP_SEGMENT)
                .build()

            historyClient!!.deleteData(requestDelete).addOnSuccessListener {
                calendar.time =
                    SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.date + " 00:00:00")!!
                val startDate = calendar.time
                val h = request.value / 60
                val m = request.value % 60
                calendar.add(Calendar.HOUR_OF_DAY, h)
                calendar.add(Calendar.MINUTE, m)
                val endDate = calendar.time

                val dataSource: DataSource = DataSource.Builder()
                    .setType(DataSource.TYPE_RAW)
                    .setDataType(DataType.TYPE_SLEEP_SEGMENT)
                    .setAppPackageName(BuildConfig.APPLICATION_ID)
                    .build()

                val dataset = DataSet.builder(dataSource).build()

                dataset.add(
                    DataPoint.builder(dataSource)
                        .setTimeInterval(
                            startDate.time / 1000,
                            endDate.time / 1000,
                            TimeUnit.SECONDS
                        )
                        .setField(Field.FIELD_SLEEP_SEGMENT_TYPE, request.value).build()
                )

                val session: Session = Session.Builder()
                    .setName("sessionName")
                    .setIdentifier("identifier")
                    .setDescription("description")
                    .setStartTime(
                        startDate.time / 1000,
                        TimeUnit.SECONDS
                    )
                    .setEndTime(
                        endDate.time / 1000,
                        TimeUnit.SECONDS
                    )
                    .setActivity(FitnessActivities.SLEEP)
                    .build()

                val requestSession = SessionInsertRequest.Builder()
                    .setSession(session)
                    .addDataSet(dataset)
                    .build()

                sessionsClient!!.insertSession(requestSession).addOnSuccessListener {
                    changeCountTracker(request, type)
                }.addOnFailureListener { e ->
                    onErrorListener?.onError(
                        code = "111",
                        message = e.message ?: "Unknown error",
                        goBack = false
                    )
                }
            }.addOnFailureListener { e ->
                onErrorListener?.onError(
                    code = "111",
                    message = e.message ?: "Unknown error",
                    goBack = false
                )
            }
        } catch (e: Exception) {
            onErrorListener?.onError(
                code = "111",
                message = e.message ?: "Unknown error",
                goBack = false
            )
        }
    }

    fun changeGoalTracker(request: UpdateTrackerGoalRequest, type: String) {
        processTask(
            userRepository.changeGoalTracker(request, type).subscribeWith(simpleSingleObserver {
                if (!it.success) onErrorListener?.onError(
                    code = it.code.toString(),
                    message = it.message ?: "",
                    goBack = false
                ) else onBack.value = true
                isProgress.set(false)
            })
        )
    }

    fun getSwitchState() = dataManager.getSwitchFit()
}