package fit.lerchek.ui.feature.marathon.workout

import android.annotation.SuppressLint
import androidx.databinding.ObservableArrayList
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.api.model.VimeoModel
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

@HiltViewModel
class WorkoutViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var workoutItems: ObservableArrayList<WorkoutAdapter.WorkoutItem> = ObservableArrayList()

    private var workoutDetailsUIModel: WorkoutDetailsUIModel? = null

    fun loadWorkoutDetails(
        filterUIModel: WorkoutFilterUIModel? = null,
        needSync: Boolean = false,
        marathonId: Int? = null
    ) {
        processTask(
            userRepository.loadWorkoutsDetails(
                filterUIModel = filterUIModel,
                needApiSync = needSync,
                workoutDetailsUIModel = workoutDetailsUIModel,
                marathonId = if (marathonId == -1) null else marathonId
            )
                .subscribeWith(simpleSingleObserver<WorkoutDetailsUIModel> {
                    this@WorkoutViewModel.workoutDetailsUIModel = it
                    workoutItems.clearAndAddAll(it.displayItems)
                    isProgress.set(false)
                })
        )
    }

    fun selectLevel(plan: PlanUIModel) {
        getCurrentFilter()?.let {
            it.selectLevel(plan.id)
            loadWorkoutDetails(it, true)
        }
    }

    fun selectWeek(week: WeekUIModel) {
        getCurrentFilter()?.let {
            it.selectWeek(week.id)
            loadWorkoutDetails(it)
        }
    }

    fun selectDay(day: DayUIModel) {
        getCurrentFilter()?.let {
            it.selectDay(day.dayNumber)
            loadWorkoutDetails(it)
        }
    }

    @SuppressLint("CheckResult")
    fun getVimeoData(videoId: String, h: String?, call: (vimeoModel: VimeoModel) -> Unit) {
        userRepository.getVimeoConfig(videoId, h).subscribeBy {
            call.invoke(it)
        }
    }

    private fun getCurrentFilter(): WorkoutFilterUIModel? {
        return workoutDetailsUIModel?.displayItems?.firstOrNull {
            it is WorkoutFilterUIModel
        } as WorkoutFilterUIModel?
    }

}