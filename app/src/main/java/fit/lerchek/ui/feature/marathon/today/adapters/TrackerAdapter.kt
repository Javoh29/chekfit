package fit.lerchek.ui.feature.marathon.today.adapters

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.databinding.*
import fit.lerchek.ui.feature.marathon.today.callbacks.TrackerCallBack
import java.util.ArrayList

class TrackerAdapter(
    private var data: ArrayList<TrackerItem>,
    private var callback: TrackerCallBack
) :
    RecyclerView.Adapter<TrackerAdapter.TrackerViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE_LINE_TRACKER = 0
        const val ITEM_VIEW_TYPE_WATER_TRACKER = 1
        const val ITEM_VIEW_TYPE_STEPS_TRACKER = 2
        const val ITEM_VIEW_TYPE_SLEEP_TRACKER = 3
        const val ITEM_VIEW_TYPE_SWITCH_FIT = 4
        const val ITEM_VIEW_TYPE_SETTING_TRACKER = 5
        const val ITEM_VIEW_TYPE_CHANGE_BTN = 6
    }

    private var selectBarIndex: Int = 0
    private var isSelect: Boolean = false

    sealed class TrackerViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class LineTrackerViewHolder(val itemLineTrackerBinding: ItemLineTrackerBinding) :
        TrackerViewHolder(itemLineTrackerBinding)

    class WaterTrackerViewHolder(val itemWaterTrackerBinding: ItemWaterTrackerBinding) :
        TrackerViewHolder(itemWaterTrackerBinding)

    class StepsTrackerViewHolder(val itemStepsTrackerBinding: ItemStepsTrackerBinding) :
        TrackerViewHolder(itemStepsTrackerBinding)

    class SleepTrackerViewHolder(val itemSleepTrackerBinding: ItemSleepTrackerBinding) :
        TrackerViewHolder(itemSleepTrackerBinding)

    class SettingTrackerViewHolder(val itemSettingTrackerBinding: ItemSettingTrackerBinding) :
        TrackerViewHolder(itemSettingTrackerBinding)

    class ChangeBtnViewHolder(val itemPurpleBtnBinding: ItemPurpleBtnBinding) :
        TrackerViewHolder(itemPurpleBtnBinding)

    class  SwitchFitViewHolder(val itemSwitchFitBinding: ItemSwitchFitBinding) :
        TrackerViewHolder(itemSwitchFitBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_LINE_TRACKER -> LineTrackerViewHolder(
                ItemLineTrackerBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_WATER_TRACKER -> WaterTrackerViewHolder(
                ItemWaterTrackerBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_STEPS_TRACKER -> StepsTrackerViewHolder(
                ItemStepsTrackerBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SLEEP_TRACKER -> SleepTrackerViewHolder(
                ItemSleepTrackerBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SETTING_TRACKER -> SettingTrackerViewHolder(
                ItemSettingTrackerBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_CHANGE_BTN -> ChangeBtnViewHolder(
                ItemPurpleBtnBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SWITCH_FIT -> SwitchFitViewHolder(
                ItemSwitchFitBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].type.itemViewType
    }

    @SuppressLint("SetTextI18n", "NotifyDataSetChanged")
    override fun onBindViewHolder(
        holder: TrackerViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        when (holder) {
            is LineTrackerViewHolder -> {
                holder.itemLineTrackerBinding.callback = callback
                val model = (data[position] as LineTrackerItem)
                holder.itemLineTrackerBinding.let { binding ->
                    var maxGoal: Int = model.goal * 2
                    model.listData.forEach {
                        if (it.barValue >= maxGoal) {
                            maxGoal = it.barValue
                        }
                    }
                    binding.tvTitleMain.text =
                        binding.root.context.getString(model.title)
                    if (!isSelect)
                        selectBarIndex =
                            if (model.listData.isEmpty()) 0 else model.listData.size - 1
                    binding.lineChart.setMaxValue(maxGoal)
                    if (callback.getTrackerType() == "dream") {
                        val hourMid: Int = model.goal / 60
                        val minutesMid: Int = model.goal % 60
                        val hourMax: Int = model.goal * 2 / 60
                        val minutesMax: Int = model.goal * 2 % 60
                        if (minutesMid > 9)
                            binding.tvMidValue.text = "$hourMid:$minutesMid"
                        else binding.tvMidValue.text = "$hourMid:0$minutesMid"
                        if (minutesMax > 9)
                            binding.tvMaxValue.text = "$hourMax:$minutesMax"
                        else binding.tvMaxValue.text = "$hourMax:0$minutesMax"
                    } else {
                        binding.tvMidValue.text = (maxGoal / 2).toString()
                        binding.tvMaxValue.text = maxGoal.toString()
                    }
                    binding.lineChart.setProgressDrawable(model.progressBarDrawable)
                    binding.lineChart.setProgressDrawable2(model.progressBarDrawable2)
                    binding.lineChart.setDataList(model.listData)
                    if (callback.getSelectPeriod() != "year") {
                        binding.lineChart.setGoalValue(model.goal)
                        binding.lineChart.setSelectBarIndex(selectBarIndex)
                        binding.lineChart.setIsBarCanBeClick(true)
                        binding.lineChart.setOnBarClickedListener { index ->
                            selectBarIndex = index
                            isSelect = true
                            data.forEachIndexed { i, trackerItem ->
                                when (callback.getTrackerType()) {
                                    "water" -> {
                                        if (trackerItem is WaterTrackerItem) {
                                            trackerItem.dimension = model.dimension
                                            trackerItem.goal = model.goal
                                            trackerItem.value = model.listData[index].barValue
                                            trackerItem.date = model.listData[index].date
                                            notifyItemChanged(i)
                                        }
                                    }
                                    "step" -> {
                                        if (trackerItem is StepsTrackerItem) {
                                            trackerItem.dimension = model.dimension
                                            trackerItem.goal = model.goal
                                            trackerItem.value = model.listData[index].barValue
                                            trackerItem.date = model.listData[index].date
                                            trackerItem.dateFormat = model.listData[index].dateFormat
                                            notifyItemChanged(i)
                                        }
                                    }
                                    else -> {
                                        if (trackerItem is SleepTrackerItem) {
                                            trackerItem.dimension = model.dimension
                                            trackerItem.goal = model.goal
                                            trackerItem.value = model.listData[index].barValue
                                            trackerItem.date = model.listData[index].date
                                            notifyItemChanged(i)
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        binding.lineChart.setGoalValue(maxGoal)
                        binding.lineChart.setIsBarCanBeClick(false)
                    }
                    binding.lineChart.build()
                    binding.hScrollView.postDelayed({
                        ObjectAnimator.ofInt(
                            binding.hScrollView,
                            "scrollX",
                            3500
                        ).setDuration(1500L).start()
                    }, 500)
                    binding.tvWeek.setOnClickListener {
                        callback.onClickPeriod("week")
                    }
                    binding.tvMonth.setOnClickListener {
                        callback.onClickPeriod("month")
                    }
                    binding.tvYear.setOnClickListener {
                        callback.onClickPeriod("year")
                    }
                }
            }

            is WaterTrackerViewHolder -> {
                val model = (data[position] as WaterTrackerItem)
                holder.itemWaterTrackerBinding.let { binding ->
                    binding.imgArrow.visibility = View.INVISIBLE
                    var maxGoal: Int = model.goal
                    if (model.value >= maxGoal) {
                        while(maxGoal < model.value) {
                            maxGoal *= 2
                        }
                    }
                    binding.waterProgress.max = maxGoal
                    if (model.value > model.goal) {
                        binding.tvMlStart.text =
                            "0 " + binding.root.context.getString(R.string.text_ml)
                        binding.tvMlEnd.text =
                            "${model.dimension * maxGoal} " + binding.root.context.getString(
                                R.string.text_ml
                            )
                        binding.tvInfo.visibility = View.VISIBLE
                        binding.tvMlStart.visibility = View.VISIBLE
                        binding.tvMlEnd.visibility = View.VISIBLE
                        binding.waterProgress.progress = model.goal
                        binding.waterProgress.progressSecond = model.value
                    } else {
                        binding.tvInfo.visibility = View.GONE
                        binding.tvMlStart.visibility = View.GONE
                        binding.tvMlEnd.visibility = View.GONE
                        binding.waterProgress.progress = model.value
                        binding.waterProgress.progressSecond = 0
                    }
                    if (model.dimension == 250) {
                        binding.waterProgress.setBottomTextVisible(true)
                        binding.waterProgress.bottomText =
                            "${model.dimension * model.value} " + binding.root.context.getString(R.string.text_ml)
                        binding.waterProgress.centerText =
                            "${model.value} " + binding.root.context.getString(R.string.text_glasses)
                        if (model.value in 5..20) {
                            binding.waterProgress.centerText += "ов"
                        } else
                            binding.waterProgress.centerText += when (model.value % 10) {
                                1 -> ""
                                2, 3, 4 -> "а"
                                else -> "ов"
                            }
                    } else {
                        binding.waterProgress.setBottomTextVisible(false)
                        binding.waterProgress.centerText = "${model.value * model.dimension} мл"
                    }
                    var isTimerPlus = false
                    val timerPlus = object : CountDownTimer(1500, 1500) {
                        override fun onTick(millisUntilFinished: Long) {
                        }

                        override fun onFinish() {
                            if (isTimerPlus) {
                                isTimerPlus = false
                                callback.changeCountTracker(
                                    UpdateTrackerRequest(model.date, model.value),
                                    "water_add"
                                )
                                val m = (data[0] as LineTrackerItem)
                                m.listData.forEach {
                                    if (it.date == model.date) {
                                        it.barValue = model.value
                                    }
                                }
                                notifyItemChanged(0)
                            }
                        }
                    }
                    binding.imgPlus.setOnClickListener {
                        if (model.dimension == 1) {
                            model.value += 50
                        } else {
                            model.value++
                        }
                        var maxG: Int = model.goal
                        if (model.value >= maxG) {
                            while(maxG < model.value) {
                                maxG *= 2
                            }
                        }
                        binding.waterProgress.max = maxG
                        if (model.value > model.goal) {
                            binding.tvMlStart.text =
                                "0 " + binding.root.context.getString(R.string.text_ml)
                            binding.tvMlEnd.text =
                                "${model.dimension * maxGoal} " + binding.root.context.getString(
                                    R.string.text_ml
                                )
                            binding.tvInfo.visibility = View.VISIBLE
                            binding.tvMlStart.visibility = View.VISIBLE
                            binding.tvMlEnd.visibility = View.VISIBLE
                            binding.waterProgress.setBothProgress(model.goal, model.value)
                        } else {
                            binding.tvInfo.visibility = View.GONE
                            binding.tvMlStart.visibility = View.GONE
                            binding.tvMlEnd.visibility = View.GONE
                            binding.waterProgress.setBothProgress(model.value, 0)
                        }
                        if (model.dimension == 250) {
                            binding.waterProgress.setBottomTextVisible(true)
                            binding.waterProgress.bottomText =
                                "${model.dimension * model.value} " + binding.root.context.getString(R.string.text_ml)
                            binding.waterProgress.centerText =
                                "${model.value} " + binding.root.context.getString(R.string.text_glasses)
                            if (model.value in 5..20) {
                                binding.waterProgress.centerText += "ов"
                            } else
                                binding.waterProgress.centerText += when (model.value % 10) {
                                    1 -> ""
                                    2, 3, 4 -> "а"
                                    else -> "ов"
                                }
                        } else {
                            binding.waterProgress.setBottomTextVisible(false)
                            binding.waterProgress.centerText = "${model.value * model.dimension} мл"
                        }
                        isTimerPlus = if (isTimerPlus) {
                            timerPlus.cancel()
                            timerPlus.start()
                            true
                        } else {
                            timerPlus.start()
                            true
                        }
                    }
                    var isTimerMinus = false
                    val timerMinus = object : CountDownTimer(1500, 1500) {
                        override fun onTick(millisUntilFinished: Long) {
                        }

                        override fun onFinish() {
                            if (isTimerMinus) {
                                isTimerMinus = false
                                callback.changeCountTracker(
                                    UpdateTrackerRequest(model.date, model.value),
                                    "water_delete"
                                )
                                val m = (data[0] as LineTrackerItem)
                                m.listData.forEach {
                                    if (it.date == model.date) {
                                        it.barValue = model.value
                                    }
                                }
                                notifyItemChanged(0)
                            }
                        }
                    }
                    binding.imgMinus.setOnClickListener {
                        if (model.value > 0) {
                            if (model.dimension == 1) {
                                model.value -= 50
                            } else {
                                model.value--
                            }
                            var maxG: Int = model.goal
                            if (model.value >= maxG) {
                                while(maxG < model.value) {
                                    maxG *= 2
                                }
                            }
                            binding.waterProgress.max = maxG
                            if (model.value > model.goal) {
                                binding.tvMlStart.text =
                                    "0 " + binding.root.context.getString(R.string.text_ml)
                                binding.tvMlEnd.text =
                                    "${model.dimension * maxGoal} " + binding.root.context.getString(
                                        R.string.text_ml
                                    )
                                binding.tvInfo.visibility = View.VISIBLE
                                binding.tvMlStart.visibility = View.VISIBLE
                                binding.tvMlEnd.visibility = View.VISIBLE
                                binding.waterProgress.setBothProgress(model.goal, model.value)
                            } else {
                                binding.tvInfo.visibility = View.GONE
                                binding.tvMlStart.visibility = View.GONE
                                binding.tvMlEnd.visibility = View.GONE
                                binding.waterProgress.setBothProgress(model.value, 0)
                            }
                            if (model.dimension == 250) {
                                binding.waterProgress.setBottomTextVisible(true)
                                binding.waterProgress.bottomText =
                                    "${model.dimension * model.value} " + binding.root.context.getString(R.string.text_ml)
                                binding.waterProgress.centerText =
                                    "${model.value} " + binding.root.context.getString(R.string.text_glasses)
                                if (model.value in 5..20) {
                                    binding.waterProgress.centerText += "ов"
                                } else
                                    binding.waterProgress.centerText += when (model.value % 10) {
                                        1 -> ""
                                        2, 3, 4 -> "а"
                                        else -> "ов"
                                    }
                            } else {
                                binding.waterProgress.setBottomTextVisible(false)
                                binding.waterProgress.centerText = "${model.value * model.dimension} мл"
                            }
                            if (isTimerMinus) {
                                isTimerMinus = false
                                timerMinus.cancel()
                                isTimerMinus = true
                                timerMinus.start()
                            } else {
                                isTimerMinus = true
                                timerMinus.start()
                            }
                        }
                    }
                }
            }

            is StepsTrackerViewHolder -> {
                val model = (data[position] as StepsTrackerItem)
                holder.itemStepsTrackerBinding.let { binding ->
                    binding.imgArrowSteps.visibility = View.INVISIBLE
                    binding.tvStepsDate.text = model.dateFormat
                    binding.tvStepsCount.text = model.value.toString()
                    binding.tvStepsStart.text = "0"
                    binding.tvConstSteps.text = model.goal.toString()
                    binding.root.postDelayed({
                        val maxWidth = binding.viewProgressMain.width
                        var maxGoal: Int = model.goal * 2
                        if (model.value >= maxGoal) {
                            while(maxGoal < model.value) {
                                maxGoal = (maxGoal * 1.3).toInt()
                            }
                        }
                        val stepsRate: Int
                        if (model.value > model.goal) {
                            stepsRate = (model.value * maxWidth) / maxGoal
                            binding.viewProgressOne.visibility = View.VISIBLE
                            binding.tvInfoSteps.visibility = View.VISIBLE
                            binding.viewLineProgress.visibility = View.VISIBLE
                            binding.viewProgressTwo.setBackgroundResource(R.drawable.rectangle_orange2)
                            binding.viewProgressOne.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = stepsRate
                            }
                            binding.viewProgressTwo.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = (model.goal * maxWidth) / maxGoal
                            }
                            binding.viewProgressDef.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = (model.goal * maxWidth) / maxGoal
                            }
                        } else {
                            stepsRate = (model.value * maxWidth) / model.goal
                            binding.viewProgressOne.visibility = View.INVISIBLE
                            binding.tvInfoSteps.visibility = View.GONE
                            binding.viewLineProgress.visibility = View.INVISIBLE
                            binding.viewProgressTwo.setBackgroundResource(R.drawable.rectangle_orange)
                            binding.viewProgressTwo.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = stepsRate
                            }
                            binding.viewProgressDef.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = maxWidth
                            }
                        }
                    }, 100)
                }
            }

            is SleepTrackerViewHolder -> {
                val model = (data[position] as SleepTrackerItem)
                holder.itemSleepTrackerBinding.let { binding ->
                    binding.imgArrowSleep.visibility = View.INVISIBLE
                    val hour: Int = model.value / 60
                    val minutes: Int = model.value % 60
                    binding.tvSleepHoursCount.text = hour.toString()
                    binding.tvSleepMinCount.text = minutes.toString()
                }
            }

            is SettingTrackerViewHolder -> {
                val model = (data[position] as SettingTrackerItem)
                holder.itemSettingTrackerBinding.let { binding ->
                    when (callback.getTrackerType()) {
                        "water" -> {
                            if (model.dimension == 250) {
                                binding.tvNormCount.text =
                                    "${model.goal} ${binding.root.context.getString(R.string.text_glasses)}"
                                if (model.goal in 5..20) {
                                    binding.tvNormCount.text = "${binding.tvNormCount.text}ов"
                                } else {
                                    val str = when (model.goal % 10) {
                                        1 -> ""
                                        2, 3, 4 -> "а"
                                        else -> "ов"
                                    }
                                    binding.tvNormCount.text = "${binding.tvNormCount.text}$str"
                                }
                            } else binding.tvNormCount.text = "${model.goal * model.dimension} мл"
                        }
                        "step" -> {
                            binding.tvNormCount.text =
                                "${model.goal} ${binding.root.context.getString(R.string.text_steps2)}"
                        }
                        else -> {
                            val hour: Int = model.goal / 60
                            val minutes: Int = model.goal % 60
                            binding.tvNormCount.text = "$hour ч. $minutes м."
                        }
                    }
                    binding.cardSettingTracker.setOnClickListener {
                        callback.onClickSetting(model.settingType, "", model.dimension, model.goal, 0)
                    }
                }
            }

            is ChangeBtnViewHolder -> {
                val model = (data[position] as PurpleBtnItem)
                holder.itemPurpleBtnBinding.let { binding ->
                    binding.save.text = binding.root.context.getString(R.string.progress_change)
                    binding.save.setOnClickListener {
                        callback.onClickSetting(model.settingType, model.date, 0, 0, (data[0] as LineTrackerItem).listData[selectBarIndex].barValue)
                    }
                }
            }

            is SwitchFitViewHolder -> {
                holder.itemSwitchFitBinding.let { binding ->
                    binding.fitSwitch.isChecked = callback.isCheckFit()
                    binding.fitSwitch.setOnCheckedChangeListener { _, _ ->
                        callback.changeSwitchFit(position)
                    }
                }
            }
        }
    }

    override fun getItemCount() = data.size
}

@BindingAdapter("tracker_items", "tracker_callback")
fun setupTrackerAdapter(
    list: RecyclerView,
    optionsData: List<TrackerItem>?,
    callback: TrackerCallBack
) {
    optionsData?.let {
        list.adapter = TrackerAdapter(optionsData as ArrayList<TrackerItem>, callback)
    }
}