package fit.lerchek.ui.feature.marathon

import androidx.lifecycle.Lifecycle

interface LifecycleHolder {
    fun getMyLifecycle(): Lifecycle
    fun onBack()
}