package fit.lerchek.ui.feature.marathon.profile.personal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.DynamicContentUIModel
import fit.lerchek.data.domain.uimodel.marathon.TestItem
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class TestViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    private var mContent = MutableLiveData<List<TestItem>>()

    val content: LiveData<List<TestItem>>
        get() = mContent

    fun loadTestData() {
        processTask(
            userRepository
                .loadTestContentDetails()
                .subscribeWith(simpleSingleObserver<List<TestItem>> {
                    mContent.postValue(it)
                    isProgress.set(false)
                })
        )
    }

    fun confirmResult() {
        processTask(
            userRepository
                .updateTestContentDetails(content = content.value ?: arrayListOf())
                .subscribeWith(simpleSingleObserver<List<TestItem>> {
                    mContent.postValue(it)
                    isProgress.set(false)
                })
        )
    }
}