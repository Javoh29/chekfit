package fit.lerchek.ui.feature.marathon

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.Observable
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentReportsBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class ReportsFragment : BaseToolbarDependentFragment(R.layout.fragment_reports) {
    private val binding by viewBindings(FragmentReportsBinding::bind)
    private val viewModel by viewModels<ReportsViewModel>()


    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadReports()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }
        viewModel.reports.apply {
            removeObservers(viewLifecycleOwner)
            observe(viewLifecycleOwner, {
                val selectedWeek = it.getSelectedWeek()
                val selectedReport = it.getReport()
                viewModel.reportText.set(selectedReport?.reportText)
                binding.tvWeekName.text = selectedWeek?.name
                binding.tvWeekSubtitle.text = selectedWeek?.dates
                binding.tvWelcomeText.text = selectedReport?.welcomeText
                binding.cvInside.isVisible = selectedReport?.insideVisible == true
                checkReportVisibility()
            })
        }
        viewModel.isEditMode.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (getView()!=null) {
                    checkReportVisibility()
                }
            }
        })
        binding.btnChooseWeek.setOnClickListener {
            viewModel.reports.value?.weeks?.let { weeks ->
                mContext.showSimpleOptionsDialog(weeks.map { it.name }.toTypedArray()) {
                    viewModel.selectWeek(weeks[it])
                }
            }
        }
    }

    private fun checkReportVisibility() {
        binding.cvText.isVisible =
            viewModel.isEditMode.get().not() && !viewModel.reportText.get().isNullOrEmpty()
    }

}