package fit.lerchek.ui.feature.marathon.voting

import fit.lerchek.data.domain.uimodel.marathon.VotingItemUIModel

interface VotingCallback {
    fun vote(user: VotingItemUIModel)
}