package fit.lerchek.ui.feature.marathon

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.data.domain.uimodel.marathon.FaqItemUIModel
import fit.lerchek.databinding.ItemFaqHeaderBinding
import fit.lerchek.databinding.ItemFaqItemBinding

class FaqAdapter(
    private var data: List<FaqListItem>,
    private var onBackListener: OnBackListener?
) : RecyclerView.Adapter<FaqAdapter.FaqViewHolder>() {

    interface FaqListItem

    class FaqHeader(var title: String) : FaqListItem

    interface OnBackListener {
        fun onBack()
    }

    companion object {
        const val ITEM_VIEW_TYPE_HEADER = 0
        const val ITEM_VIEW_TYPE_QUESTION = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder(
                ItemFaqHeaderBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_QUESTION -> QuestionViewHolder(
                ItemFaqItemBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            ITEM_VIEW_TYPE_HEADER
        } else {
            ITEM_VIEW_TYPE_QUESTION
        }
    }

    override fun onBindViewHolder(holder: FaqViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            holder.itemFaqHeaderBinding.apply {
                title = (data[position] as FaqHeader).title
                btnBack.root.setOnClickListener {
                    onBackListener?.onBack()
                }
            }
        } else if (holder is QuestionViewHolder) {
            val model = data[position] as FaqItemUIModel
            holder.itemFaqItemBinding.model = model
            holder.itemView.setOnClickListener {
                model.expanded = !model.expanded
                holder.dataBinding.executePendingBindings()
                it.post { notifyItemChanged(position) }
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    sealed class FaqViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class HeaderViewHolder(val itemFaqHeaderBinding: ItemFaqHeaderBinding) :
        FaqViewHolder(itemFaqHeaderBinding)

    class QuestionViewHolder(val itemFaqItemBinding: ItemFaqItemBinding) :
        FaqViewHolder(itemFaqItemBinding)
}

@BindingAdapter("faq_questions", "faq_back_callback")
fun setupFaqAdapter(
    list: RecyclerView,
    optionsData: List<FaqAdapter.FaqListItem>?,
    callback: FaqAdapter.OnBackListener?
) {
    optionsData?.let {
        list.adapter = FaqAdapter(optionsData, callback)
    }
}