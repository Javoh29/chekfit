package fit.lerchek.ui.feature.marathon.today.adapters

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.R
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.databinding.*
import fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback

class TodayAdapter(
    private var data: List<TodayItem>,
    private var callback: TodayCallback
) :
    RecyclerView.Adapter<TodayAdapter.TodayViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE_TELEGRAM = 0
        const val ITEM_VIEW_TYPE_VOTE = 1
        const val ITEM_VIEW_TYPE_SALE = 2
        const val ITEM_VIEW_TYPE_TASKS = 3
        const val ITEM_VIEW_TYPE_REF = 4
        const val ITEM_VIEW_TYPE_RECIPES = 5
        const val ITEM_VIEW_TYPE_WORKOUTS = 6
        const val ITEM_VIEW_TYPE_WAITING = 7
        const val ITEM_VIEW_TYPE_EXPIRED = 8
        const val ITEM_VIEW_TYPE_ERROR = 9
        const val ITEM_VIEW_TYPE_WATER_TRACKER = 10
        const val ITEM_VIEW_TYPE_STEPS_TRACKER = 11
        const val ITEM_VIEW_TYPE_SLEEP_TRACKER = 12
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodayViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_TELEGRAM -> TelegramViewHolder(
                ItemTodayTelegramBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_VOTE -> VoteViewHolder(
                ItemTodayVoteBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SALE -> SaleViewHolder(
                ItemTodaySaleBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_TASKS -> TasksViewHolder(
                ItemTodayTasksBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_REF -> RefViewHolder(
                ItemTodayRefBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_RECIPES -> RecipesViewHolder(
                ItemTodayRecipesBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_WORKOUTS -> WorkoutsViewHolder(
                ItemTodayWorkoutsBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_WAITING -> WaitingViewHolder(
                ItemTodayWaitingBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_EXPIRED -> ExpiredViewHolder(
                ItemTodayExpiredBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_ERROR -> ErrorViewHolder(
                ItemTodayErrorBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_WATER_TRACKER -> WaterTrackerViewHolder(
                ItemWaterTrackerBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_STEPS_TRACKER -> StepsTrackerViewHolder(
                ItemStepsTrackerBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SLEEP_TRACKER -> SleepTrackerViewHolder(
                ItemSleepTrackerBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].type.itemViewType
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TodayViewHolder, position: Int) {
        when (holder) {
            is TelegramViewHolder -> {
                holder.itemTodayTelegramBinding.model = data[position] as TodayTelegramItem
                holder.itemTodayTelegramBinding.callback = callback
            }
            is VoteViewHolder -> {
                holder.itemTodayVoteBinding.model = data[position] as TodayVoteItem
                holder.itemTodayVoteBinding.callback = callback
            }
            is SaleViewHolder -> {
                holder.itemTodaySaleBinding.model = data[position] as TodaySaleItem
                holder.itemTodaySaleBinding.callback = callback
            }
            is TasksViewHolder -> {
                holder.itemTodayTasksBinding.model = data[position] as TodayTasksItem
                holder.itemTodayTasksBinding.callback = callback
            }
            is RefViewHolder -> {
                holder.itemTodayRefBinding.apply {
                    tvTerms.movementMethod = LinkMovementMethod.getInstance()
                    link = (data[position] as TodayRefItem).Link
                    this.callback = object : TodayRefItem.Callback {
                        override fun onLinkCopied(link: String) {
                            this@TodayAdapter.callback.onReferralLinkCopied(link)
                        }

                        override fun onShareLink(link: String) {
                            this@TodayAdapter.callback.onReferralLinkShare(link)
                        }
                    }
                }
            }
            is RecipesViewHolder -> {
                holder.itemTodayRecipesBinding.model = data[position] as TodayRecipesItem
                holder.itemTodayRecipesBinding.callback = callback
            }
            is WorkoutsViewHolder -> {
                holder.itemTodayWorkoutsBinding.model = data[position] as TodayWorkoutsItem
                holder.itemTodayWorkoutsBinding.callback = callback
            }
            is WaitingViewHolder -> {
                val model = data[position] as TodayWaitingItem
                holder.itemTodayWaitingBinding.let { binding ->
                    binding.model = model
                    binding.callback = callback
                    binding.imgMarathonWaiting.apply {
                        Glide.with(this.context).load(model.imageUrl).into(this)
                    }
                    binding.tvMarathonStart.apply {
                        val ss = SpannableString(
                            this.context.getString(R.string.today_waiting_start, model.start)
                        )
                        val startIndex = ss.indexOf(string = model.start ?: "")
                        ss.setSpan(
                            StyleSpan(Typeface.BOLD),
                            startIndex,
                            startIndex + (model.start?.length ?: 0),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                        )
                        this.text = ss
                    }
                }
            }

            is ExpiredViewHolder -> {
                val model = data[position] as TodayExpiredItem
                holder.itemTodayExpiredBinding.let { binding ->
                    binding.model = model
                    binding.callback = callback
                    binding.tvTerms.movementMethod = LinkMovementMethod()
                    binding.imgMarathonExpired.apply {
                        Glide.with(this.context).load(model.imageUrl).into(this)
                    }
                }
            }

            is ErrorViewHolder -> {
                val model = data[position] as TodayErrorItem
                holder.itemTodayErrorBinding.let { binding ->
                    holder.itemView.context.let { context ->
                        when {
                            model.isExpired -> {
                                binding.title = context.getString(R.string.today_expired_title)
                                binding.message = context.getString(R.string.today_expired_msg)
                            }
                            model.isWaiting -> {
                                binding.title = context.getString(R.string.today_waiting_title)
                                binding.message = context.getString(R.string.today_waiting_msg)
                            }
                            else -> {
                            }
                        }
                    }
                }
            }

            is WaterTrackerViewHolder -> {
                val model = (data[position] as TodayWaterItem)
                holder.itemWaterTrackerBinding.let { binding ->
                    var maxGoal: Int = model.goal
                    if (model.value >= maxGoal) {
                        while(maxGoal < model.value) {
                            maxGoal *= 2
                        }
                    }
                    binding.waterProgress.max = maxGoal
                    if (model.value > model.goal) {
                        binding.tvMlStart.text =
                            "0 " + binding.root.context.getString(R.string.text_ml)
                        binding.tvMlEnd.text =
                            "${model.dimension * maxGoal} " + binding.root.context.getString(
                                R.string.text_ml
                            )
                        binding.tvInfo.visibility = View.VISIBLE
                        binding.tvMlStart.visibility = View.VISIBLE
                        binding.tvMlEnd.visibility = View.VISIBLE
                        binding.waterProgress.progress = model.goal
                        binding.waterProgress.progressSecond = model.value
                    } else {
                        binding.tvInfo.visibility = View.GONE
                        binding.tvMlStart.visibility = View.GONE
                        binding.tvMlEnd.visibility = View.GONE
                        binding.waterProgress.progress = model.value
                        binding.waterProgress.progressSecond = 0
                    }
                    if (model.dimension == 250) {
                        binding.waterProgress.setBottomTextVisible(true)
                        binding.waterProgress.bottomText =
                            "${model.dimension * model.value} " + binding.root.context.getString(R.string.text_ml)
                        binding.waterProgress.centerText =
                            "${model.value} " + binding.root.context.getString(R.string.text_glasses)
                        if (model.value in 5..20) {
                            binding.waterProgress.centerText += "ов"
                        } else
                            binding.waterProgress.centerText += when (model.value % 10) {
                                1 -> ""
                                2, 3, 4 -> "а"
                                else -> "ов"
                            }
                    } else {
                        binding.waterProgress.setBottomTextVisible(false)
                        binding.waterProgress.centerText = "${model.value * model.dimension} мл"
                    }
                    binding.imgArrow.setOnClickListener {
                        callback.onTrackerClick("water")
                    }
                    var isTimerPlus = false
                    val timerPlus = object : CountDownTimer(1500, 1500) {
                        override fun onTick(millisUntilFinished: Long) {
                        }

                        override fun onFinish() {
                            if (isTimerPlus) {
                                isTimerPlus = false
                                callback.changeCountTracker(
                                    UpdateTrackerRequest(null, model.value),
                                    "water_add"
                                )
                            }
                        }
                    }
                    binding.imgPlus.setOnClickListener {
                        if (model.dimension == 1) {
                            model.value += 50
                        } else {
                            model.value++
                        }
                        var maxG: Int = model.goal
                        if (model.value >= maxG) {
                            while(maxG < model.value) {
                                maxG *= 2
                            }
                        }
                        binding.waterProgress.max = maxG
                        if (model.value > model.goal) {
                            binding.tvMlStart.text =
                                "0 " + binding.root.context.getString(R.string.text_ml)
                            binding.tvMlEnd.text =
                                "${model.dimension * maxGoal} " + binding.root.context.getString(
                                    R.string.text_ml
                                )
                            binding.tvInfo.visibility = View.VISIBLE
                            binding.tvMlStart.visibility = View.VISIBLE
                            binding.tvMlEnd.visibility = View.VISIBLE
                            binding.waterProgress.setBothProgress(model.goal, model.value)
                        } else {
                            binding.tvInfo.visibility = View.GONE
                            binding.tvMlStart.visibility = View.GONE
                            binding.tvMlEnd.visibility = View.GONE
                            binding.waterProgress.setBothProgress(model.value, 0)
                        }
                        if (model.dimension == 250) {
                            binding.waterProgress.setBottomTextVisible(true)
                            binding.waterProgress.bottomText =
                                "${model.dimension * model.value} " + binding.root.context.getString(R.string.text_ml)
                            binding.waterProgress.centerText =
                                "${model.value} " + binding.root.context.getString(R.string.text_glasses)
                            if (model.value in 5..20) {
                                binding.waterProgress.centerText += "ов"
                            } else
                                binding.waterProgress.centerText += when (model.value % 10) {
                                    1 -> ""
                                    2, 3, 4 -> "а"
                                    else -> "ов"
                                }
                        } else {
                            binding.waterProgress.setBottomTextVisible(false)
                            binding.waterProgress.centerText = "${model.value * model.dimension} мл"
                        }
                        isTimerPlus = if (isTimerPlus) {
                            timerPlus.cancel()
                            timerPlus.start()
                            true
                        } else {
                            timerPlus.start()
                            true
                        }
                    }
                    var isTimerMinus = false
                    val timerMinus = object : CountDownTimer(1500, 1500) {
                        override fun onTick(millisUntilFinished: Long) {
                        }

                        override fun onFinish() {
                            if (isTimerMinus) {
                                isTimerMinus = false
                                callback.changeCountTracker(
                                    UpdateTrackerRequest(null, model.value),
                                    "water_add"
                                )
                            }
                        }
                    }
                    binding.imgMinus.setOnClickListener {
                        if (model.value > 0) {
                            if (model.dimension == 1) {
                                model.value -= 50
                            } else {
                                model.value--
                            }
                            var maxG: Int = model.goal
                            if (model.value >= maxG) {
                                while(maxG < model.value) {
                                    maxG *= 2
                                }
                            }
                            binding.waterProgress.max = maxG
                            if (model.value > model.goal) {
                                binding.tvMlStart.text =
                                    "0 " + binding.root.context.getString(R.string.text_ml)
                                binding.tvMlEnd.text =
                                    "${model.dimension * maxGoal} " + binding.root.context.getString(
                                        R.string.text_ml
                                    )
                                binding.tvInfo.visibility = View.VISIBLE
                                binding.tvMlStart.visibility = View.VISIBLE
                                binding.tvMlEnd.visibility = View.VISIBLE
                                binding.waterProgress.setBothProgress(model.goal, model.value)
                            } else {
                                binding.tvInfo.visibility = View.GONE
                                binding.tvMlStart.visibility = View.GONE
                                binding.tvMlEnd.visibility = View.GONE
                                binding.waterProgress.setBothProgress(model.value, 0)
                            }
                            if (model.dimension == 250) {
                                binding.waterProgress.setBottomTextVisible(true)
                                binding.waterProgress.bottomText =
                                    "${model.dimension * model.value} " + binding.root.context.getString(R.string.text_ml)
                                binding.waterProgress.centerText =
                                    "${model.value} " + binding.root.context.getString(R.string.text_glasses)
                                if (model.value in 5..20) {
                                    binding.waterProgress.centerText += "ов"
                                } else
                                    binding.waterProgress.centerText += when (model.value % 10) {
                                        1 -> ""
                                        2, 3, 4 -> "а"
                                        else -> "ов"
                                    }
                            } else {
                                binding.waterProgress.setBottomTextVisible(false)
                                binding.waterProgress.centerText = "${model.value * model.dimension} мл"
                            }
                            if (isTimerMinus) {
                                isTimerMinus = false
                                timerMinus.cancel()
                                isTimerMinus = true
                                timerMinus.start()
                            } else {
                                isTimerMinus = true
                                timerMinus.start()
                            }
                        }
                    }
                }
            }

            is StepsTrackerViewHolder -> {
                val model = (data[position] as TodayStepsItem)
                holder.itemStepsTrackerBinding.let { binding ->
                    binding.tvStepsDate.text =
                        binding.root.context.getString(R.string.text_today) + ", ${model.date}"
                    binding.tvStepsCount.text = model.value.toString()
                    binding.tvStepsStart.text = "0"
                    binding.tvConstSteps.text = model.goal.toString()
                    binding.root.postDelayed({
                        val maxWidth = binding.viewProgressMain.width
                        var maxGoal: Int = model.goal * 2
                        if (model.value >= maxGoal) {
                            while(maxGoal < model.value) {
                                maxGoal = (maxGoal * 1.3).toInt()
                            }
                        }
                        val stepsRate: Int
                        if (model.value > model.goal) {
                            stepsRate = (model.value * maxWidth) / maxGoal
                            binding.viewProgressOne.visibility = View.VISIBLE
                            binding.tvInfoSteps.visibility = View.VISIBLE
                            binding.viewLineProgress.visibility = View.VISIBLE
                            binding.viewProgressTwo.setBackgroundResource(R.drawable.rectangle_orange2)
                            binding.viewProgressOne.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = stepsRate
                            }
                            binding.viewProgressTwo.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = (model.goal * maxWidth) / maxGoal
                            }
                            binding.viewProgressDef.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = (model.goal * maxWidth) / maxGoal
                            }
                        } else {
                            stepsRate = (model.value * maxWidth) / model.goal
                            binding.viewProgressOne.visibility = View.INVISIBLE
                            binding.tvInfoSteps.visibility = View.GONE
                            binding.viewLineProgress.visibility = View.INVISIBLE
                            binding.viewProgressTwo.setBackgroundResource(R.drawable.rectangle_orange)
                            binding.viewProgressTwo.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = stepsRate
                            }
                            binding.viewProgressDef.updateLayoutParams<ViewGroup.LayoutParams> {
                                width = maxWidth
                            }
                        }
                    }, 100)
                    binding.imgArrowSteps.setOnClickListener {
                        callback.onTrackerClick("step")
                    }
                }
            }

            is SleepTrackerViewHolder -> {
                val model = (data[position] as TodaySleepItem)
                holder.itemSleepTrackerBinding.let { binding ->
                    val hour: Int = model.value / 60
                    val minutes: Int = model.value % 60
                    binding.tvSleepHoursCount.text = hour.toString()
                    binding.tvSleepMinCount.text = minutes.toString()
                    binding.imgArrowSleep.setOnClickListener {
                        callback.onTrackerClick("dream")
                    }
                }
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

    sealed class TodayViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class TelegramViewHolder(val itemTodayTelegramBinding: ItemTodayTelegramBinding) :
        TodayViewHolder(itemTodayTelegramBinding)

    class VoteViewHolder(val itemTodayVoteBinding: ItemTodayVoteBinding) :
        TodayViewHolder(itemTodayVoteBinding)

    class SaleViewHolder(val itemTodaySaleBinding: ItemTodaySaleBinding) :
        TodayViewHolder(itemTodaySaleBinding)

    class TasksViewHolder(val itemTodayTasksBinding: ItemTodayTasksBinding) :
        TodayViewHolder(itemTodayTasksBinding)

    class RefViewHolder(val itemTodayRefBinding: ItemTodayRefBinding) :
        TodayViewHolder(itemTodayRefBinding)

    class RecipesViewHolder(val itemTodayRecipesBinding: ItemTodayRecipesBinding) :
        TodayViewHolder(itemTodayRecipesBinding)

    class WorkoutsViewHolder(val itemTodayWorkoutsBinding: ItemTodayWorkoutsBinding) :
        TodayViewHolder(itemTodayWorkoutsBinding)

    class WaitingViewHolder(val itemTodayWaitingBinding: ItemTodayWaitingBinding) :
        TodayViewHolder(itemTodayWaitingBinding)

    class ExpiredViewHolder(val itemTodayExpiredBinding: ItemTodayExpiredBinding) :
        TodayViewHolder(itemTodayExpiredBinding)

    class ErrorViewHolder(val itemTodayErrorBinding: ItemTodayErrorBinding) :
        TodayViewHolder(itemTodayErrorBinding)

    class WaterTrackerViewHolder(val itemWaterTrackerBinding: ItemWaterTrackerBinding) :
        TodayViewHolder(itemWaterTrackerBinding)

    class StepsTrackerViewHolder(val itemStepsTrackerBinding: ItemStepsTrackerBinding) :
        TodayViewHolder(itemStepsTrackerBinding)

    class SleepTrackerViewHolder(val itemSleepTrackerBinding: ItemSleepTrackerBinding) :
        TodayViewHolder(itemSleepTrackerBinding)

}

@BindingAdapter("today_items", "today_callback")
fun setupTodayAdapter(
    list: RecyclerView,
    optionsData: List<TodayItem>?,
    callback: TodayCallback
) {
    optionsData?.let {
        list.adapter = TodayAdapter(optionsData, callback)
    }
}