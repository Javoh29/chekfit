package fit.lerchek.ui.feature.marathon.profile.referral

import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.profile.ReferralDetailsUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class ReferralViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val referralDetails: ObservableField<ReferralDetailsUIModel> = ObservableField()

    fun loadReferralDetails() {
        processTask(
            userRepository.loadReferralDetails()
                .subscribeWith(simpleSingleObserver<ReferralDetailsUIModel> {
                    referralDetails.set(it)
                    isProgress.set(false)
                })
        )
    }

}