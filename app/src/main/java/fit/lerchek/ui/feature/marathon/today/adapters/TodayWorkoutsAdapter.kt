package fit.lerchek.ui.feature.marathon.today.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.data.domain.uimodel.marathon.WorkoutUIModel
import fit.lerchek.databinding.ItemTodayWorkoutBinding
import fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback

class TodayWorkoutsAdapter(
    private var workouts: List<WorkoutUIModel>,
    private var callback: TodayCallback?
) : RecyclerView.Adapter<TodayWorkoutsAdapter.WorkoutViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkoutViewHolder {
        return WorkoutViewHolder(
            ItemTodayWorkoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: WorkoutViewHolder, position: Int) {
        val workout = workouts[position]
        holder.binding.model = workout
        holder.binding.executePendingBindings()
        holder.bind(workout, callback)
    }

    override fun getItemCount(): Int {
        return workouts.size
    }

    class WorkoutViewHolder(val binding: ItemTodayWorkoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(workout: WorkoutUIModel, callback: TodayCallback?) {
            itemView.setOnClickListener {
                callback?.onWorkoutClick(workout)
            }
            binding.imgWorkout.apply {
                Glide.with(this.context).load(workout.imageUrl).into(this)
            }
        }

    }

}


@BindingAdapter("today_workouts", "today_workouts_callback")
fun setupTodayWorkoutsAdapter(
    list: RecyclerView,
    items: List<WorkoutUIModel>?,
    callback: TodayCallback?
) {
    items?.let {
        list.adapter = TodayWorkoutsAdapter(it, callback)
    }
}