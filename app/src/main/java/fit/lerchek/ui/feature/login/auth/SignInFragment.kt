package fit.lerchek.ui.feature.login.auth

import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.ObservableField
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.BuildConfig
import fit.lerchek.R
import fit.lerchek.databinding.FragmentSignInBinding
import fit.lerchek.ui.base.BaseFragment
import fit.lerchek.ui.feature.marathon.MarathonActivity
import fit.lerchek.ui.util.SupportUtils
import fit.lerchek.ui.util.viewBindings
import fit.lerchek.util.EventObserver

@AndroidEntryPoint
class SignInFragment : BaseFragment(R.layout.fragment_sign_in) {

    private val binding by viewBindings(FragmentSignInBinding::bind)
    private val viewModel by viewModels<SignInViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.success.observe(this, EventObserver { success ->
            mContext.closeExistingAlert()
            if (success) {
                mContext.apply {
                    startActivity(Intent(this, MarathonActivity::class.java))
                    finish()
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        binding.btnSignIn.setOnClickListener {
            mContext.showProgressDialog()
            viewModel.doLogin()
        }

        binding.forgotPwd.setOnClickListener {
            navigateTo(R.id.action_fragment_sign_in_to_forgotPwdFragment)
        }

        binding.showPwd.setOnClickListener {
            val inputPwd = binding.inputPwd
            if (inputPwd.transformationMethod != null) {
                binding.showPwd.setImageResource(R.drawable.ic_pass_show)
                inputPwd.transformationMethod = null
                inputPwd.setSelection(inputPwd.text.toString().length)
            } else {
                binding.showPwd.setImageResource(R.drawable.ic_pass_hiden)
                inputPwd.transformationMethod = PasswordTransformationMethod.getInstance()
                inputPwd.setSelection(inputPwd.text.toString().length)
            }
        }

        binding.back.root.setOnClickListener {
            navigateUp()
        }
    }

    override fun onStart() {
        super.onStart()

        binding.inputPwd.doAfterTextChanged {
            binding.btnSignIn.isEnabled = viewModel.checkInput().also {
                binding.btnSignIn.elevation =
                    if (!it) 1f else SupportUtils.convertDpToPixel(12f, requireContext())
                binding.btnSignIn.invalidateOutline()
            }
        }

        binding.inputEmail.doAfterTextChanged {
            binding.btnSignIn.isEnabled = viewModel.checkInput().also {
                if (!it) binding.btnSignIn.elevation =
                    if (!it) 1f else SupportUtils.convertDpToPixel(12f, requireContext())
                viewModel.loginError.set(null)
                binding.btnSignIn.invalidateOutline()
            }
        }

//        if (BuildConfig.DEBUG) {
//            binding.titleLogin.setOnClickListener {
//                showTestCredentialsChooser()
//            }
//        }
    }



}
