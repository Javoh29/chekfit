package fit.lerchek.ui.feature.marathon.profile

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.rx.CustomSingleObserver
import fit.lerchek.data.domain.uimodel.profile.ProfileDetailsUIModel
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.ThemeUtil
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val dataManager: DataManager,
    private val userRepository: UserRepository
) : BaseViewModel() {

    var isMarathonLocalItemsVisible: ObservableBoolean = ObservableBoolean()
    val profileDetails: ObservableField<ProfileDetailsUIModel> = ObservableField()


    init {
        checkIsInsideMarathon()
    }

    private fun checkIsInsideMarathon() {
        processTask(userRepository.checkIsInsideMarathon().subscribeWith(
            object : CustomSingleObserver<Boolean>() {
                override fun onSuccess(result: Boolean) {
                    isMarathonLocalItemsVisible.set(result)
                }
            }
        ))
    }

    fun loadProfileDetails1() {
        processTask(
            userRepository.loadProfileDetails()
                .subscribeWith(simpleSingleObserver<ProfileDetailsUIModel> {
                    profileDetails.set(it)
                    isProgress.set(false)
                })
        )
    }
    fun loadProfileDetails(withProgress : Boolean = true) {
        processTask(
            userRepository.loadProfileDetails()
                .subscribeWith(object : DisposableSingleObserver<ProfileDetailsUIModel>() {
                    override fun onStart() {
                        super.onStart()
                        if (withProgress)
                            isProgress.set(true)
                    }
                    override fun onSuccess(it: ProfileDetailsUIModel) {
                        profileDetails.set(it)
                        if (withProgress)
                            isProgress.set(false)
                    }

                    override fun onError(e: Throwable) {
                        if (withProgress)
                            isProgress.set(false)
                    }
                })
        )
    }

    fun switchTheme(isNightMode: Boolean) {
        ThemeUtil.setNightMode(isNightMode)
        dataManager.saveIsNightMode(isNightMode)
    }

    fun isNightTheme(): Boolean {
        return ThemeUtil.isNightMode()
    }

}