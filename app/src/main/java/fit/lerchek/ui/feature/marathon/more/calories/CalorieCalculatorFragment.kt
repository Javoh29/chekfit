package fit.lerchek.ui.feature.marathon.more.calories

import android.os.Bundle
import android.view.View
import androidx.databinding.Observable
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentCalorieCalculatorBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class CalorieCalculatorFragment :
    BaseToolbarDependentFragment(R.layout.fragment_calorie_calculator) {

    private val binding by viewBindings(FragmentCalorieCalculatorBinding::bind)
    private val viewModel by viewModels<CalorieCalculatorViewModel>()
    private var adapter: CalorieAdapter? = null

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.nsvCaloriesCalculator
        binding.viewModel = viewModel
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }
        viewModel.content.observe(viewLifecycleOwner, {
            if (adapter == null) {
                adapter = CalorieAdapter(
                    data = it,
                    onChange = viewModel::onChange
                )
                binding.rvCalorie.adapter = adapter
            } else {
                adapter?.update(it)
            }
        })

        viewModel.yourCalorie.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if(getView()!=null) {
                    binding.nsvCaloriesCalculator.postDelayed({
                        val child = binding.btnCalculate
                        binding.nsvCaloriesCalculator.smoothScrollTo(
                            child.x.toInt(),
                            child.y.toInt()
                        )
                    }, 100)
                }
            }
        })
    }
}