package fit.lerchek.ui.feature.marathon.more

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.MealUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.FragmentExtraRecipesBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class ExtraRecipesFragment :
    BaseToolbarDependentFragment(R.layout.fragment_extra_recipes),
    ExtraRecipesAdapter.ExtraRecipesCallback {

    private val binding by viewBindings(FragmentExtraRecipesBinding::bind)
    private val viewModel by viewModels<ExtraRecipesViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadExtraRecipes()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
    }

    override fun onSelectRecipe(recipeUIModel: RecipeUIModel) {
        navigateTo(
            ExtraRecipesFragmentDirections.actionNavigationExtraRecipesToNavigationRecipeDetails(
                false,
                recipeUIModel.id,
                recipeUIModel.baseId ?: -1,
                false,
                true,
                recipeUIModel.dayNumber ?: -1
            )
        )
    }

    override fun onSelectMeals(meals: List<MealUIModel>) {
        mContext.showSimpleOptionsDialog(meals.map { it.name }.toTypedArray()) {
            viewModel.selectMeal(meals[it])
        }
    }

    override fun onBack() {
        navigateUp()
    }
}