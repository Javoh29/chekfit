package fit.lerchek.ui.feature.marathon.voting

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.Constants.UNKNOWN_ERROR
import fit.lerchek.data.domain.uimodel.marathon.VotingItem
import fit.lerchek.data.domain.uimodel.marathon.VotingItemUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class VotingViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    private val mVotingItems = MutableLiveData<List<VotingItem>>()

    val votingItems: LiveData<List<VotingItem>>
        get() = mVotingItems

    fun loadVoting() {
        processTask(
            userRepository.getVoting()
                .subscribeWith(simpleSingleObserver<List<VotingItem>> {
                    mVotingItems.postValue(it)
                    isProgress.set(false)
                })
        )
    }

    fun toggleLike(user: VotingItemUIModel) {
        processTask(
            userRepository.vote(mVotingItems.value ?: arrayListOf(), user)
                .subscribeWith(object : DisposableSingleObserver<List<VotingItem>>() {
                    override fun onSuccess(result: List<VotingItem>) {
                        mVotingItems.postValue(result)
                    }

                    override fun onError(e: Throwable) {
                        onErrorListener?.onError(
                            message = e.message ?: UNKNOWN_ERROR,
                            goBack = false
                        )
                    }
                })
        )
    }

}