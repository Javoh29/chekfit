package fit.lerchek.ui.feature.marathon.menu

import fit.lerchek.data.domain.uimodel.MenuItemUIModel

interface MenuItemSelectListener {
    fun onMenuItemSelect(item: MenuItemUIModel)
}