package fit.lerchek.ui.feature.marathon.more.cardiotest

import android.os.Bundle
import android.view.View
import androidx.databinding.Observable
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentCardioTestBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class CardioTestFragment :
    BaseToolbarDependentFragment(R.layout.fragment_cardio_test) {

    private val binding by viewBindings(FragmentCardioTestBinding::bind)
    private val viewModel by viewModels<CardioTestViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.nsvCardioTest
        binding.viewModel = viewModel
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }

        viewModel.heartRateLimitLower.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if(getView()!=null) {
                    binding.nsvCardioTest.postDelayed({
                        binding.nsvCardioTest.fullScroll(View.FOCUS_DOWN)
                    }, 100)
                }
            }
        })
    }
}