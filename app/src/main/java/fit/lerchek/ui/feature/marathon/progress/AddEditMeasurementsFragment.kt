package fit.lerchek.ui.feature.marathon.progress

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentAddEditMeasurementsBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class AddEditMeasurementsFragment :
    BaseToolbarDependentFragment(R.layout.fragment_add_edit_measurements) {
    private val binding by viewBindings(FragmentAddEditMeasurementsBinding::bind)
    private val viewModel by viewModels<AddEditMeasurementsViewModel>()
    private val args: AddEditMeasurementsFragmentArgs by navArgs()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override var isBottomNavigationVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadMeasurement(args.type, args.week, args.value)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.apply {
            btnClose.setOnClickListener {
                navigateUp()
            }

            btnSave.setOnClickListener {
                viewModel?.save()
            }
        }

        viewModel.savedState.observe(viewLifecycleOwner) {
            navigateUp()
        }
    }
}