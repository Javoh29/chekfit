package fit.lerchek.ui.feature.marathon.more

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.MealUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.FragmentExtraRecipesBinding
import fit.lerchek.databinding.FragmentProductsBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class ProductsFragment :
    BaseToolbarDependentFragment(R.layout.fragment_products),
    ProductsAdapter.ProductsCallback {

    private val binding by viewBindings(FragmentProductsBinding::bind)
    private val viewModel by viewModels<ProductsViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadProducts()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
    }

    override fun onSelectCategory(categories: List<MealUIModel>) {
        mContext.showSimpleOptionsDialog(categories.map { it.name }.toTypedArray()) {
            viewModel.selectCategory(categories[it])
        }
    }

    override fun onBack() {
        navigateUp()
    }
}