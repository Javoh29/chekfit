package fit.lerchek.ui.feature.marathon.progress

import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class ProgressViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val progress: ObservableField<MarathonProgressUIModel> = ObservableField()

    fun loadProgress() {
        isProgress.set(true)
        userRepository.getProgressForMarathon()
            ?.subscribeWith(simpleSingleObserver<MarathonProgressUIModel> {
                progress.set(it)
                isProgress.set(false)
            })?.let { processTask(it) }
    }

}