package fit.lerchek.ui.feature.marathon

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.MarathonUIModel
import fit.lerchek.databinding.ItemMarathonsListBinding
import fit.lerchek.databinding.ItemMarathonsListHeaderBinding

class MarathonsAdapter(
    private var data: List<MarathonUIModel>?,
    private var callback: SelectMarathonCallback
) :
    RecyclerView.Adapter<MarathonsAdapter.MarathonViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE_HEADER = 0
        const val ITEM_VIEW_TYPE_MARATHON = 1
    }

    interface SelectMarathonCallback {
        fun onSelectItem(marathon: MarathonUIModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarathonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder(
                ItemMarathonsListHeaderBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_MARATHON -> MarathonItemViewHolder(
                ItemMarathonsListBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            ITEM_VIEW_TYPE_HEADER
        } else {
            ITEM_VIEW_TYPE_MARATHON
        }
    }

    override fun onBindViewHolder(holder: MarathonViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            holder.itemMarathonsListHeaderBinding.headerText =
                holder.itemMarathonsListHeaderBinding.root.context.getString(R.string.choose_marathon)
        } else if (holder is MarathonItemViewHolder) {
            val model = data!![position - 1]
            holder.itemMarathonsListBinding.model = model
            holder.itemMarathonsListBinding.callback = callback
            holder.itemMarathonsListBinding.executePendingBindings()
            holder.bind(model)
        }
    }

    override fun getItemCount(): Int {
        return if (data == null) 0 else data!!.size + 1
    }

    sealed class MarathonViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class HeaderViewHolder(val itemMarathonsListHeaderBinding: ItemMarathonsListHeaderBinding) :
        MarathonViewHolder(itemMarathonsListHeaderBinding)

    class MarathonItemViewHolder(val itemMarathonsListBinding: ItemMarathonsListBinding) :
        MarathonViewHolder(itemMarathonsListBinding) {
        fun bind(model: MarathonUIModel) {
            itemMarathonsListBinding.imgMarathon.apply {
                Glide.with(this.context).load(model.imageUrl).into(this)
            }
        }
    }
}

@BindingAdapter("marathons", "select_marathon_callback")
fun setupMarathonsAdapter(
    list: RecyclerView,
    optionsData: List<MarathonUIModel>?,
    callback: MarathonsAdapter.SelectMarathonCallback
) {
    optionsData?.let {
        list.adapter = MarathonsAdapter(optionsData, callback)
    }
}

@BindingAdapter("marathonStatus")
fun setMarathonStatus(textView: AppCompatTextView, status: String?) {
    status?.let {
        val context = textView.context
        when (status) {
            "wait" -> {
                textView.text = context.getString(R.string.marathon_status_wait)
                textView.background.setTint(ContextCompat.getColor(context, R.color.status_pink))
            }
            "running" -> {
                textView.text = context.getString(R.string.marathon_status_running)
                textView.background.setTint(ContextCompat.getColor(context, R.color.status_green))
            }
            "finished" -> {
                textView.text = context.getString(R.string.marathon_status_finished)
                textView.background.setTint(ContextCompat.getColor(context, R.color.status_grey))
            }
        }
    }
}