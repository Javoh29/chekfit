@file:Suppress("DEPRECATION")

package fit.lerchek.ui.feature.marathon.today

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.request.SessionReadRequest
import com.google.android.gms.fitness.result.SessionReadResponse
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.Constants.LABEL_REF_CLIP_DATA
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.data.domain.uimodel.marathon.TodayVoteItem
import fit.lerchek.data.domain.uimodel.marathon.WorkoutUIModel
import fit.lerchek.data.fcm.FMService
import fit.lerchek.databinding.FragmentTodayBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.chat.ChatActivity
import fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback
import fit.lerchek.ui.util.BrowserUtils
import fit.lerchek.ui.util.TextUtils.copyTextToBuffer
import fit.lerchek.ui.util.viewBindings
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

@AndroidEntryPoint
class TodayFragment : BaseToolbarDependentFragment(R.layout.fragment_today), TodayCallback {

    private val binding by viewBindings(FragmentTodayBinding::bind)
    private val viewModel by viewModels<TodayViewModel>()
    private val args: TodayFragmentArgs by navArgs()
    private val recognitionRequestCode: Int = 1122
    private val mapFitData: HashMap<String, Int> = HashMap()
    private var mGoogleApiClient: GoogleApiClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        getGoogleFitDaily()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadMarathonDetails(args.marathonId, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        viewModel.bottomBarAvailable.observe(viewLifecycleOwner) { isVisible ->
            isBottomNavigationVisible = isVisible
            updateBottomNavigation()
            processDeepLink(args.deepLinkType)
        }
    }

    private fun processDeepLink(deepLinkType: String) {
        if (viewModel.deepLinkProcessed.not()) {
            viewModel.deepLinkProcessed = true
            when (deepLinkType) {
                FMService.TYPE_VOTE -> navigateTo(R.id.navigation_voting)
                FMService.TYPE_QA -> startActivity(Intent(mContext, ChatActivity::class.java))
                FMService.TYPE_PHOTO -> navigateTo(R.id.navigation_progress)
            }
        }
    }

    private fun getGoogleFitDaily() {
        if (viewModel.getSwitchState()) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACTIVITY_RECOGNITION
                )
                != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
            ) {
                requestPermissions(
                    arrayOf(Manifest.permission.ACTIVITY_RECOGNITION),
                    recognitionRequestCode
                )
            } else {
                getStepsData()
            }
        }
    }

    private fun getStepsData() {
        val fitnessOptions = FitnessOptions.builder()
            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .build()

        Fitness.getHistoryClient(
            requireActivity(),
            GoogleSignIn.getAccountForExtension(requireContext(), fitnessOptions)
        )
            .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
            .addOnSuccessListener { result ->
                mapFitData["step"] =
                    result.dataPoints.firstOrNull()?.getValue(Field.FIELD_STEPS)?.asInt() ?: 0
                getWaterData()
            }
            .addOnFailureListener { e ->
                Log.i("BUG", "There was a problem getting steps.", e)
            }
    }

    private fun getWaterData() {
        val calendar = Calendar.getInstance()
        val endDate = calendar.time.time / 1000
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val startDate = calendar.time.time / 1000

        mGoogleApiClient =
            GoogleApiClient.Builder(requireContext().applicationContext).addApi(Fitness.HISTORY_API)
                .addConnectionCallbacks(
                    object : GoogleApiClient.ConnectionCallbacks {
                        @SuppressLint("CheckResult", "SimpleDateFormat")
                        override fun onConnected(p0: Bundle?) {
                            val readRequest: DataReadRequest = DataReadRequest.Builder()
                                .read(DataType.TYPE_HYDRATION)
                                .setTimeRange(
                                    startDate,
                                    endDate,
                                    TimeUnit.SECONDS
                                ).build()

                            Fitness.HistoryApi.readData(mGoogleApiClient!!, readRequest)
                                .setResultCallback {
                                    if (it.status.isSuccess) {
                                        if (it.dataSets.size > 0) {
                                            val map = HashMap<String, Int>()
                                            mapFitData["water"] = 0
                                            for (dataSet in it.dataSets) {
                                                for (dp in dataSet.dataPoints) {
                                                    val dpTime =
                                                        SimpleDateFormat("yyyy-MM-dd").format(
                                                            Date(
                                                                dp.getTimestamp(TimeUnit.MILLISECONDS)
                                                            )
                                                        )
                                                    if (map.containsKey(dpTime)) {
                                                        var value = map[dpTime]!!
                                                        value += (dp.getValue(Field.FIELD_VOLUME)
                                                            .asFloat() * 1000).toInt()
                                                        map[dpTime] = value
                                                    } else map[dpTime] =
                                                        (dp.getValue(Field.FIELD_VOLUME)
                                                            .asFloat() * 1000).toInt()
                                                }
                                            }
                                            map.forEach { fit ->
                                                mapFitData["water"] = fit.value
                                            }
                                            getDreamData()
                                        }
                                    }
                                }
                        }

                        override fun onConnectionSuspended(p0: Int) {
                        }
                    }
                )
                .addOnConnectionFailedListener { connectionResult ->
                    Log.i("BUG", "Authorization - Failed Authorization Mgr:$connectionResult")
                }
                .build()

        mGoogleApiClient?.connect()
    }

    private fun getDreamData() {
        val calendar = Calendar.getInstance()
        val endDate = calendar.time
        calendar.add(Calendar.DATE, -1)
        val startDate = calendar.time

        val fitnessOptions = FitnessOptions.builder()
            .addDataType(DataType.TYPE_SLEEP_SEGMENT, FitnessOptions.ACCESS_READ)
            .build()

        val request = SessionReadRequest.Builder()
            .readSessionsFromAllApps()
            .includeSleepSessions()
            .read(DataType.TYPE_SLEEP_SEGMENT)
            .setTimeInterval(startDate.time / 1000, endDate.time / 1000, TimeUnit.SECONDS)
            .build()

        Fitness.getSessionsClient(
            requireActivity(),
            GoogleSignIn.getAccountForExtension(requireContext(), fitnessOptions)
        ).readSession(request)
            .addOnSuccessListener { sessionReadResponse: SessionReadResponse ->
                var dreamSum = 0
                for (ses in sessionReadResponse.sessions) {
                    val sessionStart = ses.getStartTime(TimeUnit.MILLISECONDS)
                    val sessionEnd = ses.getEndTime(TimeUnit.MILLISECONDS)
                    val sessionSleepTime = sessionEnd - sessionStart
                    dreamSum = TimeUnit.MILLISECONDS.toMinutes(sessionSleepTime).toInt()
                }
                mapFitData["dream"] = dreamSum
                viewModel.loadMarathonDetails(args.marathonId, mapFitData)
            }.addOnFailureListener { err: Exception ->
                Log.i(
                    "BUG",
                    err.message!!
                )
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == recognitionRequestCode && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            getGoogleFitDaily()
        }
    }

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onTgChannelClick(channel: String) {
        BrowserUtils.openBrowser(mContext, channel)
    }

    override fun onTgGroupClick(group: String) {
        BrowserUtils.openBrowser(mContext, group)
    }

    override fun onSaleOfferClick(link: String) {
        BrowserUtils.openBrowser(mContext, link)
    }

    override fun onVoteClick(vote: TodayVoteItem) {
        navigateTo(R.id.navigation_voting)
    }

    override fun onActivityCalendarClick() {
        navigateTo(R.id.navigation_activity_calendar)
    }

    override fun updateTaskCompletion(task: TaskUIModel, currentDay: Int) {
        viewModel.updateTaskCompletion(task, currentDay)
    }

    override fun onTaskInfoClick(task: TaskUIModel) {
        navigateTo(
            TodayFragmentDirections.actionNavigationTodayToNavigationInfo(
                task.title, task.comment
            )
        )
    }

    override fun onReferralLinkCopied(link: String) {
        mContext.copyTextToBuffer(LABEL_REF_CLIP_DATA, link)
        mContext.showToast(mContext.getString(R.string.today_ref_copied))
    }

    override fun onReferralLinkShare(link: String) {
        BrowserUtils.shareLink(mContext, link)
    }

    override fun onRecipeClick(recipeUIModel: RecipeUIModel) {
        navigateTo(
            TodayFragmentDirections.actionNavigationTodayToNavigationRecipeDetails(
                true,
                recipeUIModel.id,
                recipeUIModel.baseId ?: -1,
                true,
                false,
                recipeUIModel.dayNumber ?: -1
            )
        )
    }

    override fun onWorkoutClick(workoutUIModel: WorkoutUIModel) {
        if (workoutUIModel.descriptionRawData == "null") workoutUIModel.descriptionRawData = null

        navigateTo(
            TodayFragmentDirections.actionNavigationTodayToNavigationWorkoutDetails(
                workoutUIModel.name,
                workoutUIModel.descriptionRawData ?: workoutUIModel.description ?: "",
                workoutUIModel.videoUrl,
                workoutUIModel.videoType
            )
        )
    }

    override fun onWaitingClick() {
        navigateUp()
    }

    override fun onRenewClick(link: String) {
        BrowserUtils.openBrowser(mContext, link)
    }

    override fun onTrackerClick(type: String) {
        navigateTo(TodayFragmentDirections.actionNavigationTodayToNavigationTracker(type))
    }

    override fun changeCountTracker(request: UpdateTrackerRequest, type: String) {
        viewModel.changeCountTracker(request, type, mGoogleApiClient)
    }
}