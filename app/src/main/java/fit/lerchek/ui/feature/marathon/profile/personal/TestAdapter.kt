package fit.lerchek.ui.feature.marathon.profile.personal

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.data.domain.uimodel.marathon.TestFooterItem
import fit.lerchek.data.domain.uimodel.marathon.TestItem
import fit.lerchek.data.domain.uimodel.marathon.TestItemUIModel
import fit.lerchek.databinding.ItemTestFooterBinding
import fit.lerchek.databinding.ItemTestHeaderBinding
import fit.lerchek.databinding.ItemTestItemBinding


class TestAdapter(
    private var data: List<TestItem>,
    private var onBack: () -> Unit?,
    private var onConfirm: () -> Unit?
) : RecyclerView.Adapter<TestAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_HEADER = 0
        private const val ITEM_VIEW_TYPE_ITEM = 1
        private const val ITEM_VIEW_TYPE_FOOTER = 2
    }

    fun update(content: List<TestItem>) {
        data = content
        notifyItemRangeChanged(0, data.size)
    }

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class HeaderViewHolder(val itemHeaderBinding: ItemTestHeaderBinding) :
        ViewHolder(itemHeaderBinding)

    class ItemViewHolder(val itemBinding: ItemTestItemBinding) :
        ViewHolder(itemBinding)

    class FooterViewHolder(val itemFooterBinding: ItemTestFooterBinding) :
        ViewHolder(itemFooterBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder(
                ItemTestHeaderBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_ITEM -> ItemViewHolder(
                ItemTestItemBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_FOOTER -> FooterViewHolder(
                ItemTestFooterBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is TestItemUIModel -> ITEM_VIEW_TYPE_ITEM
            is TestFooterItem -> ITEM_VIEW_TYPE_FOOTER
            else -> ITEM_VIEW_TYPE_HEADER
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                holder.itemHeaderBinding.apply {
                    btnBack.root.setOnClickListener {
                        onBack()
                    }
                    (data[position]).let { item ->
                        title = item.title
                        description = item.description
                    }
                }
            }
            is FooterViewHolder -> {
                holder.itemFooterBinding.apply {
                    model = data[position] as TestFooterItem
                    btnSave.setOnClickListener {
                        onConfirm()
                    }
                }
            }
            is ItemViewHolder -> {
                (data[position] as TestItemUIModel).let { item ->
                    holder.itemBinding.apply {
                        model = item
                        rvTestOptions.adapter = TestOptionsAdapter(item.options ?: arrayListOf())
                    }
                }
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}