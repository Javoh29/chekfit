package fit.lerchek.ui.feature.marathon.menu

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.MenuItemType
import fit.lerchek.data.domain.uimodel.MenuItemUIModel
import fit.lerchek.databinding.ItemMenuButtonBinding
import fit.lerchek.databinding.ItemMenuLabelBinding

class MenuAdapter(
    private var data: List<MenuItemUIModel>,
    private var callback: MenuItemSelectListener?
) : RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE_BUTTON = 0
        const val ITEM_VIEW_TYPE_LABEL = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_LABEL -> LabelViewHolder(
                ItemMenuLabelBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_BUTTON -> ButtonViewHolder(
                ItemMenuButtonBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].type == MenuItemType.Logout)
            ITEM_VIEW_TYPE_BUTTON else ITEM_VIEW_TYPE_LABEL
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val item = data[position]
        if (holder is ButtonViewHolder) {
            holder.itemMenuButtonBinding.let {
                it.item = item
                it.callback = callback
                it.executePendingBindings()
            }
        } else if (holder is LabelViewHolder) {
            holder.itemMenuLabelBinding.let {
                it.item = item
                it.callback = callback
                it.executePendingBindings()
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    sealed class MenuViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class ButtonViewHolder(val itemMenuButtonBinding: ItemMenuButtonBinding) :
        MenuViewHolder(itemMenuButtonBinding)

    class LabelViewHolder(val itemMenuLabelBinding: ItemMenuLabelBinding) :
        MenuViewHolder(itemMenuLabelBinding)

}

@BindingAdapter("menu_items", "menu_items_callback")
fun setupMenu(
    list: RecyclerView,
    menuItems: List<MenuItemUIModel>?,
    callback: MenuItemSelectListener?
) {
    menuItems?.let {
        list.adapter = MenuAdapter(it, callback)
    }
}

@BindingAdapter("menu_item")
fun setupMenuItem(
    textView: AppCompatTextView,
    menuItem: MenuItemUIModel?
) {
    menuItem?.let {
        when (it.type) {
            MenuItemType.MarathonSelect -> textView.setText(R.string.app_menu_marathon_select)
            MenuItemType.ReferralProgram -> textView.setText(R.string.app_menu_referral)
            MenuItemType.AskQuestion -> textView.setText(R.string.app_menu_ask)
            MenuItemType.TelegramChannel -> textView.setText(R.string.app_menu_tg_channel)
            MenuItemType.Instruction -> textView.setText(R.string.app_menu_instructions)
            MenuItemType.MarathonReports -> textView.setText(R.string.app_menu_marathon_reports)
            MenuItemType.ActivityCalendar -> textView.setText(R.string.app_menu_activity_calendar)
            MenuItemType.TelegramGroup -> textView.setText(R.string.app_menu_tg_group)
            MenuItemType.Profile -> textView.setText(R.string.app_menu_profile)
            MenuItemType.Logout -> textView.setText(R.string.app_menu_logout)
            MenuItemType.ExtraWorkout -> textView.text = it.workoutName
        }
    }
}
