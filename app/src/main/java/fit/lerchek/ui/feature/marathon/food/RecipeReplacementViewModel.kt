package fit.lerchek.ui.feature.marathon.food

import androidx.databinding.ObservableArrayList
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import javax.inject.Inject

@HiltViewModel
class RecipeReplacementViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var replacements: ObservableArrayList<RecipeReplacementAdapter.ReplacementItem> =
        ObservableArrayList()

    fun loadReplacements(recipeId: Int, baseRecipeId: Int) {
        processTask(
            userRepository.loadRecipeReplacements(recipeId, baseRecipeId)
                .subscribeWith(simpleSingleObserver<List<RecipeReplacementAdapter.ReplacementItem>> {
                    replacements.clearAndAddAll(it)
                    isProgress.set(false)
                })
        )
    }

    fun replaceRecipe(
        recipeId: Int,
        baseRecipeId: Int,
        replacement: RecipeUIModel,
        nutritionPlanId: Int,
        dayNumber: Int,
        onComplete: () -> Unit,
    ) {
        processTask(
            userRepository.replaceRecipe(
                recipeId = recipeId,
                baseRecipeId = baseRecipeId,
                replacementId = replacement.id,
                nutritionPlanId = nutritionPlanId,
                dayNumber = dayNumber
            ).subscribeWith(simpleCompletableObserver(onComplete))
        )
    }

    fun getMarathonId() = userRepository.getCurrentMarathonId()

}