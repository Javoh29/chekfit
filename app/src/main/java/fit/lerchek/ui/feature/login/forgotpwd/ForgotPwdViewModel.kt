package fit.lerchek.ui.feature.login.forgotpwd

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.exceptions.LoginError
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.SupportUtils
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class ForgotPwdViewModel @Inject constructor(private val userRepository: UserRepository) : BaseViewModel() {

    enum class State{
        CODE_REQUEST,
        CODE_CONFIRM
    }

    private val mSuccess = MutableLiveData<Event<State>>()
    val success: LiveData<Event<State>>
        get() = mSuccess

    var email: ObservableField<String> = ObservableField("")
    var emailError: ObservableField<String> = ObservableField()
    var code: ObservableField<String> = ObservableField("")

    fun codeRequest(){
        processTask(userRepository.requestForgotPwdCode(email.get()).subscribeWith(object : DisposableSingleObserver<Boolean>() {
            override fun onStart() {
                super.onStart()
                emailError.set(null)
            }

            override fun onSuccess(success: Boolean) {
                this@ForgotPwdViewModel.mSuccess.postValue(Event(State.CODE_REQUEST))
            }

            override fun onError(e: Throwable) {
                if (e is LoginError) {
                    emailError.set(e.emailError)
                }
            }
        }))
    }

    fun sendConfirmCode(){
        processTask(userRepository.confirmForgotPwdCode(code.get() , email.get()).subscribeWith(object : DisposableSingleObserver<Boolean>() {
            override fun onStart() {
                super.onStart()
                emailError.set(null)
            }

            override fun onSuccess(success: Boolean) {
                this@ForgotPwdViewModel.mSuccess.postValue(Event(State.CODE_CONFIRM))
            }

            override fun onError(e: Throwable) {
                if (e is LoginError) {
                    emailError.set(e.emailError)
                }
            }
        }

        ))
    }

    fun canSendCode() : Boolean{
        return !code.get().isNullOrEmpty() && code.get()?.length?:0 > 3 && SupportUtils.isValidEmail(email.get())
    }
}