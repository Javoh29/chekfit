package fit.lerchek.ui.feature.marathon.progress

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.api.model.Week
import fit.lerchek.data.domain.uimodel.marathon.MarathonProgressUIModel
import fit.lerchek.data.domain.uimodel.marathon.MeasurementsUIModel
import fit.lerchek.databinding.FragmentMakeMeasurementsBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.FaqFragment
import fit.lerchek.ui.feature.marathon.MarathonActivity
import fit.lerchek.ui.feature.marathon.progress.crop.CropActivity
import fit.lerchek.ui.feature.marathon.progress.crop.UploadPhotoListener
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class MakeMeasurementsFragment : BaseToolbarDependentFragment(R.layout.fragment_make_measurements),
    MakeMeasurementsCallback,
    UploadPhotoListener {
    private val binding by viewBindings(FragmentMakeMeasurementsBinding::bind)
    private val viewModel by viewModels<MakeMeasurementsViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override var isBottomNavigationVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.nsvMakeMeasurements
        binding.lifecycleOwner = viewLifecycleOwner
        binding.model = viewModel
        binding.callback = this

        viewModel.loadMeasurements()
        binding.apply {

            progressMakePhotos.textFrontPhoto.setOnClickListener {
                model?.checkFrontPhoto?.set(true)
                model?.checkBackPhoto?.set(false)
                model?.checkSidePhoto?.set(false)
                updatePhotos(model?.progress?.get())
            }
            progressMakePhotos.textBackPhoto.setOnClickListener {
                model?.checkFrontPhoto?.set(false)
                model?.checkBackPhoto?.set(true)
                model?.checkSidePhoto?.set(false)
                updatePhotos(model?.progress?.get())
            }
            progressMakePhotos.textSidePhoto.setOnClickListener {
                model?.checkFrontPhoto?.set(false)
                model?.checkBackPhoto?.set(false)
                model?.checkSidePhoto?.set(true)
                updatePhotos(model?.progress?.get())
            }

            model?.progress?.addOnPropertyChangedCallback(object :
                Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                    if(getView()!=null) {
                        (sender as? ObservableField<*>)?.get()?.let {
                            if (getView() != null) {
                                updatePhotos(it as MarathonProgressUIModel)
                            }
                        }
                    }
                }
            })

            progressResult.itemWeight.setOnClickListener {
                val type = MeasurementsUIModel.Type.WEIGHT
                val week = viewModel.selectedWeek.get()
                val value = viewModel.progress.get()?.measurement(type, week) ?: -1F
                navigateTo(
                    MakeMeasurementsFragmentDirections.actionNavigationMakeMeasurementsToNavigationAddEditMeasurements(
                        type.name, week, value
                    )
                )
            }

            progressResult.itemWaist.setOnClickListener {
                val type = MeasurementsUIModel.Type.WAIST
                val week = viewModel.selectedWeek.get()
                val value = viewModel.progress.get()?.measurement(type, week) ?: -1F
                navigateTo(
                    MakeMeasurementsFragmentDirections.actionNavigationMakeMeasurementsToNavigationAddEditMeasurements(
                        type.name, week, value
                    )
                )
            }

            progressResult.itemBreast.setOnClickListener {
                val type = MeasurementsUIModel.Type.BREAST
                val week = viewModel.selectedWeek.get()
                val value = viewModel.progress.get()?.measurement(type, week) ?: -1F
                navigateTo(
                    MakeMeasurementsFragmentDirections.actionNavigationMakeMeasurementsToNavigationAddEditMeasurements(
                        type.name, week, value
                    )
                )
            }

            progressResult.itemHips.setOnClickListener {
                val type = MeasurementsUIModel.Type.HIPS
                val week = viewModel.selectedWeek.get()
                val value = viewModel.progress.get()?.measurement(type, week) ?: -1F
                navigateTo(
                    MakeMeasurementsFragmentDirections.actionNavigationMakeMeasurementsToNavigationAddEditMeasurements(
                        type.name, week, value
                    )
                )
            }

            progressResult.itemReports.setOnClickListener {
                navigateTo(R.id.navigation_reports)
            }
        }
    }

    private fun updatePhotos(progress: MarathonProgressUIModel?) {
        val isMale = progress?.isMale ?: false
        val photoRes = when {
            viewModel.checkFrontPhoto.get() -> if (isMale) R.drawable.ic_front_photo_male else R.drawable.ic_front_photo_female
            viewModel.checkBackPhoto.get() -> if (isMale) R.drawable.ic_back_photo_male else R.drawable.ic_back_photo_female
            else -> if (isMale) R.drawable.ic_side_photo_male else R.drawable.ic_side_photo_female
        }
        binding.progressMakePhotos.imgPhoto.setImageResource(photoRes)

        progress?.photo(viewModel.selectedWeek.get())?.let { photo ->
            when {
                viewModel.checkFrontPhoto.get() -> photo.front
                viewModel.checkBackPhoto.get() -> photo.back
                viewModel.checkSidePhoto.get() -> photo.side
                else -> null
            }?.let { url ->
                binding.progressMakePhotos.imgPhoto.apply {
                    Glide.with(this.context).load(url).into(this)
                }
            }
        }
    }

    override fun onFAQClick() {
        navigateTo(
            MakeMeasurementsFragmentDirections.actionNavigationMakeMeasurementsToNavigationFaq(
                FaqFragment.TAG_ALIAS_PROGRESS
            )
        )
    }

    override fun onSelectWeekClick(weeks: List<Week>) {
        mContext.showSimpleOptionsDialog(weeks.map { it.name }.toTypedArray()) {
            viewModel.selectWeek(it)
        }
    }

    override fun onPhotoUploaded() {}

    override fun onUploadPhoto(week: Int) {
        val type = when {
            viewModel.checkFrontPhoto.get() -> CropActivity.TYPE_FRONT
            viewModel.checkBackPhoto.get() -> CropActivity.TYPE_BACK
            else -> CropActivity.TYPE_SIDE
        }
        Intent(mContext, CropActivity::class.java).apply {
            putExtra(CropActivity.EXTRA_TYPE, type)
            putExtra(CropActivity.EXTRA_WEEK, week)
            mContext.getActivityResultLauncher(MarathonActivity.REQUEST_CODE_UPLOAD_PROGRESS_PHOTO)
                ?.launch(this)
        }
    }
}