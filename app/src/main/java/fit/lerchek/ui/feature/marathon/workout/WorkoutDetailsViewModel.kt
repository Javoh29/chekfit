package fit.lerchek.ui.feature.marathon.workout

import android.annotation.SuppressLint
import android.text.SpannableStringBuilder
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.api.model.VimeoModel
import fit.lerchek.data.domain.uimodel.marathon.WorkoutUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class WorkoutDetailsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    private var mWorkoutContent = MutableLiveData<SpannableStringBuilder>()

    val vimeoModel = MutableLiveData<VimeoModel>()
    val workoutContent: LiveData<SpannableStringBuilder>
        get() = mWorkoutContent

    fun loadWorkoutDetails(rawContent: String) {
        processTask(
            userRepository.generateDynamicContent(rawContent)
                .subscribeWith(simpleSingleObserver<SpannableStringBuilder> {
                    mWorkoutContent.postValue(it)
                    isProgress.set(false)
                })
        )
    }

    @SuppressLint("CheckResult")
    fun getVimeoVideo(videoId: String, h: String?) {
        userRepository.getVimeoConfig(videoId, h).subscribeWith(simpleSingleObserver<VimeoModel> {
            vimeoModel.value = it
        })
    }
}