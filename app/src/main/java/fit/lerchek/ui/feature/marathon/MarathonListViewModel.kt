package fit.lerchek.ui.feature.marathon

import androidx.databinding.ObservableArrayList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.MarathonUIModel
import fit.lerchek.data.managers.DataManager
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class MarathonListViewModel @Inject constructor(
    private val dataManager: DataManager,
    private val userRepository: UserRepository
) : BaseViewModel() {

    var marathons: ObservableArrayList<MarathonUIModel> = ObservableArrayList()

    private val mSuccessLogout = MutableLiveData<Event<Boolean>>()
    val successLogout: LiveData<Event<Boolean>>
        get() = mSuccessLogout

    init {
        resetMarathonMenu()
    }

    private fun resetMarathonMenu() {
        userRepository.clearAppMenuCache()
    }

    fun loadMarathons() {
        processTask(
            userRepository.loadMarathons()
                .subscribeWith(simpleSingleObserver<List<MarathonUIModel>> {
                    marathons.clearAndAddAll(it)
                    isProgress.set(false)
                })
        )
    }

    fun logout() {
        processTask(
            userRepository.logout().subscribeWith(object : DisposableSingleObserver<Boolean>() {
                override fun onSuccess(success: Boolean) {
                    mSuccessLogout.postValue(Event(success))
                }

                override fun onError(e: Throwable) {
                    mSuccessLogout.postValue(Event(false))
                }
            })
        )
    }

    fun onMarathonSelected(marathon: MarathonUIModel){
        dataManager.saveCurrentMarathonImageUrl(marathon.imageUrl)
    }


}