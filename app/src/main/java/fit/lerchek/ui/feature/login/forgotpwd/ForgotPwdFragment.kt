package fit.lerchek.ui.feature.login.forgotpwd

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentForgotPwdBinding
import fit.lerchek.ui.base.BaseFragment
import fit.lerchek.ui.util.viewBindings
import fit.lerchek.util.EventObserver

@AndroidEntryPoint
class ForgotPwdFragment : BaseFragment(R.layout.fragment_forgot_pwd) {

    private val binding by viewBindings(FragmentForgotPwdBinding::bind)

    private val viewModel by viewModels<ForgotPwdViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.success.observe(this, EventObserver { state ->
            when (state){
                ForgotPwdViewModel.State.CODE_CONFIRM -> {
                    navigateTo(ForgotPwdFragmentDirections.actionForgotPwdFragmentToSetNewPwdFragment(viewModel.email.get()?:"" , viewModel.code.get()?:""))
                }
                ForgotPwdViewModel.State.CODE_REQUEST -> {
                    Toast.makeText(mContext, "Код был успешно отправлен!", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        binding.back.root.setOnClickListener {
            navigateUp()
        }

        binding.requestCode.setOnClickListener {
            viewModel.codeRequest()
        }
        binding.sendCode.setOnClickListener {
            viewModel.sendConfirmCode()
        }

        binding.email.doAfterTextChanged {
            viewModel.emailError.set(null)
            binding.sendCode.isEnabled = viewModel.canSendCode()
        }

        binding.code.doAfterTextChanged {
            binding.sendCode.isEnabled = viewModel.canSendCode()
        }
    }

}