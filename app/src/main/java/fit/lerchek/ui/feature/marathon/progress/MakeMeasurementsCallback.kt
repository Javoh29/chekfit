package fit.lerchek.ui.feature.marathon.progress

import fit.lerchek.data.api.model.Week

interface MakeMeasurementsCallback {
    fun onFAQClick()
    fun onSelectWeekClick(weeks: List<Week>)
    fun onUploadPhoto(week: Int)
}