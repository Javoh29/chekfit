package fit.lerchek.ui.feature.marathon

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.MarathonUIModel
import fit.lerchek.databinding.FragmentMarathonListBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.login.LoginActivity
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.viewBindings
import fit.lerchek.util.EventObserver

@AndroidEntryPoint
class MarathonListFragment : BaseToolbarDependentFragment(R.layout.fragment_marathon_list) {

    private val binding by viewBindings(FragmentMarathonListBinding::bind)
    private val viewModel by viewModels<MarathonListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.rvMarathons
        binding.viewModel = viewModel
        binding.callback = object : MarathonsAdapter.SelectMarathonCallback {
            override fun onSelectItem(marathon: MarathonUIModel) {
                viewModel.onMarathonSelected(marathon)
                navigateTo(
                    MarathonListFragmentDirections.actionNavigationMarathonListToNavigationToday(
                        marathon.id, ""
                    )
                )
            }
        }
        viewModel.loadMarathons()
        viewModel.successLogout.observe(viewLifecycleOwner, EventObserver { success ->
            if (success) {
                startActivity(Intent(requireActivity(), LoginActivity::class.java))
                requireActivity().finish()
            }
        })
    }

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onError(code: String, message: String, goBack: Boolean) {
        ToastUtils.show(mContext, message)
        if (code == "401")
            viewModel.logout()
    }
}