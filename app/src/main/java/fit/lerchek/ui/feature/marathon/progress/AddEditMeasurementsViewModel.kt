package fit.lerchek.ui.feature.marathon.progress

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.api.message.marathon.TaskToggleResponse
import fit.lerchek.data.api.model.Body
import fit.lerchek.data.domain.uimodel.marathon.AddEditMeasurementsUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.TextUtils.convertWithOneDecimal
import javax.inject.Inject

@HiltViewModel
class AddEditMeasurementsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val data: ObservableField<AddEditMeasurementsUIModel> = ObservableField()
    val value: ObservableField<String> = ObservableField("")
    var week: Int = 0

    val savedState: MutableLiveData<Boolean> by lazy { MutableLiveData() }

    fun loadMeasurement(type: String, week: Int, value: Float) {
        this.data.set(
            AddEditMeasurementsUIModel(
                AddEditMeasurementsUIModel.Type.valueOf(type)
            )
        )
        this.week = week
        this.value.set(if (value == -1F) "" else value.convertWithOneDecimal())
    }

    fun save() {
        isProgress.set(true)
        val body = Body().apply {
            val value = value.get()?.replace(",",".")?.toFloatOrNull() ?: return
            when (data.get()?.type) {
                AddEditMeasurementsUIModel.Type.WEIGHT -> weight = value
                AddEditMeasurementsUIModel.Type.WAIST -> waist = value
                AddEditMeasurementsUIModel.Type.BREAST -> breast_volume = value
                AddEditMeasurementsUIModel.Type.HIPS -> hips = value
            }
            week = this@AddEditMeasurementsViewModel.week
        }
        userRepository.saveBodyProgressForMarathon(body = body)
            ?.subscribeWith(simpleSingleObserver<TaskToggleResponse> {
                savedState.postValue(true)
                isProgress.set(false)
            })?.let { processTask(it) }
    }

}