package fit.lerchek.ui.feature.marathon

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentDynamicContentBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class DynamicContentFragment : BaseToolbarDependentFragment(R.layout.fragment_dynamic_content), LifecycleHolder {
    private val binding by viewBindings(FragmentDynamicContentBinding::bind)
    private val viewModel by viewModels<DynamicContentViewModel>()
    private val args: DynamicContentFragmentArgs by navArgs()

    companion object {
        const val TAG_ALIAS_INSTRUCTION = "instruction"
        const val TAG_ALIAS_SWEET_POTATO = "sweet-potato"
        const val TAG_ALIAS_SKIN_CARE = "skin-care"
        const val TAG_ALIAS_CHECK_LIST = "check-list"
        const val TAG_ALIAS_CHECK_UP_WOMEN = "check-up-women"
        const val TAG_ALIAS_CHECK_UP_MEN = "check-up-men"
        const val TAG_ALIAS_CARDIO_WOMEN = "cardio-women"
        const val TAG_ALIAS_CARDIO_MEN = "cardio-men"
        const val TAG_ALIAS_VACUUM = "vacuum"
        const val TAG_ALIAS_MAP = "map"
    }


    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadContent(args.alias)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.lifecycleHolder = this
    }

    override fun getMyLifecycle() = activity?.lifecycle ?: lifecycle

    override fun onBack() {
        navigateUp()
    }
}