package fit.lerchek.ui.feature.marathon.progress.crop

interface UploadPhotoListener {
    fun onPhotoUploaded()
}