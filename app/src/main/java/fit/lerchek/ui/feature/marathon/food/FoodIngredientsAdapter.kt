package fit.lerchek.ui.feature.marathon.food

import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.DynamicDrawableSpan
import android.text.style.ImageSpan
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.data.domain.uimodel.marathon.TodayTasksItem
import fit.lerchek.databinding.ItemTodayTaskBinding
import fit.lerchek.ui.feature.marathon.today.callbacks.TodayCallback

class FoodIngredientsAdapter(
    private var ingredients: List<TaskUIModel>,
    private var callback: FoodIngredientsCallback?
) : RecyclerView.Adapter<FoodIngredientsAdapter.IngredientViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientViewHolder {
        return IngredientViewHolder(
            ItemTodayTaskBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: IngredientViewHolder, position: Int) {
        holder.bind(ingredients[position], callback) {
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return ingredients.size
    }

    class IngredientViewHolder(val binding: ItemTodayTaskBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(ingredient: TaskUIModel, callback: FoodIngredientsCallback?, notifyUpdate: () -> Unit) {
            binding.model = ingredient
            binding.executePendingBindings()
            updateItem(binding.tvTask, ingredient)
            val listener = CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                ingredient.checked = isChecked
                callback?.updateIngredientState(ingredient)
                buttonView.post {
                    notifyUpdate()
                }
            }
            binding.cbTask.apply {
                setOnCheckedChangeListener(null)
                isChecked = ingredient.checked
                setOnCheckedChangeListener(listener)
            }
        }

        private fun updateItem(
            textView: AppCompatTextView,
            task: TaskUIModel,
        ) {
            SpannableString(task.title).apply {
                if (task.checked) {
                    setSpan(StrikethroughSpan(), 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                }
                textView.text = this
            }
        }
    }

}


@BindingAdapter("ingredients", "ingredients_callback")
fun setupIngredientsAdapter(
    list: RecyclerView,
    ingredients: List<TaskUIModel>?,
    callback: FoodIngredientsCallback?
) {
    ingredients?.let {
        if (!it.isNullOrEmpty()) {
            list.adapter = FoodIngredientsAdapter(it, callback)
        }
    }
}