package fit.lerchek.ui.feature.marathon.profile.personal

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class PersonalParamViewModel @Inject constructor(private val userRepository: UserRepository) : BaseViewModel() {

    val personalParams: ObservableField<ProfileBodyParamsUIModel> = ObservableField()

    val success = MutableLiveData<Event<Boolean>>()

    init {
        loadUsersParams()
    }

    private fun loadUsersParams(){
        val request = (userRepository.getProfileParamsByMarathon()?.subscribeWith(
            object : DisposableSingleObserver<ProfileBodyParamsUIModel>(){
            override fun onStart() {
                super.onStart()
                isProgress.set(true)
            }

            override fun onError(e: Throwable) {
                isProgress.set(false)
            }

            override fun onSuccess(t: ProfileBodyParamsUIModel) {
                personalParams.set(t)
                isProgress.set(false)
            }

        }))

        if (request != null) {
            processTask(request)
        }
    }

    fun savePersonalParam() {
        if (personalParams.get() == null){
            return
        }
        val update = (userRepository.updateBodyParamsByMarathon(request = personalParams.get()!!.buildUpdateBodyParamsRequest())?.subscribeWith(object : DisposableSingleObserver<ProfileBodyParamsUIModel>(){

            override fun onStart() {
                super.onStart()
                isProgress.set(true)
            }

            override fun onSuccess(t: ProfileBodyParamsUIModel) {
                personalParams.set(t)
                isProgress.set(false)
                success.value = Event(true)
            }

            override fun onError(e: Throwable) {
                isProgress.set(false)
                success.value = Event(false)
            }

        }))

        if (update != null) {
            processTask(update)
        }
    }
}
