package fit.lerchek.ui.feature.marathon.more

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentMoreBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.DynamicContentFragment
import fit.lerchek.ui.feature.marathon.DynamicContentFragmentArgs
import fit.lerchek.ui.util.BrowserUtils
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class MoreFragment : BaseToolbarDependentFragment(R.layout.fragment_more) {

    companion object {
        private const val BASE_TRACKERS_URL = "https://lerchek.fit/storage/trackers/"
        private const val LINK_WATER = "${BASE_TRACKERS_URL}water.jpg"
        private const val LINK_DREAM = "${BASE_TRACKERS_URL}dream.jpg"
        private const val LINK_CARE = "${BASE_TRACKERS_URL}care.jpg"
        private const val LINK_STEP = "${BASE_TRACKERS_URL}step.jpg"
    }

    private val binding by viewBindings(FragmentMoreBinding::bind)
    private val viewModel by viewModels<MoreViewModel>()

    override var isBottomNavigationVisible = true

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.apply {
            itemNutrients.setOnClickListener {
                navigateTo(R.id.navigation_products)
            }
            itemRecipes.setOnClickListener {
                navigateTo(MoreFragmentDirections.actionNavigationMoreToNavigationExtraRecipes())
            }
            itemYam.setOnClickListener {
                navigateToPageByAlias(DynamicContentFragment.TAG_ALIAS_SWEET_POTATO)
            }
            itemSkinCare.setOnClickListener {
                navigateToPageByAlias(DynamicContentFragment.TAG_ALIAS_SKIN_CARE)
            }
            itemProductsCheckList.setOnClickListener {
                navigateToPageByAlias(DynamicContentFragment.TAG_ALIAS_CHECK_LIST)
            }
            itemHealthChekUp.setOnClickListener {
                navigateToPageByAlias(
                    if (viewModel?.profileIsMale?.get() == true)
                        DynamicContentFragment.TAG_ALIAS_CHECK_UP_MEN
                    else
                        DynamicContentFragment.TAG_ALIAS_CHECK_UP_WOMEN
                )
            }
            itemCalorieCalculator.setOnClickListener {
                navigateTo(R.id.navigation_calorie_calculator)
            }
            itemFatCalculator.setOnClickListener {
                navigateTo(R.id.navigation_fat_calculator)
            }
            itemBMIcalculator.setOnClickListener {
                navigateTo(R.id.navigation_bmi_calculator)
            }
            itemWaterTracker.setOnClickListener {
                navigateTo(MoreFragmentDirections.actionNavigationMoreToNavigationTracker("water"))
            }
            itemSleepTracker.setOnClickListener {
                navigateTo(MoreFragmentDirections.actionNavigationMoreToNavigationTracker("dream"))
            }
            itemSelfCareTracker.setOnClickListener {
                BrowserUtils.openBrowser(mContext, LINK_CARE)
            }
            itemCardio.setOnClickListener {
                navigateToPageByAlias(
                    if (viewModel?.profileIsMale?.get() == true)
                        DynamicContentFragment.TAG_ALIAS_CARDIO_MEN
                    else
                        DynamicContentFragment.TAG_ALIAS_CARDIO_WOMEN
                )
            }
            itemVacuum.setOnClickListener {
                navigateToPageByAlias(DynamicContentFragment.TAG_ALIAS_VACUUM)
            }
            itemStepTracker.setOnClickListener {
                navigateTo(MoreFragmentDirections.actionNavigationMoreToNavigationTracker("step"))
            }
            itemCardioTest.setOnClickListener {
                navigateTo(R.id.navigation_cardio_test)
            }
        }
    }

    private fun navigateToPageByAlias(alias: String) {
        navigateTo(
            R.id.navigation_dynamic_content_page,
            DynamicContentFragmentArgs.Builder()
                .setAlias(alias)
                .build().toBundle()
        )
    }
}