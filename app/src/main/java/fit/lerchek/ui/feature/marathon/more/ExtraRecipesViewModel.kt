package fit.lerchek.ui.feature.marathon.more

import androidx.databinding.ObservableArrayList
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import javax.inject.Inject

@HiltViewModel
class ExtraRecipesViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var recipes: ObservableArrayList<ExtraRecipesAdapter.ExtraRecipeItem> =
        ObservableArrayList()

    private var extraRecipesCache: ExtraRecipesUIModel? = null

    fun loadExtraRecipes(filterUIModel: ExtraRecipesFilterUIModel? = null) {
        processTask(
            userRepository.loadExtraRecipes(filterUIModel, extraRecipesCache)
                .subscribeWith(simpleSingleObserver<ExtraRecipesUIModel> {
                    extraRecipesCache = it
                    recipes.clearAndAddAll(it.displayItems)
                    isProgress.set(false)
                })
        )
    }

    fun selectMeal(meal: MealUIModel) {
        getCurrentFilter()?.let {
            it.selectMeal(meal.id)
            loadExtraRecipes(it)
        }
    }

    private fun getCurrentFilter(): ExtraRecipesFilterUIModel? {
        return extraRecipesCache?.displayItems?.firstOrNull {
            it is ExtraRecipesFilterUIModel
        } as ExtraRecipesFilterUIModel?
    }

}