package fit.lerchek.ui.feature.marathon.more

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.data.domain.uimodel.marathon.CdfcFilterUIModel
import fit.lerchek.data.domain.uimodel.marathon.MealUIModel
import fit.lerchek.data.domain.uimodel.marathon.ProductUIModel
import fit.lerchek.databinding.ItemProductBinding
import fit.lerchek.databinding.ItemProductsFilterBinding

class ProductsAdapter(
    private var data: List<ProductItem>,
    private var callback: ProductsCallback?
) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_FILTER = 0
        private const val ITEM_VIEW_TYPE_PRODUCT = 1
    }

    interface ProductsCallback {
        fun onSelectCategory(categories: List<MealUIModel>)
        fun onBack()
    }

    interface ProductItem

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class FilterViewHolder(val itemFilterBinding: ItemProductsFilterBinding) :
        ViewHolder(itemFilterBinding)

    class ItemViewHolder(val itemBinding: ItemProductBinding) :
        ViewHolder(itemBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_FILTER -> FilterViewHolder(
                ItemProductsFilterBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_PRODUCT -> ItemViewHolder(
                ItemProductBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is ProductUIModel) ITEM_VIEW_TYPE_PRODUCT else ITEM_VIEW_TYPE_FILTER
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is FilterViewHolder -> {
                holder.itemFilterBinding.apply {
                    val filter = data[position] as CdfcFilterUIModel
                    this.model = filter
                    imgMeal.let {
                        Glide.with(it.context).load(filter.getSelectedType()?.imageUrl).into(it)
                    }
                    btnChooseMeal.setOnClickListener {
                        callback?.onSelectCategory(filter.types)
                    }
                    btnBack.root.setOnClickListener {
                        callback?.onBack()
                    }
                }
            }
            is ItemViewHolder -> {
                val product = (data[position] as ProductUIModel)
                holder.itemBinding.apply {
                    model = product
                    Glide.with(imgProduct.context).load(product.imageUrl).into(imgProduct)
                }
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}

@BindingAdapter("products", "products_callback")
fun setupProductsAdapter(
    list: RecyclerView,
    items: List<ProductsAdapter.ProductItem>?,
    callback: ProductsAdapter.ProductsCallback?
) {
    items?.let {
        list.layoutManager = GridLayoutManager(list.context, 2).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                @Override
                override fun getSpanSize(position: Int): Int {
                    return if (position == 0) 2 else 1
                }
            }
        }
        list.adapter = ProductsAdapter(it, callback)
    }
}