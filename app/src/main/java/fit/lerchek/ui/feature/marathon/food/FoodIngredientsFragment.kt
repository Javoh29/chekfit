package fit.lerchek.ui.feature.marathon.food

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.data.domain.uimodel.marathon.WeekUIModel
import fit.lerchek.databinding.FragmentFoodIngredientsBinding
import fit.lerchek.ui.base.BaseToolbarTitleFragment
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class FoodIngredientsFragment : BaseToolbarTitleFragment(R.layout.fragment_food_ingredients), FoodIngredientsCallback {

    private val binding by viewBindings(FragmentFoodIngredientsBinding::bind)
    private val viewModel by viewModels<FoodIngredientsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadIngredients()
    }

    override fun getToolbarTitle() = mContext.getString(R.string.ingredients_list)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        binding.btnCheckAll.setOnClickListener {
            viewModel.checkAllIngredients()
        }
    }

    override fun updateIngredientState(ingredient: TaskUIModel) {
        viewModel.updateIngredientState(ingredient)
    }

    override fun onSelectWeekClick(weeks: List<WeekUIModel>) {
        mContext.showSimpleOptionsDialog(weeks.map { it.name }.toTypedArray()){
            viewModel.selectWeek(weeks[it])
        }
    }
}