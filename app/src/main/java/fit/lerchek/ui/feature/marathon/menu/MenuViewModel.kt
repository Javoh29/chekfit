package fit.lerchek.ui.feature.marathon.menu

import androidx.databinding.ObservableArrayList
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.rx.CustomSingleObserver
import fit.lerchek.data.domain.uimodel.MenuItemUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import javax.inject.Inject

@HiltViewModel
class MenuViewModel @Inject constructor(
    private val userRepository: UserRepository,
) : BaseViewModel() {

    val menuItems: ObservableArrayList<MenuItemUIModel> = ObservableArrayList()

    init {
        loadMenuItems()
    }

    private fun loadMenuItems() {
        processTask(userRepository.loadMenuItems().subscribeWith(
            object : CustomSingleObserver<List<MenuItemUIModel>>() {
                override fun onSuccess(result: List<MenuItemUIModel>) {
                    menuItems.clearAndAddAll(result)
                }
            }
        ))
    }
}