package fit.lerchek.ui.feature.marathon.today.callbacks

import androidx.databinding.ObservableField
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest

interface TrackerCallBack {
    fun onChangeBtn()
    fun getSelectParam(): ObservableField<Int>
    fun getSelectPeriod(): String
    fun getTrackerType(): String
    fun onClickSetting(settingType: String, date: String, dimension: Int, goal: Int, value: Int)
    fun onClickPeriod(period: String)
    fun changeCountTracker(request: UpdateTrackerRequest, type: String)
    fun changeSwitchFit(index: Int)
    fun isCheckFit(): Boolean
}