package fit.lerchek.ui.feature.marathon.workout

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import cn.jzvd.JzvdStd
import com.bumptech.glide.Glide
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentWorkoutDetailsBinding
import fit.lerchek.ui.base.BaseToolbarTitleFragment
import fit.lerchek.ui.util.viewBindings


@AndroidEntryPoint
class WorkoutDetailsFragment : BaseToolbarTitleFragment(R.layout.fragment_workout_details) {

    private val binding by viewBindings(FragmentWorkoutDetailsBinding::bind)
    private val viewModel by viewModels<WorkoutDetailsViewModel>()
    private val args: WorkoutDetailsFragmentArgs by navArgs()

    private var youTubePlayer: YouTubePlayer? = null

    override fun getToolbarTitle() = args.title

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadWorkoutDetails(args.content)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        if (args.videoType) {
            binding.root.findViewById<YouTubePlayerView>(R.id.youtube_player_view)?.let { playerView ->
                lifecycle.addObserver(playerView)
                playerView.apply {
                    addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                        override fun onReady(youTubePlayer: YouTubePlayer) {
                            this@WorkoutDetailsFragment.youTubePlayer = youTubePlayer
                            cueVideo(args.videoId)
                        }
                    })
                    getPlayerUiController().apply {
                        //не работает сокрытие. но кнопки не кликабельны
                        //https://github.com/PierfrancescoSoffritti/android-youtube-player/issues/513
                        showMenuButton(false)
                        showVideoTitle(false)
                    }
                }
            }
        } else {
            var h: String? = null
            if (args.videoId.split("?").size > 1) h = "?${args.videoId.split("?").last()}"
            viewModel.getVimeoVideo(args.videoId.split("?").first(), h)
            binding.root.findViewById<YouTubePlayerView>(R.id.youtube_player_view).visibility = View.GONE
            binding.root.findViewById<JzvdStd>(R.id.jz_video)?.visibility = View.VISIBLE
            viewModel.vimeoModel.observe(viewLifecycleOwner, {
                if (it != null) {
                    binding.root.findViewById<JzvdStd>(R.id.jz_video)?.apply {
                        setUp(
                            it.videoUrl,
                            ""
                        )
                        mRetryBtn.setOnClickListener { viewModel.getVimeoVideo(args.videoId.split("?").first(), h) }
                        posterImageView?.let { img ->
                            Glide.with(img.context).load(it.posterImage).into(img)
                        }
                    }
                }
            })
        }

        viewModel.workoutContent.apply {
            removeObservers(viewLifecycleOwner)
            observe(viewLifecycleOwner) { workoutContent ->
                binding.root.findViewById<AppCompatTextView>(R.id.tvName).text = args.title
                binding.root.findViewById<AppCompatTextView>(R.id.tvDescription)?.apply {
                    if (workoutContent.isNullOrEmpty().not()) {
                        setText(workoutContent, TextView.BufferType.SPANNABLE)
                    }
                }
            }
        }
    }

    private fun cueVideo(videoId: String) {
        youTubePlayer?.cueVideo(videoId, 0f)
    }
}