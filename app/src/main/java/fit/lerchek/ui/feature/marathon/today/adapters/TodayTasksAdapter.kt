package fit.lerchek.ui.feature.marathon.today.adapters

import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.DynamicDrawableSpan
import android.text.style.ImageSpan
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.databinding.ItemTodayTaskBinding
import fit.lerchek.ui.feature.marathon.today.callbacks.TaskCallback

class TodayTasksAdapter(
    private val currentDay: Int,
    private var tasks: List<TaskUIModel>,
    private var callback: TaskCallback?
) : RecyclerView.Adapter<TodayTasksAdapter.TaskViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(
            ItemTodayTaskBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(currentDay, tasks[position], callback) {
            notifyItemChanged(position)
        }
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    class TaskViewHolder(val binding: ItemTodayTaskBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            currentDay: Int,
            task: TaskUIModel,
            callback: TaskCallback?,
            notifyUpdate: () -> Unit
        ) {
            binding.model = task
            binding.executePendingBindings()
            updateItem(binding.tvTask, task, callback)
            val listener = CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                task.checked = isChecked
                callback?.updateTaskCompletion(task, currentDay)
                buttonView.post {
                    notifyUpdate()
                }
            }
            binding.cbTask.apply {
                setOnCheckedChangeListener(null)
                isChecked = task.checked
                setOnCheckedChangeListener(listener)
            }
        }

        private fun updateItem(
            textView: AppCompatTextView,
            task: TaskUIModel,
            callback: TaskCallback?
        ) {
            val clickableSpan = object : ClickableSpan() {
                override fun onClick(textView: View) {
                    callback?.onTaskInfoClick(task)
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                }
            }
            SpannableString(task.title + "   ").apply {
                val spanFlags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                if (task.checked) {
                    setSpan(StrikethroughSpan(), 0, length - 3, spanFlags)
                }
                if (task.comment.isNotEmpty()) {
                    ContextCompat.getDrawable(
                        textView.context, R.drawable.ic_info_btn
                    )?.let { icon ->
                        icon.setBounds(0, 0, icon.intrinsicWidth, icon.intrinsicHeight)
                        val imageSpan = ImageSpan(icon, DynamicDrawableSpan.ALIGN_BOTTOM)
                        setSpan(imageSpan, length - 1, length, spanFlags)
                        setSpan(clickableSpan, length - 1, length, spanFlags)
                        textView.movementMethod = LinkMovementMethod.getInstance()
                    }
                }
                textView.text = this
            }
        }
    }

}


@BindingAdapter("current_day", "today_tasks", "today_tasks_callback")
fun setupTodayTasksAdapter(
    list: RecyclerView,
    currentDay: Int?,
    tasks: List<TaskUIModel>?,
    callback: TaskCallback?
) {
    if (currentDay != null && tasks != null) {
        list.adapter = TodayTasksAdapter(currentDay, tasks, callback)
    }
}