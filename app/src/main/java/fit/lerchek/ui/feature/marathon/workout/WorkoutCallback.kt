package fit.lerchek.ui.feature.marathon.workout

import fit.lerchek.data.api.model.VimeoModel
import fit.lerchek.data.domain.uimodel.marathon.DayUIModel
import fit.lerchek.data.domain.uimodel.marathon.PlanUIModel
import fit.lerchek.data.domain.uimodel.marathon.WeekUIModel
import fit.lerchek.ui.feature.marathon.LifecycleHolder
import io.reactivex.Single

interface WorkoutCallback: LifecycleHolder {
    fun onFAQClick()
    fun onSelectWeekClick(weeks: List<WeekUIModel>)
    fun onSelectLevelClick(levels: List<PlanUIModel>)
    fun onSelectDayClick(dayUIModel: DayUIModel)
    fun getVimeoData(videoId: String, h: String?, call: (vimeoModel: VimeoModel) -> Unit)
}