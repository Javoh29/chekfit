package fit.lerchek.ui.feature.marathon.voting

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.VotingHeaderUIModel
import fit.lerchek.data.domain.uimodel.marathon.VotingItem
import fit.lerchek.data.domain.uimodel.marathon.VotingItemUIModel
import fit.lerchek.databinding.ItemVotingHeaderBinding
import fit.lerchek.databinding.ItemVotingUserBinding
import fit.lerchek.ui.util.clearAndAddAll

class VotingAdapter(
    private var callback: VotingCallback?
) :
    RecyclerView.Adapter<VotingAdapter.ViewHolder>() {

    private var data: MutableList<VotingItem> = arrayListOf()

    companion object {
        const val ITEM_VIEW_TYPE_HEADER = 0
        const val ITEM_VIEW_TYPE_USER = 1
    }

    fun update(items: List<VotingItem>) {
        data.clearAndAddAll(items)
        notifyItemRangeChanged(0, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder(
                ItemVotingHeaderBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_USER -> ItemViewHolder(
                ItemVotingUserBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is VotingHeaderUIModel) {
            ITEM_VIEW_TYPE_HEADER
        } else {
            ITEM_VIEW_TYPE_USER
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            val header = data[position] as VotingHeaderUIModel
            holder.itemHeaderBinding.apply {
                model = header
                tvLink.movementMethod = LinkMovementMethod.getInstance()
                val ssb = SpannableStringBuilder(header.getVotesState())
                ssb.setSpan(
                    ForegroundColorSpan(
                        ContextCompat.getColor(
                            tvLikesCount.context,
                            R.color.text_primary
                        )
                    ), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                tvLikesCount.text = ssb

            }
        } else if (holder is ItemViewHolder) {
            holder.bind(data[position] as VotingItemUIModel, callback)
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class HeaderViewHolder(val itemHeaderBinding: ItemVotingHeaderBinding) :
        ViewHolder(itemHeaderBinding)

    class ItemViewHolder(private val itemListBinding: ItemVotingUserBinding) :
        ViewHolder(itemListBinding) {

        fun bind(item: VotingItemUIModel, callback: VotingCallback?) {
            itemListBinding.apply {
                model = item
                checkLikeState(item)
                btnLike.setOnClickListener {
                    callback?.vote(item)
                }
                imagesPager.adapter = VotingImageAdapter(item.images)
                TabLayoutMediator(tabs, imagesPager) { _, _ -> }.attach()
                imagesPager.currentItem = item.selectedImagePosition
                btnImagePrev.setOnClickListener {
                    if (imagesPager.currentItem > 0) {
                        imagesPager.currentItem--
                    }
                }
                btnImageNext.setOnClickListener {
                    if (imagesPager.currentItem < item.images.size - 1) {
                        imagesPager.currentItem++
                    }
                }

                imagesPager.registerOnPageChangeCallback(object :
                    ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        item.selectedImagePosition = position
                    }
                })

            }
        }

        private fun checkLikeState(item: VotingItemUIModel) {
            itemListBinding.ivLike.setBackgroundResource(
                if (item.isLiked)
                    R.drawable.ic_like_active
                else R.drawable.ic_like_inactive
            )
        }
    }
}