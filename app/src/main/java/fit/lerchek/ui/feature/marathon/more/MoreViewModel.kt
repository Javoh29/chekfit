package fit.lerchek.ui.feature.marathon.more

import androidx.databinding.ObservableBoolean
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class MoreViewModel @Inject constructor(
    private val userRepository: UserRepository,
) : BaseViewModel() {

    var profileIsMale = ObservableBoolean()

    init {
        checkGender()
    }

    private fun checkGender() {
        processTask(userRepository.isCurrentProfileMale().subscribeWith(
            simpleSingleObserver<Boolean> {
                profileIsMale.set(it)
            }
        ))
    }

}