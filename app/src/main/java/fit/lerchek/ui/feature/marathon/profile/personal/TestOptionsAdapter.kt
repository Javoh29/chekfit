package fit.lerchek.ui.feature.marathon.profile.personal

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.TestFooterItem
import fit.lerchek.data.domain.uimodel.marathon.TestItem
import fit.lerchek.data.domain.uimodel.marathon.TestItemOptionUIModel
import fit.lerchek.data.domain.uimodel.marathon.TestItemUIModel
import fit.lerchek.databinding.ItemTestFooterBinding
import fit.lerchek.databinding.ItemTestHeaderBinding
import fit.lerchek.databinding.ItemTestItemBinding
import fit.lerchek.databinding.ItemTestOptionItemBinding

class TestOptionsAdapter(
    private var data: List<TestItemOptionUIModel>
) : RecyclerView.Adapter<TestOptionsAdapter.ViewHolder>() {


    class ViewHolder(val dataBinding: ItemTestOptionItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            ItemTestOptionItemBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val option = data[position]
        holder.dataBinding.apply {
            model = option
            ivCheck.apply {
                if (option.selected.get()) {
                    setBackgroundResource(R.drawable.calendar_completed_day_bg)
                    setImageResource(R.drawable.ic_check_mark)
                    imageTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(context, R.color.black)
                    )
                } else {
                    setBackgroundResource(R.drawable.calendar_enabled_day_bg)
                    setImageDrawable(null)
                }
            }

            btnOption.setOnClickListener { _ ->
                if (option.selected.get().not()) {
                    val selectedIndex = data.indexOfFirst { it.selected.get() }
                    if (selectedIndex != -1)
                        data[selectedIndex].selected.set(false)
                    option.selected.set(true)
                    if (selectedIndex != -1)
                        notifyItemChanged(selectedIndex)
                    notifyItemChanged(position)
                }
            }
            executePendingBindings()
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}