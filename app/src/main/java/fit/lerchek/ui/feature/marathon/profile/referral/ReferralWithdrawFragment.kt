package fit.lerchek.ui.feature.marathon.profile.referral

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.marathon.TodayRefItem
import fit.lerchek.databinding.FragmentReferralWithdrawBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.TextUtils.copyTextToBuffer
import fit.lerchek.ui.util.viewBindings
import ru.tinkoff.decoro.slots.PredefinedSlots

import ru.tinkoff.decoro.MaskImpl

import ru.tinkoff.decoro.watchers.MaskFormatWatcher

import ru.tinkoff.decoro.watchers.FormatWatcher




@AndroidEntryPoint
class ReferralWithdrawFragment : BaseToolbarDependentFragment(R.layout.fragment_referral_withdraw),
    TodayRefItem.Callback {

    private val binding by viewBindings(FragmentReferralWithdrawBinding::bind)
    private val viewModel by viewModels<ReferralWithdrawViewModel>()
    private val args: ReferralWithdrawFragmentArgs by navArgs()


    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Menu
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        binding.price = args.balance.toString()
        binding.callback = this
        binding.btnBack.setOnClickListener {
            navigateUp()
        }
        binding.max.setOnClickListener {
            binding.sum.setText(args.balance.toString())
            try {
                binding.sum.setSelection(binding.sum.length())
            } catch (t:Throwable){}
        }
        val formatWatcher: FormatWatcher = MaskFormatWatcher(
            MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER)
        )
        formatWatcher.installOn(binding.phone)
        formatWatcher.refreshMask("+7")
    }

    override fun onLinkCopied(link: String) {
        mContext.copyTextToBuffer(Constants.LABEL_REF_CLIP_DATA, link)
        mContext.showToast(mContext.getString(R.string.today_ref_copied))
    }

    override fun onWithdraw() {
        viewModel.processWithdraw(args.balance)
    }
}