package fit.lerchek.ui.feature.marathon.progress

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentMeasurementsBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class MeasurementsFragment : BaseToolbarDependentFragment(R.layout.fragment_measurements) {
    private val binding by viewBindings(FragmentMeasurementsBinding::bind)
    private val viewModel by viewModels<MeasurementsViewModel>()
    private val args: MeasurementsFragmentArgs by navArgs()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override var isBottomNavigationVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.nsvMeasurements
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.context = requireContext()

        viewModel.loadMeasurements(args.type)
        binding.apply {
            btnBack.root.setOnClickListener {
                navigateUp()
            }

            itemWeek1.setOnClickListener {
                viewModel?.data?.get()?.let {
                    navigateTo(
                        MeasurementsFragmentDirections.actionNavigationMeasurementsToNavigationAddEditMeasurements(
                            it.type.name, 1, it.progress.measurement(it.type, 1) ?: -1F
                        )
                    )
                }
            }

            itemWeek2.setOnClickListener {
                viewModel?.data?.get()?.let {
                    navigateTo(
                        MeasurementsFragmentDirections.actionNavigationMeasurementsToNavigationAddEditMeasurements(
                            it.type.name, 2, it.progress.measurement(it.type, 2) ?: -1F
                        )
                    )
                }
            }

            itemWeek3.setOnClickListener {
                viewModel?.data?.get()?.let {
                    navigateTo(
                        MeasurementsFragmentDirections.actionNavigationMeasurementsToNavigationAddEditMeasurements(
                            it.type.name, 3, it.progress.measurement(it.type, 3) ?: -1F
                        )
                    )
                }
            }

            itemWeek4.setOnClickListener {
                viewModel?.data?.get()?.let {
                    navigateTo(
                        MeasurementsFragmentDirections.actionNavigationMeasurementsToNavigationAddEditMeasurements(
                            it.type.name, 4, it.progress.measurement(it.type, 4) ?: -1F
                        )
                    )
                }
            }
        }
    }
}