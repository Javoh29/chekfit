package fit.lerchek.ui.feature.login.forgotpwd

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.SupportUtils.Companion.isValidPassword
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class SetNewPwdViewModel @Inject constructor(private val userRepository: UserRepository) : BaseViewModel() {

    private val mSuccess = MutableLiveData<Event<Boolean>>()
    val success: LiveData<Event<Boolean>>
        get() = mSuccess

    var pwdNew: ObservableField<String> = ObservableField("")
//    var pwdConfirm: ObservableField<String> = ObservableField("")

    var email : String? = null
    var code : String? = null


    fun sendNewPwd(){
        processTask(userRepository.sendNewPwd(pwdNew.get() , pwdNew.get(), email , code).subscribeWith(object : DisposableSingleObserver<Boolean>() {

            override fun onSuccess(success: Boolean) {
                this@SetNewPwdViewModel.mSuccess.postValue(Event(success))
            }

            override fun onError(e: Throwable) {
//                if (e is LoginError) {
//                    emailError.set(e.emailError)
//                }
            }
        }))
    }

    fun canSendNewPwd() : Boolean{
        return isValidPassword(pwdNew.get()) && email != null && code != null
    }

}