package fit.lerchek.ui.feature.marathon.more.cardiotest

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.R
import fit.lerchek.ui.base.BaseViewModel
import javax.inject.Inject
import kotlin.math.roundToInt


@HiltViewModel
class CardioTestViewModel @Inject constructor() : BaseViewModel() {

    var age: ObservableField<String> = ObservableField("")
    var heartRateLimitLower: ObservableField<String> = ObservableField("")
    var heartRateLimitLowerSpeedUnit: ObservableInt =
        ObservableInt(R.string.cardio_test_heart_rate_speed_unit_3)
    var heartRateLimitUpper: ObservableField<String> = ObservableField("")
    var heartRateLimitUpperSpeedUnit: ObservableInt =
        ObservableInt(R.string.cardio_test_heart_rate_speed_unit_3)

    var isCalculate: ObservableBoolean = ObservableBoolean()

    fun calculate() {
        try {
            val ageValue = age.get()?.toInt() ?: throw NumberFormatException()
            if (ageValue !in 1..220) throw NumberFormatException()

            val lowerHeartRateLimit = ((220 - ageValue) * 0.65).roundToInt().toString()
            val upperHeartRateLimit = ((220 - ageValue) * 0.85).roundToInt().toString()
            update(lowerHeartRateLimit, upperHeartRateLimit)
        } catch (e: NumberFormatException) {
            update("", "")
        }
    }

    private fun update(lower: String, upper: String) {
        heartRateLimitLower.set(lower)
        heartRateLimitLowerSpeedUnit.set(getSpeedUnit(lower))
        heartRateLimitUpper.set(upper)
        heartRateLimitUpperSpeedUnit.set(getSpeedUnit(upper))
        isCalculate.set(lower != "" && upper != "")
    }

    private fun getSpeedUnit(heartRate: String): Int {
        return when (heartRate.lastOrNull()) {
            '1' -> R.string.cardio_test_heart_rate_speed_unit_1
            '2', '3', '4' -> R.string.cardio_test_heart_rate_speed_unit_2
            else -> R.string.cardio_test_heart_rate_speed_unit_3
        }
    }

}