package fit.lerchek.ui.feature.marathon.food

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentRecipeDetailsBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class RecipeDetailsFragment : BaseToolbarDependentFragment(R.layout.fragment_recipe_details) {

    private val binding by viewBindings(FragmentRecipeDetailsBinding::bind)
    private val viewModel by viewModels<RecipeDetailsViewModel>()
    private val args: RecipeDetailsFragmentArgs by navArgs()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadRecipe(args.recipeId, args.baseRecipeId, args.replacementAllowed, args.isExtra)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }
        binding.viewRecipeDetails.root.findViewById<View>(R.id.btnReplace).setOnClickListener {
            viewModel.recipe.value?.let { recipe->
                navigateTo(
                    RecipeDetailsFragmentDirections
                        .actionNavigationRecipeDetailsToNavigationRecipeReplacement(
                            args.isFromToday,
                            args.recipeId,
                            args.baseRecipeId,
                            recipe.planId ?:-1,
                            args.dayNumber
                    )
                )
            }
        }
        viewModel.recipe.observe(viewLifecycleOwner, { recipe ->
            binding.viewRecipeDetails.root.findViewById<AppCompatImageView>(R.id.imgRecipe).apply {
                Glide.with(mContext).load(recipe.imageUrl).into(this)
            }
        })
    }
}