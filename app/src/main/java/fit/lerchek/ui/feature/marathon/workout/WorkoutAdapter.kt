package fit.lerchek.ui.feature.marathon.workout

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import fit.lerchek.common.rx.CustomSingleObserver
import fit.lerchek.data.api.model.VimeoModel
import fit.lerchek.data.domain.uimodel.marathon.WorkoutFilterUIModel
import fit.lerchek.data.domain.uimodel.marathon.WorkoutUIModel
import fit.lerchek.databinding.*
import fit.lerchek.ui.util.setHtmlText
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class WorkoutAdapter(
    private var data: List<WorkoutItem>,
    private var callback: WorkoutCallback?
) : RecyclerView.Adapter<WorkoutAdapter.WorkoutViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE_FILTER = 0
        const val ITEM_VIEW_TYPE_WORKOUT = 1
        const val ITEM_VIEW_TYPE_EMPTY = 3
    }

    interface WorkoutItem

    sealed class WorkoutViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class EmptyViewHolder(itemWorkoutEmptyBinding: ItemWorkoutEmptyBinding) :
        WorkoutViewHolder(itemWorkoutEmptyBinding)

    class FilterViewHolder(val itemWorkoutFilterBinding: ItemWorkoutFilterBinding) :
        WorkoutViewHolder(itemWorkoutFilterBinding)

    class WorkoutItemViewHolder(val itemWorkoutBinding: ItemWorkoutBinding) :
        WorkoutViewHolder(itemWorkoutBinding) {

        private var currentVideoId: String? = null
        private var youTubePlayer: YouTubePlayer? = null

        init {
            itemWorkoutBinding.youtubePlayerView.apply {
                addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                    override fun onReady(youTubePlayer: YouTubePlayer) {
                        this@WorkoutItemViewHolder.youTubePlayer = youTubePlayer
                        cueVideo(currentVideoId)
                    }
                })
                getPlayerUiController().apply {
                    //не работает сокрытие. но кнопки не кликабельны
                    //https://github.com/PierfrancescoSoffritti/android-youtube-player/issues/513
                    showMenuButton(false)
                    showVideoTitle(false)
                }
            }
        }

        fun cueVideo(videoId: String?) {
            currentVideoId = videoId
            currentVideoId?.let {
                youTubePlayer?.cueVideo(it, 0f)
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkoutViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_FILTER -> FilterViewHolder(
                ItemWorkoutFilterBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_WORKOUT -> {
                ItemWorkoutBinding.inflate(inflater, parent, false).let {
                    callback?.getMyLifecycle()?.addObserver(it.youtubePlayerView)
                    WorkoutItemViewHolder(it)
                }
            }
            ITEM_VIEW_TYPE_EMPTY -> EmptyViewHolder(
                ItemWorkoutEmptyBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is WorkoutFilterUIModel -> ITEM_VIEW_TYPE_FILTER
            is WorkoutUIModel -> ITEM_VIEW_TYPE_WORKOUT
            else -> ITEM_VIEW_TYPE_EMPTY
        }
    }

    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: WorkoutViewHolder, position: Int) {
        when (holder) {
            is FilterViewHolder -> {
                holder.itemWorkoutFilterBinding.apply {
                    val filter = data[position] as WorkoutFilterUIModel
                    this.callback = this@WorkoutAdapter.callback
                    this.model = filter
                }
            }
            is WorkoutItemViewHolder -> {
                (data[position] as WorkoutUIModel).let { workout ->
                    holder.apply {
                        itemWorkoutBinding.model = workout
                        itemWorkoutBinding.tvDescription.let { description ->
                            if (workout.descriptionData != null) {
                                description.setText(
                                    workout.descriptionData,
                                    TextView.BufferType.SPANNABLE
                                )
                            } else {
                                setHtmlText(description, workout.description)
                            }
                        }
                        if (workout.videoType)
                            cueVideo(workout.videoUrl)
                        else {
                            itemWorkoutBinding.youtubePlayerView.visibility = View.GONE
                            itemWorkoutBinding.jzVideo.visibility = View.VISIBLE
                            var h: String? = null
                            if (workout.videoUrl.split("?").size > 1) h = "?${workout.videoUrl.split("?").last()}"
                            itemWorkoutBinding.jzVideo.mRetryBtn.setOnClickListener {
                                this@WorkoutAdapter.callback?.getVimeoData(workout.videoUrl.split("?").first(), h) {
                                    itemWorkoutBinding.jzVideo.setUp(it.videoUrl, "")
                                    itemWorkoutBinding.jzVideo.posterImageView?.let { img ->
                                        Glide.with(img.context).load(it.posterImage).into(img)
                                    }
                                }
                            }
                            this@WorkoutAdapter.callback?.getVimeoData(workout.videoUrl.split("?").first(), h) {
                                itemWorkoutBinding.jzVideo.setUp(it.videoUrl, "")
                                itemWorkoutBinding.jzVideo.posterImageView?.let { img ->
                                    Glide.with(img.context).load(it.posterImage).into(img)
                                }
                            }
                        }
                    }
                }
            }
            else -> {
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}

@BindingAdapter("workout_items", "workout_adapter_callback")
fun setupWorkoutItemsAdapter(
    list: RecyclerView,
    items: List<WorkoutAdapter.WorkoutItem>?,
    callback: WorkoutCallback?
) {
    items?.let {
        list.adapter = WorkoutAdapter(it, callback)
    }
}