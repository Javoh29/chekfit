package fit.lerchek.ui.feature.login.auth

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.exceptions.LoginError
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.SupportUtils
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var login: ObservableField<String> = ObservableField()
    var password: ObservableField<String> = ObservableField()
    var loginError: ObservableField<String> = ObservableField()
    var passwordError: ObservableField<String> = ObservableField()

    private val mSuccess = MutableLiveData<Event<Boolean>>()
    val success: LiveData<Event<Boolean>>
        get() = mSuccess

    fun doLogin() {

        processTask(
            userRepository.login(
                email = login.get() ?: "",
                password = password.get() ?: ""
            ).subscribeWith(object : DisposableSingleObserver<Boolean>() {
                override fun onStart() {
                    super.onStart()
                    loginError.set(null)
                    passwordError.set(null)
                }

                override fun onSuccess(success: Boolean) {
                    mSuccess.postValue(Event(success))
                }

                override fun onError(e: Throwable) {
                    if (e is LoginError) {
                        loginError.set(e.emailError)
                        passwordError.set(e.passwordError)
                    }
                    mSuccess.postValue(Event(false))
                }
            })
        )
    }

    fun checkInput() : Boolean {
        return SupportUtils.isValidEmail(login.get()) && !password.get().isNullOrEmpty()

    }
}