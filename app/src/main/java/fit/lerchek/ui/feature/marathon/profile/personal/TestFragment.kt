package fit.lerchek.ui.feature.marathon.profile.personal

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentProfileTestBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class TestFragment : BaseToolbarDependentFragment(R.layout.fragment_profile_test) {
    private val binding by viewBindings(FragmentProfileTestBinding::bind)
    private val viewModel by viewModels<TestViewModel>()
    private var adapter: TestAdapter? = null

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Close
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadTestData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        viewModel.content.observe(viewLifecycleOwner, {
            if (adapter == null) {
                adapter = TestAdapter(
                    data = it,
                    onBack = ::navigateUp,
                    onConfirm = viewModel::confirmResult
                )
                binding.rvTest.adapter = adapter
            } else {
                adapter?.update(it)
                binding.rvTest.scrollToPosition(it.size-1)
            }
        })
    }
}