package fit.lerchek.ui.feature.marathon.profile.referral

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.profile.TransactionUIModel
import fit.lerchek.databinding.ItemRefTransactionBinding
import fit.lerchek.ui.widget.DividerItemDecorator

class RefTransactionsAdapter(
    private var data: List<TransactionUIModel>
) : RecyclerView.Adapter<RefTransactionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(
            ItemRefTransactionBinding.inflate(inflater, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        data[position].let { item ->
            holder.dataBinding.apply {
                model = item
                val viewsAlpha = if (item.confirmed) 1f else 0.5f
                arrayOf(tvAmount, tvDate, tvDescription).forEach {
                    it.alpha = viewsAlpha
                }
                executePendingBindings()
            }
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder(val dataBinding: ItemRefTransactionBinding) :
        RecyclerView.ViewHolder(dataBinding.root)
}

@BindingAdapter("ref_transactions")
fun setupRefAdapter(
    list: RecyclerView,
    optionsData: List<TransactionUIModel>?
) {
    optionsData?.let { items ->
        ContextCompat.getDrawable(list.context, R.drawable.card_item_list_divider_without_padding)
            ?.let {
                DividerItemDecorator(it).apply {
                    list.addItemDecoration(this)
                }
            }
        list.adapter = RefTransactionsAdapter(items)
    }
}