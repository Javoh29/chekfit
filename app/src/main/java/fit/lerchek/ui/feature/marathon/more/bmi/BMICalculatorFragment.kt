package fit.lerchek.ui.feature.marathon.more.bmi

import android.os.Bundle
import android.view.View
import androidx.databinding.Observable
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentBmiCalculatorBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class BMICalculatorFragment :
    BaseToolbarDependentFragment(R.layout.fragment_bmi_calculator) {

    private val binding by viewBindings(FragmentBmiCalculatorBinding::bind)
    private val viewModel by viewModels<BMICalculatorViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootScrollingView = binding.nsvBMICalculator
        binding.viewModel = viewModel
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }

        viewModel.yourBMI.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if(getView()!=null) {
                    binding.nsvBMICalculator.postDelayed({
                        val child = binding.btnCalculate
                        binding.nsvBMICalculator.smoothScrollTo(child.x.toInt(), child.y.toInt())
                    }, 100)
                }
            }
        })
    }
}