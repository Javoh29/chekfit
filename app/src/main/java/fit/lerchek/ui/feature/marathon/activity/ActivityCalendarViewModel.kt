package fit.lerchek.ui.feature.marathon.activity

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.DateFormatUtils.formatDateDayFullMonth
import fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarItem
import fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarUIModel
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import javax.inject.Inject

@HiltViewModel
class ActivityCalendarViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val isMale = ObservableBoolean()
    val isToday = ObservableBoolean()
    val currentDay = ObservableInt()
    val currentMarathonDay = ObservableInt()
    val profileName = ObservableField<String>()
    val currentDate = ObservableField<String>()
    val currentTasksDate = ObservableField<String>()
    val currentTasks = ObservableArrayList<TaskUIModel>()
    val calendarItems = ObservableArrayList<ActivityCalendarItem>()
    private val calendarTasks = mutableMapOf<String, List<TaskUIModel>>()

    fun loadActivityCalendar(marathonId: Int) {
        processTask(
            userRepository.loadMarathonActivityCalendar(marathonId)
                .subscribeWith(simpleSingleObserver<ActivityCalendarUIModel> {
                    isMale.set(it.isMale)
                    currentDay.set(it.currentDay)
                    profileName.set(it.profileName)
                    currentDate.set(it.currentDate)
                    currentMarathonDay.set(it.currentMarathonDay)
                    calendarItems.clearAndAddAll(it.calendar)
                    calendarTasks.putAll(it.tasks)
                    currentTasks.clearAndAddAll(
                        getTasks(it.currentDay)
                    )
                    isToday.set(isTodaySelected())
                    isProgress.set(false)
                })
        )
    }

    fun updateTaskCompletion(task: TaskUIModel, currentDay: Int) {
        val selectedDay = getSelectedDay()?.number ?: currentDay
        processTask(userRepository.toggleTaskCompletion(task, selectedDay).subscribe())
        getTasks(selectedDay).all { it.checked }.let { allTasksCompleted ->
            calendarItems.firstOrNull {
                it is ActivityCalendarItem.Day && it.number == selectedDay
            }?.let { selectedDay ->
                (selectedDay as ActivityCalendarItem.Day).let { day ->
                    if (day.completed != allTasksCompleted) {
                        day.completed = allTasksCompleted
                        val newItems = calendarItems.map { it }
                        calendarItems.clearAndAddAll(newItems)
                    }
                }
            }
        }
    }

    fun updateSelectedDay(day: ActivityCalendarItem.Day) {
        currentTasksDate.set(formatDateDayFullMonth(day.createDate()))
        currentTasks.clearAndAddAll(getTasks(day.number))
        isToday.set(isTodaySelected())
    }

    private fun getTasks(day: Int): List<TaskUIModel> {
        return calendarTasks[day.toString()] ?: emptyList()
    }

    private fun getSelectedDay(): ActivityCalendarItem.Day? {
        return calendarItems.firstOrNull {
            it is ActivityCalendarItem.Day && it.selected
        } as ActivityCalendarItem.Day?
    }

    private fun isTodaySelected(): Boolean {
        return getSelectedDay()?.number == currentDay.get()
    }
}