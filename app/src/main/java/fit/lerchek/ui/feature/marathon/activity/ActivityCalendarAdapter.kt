package fit.lerchek.ui.feature.marathon.activity

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarItem
import fit.lerchek.databinding.ItemActivityCalendarDayBinding
import fit.lerchek.databinding.ItemActivityCalendarMonthBinding
import fit.lerchek.databinding.ItemActivityCalendarWeekdayBinding

class ActivityCalendarAdapter(
    private val items: List<ActivityCalendarItem>,
    private val callback: ActivityCalendarCallback
) : RecyclerView.Adapter<ActivityCalendarAdapter.CalendarItemViewHolder>() {

    var currentlySelectedDay: ActivityCalendarItem.Day? = null

    sealed class CalendarItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        class DayViewHolder(val binding: ItemActivityCalendarDayBinding) :
            CalendarItemViewHolder(binding.root) {
            fun bind(day: ActivityCalendarItem.Day) {
                binding.apply {
                    label.apply {
                        isInvisible = (day.id == -1).also { stub ->
                            if (stub.not()) text = day.id.toString()
                        }
                        alpha = if(day.enabled) 1.0f else 0.25f
                    }
                    image.apply {
                        isInvisible = (day.id == -1).also { stub ->
                            if (stub.not()) {
                                setBackgroundResource(
                                    when {
                                        day.selected -> R.drawable.calendar_selected_day_bg
                                        day.completed -> R.drawable.calendar_completed_day_bg
                                        day.enabled -> R.drawable.calendar_enabled_day_bg
                                        else -> R.drawable.calendar_disabled_day_bg
                                    }
                                )
                                if (day.completed) {
                                    setImageResource(R.drawable.ic_check_mark)
                                    imageTintList = ColorStateList.valueOf(
                                        ContextCompat.getColor(
                                            context,
                                            if (day.selected) {
                                                R.color.white
                                            } else {
                                                R.color.black
                                            }
                                        )
                                    )
                                } else setImageDrawable(null)
                            }
                        }
                    }
                    todayIndicator.isVisible = day.today
                }
            }

            fun setOnClickListener(listener: View.OnClickListener) {
                binding.root.setOnClickListener(listener)
            }
        }

        class MonthViewHolder(val binding: ItemActivityCalendarMonthBinding) :
            CalendarItemViewHolder(binding.root) {
            fun bind(month: ActivityCalendarItem.Month) {
                binding.root.text = month.name
            }
        }

        class WeekdayViewHolder(val binding: ItemActivityCalendarWeekdayBinding) :
            CalendarItemViewHolder(binding.root) {
            fun bind(weekday: ActivityCalendarItem.Weekday) {
                binding.root.text = weekday.name
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarItemViewHolder {
        return when (ActivityCalendarItem.Type.values()[viewType]) {
            ActivityCalendarItem.Type.WEEKDAY -> {
                CalendarItemViewHolder.WeekdayViewHolder(
                    ItemActivityCalendarWeekdayBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent, false
                    )
                )
            }
            ActivityCalendarItem.Type.MONTH -> {
                CalendarItemViewHolder.MonthViewHolder(
                    ItemActivityCalendarMonthBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent, false
                    )
                )
            }
            ActivityCalendarItem.Type.DAY -> {
                CalendarItemViewHolder.DayViewHolder(
                    ItemActivityCalendarDayBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent, false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: CalendarItemViewHolder, position: Int) {
        when (holder) {
            is CalendarItemViewHolder.DayViewHolder -> {
                (items[position] as ActivityCalendarItem.Day).also { day ->
                    holder.bind(day)
                    if (day.enabled) {
                        holder.setOnClickListener(View.OnClickListener {
                            updateSelectedDay(day)
                            callback.onDaySelected(day)
                        })
                    }
                }
            }
            is CalendarItemViewHolder.MonthViewHolder -> {
                holder.bind(items[position] as ActivityCalendarItem.Month)
            }
            is CalendarItemViewHolder.WeekdayViewHolder -> {
                holder.bind(items[position] as ActivityCalendarItem.Weekday)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun updateSelectedDay(day: ActivityCalendarItem.Day) {
        items.forEachIndexed { index, activityCalendarItem ->
            if (activityCalendarItem is ActivityCalendarItem.Day) {
                if (activityCalendarItem.selected && activityCalendarItem != day) {
                    activityCalendarItem.selected = false
                    notifyItemChanged(index)
                }
                if (activityCalendarItem == day) {
                    activityCalendarItem.selected = true
                    notifyItemChanged(index)
                }
            }
        }
    }
}

@BindingAdapter("calendar_items", "calendar_callback")
fun setCalendarAdapter(
    view: RecyclerView,
    items: List<ActivityCalendarItem>?,
    callback: ActivityCalendarCallback?
) {
    if (items != null && callback != null) {
        view.apply {
            adapter = ActivityCalendarAdapter(items, callback).apply {
                items.filterIsInstance<ActivityCalendarItem.Day>()
                    .firstOrNull { it.selected }?.let { currentlySelectedDay = it }
            }
            layoutManager = GridLayoutManager(context, 7).apply {
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    @Override
                    override fun getSpanSize(position: Int): Int {
                        return if (items[position].type == ActivityCalendarItem.Type.MONTH) 7 else 1
                    }
                }
            }
        }
    }
}