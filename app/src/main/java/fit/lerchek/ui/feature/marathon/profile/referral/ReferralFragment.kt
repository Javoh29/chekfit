package fit.lerchek.ui.feature.marathon.profile.referral

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.marathon.TodayRefItem
import fit.lerchek.databinding.FragmentReferralBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.TextUtils.copyTextToBuffer
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class ReferralFragment : BaseToolbarDependentFragment(R.layout.fragment_referral),
    TodayRefItem.Callback {

    private val binding by viewBindings(FragmentReferralBinding::bind)
    private val viewModel by viewModels<ReferralViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadReferralDetails()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel
        binding.callback = this
        binding.root.findViewById<View>(R.id.btnRefShare).visibility = View.GONE
        binding.root.findViewById<AppCompatTextView>(R.id.tvTerms).movementMethod =
            LinkMovementMethod.getInstance()
    }

    override fun onLinkCopied(link: String) {
        mContext.copyTextToBuffer(Constants.LABEL_REF_CLIP_DATA, link)
        mContext.showToast(mContext.getString(R.string.today_ref_copied))
    }

    override fun onWithdraw() {
        navigateTo(
            ReferralFragmentDirections.actionNavigationReferralToNavigationRefWithdraw(
                viewModel.referralDetails.get()?.balance?.toIntOrNull() ?: 0
            )
        )
    }
}