package fit.lerchek.ui.feature.marathon

import android.os.Handler
import android.os.Looper
import androidx.databinding.ObservableArrayList
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.Constants
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import javax.inject.Inject

@HiltViewModel
class FaqViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    val faqItems: ObservableArrayList<FaqAdapter.FaqListItem> = ObservableArrayList()

    init {
        isProgress.set(true)
    }

    fun loadQuestions(alias: String) {
        processTask(
            userRepository.loadFaq(alias)
                .subscribeWith(simpleSingleObserver<List<FaqAdapter.FaqListItem>> {
                    faqItems.clearAndAddAll(it)
                    Handler(Looper.getMainLooper()).postDelayed(
                        {
                            isProgress.set(false)
                        },
                        Constants.DEFAULT_DELAY
                    )
                })
        )
    }
}