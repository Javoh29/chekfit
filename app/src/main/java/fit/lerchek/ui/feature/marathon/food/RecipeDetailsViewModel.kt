package fit.lerchek.ui.feature.marathon.food

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class RecipeDetailsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    private val mRecipe: MutableLiveData<RecipeUIModel> = MutableLiveData()

    val recipe :LiveData<RecipeUIModel>
        get() = mRecipe

    fun loadRecipe(recipeId:Int, baseRecipeId:Int, replacementAllowed: Boolean, isExtra: Boolean) {
        processTask(
            userRepository.loadRecipeDetails(recipeId, baseRecipeId, replacementAllowed, isExtra)
                .subscribeWith(simpleSingleObserver<RecipeUIModel> { recipe ->
                   mRecipe.postValue(recipe)
                    Handler(Looper.getMainLooper()).postDelayed({
                        isProgress.set(false)
                    }, Constants.DEFAULT_DELAY)
                })
        )
    }

    fun favoriteChange() {
        val model = mRecipe.value!!
        model.isFavorite = !model.isFavorite
        mRecipe.postValue(model)
        processTask(
            userRepository.toggleFavorite(!model.isFavorite, recipe.value?.id!!)
                .subscribeWith(object : DisposableSingleObserver<Boolean>() {
                    override fun onSuccess(t: Boolean) {
                        FoodFragment.onUpdate.value = true
                    }

                    override fun onError(e: Throwable) {
                        Log.d("BUG", "Error: $e")
                        model.isFavorite = !model.isFavorite
                        mRecipe.postValue(model)
                    }

                })
        )
    }

}