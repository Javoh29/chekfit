package fit.lerchek.ui.feature.marathon.food

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.data.domain.uimodel.marathon.RecipeIngredientUIModel
import fit.lerchek.databinding.ItemRecipeIngredientBinding
import fit.lerchek.databinding.ItemRecipeIngredientHeaderBinding

class RecipeIngredientsAdapter(
    private var data: List<RecipeIngredientUIModel>
) :
    RecyclerView.Adapter<RecipeIngredientsAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_HEADER = 0
        private const val ITEM_VIEW_TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder(
                ItemRecipeIngredientHeaderBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_ITEM -> ItemViewHolder(
                ItemRecipeIngredientBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].isGroup) {
            ITEM_VIEW_TYPE_HEADER
        } else {
            ITEM_VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        data[position].name.let{
            if (holder is HeaderViewHolder) {
                holder.headerBinding.text = it
            } else if (holder is ItemViewHolder) {
                holder.itemBinding.text = it
            }
        }

        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class HeaderViewHolder(val headerBinding: ItemRecipeIngredientHeaderBinding) :
        ViewHolder(headerBinding)

    class ItemViewHolder(val itemBinding: ItemRecipeIngredientBinding) :
        ViewHolder(itemBinding)
}

@BindingAdapter("recipe_ingredients")
fun setRecipeIngredients(
    list: RecyclerView,
    optionsData: List<RecipeIngredientUIModel>?,
) {
    optionsData?.let {
        list.adapter = RecipeIngredientsAdapter(optionsData)
    }
}