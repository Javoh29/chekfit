package fit.lerchek.ui.feature.marathon.food

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class FoodViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var foodItems: ObservableArrayList<FoodAdapter.FoodItem> = ObservableArrayList()
    val errorItemPosition = MutableLiveData<Int>()

    private var foodDetailsUIModel: FoodDetailsUIModel? = null

    fun loadFoodDetails(
        filterUIModel: FoodFilterUIModel? = null,
        needSync: Boolean = false
    ) {
        processTask(
            userRepository.loadFoodDetails(filterUIModel, needSync, foodDetailsUIModel)
                .subscribeWith(simpleSingleObserver<FoodDetailsUIModel> {
                    this@FoodViewModel.foodDetailsUIModel = it
                    foodItems.clearAndAddAll(it.displayItems)
                    isProgress.set(false)
                })
        )
    }

    fun selectNutritionPlan(plan: PlanUIModel) {
        getCurrentFilter()?.let {
            it.selectNutritionPlan(plan.id)
            loadFoodDetails(it, true)
        }
    }

    fun selectWeek(week: WeekUIModel) {
        getCurrentFilter()?.let {
            it.selectWeek(week.id)
            loadFoodDetails(it)
        }
    }

    fun selectDay(day: DayUIModel) {
        getCurrentFilter()?.let {
            it.selectDay(day.dayNumber)
            loadFoodDetails(it)
        }
    }

    fun favoriteChange(isDelete: Boolean, recipeId: Int, position: Int) {
        processTask(
            userRepository.toggleFavorite(isDelete, recipeId)
                .subscribeWith(object : DisposableSingleObserver<Boolean>() {
                    override fun onSuccess(t: Boolean) {
                    }

                    override fun onError(e: Throwable) {
                        Log.d("BUG", "Error: $e")
                        errorItemPosition.value = position
                    }

                })
        )
    }

    private fun getCurrentFilter(): FoodFilterUIModel? {
        return foodDetailsUIModel?.displayItems?.firstOrNull {
            it is FoodFilterUIModel
        } as FoodFilterUIModel?
    }

    fun getCurrentWeekId() = getCurrentFilter()?.getSelectedWeek()?.id ?: -1
}