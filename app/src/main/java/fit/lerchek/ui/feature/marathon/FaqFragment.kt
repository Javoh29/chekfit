package fit.lerchek.ui.feature.marathon

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentFaqBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class FaqFragment : BaseToolbarDependentFragment(R.layout.fragment_faq) {

    private val binding by viewBindings(FragmentFaqBinding::bind)
    private val viewModel by viewModels<FaqViewModel>()
    private val args: FaqFragmentArgs by navArgs()

    companion object {
        const val TAG_ALIAS_FOOD = "food"
        const val TAG_ALIAS_WORKOUT = "workout"
        const val TAG_ALIAS_PROGRESS = "progress"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadQuestions(args.alias)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.faqBackCallback = object :FaqAdapter.OnBackListener{
            override fun onBack() {
                navigateUp()
            }
        }
    }

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )
}