package fit.lerchek.ui.feature.marathon.more.bmi

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.BMIItemUIModel
import fit.lerchek.data.domain.uimodel.marathon.BMIItemUIModel.Type
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.TextUtils.convertWithTwoDecimal
import javax.inject.Inject

@HiltViewModel
class BMICalculatorViewModel @Inject constructor() : BaseViewModel() {

    var height: ObservableField<String> = ObservableField("")
    var weight: ObservableField<String> = ObservableField("")
    var yourBMI: ObservableField<String> = ObservableField("")
    var valueBMI: ObservableField<BMIItemUIModel> = ObservableField(BMIItemUIModel(Type.get(0.0)))

    var isCalculate: ObservableBoolean = ObservableBoolean()

    fun calculate() {
        try {
            var height = this.height.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()
            val weight = this.weight.get()?.replace(',', '.')?.toDouble() ?: throw NumberFormatException()
            if (height <= 0 || weight <= 0) throw NumberFormatException()

            height /= 100
            val bmi = weight / (height * height)
            update(bmi)
        } catch (e: NumberFormatException) {
            update(0.0)
        }
    }

    private fun update(bmi: Double) {
        yourBMI.set(bmi.convertWithTwoDecimal())
        valueBMI.set(BMIItemUIModel(Type.get(bmi)))
        isCalculate.set(bmi > 0)
    }

}