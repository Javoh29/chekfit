package fit.lerchek.ui.feature.marathon.food

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fit.lerchek.data.domain.uimodel.marathon.RecipeReplacementFilterUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.databinding.ItemRecipeReplacementFilterBinding
import fit.lerchek.databinding.ViewRecipeDetailsBinding

class RecipeReplacementAdapter(
    private var data: List<ReplacementItem>,
    private var callback: RecipeReplacementCallback?
) : RecyclerView.Adapter<RecipeReplacementAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_FILTER = 0
        private const val ITEM_VIEW_TYPE_RECIPE = 1
    }

    interface RecipeReplacementCallback {
        fun onReplace(recipeUIModel: RecipeUIModel)
        fun onCancel()
    }

    interface ReplacementItem

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class FilterViewHolder(val itemFilterBinding: ItemRecipeReplacementFilterBinding) :
        ViewHolder(itemFilterBinding)

    class RecipeItemViewHolder(val itemRecipeBinding: ViewRecipeDetailsBinding) :
        ViewHolder(itemRecipeBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ITEM_VIEW_TYPE_FILTER -> FilterViewHolder(
                ItemRecipeReplacementFilterBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_RECIPE -> RecipeItemViewHolder(
                ViewRecipeDetailsBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is RecipeUIModel) ITEM_VIEW_TYPE_RECIPE else ITEM_VIEW_TYPE_FILTER
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is FilterViewHolder -> {
                holder.itemFilterBinding.apply {
                    val filter = data[position] as RecipeReplacementFilterUIModel
                    this.model = filter
                    imgReplacement.let {
                        Glide.with(it.context).load(filter.imageUrl).into(it)
                    }
                    imgRemove.setOnClickListener {
                        callback?.onCancel()
                    }
                    btnBack.root.setOnClickListener {
                        callback?.onCancel()
                    }
                }
            }
            is RecipeItemViewHolder -> {
                val recipe = data[position] as RecipeUIModel
                holder.itemRecipeBinding.apply {
                    model = recipe
                    isReplacement = true
                    Glide.with(imgRecipe.context).load(recipe.imageUrl).into(imgRecipe)
                    btnUseThis.setOnClickListener {
                        callback?.onReplace(recipe)
                    }
                }

            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount() = data.size

}

@BindingAdapter("recipe_replacements", "recipe_replacements_callback")
fun setupReplacementsAdapter(
    list: RecyclerView,
    items: List<RecipeReplacementAdapter.ReplacementItem>?,
    callback: RecipeReplacementAdapter.RecipeReplacementCallback?
) {
    items?.let {
        list.adapter = RecipeReplacementAdapter(it, callback)
    }
}