package fit.lerchek.ui.feature.marathon.today.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.api.message.marathon.UpdateTrackerGoalRequest
import fit.lerchek.data.api.message.marathon.UpdateTrackerRequest
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.databinding.*
import fit.lerchek.ui.feature.marathon.today.callbacks.TrackerSettingCallback

class SettingTrackerAdapter(
    private var data: List<TrackerSettingItem>,
    private var callback: TrackerSettingCallback
) : RecyclerView.Adapter<SettingTrackerAdapter.SettingTrackerViewHolder>() {

    companion object {
        const val ITEM_VIEW_TYPE_WATER_TRACKER_SET = 0
        const val ITEM_VIEW_TYPE_STEPS_TRACKER_SET = 1
        const val ITEM_VIEW_TYPE_SLEEP_TRACKER_SET = 2
        const val ITEM_VIEW_TYPE_STEPS_TRACKER_ADD = 3
        const val ITEM_VIEW_TYPE_SLEEP_TRACKER_ADD = 4
    }

    sealed class SettingTrackerViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class SettingWaterViewHolder(val itemSettingWaterSetBinding: ItemSettingWaterSetBinding) :
        SettingTrackerAdapter.SettingTrackerViewHolder(itemSettingWaterSetBinding)

    class SettingStepsAddViewHolder(val itemSettingStepsAddBinding: ItemSettingStepsAddBinding) :
        SettingTrackerAdapter.SettingTrackerViewHolder(itemSettingStepsAddBinding)

    class SettingStepsSetViewHolder(val itemSettingStepsSetBinding: ItemSettingStepsSetBinding) :
        SettingTrackerAdapter.SettingTrackerViewHolder(itemSettingStepsSetBinding)

    class SettingSleepAddViewHolder(val itemSettingSleepAddBinding: ItemSettingSleepAddBinding) :
        SettingTrackerAdapter.SettingTrackerViewHolder(itemSettingSleepAddBinding)

    class SettingSleepSetViewHolder(val itemSettingSleepSetBinding: ItemSettingSleepSetBinding) :
        SettingTrackerAdapter.SettingTrackerViewHolder(itemSettingSleepSetBinding)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingTrackerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_WATER_TRACKER_SET -> SettingWaterViewHolder(
                ItemSettingWaterSetBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_STEPS_TRACKER_ADD -> SettingStepsAddViewHolder(
                ItemSettingStepsAddBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_STEPS_TRACKER_SET -> SettingStepsSetViewHolder(
                ItemSettingStepsSetBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SLEEP_TRACKER_ADD -> SettingSleepAddViewHolder(
                ItemSettingSleepAddBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_SLEEP_TRACKER_SET -> SettingSleepSetViewHolder(
                ItemSettingSleepSetBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(
        holder: SettingTrackerViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        when (holder) {
            is SettingWaterViewHolder -> {
                val model = (data[position] as WaterTrackerSetItem)
                holder.itemSettingWaterSetBinding.let { binding ->
                    binding.model = model
                    binding.inputWaterVolume.setText(model.countMl)
                    binding.inputWaterRate.setText(model.countGoal)
                    binding.waterSpinner.setSelection(model.methodType)
                    binding.waterSpinner.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                pos: Int,
                                id: Long
                            ) {
                                model.methodType = pos
                                if (pos == 2) {
                                    binding.tvVolume.visibility = View.VISIBLE
                                    binding.cardVolume.visibility = View.VISIBLE
                                } else {
                                    binding.tvVolume.visibility = View.GONE
                                    binding.cardVolume.visibility = View.GONE
                                }
                                if (pos == 0) {
                                    binding.inputWaterRate.setHint(R.string.text_enter_count_glasses)
                                } else binding.inputWaterRate.setHint(R.string.text_enter_daily_goal)
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }

                        }
                    binding.btnSave.save.setOnClickListener {
                        if (model.methodType == 2 && (binding.inputWaterVolume.text == null || binding.inputWaterVolume.text!!.isEmpty())) return@setOnClickListener
                        if (binding.inputWaterRate.text == null || binding.inputWaterRate.text!!.isEmpty()) return@setOnClickListener
                        var demin = 0
                        demin = when (model.methodType) {
                            0 -> {
                                250
                            }
                            1 -> 1
                            else -> binding.inputWaterVolume.text!!.toString().toInt()
                        }
                        callback.changeGoalTracker(UpdateTrackerGoalRequest(binding.inputWaterRate.text.toString().toInt(), demin), "water_set")
                    }
                }
            }
            is SettingStepsAddViewHolder -> {
                val model = (data[position] as StepsTrackerAddItem)
                holder.itemSettingStepsAddBinding.let { binding ->
                    binding.btnSave.save.setOnClickListener {
                        if (binding.inputStepsCount.text != null && binding.inputStepsCount.text.toString()
                                .isNotEmpty()
                        )
                            callback.changeCountTracker(
                                UpdateTrackerRequest(
                                    model.date,
                                    binding.inputStepsCount.text.toString().toInt()
                                ), "steps_add"
                            )
                    }
                }
            }
            is SettingStepsSetViewHolder -> {
                holder.itemSettingStepsSetBinding.let { binding ->
                    binding.inputStepsCount.setText((data[position] as StepsTrackerSetItem).countSteps)
                    binding.btnSave.save.setOnClickListener {
                        if (binding.inputStepsCount.text == null || binding.inputStepsCount.text!!.isEmpty()) return@setOnClickListener
                        callback.changeGoalTracker(UpdateTrackerGoalRequest(binding.inputStepsCount.text.toString().toInt(), 1), "steps_set")
                    }
                }
            }
            is SettingSleepAddViewHolder -> {
                val model = (data[position] as SleepTrackerAddItem)
                holder.itemSettingSleepAddBinding.let { binding ->
                    binding.root.context.resources.getStringArray(R.array.DreamHours).forEachIndexed { index, s ->
                        if (s == model.sleepHours) {
                            binding.dreamHoursSpinner.setSelection(index)
                        }
                    }
                    binding.root.context.resources.getStringArray(R.array.DreamMinutes).forEachIndexed { index, s ->
                        if (s == model.sleepMinutes) {
                            binding.dreamMinutesSpinner.setSelection(index)
                        }
                    }
                    binding.btnSave.save.setOnClickListener {
                        if (binding.dreamHoursSpinner.selectedItem != "0" || binding.dreamMinutesSpinner.selectedItem != "0") {
                            val h = binding.dreamHoursSpinner.selectedItem.toString().toInt()
                            val m = binding.dreamMinutesSpinner.selectedItem.toString().toInt()
                            callback.changeCountTracker(
                                UpdateTrackerRequest(
                                    model.date,
                                    (h * 60 + m)
                                ), "sleep_add"
                            )
                        }
                    }
                }
            }
            is SettingSleepSetViewHolder -> {
                val model = data[position] as SleepTrackerSetItem
                holder.itemSettingSleepSetBinding.let { binding ->
                    binding.root.context.resources.getStringArray(R.array.DreamHours).forEachIndexed { index, s ->
                        if (s == model.sleepHours) {
                            binding.dreamHoursSpinner.setSelection(index)
                        }
                    }
                    binding.root.context.resources.getStringArray(R.array.DreamMinutes).forEachIndexed { index, s ->
                        if (s == model.sleepMinutes) {
                            binding.dreamMinutesSpinner.setSelection(index)
                        }
                    }
                    binding.btnSave.save.setOnClickListener {
                        if (binding.dreamHoursSpinner.selectedItem != "0" || binding.dreamMinutesSpinner.selectedItem != "0") {
                            val h = binding.dreamHoursSpinner.selectedItem.toString().toInt()
                            val m = binding.dreamMinutesSpinner.selectedItem.toString().toInt()
                            callback.changeGoalTracker(UpdateTrackerGoalRequest((h * 60 + m), 1), "sleep_set")
                        }
                    }
                }
            }
            else -> {}
        }
    }

    override fun getItemCount() = data.size

    override fun getItemViewType(position: Int): Int {
        return data[position].type.itemViewType
    }
}

@BindingAdapter("setting_items", "setting_callback")
fun setupSettingTrackerAdapter(
    list: RecyclerView,
    optionsData: List<TrackerSettingItem>?,
    callback: TrackerSettingCallback
) {
    optionsData?.let {
        list.adapter = SettingTrackerAdapter(optionsData, callback)
    }
}