package fit.lerchek.ui.feature.marathon.more

import androidx.databinding.ObservableArrayList
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.domain.uimodel.marathon.CdfcFilterUIModel
import fit.lerchek.data.domain.uimodel.marathon.CdfcUIModel
import fit.lerchek.data.domain.uimodel.marathon.MealUIModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.ui.util.clearAndAddAll
import javax.inject.Inject

@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var products: ObservableArrayList<ProductsAdapter.ProductItem> =
        ObservableArrayList()

    private var cache: CdfcUIModel? = null

    fun loadProducts(filterUIModel: CdfcFilterUIModel? = null) {
        processTask(
            userRepository.loadCdfc(filterUIModel, cache)
                .subscribeWith(simpleSingleObserver<CdfcUIModel> {
                    cache = it
                    products.clearAndAddAll(it.displayItems)
                    isProgress.set(false)
                })
        )
    }

    fun selectCategory(category: MealUIModel) {
        getCurrentFilter()?.let {
            it.selectType(category.id)
            loadProducts(it)
        }
    }

    private fun getCurrentFilter(): CdfcFilterUIModel? {
        return cache?.displayItems?.firstOrNull {
            it is CdfcFilterUIModel
        } as CdfcFilterUIModel?
    }

}