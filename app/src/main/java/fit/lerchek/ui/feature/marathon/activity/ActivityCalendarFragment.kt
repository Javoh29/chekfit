package fit.lerchek.ui.feature.marathon.activity

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.marathon.ActivityCalendarItem
import fit.lerchek.data.domain.uimodel.marathon.TaskUIModel
import fit.lerchek.databinding.FragmentActivityCalendarBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.feature.marathon.today.callbacks.TaskCallback
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class ActivityCalendarFragment : BaseToolbarDependentFragment(R.layout.fragment_activity_calendar),
    ActivityCalendarCallback, TaskCallback {

    private val binding by viewBindings(FragmentActivityCalendarBinding::bind)
    private val viewModel by viewModels<ActivityCalendarViewModel>()
    private val args by navArgs<ActivityCalendarFragmentArgs>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadActivityCalendar(args.marathonId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.calendarCallback = this
        binding.taskCallback = this
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }
    }

    override fun updateTaskCompletion(task: TaskUIModel, currentDay: Int) {
        viewModel.updateTaskCompletion(task, currentDay)
    }

    override fun onTaskInfoClick(task: TaskUIModel) {
        navigateTo(
            ActivityCalendarFragmentDirections.actionNavigationActivityCalendarToNavigationInfo(
                task.title, task.comment
            )
        )
    }

    override fun onDaySelected(day: ActivityCalendarItem.Day) {
        viewModel.updateSelectedDay(day)
    }
}