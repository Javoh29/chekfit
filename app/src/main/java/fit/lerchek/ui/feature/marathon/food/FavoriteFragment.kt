package fit.lerchek.ui.feature.marathon.food

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.common.util.Constants
import fit.lerchek.data.domain.uimodel.marathon.DayUIModel
import fit.lerchek.data.domain.uimodel.marathon.PlanUIModel
import fit.lerchek.data.domain.uimodel.marathon.RecipeUIModel
import fit.lerchek.data.domain.uimodel.marathon.WeekUIModel
import fit.lerchek.databinding.FavoriteFragmentBinding
import fit.lerchek.ui.base.BaseToolbarDependentFragment
import fit.lerchek.ui.base.ToolbarContentItem
import fit.lerchek.ui.util.ToastUtils
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class FavoriteFragment : BaseToolbarDependentFragment(R.layout.favorite_fragment), FoodCallback {
    private val binding by viewBindings(FavoriteFragmentBinding::bind)
    private val viewModel by viewModels<FavoriteViewModel>()

    override fun getToolbarContent() = setOf(
        ToolbarContentItem.Logo,
        ToolbarContentItem.Dropdown,
        ToolbarContentItem.Profile,
        ToolbarContentItem.Menu
    )

    override var isBottomNavigationVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onErrorListener = this
        viewModel.loadFoodDetails()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.callback = this
        viewModel.errorItemPosition.observe(viewLifecycleOwner, {
            if (it != null) {
                (binding.rvFavorite.adapter as FoodAdapter).update(it)
                ToastUtils.show(requireContext(), Constants.UNKNOWN_ERROR)
            }
        })
        viewModel.displayItems.observe(viewLifecycleOwner, {
            if (it != null) {
                Glide.with(requireContext()).load(it.image["2x"]).into(binding.imgNutritionPlan)
            }
        })
        binding.btnBack.root.setOnClickListener {
            navigateUp()
        }
        binding.btnSelectMeal.setOnClickListener {
            mContext.showSimpleOptionsDialog(viewModel.mealList.map { it.name }.toTypedArray()) {
                viewModel.displayItems.value = viewModel.mealList[it]
                viewModel.setMealFilter()
            }
        }
    }

    override fun onShoppingCartClick() {
    }

    override fun onFAQClick() {
    }

    override fun onFavoriteClick() {

    }

    override fun onSelectWeekClick(weeks: List<WeekUIModel>) {
    }

    override fun onSelectNutritionPlanClick(plans: List<PlanUIModel>) {
    }

    override fun onSelectDayClick(dayUIModel: DayUIModel) {
    }

    override fun onSelectRecipe(recipeUIModel: RecipeUIModel) {
        navigateTo(
            FavoriteFragmentDirections.actionNavigationFavoriteToNavigationRecipeDetails(
                false,
                recipeUIModel.id,
                recipeUIModel.baseId ?: -1,
                true,
                false,
                recipeUIModel.dayNumber ?: -1,
            )
        )
    }

    override fun onReplaceRecipe(recipeUIModel: RecipeUIModel) {
        navigateTo(
            FavoriteFragmentDirections.actionNavigationFavoriteToNavigationRecipeReplacement(
                false,
                recipeUIModel.id,
                recipeUIModel.baseId ?: -1,
                recipeUIModel.planId ?: -1,
                recipeUIModel.dayNumber ?: -1
            )
        )
    }

    override fun onFavToggleClick(isDelete: Boolean, recipeId: Int, position: Int) {
        viewModel.favoriteChange(isDelete, recipeId, position)
    }

}