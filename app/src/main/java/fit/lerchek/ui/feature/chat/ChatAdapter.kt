package fit.lerchek.ui.feature.chat

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fit.lerchek.R
import fit.lerchek.data.domain.uimodel.chat.ChatItem
import fit.lerchek.data.domain.uimodel.chat.ChatMessageUIModel
import fit.lerchek.databinding.ItemChatFooterBinding
import fit.lerchek.databinding.ItemChatMessageBinding
import fit.lerchek.ui.util.clearAndAddAll

class ChatAdapter(private var data: MutableList<ChatItem>) :
    RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_FOOTER = 0
        private const val ITEM_VIEW_TYPE_MESSAGE = 1
    }

    fun update(items: List<ChatItem>) {
        data.clearAndAddAll(items)
        notifyItemRangeChanged(0, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ITEM_VIEW_TYPE_FOOTER -> FooterViewHolder(
                ItemChatFooterBinding.inflate(inflater, parent, false)
            )
            ITEM_VIEW_TYPE_MESSAGE -> ItemViewHolder(
                ItemChatMessageBinding.inflate(inflater, parent, false)
            )
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is ChatMessageUIModel) {
            ITEM_VIEW_TYPE_MESSAGE
        } else {
            ITEM_VIEW_TYPE_FOOTER
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            val message = data[position] as ChatMessageUIModel
            holder.itemBinding.apply {
                model = message
                tvMessage.movementMethod = LinkMovementMethod()
                if (message.isOperator) {
                    tvName.setTextColor(ContextCompat.getColor(tvName.context, R.color.chat_purple))
                    if (message.isMale) {
                        imgAvatar.setBackgroundResource(R.drawable.ic_chat_male)
                        tvName.setText(R.string.chat_support_artemchek)
                    } else {
                        imgAvatar.setBackgroundResource(R.drawable.ic_chat_female)
                        tvName.setText(R.string.chat_support_lercheck)
                    }
                } else {
                    tvName.setTextColor(ContextCompat.getColor(tvName.context, R.color.chat_pink))
                    if (message.isMale) {
                        imgAvatar.setBackgroundResource(R.drawable.ic_profile_male)
                        tvName.setText(R.string.chat_support_artemchek)
                    } else {
                        imgAvatar.setBackgroundResource(R.drawable.ic_profile_female)
                        tvName.setText(R.string.chat_support_lercheck)
                    }
                }
            }
        }
        holder.dataBinding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    sealed class ViewHolder(val dataBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(dataBinding.root)

    class FooterViewHolder(footerBinding: ItemChatFooterBinding) : ViewHolder(footerBinding)

    class ItemViewHolder(val itemBinding: ItemChatMessageBinding) : ViewHolder(itemBinding)
}