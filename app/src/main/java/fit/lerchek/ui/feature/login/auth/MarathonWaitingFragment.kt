package fit.lerchek.ui.feature.login.auth

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.databinding.FragmentMarathonWaitingBinding
import fit.lerchek.ui.base.BaseFragment
import fit.lerchek.ui.util.BrowserUtils
import fit.lerchek.common.util.Constants.WEB_HOME
import fit.lerchek.ui.util.viewBindings

@AndroidEntryPoint
class MarathonWaitingFragment : BaseFragment(R.layout.fragment_marathon_waiting){

    private val binding by viewBindings(FragmentMarathonWaitingBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE

        binding.btnSignIn.setOnClickListener {
            navigateTo(R.id.action_fragment_marathon_waiting_to_fragment_sign_in)
        }
        binding.btnBuyMarathon.setOnClickListener {
            BrowserUtils.openBrowser(mContext, WEB_HOME)
        }
    }
}