package fit.lerchek.ui.feature.marathon.progress.crop

import android.graphics.Bitmap
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.base.BaseViewModel
import fit.lerchek.util.Event
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@HiltViewModel
class CropActivityViewModel @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModel() {

    var profileIsMale = ObservableBoolean()

    private val mSuccess = MutableLiveData<Event<Boolean>>()
    val success: LiveData<Event<Boolean>>
        get() = mSuccess

    init {
        checkGender()
    }

    private fun checkGender() {
        processTask(
            userRepository.isCurrentProfileMale().subscribeWith(
                object: DisposableSingleObserver<Boolean>() {
                    override fun onSuccess(isMale: Boolean) {
                        profileIsMale.set(isMale)
                        isProgress.set(false)

                    }

                    override fun onError(e: Throwable) {
                    }
                })
        )
    }

    fun uploadImages(type: String, week: Int, cropped: Bitmap?, originalBitmap: Bitmap?) {
        processTask(
            userRepository.uploadPhotosToServer(
                type = type,
                week = week,
                cropped = cropped,
                originalBitmap = originalBitmap
            ).subscribeWith(
                simpleSingleObserver {
                    mSuccess.postValue(Event(it))
                    isProgress.set(false)
                })
        )
    }
}