package fit.lerchek.data.fcm

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import fit.lerchek.R
import fit.lerchek.data.repository.user.UserRepository
import fit.lerchek.ui.feature.login.LoginActivity
import javax.inject.Inject

@AndroidEntryPoint
class FMService : FirebaseMessagingService() {

    @Inject
    lateinit var userRepository: UserRepository

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        userRepository.updateFCMToken(token)
    }

    companion object {
        private const val CHANNEL_NAME = "lerchek_channel name"
        private const val CHANNEL_ID = "lercheck_channel_id"

        const val TYPE_MARATHON = "marathon"
        const val TYPE_VOTE = "vote"
        const val TYPE_PHOTO = "photo"
        const val TYPE_QA = "qa"

        const val KEY_TYPE = "type"
        const val KEY_MARATHON_ID = "marathon_id"
        const val KEY_VOTE_ID = "vote_id"

        fun prepareFCMToken(complete: (token: String?) -> Unit) {
            FirebaseMessaging.getInstance().token
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("TAG", "Fetching FCM registration token failed", task.exception)
                        complete.invoke(null)
                        return@OnCompleteListener
                    }
                    val token: String? = task.result
                    complete.invoke(token)
                })
        }

        @SuppressLint("UnspecifiedImmutableFlag")
        fun sendNotification(context: Context, title: String?, message: String?, intent: Intent) {
            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.im_lerchek_logo)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(
                    PendingIntent.getActivity(
                        context,
                        0,
                        Intent(context, LoginActivity::class.java).apply {
                            intent.extras?.let {
                                putExtras(it)
                            }
                        },
                        PendingIntent.FLAG_CANCEL_CURRENT
                    )
                )
            (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(
                (System.currentTimeMillis()/1000).toInt(),
                builder.build()
            )
        }

        fun initNotificationChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
                ).apply {
                    setShowBadge(false)
                    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).let {
                        it.createNotificationChannel(this)
                    }
                }
            }
        }
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        sendNotification(
            context = this,
            title = message.notification?.title,
            message = message.notification?.body,
            intent = message.toIntent()
        )
    }
}