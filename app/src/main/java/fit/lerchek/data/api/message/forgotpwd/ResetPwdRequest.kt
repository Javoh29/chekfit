package fit.lerchek.data.api.message.forgotpwd

import com.google.gson.annotations.SerializedName

class ResetPwdRequest (var email : String?,var code : String?,var password : String?, @SerializedName("password_confirmation")var passwordConfirm : String? , var device_id:String? = null)