package fit.lerchek.data.api.model


class WorkoutDetails(
    val current_day:Int,
    val current_week:Int,
    val current_week_day_number:Int,
    val workout_plan_id:Int,
    val weeks: List<Week>,
    val today:String,
    val can_edit:Boolean,
    val can_add_phot:Boolean,
    val available_plans: List<Plan>,
    val workouts: List<TodayWorkout>?
)