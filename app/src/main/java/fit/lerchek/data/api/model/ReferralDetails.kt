package fit.lerchek.data.api.model

class ReferralDetails(
    val balance: Int?,
    val referral: String?,
    val weekly_transactions: List<WeaklyTransaction>?,
)

class WeaklyTransaction(
    val amount: Int?,
    val type: Int?,
    val confirmed: Int?,
    val created_at: String?,
    val type_code: String?,
    val phone: String?,
)