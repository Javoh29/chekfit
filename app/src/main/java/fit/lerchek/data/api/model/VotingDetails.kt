package fit.lerchek.data.api.model


class ChatDetails(
    val messages: List<ChatMessage>,
    val user_id: Int,
    val can_edit: Boolean
)

class ChatMessage(
    val user_id: Int,
    val text: String,
    val html: String?,
    val created_at: String
)
