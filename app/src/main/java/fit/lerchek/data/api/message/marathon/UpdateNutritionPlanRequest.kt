package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class UpdateNutritionPlanRequest(val nutrition_plan_id: String) : BaseRequest()