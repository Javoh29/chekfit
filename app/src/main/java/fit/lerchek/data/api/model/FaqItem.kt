package fit.lerchek.data.api.model


class FaqItem(
    val question:String,
    val answer:String
)