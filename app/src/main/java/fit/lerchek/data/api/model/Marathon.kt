package fit.lerchek.data.api.model

class Marathon(
    val name: String?,
    val id: Int,
    val start: String?,
    val end: String?,
    val status: String?,
    val image: HashMap<String,String>?
)