package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.ChatDetails
import fit.lerchek.data.api.model.ChatMessage

class ChatMessageResponse: BaseResponse<List<ChatMessage>>()