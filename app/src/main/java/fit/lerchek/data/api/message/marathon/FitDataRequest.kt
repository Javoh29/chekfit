package fit.lerchek.data.api.message.marathon

import com.google.gson.annotations.SerializedName
import fit.lerchek.data.api.message.base.BaseRequest

class FitDataRequest(
    val tracker: String,
    @SerializedName("data") val listFitData: List<FitData>
) : BaseRequest()

data class FitData(val date: String, var value: Int)
