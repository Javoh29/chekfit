package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class PushRequest(var device_id: String) : BaseRequest()