package fit.lerchek.data.api.model


class Faq(
    val title:String,
    val list: List<FaqItem>
)