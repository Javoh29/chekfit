package fit.lerchek.data.api.model

class Sex(
    val name: String?,
    val id: Int,
) {
    fun isMale() = id == 1
}