package fit.lerchek.data.api.model


class ContentFont(
    val style:String?,
    val text:String?
)