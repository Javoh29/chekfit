package fit.lerchek.data.api.model


import com.google.gson.annotations.SerializedName

data class DetailInfoTracker(
    @SerializedName("date")
    val date: String,
    @SerializedName("date_format")
    val dateFormat: String,
    @SerializedName("dimension")
    val dimension: Int,
    @SerializedName("goal")
    val goal: Int,
    @SerializedName("value")
    val value: Int
)