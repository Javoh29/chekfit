package fit.lerchek.data.api.model


class FoodDetails(
    val current_day:Int,
    val current_week:Int,
    val current_week_day_number:Int,
    val nutrition_plan_id:Int,
    val weeks: List<Week>,
    val recipes: List<TodayRecipe>,
    val today:String,
    val can_edit:Boolean,
    val can_add_phot:Boolean,
    val meals:HashMap<String, Meal>,
    val available_plans: List<Plan>
)