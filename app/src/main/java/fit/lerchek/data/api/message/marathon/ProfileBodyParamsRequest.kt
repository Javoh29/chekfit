package fit.lerchek.data.api.message.marathon


class ProfileBodyParamsRequest constructor(
   val weight: String?,
   val height: String?,
   val breast_volume: String?,
   val waist: String?,
   val hips: String?)