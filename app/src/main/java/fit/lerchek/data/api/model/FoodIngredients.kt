package fit.lerchek.data.api.model


class FoodIngredients(
    val current_week:Int,
    val weeks: List<Week>,
    val ingredients: List<Ingredient>,
)