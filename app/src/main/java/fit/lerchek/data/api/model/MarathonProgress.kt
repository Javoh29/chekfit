package fit.lerchek.data.api.model

class MarathonProgress(
    val progress: Progress,
    var current_day: Int,
    var current_week: Int,
    var current_week_day_number: Int,
    var weeks: List<Week>?,
    var today: String?,
    var can_edit: Boolean,
    var can_add_phot: Boolean,
    var collage: Any?,
    var statistics_weeks: List<StatisticsWeek>?
)

class Progress(
    var bodies: List<Body>? = null,
    var videos: List<Video>? = null,
    var photos: List<Photo>? = null,
    var selectedImagePosition: Int = 0,
)

class Body(
    var weight: Float? = null,
    var height: Float? = null,
    var breast_volume: Float? = null,
    var waist: Float? = null,
    var hips: Float? = null,
    var user_id: Int? = null,
    var week: Int = 0
)

class Photo(
    var id: Int,
    var front: String?,
    var front_original: String?,
    var back: String?,
    var back_original: String?,
    var side: String?,
    var side_original: String?,
    var week: Int,
    var review_by: Any?,
    var review_comment: String?,
    var review_status: Int,
    var review_date: Any?,
    var compressed: Any?
)

class Video(
    var week: Int,
    var review_comment: String?,
    var url: String?,
    var review_status: Int,
)

class StatisticsWeek(
    name: String = "",
    days: List<Day> = listOf(),
    var show_photo: Boolean = false,
    var show_video: Boolean = false,
    id: Int? = null
) : Week(name, id, days)
