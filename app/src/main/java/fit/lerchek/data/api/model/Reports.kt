package fit.lerchek.data.api.model

class Reports(
    val can_edit:Boolean?,
    val current_day:Int?,
    val weeks: List<Week>?,
    val reports: List<Report>?
)

class Report(
    val id:Int?,
    val text:String?,
    val week:Int?
)
