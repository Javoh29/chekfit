package fit.lerchek.data.api.message.marathon

import com.google.gson.JsonElement
import fit.lerchek.data.api.message.base.BaseResponse

class TaskToggleResponse : BaseResponse<JsonElement>()