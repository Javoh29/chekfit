package fit.lerchek.data.api.model

import java.math.BigDecimal


class VotingDetails(
    val users: List<VoteUser>?,
    val votes: List<Int>,
    val available_votes: Int,
    val max_votes: Int,
    val vote_id: Int,
    val is_active: Boolean?
)

class VoteUser(
    val first_name: String,
    val age: Int,
    val vote_count: Int,
    val weight_1: BigDecimal,
    val weight_2: BigDecimal,
    val id: Int,
    val front_1: String,
    val front_2: String,
    val side_1: String,
    val side_2: String,
    val back_1: String,
    val back_2: String
)