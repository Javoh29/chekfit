package fit.lerchek.data.api.model

class ExtraWorkout(
    val name: String,
    val marathon_id: Int
)
