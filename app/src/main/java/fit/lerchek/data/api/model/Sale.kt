package fit.lerchek.data.api.model

class Sale(
    val title: String?,
    val description: String?,
    val offers: List<SaleOffer>?
)