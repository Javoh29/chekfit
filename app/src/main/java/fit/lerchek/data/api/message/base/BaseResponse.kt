package fit.lerchek.data.api.message.base

import com.google.gson.JsonElement
import fit.lerchek.common.util.Constants

abstract class BaseResponse<R> {
    var success: Boolean = false
    var code: Int? = null
    var message: String? = null
    var string_code: String? = null
    var result: R? = null
    var errors: JsonElement? = null

    fun getErrorContent(): String {
        return if (success) {
            ""
        } else {
            message ?: string_code ?: errors?.toString() ?: Constants.UNKNOWN_ERROR
        }

    }
}