package fit.lerchek.data.api.model

import java.math.BigDecimal


class TodayRecipe(
    val id: Int,
    val base_recipe_id: Int?,
    var name: String,
    var wo_oven: Int?,
    var calories: BigDecimal,
    val image: HashMap<String,String>,
    var proteins: BigDecimal?,
    var carbohydrates: BigDecimal?,
    var fats: BigDecimal?,
    var has_replacements:Boolean?,
    var recipe_groups: List<RecipeGroup>?,
    var ingredients: List<Ingredient>?,
    var meals: List<Meal>?,
    var description: String?,
    var description_data: List<DynamicContentItem>?,
    var meal_name: String?,
    var meal_id: Int?,
    var day_number: Int?,
    var is_favorite: Boolean?,
    var nutrition_plan_id:Int?
)