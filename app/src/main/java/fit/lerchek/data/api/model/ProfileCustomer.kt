package fit.lerchek.data.api.model

class ProfileCustomer(
    val id: Int?,
    val email: String?,
    val profile: Profile?
)