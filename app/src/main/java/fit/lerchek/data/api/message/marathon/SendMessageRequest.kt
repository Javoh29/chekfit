package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class SendMessageRequest(val text: String) : BaseRequest()