package fit.lerchek.data.api.model

class SaleOffer(
    val title: String?,
    val price: String?,
    val link: String?,
    val button_text: String?
)