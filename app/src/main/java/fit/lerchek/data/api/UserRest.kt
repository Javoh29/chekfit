package fit.lerchek.data.api

import fit.lerchek.data.api.message.forgotpwd.CheckCodeForgotPwdRequest
import fit.lerchek.data.api.message.forgotpwd.ForgotPwdRequest
import fit.lerchek.data.api.message.forgotpwd.ForgotPwdResponse
import fit.lerchek.data.api.message.forgotpwd.ResetPwdRequest
import fit.lerchek.data.api.message.login.LoginRequest
import fit.lerchek.data.api.message.login.LoginResponse
import fit.lerchek.data.api.message.login.LogoutResponse
import fit.lerchek.data.api.message.marathon.*
import fit.lerchek.data.api.model.FavoriteDetails
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface UserRest {

    @POST("api/user/auth/login")
    fun login(@Body loginRequest: LoginRequest): Single<LoginResponse>

    @GET("api/auth/logout")
    fun logout(): Call<LogoutResponse>

    @GET("api/user/marathon")
    fun getMarathons(): Single<MarathonsResponse>

    @GET("api/user/marathon/{marathon}")
    fun getMarathonDetails(@Path(value = "marathon") marathon: Int): Call<MarathonDetailsResponse>

    @GET("api/user/marathon/{marathon}/profile")
    fun getMarathonProfileDetails(@Path(value = "marathon") marathon: Int): Single<MarathonProfileDetailsResponse>

    @PUT("api/user/marathon/{marathon}/profile")
    fun updateMarathonProfileTestDetails(
        @Path(value = "marathon") marathon: Int,
        @Body completeTestRequest: CompleteTestRequest
    ): Call<MarathonProfileDetailsResponse>

    @GET("api/user/marathon/{marathon}/calendar")
    fun getMarathonActivityCalendar(@Path(value = "marathon") marathon: Int): Call<MarathonActivityCalendarResponse>

    @GET("api/user/marathon/{marathon}/check-vote")
    fun getMarathonVote(@Path(value = "marathon") marathon: Int): Call<MarathonVoteResponse>

    @POST("api/user/marathon/{marathon}/task/{task}")
    fun completeTask(
        @Body request: TaskToggleRequest,
        @Path(value = "marathon") marathon: Int,
        @Path(value = "task") task: Int
    ): Single<TaskToggleResponse>

    @HTTP(method = "DELETE", path = "api/user/marathon/{marathon}/task/{task}", hasBody = true)
    fun restoreTask(
        @Body request: TaskToggleRequest,
        @Path(value = "marathon") marathon: Int,
        @Path(value = "task") task: Int
    ): Single<TaskToggleResponse>

    @GET("api/user/marathon/{marathon}/recipe")
    fun getFoodDetails(@Path(value = "marathon") marathon: Int): Single<FoodDetailsResponse>

    @GET("api/user/marathon/{marathon}/recipe/favorite")
    fun getFavorites(@Path(value = "marathon") marathon: Int): Single<FavoriteResponse>

    @GET("api/user/marathon/{marathon}/recipe/{recipe}")
    fun getRecipeDetails(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "recipe") recipe: Int
    ): Single<RecipeDetailsResponse>

    @GET("api/user/marathon/{marathon}/recipe/{recipe}/replacement")
    fun getRecipeReplacements(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "recipe") recipe: Int,
        @Query(value = "base_recipe_id") base_recipe_id: Int
    ): Single<RecipeReplacementsResponse>

    @GET("api/user/marathon/{marathon}/extra-recipe")
    fun getExtraRecipes(
        @Path(value = "marathon") marathon: Int,
    ): Single<ExtraRecipesResponse>

    @GET("api/user/marathon/{marathon}/extra-recipe/{recipe}")
    fun getExtraRecipeDetails(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "recipe") recipe: Int
    ): Single<RecipeDetailsResponse>

    @GET("api/user/marathon/{marathon}/cdfc")
    fun getCdfc(
        @Path(value = "marathon") marathon: Int,
    ): Single<CdfcResponse>

    @POST("api/user/marathon/{marathon}/recipe/{recipe}/replace")
    fun replaceRecipe(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "recipe") recipe: Int,
        @Body request: ReplaceRecipeRequest,
    ): Single<TaskToggleResponse>

    @GET("api/user/marathon/{marathon}/workout/{workout}")
    fun getWorkoutDetails(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "workout") workout: Int
    ): Single<WorkoutResponse>

    @GET("api/user/marathon/{marathon}/ingredients")
    fun getFoodIngredients(
        @Path(value = "marathon") marathon: Int,
        @Query("week") week: Int?
    ): Single<FoodIngredientsResponse>

    @POST("api/user/marathon/{marathon}/ingredients/all")
    fun checkFoodIngredients(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateIngredientsRequest
    ): Single<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/ingredients/{ingredient}")
    fun checkFoodIngredient(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "ingredient") ingredient: Int,
        @Query(value = "week") week: Int
    ): Single<TaskToggleResponse>

    @DELETE("api/user/marathon/{marathon}/ingredients/{ingredient}")
    fun unCheckFoodIngredient(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "ingredient") ingredient: Int,
        @Query(value = "week") week: Int
    ): Single<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/recipe")
    fun updateNutritionPlan(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateNutritionPlanRequest
    ): Single<FoodDetailsResponse>

    @PUT("api/user/profile")
    fun updateProfileDetails(@Body request: UpdateProfileRequest): Single<UpdateProfileDetailsResponse>

    @GET("api/user/marathon/{marathon}/workout")
    fun getWorkoutsDetails(@Path(value = "marathon") marathon: Int): Single<WorkoutDetailsResponse>

    @GET("api/user/marathon/{marathon}/tracker")
    fun getTodayTrackers(@Path(value = "marathon") marathon: Int): Call<TrackersTodayResponse>

    @POST("api/user/marathon/{marathon}/workout")
    fun updateWorkoutLevel(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateWorkoutLevelRequest
    ): Single<WorkoutDetailsResponse>

    @PUT("api/user/marathon/{marathon}/tracker/water")
    fun addCountWaterTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerRequest
    ): Single<TaskToggleResponse>

    @PUT("api/user/marathon/{marathon}/tracker/water")
    fun addCountWaterPutTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerRequest
    ): Call<TaskToggleResponse>

    @HTTP(method = "DELETE", path = "api/user/marathon/{marathon}/tracker/water", hasBody = true)
    fun deleteCountWaterTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerRequest
    ): Single<TaskToggleResponse>

    @PUT("api/user/marathon/{marathon}/tracker/step")
    fun addCountStepTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerRequest
    ): Single<TaskToggleResponse>

    @PUT("api/user/marathon/{marathon}/tracker/step")
    fun addCountStepPutTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerRequest
    ): Call<TaskToggleResponse>

    @PUT("api/user/marathon/{marathon}/tracker/dream")
    fun addCountDreamTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerRequest
    ): Single<TaskToggleResponse>

    @PUT("api/user/marathon/{marathon}/tracker/dream")
    fun addCountDreamPutTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerRequest
    ): Call<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/tracker/water/goal")
    fun updateGoalWaterTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerGoalRequest
    ): Single<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/tracker/step/goal")
    fun updateGoalStepTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerGoalRequest
    ): Single<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/tracker/dream/goal")
    fun updateGoalSleepTracker(
        @Path(value = "marathon") marathon: Int,
        @Body request: UpdateTrackerGoalRequest
    ): Single<TaskToggleResponse>

    @GET("api/user/marathon/{marathon}/faq/{alias}")
    fun getFaq(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "alias") alias: String
    ): Single<FaqResponse>

    @GET("api/user/marathon/{marathon}/tracker/{type}")
    fun getDetailInfoTracker(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "type") type: String,
        @Query(value = "period") period: String
    ): Single<DetailInfoTrackerResponse>

    @GET("/api/user/pa-page/{alias}")
    fun getPaPage(
        @Path(value = "alias") alias: String
    ): Single<PaPageResponse>

    @GET("api/user/profile")
    fun getProfileDetails(): Single<ProfileDetailsResponse>

    @GET("api/user/marathon/{marathon}/report")
    fun getMarathonReports(@Path(value = "marathon") marathon: Int): Single<ReportsResponse>

    @POST("api/user/marathon/{marathon}/report")
    fun saveMarathonReport(
        @Path(value = "marathon") marathon: Int,
        @Body request: SaveReportRequest
    ): Single<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/tracker/batch-update")
    fun setFitUpdate(
        @Path(value = "marathon") marathon: Int,
        @Body request: FitDataRequest
    ): Single<TaskToggleResponse>

    @GET("api/user/referral-transaction")
    fun getReferralDetails(): Single<ReferralDetailsResponse>

    @POST("api/user/referral-transaction/withdraw")
    fun withdraw(@Body request: WithdrawRequest): Call<TaskToggleResponse>

    @POST("api/user/auth/password/get-code")
    fun requestForgotPwdCode(@Body request: ForgotPwdRequest): Single<ForgotPwdResponse>

    @POST("api/user/auth/password/validate-code")
    fun confirmForgotPwdCode(@Body request: CheckCodeForgotPwdRequest): Single<ForgotPwdResponse>

    @POST("api/user/auth/password/reset")
    fun resetPwdCode(@Body request: ResetPwdRequest): Single<LoginResponse>

    @GET("api/user/marathon/{marathon}/body")
    fun getProfileParamsByMarathon(@Path(value = "marathon") marathon: Int): Single<ProfileBodyParamsResponse>

    @PUT("api/user/marathon/{marathon}/body")
    fun updateBodyParamsByMarathon(
        @Path(value = "marathon") marathon: Int,
        @Body params: ProfileBodyParamsRequest
    ): Single<ProfileBodyParamsResponse>

    @GET("api/user/marathon/{marathon}/vote")
    fun getVoting(@Path(value = "marathon") marathon: Int): Single<VotingResponse>

    @GET("api/user/marathon/{marathon}/qa")
    fun getChat(@Path(value = "marathon") marathon: Int): Single<ChatResponse>

    @POST("api/user/marathon/{marathon}/qa")
    fun sendChatMessage(
        @Path(value = "marathon") marathon: Int,
        @Body request: SendMessageRequest
    ): Single<ChatMessageResponse>

    @POST("api/user/marathon/{marathon}/vote/{vote}")
    fun like(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "vote") vote: Int,
        @Body request: VoteRequest
    ): Call<VotingResponse>

    @HTTP(method = "DELETE", path = "api/user/marathon/{marathon}/vote/{vote}", hasBody = true)
    fun dislike(
        @Path(value = "marathon") marathon: Int,
        @Path(value = "vote") vote: Int,
        @Body request: VoteRequest
    ): Call<VotingResponse>

    @PUT("api/user/device")
    fun updatePushToken(@Body pushRequest:PushRequest):Call<TaskToggleResponse>

    @Multipart
    @POST("api/user/marathon/{marathon}/progress/photo")
    fun uploadPhoto(
        @Path(value = "marathon") marathon: Int,
        @Part photo: MultipartBody.Part,
        @Part ("type") type : RequestBody,
        @Part ("week") week : RequestBody,
    ):Call<TaskToggleResponse>

    @HTTP(method = "DELETE", path = "api/user/device", hasBody = true)
    fun removePushToken(@Body pushRequest:PushRequest):Call<TaskToggleResponse>

    @GET("api/user/marathon/{marathon}/progress")
    fun getProgressForMarathon(@Path(value = "marathon") marathon: Int): Single<MarathonProgressResponse>

    @POST("api/user/marathon/{marathon}/progress")
    fun saveBodyProgressForMarathon(@Path(value = "marathon") marathon: Int, @Body body: fit.lerchek.data.api.model.Body): Single<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/progress/video")
    fun saveVideoLinkForMarathon(@Path(value = "marathon") marathon: Int, @Body video: VideoRequest): Single<TaskToggleResponse>

    @POST("api/user/marathon/{marathon}/recipe/favorite")
    fun setFavorite(@Path(value = "marathon") marathon: Int, @Query("recipe_id") recipe_id: Int): Call<FavoriteToggleResponse>

    @HTTP(method = "DELETE", path = "api/user/marathon/{marathon}/recipe/favorite", hasBody = true)
    fun deleteFavorite(@Path(value = "marathon") marathon: Int, @Query("recipe_id") recipe_id: Int): Call<FavoriteToggleResponse>

}