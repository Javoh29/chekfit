package fit.lerchek.data.api.model

class Meal(
    val id: Int,
    var name: String,
    var index_in_day: Int?,
    val image: HashMap<String,String>
)