package fit.lerchek.data.api.model

class ActivityCalendar(
    val current_day: Int?,
    val current_marathon_day: Int?,
    val tasks: Map<String, List<TodayTask>>?,
    val completed_task: Map<String, List<CompletedTask>>?,
    val calendar: Map<String, ActivityCalendarMonth>?
)