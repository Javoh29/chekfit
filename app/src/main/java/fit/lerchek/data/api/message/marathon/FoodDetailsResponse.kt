package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.FoodDetails
import fit.lerchek.data.api.model.ProfileDetails

class FoodDetailsResponse : BaseResponse<FoodDetails>()