package fit.lerchek.data.api.model

class MarathonVote(
    val id: Int?,
    val name: String?,
    val stage: Int?
)