package fit.lerchek.data.api.model


class RecipeGroup(
    val id:Int,
    val name:String
)