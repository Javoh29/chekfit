package fit.lerchek.data.api.model


class MarathonDetails(
    val marathon_name: String?,
    val start: String?,
    val ref: String?,
    val app_menu: AppMenu?,
    val current_day: Int?,
    val current_marathon_day: Int?,
    val telegram_channel: String?,
    val telegram_group: String?,
    val renewal_price: String?,
    val pay_link: String?,
    val sale: Sale?,
    val tasks: List<TodayTask>?,
    val recipes: List<TodayRecipe>?,
    val workouts: List<TodayWorkout>?,
    val completed_tasks: List<Int>?,
    val image: HashMap<String,String>
)