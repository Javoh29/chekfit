package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.ProfileDetails

class ProfileDetailsResponse : BaseResponse<ProfileDetails>()
class UpdateProfileDetailsResponse : BaseResponse<UpdateResponseProfile>()
class UpdateResponseProfile(val original : ProfileDetailsResponse)