package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.MarathonDetails
import fit.lerchek.data.api.model.MarathonProfileDetails

class MarathonProfileDetailsResponse : BaseResponse<MarathonProfileDetails>()