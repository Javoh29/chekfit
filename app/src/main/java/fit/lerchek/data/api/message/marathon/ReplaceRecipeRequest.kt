package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class ReplaceRecipeRequest(
    val recipeId: Int,
    val marathonId: Int,
    val replacement_recipe_id: Int,
    val nutrition_plan_id: Int,
    val day_number: Int
) : BaseRequest()