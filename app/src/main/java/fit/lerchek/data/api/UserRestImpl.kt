package fit.lerchek.data.api

import fit.lerchek.data.api.message.forgotpwd.CheckCodeForgotPwdRequest
import fit.lerchek.data.api.message.forgotpwd.ForgotPwdRequest
import fit.lerchek.data.api.message.forgotpwd.ResetPwdRequest
import fit.lerchek.data.api.message.login.LoginRequest
import fit.lerchek.data.api.message.login.LoginResponse
import fit.lerchek.data.api.message.marathon.*
import fit.lerchek.data.api.model.Body
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRestImpl @Inject constructor(private val retrofit: Retrofit) : UserRest {

    private val service by lazy { retrofit.create(UserRest::class.java) }

    override fun login(loginRequest: LoginRequest) = service.login(loginRequest)

    override fun logout() = service.logout()

    override fun getMarathons() = service.getMarathons()

    override fun getMarathonActivityCalendar(marathon: Int) =
        service.getMarathonActivityCalendar(marathon)

    override fun getMarathonDetails(marathon: Int) = service.getMarathonDetails(marathon)

    override fun getMarathonProfileDetails(marathon: Int) =
        service.getMarathonProfileDetails(marathon)

    override fun updateMarathonProfileTestDetails(
        marathon: Int,
        completeTestRequest: CompleteTestRequest
    ) = service.updateMarathonProfileTestDetails(marathon, completeTestRequest)

    override fun getMarathonVote(marathon: Int) = service.getMarathonVote(marathon)

    override fun completeTask(
        request: TaskToggleRequest,
        marathon: Int,
        task: Int
    ) = service.completeTask(request, marathon, task)

    override fun restoreTask(
        request: TaskToggleRequest,
        marathon: Int,
        task: Int
    ) = service.restoreTask(request, marathon, task)

    override fun getFoodDetails(marathon: Int) = service.getFoodDetails(marathon)

    override fun getFavorites(marathon: Int) = service.getFavorites(marathon)

    override fun getRecipeDetails(marathon: Int, recipe: Int) =
        service.getRecipeDetails(marathon, recipe)

    override fun getRecipeReplacements(
        marathon: Int,
        recipe: Int,
        base_recipe_id: Int
    ) = service.getRecipeReplacements(marathon, recipe, base_recipe_id)

    override fun getExtraRecipes(marathon: Int) = service.getExtraRecipes(marathon)

    override fun getExtraRecipeDetails(marathon: Int, recipe: Int) =
        service.getExtraRecipeDetails(marathon, recipe)

    override fun getCdfc(marathon: Int) = service.getCdfc(marathon)

    override fun replaceRecipe(
        marathon: Int,
        recipe: Int,
        request: ReplaceRecipeRequest
    ) = service.replaceRecipe(marathon, recipe, request)

    override fun getWorkoutDetails(marathon: Int, workout: Int) =
        service.getWorkoutDetails(marathon, workout)


    override fun updateNutritionPlan(
        marathon: Int,
        request: UpdateNutritionPlanRequest
    ) = service.updateNutritionPlan(marathon, request)


    override fun updateProfileDetails(request: UpdateProfileRequest) =
        service.updateProfileDetails(request)

    override fun requestForgotPwdCode(request: ForgotPwdRequest) =
        service.requestForgotPwdCode(request)

    override fun confirmForgotPwdCode(request: CheckCodeForgotPwdRequest) =
        service.confirmForgotPwdCode(request)

    override fun resetPwdCode(request: ResetPwdRequest): Single<LoginResponse> =
        service.resetPwdCode(request)

    override fun getProfileParamsByMarathon(marathon: Int): Single<ProfileBodyParamsResponse> =
        service.getProfileParamsByMarathon(marathon)

    override fun updateBodyParamsByMarathon(
        marathon: Int,
        params: ProfileBodyParamsRequest
    ): Single<ProfileBodyParamsResponse> {
        return service.updateBodyParamsByMarathon(marathon, params)
    }

    override fun getProgressForMarathon(marathon: Int): Single<MarathonProgressResponse> {
        return service.getProgressForMarathon(marathon)
    }

    override fun saveBodyProgressForMarathon(
        marathon: Int,
        body: Body
    ): Single<TaskToggleResponse> {
        return service.saveBodyProgressForMarathon(marathon, body)
    }

    override fun saveVideoLinkForMarathon(
        marathon: Int,
        video: VideoRequest
    ): Single<TaskToggleResponse> {
        return service.saveVideoLinkForMarathon(marathon, video)
    }

    override fun setFavorite(marathon: Int, recipe_id: Int): Call<FavoriteToggleResponse> {
        return service.setFavorite(marathon, recipe_id)
    }

    override fun deleteFavorite(marathon: Int, recipe_id: Int): Call<FavoriteToggleResponse> {
        return service.deleteFavorite(marathon, recipe_id)
    }

    override fun getWorkoutsDetails(marathon: Int) = service.getWorkoutsDetails(marathon)

    override fun getTodayTrackers(marathon: Int) = service.getTodayTrackers(marathon)

    override fun updateWorkoutLevel(
        marathon: Int,
        request: UpdateWorkoutLevelRequest
    ) = service.updateWorkoutLevel(marathon, request)

    override fun addCountWaterTracker(
        marathon: Int,
        request: UpdateTrackerRequest
    ): Single<TaskToggleResponse> {
        return service.addCountWaterTracker(marathon, request)
    }

    override fun addCountWaterPutTracker(
        marathon: Int,
        request: UpdateTrackerRequest
    ) = service.addCountWaterPutTracker(marathon, request)

    override fun deleteCountWaterTracker(
        marathon: Int,
        request: UpdateTrackerRequest
    ): Single<TaskToggleResponse> {
        return service.deleteCountWaterTracker(marathon, request)
    }

    override fun addCountStepTracker(
        marathon: Int,
        request: UpdateTrackerRequest
    ) = service.addCountStepTracker(marathon, request)

    override fun addCountStepPutTracker(
        marathon: Int,
        request: UpdateTrackerRequest
    ) = service.addCountStepPutTracker(marathon, request)

    override fun addCountDreamTracker(
        marathon: Int,
        request: UpdateTrackerRequest
    ) = service.addCountDreamTracker(marathon, request)

    override fun addCountDreamPutTracker(
        marathon: Int,
        request: UpdateTrackerRequest
    ) = service.addCountDreamPutTracker(marathon, request)

    override fun updateGoalWaterTracker(
        marathon: Int,
        request: UpdateTrackerGoalRequest
    ): Single<TaskToggleResponse> {
        return service.updateGoalWaterTracker(marathon, request)
    }

    override fun updateGoalStepTracker(
        marathon: Int,
        request: UpdateTrackerGoalRequest
    ): Single<TaskToggleResponse> {
        return service.updateGoalStepTracker(marathon, request)
    }

    override fun updateGoalSleepTracker(
        marathon: Int,
        request: UpdateTrackerGoalRequest
    ): Single<TaskToggleResponse> {
        return service.updateGoalSleepTracker(marathon, request)
    }

    override fun getFoodIngredients(marathon: Int, week: Int?) =
        service.getFoodIngredients(marathon, week)

    override fun checkFoodIngredients(
        marathon: Int,
        request: UpdateIngredientsRequest
    ) = service.checkFoodIngredients(marathon, request)

    override fun checkFoodIngredient(
        marathon: Int,
        ingredient: Int,
        week: Int
    ) = service.checkFoodIngredient(marathon, ingredient, week)

    override fun unCheckFoodIngredient(
        marathon: Int,
        ingredient: Int,
        week: Int
    ) = service.unCheckFoodIngredient(marathon, ingredient, week)

    override fun getFaq(marathon: Int, alias: String) = service.getFaq(marathon, alias)

    override fun getDetailInfoTracker(
        marathon: Int,
        type: String,
        period: String
    ): Single<DetailInfoTrackerResponse> = service.getDetailInfoTracker(marathon, type, period)

    override fun getPaPage(alias: String) = service.getPaPage(alias)

    override fun getProfileDetails() = service.getProfileDetails()

    override fun getMarathonReports(marathon: Int) = service.getMarathonReports(marathon)

    override fun saveMarathonReport(
        marathon: Int,
        request: SaveReportRequest
    ) = service.saveMarathonReport(marathon, request)

    override fun setFitUpdate(marathon: Int, request: FitDataRequest) = service.setFitUpdate(marathon, request)

    override fun getReferralDetails() = service.getReferralDetails()

    override fun withdraw(request: WithdrawRequest) = service.withdraw(request)

    override fun getVoting(marathon: Int) = service.getVoting(marathon)

    override fun getChat(marathon: Int) = service.getChat(marathon)

    override fun sendChatMessage(
        marathon: Int,
        request: SendMessageRequest
    ) = service.sendChatMessage(marathon, request)

    override fun like(marathon: Int, vote: Int, request: VoteRequest) =
        service.like(marathon, vote, request)

    override fun dislike(marathon: Int, vote: Int, request: VoteRequest) =
        service.dislike(marathon, vote, request)

    override fun updatePushToken(pushRequest: PushRequest) = service.updatePushToken(pushRequest)

    override fun uploadPhoto(
        marathon: Int,
        photo: MultipartBody.Part,
        type: RequestBody,
        week: RequestBody
    ) = service.uploadPhoto(marathon, photo, type, week)

    override fun removePushToken(pushRequest: PushRequest) = service.removePushToken(pushRequest)

}