package fit.lerchek.data.api.model

class AppMenu(
    val marathon_name: String?,
    val qa: Boolean?,
    val telegram_channel: String?,
    val telegram_group: String?,
    val report: Boolean?,
    val vote: Boolean?,
    val instruction: Boolean?,
    val product_list: Boolean?,
    val show_calendar_button: Boolean?,
    val ref: Boolean?,
    val extra_workout: List<ExtraWorkout>?
)