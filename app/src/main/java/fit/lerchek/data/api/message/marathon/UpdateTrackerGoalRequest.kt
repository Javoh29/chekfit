package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class UpdateTrackerGoalRequest(val goal: Int, val dimension: Int) : BaseRequest()