package fit.lerchek.data.api.message.forgotpwd

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.Email

class ForgotPwdResponse : BaseResponse<Nothing>()