package fit.lerchek.data.api.model


class Day(
    val name: String,
    val day_in_month: Int,
    val day_number: Int,
    val month: Int,
)