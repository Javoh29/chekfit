package fit.lerchek.data.api.model

class NutritionPlan(
    val id: Int,
    var name: String,
    var description: String,
    val image: HashMap<String,String>
)