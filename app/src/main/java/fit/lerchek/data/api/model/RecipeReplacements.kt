package fit.lerchek.data.api.model


class RecipeReplacements(
    val recipe: TodayRecipe,
    val replacements: List<TodayRecipe>
)