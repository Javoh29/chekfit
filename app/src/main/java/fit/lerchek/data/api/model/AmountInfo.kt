package fit.lerchek.data.api.model

import java.math.BigDecimal


class AmountInfo(
    val amount_round: BigDecimal?,
    val amount: BigDecimal?,
    val amount_gram: BigDecimal?,
    val dimension_type_name: String,
    val recipe_ingredient_group_id: Int,
)