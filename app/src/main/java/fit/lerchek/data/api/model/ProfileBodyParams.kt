package fit.lerchek.data.api.model


class ProfileBodyParams constructor(
   val weight: String,
   val height: String,
   val breast_volume: String,
   val waist: String,
   val hips: String,
   val marathon_profile_id: String)