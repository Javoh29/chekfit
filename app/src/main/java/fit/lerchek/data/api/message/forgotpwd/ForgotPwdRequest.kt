package fit.lerchek.data.api.message.forgotpwd

import com.google.gson.annotations.SerializedName

class ForgotPwdRequest(@SerializedName("email") var email : String?)