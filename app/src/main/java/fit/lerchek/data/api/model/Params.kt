package fit.lerchek.data.api.model

class Params (var family_status : FamilyStatus)

class FamilyStatus(val id : String , val name : String, val alias : String, val values : ArrayList<FamilyStatusValue>, val descriptionT: String?)

class FamilyStatusValue(val id : Int , val dictionary_id : String ,val value : String,  val description : String?)

class ProfileParams(
    val weight: String?,
    val height: String?,
    val breast: String?,
    val waist: String?,
    val hips: String?
)
