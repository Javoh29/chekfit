package fit.lerchek.data.api.message.marathon

import java.util.*

class UpdateProfileRequest
    constructor( val last_name: String?,
             val first_name: String?,
             val middle_name: String?,
             val phone: String?,
             val birth_date: String?,
             val city: String?,
             val children_count: String?,
             val instagram: String?,
             val telegram: String?,
             val family_status: String?
)