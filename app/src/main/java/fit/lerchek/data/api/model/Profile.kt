package fit.lerchek.data.api.model

import java.util.*

open class Profile(
    val user_id: Int?,
    val last_name: String?,
    val first_name: String?,
    val middle_name: String?,
    val phone: String?,
    val birth_date: String?,
    val city: String?,
    val children_count: String?,
    val instagram: String?,
    val telegram: String?,
    val sex: Sex?,
    var email: String?,
    val family_status: FamilyStatus?)

open class FamilyStatusData(
    val id : Int?,
    val dictionary_id : Int?,
    val value : String?,
    val value2 : String?,
    val is_active : Int?,
)