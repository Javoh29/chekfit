package fit.lerchek.data.api.message.login

import fit.lerchek.data.api.message.base.BaseRequest

class LoginRequest(
    val email: String,
    val password: String,
    val device_id: String? = null
) : BaseRequest()