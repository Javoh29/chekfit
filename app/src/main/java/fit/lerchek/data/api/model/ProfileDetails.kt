package fit.lerchek.data.api.model

class ProfileDetails(
    val customer: ProfileCustomer?,
    val marathon_count: Int?,
    val marathon_days: Int?,
    val prizes: Int?,
    val params : Params?
)