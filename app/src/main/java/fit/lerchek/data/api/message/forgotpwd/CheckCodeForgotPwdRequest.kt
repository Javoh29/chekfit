package fit.lerchek.data.api.message.forgotpwd

import com.google.gson.annotations.SerializedName

class CheckCodeForgotPwdRequest(@SerializedName("code") var code   : String? , @SerializedName("email") var email : String?)