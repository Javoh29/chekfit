package fit.lerchek.data.api.model


class ExtraRecipes(
    val recipes: List<TodayRecipe>,
    val meals: List<Meal>
)