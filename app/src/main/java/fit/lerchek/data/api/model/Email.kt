package fit.lerchek.data.api.model

import com.google.gson.JsonElement

open class Email (val email: JsonElement)