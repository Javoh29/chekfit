package fit.lerchek.data.api.message.marathon

import com.google.gson.annotations.SerializedName
import fit.lerchek.data.api.message.base.BaseRequest

class UpdateIngredientsRequest(
    val week: Int,
    @SerializedName("ingredient_id") val ingredientIds: List<Int>
) : BaseRequest()