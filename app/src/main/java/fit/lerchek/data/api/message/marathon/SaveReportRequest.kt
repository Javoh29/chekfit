package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class SaveReportRequest(val week: Int, val text: String) : BaseRequest()