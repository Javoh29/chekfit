package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.Cdfc
import fit.lerchek.data.api.model.ExtraRecipes
import fit.lerchek.data.api.model.RecipeDetails
import fit.lerchek.data.api.model.RecipeReplacements

class CdfcResponse : BaseResponse<Cdfc>()