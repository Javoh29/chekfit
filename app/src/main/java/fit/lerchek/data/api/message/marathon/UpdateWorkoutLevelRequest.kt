package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class UpdateWorkoutLevelRequest(val workout_plan_id: String) : BaseRequest()