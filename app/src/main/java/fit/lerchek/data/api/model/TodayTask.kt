package fit.lerchek.data.api.model


class TodayTask(
    val name: String?,
    val id: Int,
    val description: String?
)