package fit.lerchek.data.api.model

import com.google.gson.JsonElement

class Login(
    val token: String?,
    val email: JsonElement?,
    val password: List<String>?,
    val name: String?,
    val profile: Profile?,
    val ref: String?
)