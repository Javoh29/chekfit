package fit.lerchek.data.api.model

import java.math.BigDecimal


class Ingredient(
    val name:String,
    val amount: Int?,
    val amount_info: AmountInfo?,
    val id: Int,
    val in_cart: Boolean?,
    val category_id: Int?,
    var calories: BigDecimal?,
    val image: HashMap<String,String>?,
    var proteins: BigDecimal?,
    var carbohydrates: BigDecimal?,
    var fats: BigDecimal?,
)