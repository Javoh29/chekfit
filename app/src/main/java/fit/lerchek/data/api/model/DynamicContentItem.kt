package fit.lerchek.data.api.model


class DynamicContentItem(
    val type:String?,
    val font:ContentFont?,
    val href:String?,
    val src:String?,
    val src2x:String?,
    val children: List<DynamicContentItem>?
) {
    fun isParagraph() = type == "paragraph"
    fun isList() = type in arrayOf("list", "number_list")
    fun isListItem() = type == "list_item"
    fun isSpan() = type == "span"
    fun isStrong() = font?.style == "strong"
    fun isWhiteBlock() = type == "white-block"
    fun isNumber() = type == "number"
    fun isTitle() = type == "title"
    fun isTitle2() = type == "title-2"
    fun isImage() = type == "image"
    fun isVideo() = type == "video"
}