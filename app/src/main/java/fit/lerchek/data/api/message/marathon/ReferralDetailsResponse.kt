package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.ProfileDetails
import fit.lerchek.data.api.model.ReferralDetails

class ReferralDetailsResponse : BaseResponse<ReferralDetails>()