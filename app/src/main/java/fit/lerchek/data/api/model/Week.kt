package fit.lerchek.data.api.model


open class Week(
    val name: String = "",
    var id: Int? = null,
    val days: List<Day> = listOf()
)