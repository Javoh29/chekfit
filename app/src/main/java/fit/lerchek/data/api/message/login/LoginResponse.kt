package fit.lerchek.data.api.message.login

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.Login

class LoginResponse : BaseResponse<Login>()