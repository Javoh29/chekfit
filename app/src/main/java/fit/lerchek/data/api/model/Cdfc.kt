package fit.lerchek.data.api.model


class Cdfc(
    val ingredients: List<Ingredient>,
    val categories: List<CdfcCategory>
)