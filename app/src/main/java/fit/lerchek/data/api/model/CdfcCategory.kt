package fit.lerchek.data.api.model

class CdfcCategory(
    val id: Int,
    var name: String,
    val image: HashMap<String,String>
)