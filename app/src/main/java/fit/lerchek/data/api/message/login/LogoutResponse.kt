package fit.lerchek.data.api.message.login

import com.google.gson.JsonElement
import fit.lerchek.data.api.message.base.BaseResponse

class LogoutResponse : BaseResponse<JsonElement>()