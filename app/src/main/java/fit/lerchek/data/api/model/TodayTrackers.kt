package fit.lerchek.data.api.model


import com.google.gson.annotations.SerializedName

data class TodayTrackers(
    @SerializedName("date")
    val date: String?,
    @SerializedName("trackers")
    val trackers: Trackers?
)

data class Trackers(
    @SerializedName("dream")
    val dream: Dream?,
    @SerializedName("step")
    val step: Step?,
    @SerializedName("water")
    val water: Water?
)

data class Water(
    @SerializedName("dimension")
    val dimension: Int?,
    @SerializedName("goal")
    val goal: Int?,
    @SerializedName("value")
    val value: Int?
)

data class Step(
    @SerializedName("dimension")
    val dimension: Int?,
    @SerializedName("goal")
    val goal: Int?,
    @SerializedName("value")
    val value: Int?
)

data class Dream(
    @SerializedName("dimension")
    val dimension: Int?,
    @SerializedName("goal")
    val goal: Int?,
    @SerializedName("value")
    val value: Int?
)