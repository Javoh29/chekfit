package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class CompleteTestRequest(
    var daily_activity_id: Int? = null,
    var diet_purpose_id: Int? = null,
    var smoke_id: Int? = null,
    var alcohol_id: Int? = null,
    var lactation_id: Int? = null,
    var pregnancy_id: Int? = null,
    var nutrition_plan_id: Int? = null,
    var marathonId: String? = null,
    var workout_plan_id: Int? = null,
    var goals: String? = null,
) : BaseRequest()