package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.RecipeDetails
import fit.lerchek.data.api.model.RecipeReplacements

class RecipeReplacementsResponse : BaseResponse<RecipeReplacements>()