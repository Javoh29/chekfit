package fit.lerchek.data.api.model

data class VimeoModel(
    val videoUrl: String,
    val posterImage: String,
)
