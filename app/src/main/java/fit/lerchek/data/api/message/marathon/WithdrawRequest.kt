package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class WithdrawRequest(val amount: Int, val phone: String) : BaseRequest()