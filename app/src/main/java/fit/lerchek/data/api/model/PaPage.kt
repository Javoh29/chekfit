package fit.lerchek.data.api.model


class PaPage(
    val title:String,
    val description_data: List<DynamicContentItem>?,
    val body_data: List<DynamicContentItem>?,
    val body: String?
)