package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class UpdateTrackerRequest(val date: String?, val value: Int) : BaseRequest()