package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class VoteRequest(var to: Int) : BaseRequest()