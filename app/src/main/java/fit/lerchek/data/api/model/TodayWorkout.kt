package fit.lerchek.data.api.model

import com.google.gson.JsonElement


class TodayWorkout(
    val id: Int,
    var name: String,
    var description: String?,
    var description_data: JsonElement?,
    var video_url: String?,
    var video_type: String?,
    var day_number: Int?,
    var index_in_day: Int?,
    val image: HashMap<String,String>?
)