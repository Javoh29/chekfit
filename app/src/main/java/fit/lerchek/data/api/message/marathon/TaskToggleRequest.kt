package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class TaskToggleRequest(val day_number: Int) : BaseRequest()