package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class VideoRequest(val url: String, val week: Int) : BaseRequest()