package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseRequest

class UploadPhotoRequest(val type: String, val week:Int) : BaseRequest()