package fit.lerchek.data.api.model

class FavoriteDetails(
    val meals:HashMap<String, Meal>,
    val recipes: List<TodayRecipe>,
)