package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.TodayTrackers

class TrackersTodayResponse : BaseResponse<TodayTrackers>()