package fit.lerchek.data.api.message.marathon

import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.model.ProfileDetails
import fit.lerchek.data.api.model.ReferralDetails
import fit.lerchek.data.api.model.Reports

class ReportsResponse : BaseResponse<Reports>()