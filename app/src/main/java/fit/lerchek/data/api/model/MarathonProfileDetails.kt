package fit.lerchek.data.api.model

class MarathonProfileDetails(
    val profile: MarathonProfile?,
    val params: HashMap<String, TestParam>?
)

class MarathonProfile(
    val daily_activity_id: Int?,
    val diet_purpose_id: Int?,
    val smoke_id: Int?,
    val alcohol_id: Int?,
    val lactation_id: Int?,
    val pregnancy_id: Int?,
    val nutrition_plan_id: Int?,
    val workout_plan_id: Int?,
    val goals: String?,
    val family_status: Int?
) {
    fun getDailyActivityId() = daily_activity_id ?: 0
    fun getDietPurposeId() = diet_purpose_id ?: 0
    fun getSmokeId() = smoke_id ?: 0
    fun getAlcoholId() = alcohol_id ?: 0
    fun getLactationId() = lactation_id ?: 0
    fun getPregnancyId() = pregnancy_id ?: 0
    fun getNutritionPlanId() = nutrition_plan_id ?: 0
    fun getWorkoutPlanId() = workout_plan_id ?: 0
    fun getFamilyStatus() = family_status ?: 0
}

class TestParam(
    val id: Int?,
    val name: String?,
    val description: String?,
    val values: List<TestParamValue>?
)

class TestParamValue(
    val id: Int,
    val value: String?,
    val value2: String?,
    val name: String?,
    val description: String?,
)