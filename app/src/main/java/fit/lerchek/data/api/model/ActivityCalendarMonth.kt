package fit.lerchek.data.api.model

class ActivityCalendarMonth(val name: String? = "", val days : Map<String, List<Int?>>? = mapOf())