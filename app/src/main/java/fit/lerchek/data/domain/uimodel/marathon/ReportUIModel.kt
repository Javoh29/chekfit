package fit.lerchek.data.domain.uimodel.marathon

class ReportUIModel(
    var reportText:String,
    var welcomeText:String,
    var insideVisible:Boolean
)