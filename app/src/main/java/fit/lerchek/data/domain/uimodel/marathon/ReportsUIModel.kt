package fit.lerchek.data.domain.uimodel.marathon

class ReportsUIModel(
    private val reports: Map<Int, ReportUIModel>,
    val weeks: List<WeekUIModel>,
    val canEdit:Boolean
) {

    fun getSelectedWeek(): WeekUIModel? {
        return weeks.firstOrNull { it.selected } ?: weeks.firstOrNull()
    }

    fun selectWeek(id: Int?) {
        weeks.onEach { it.selected = it.id == id }
    }

    fun getReport(): ReportUIModel? {
        return try {
            reports[getSelectedWeek()!!.id]
        } catch (_: Throwable) {
            null
        }
    }
}