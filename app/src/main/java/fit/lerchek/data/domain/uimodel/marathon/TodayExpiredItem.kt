package fit.lerchek.data.domain.uimodel.marathon

class TodayExpiredItem(
    var imageUrl: String? = null,
    var renewalPrice: String? = null,
    var payLink: String? = null,
): TodayItem(Type.Expired)