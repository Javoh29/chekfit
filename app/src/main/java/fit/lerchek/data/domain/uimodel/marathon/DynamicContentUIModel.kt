package fit.lerchek.data.domain.uimodel.marathon

import android.text.SpannableStringBuilder

class DynamicContentUIModel(val displayItems: List<DynamicContentListItem>) {

    interface DynamicContentListItem

    interface DynamicContentInnerListItem : DynamicContentListItem

    class Header : DynamicContentListItem

    class WhiteBlock(var displayItems: List<DynamicContentInnerListItem>) : DynamicContentListItem

    class Title(var text: String, var isInner:Boolean) : DynamicContentInnerListItem

    class Image(var imageUrl: String) : DynamicContentInnerListItem

    class Video(var videoUrl: String) : DynamicContentInnerListItem

    class SpannedContent(var spannableString: SpannableStringBuilder) : DynamicContentInnerListItem
}
