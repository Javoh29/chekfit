package fit.lerchek.data.domain.uimodel.marathon

class TodaySaleItem(
    val title: String?,
    val description: String?,
    val offers: List<MarathonSaleOfferUIModel>?
): TodayItem(Type.Sale)