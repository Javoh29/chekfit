package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.today.adapters.TodayAdapter

abstract class TodayItem(var type: Type) {

    enum class Type(val itemViewType: Int) {
        Telegram(TodayAdapter.ITEM_VIEW_TYPE_TELEGRAM),
        Vote(TodayAdapter.ITEM_VIEW_TYPE_VOTE),
        Sale(TodayAdapter.ITEM_VIEW_TYPE_SALE),
        Tasks(TodayAdapter.ITEM_VIEW_TYPE_TASKS),
        Referral(TodayAdapter.ITEM_VIEW_TYPE_REF),
        Recipes(TodayAdapter.ITEM_VIEW_TYPE_RECIPES),
        Workouts(TodayAdapter.ITEM_VIEW_TYPE_WORKOUTS),
        Waiting(TodayAdapter.ITEM_VIEW_TYPE_WAITING),
        Expired(TodayAdapter.ITEM_VIEW_TYPE_EXPIRED),
        Error(TodayAdapter.ITEM_VIEW_TYPE_ERROR),
        WaterTracker(TodayAdapter.ITEM_VIEW_TYPE_WATER_TRACKER),
        StepsTracker(TodayAdapter.ITEM_VIEW_TYPE_STEPS_TRACKER),
        SleepTracker(TodayAdapter.ITEM_VIEW_TYPE_SLEEP_TRACKER)
    }

}