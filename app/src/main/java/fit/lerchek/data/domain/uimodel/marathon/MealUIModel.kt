package fit.lerchek.data.domain.uimodel.marathon

class MealUIModel(
    val id: Int,
    val name:String,
    val imageUrl:String,
    var selected:Boolean = false
)