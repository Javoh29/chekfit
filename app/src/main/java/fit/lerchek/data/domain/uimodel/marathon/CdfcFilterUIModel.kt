package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.more.ProductsAdapter

class CdfcFilterUIModel(
    var types: List<MealUIModel>,
) : ProductsAdapter.ProductItem{

    fun getSelectedType(): MealUIModel? {
        return types.firstOrNull { it.selected } ?: types.firstOrNull()
    }

    fun selectType(id: Int?) {
        types.onEach { it.selected = it.id == id }
    }

}