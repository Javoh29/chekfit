package fit.lerchek.data.domain.uimodel.marathon

import java.util.*

sealed class ActivityCalendarItem(val type: Type) {

    enum class Type { WEEKDAY, MONTH, DAY }

    data class Day(
        var id: Int = -1,
        var number: Int = -1,
        var month: Int = -1,
        var today: Boolean = false,
        var enabled: Boolean = false,
        var selected: Boolean = false,
        var completed: Boolean = false,
    ) : ActivityCalendarItem(Type.DAY) {
        fun createDate(): Date {
            return GregorianCalendar.getInstance().apply {
                set(GregorianCalendar.DAY_OF_MONTH, id)
                set(GregorianCalendar.MONTH, month - 1)
            }.time
        }
    }

    class Month(val name: String) : ActivityCalendarItem(Type.MONTH)
    class Weekday(val name: String) : ActivityCalendarItem(Type.WEEKDAY)

}