package fit.lerchek.data.domain.uimodel.marathon

class StepsTrackerItem(
    var dimension: Int,
    var goal: Int,
    var value: Int,
    var date: String,
    var dateFormat: String
) : TrackerItem(Type.StepsTracker)