package fit.lerchek.data.domain.uimodel.marathon

enum class MenuItemType {
    MarathonSelect,
    ReferralProgram,
    AskQuestion,
    TelegramChannel,
    Instruction,
    MarathonReports,
    ActivityCalendar,
    TelegramGroup,
    Profile,
    ExtraWorkout,
    Logout
}