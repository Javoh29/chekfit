package fit.lerchek.data.domain.uimodel.marathon

class TodayWorkoutsItem(
    val workouts: List<WorkoutUIModel>? = arrayListOf()
): TodayItem(Type.Workouts)