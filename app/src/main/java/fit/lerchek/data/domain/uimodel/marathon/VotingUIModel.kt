package fit.lerchek.data.domain.uimodel.marathon

interface VotingItem

class VotingHeaderUIModel(
    var isAgreed: Boolean = false,
    val isActive: Boolean = false,
    val isMale: Boolean = false,
    var maxVotes: Int = 0,
    var votes: Int = 0,
    var voteId: Int = 0,
    var availableVotes: Int = 0
) : VotingItem {
    fun getVotesState() = "$availableVotes / $maxVotes"
}

class VotingItemUIModel(
    val id: Int = 0,
    val title: String = "",
    val startWeight: String = "",
    val endWeight: String = "",
    val weightResult: String = "",
    val images: List<Pair<String, String>> = arrayListOf(),
    var votes: Int = 0,
    var isLiked: Boolean = false,
    val isMale: Boolean = false,
    var selectedImagePosition: Int = 0,
) : VotingItem {
    fun votesString() = "$votes"
}

