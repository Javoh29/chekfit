package fit.lerchek.data.domain.uimodel.marathon

class MarathonDetailsUIModel (
    val isRunning: Boolean = false,
    val bottomNavigationVisible:Boolean = false,
    val todayItems : List<TodayItem> = arrayListOf()
)