package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.food.FoodAdapter

class FoodDetailsUIModel (
    var displayItems: List<FoodAdapter.FoodItem> = arrayListOf(object : FoodAdapter.FoodItem{}),
    var recipes : List<RecipeUIModel> = arrayListOf()
)