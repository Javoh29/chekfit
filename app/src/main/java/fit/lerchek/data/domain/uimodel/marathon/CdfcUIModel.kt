package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.more.ExtraRecipesAdapter
import fit.lerchek.ui.feature.marathon.more.ProductsAdapter

class CdfcUIModel (
    var displayItems: List<ProductsAdapter.ProductItem> = arrayListOf(),
    var products : List<ProductUIModel> = arrayListOf()
)