package fit.lerchek.data.domain.uimodel.marathon

class ActivityCalendarUIModel(
    val isMale: Boolean = true,
    val profileName: String = "",
    val currentDay: Int = 0,
    val currentDate: String = "",
    val currentMarathonDay: Int = 0,
    val calendar: List<ActivityCalendarItem> = listOf(),
    val tasks: Map<String, List<TaskUIModel>> = mapOf()
)