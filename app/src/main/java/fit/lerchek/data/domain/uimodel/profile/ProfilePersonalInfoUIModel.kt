package fit.lerchek.data.domain.uimodel.profile

import androidx.databinding.ObservableField
import fit.lerchek.R
import fit.lerchek.common.util.toDate
import fit.lerchek.common.util.toString
import fit.lerchek.data.api.message.marathon.UpdateProfileRequest
import fit.lerchek.data.api.model.Params
import java.lang.Exception


class ProfilePersonalInfoUIModel constructor(
    var email: ObservableField<String> = ObservableField(),
    var name: ObservableField<String> = ObservableField(),
    var lastName: ObservableField<String> = ObservableField(),
    var middleName: ObservableField<String> = ObservableField(),
    var phone: ObservableField<String> = ObservableField(),
    var city: ObservableField<String> = ObservableField(),
    var instagram: ObservableField<String> = ObservableField(),
    var telegram: ObservableField<String> = ObservableField(),
    var dateOfBirth: ObservableField<String> = ObservableField(),
    var childrenCount: ObservableField<String> = ObservableField(),
    var familyStatus: ObservableField<Int> = ObservableField()
) {

    var familyStatusParams: Params? = null

    fun prepareRequestForUpdate(): UpdateProfileRequest {
        var date : String? = null
        try {
            date = dateOfBirth.get()?.toDate("dd/MM/yyyy")?.toString("yyyy-MM-dd")
        }catch (e : Exception){
            e.printStackTrace()
        }

        return UpdateProfileRequest(
            last_name = lastName.get(),
            first_name = name.get(),
            middle_name = middleName.get(),
            phone = phone.get(),
            birth_date = date,
            city = city.get(),
            children_count = childrenCount.get(),
            instagram = instagram.get(),
            telegram = telegram.get(),
            family_status = familyStatus.get()?.toString()
        )
    }


    fun firstStateForFamily(): String {
        val value = familyStatusParams?.family_status?.values?.getOrNull(0)
        return value?.value ?: value?.description ?: ""
    }

    fun secondStateForFamily(): String {
        val value = familyStatusParams?.family_status?.values?.getOrNull(1)
        return value?.value ?: value?.description ?: ""
    }

    fun firstStateId(): Int {
        return familyStatusParams?.family_status?.values?.getOrNull(0)?.id ?: 1
    }

    fun secondStateId(): Int {
        return familyStatusParams?.family_status?.values?.getOrNull(1)?.id ?: 1
    }

    fun checkedButton(): Int {
        return if (familyStatusParams?.family_status?.values?.getOrNull(0)?.id == familyStatus.get()) R.id.first else R.id.second
    }
}