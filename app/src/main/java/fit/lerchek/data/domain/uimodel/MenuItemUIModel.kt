package fit.lerchek.data.domain.uimodel

import fit.lerchek.data.domain.uimodel.marathon.MenuItemType

class MenuItemUIModel(
    var type: MenuItemType,
    var link: String? = null,
    var workoutName: String? = null,
    var extraMarathonId: Int? = null
)