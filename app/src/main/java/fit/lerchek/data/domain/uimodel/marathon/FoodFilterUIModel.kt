package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.food.FoodAdapter

class FoodFilterUIModel(
    weeks: List<WeekUIModel>,
    var plans: List<PlanUIModel> = arrayListOf()
) : FilterUIModel(weeks), FoodAdapter.FoodItem {

    fun getNutritionPlan(): PlanUIModel? {
        return plans.firstOrNull { it.selected } ?: plans.firstOrNull()
    }

    fun selectNutritionPlan(id: Int?) {
        plans.onEach { it.selected = it.id == id }
    }

}