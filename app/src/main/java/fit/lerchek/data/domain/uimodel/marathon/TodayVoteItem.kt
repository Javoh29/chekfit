package fit.lerchek.data.domain.uimodel.marathon

class TodayVoteItem(
    var name: String? = null,
    var id: Int? = null,
    var stage: Int? = null
): TodayItem(Type.Vote)