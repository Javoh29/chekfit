package fit.lerchek.data.domain.uimodel.marathon

class FoodWeekUIModel(
    val id: Int,
    val name:String,
    var selected: Boolean,
    var days: List<FoodDayUIModel> = arrayListOf()
)