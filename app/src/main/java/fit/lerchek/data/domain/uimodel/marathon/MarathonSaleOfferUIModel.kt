package fit.lerchek.data.domain.uimodel.marathon

class MarathonSaleOfferUIModel(
    val title: String?,
    val price: String?,
    val link: String?,
    val buttonText: String?
)