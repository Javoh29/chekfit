package fit.lerchek.data.domain.uimodel.marathon

class PlanUIModel(
    val id: Int,
    val name: String,
    val description: String,
    val imageUrl: String,
    var selected: Boolean
)