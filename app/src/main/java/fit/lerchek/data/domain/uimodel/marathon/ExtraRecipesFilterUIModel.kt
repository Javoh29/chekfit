package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.more.ExtraRecipesAdapter
import fit.lerchek.ui.feature.marathon.more.ProductsAdapter

class ExtraRecipesFilterUIModel(
    var meals: List<MealUIModel>,
) : ExtraRecipesAdapter.ExtraRecipeItem{

    fun getSelectedMeal(): MealUIModel? {
        return meals.firstOrNull { it.selected } ?: meals.firstOrNull()
    }

    fun selectMeal(id: Int?) {
        meals.onEach { it.selected = it.id == id }
    }

}