package fit.lerchek.data.domain.uimodel.profile

class ProfileDetailsUIModel constructor(
    var isMale: Boolean,
    var name: String,
    var nick: String,
    var marathonsCount: Int = 0,
    var activeDays: Int? = 0,
    var prizes: Int? = 0
) {
    fun getMarathonCountString() = marathonsCount.toString()
    fun getActiveDaysString() = activeDays.toString()
    fun getPrizesString() = prizes.toString()
}