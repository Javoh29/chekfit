package fit.lerchek.data.domain.uimodel.chat

interface ChatItem

class ChatMessageUIModel(
    val isMale: Boolean,
    val dateTime: String,
    val isOperator: Boolean,
    val name: String,
    val text: String
):ChatItem

