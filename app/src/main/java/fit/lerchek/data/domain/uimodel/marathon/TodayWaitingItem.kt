package fit.lerchek.data.domain.uimodel.marathon

class TodayWaitingItem(
    var imageUrl: String? = null,
    var start: String? = null,
): TodayItem(Type.Waiting)