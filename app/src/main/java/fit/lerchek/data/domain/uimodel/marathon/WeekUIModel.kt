package fit.lerchek.data.domain.uimodel.marathon

class WeekUIModel(
    val id: Int,
    val name:String,
    var selected: Boolean,
    var days: List<DayUIModel> = arrayListOf(),
    var dates:String? = null
)