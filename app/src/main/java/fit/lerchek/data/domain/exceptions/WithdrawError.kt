package fit.lerchek.data.domain.exceptions

class WithdrawError : Throwable() {
    var code: Int = 0
    override var message: String = ""
    var phoneError: String? = null
    var amountError: String? = null
}