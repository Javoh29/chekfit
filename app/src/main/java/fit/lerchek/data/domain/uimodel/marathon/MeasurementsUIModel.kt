package fit.lerchek.data.domain.uimodel.marathon

import android.content.Context
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import fit.lerchek.R
import fit.lerchek.ui.util.TextUtils.convertWithOneDecimal
import kotlin.math.abs

class MeasurementsUIModel(val type: Type, val progress: MarathonProgressUIModel) {

    enum class Type(
        @StringRes val titleRes: Int,
        @StringRes val unitRes: Int,
        @StringRes val yourRes: Int,
        @StringRes val increasedRes: Int,
        @StringRes val decreasedRes: Int,
        @DrawableRes val progressRes: Int
    ) {
        WEIGHT(
            R.string.weight,
            R.string.kilograms,
            R.string.progress_your_weight,
            R.string.progress_increased_weight,
            R.string.progress_decreased_weight,
            R.drawable.bg_progress_weight
        ),
        WAIST(
            R.string.waist,
            R.string.centimeters,
            R.string.progress_your_waist_breast,
            R.string.progress_increased_waist_breast,
            R.string.progress_decreased_waist_breast,
            R.drawable.bg_progress_waist
        ),
        BREAST(
            R.string.breast,
            R.string.centimeters,
            R.string.progress_your_waist_breast,
            R.string.progress_increased_waist_breast,
            R.string.progress_decreased_waist_breast,
            R.drawable.bg_progress_breast
        ),
        HIPS(
            R.string.hips,
            R.string.centimeters,
            R.string.progress_your_hips,
            R.string.progress_increased_hips,
            R.string.progress_decreased_hips,
            R.drawable.bg_progress_hips
        );
    }

    fun unit(context: Context, value: String): String {
        return context.getString(type.unitRes, value)
    }

    fun showFire(value: Float?): Boolean {
        if (value == null) return false
        return value < 0F
    }

    fun unitDiff(context: Context, value: Float?): String {
        if (value == null) return ""
        val level = if (value > 0F) type.increasedRes else type.decreasedRes
        return context.getString(type.yourRes).plus(" ")
            .plus(context.getString(type.titleRes).lowercase()).plus(" ")
            .plus(context.getString(level)).plus(" ")
            .plus(unit(context, abs(value).convertWithOneDecimal())).plus(".")
    }
}