package fit.lerchek.data.domain.uimodel.marathon

class TodayRecipesItem(
    val recipes: List<RecipeUIModel>? = arrayListOf()
): TodayItem(Type.Recipes)