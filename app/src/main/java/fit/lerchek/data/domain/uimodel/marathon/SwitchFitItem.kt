package fit.lerchek.data.domain.uimodel.marathon

class SwitchFitItem(
    val typeTracker: Int
) : TrackerItem(Type.SwitchFit)