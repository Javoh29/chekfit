package fit.lerchek.data.domain.uimodel.marathon

class TodayStepsItem(
    val dimension: Int,
    val goal: Int,
    val value: Int,
    val date: String
) : TodayItem(Type.StepsTracker)