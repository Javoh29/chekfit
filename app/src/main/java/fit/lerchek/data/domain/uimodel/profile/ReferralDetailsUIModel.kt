package fit.lerchek.data.domain.uimodel.profile

class ReferralDetailsUIModel(
    var link: String,
    var balance: String,
    var transactions: List<TransactionUIModel>,
)