package fit.lerchek.data.domain.converter

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import fit.lerchek.R
import fit.lerchek.common.util.Constants
import fit.lerchek.common.util.DateFormatUtils
import fit.lerchek.common.util.toString
import fit.lerchek.data.api.message.forgotpwd.ForgotPwdResponse
import fit.lerchek.data.api.message.login.LoginResponse
import fit.lerchek.data.api.message.marathon.*
import fit.lerchek.data.api.model.*
import fit.lerchek.data.domain.exceptions.CommonException
import fit.lerchek.data.domain.exceptions.LoginError
import fit.lerchek.data.domain.uimodel.MenuItemUIModel
import fit.lerchek.data.domain.uimodel.chat.ChatItem
import fit.lerchek.data.domain.uimodel.chat.ChatMessageUIModel
import fit.lerchek.data.domain.uimodel.chat.ChatUIModel
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.domain.uimodel.profile.*
import fit.lerchek.data.managers.DataManager
import fit.lerchek.ui.feature.marathon.FaqAdapter
import fit.lerchek.ui.feature.marathon.food.FoodAdapter
import fit.lerchek.ui.feature.marathon.food.RecipeReplacementAdapter
import fit.lerchek.ui.feature.marathon.more.ExtraRecipesAdapter
import fit.lerchek.ui.feature.marathon.more.ProductsAdapter
import fit.lerchek.ui.feature.marathon.workout.WorkoutAdapter
import fit.lerchek.ui.widget.linechart.BarData
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList

class UserDataConverter {

    fun processLoginResponseAndGetLogin(response: LoginResponse): String? {
        if (response.success) {
            return response.result?.token
        } else {
            throw LoginError().apply {
                code = response.code ?: 0
                message = response.message ?: ""
                passwordError = try {
                    response.errors?.asJsonObject?.get("password")?.asJsonArray?.get(0)?.asString
                        ?: ""
                } catch (_: RuntimeException) {
                    null
                }
                emailError = try {
                    response.errors?.asJsonObject?.get("email")?.asJsonArray?.get(0)?.asString ?: ""
                } catch (_: RuntimeException) {
                    null
                }
            }
        }
    }

    fun convertMarathonsToUIModel(
        marathons: List<Marathon>?,
        dataManager: DataManager
    ): List<MarathonUIModel> {
        return marathons?.map {
            val imageUrl = convertImageToUIModel(it.image, false)
            MarathonUIModel(
                id = it.id,
                name = it.name,
                status = it.status,
                startDate = if (it.start != null) DateFormatUtils.formatDateDayFullMonth(
                    DateFormatUtils.parseServerDate(
                        it.start
                    )
                ) else "",
                endDate = if (it.end != null) DateFormatUtils.formatDateDayFullMonth(
                    DateFormatUtils.parseServerDate(
                        it.end
                    )
                ) else "",
                imageUrl = imageUrl
            )
        } ?: arrayListOf()
    }

    fun generateMenuItemsByUserDataCaches(appMenu: AppMenu?): List<MenuItemUIModel> {
        val menuItemsList = arrayListOf<MenuItemUIModel>()
        menuItemsList.add(MenuItemUIModel(type = MenuItemType.MarathonSelect))
        menuItemsList.add(MenuItemUIModel(type = MenuItemType.ReferralProgram))
        if (appMenu?.qa == true) {
            menuItemsList.add(MenuItemUIModel(type = MenuItemType.AskQuestion))
        }
        if (!appMenu?.telegram_channel.isNullOrEmpty()) {
            menuItemsList.add(
                MenuItemUIModel(
                    type = MenuItemType.TelegramChannel,
                    link = appMenu?.telegram_channel
                )
            )
        }
        if (appMenu?.instruction == true) {
            menuItemsList.add(MenuItemUIModel(type = MenuItemType.Instruction))
        }
        if (appMenu?.report == true) {
            menuItemsList.add(MenuItemUIModel(type = MenuItemType.MarathonReports))
        }
        if (appMenu?.show_calendar_button == true) {
            menuItemsList.add(MenuItemUIModel(type = MenuItemType.ActivityCalendar))
        }
        if (!appMenu?.telegram_group.isNullOrEmpty()) {
            menuItemsList.add(
                MenuItemUIModel(
                    type = MenuItemType.TelegramGroup,
                    link = appMenu?.telegram_group
                )
            )
        }
        menuItemsList.add(MenuItemUIModel(type = MenuItemType.Profile))
        appMenu?.extra_workout?.forEach { extraWorkout ->
            menuItemsList.add(
                MenuItemUIModel(
                    type = MenuItemType.ExtraWorkout,
                    workoutName = extraWorkout.name,
                    extraMarathonId = extraWorkout.marathon_id
                )
            )
        }
        menuItemsList.add(MenuItemUIModel(type = MenuItemType.Logout))
        return menuItemsList
    }

    fun convertProfileDetailsResponseToUIModel(profileDetailsResponse: ProfileDetailsResponse): ProfileDetailsUIModel {
        profileDetailsResponse.result!!.let { profileDetails ->
            return ProfileDetailsUIModel(
                isMale = profileDetails.customer?.profile?.sex?.isMale() == true,
                name = profileDetails.customer?.profile?.first_name + " " + profileDetails.customer?.profile?.last_name,
                nick = profileDetails.customer?.email ?: "",
                marathonsCount = profileDetails.marathon_count ?: 0,
                activeDays = profileDetails.marathon_days ?: 0,
                prizes = profileDetails.prizes ?: 0
            )

        }
    }

    fun convertFaqToUIModel(faqResponse: FaqResponse): List<FaqAdapter.FaqListItem> {
        val items = arrayListOf<FaqAdapter.FaqListItem>()
        val title = faqResponse.result?.title ?: ""
        items.add(FaqAdapter.FaqHeader(title))
        items.addAll(faqResponse.result?.list?.map { item ->
            FaqItemUIModel(
                question = item.question,
                answer = item.answer
            )
        } ?: arrayListOf())
        return items
    }

    fun convertPaPageToUIModel(paPage: PaPage): DynamicContentUIModel {
        val result =
            arrayListOf<DynamicContentUIModel.DynamicContentListItem>(DynamicContentUIModel.Header())
        paPage.body_data?.forEach { item ->
            when {
                item.isWhiteBlock() && item.children.isNullOrEmpty().not() ->
                    DynamicContentUIModel.WhiteBlock(convertDynamicItemsToUIModel(item.children!!))
                item.isVideo() && item.href.isNullOrEmpty().not() ->
                    DynamicContentUIModel.Video(item.href!!.split("/").last())
                item.isImage() && !item.src2x.isNullOrEmpty() ->
                    DynamicContentUIModel.Image(item.src2x)
                item.isTitle() && !item.font?.text.isNullOrEmpty() ->
                    DynamicContentUIModel.Title(item.font!!.text!!, false)
                else -> null
            }?.let {
                result.add(it)
            }
        }
        return DynamicContentUIModel(result)
    }

    private fun convertDynamicItemsToUIModel(
        items: List<DynamicContentItem>
    ): List<DynamicContentUIModel.DynamicContentInnerListItem> {
        arrayListOf<DynamicContentUIModel.DynamicContentInnerListItem>().apply {
            items.forEach { item ->
                when {
                    item.isVideo() && item.href.isNullOrEmpty().not() ->
                        DynamicContentUIModel.Video(item.href!!.split("/").last())
                    item.isImage() && !item.src2x.isNullOrEmpty() ->
                        DynamicContentUIModel.Image(item.src2x)
                    (item.isTitle() || item.isTitle2()) && !item.font?.text.isNullOrEmpty() ->
                        DynamicContentUIModel.Title(item.font!!.text!!, true)
                    item.isParagraph() || item.isList() -> DynamicContentUIModel.SpannedContent(
                        buildSpannableStringFromDescriptionData(
                            arrayListOf(item)
                        ) ?: SpannableStringBuilder()
                    )
                    else -> null
                }?.let {
                    add(it)
                }
            }
            return this
        }
    }

    fun generateNutritionPlanUpdateApiRequest(nutritionPlanId: Int?): UpdateNutritionPlanRequest {
        return UpdateNutritionPlanRequest(nutrition_plan_id = nutritionPlanId?.toString() ?: "0")
    }

    fun generateWorkoutLevelUpdateApiRequest(workoutLevelId: Int?): UpdateWorkoutLevelRequest {
        return UpdateWorkoutLevelRequest(workout_plan_id = workoutLevelId?.toString() ?: "0")
    }

    fun convertFoodDetailsToUIModel(
        foodDetails: FoodDetails,
        selectedWeekId: Int?,
        selectedPlanId: Int?,
        selectedDay: Int?
    ): FoodDetailsUIModel {
        val recipes = foodDetails.recipes.map { convertRecipeToUIModel(it) }
        val displayItems = arrayListOf<FoodAdapter.FoodItem>()
        displayItems.add(FoodFilterUIModel(
            weeks = convertWeeksToUIModels(
                weeks = foodDetails.weeks,
                currentWeek = selectedWeekId ?: foodDetails.current_week,
                currentDay = foodDetails.current_day
            ),
            plans = foodDetails.available_plans.map {
                convertPlanToUIModel(
                    plan = it,
                    selectedPlanId = selectedPlanId,
                    apiPlanId = foodDetails.nutrition_plan_id
                )
            }
        ))
        recipes.filter {
            it.dayNumber == selectedDay ?: foodDetails.current_day
        }.let { currentRecipes ->
            if (currentRecipes.isEmpty()) {
                displayItems.add(object : FoodAdapter.FoodItem {})
            } else {
                displayItems.addAll(currentRecipes)
            }
        }
        return FoodDetailsUIModel(
            displayItems = displayItems,
            recipes = recipes
        )
    }

    fun convertWorkoutDetailsToUIModel(
        workoutDetails: WorkoutDetails,
        selectedWeekId: Int?,
        selectedLevelId: Int?,
        selectedDay: Int?,
        isMale: Boolean
    ): WorkoutDetailsUIModel {
        val workouts = workoutDetails.workouts?.map { convertWorkoutToUIModel(it) }
        val displayItems = arrayListOf<WorkoutAdapter.WorkoutItem>()
        displayItems.add(
            WorkoutFilterUIModel(
                weeks = convertWeeksToUIModels(
                    weeks = workoutDetails.weeks,
                    currentWeek = selectedWeekId ?: workoutDetails.current_week,
                    currentDay = workoutDetails.current_day
                ),
                levels = workoutDetails.available_plans.map {
                    convertPlanToUIModel(
                        plan = it,
                        selectedPlanId = selectedLevelId,
                        apiPlanId = workoutDetails.workout_plan_id
                    )
                },
                isMale = isMale
            )
        )
        workouts?.filter {
            it.dayNumber == selectedDay ?: workoutDetails.current_day
        }.let { currentWorkouts ->
            if (currentWorkouts.isNullOrEmpty()) {
                displayItems.add(object : WorkoutAdapter.WorkoutItem {})
            } else {
                displayItems.addAll(currentWorkouts)
            }
        }
        return WorkoutDetailsUIModel(
            displayItems = displayItems,
            workouts = workouts ?: arrayListOf()
        )
    }

    fun updateFoodFilterUIModelWithSelectedValues(
        foodDetailsUIModel: FoodDetailsUIModel?,
        selectedWeekId: Int?,
        selectedPlanId: Int?,
        selectedDay: Int?
    ) = foodDetailsUIModel?.apply {

        displayItems.firstOrNull { it is FoodFilterUIModel }?.apply {
            (this as FoodFilterUIModel).let { filterModel ->
                filterModel.selectWeek(selectedWeekId)
                filterModel.selectDay(selectedDay)
                filterModel.selectNutritionPlan(selectedPlanId)
            }
        }
        displayItems = displayItems.subList(0, 1)
        recipes.filter {
            it.dayNumber == selectedDay
        }.let { currentRecipes ->
            if (currentRecipes.isEmpty()) {
                displayItems += object : FoodAdapter.FoodItem {}
            } else {
                displayItems += (currentRecipes)
            }
        }
    } ?: FoodDetailsUIModel()

    fun updateWorkoutFilterUIModelWithSelectedValues(
        workoutDetailsUIModel: WorkoutDetailsUIModel?,
        selectedWeekId: Int?,
        selectedLevelId: Int?,
        selectedDay: Int?
    ) = workoutDetailsUIModel?.apply {

        displayItems.firstOrNull { it is WorkoutFilterUIModel }?.apply {
            (this as WorkoutFilterUIModel).let { filterModel ->
                filterModel.selectWeek(selectedWeekId)
                filterModel.selectDay(selectedDay)
                filterModel.selectLevel(selectedLevelId)
            }
        }
        displayItems = displayItems.subList(0, 1)
        workouts.filter {
            it.dayNumber == selectedDay
        }.let { currentWorkouts ->
            if (currentWorkouts.isEmpty()) {
                displayItems += object : WorkoutAdapter.WorkoutItem {}
            } else {
                displayItems += (currentWorkouts)
            }
        }
    } ?: WorkoutDetailsUIModel()


    fun convertRequestPwdCode(response: ForgotPwdResponse): Boolean {
        if (response.success) {
            return response.success
        } else {
            throw LoginError().apply {
                code = response.code ?: 0
                message = response.message ?: ""
                emailError = try {
                    response.errors?.asJsonObject?.get("email")?.asJsonArray?.get(0)?.asString ?: ""
                } catch (_: RuntimeException) {
                    null
                }
            }

        }
    }

    fun getCalendarTasks(
        calendarTasks: Map<String, List<TodayTask>>,
        completedTasks: Map<String, List<CompletedTask>>
    ): Map<String, List<TaskUIModel>> {
        return hashMapOf<String, List<TaskUIModel>>().apply {
            calendarTasks.keys.forEach { day ->
                calendarTasks[day]?.let { tasks ->
                    val dayCompletedTaskIds = completedTasks[day]?.mapNotNull {
                        it.task_id
                    } ?: listOf()
                    put(day, tasks.map { task ->
                        getTask(task, task.id in dayCompletedTaskIds)
                    })
                }
            }
        }
    }

    fun getActivityCalendarItems(
        weekdays: Array<String>,
        calendar: Map<String, ActivityCalendarMonth>,
        tasks: Map<String, List<TaskUIModel>>,
        currentDay: Int?,
    ): List<ActivityCalendarItem> {
        var dayNumber = 0
        if (calendar.isNotEmpty()) {
            val calendarItems = mutableListOf<ActivityCalendarItem>()
            weekdays.forEach { name ->
                ActivityCalendarItem.Weekday(name).also { calendarItems.add(it) }
            }
            calendar.keys.forEach { monthIdx ->
                calendar.getValue(monthIdx).also { month ->
                    if (!month.days.isNullOrEmpty()) {
                        calendarItems.add(ActivityCalendarItem.Month(month.name ?: ""))
                        month.days.keys.forEach { weekIdx ->
                            month.days.getValue(weekIdx).forEach { day ->
                                calendarItems.add(ActivityCalendarItem.Day().also { calendarDay ->
                                    day?.let {
                                        dayNumber++
                                        val dayTasks = tasks[dayNumber.toString()] ?: listOf()
                                        val completed = dayTasks.all { task -> task.checked }
                                        val enabled =
                                            dayTasks.isNotEmpty() && dayNumber <= currentDay ?: 0
                                        calendarDay.apply {
                                            this.id = day
                                            this.number = dayNumber
                                            this.enabled = enabled
                                            this.month = monthIdx.toInt()
                                            this.today = currentDay == dayNumber
                                            this.selected = currentDay == dayNumber
                                            this.completed = completed && enabled
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
            return calendarItems
        }
        return emptyList()
    }

    private fun getTask(task: TodayTask, complete: Boolean) =
        TaskUIModel(
            id = task.id,
            checked = complete,
            title = task.name ?: "",
            comment = task.description ?: ""
        )

    fun getDetailInfoTrackerConvertToList(
        type: String,
        period: String,
        list: List<DetailInfoTracker>
    ): List<TrackerItem> {
        val trackerItems = arrayListOf<TrackerItem>()
        var barList = arrayListOf<BarData>()
        list.forEach {
            barList.add(
                BarData(
                    it.dateFormat.substring(0, 3).trim(),
                    it.value,
                    it.date,
                    it.dateFormat
                )
            )
        }
        barList = barList.reversed() as ArrayList<BarData>
        val firstItem = list.first()
        when (type) {
            "water" -> {
                trackerItems.add(
                    LineTrackerItem(
                        title = R.string.text_water,
                        goal = firstItem.goal,
                        dimension = firstItem.dimension,
                        listData = barList,
                        progressBarDrawable = R.drawable.water_progress_bar_shape,
                        progressBarDrawable2 = R.drawable.water_progress_bar_shape2
                    )
                )
                if (period != "year")
                    trackerItems.add(
                        WaterTrackerItem(
                            firstItem.dimension,
                            firstItem.goal,
                            firstItem.value,
                            firstItem.date
                        )
                    )
                trackerItems.add(
                    SwitchFitItem(1)
                )
                trackerItems.add(
                    SettingTrackerItem(
                        "water_set",
                        firstItem.goal,
                        firstItem.dimension
                    )
                )
            }
            "step" -> {
                trackerItems.add(
                    LineTrackerItem(
                        title = R.string.text_steps,
                        goal = firstItem.goal,
                        dimension = firstItem.dimension,
                        listData = barList,
                        progressBarDrawable = R.drawable.steps_progress_bar_shape,
                        progressBarDrawable2 = R.drawable.steps_progress_bar_shape2
                    )
                )
                if (period != "year") {
                    trackerItems.add(
                        StepsTrackerItem(
                            firstItem.dimension,
                            firstItem.goal,
                            firstItem.value,
                            firstItem.date,
                            firstItem.dateFormat
                        )
                    )
                    trackerItems.add(PurpleBtnItem("steps_add", firstItem.date))
                }
                trackerItems.add(
                    SwitchFitItem(2)
                )
                trackerItems.add(
                    SettingTrackerItem(
                        "steps_set",
                        firstItem.goal,
                        firstItem.dimension
                    )
                )
            }
            "dream" -> {
                trackerItems.add(
                    LineTrackerItem(
                        title = R.string.text_sleep,
                        goal = firstItem.goal,
                        dimension = firstItem.dimension,
                        listData = barList,
                        progressBarDrawable = R.drawable.sleep_progress_bar_shape,
                        progressBarDrawable2 = R.drawable.sleep_progress_bar_shape2
                    )
                )
                if (period != "year") {
                    trackerItems.add(
                        SleepTrackerItem(
                            firstItem.dimension,
                            firstItem.goal,
                            firstItem.value,
                            firstItem.date
                        )
                    )
                    trackerItems.add(PurpleBtnItem("sleep_add", firstItem.date))
                }
                trackerItems.add(
                    SwitchFitItem(3)
                )
                trackerItems.add(
                    SettingTrackerItem(
                        "sleep_set",
                        firstItem.goal,
                        firstItem.dimension
                    )
                )
            }
        }
        return trackerItems
    }

    fun getTodayItemsForMarathon(
        profileInfo: Profile?,
        marathonDetails: MarathonDetailsResponse?,
        checkVoteResult: MarathonVoteResponse?,
        trackers: TodayTrackers?
    ): List<TodayItem> {
        val todayItems = arrayListOf<TodayItem>()

        val telegramChannel = marathonDetails?.result?.telegram_channel
        val telegramGroup = marathonDetails?.result?.telegram_group
        if (!(telegramChannel.isNullOrEmpty() && telegramGroup.isNullOrEmpty())) {
            todayItems.add(
                TodayTelegramItem(
                    channel = telegramChannel,
                    group = telegramGroup
                )
            )
        }

        val isExpired = marathonDetails?.string_code == "marathon_access_expired"
        val isWaiting = marathonDetails?.string_code == "marathon_not_started"

        if (isExpired || isWaiting) {
            todayItems.add(TodayErrorItem(isExpired = isExpired, isWaiting = isWaiting))
        } else if (marathonDetails?.success != true) {
            throw CommonException().apply {
                message = marathonDetails?.getErrorContent() ?: Constants.UNKNOWN_ERROR
            }
        }

        checkVoteResult?.let { voteResponse ->
            if (voteResponse.success) {
                voteResponse.result?.let { vote ->
                    todayItems.add(
                        TodayVoteItem(
                            name = vote.name,
                            id = vote.id,
                            stage = vote.stage
                        )
                    )
                }
            }
        }

        if (isExpired || isWaiting) {
            val result = try {
                marathonDetails?.result ?: Gson().fromJson(
                    marathonDetails?.errors,
                    MarathonDetails::class.java
                )
            } catch (_: Throwable) {
                null
            }
            result?.let { marathon ->
                when {
                    isExpired -> todayItems.add(
                        TodayExpiredItem(
                            renewalPrice = marathon.renewal_price,
                            payLink = marathon.pay_link,
                            imageUrl = convertImageToUIModel(marathon.image, true)
                        )
                    )
                    isWaiting -> todayItems.add(
                        TodayWaitingItem(
                            start = if (marathon.start != null)
                                DateFormatUtils.formatDateDayFullMonth(
                                    DateFormatUtils.parseServerDate(
                                        marathon.start
                                    )
                                ) else "",
                            imageUrl = convertImageToUIModel(marathon.image, true)
                        )
                    )
                    else -> {
                    }
                }
            }
        }

        marathonDetails?.result?.sale?.let { sale ->
            todayItems.add(TodaySaleItem(
                title = sale.title,
                description = sale.description,
                offers = sale.offers?.map { saleOffer ->
                    MarathonSaleOfferUIModel(
                        title = saleOffer.title,
                        price = saleOffer.price,
                        link = saleOffer.link,
                        buttonText = saleOffer.button_text
                    )
                }
            ))
        }
        marathonDetails?.result?.tasks?.let { taskItems ->
            if (taskItems.isNotEmpty()) {
                val completedTask = marathonDetails.result!!.completed_tasks?.toSet() ?: setOf()
                todayItems.add(
                    TodayTasksItem(
                        marathonName = marathonDetails.result!!.marathon_name,
                        isMale = profileInfo?.sex?.isMale() == true,
                        profileName = profileInfo?.first_name ?: "",
                        currentDay = marathonDetails.result!!.current_day ?: 0,
                        currentMarathonDay = marathonDetails.result!!.current_marathon_day ?: 0,
                        tasks = taskItems.map { task ->
                            getTask(task, task.id in completedTask)
                        },
                        activityCalendarVisible = marathonDetails.result?.app_menu?.show_calendar_button == true
                    )
                )
            }
        }

        trackers?.trackers?.water?.let {
            todayItems.add(
                TodayWaterItem(
                    it.dimension ?: 0,
                    it.goal ?: 0,
                    it.value ?: 0
                )
            )
        }

        trackers?.trackers?.step?.let {
            todayItems.add(
                TodayStepsItem(
                    it.dimension ?: 0,
                    it.goal ?: 0,
                    it.value ?: 0,
                    trackers.date ?: ""
                )
            )
        }

        trackers?.trackers?.dream?.let {
            todayItems.add(
                TodaySleepItem(
                    it.dimension ?: 0,
                    it.goal ?: 0,
                    it.value ?: 0
                )
            )
        }

        marathonDetails?.result?.ref?.let { link ->
            (if (link.startsWith(Constants.WEB_HOME)) link else Constants.WEB_HOME.dropLast(1) + link).let {
                todayItems.add(TodayRefItem(it))
            }
        }

        marathonDetails?.result?.recipes?.let { recipes ->
            if (recipes.isNotEmpty()) {
                todayItems.add(TodayRecipesItem(
                    recipes = recipes.map {
                        convertRecipeToUIModel(
                            recipe = it,
                            defaultDayNumber = marathonDetails.result!!.current_day
                        )
                    }
                ))
            }
        }

        marathonDetails?.result?.workouts?.let { workouts ->
            if (workouts.isNotEmpty()) {
                todayItems.add(TodayWorkoutsItem(
                    workouts = workouts.map {
                        convertWorkoutToUIModel(it)
                    }
                ))
            }
        }

        return todayItems
    }

    private fun convertPlanToUIModel(
        plan: Plan,
        selectedPlanId: Int?,
        apiPlanId: Int?
    ): PlanUIModel {
        return PlanUIModel(
            id = plan.id,
            name = plan.name,
            description = plan.description,
            imageUrl = convertImageToUIModel(plan.image, true),
            selected = plan.id == selectedPlanId ?: apiPlanId
        )
    }

    fun convertRecipeToUIModel(
        recipe: TodayRecipe,
        replacementAllowed: Boolean = true,
        defaultDayNumber: Int? = null,
        baseId: Int? = null
    ): RecipeUIModel {
        return RecipeUIModel(
            id = recipe.id,
            baseId = recipe.base_recipe_id ?: baseId,
            name = recipe.name,
            calories = recipe.calories.toInt().toString(),
            mealName = recipe.meal_name ?: "",
            mealId = recipe.meal_id ?: -1,
            imageUrl = convertImageToUIModel(recipe.image, true),
            hasReplacements = replacementAllowed && recipe.has_replacements == true,
            showWithoutOven = recipe.wo_oven == 1,
            dayNumber = recipe.day_number ?: defaultDayNumber,
            planId = recipe.nutrition_plan_id,
            proteins = recipe.proteins.toString(),
            fats = recipe.fats.toString(),
            carbohydrates = recipe.carbohydrates.toString(),
            isFavorite = recipe.is_favorite ?: false,
            descriptionSpan = buildSpannableStringFromDescriptionData(
                descriptionData = recipe.description_data
            ) ?: SpannableStringBuilder(recipe.description ?: ""),
            ingredients = convertRecipeIngredientsToUIModel(
                recipe.ingredients,
                recipe.recipe_groups
            ),
            mealIds = recipe.meals?.map { it.id } ?: arrayListOf()

        )
    }

    fun convertFavRecipeToUIModel(
        recipe: TodayRecipe
    ): RecipeUIModel {
        return RecipeUIModel(
            id = recipe.id,
            baseId = recipe.base_recipe_id ?: 0,
            name = recipe.name,
            calories = recipe.calories.toInt().toString(),
            mealName = recipe.meals!!.firstNotNullOf { it.name },
            mealId = recipe.meals!!.firstNotNullOf { it.id },
            imageUrl = convertImageToUIModel(recipe.image, true),
            hasReplacements = false,
            showWithoutOven = recipe.wo_oven == 1,
            planId = recipe.nutrition_plan_id,
            proteins = recipe.proteins.toString(),
            fats = recipe.fats.toString(),
            carbohydrates = recipe.carbohydrates.toString(),
            isFavorite = recipe.is_favorite ?: false,
            descriptionSpan = buildSpannableStringFromDescriptionData(
                descriptionData = recipe.description_data
            ) ?: SpannableStringBuilder(recipe.description ?: ""),
            ingredients = convertRecipeIngredientsToUIModel(
                recipe.ingredients,
                recipe.recipe_groups
            ),
            mealIds = recipe.meals?.map { it.id } ?: arrayListOf()

        )
    }

    fun convertRecipeReplacementsToUIModel(
        recipe: TodayRecipe,
        replacements: List<TodayRecipe>
    ): List<RecipeReplacementAdapter.ReplacementItem> {
        return arrayListOf<RecipeReplacementAdapter.ReplacementItem>().apply {
            add(
                RecipeReplacementFilterUIModel(
                    imageUrl = convertImageToUIModel(recipe.image, true),
                    name = recipe.name
                )
            )
            addAll(replacements.map { convertRecipeToUIModel(it) })
        }
    }

    fun convertExtraRecipesToUIModel(
        cache: ExtraRecipesUIModel? = null,
        filter: ExtraRecipesFilterUIModel? = null,
        recipes: List<TodayRecipe>? = null,
        meals: List<Meal>? = null
    ): ExtraRecipesUIModel {
        var selectedMeal = filter?.getSelectedMeal()
        val allMeals = (filter?.meals ?: meals?.map {
            convertMealToUIModel(it)
        })?.onEach { meal ->
            meal.selected = selectedMeal?.id == meal.id
        }
        selectedMeal = allMeals!!.firstOrNull { it.selected }
        val allRecipes = cache?.recipes ?: recipes?.map {
            convertRecipeToUIModel(it)
        }

        val filteredMeals =
            allMeals.filter { meal -> allRecipes?.any { meal.id in it.mealIds } == true }

        if (selectedMeal == null) {
            selectedMeal = filteredMeals.firstOrNull()?.apply { selected = true }
        }
        allRecipes?.forEach {
            it.mealName = selectedMeal?.name ?: ""
        }
        val displayItems = arrayListOf<ExtraRecipesAdapter.ExtraRecipeItem>().apply {
            add(ExtraRecipesFilterUIModel(filteredMeals))
            addAll(allRecipes?.filter { selectedMeal?.id in it.mealIds } ?: arrayListOf())
        }
        return ExtraRecipesUIModel(
            recipes = allRecipes ?: arrayListOf(),
            displayItems = displayItems
        )
    }

    fun convertCdfcToUIModel(
        cache: CdfcUIModel? = null,
        filter: CdfcFilterUIModel? = null,
        products: List<Ingredient>? = null,
        types: List<CdfcCategory>? = null
    ): CdfcUIModel {
        var selectedType = filter?.getSelectedType()
        val allTypes = (filter?.types ?: types?.map {
            MealUIModel(
                id = it.id,
                name = it.name,
                imageUrl = convertImageToUIModel(it.image, false)
            )
        })?.onEach {
            it.selected = selectedType?.id == it.id
        }
        selectedType = allTypes!!.firstOrNull { it.selected }
        val allProducts = (cache?.products ?: products?.map {
            convertIngredientToUIModel(it)
        }) ?: arrayListOf()
        val displayItems = arrayListOf<ProductsAdapter.ProductItem>().apply {
            add(CdfcFilterUIModel(allTypes).apply {
                if (selectedType == null) {
                    selectedType = this.types.filter { type ->
                        allProducts.count { it.categoryId == type.id } >= 10
                    }.random().apply { selected = true }
                }
            })
            addAll(allProducts.filter { selectedType?.id == it.categoryId })
        }
        return CdfcUIModel(
            products = allProducts,
            displayItems = displayItems
        )
    }

    private fun convertMealToUIModel(meal: Meal): MealUIModel {
        return MealUIModel(
            id = meal.id,
            name = meal.name,
            imageUrl = convertImageToUIModel(meal.image, false)
        )
    }

    private fun convertRecipeIngredientsToUIModel(
        ingredients: List<Ingredient>?,
        recipeGroups: List<RecipeGroup>?
    ): List<RecipeIngredientUIModel> {
        val useGroupNames = (recipeGroups?.size ?: 0) > 1
        arrayListOf<RecipeIngredientUIModel>().let { list ->
            recipeGroups?.forEach {
                if (useGroupNames) {
                    list.add(RecipeIngredientUIModel(name = it.name, isGroup = true))
                }
                list.addAll(getRecipeIngredientsByGroup(ingredients ?: arrayListOf(), it.id))
            }
            return list
        }

    }

    private fun getRecipeIngredientsByGroup(
        ingredients: List<Ingredient>,
        groupId: Int
    ): List<RecipeIngredientUIModel> {
        return ingredients.filter { it.amount_info?.recipe_ingredient_group_id == groupId }.map {
            val name = it.name
            val amountValue = it.amount_info?.amount_round
                ?: it.amount_info?.amount_gram
                ?: it.amount_info?.amount
            val amountName = it.amount_info?.dimension_type_name
            val emptyAmount = amountValue == null || amountValue.toInt() == 0
            val emptyDimen = amountName.isNullOrEmpty()
            val sb = StringBuilder()
            sb.append(name)
            if (!emptyAmount || !emptyDimen) {
                sb.append(" ")
                sb.append("(")
                if (!emptyAmount) {
                    sb.append(amountValue)
                    if (!emptyDimen) {
                        sb.append(" ")
                    }
                }
                if (!emptyDimen) {
                    sb.append(amountName)
                }
                sb.append(")")
            }
            RecipeIngredientUIModel(name = sb.toString())
        }
    }

    fun convertImageToUIModel(image: HashMap<String, String>?, isSmall: Boolean): String {
        var url = image?.get(if (isSmall) "1x" else "2x")
        if (url.isNullOrEmpty().not() && url?.startsWith("http") != true) {
            url = Constants.PROD_URL + url
        }
        return url ?: ""
    }

    fun convertFoodIngredientsToUIModel(
        foodIngredientsResponse: FoodIngredientsResponse,
        week: Int?
    ): FoodIngredientsUIModel {
        val apiModel = foodIngredientsResponse.result
        val weekId = week ?: apiModel?.current_week
        return FoodIngredientsUIModel(
            weeks = convertWeeksToUIModels(
                weeks = apiModel?.weeks ?: arrayListOf(),
                currentWeek = weekId
            ),
            ingredients = apiModel?.ingredients?.map { ingredient ->
                TaskUIModel(
                    id = ingredient.id,
                    title = "${ingredient.name} " + if (ingredient.amount != 0) "(${ingredient.amount} г.)" else "",
                    checked = ingredient.in_cart == true
                )
            } ?: arrayListOf()
        )
    }

    private fun convertIngredientToUIModel(ingredient: Ingredient): ProductUIModel {
        return ProductUIModel(
            id = ingredient.id,
            categoryId = ingredient.category_id,
            name = ingredient.name,
            imageUrl = convertImageToUIModel(ingredient.image, false),
            calories = "${ingredient.calories}",
            proteins = "${ingredient.proteins} г",
            fats = "${ingredient.fats} г",
            carbohydrates = "${ingredient.carbohydrates} г",
        )
    }

    private fun convertWeeksToUIModels(
        weeks: List<Week>,
        currentWeek: Int?,
        currentDay: Int? = null
    ): List<WeekUIModel> {
        return weeks.mapIndexed { index, week ->
            WeekUIModel(
                id = week.id ?: index,
                name = week.name,
                selected = week.id ?: index == currentWeek,
                days = if (currentDay == null) arrayListOf() else week.days.map { day ->
                    val date = Calendar.getInstance().apply {
                        set(Calendar.MONTH, day.month - 1)
                        set(Calendar.DAY_OF_MONTH, day.day_in_month)
                    }.time
                    DayUIModel(
                        date = date,
                        name = day.name,
                        dayNumber = day.day_number,
                        fullName = DateFormatUtils.formatDateSimpleWithDOW(date)
                            ?: day.name,
                        dayName = day.day_in_month.toString(),
                        selected = currentDay == day.day_number
                    )
                }.apply {
                    if (this.any { it.selected }.not()) {
                        this.firstOrNull()?.selected = true
                    }
                },
            ).apply {
                if (days.isNotEmpty()) {
                    val firstDate =
                        DateFormatUtils.formatDateDayFullMonth(days.first().date)?.split(" ")
                    val lastDayRaw = DateFormatUtils.formatDateDayFullMonth(days.last().date)
                    val lastDate = lastDayRaw?.split(" ")
                    val sameMonth = firstDate?.lastOrNull() == lastDate?.lastOrNull()
                    this.dates =
                        "${firstDate?.first() + if (!sameMonth) " " + firstDate?.last() else ""} - $lastDayRaw"
                }
            }
        }
    }

    fun convertWorkoutToUIModel(workout: TodayWorkout): WorkoutUIModel {
        val descriptionData = try {
            workout.description_data?.asJsonArray?.map {
                Gson().fromJson(it, DynamicContentItem::class.java)
            }
        } catch (_: Throwable) {
            arrayListOf<DynamicContentItem>()
        }
        return WorkoutUIModel(
            id = workout.id,
            dayNumber = workout.day_number,
            indexInDay = workout.index_in_day,
            name = workout.name,
            description = workout.description,
            descriptionRawData = workout.description_data.toString(),
            descriptionData = buildSpannableStringFromDescriptionData(
                descriptionData = descriptionData
            ),
            videoType = workout.video_type == "youtube",
            videoUrl = workout.video_url ?: "",
            imageUrl = convertImageToUIModel(workout.image, false)
        )
    }

    fun createDescriptionDataSpannableString(rawContent: String): SpannableStringBuilder {
        val descriptionData = try {
            val type = object : TypeToken<List<DynamicContentItem>>() {}.type
            Gson().fromJson<List<DynamicContentItem>>(rawContent, type)
        } catch (_: Throwable) {
            arrayListOf()
        }
        return buildSpannableStringFromDescriptionData(
            descriptionData = descriptionData
        ) ?: SpannableStringBuilder(rawContent)
    }

    private fun buildSpannableStringFromDescriptionData(
        descriptionData: List<DynamicContentItem>?
    ): SpannableStringBuilder? {
        if (descriptionData.isNullOrEmpty()) {
            return null
        }
        val spannableStringBuilder = SpannableStringBuilder()
        var prevItem: DynamicContentItem? = null
        descriptionData.forEach { item ->
            processDynamicItem(item, prevItem).forEach { ss ->
                spannableStringBuilder.append(ss)
            }
            prevItem = item
        }
        return spannableStringBuilder
    }

    private fun processDynamicItem(
        item: DynamicContentItem,
        prevItem: DynamicContentItem?
    ): List<SpannableString> {
        val result = arrayListOf<SpannableString>()
        var text = item.font?.text
        if (item.isNumber())
            text += "."

        if ((item.isSpan() || item.isNumber()) && (prevItem?.isNumber() == true || prevItem?.isSpan() == true)) {
            result.add(SpannableString(" "))
        }

        if (item.isListItem() && item.children?.any { it.isNumber() } != true) {
            result.add(SpannableString("·"))
            result.add(SpannableString(" "))
        }

        if (text.isNullOrEmpty().not()) {
            val span = SpannableString(text)
            if (item.isStrong()) {
                span.setSpan(
                    StyleSpan(Typeface.BOLD),
                    0,
                    text?.length ?: 0,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            result.add(span)
        }

        var prevInner: DynamicContentItem? = prevItem
        item.children?.forEach {
            processDynamicItem(it, prevInner).forEach { ss ->
                result.add(ss)
            }
            prevInner = it
        }
        if (item.isSpan().not() && item.isNumber().not() &&
            item.isList().not() &&
            result.toString().endsWith("\n\n").not()
        ) {
            result.add(SpannableString("\n"))
        }
        return result
    }

    fun createUpdateIngredientsRequest(
        weekId: Int,
        ingredients: List<TaskUIModel>
    ) = UpdateIngredientsRequest(
        week = weekId,
        ingredientIds = ingredients.filter { it.checked }.map { it.id }
    )

    fun convertProfilePersonalInfoUIModel(profile: Profile?): ProfilePersonalInfoUIModel {
        val birthDate = DateFormatUtils.parseServerSimpleDate(profile?.birth_date ?: "")
        return ProfilePersonalInfoUIModel(
            name = ObservableField(profile?.first_name),
            lastName = ObservableField(profile?.last_name ?: ""),
            middleName = ObservableField(profile?.middle_name ?: ""),
            email = ObservableField(profile?.email ?: ""),
            city = ObservableField(profile?.city ?: ""),
            phone = ObservableField(profile?.phone ?: ""),
            instagram = ObservableField(profile?.instagram ?: ""),
            telegram = ObservableField(profile?.telegram ?: ""),
            childrenCount = ObservableField(profile?.children_count ?: ""),
            dateOfBirth = ObservableField(birthDate?.toString("dd/MM/yyyy ") ?: ""),
            familyStatus = ObservableField(profile?.family_status?.id?.toInt() ?: -1)
        )
    }

    fun convertProfilePersonalParamUIModel(response: ProfileBodyParamsResponse): ProfileBodyParamsUIModel {
        response.result?.let {
            return ProfileBodyParamsUIModel(
                weight = ObservableField(it.weight),
                height = ObservableField(it.height),
                breast = ObservableField(it.breast_volume),
                waist = ObservableField(it.waist),
                hips = ObservableField(it.hips)

            )

        } ?: kotlin.run {
            return ProfileBodyParamsUIModel(
                weight = ObservableField(""),
                height = ObservableField(""),
                breast = ObservableField(""),
                waist = ObservableField(""),
                hips = ObservableField("")
            )
        }

    }

    fun convertTestDetailsToUIModel(
        response: MarathonProfileDetails?,
        dataManager: DataManager,
        isUpdate: Boolean = false
    ): List<TestItem> {
        val profile = response?.profile
        val params = response?.params
        if (profile != null && params != null) {
            arrayListOf<TestItem>().let { list ->
                list.add(
                    TestHeaderItem(
                        title = dataManager.getStringFromResources(R.string.profile_test),
                        description = dataManager.getStringFromResources(R.string.test_description)
                    )
                )
                params.forEach { mapEntity ->
                    when (mapEntity.key) {
                        TEST_ITEM_DAILY_ACTIVITY -> profile.getDailyActivityId()
                        TEST_ITEM_DIET_PURPOSE -> profile.getDietPurposeId()
                        TEST_ITEM_SMOKE -> profile.getSmokeId()
                        TEST_ITEM_ALCOHOL -> profile.getAlcoholId()
                        TEST_ITEM_LACTATION -> profile.getLactationId()
                        TEST_ITEM_PREGNANCY -> profile.getPregnancyId()
                        TEST_ITEM_NUTRITION_PLAN -> profile.getNutritionPlanId()
                        TEST_ITEM_WORKOUT_PLAN -> profile.getWorkoutPlanId()
                        TEST_ITEM_FAMILY_STATUS -> profile.getFamilyStatus()
                        else -> null
                    }?.let { selectedId ->
                        val item = mapEntity.value
                        list.add(
                            TestItemUIModel(
                                title = item.name ?: "",
                                type = mapEntity.key,
                                description = item.description ?: "",
                                options = item.values?.map {
                                    TestItemOptionUIModel(
                                        id = it.id,
                                        title = it.value ?: it.name ?: "",
                                        description = it.value2 ?: it.description ?: "",
                                        selected = ObservableBoolean(it.id == selectedId)
                                    )
                                } ?: arrayListOf()
                            )
                        )
                    }
                }
                list.add(
                    TestFooterItem(
                        title = dataManager.getStringFromResources(R.string.additional_info),
                        description = dataManager.getStringFromResources(R.string.purposes),
                        fieldHint = dataManager.getStringFromResources(R.string.purposes_hint),
                        textValue = ObservableField(if (profile.goals.isNullOrEmpty()) "" else profile.goals)
                    ).apply {
                        if (isUpdate) {
                            hintText = dataManager.getStringFromResources(R.string.data_saved)
                        }
                    }
                )
                return list
            }
        } else {
            return arrayListOf()
        }
    }

    fun createUpdateTestApiModel(
        content: List<TestItemUIModel>,
        marathon: Int,
        goalsText: String?
    ): CompleteTestRequest {
        return CompleteTestRequest(
            marathonId = marathon.toString()
        ).apply {
            if (!goalsText.isNullOrEmpty()) {
                goals = goalsText
            }
            content.forEach {
                val id = it.getSelectedOptionId()
                when (it.type) {
                    TEST_ITEM_DAILY_ACTIVITY -> daily_activity_id = id
                    TEST_ITEM_DIET_PURPOSE -> diet_purpose_id = id
                    TEST_ITEM_SMOKE -> smoke_id = id
                    TEST_ITEM_ALCOHOL -> alcohol_id = id
                    TEST_ITEM_LACTATION -> lactation_id = id
                    TEST_ITEM_PREGNANCY -> pregnancy_id = id
                    TEST_ITEM_NUTRITION_PLAN -> nutrition_plan_id = id
                    TEST_ITEM_WORKOUT_PLAN -> workout_plan_id = id
                }
            }
        }
    }

    fun convertReferralDetailsToUIModel(
        referralDetails: ReferralDetails
    ): ReferralDetailsUIModel {
        var ref = referralDetails.referral ?: ""
        ref = if (ref.startsWith(Constants.WEB_HOME)) ref else Constants.WEB_HOME.dropLast(1) + ref
        return ReferralDetailsUIModel(
            link = ref,
            balance = referralDetails.balance.toString(),
            transactions = referralDetails.weekly_transactions?.map {
                TransactionUIModel(
                    date = DateFormatUtils.formatDateTech(
                        DateFormatUtils.parseServerDate(
                            it.created_at ?: ""
                        )
                    ) ?: "",
                    summ = "${if (it.type_code == "withdraw") "-" else "+"}${it.amount} ₽",
                    description = if (it.type == 2 && it.phone.isNullOrEmpty().not())
                        "(Вывод на телефон ${it.phone!!})" else "",
                    confirmed = it.confirmed == 1
                )
            } ?: arrayListOf()
        )
    }

    fun convertReportsToUIModel(api: Reports, dataManager: DataManager): ReportsUIModel {
        val weeks =
            convertWeeksToUIModels(api.weeks ?: arrayListOf(), null, api.current_day).apply {
                lastOrNull()?.selected = true
            }
        return ReportsUIModel(
            weeks = weeks,
            canEdit = api.can_edit == true,
            reports = weeks.map { week ->
                week.id to ReportUIModel(
                    insideVisible = week.id != 0,
                    welcomeText = if (week.id == 0)
                        dataManager.getStringFromResources(R.string.report_welcome_first)
                    else String.format(
                        dataManager.getStringFromResources(R.string.report_welcome),
                        week.id
                    ),
                    reportText = api.reports?.firstOrNull { it.week == week.id }?.text ?: ""
                )
            }.toMap()
        )
    }

    fun convertVotingToUIModel(
        apiVoting: VotingDetails,
        dataManager: DataManager
    ): List<VotingItem> {
        return arrayListOf<VotingItem>().apply {
            val isMale = dataManager.getProfileInfo()?.sex?.isMale() == true
            add(
                VotingHeaderUIModel(
                    isAgreed = true,
                    isActive = apiVoting.is_active == true,
                    maxVotes = apiVoting.max_votes,
                    votes = apiVoting.votes.size,
                    voteId = apiVoting.vote_id,
                    availableVotes = apiVoting.available_votes,
                    isMale = isMale
                )
            )
            addAll(apiVoting.users?.map { vote ->
                VotingItemUIModel(
                    id = vote.id,
                    isMale = isMale,
                    title = "${vote.first_name}, ${vote.age}",
                    startWeight = "${vote.weight_1} кг",
                    endWeight = "${vote.weight_2} кг",
                    weightResult = "${vote.weight_2 - vote.weight_1} кг",
                    images = arrayListOf<Pair<String, String>>().apply {
                        add(Pair(vote.front_1, vote.front_2))
                        add(Pair(vote.side_1, vote.side_2))
                        add(Pair(vote.back_1, vote.back_2))
                    },
                    votes = vote.vote_count,
                    isLiked = vote.id in apiVoting.votes
                )
            } ?: arrayListOf())
        }
    }

    fun createCalorieUIModel(dataManager: DataManager): List<CalorieItem> {
        val isMale = dataManager.getProfileInfo()?.sex?.isMale() == true
        return arrayListOf<CalorieItem>(
            CalorieItemUIModel(
                type = CALORIE_ITEM_GOAL,
                title = dataManager.getStringFromResources(R.string.calorie_calculator_goal_title),
                description = dataManager.getStringFromResources(R.string.calorie_calculator_goal_description),
                options = arrayListOf(
                    CalorieItemOptionUIModel(
                        coefficient = if (isMale) 450.0 else 0.8,
                        title = dataManager.getStringFromResources(R.string.calorie_calculator_goal_option_1),
                        description = "",
                        selected = ObservableBoolean()
                    ),
                    CalorieItemOptionUIModel(
                        coefficient = if (isMale) 300.0 else 1.0,
                        title = dataManager.getStringFromResources(R.string.calorie_calculator_goal_option_2),
                        description = "",
                        selected = ObservableBoolean()
                    )
                )
            ),
            CalorieItemUIModel(
                type = CALORIE_ITEM_ACTIVITY,
                title = dataManager.getStringFromResources(R.string.calorie_calculator_activity_title),
                description = dataManager.getStringFromResources(R.string.calorie_calculator_activity_description),
                options = arrayListOf(
                    CalorieItemOptionUIModel(
                        coefficient = if (isMale) 1.2 else 1.2,
                        title = dataManager.getStringFromResources(if (isMale) R.string.calorie_calculator_activity_male_opt_1_title else R.string.calorie_calculator_activity_female_opt_1_title),
                        description = dataManager.getStringFromResources(if (isMale) R.string.calorie_calculator_activity_male_opt_1_description else R.string.calorie_calculator_activity_female_opt_1_description),
                        selected = ObservableBoolean()
                    ),
                    CalorieItemOptionUIModel(
                        coefficient = if (isMale) 1.375 else 1.38,
                        title = dataManager.getStringFromResources(if (isMale) R.string.calorie_calculator_activity_male_opt_2_title else R.string.calorie_calculator_activity_female_opt_2_title),
                        description = dataManager.getStringFromResources(if (isMale) R.string.calorie_calculator_activity_male_opt_2_description else R.string.calorie_calculator_activity_female_opt_2_description),
                        selected = ObservableBoolean()
                    ),
                    CalorieItemOptionUIModel(
                        coefficient = if (isMale) 1.55 else 1.56,
                        title = dataManager.getStringFromResources(if (isMale) R.string.calorie_calculator_activity_male_opt_3_title else R.string.calorie_calculator_activity_female_opt_3_title),
                        description = dataManager.getStringFromResources(if (isMale) R.string.calorie_calculator_activity_male_opt_3_description else R.string.calorie_calculator_activity_female_opt_3_description),
                        selected = ObservableBoolean()
                    )
                ).apply {
                    if (isMale) add(
                        CalorieItemOptionUIModel(
                            coefficient = 1.725,
                            title = dataManager.getStringFromResources(R.string.calorie_calculator_activity_male_opt_4_title),
                            description = dataManager.getStringFromResources(R.string.calorie_calculator_activity_male_opt_4_description),
                            selected = ObservableBoolean()
                        )
                    )
                }
            ),
        ).apply {
            if (!isMale) add(
                CalorieItemUIModel(
                    type = CALORIE_ITEM_LACTATION,
                    title = dataManager.getStringFromResources(R.string.calorie_calculator_lactation_title),
                    description = dataManager.getStringFromResources(R.string.calorie_calculator_lactation_description),
                    options = arrayListOf(
                        CalorieItemOptionUIModel(
                            coefficient = 350.0,
                            title = dataManager.getStringFromResources(R.string.calorie_calculator_lactation_yes),
                            description = "",
                            selected = ObservableBoolean()
                        ),
                        CalorieItemOptionUIModel(
                            coefficient = 0.0,
                            title = dataManager.getStringFromResources(R.string.calorie_calculator_lactation_no),
                            description = "",
                            selected = ObservableBoolean()
                        )
                    )
                )
            )
        }
    }

    fun convertChatToUIModel(chatDetails: ChatDetails, dataManager: DataManager): ChatUIModel {
        val userId = chatDetails.user_id
        val canEdit = chatDetails.can_edit
        var messages = chatDetails.messages.map {
            convertChatMessageToUIModel(
                message = it,
                userId = userId,
                dataManager = dataManager
            )
        }
        if (!canEdit) {
            messages = messages + object : ChatItem {}
        }
        return ChatUIModel(
            canEdit = canEdit,
            userId = userId,
            messages = messages
        )
    }

    fun convertChatMessageToUIModel(
        message: ChatMessage,
        userId: Int,
        dataManager: DataManager
    ): ChatItem {
        val isOperator = userId != message.user_id
        val isMale = dataManager.getProfileInfo()?.sex?.isMale() == true
        return ChatMessageUIModel(
            isMale = isMale,
            dateTime = DateFormatUtils.formatChatDateTime(
                DateFormatUtils.parseServerDate(
                    message.created_at
                )
            ) ?: "",
            isOperator = isOperator,
            name = when {
                isOperator -> dataManager.getStringFromResources(
                    if (isMale) R.string.chat_support_artemchek
                    else R.string.chat_support_lercheck
                )
                else -> dataManager.getProfileInfo()?.first_name ?: ""
            },
            text = message.html ?: message.text
        )
    }

    fun convertMarathonsProgressToUIModel(
        response: MarathonProgressResponse,
        dataManager: DataManager
    ): MarathonProgressUIModel {
        return MarathonProgressUIModel(
            isMale = dataManager.getProfileInfo()?.sex?.isMale() == true,
            profileName = dataManager.getProfileInfo()?.first_name ?: "",
            marathonImageUrl = dataManager.getCurrentMarathonImageUrl(),
            response.result?.progress ?: Progress(),
            response.result?.weeks,
            response.result?.statistics_weeks,
            response.result?.current_week ?: -1
        )
    }

}