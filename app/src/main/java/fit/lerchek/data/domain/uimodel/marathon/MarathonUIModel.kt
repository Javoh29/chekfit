package fit.lerchek.data.domain.uimodel.marathon

data class MarathonUIModel(
    var id: Int,
    var name: String?,
    var status: String?,
    var startDate: String?,
    var endDate: String?,
    var imageUrl: String?,
)