package fit.lerchek.data.domain.uimodel.marathon

import androidx.databinding.ObservableBoolean

const val CALORIE_ITEM_GOAL = "goal"
const val CALORIE_ITEM_ACTIVITY = "activity"
const val CALORIE_ITEM_LACTATION = "lactation"

open class CalorieItem(val title: String, val description: String)

class CalorieItemUIModel(
    title: String,
    description: String,
    val type: String,
    val options: List<CalorieItemOptionUIModel>?,
) : CalorieItem(title, description) {
    fun getSelectedOptionCoefficient(): Double? {
        return options?.firstOrNull { it.selected.get() }?.coefficient
    }
}

class CalorieItemOptionUIModel(
    title: String,
    description: String,
    val coefficient: Double,
    val selected: ObservableBoolean
) : CalorieItem(title, description)