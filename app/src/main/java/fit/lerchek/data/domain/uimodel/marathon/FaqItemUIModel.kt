package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.FaqAdapter

class FaqItemUIModel(
    var question: String = "",
    var answer: String = "",
    var expanded: Boolean = false
) : FaqAdapter.FaqListItem
