package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.more.ExtraRecipesAdapter

class ExtraRecipesUIModel (
    var displayItems: List<ExtraRecipesAdapter.ExtraRecipeItem> = arrayListOf(),
    var recipes : List<RecipeUIModel> = arrayListOf()
)