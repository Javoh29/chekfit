package fit.lerchek.data.domain.exceptions

class LoginError : Throwable() {
    var code: Int = 0
    override var message: String = ""
    var emailError: String? = null
    var passwordError: String? = null
}