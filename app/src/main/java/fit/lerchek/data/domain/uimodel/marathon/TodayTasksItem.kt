package fit.lerchek.data.domain.uimodel.marathon

class TodayTasksItem(
    val marathonName: String? = "",
    val isMale:Boolean = true,
    val profileName:String ="",
    val currentDay:Int = 0,
    val currentMarathonDay:Int = 0,
    val activityCalendarVisible:Boolean = false,
    val tasks: List<TaskUIModel>? = arrayListOf()
): TodayItem(Type.Tasks)