package fit.lerchek.data.domain.uimodel.marathon

class TodayErrorItem(
    var isExpired: Boolean = false,
    var isWaiting: Boolean
): TodayItem(Type.Error)