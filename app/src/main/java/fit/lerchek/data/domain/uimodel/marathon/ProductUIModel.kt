package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.more.ProductsAdapter

class ProductUIModel(
    val id: Int,
    val categoryId: Int?,
    var name: String = "",
    var imageUrl: String? = null,
    var calories: String = "",
    var proteins: String = "",
    var fats: String = "",
    var carbohydrates: String = "",
) : ProductsAdapter.ProductItem