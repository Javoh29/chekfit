package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.workout.WorkoutAdapter

class WorkoutDetailsUIModel (
    var displayItems: List<WorkoutAdapter.WorkoutItem> = arrayListOf(object : WorkoutAdapter.WorkoutItem{}),
    var workouts : List<WorkoutUIModel> = arrayListOf()
)