package fit.lerchek.data.domain.uimodel.marathon

open class FilterUIModel(
    var weeks: List<WeekUIModel> = arrayListOf()
) {

    fun getSelectedWeek(): WeekUIModel? {
        return weeks.firstOrNull { it.selected } ?: weeks.firstOrNull()
    }

    fun selectWeek(id: Int?) {
        weeks.onEach { it.selected = it.id == id }
    }

    fun getCurrentDay(): DayUIModel? {
        val days = getSelectedWeek()?.days
        return days?.firstOrNull { it.selected } ?: days?.firstOrNull()
    }

    fun selectDay(dayNumber: Int?) {
        val days = getSelectedWeek()?.days
        days?.onEach { it.selected = it.dayNumber == dayNumber }
    }

    fun getTodayName(): String {
        return getCurrentDay()?.fullName ?: ""
    }

    fun getDays(): List<DayUIModel> {
        return getSelectedWeek()?.days ?: arrayListOf()
    }
}