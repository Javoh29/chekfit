package fit.lerchek.data.domain.uimodel.marathon

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import fit.lerchek.R

class BMIItemUIModel(val type: Type) {

    enum class Type(
        val min: Double,
        val max: Double,
        @DrawableRes val imgRes: Int,
        @StringRes val nameRes: Int,
        @StringRes val descriptionRes: Int
    ) {
        UNDERWEIGHT(
            0.0,
            18.5,
            R.drawable.ic_bmi_underweight,
            R.string.bmi_calculator_underweight_title,
            R.string.bmi_calculator_underweight_description
        ),
        NORMAL(
            18.5,
            24.9,
            R.drawable.ic_bmi_normal,
            R.string.bmi_calculator_normal_title,
            R.string.bmi_calculator_normal_description
        ),
        OVERWEIGHT(
            25.0,
            29.9,
            R.drawable.ic_bmi_overweight,
            R.string.bmi_calculator_overweight_title,
            R.string.bmi_calculator_overweight_description
        ),
        OBESITY(
            30.0,
            39.9,
            R.drawable.ic_bmi_obesity,
            R.string.bmi_calculator_obesity_title,
            R.string.bmi_calculator_obesity_description
        );

        companion object {

            fun get(value: Double): Type {
                for (type in values()) {
                    when (type.ordinal) {
                        0 -> if (value < type.max) return type
                        else -> if (value >= type.min && value < type.max + 0.1) return type
                    }
                }
                return OBESITY
            }
        }
    }

    val value: String
        get() {
            return when (type.ordinal) {
                0 -> "< ${type.max}"
                else -> "${type.min} - ${type.max}"
            }
        }

    val number: String
        get() {
            return (type.ordinal + 1).toString()
        }
}