package fit.lerchek.data.domain.uimodel.profile

class TransactionUIModel(
    var date: String,
    var description: String,
    var summ: String,
    var confirmed:Boolean
)