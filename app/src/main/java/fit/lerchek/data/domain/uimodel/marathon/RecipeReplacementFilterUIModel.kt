package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.food.RecipeReplacementAdapter

class RecipeReplacementFilterUIModel(
    var imageUrl: String,
    var name: String
): RecipeReplacementAdapter.ReplacementItem