package fit.lerchek.data.domain.uimodel.marathon

class WaterTrackerItem(
    var dimension: Int,
    var goal: Int,
    var value: Int,
    var date: String
) : TrackerItem(Type.WaterTracker)