package fit.lerchek.data.domain.uimodel.marathon

import java.util.*

class DayUIModel(
    val dayNumber: Int,
    val name: String,
    val dayName: String,
    val fullName: String,
    val date: Date,
    var selected: Boolean
)