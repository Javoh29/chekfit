package fit.lerchek.data.domain.uimodel.profile

import androidx.databinding.ObservableField
import fit.lerchek.data.api.message.marathon.ProfileBodyParamsRequest

class ProfileBodyParamsUIModel (
    var weight: ObservableField<String> = ObservableField(),
    var height: ObservableField<String> = ObservableField(),
    var breast: ObservableField<String> = ObservableField(),
    var waist: ObservableField<String> = ObservableField(),
    var hips: ObservableField<String> = ObservableField()
){
    fun buildUpdateBodyParamsRequest(): ProfileBodyParamsRequest {
        return ProfileBodyParamsRequest(weight.get(), height.get(), breast.get(), waist.get(), hips.get())
    }
}
