package fit.lerchek.data.domain.uimodel.marathon

class TodayRefItem(
    var Link: String
) : TodayItem(Type.Referral) {
    interface Callback {
        fun onLinkCopied(link: String)
        fun onShareLink(link: String){}
        fun onWithdraw(){}
    }
}