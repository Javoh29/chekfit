package fit.lerchek.data.domain.uimodel.marathon

class FoodNutritionPlanUIModel(
    val id: Int,
    val name: String,
    val description: String,
    val imageUrl: String,
    var selected: Boolean
)