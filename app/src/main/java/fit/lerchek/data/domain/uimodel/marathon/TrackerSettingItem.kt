package fit.lerchek.data.domain.uimodel.marathon

import androidx.databinding.ObservableField
import fit.lerchek.ui.feature.marathon.today.adapters.SettingTrackerAdapter

abstract class TrackerSettingItem(var type: Type) {
    enum class Type(val itemViewType: Int) {
        WaterTrackerSet(SettingTrackerAdapter.ITEM_VIEW_TYPE_WATER_TRACKER_SET),
        StepsTrackerSet(SettingTrackerAdapter.ITEM_VIEW_TYPE_STEPS_TRACKER_SET),
        SleepTrackerSet(SettingTrackerAdapter.ITEM_VIEW_TYPE_SLEEP_TRACKER_SET),
        StepsTrackerAdd(SettingTrackerAdapter.ITEM_VIEW_TYPE_STEPS_TRACKER_ADD),
        SleepTrackerAdd(SettingTrackerAdapter.ITEM_VIEW_TYPE_SLEEP_TRACKER_ADD),
    }
}

class WaterTrackerSetItem(
    var methodType: Int,
    val countMl: String,
    val countGoal: String,
    val isSpinnerView: Boolean
) : TrackerSettingItem(Type.WaterTrackerSet)

class StepsTrackerAddItem(
    val countSteps: ObservableField<String>,
    val date: String
) : TrackerSettingItem(Type.StepsTrackerAdd)

class StepsTrackerSetItem(
    val countSteps: String
) : TrackerSettingItem(Type.StepsTrackerSet)

class SleepTrackerAddItem(
    val sleepHours: String,
    val sleepMinutes: String,
    val date: String
) : TrackerSettingItem(Type.SleepTrackerAdd)

class SleepTrackerSetItem(
    val sleepHours: String,
    val sleepMinutes: String
) : TrackerSettingItem(Type.SleepTrackerSet)