package fit.lerchek.data.domain.uimodel.marathon

class FoodIngredientsUIModel (
    var ingredients: List<TaskUIModel> = arrayListOf(),
    var weeks : List<WeekUIModel> = arrayListOf()
)