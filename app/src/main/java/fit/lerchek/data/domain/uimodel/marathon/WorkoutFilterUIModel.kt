package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.workout.WorkoutAdapter

class WorkoutFilterUIModel(
    weeks: List<WeekUIModel>,
    var levels: List<PlanUIModel> = arrayListOf(),
    var isMale:Boolean
) : FilterUIModel(weeks), WorkoutAdapter.WorkoutItem {

    fun getLevel(): PlanUIModel? {
        return levels.firstOrNull { it.selected } ?: levels.firstOrNull()
    }

    fun selectLevel(id: Int?) {
        levels.onEach { it.selected = it.id == id }
    }

}