package fit.lerchek.data.domain.uimodel.marathon

import androidx.core.text.HtmlCompat
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField

const val TEST_ITEM_DAILY_ACTIVITY = "daily_activity"
const val TEST_ITEM_DIET_PURPOSE = "diet_purpose"
const val TEST_ITEM_SMOKE = "smoke"
const val TEST_ITEM_ALCOHOL = "alcohol"
const val TEST_ITEM_LACTATION = "lactation"
const val TEST_ITEM_PREGNANCY = "pregnancy"
const val TEST_ITEM_NUTRITION_PLAN = "nutrition_plan"
const val TEST_ITEM_WORKOUT_PLAN = "workout_plan"
const val TEST_ITEM_FAMILY_STATUS = "family_status"

open class TestItem{

    val title: String
    val description: String
    get() {
         return HtmlCompat.fromHtml(field, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
    }

    constructor(title: String, description: String) {
        this.title = title
        this.description = description
    }
}

class TestItemUIModel(
    title: String,
    description: String,
    val type: String,
    val options: List<TestItemOptionUIModel>?,
) : TestItem(title, description) {
    fun getSelectedOptionId(): Int? {
        return options?.firstOrNull { it.selected.get() }?.id
    }
}

class TestHeaderItem(
    title: String,
    description: String,
) : TestItem(title, description)

class TestFooterItem(
    title: String,
    description: String,
    val fieldHint: String,
    var hintText: String = "",
    val textValue: ObservableField<String>?,
) : TestItem(title, description)

class TestItemOptionUIModel(
    title: String,
    description: String,
    val id: Int,
    val selected: ObservableBoolean
) : TestItem(title, description)