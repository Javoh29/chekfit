package fit.lerchek.data.domain.uimodel.marathon

class TodaySleepItem(
    val dimension: Int,
    val goal: Int,
    val value: Int
) : TodayItem(Type.SleepTracker)