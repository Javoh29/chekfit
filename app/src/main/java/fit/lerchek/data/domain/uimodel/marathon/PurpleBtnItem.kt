package fit.lerchek.data.domain.uimodel.marathon

class PurpleBtnItem(
    val settingType: String,
    val date: String
) : TrackerItem(Type.ChangeBtn)