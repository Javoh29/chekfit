package fit.lerchek.data.domain.uimodel.marathon

class FoodDayUIModel(
    val dayNumber: Int,
    val name: String,
    val dayName: String,
    val fullName: String,
    var selected: Boolean
)