package fit.lerchek.data.domain.uimodel.marathon

class RecipeIngredientUIModel (
    var name:String = "",
    var isGroup : Boolean = false
)