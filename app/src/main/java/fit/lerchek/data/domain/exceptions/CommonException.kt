package fit.lerchek.data.domain.exceptions

class CommonException : Throwable() {
    var code: Int = 0
    var stringCode: String? = null
    override var message: String = ""
}