package fit.lerchek.data.domain.uimodel.marathon

class SettingTrackerItem(
    val settingType: String,
    val goal: Int,
    val dimension: Int
) : TrackerItem(Type.SettingTracker)