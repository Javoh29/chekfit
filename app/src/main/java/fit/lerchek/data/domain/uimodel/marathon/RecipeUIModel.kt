package fit.lerchek.data.domain.uimodel.marathon

import android.text.SpannableStringBuilder
import fit.lerchek.ui.feature.marathon.food.FoodAdapter
import fit.lerchek.ui.feature.marathon.food.RecipeReplacementAdapter
import fit.lerchek.ui.feature.marathon.more.ExtraRecipesAdapter

class RecipeUIModel(
    val id: Int,
    val baseId: Int?,
    val index: Int? = null,
    val dayNumber: Int? = null,
    val planId: Int? = null,
    var mealName: String = "",
    var mealId: Int = -1,
    var calories: String = "",
    var name: String = "",
    var imageUrl: String? = null,
    var hasReplacements: Boolean = false,
    var showWithoutOven: Boolean = false,
    var proteins: String = "",
    var fats: String = "",
    var carbohydrates: String = "",
    var ingredients: List<RecipeIngredientUIModel>,
    var mealIds: List<Int> = arrayListOf(),
    var description: String = "",
    var isFavorite: Boolean = false,
    var descriptionSpan: SpannableStringBuilder =SpannableStringBuilder(""),
) : FoodAdapter.FoodItem, RecipeReplacementAdapter.ReplacementItem,
    ExtraRecipesAdapter.ExtraRecipeItem