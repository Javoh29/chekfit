package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.widget.linechart.BarData

class LineTrackerItem(
    val title: Int,
    val goal: Int,
    val dimension: Int,
    val listData: ArrayList<BarData>,
    val progressBarDrawable: Int,
    val progressBarDrawable2: Int
) : TrackerItem(Type.LineTracker)