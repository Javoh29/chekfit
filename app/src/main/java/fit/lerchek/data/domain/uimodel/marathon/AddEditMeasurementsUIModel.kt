package fit.lerchek.data.domain.uimodel.marathon

import androidx.annotation.StringRes
import fit.lerchek.R

class AddEditMeasurementsUIModel(val type: Type) {

    enum class Type(
        @StringRes val titleRes: Int,
        @StringRes val hintRes: Int
    ) {
        WEIGHT(
            R.string.progress_your_weight_title,
            R.string.progress_your_weight_hint
        ),
        WAIST(
            R.string.progress_your_waist_title,
            R.string.progress_your_waist_hint
        ),
        BREAST(
            R.string.progress_your_breast_title,
            R.string.progress_your_breast_hint
        ),
        HIPS(
            R.string.progress_your_hips_title,
            R.string.progress_your_hips_hint
        );
    }
}