package fit.lerchek.data.domain.uimodel.marathon

import android.text.SpannableStringBuilder
import fit.lerchek.ui.feature.marathon.workout.WorkoutAdapter

class WorkoutUIModel(
    val id: Int,
    var name: String = "",
    var imageUrl: String? = null,
    var videoUrl: String = "",
    var videoType: Boolean = true,
    var description: String? = null,
    var descriptionData: SpannableStringBuilder? = null,
    var descriptionRawData: String? = null,
    var dayNumber: Int? = null,
    var indexInDay: Int? = null,
) : WorkoutAdapter.WorkoutItem