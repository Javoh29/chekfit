package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.data.api.model.Meal

class FavoriteUIModel(
    val meals:HashMap<String, Meal>,
    val recipes: List<RecipeUIModel>,
)