package fit.lerchek.data.domain.uimodel.marathon

class TaskUIModel(
    val id: Int,
    var checked: Boolean = false,
    var title: String = "",
    var comment: String = ""
)