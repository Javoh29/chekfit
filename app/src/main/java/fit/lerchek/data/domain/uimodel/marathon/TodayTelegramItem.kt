package fit.lerchek.data.domain.uimodel.marathon

class TodayTelegramItem(
    var channel: String? = null,
    var group: String? = null
): TodayItem(Type.Telegram)