package fit.lerchek.data.domain.uimodel.chat

class ChatUIModel(
    val canEdit: Boolean,
    val userId: Int,
    val messages: List<ChatItem>
)