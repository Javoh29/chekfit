package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.data.api.model.*
import fit.lerchek.ui.util.TextUtils.convertWithOneDecimal
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class MarathonProgressUIModel constructor(
    val isMale: Boolean = true,
    val profileName: String = "",
    val marathonImageUrl: String? = null,
    val progress: Progress = Progress(),
    val weeks: List<Week>? = null,
    val statistics_weeks: List<StatisticsWeek>? = null,
    val current_week: Int = -1
) {
    val progressEmpty: Boolean
        get() = progress.bodies.isNullOrEmpty()
                && progress.videos.isNullOrEmpty()
                && progress.photos.isNullOrEmpty()

    val photosEmpty: Boolean
        get() = progress.photos?.let { photos ->
            photos.size < 2 || photos.any { it.front.isNullOrEmpty() || it.back.isNullOrEmpty() || it.side.isNullOrEmpty() }
        } ?: true

    val photosError: Boolean
        get() = photosError(current_week)

    val photosReviewComment: String?
        get() = photosReviewComment(current_week)

    fun photosError(week: Int): Boolean {
        val photo = photo(week) ?: return false
        return photo.review_status == 2 && !photo.review_comment.isNullOrEmpty()
    }

    fun photosReviewComment(week: Int): String? {
        photo(week)?.let { photo ->
            if (photo.review_status == 2) {
                photo.review_comment?.let {
                    return photo.review_comment
                }
            }
        }
        return null
    }

    fun videoError(week: Int): Boolean {
        val video = video(week) ?: return false
        return video.review_status == 2 && !video.review_comment.isNullOrEmpty()
    }

    fun videoReviewComment(week: Int): String? {
        video(week)?.let { photo ->
            if (photo.review_status == 2) {
                photo.review_comment?.let {
                    return photo.review_comment
                }
            }
        }
        return null
    }

    fun photoSucceed(week: Int): Boolean {
        val photo = photo(week) ?: return false
        return photo.review_status == 1 && !photo.review_comment.isNullOrEmpty()
    }

    fun photo(week: Int): Photo? {
        return progress.photos?.find { it.week == week }
    }

    fun emptyPhoto(week: Int): Boolean {
        return photo(week) == null
    }

    fun video(week: Int): Video? {
        return progress.videos?.find { it.week == week }
    }

    fun videoUrl(week:Int) :String?{
        return video(week)?.url
    }

    private fun measurements(type: MeasurementsUIModel.Type): List<Float> {
        return when (type) {
            MeasurementsUIModel.Type.WEIGHT -> progress.bodies?.map { it.weight ?: -1F }
            MeasurementsUIModel.Type.WAIST -> progress.bodies?.map { it.waist ?: -1F }
            MeasurementsUIModel.Type.BREAST -> progress.bodies?.map { it.breast_volume ?: -1F }
            MeasurementsUIModel.Type.HIPS -> progress.bodies?.map { it.hips ?: -1F }
        } ?: listOf()
    }

    fun measurementsMinMidMax(type: MeasurementsUIModel.Type): Triple<Int, Int, Int> {
        val measurements = measurements(type)
        val max = getMax(measurements.maxOfOrNull { it } ?: 0F)
        val min = getMin(measurements.minOfOrNull { it } ?: 0F)
        val mid = min + (max - min) / 2

        return Triple(min, mid, max)
    }

    private fun getMax(max: Float): Int {
        if (max <= 0F) return 0
        val r = max.roundToInt() % 5
        return max.roundToInt() + (5 - r)
    }

    private fun getMin(min: Float): Int {
        if (min <= 0F) return 0
        val r = min.roundToInt() % 5
        val diff = if (r > 0) r + 10 else 15
        val result = min.roundToInt() - diff
        return if (result < 0) 0 else result
    }

    fun measurement(type: MeasurementsUIModel.Type, index: Int): Float? {
        return measurements(type).getOrNull(index)?.toFloat()
    }

    fun measurementStr(type: MeasurementsUIModel.Type, index: Int): String? {
        return measurement(type, index)?.convertWithOneDecimal()
    }

    fun measurementGraph(type: MeasurementsUIModel.Type, index: Int): Float? {
        val measurement = measurement(type, index) ?: return null
        val minMidMax = measurementsMinMidMax(type)
        val divider = (minMidMax.third - minMidMax.first)
        if (divider == 0) return null
        return (measurement - minMidMax.first) / divider
    }

    fun isEmptyMeasurement(type: MeasurementsUIModel.Type, index: Int): Boolean {
        return measurementGraph(type, index) == null
    }

    private fun lastMeasurement(type: MeasurementsUIModel.Type): Float {
        return measurements(type).lastOrNull() ?: 0F
    }

    fun lastMeasurementStr(type: MeasurementsUIModel.Type): String {
        return lastMeasurement(type).convertWithOneDecimal()
    }

    private fun firstMeasurement(type: MeasurementsUIModel.Type): Float {
        return measurements(type).firstOrNull() ?: 0F
    }

    fun isEmptyDiffMeasurement(type: MeasurementsUIModel.Type): Boolean {
        return diffMeasurement(type) == null
    }

    fun diffMeasurement(type: MeasurementsUIModel.Type): String? {
        return diffMeasurementFloat(type)?.convertWithOneDecimal()
    }

    fun diffMeasurementFloat(type: MeasurementsUIModel.Type): Float? {
        val lastMeasurement = lastMeasurement(type)
        if (lastMeasurement <= 0F) return null
        var firstMeasurement = firstMeasurement(type)
        if (firstMeasurement <= 0F) firstMeasurement = 0F
        if (lastMeasurement == firstMeasurement) return null
        return lastMeasurement - firstMeasurement
    }

    fun getString(value: Float): String {
        val formatted = value.convertWithOneDecimal()
        return if (value > 0F) "+ $formatted" else formatted.replace("-", "- ")
    }

    val isEmptyWeeks: Boolean
        get() = weeks.isNullOrEmpty()

    fun week(index: Int): Week {
        return weeks?.getOrNull(index) ?: Week()
    }

    fun statisticWeek(index: Int): StatisticsWeek {
        return statistics_weeks?.getOrNull(index) ?: StatisticsWeek()
    }

    fun statWeeksDaysInMonth(index: Int): String {
        return statisticWeek(index).days.let {
            val first = it.firstOrNull()
            val last = it.lastOrNull()
            if (first != null && last != null) {
                "${first.day_in_month}-${last.day_in_month}"
            } else ""
        }
    }

    fun statWeeksFullDays(index: Int): String {
        return statisticWeek(index).days.let {
            val first = it.firstOrNull()
            val last = it.lastOrNull()
            if (first != null && last != null) {
                val format = SimpleDateFormat("d MMMM", Locale.forLanguageTag("ru"))
                val calendar = Calendar.getInstance()

                calendar.set(Calendar.MONTH, first.month - 1)
                calendar.set(Calendar.DAY_OF_MONTH, first.day_in_month)
                val firstDate = format.format(calendar.time)

                calendar.set(Calendar.MONTH, last.month - 1)
                calendar.set(Calendar.DAY_OF_MONTH, last.day_in_month)
                val lastDate = format.format(calendar.time)

                "$firstDate - $lastDate"
            } else ""
        }
    }
}