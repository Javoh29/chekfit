package fit.lerchek.data.domain.uimodel.marathon

import fit.lerchek.ui.feature.marathon.today.adapters.TrackerAdapter

abstract class TrackerItem(var type: Type) {
    enum class Type(val itemViewType: Int) {
        LineTracker(TrackerAdapter.ITEM_VIEW_TYPE_LINE_TRACKER),
        WaterTracker(TrackerAdapter.ITEM_VIEW_TYPE_WATER_TRACKER),
        StepsTracker(TrackerAdapter.ITEM_VIEW_TYPE_STEPS_TRACKER),
        SleepTracker(TrackerAdapter.ITEM_VIEW_TYPE_SLEEP_TRACKER),
        SwitchFit(TrackerAdapter.ITEM_VIEW_TYPE_SWITCH_FIT),
        SettingTracker(TrackerAdapter.ITEM_VIEW_TYPE_SETTING_TRACKER),
        ChangeBtn(TrackerAdapter.ITEM_VIEW_TYPE_CHANGE_BTN)
    }
}