package fit.lerchek.data.domain.uimodel.marathon

class TodayWaterItem(
    val dimension: Int,
    val goal: Int,
    var value: Int
) : TodayItem(Type.WaterTracker)