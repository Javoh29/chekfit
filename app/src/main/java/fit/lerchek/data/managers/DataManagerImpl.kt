package fit.lerchek.data.managers

import android.content.Context
import com.google.gson.Gson
import fit.lerchek.data.api.model.*

class DataManagerImpl(private val context: Context, private val gson: Gson) : DataManager {
    companion object {
        // TAGS
        private const val TAG_NIGHT_THEME = "tag_night_theme"
        private const val TAG_AUTH_TOKEN = "tag_auth_token"
        private const val TAG_PROFILE = "tag_profile"
        private const val TAG_PROFILE_PARAMS = "tag_profile_params"
        private const val TAG_APP_MENU = "tag_app_menu"
        private const val TAG_FCM_TOKEN = "fcm_token"
        private const val TAG_MARATHON_ID = "tag_marathon_id"
        private const val TAG_MARATHON_IMAGE_URL = "tag_marathon_image_url"
        private const val TAG_SWITCH_FIT = "tag_switch_fit"

        // Filename
        private const val PREFERENCES_FILE_NAME = "preference_file.lerchek"
    }

    private val prefs = context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)

    private val userCacheTags = listOf(
        TAG_AUTH_TOKEN, TAG_PROFILE, TAG_APP_MENU, TAG_MARATHON_ID, TAG_FCM_TOKEN
    )

    private var currentRawUserInfo: Profile? = null

    private var currentRawUserParam: Params? = null

    private var currentMarathonId: Int? = null

    private var currentMarathonImageUrl: String? = null

    private var currentRawAppMenu: AppMenu? = null

    private var fcmToken: String? = null

    //DataManager's methods
    override fun saveIsNightMode(isNightMode: Boolean) {
        putBoolean(TAG_NIGHT_THEME, isNightMode)
    }

    override fun isStoredNightMode() = if (contains(TAG_NIGHT_THEME))
        getBoolean(TAG_NIGHT_THEME, false) else null


    override fun saveAuthToken(authToken: String?) {
        if (authToken.isNullOrEmpty()) {
            remove(TAG_AUTH_TOKEN)
        } else {
            putString(TAG_AUTH_TOKEN, authToken)
        }
    }

    override fun saveProfileInfo(profile: Profile?) {
        currentRawUserInfo = profile
        if (profile == null) {
            remove(TAG_PROFILE)
        } else {
            putString(TAG_PROFILE, gson.toJson(profile))
        }
    }

    override fun getProfileInfo(): Profile? {
        return currentRawUserInfo ?: try {
            gson.fromJson(getString(TAG_PROFILE), Profile::class.java)
        } catch (_:Throwable){
            null
        }
    }

    override fun getProfileParam(): Params? {
        return currentRawUserParam ?: try {
            gson.fromJson(getString(TAG_PROFILE_PARAMS), Params::class.java)
        } catch (_:Throwable){
            null
        }
    }


    override fun saveProfileParam(params: Params?) {
        currentRawUserParam = params
        if (params == null) {
            remove(TAG_PROFILE_PARAMS)
        } else {
            putString(TAG_PROFILE_PARAMS, gson.toJson(params))
        }
    }

    override fun saveMarathonLocalMenu(appMenu: AppMenu?) {
        currentRawAppMenu = appMenu
        if (appMenu == null) {
            remove(TAG_APP_MENU)
        } else {
            putString(TAG_APP_MENU, gson.toJson(appMenu))
        }
    }

    override fun getMarathonLocalMenu(): AppMenu? {
        return currentRawAppMenu ?: try {
            gson.fromJson(getString(TAG_APP_MENU), AppMenu::class.java)
        } catch (_:Throwable){
            null
        }
    }

    override fun getCurrentMarathonId(): Int? {
        return currentMarathonId ?: try {
            val res = getInt(TAG_MARATHON_ID, -1)
            if(res!=-1) res else null
        } catch (_:Throwable){
            null
        }
    }

    override fun saveCurrentMarathonId(marathonId : Int?) {
        currentMarathonId = marathonId
        if (marathonId == null) {
            remove(TAG_MARATHON_ID)
        } else {
            putInt(TAG_MARATHON_ID, marathonId)
        }
    }

    override fun getCurrentMarathonImageUrl(): String? {
        return currentMarathonImageUrl ?: try {
            getString(TAG_MARATHON_IMAGE_URL)
        } catch (_:Throwable){
            null
        }
    }

    override fun saveCurrentMarathonImageUrl(marathonImageUrl : String?) {
        currentMarathonImageUrl = marathonImageUrl
        if (marathonImageUrl == null) {
            remove(TAG_MARATHON_IMAGE_URL)
        } else {
            putString(TAG_MARATHON_IMAGE_URL, marathonImageUrl)
        }
    }

    override fun getAuthToken() = getString(TAG_AUTH_TOKEN)

    override fun clearUserData() {
        userCacheTags.forEach { if(contains(it)) remove(it) }
    }

    override fun getStringFromResources(resId: Int): String {
        return context.getString(resId)
    }

    override fun getStringArrayFromResources(resId: Int): Array<String> {
        return context.resources.getStringArray(resId)
    }

    override fun saveSwitchFit(value: Boolean) {
        putBoolean(TAG_SWITCH_FIT, value)
    }

    override fun getSwitchFit() = getBoolean(TAG_SWITCH_FIT, defValue = false)

    override fun saveFCMToken(token: String?) {
        fcmToken = token
        if (token.isNullOrEmpty()) {
            remove(TAG_FCM_TOKEN)
        } else {
            putString(TAG_FCM_TOKEN, token)
        }
    }

    override fun getFCMToken(): String? {
        return fcmToken ?: try {
            val res = getString(TAG_FCM_TOKEN)
            if(res.isNullOrEmpty()) null else res
        } catch (_:Throwable){
            null
        }
    }

    // Shared Preferences Methods
    private fun contains(key: String): Boolean {
        return prefs.contains(key)
    }

    private fun remove(key: String) {
        prefs.edit().remove(key).apply()
    }

    private fun putBoolean(key: String, value: Boolean) {
        prefs.edit().putBoolean(key, value).apply()
    }

    private fun getBoolean(key: String, defValue: Boolean): Boolean {
        return prefs.getBoolean(key, defValue)
    }

    private fun putInt(key: String, value: Int) {
        prefs.edit().putInt(key, value).apply()
    }

    private fun getInt(key: String, defValue: Int): Int {
        return prefs.getInt(key, defValue)
    }

    private fun putLong(key: String, value: Long) {
        prefs.edit().putLong(key, value).apply()
    }

    private fun getLong(key: String, defValue: Long): Long {
        return prefs.getLong(key, defValue)
    }

    private fun putString(key: String, value: String) {
        prefs.edit().putString(key, value).apply()
    }

    private fun getString(key: String): String? {
        return prefs.getString(key, null)
    }


}