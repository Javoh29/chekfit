package fit.lerchek.data.managers

import android.support.annotation.StringRes
import androidx.annotation.ArrayRes
import fit.lerchek.data.api.model.AppMenu
import fit.lerchek.data.api.model.Params
import fit.lerchek.data.api.model.Profile

interface DataManager {
    fun saveIsNightMode(isNightMode: Boolean)
    fun isStoredNightMode(): Boolean?
    fun getAuthToken(): String?
    fun saveAuthToken(authToken: String?)
    fun saveProfileInfo(profile: Profile?)
    fun getProfileInfo(): Profile?
    fun getProfileParam():Params?
    fun saveProfileParam(params: Params?)
    fun saveMarathonLocalMenu(appMenu: AppMenu?)
    fun getMarathonLocalMenu(): AppMenu?
    fun saveCurrentMarathonId(marathonId : Int?)
    fun getCurrentMarathonId(): Int?
    fun saveCurrentMarathonImageUrl(marathonImageUrl : String?)
    fun getCurrentMarathonImageUrl(): String?
    fun clearUserData()
    fun saveFCMToken(token:String?)
    fun getFCMToken():String?
    fun getStringFromResources(@StringRes resId: Int) : String
    fun getStringArrayFromResources(@ArrayRes resId : Int): Array<String>
    fun saveSwitchFit(value: Boolean)
    fun getSwitchFit(): Boolean

}
