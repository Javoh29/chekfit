package fit.lerchek.data.repository.user

import android.graphics.Bitmap
import android.text.SpannableStringBuilder
import fit.lerchek.R
import fit.lerchek.common.rx.SchedulerProvider
import fit.lerchek.common.util.Constants
import fit.lerchek.common.util.DateFormatUtils
import fit.lerchek.data.api.UserRest
import fit.lerchek.data.api.message.base.BaseResponse
import fit.lerchek.data.api.message.forgotpwd.CheckCodeForgotPwdRequest
import fit.lerchek.data.api.message.forgotpwd.ForgotPwdRequest
import fit.lerchek.data.api.message.forgotpwd.ResetPwdRequest
import fit.lerchek.data.api.message.login.LoginRequest
import fit.lerchek.data.api.message.marathon.*
import fit.lerchek.data.api.model.Body
import fit.lerchek.data.api.model.Meal
import fit.lerchek.data.api.model.VimeoModel
import fit.lerchek.data.domain.converter.UserDataConverter
import fit.lerchek.data.domain.exceptions.CommonException
import fit.lerchek.data.domain.exceptions.WithdrawError
import fit.lerchek.data.domain.uimodel.MenuItemUIModel
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel
import fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel
import fit.lerchek.data.managers.DataManager
import fit.lerchek.ui.util.FileUtils
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.concurrent.thread

@Singleton
class UserRepositoryImpl @Inject constructor(
    private val rest: UserRest,
    private val dataManager: DataManager,
    private val userDataConverter: UserDataConverter,
    private val schedulerProvider: SchedulerProvider
) : UserRepository {

    override fun login(email: String, password: String): Single<Boolean> =
        rest.login(LoginRequest(email, password, dataManager.getFCMToken()))
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                dataManager.saveProfileInfo(it.result?.profile)

                userDataConverter.processLoginResponseAndGetLogin(it)?.apply {
                    dataManager.saveAuthToken(this)
                }
                isLoggedIn()
            }

    override fun logout() = Single.create<Boolean> { emitter ->
        try {
            rest.removePushToken(PushRequest(dataManager.getFCMToken() ?: "")).execute()
            emitter.onSuccess(rest.logout().execute().body()?.success == true)
        } catch (t: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(CommonException().apply {
                    message = "UserRepository.logout"
                })
            }
        }
    }.subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            if (it) {
                dataManager.clearUserData()
            }
            !isLoggedIn()
        }


    override fun isCurrentProfileMale() = Single.create<Boolean> { emitter ->
        dataManager.getProfileInfo().let {
            if (it != null) {
                emitter.onSuccess(it.sex?.isMale() == true)
            } else {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.isCurrentProfileMale logged out"
                    })
                }
            }
        }
    }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())

    override fun checkIsInsideMarathon() = Single.create<Boolean> { emitter ->
        try {
            emitter.onSuccess(dataManager.getMarathonLocalMenu() != null)
        } catch (e: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(CommonException().apply {
                    message = "UserRepository.checkIsInsideMarathon == null"
                })
            }
        }
    }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())

    override fun processWithdraw(
        amount: Int, balance: Int, phone: String
    ) = Single.create<Boolean> { emitter ->
        try {
            val rawPhone = phone.filter { it.isDigit() }
            if (rawPhone.length != 11) {
                throw WithdrawError().apply {
                    phoneError = "Проверьте введенные данные"
                }
            }

            val result = rest.withdraw(WithdrawRequest(amount, rawPhone)).execute()
            if (result.isSuccessful && result.body()?.success == true) {
                emitter.onSuccess(true)
            } else {
                throw WithdrawError().apply {
                    amountError = result.body()?.getErrorContent() ?: Constants.UNKNOWN_ERROR
                }

            }

            emitter.onSuccess(dataManager.getMarathonLocalMenu() != null)
        } catch (e: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(e)
            }
        }
    }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())

    override fun isLoggedIn() = dataManager.getAuthToken() != null

    override fun loadMarathons() = rest.getMarathons()
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertMarathonsToUIModel(processResponse(it), dataManager)
        }

    override fun loadTestContentDetails() = rest.getMarathonProfileDetails(getCurrentMarathonId())
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertTestDetailsToUIModel(processResponse(it), dataManager)
        }

    override fun updateTestContentDetails(
        content: List<TestItem>
    ) = Single.create<List<TestItem>> { emitter ->
        try {
            val request = userDataConverter.createUpdateTestApiModel(
                content = content.filterIsInstance<TestItemUIModel>(),
                marathon = getCurrentMarathonId(),
                goalsText = (content.last() as TestFooterItem).textValue?.get()
            )
            val marathonDetails = processResponse(
                rest.updateMarathonProfileTestDetails(
                    marathon = getCurrentMarathonId(),
                    completeTestRequest = request
                ).execute().body()!!
            )
            emitter.onSuccess(
                userDataConverter.convertTestDetailsToUIModel(marathonDetails, dataManager, true)
            )
        } catch (e: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(CommonException().apply {
                    if (message.isEmpty()) {
                        message = "UserRepository.updateTestContentDetails"
                    }
                })
            }
        }
    }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())


    override fun loadMarathonDetails(marathonId: Int, mapFit: HashMap<String, Int>?) =
        Single.create<MarathonDetailsUIModel> { emitter ->
            try {
                val searchId = if (marathonId == -1) dataManager.getCurrentMarathonId()
                    ?: marathonId else marathonId
                val marathonDetails = rest.getMarathonDetails(searchId).execute().body()
                val checkVoteResult = rest.getMarathonVote(searchId).execute().body()
                mapFit?.forEach { (k, v) ->
                    when (k) {
                        "water" -> rest.addCountWaterPutTracker(
                            searchId,
                            UpdateTrackerRequest(null, v)
                        ).execute().body()
                        "step" -> rest.addCountStepPutTracker(
                            searchId,
                            UpdateTrackerRequest(null, v)
                        ).execute().body()
                        "dream" -> rest.addCountDreamPutTracker(
                            searchId,
                            UpdateTrackerRequest(null, v)
                        ).execute().body()
                    }
                }
                val trackers = rest.getTodayTrackers(searchId).execute().body()
                val imageUrl =
                    userDataConverter.convertImageToUIModel(marathonDetails?.result?.image, false)
                if (imageUrl.isNotEmpty()) {
                    dataManager.saveCurrentMarathonImageUrl(imageUrl)
                }
                dataManager.saveCurrentMarathonId(searchId)
                dataManager.saveMarathonLocalMenu(marathonDetails?.result?.app_menu)
                emitter.onSuccess(
                    MarathonDetailsUIModel(
                        isRunning = marathonDetails?.success == true,
                        bottomNavigationVisible = marathonDetails?.success == true,
                        todayItems = userDataConverter.getTodayItemsForMarathon(
                            profileInfo = dataManager.getProfileInfo(),
                            marathonDetails = marathonDetails,
                            checkVoteResult = checkVoteResult,
                            trackers = trackers?.result
                        )
                    )
                )
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.loadMarathonDetails"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())

    override fun loadMarathonActivityCalendar(marathonId: Int) =
        Single.create<ActivityCalendarUIModel> { emitter ->
            try {
                val searchId = if (marathonId == -1) dataManager.getCurrentMarathonId()
                    ?: marathonId else marathonId
                val activityCalendar = rest.getMarathonActivityCalendar(searchId).execute().body()
                dataManager.saveCurrentMarathonId(searchId)
                val tasks = userDataConverter.getCalendarTasks(
                    activityCalendar?.result?.tasks ?: mapOf(),
                    activityCalendar?.result?.completed_task ?: mapOf()
                )
                val currentDay = activityCalendar?.result?.current_day ?: 0
                val calendar = activityCalendar?.result?.calendar ?: mapOf()
                emitter.onSuccess(
                    ActivityCalendarUIModel(
                        isMale = dataManager.getProfileInfo()?.sex?.isMale() == true,
                        profileName = dataManager.getProfileInfo()?.first_name ?: "",
                        currentDate = DateFormatUtils.formatDateSimpleWithDOW(Date()) ?: "",
                        currentMarathonDay = activityCalendar?.result?.current_marathon_day ?: 0,
                        tasks = tasks,
                        calendar = userDataConverter.getActivityCalendarItems(
                            weekdays = dataManager.getStringArrayFromResources(R.array.weekdays),
                            calendar = calendar.toSortedMap(compareBy<String> { it.length }.thenBy { it }),
                            tasks = tasks,
                            currentDay = currentDay,
                        ), currentDay = currentDay
                    )
                )
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.loadMarathonActivityCalendar"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())

    override fun loadFoodDetails(
        filterUIModel: FoodFilterUIModel?,
        needApiSync: Boolean,
        foodDetailsUIModel: FoodDetailsUIModel?
    ): Single<FoodDetailsUIModel> {
        val selectedWeekId = filterUIModel?.getSelectedWeek()?.id
        val selectedPlanId = filterUIModel?.getNutritionPlan()?.id
        val selectedDay = filterUIModel?.getCurrentDay()?.dayNumber
        return if (needApiSync) {
            val request = (if (selectedPlanId == null) rest.getFoodDetails(
                getCurrentMarathonId()
            ) else rest.updateNutritionPlan(
                getCurrentMarathonId(),
                userDataConverter.generateNutritionPlanUpdateApiRequest(selectedPlanId)
            ))
            request.subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .map {
                    userDataConverter.convertFoodDetailsToUIModel(
                        foodDetails = processResponse(it)!!,
                        selectedWeekId = selectedWeekId,
                        selectedPlanId = selectedPlanId,
                        selectedDay = selectedDay
                    )
                }
        } else Single.create<FoodDetailsUIModel> { emitter ->
            try {
                emitter.onSuccess(
                    userDataConverter.updateFoodFilterUIModelWithSelectedValues(
                        foodDetailsUIModel = foodDetailsUIModel,
                        selectedWeekId = selectedWeekId,
                        selectedPlanId = selectedPlanId,
                        selectedDay = selectedDay
                    )
                )
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.loadFoodDetails == null"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
    }

    override fun loadFavorites(): Single<FavoriteUIModel> {
        return rest.getFavorites(getCurrentMarathonId()).subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui()).map {
                val list: ArrayList<RecipeUIModel> = arrayListOf()
                val listMeals: HashMap<String, Meal> = HashMap()
                list.addAll(it.result!!.recipes.map { r ->
                    userDataConverter.convertFavRecipeToUIModel(
                        r
                    )
                })
                it.result!!.meals.forEach { (k, v) ->
                    list.forEach { r ->
                        if (r.mealId == v.id) {
                            listMeals[k] = v
                        }
                    }
                }
                FavoriteUIModel(listMeals, list)
            }
    }

    override fun mealFilter(mealId: Int, list: List<RecipeUIModel>): Single<List<RecipeUIModel>> {
        return Single.create<List<RecipeUIModel>> { emitter ->
            try {
                emitter.onSuccess(list.filter { it.mealId == mealId })
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.mealFilter == null"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
    }

    override fun loadRecipeDetails(
        recipeId: Int,
        baseRecipeId: Int,
        replacementAllowed: Boolean,
        isExtra: Boolean
    ) = (if (isExtra) rest.getExtraRecipeDetails(
        marathon = getCurrentMarathonId(),
        recipe = recipeId
    ) else rest.getRecipeDetails(
        marathon = getCurrentMarathonId(),
        recipe = recipeId
    )).subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertRecipeToUIModel(
                processResponse(it)!!.recipe,
                replacementAllowed
            )
        }

    override fun loadRecipeReplacements(recipeId: Int, baseRecipeId: Int) =
        rest.getRecipeReplacements(
            marathon = getCurrentMarathonId(),
            recipe = recipeId,
            base_recipe_id = baseRecipeId
        ).subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                val response = processResponse(it)!!
                userDataConverter.convertRecipeReplacementsToUIModel(
                    recipe = response.recipe,
                    replacements = response.replacements
                )
            }

    override fun loadExtraRecipes(
        filter: ExtraRecipesFilterUIModel?,
        cache: ExtraRecipesUIModel?
    ): Single<ExtraRecipesUIModel> {
        return if (filter == null) {
            rest.getExtraRecipes(
                marathon = getCurrentMarathonId()
            ).subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .map {
                    val response = processResponse(it)!!
                    userDataConverter.convertExtraRecipesToUIModel(
                        recipes = response.recipes,
                        meals = response.meals
                    )
                }
        } else Single.create<ExtraRecipesUIModel> { emitter ->
            try {
                emitter.onSuccess(
                    userDataConverter.convertExtraRecipesToUIModel(
                        cache = cache,
                        filter = filter,
                    )
                )
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.loadExtraRecipes"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
    }

    override fun loadCdfc(filter: CdfcFilterUIModel?, cache: CdfcUIModel?): Single<CdfcUIModel> {
        return if (filter == null) {
            rest.getCdfc(
                marathon = getCurrentMarathonId()
            ).subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .map {
                    val response = processResponse(it)!!
                    userDataConverter.convertCdfcToUIModel(
                        products = response.ingredients,
                        types = response.categories
                    )
                }
        } else Single.create<CdfcUIModel> { emitter ->
            try {
                emitter.onSuccess(
                    userDataConverter.convertCdfcToUIModel(
                        cache = cache,
                        filter = filter,
                    )
                )
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.loadCdfc"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
    }

    override fun replaceRecipe(
        recipeId: Int,
        baseRecipeId: Int,
        replacementId: Int,
        nutritionPlanId: Int,
        dayNumber: Int
    ) = rest.replaceRecipe(
        marathon = getCurrentMarathonId(),
        recipe = baseRecipeId,
        request = ReplaceRecipeRequest(
            recipeId = recipeId,
            marathonId = getCurrentMarathonId(),
            replacement_recipe_id = replacementId,
            nutrition_plan_id = nutritionPlanId,
            day_number = dayNumber
        )
    ).subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .flatMapCompletable {
            processResponse(it)
            Completable.complete()
        }

    override fun loadWorkoutDetails(workoutId: Int) = rest.getWorkoutDetails(
        getCurrentMarathonId(), workoutId
    ).subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertWorkoutToUIModel(processResponse(it)!!)
        }

    override fun loadWorkoutsDetails(
        filterUIModel: WorkoutFilterUIModel?,
        needApiSync: Boolean,
        workoutDetailsUIModel: WorkoutDetailsUIModel?,
        marathonId: Int?
    ): Single<WorkoutDetailsUIModel> {
        val selectedWeekId = filterUIModel?.getSelectedWeek()?.id
        val selectedLevelId = filterUIModel?.getLevel()?.id
        val selectedDay = filterUIModel?.getCurrentDay()?.dayNumber
        val currentMarathonId = marathonId ?: getCurrentMarathonId()

        val request = (if (selectedLevelId == null) rest.getWorkoutsDetails(
            currentMarathonId
        ) else rest.updateWorkoutLevel(
            currentMarathonId,
            userDataConverter.generateWorkoutLevelUpdateApiRequest(selectedLevelId)
        ))
        return if (needApiSync) {
            request.subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .map {
                    userDataConverter.convertWorkoutDetailsToUIModel(
                        workoutDetails = processResponse(it)!!,
                        selectedWeekId = selectedWeekId,
                        selectedLevelId = selectedLevelId,
                        selectedDay = selectedDay,
                        isMale = dataManager.getProfileInfo()?.sex?.isMale() == true
                    )
                }
        } else Single.create<WorkoutDetailsUIModel> { emitter ->
            try {
                emitter.onSuccess(
                    userDataConverter.updateWorkoutFilterUIModelWithSelectedValues(
                        workoutDetailsUIModel = workoutDetailsUIModel,
                        selectedWeekId = selectedWeekId,
                        selectedLevelId = selectedLevelId,
                        selectedDay = selectedDay
                    )
                )
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.loadWorkoutsDetails == null"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
    }

    override fun loadFoodIngredients(weekId: Int?) = rest.getFoodIngredients(
        getCurrentMarathonId(), weekId
    ).subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertFoodIngredientsToUIModel(it, weekId)
        }

    override fun updateIngredientsList(
        weekId: Int,
        ingredients: List<TaskUIModel>
    ) = rest.checkFoodIngredients(
        marathon = getCurrentMarathonId(),
        request = userDataConverter.createUpdateIngredientsRequest(weekId, ingredients)
    ).subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            it.success
        }

    override fun toggleFoodIngredient(weekId: Int, ingredient: TaskUIModel): Single<Boolean> {
        val marathonId = getCurrentMarathonId()
        val ingredientId = ingredient.id
        return (if (!ingredient.checked)
            rest.unCheckFoodIngredient(
                marathon = marathonId,
                ingredient = ingredientId,
                week = weekId
            ) else
            rest.checkFoodIngredient(
                marathon = marathonId,
                ingredient = ingredientId,
                week = weekId
            )).subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                it.success
            }
    }

    override fun toggleFavorite(isDelete: Boolean, recipeId: Int) =
        Single.create<Boolean> { emitter ->
            try {
                val call = if (isDelete) rest.deleteFavorite(
                    getCurrentMarathonId(),
                    recipeId
                ) else rest.setFavorite(getCurrentMarathonId(), recipeId)
                val result = call.execute()
                if (result.isSuccessful && result.body()?.success == true) {
                    emitter.onSuccess(true)
                } else {
                    throw CommonException().apply {
                        message = result.body()?.getErrorContent() ?: Constants.UNKNOWN_ERROR
                    }
                }
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())

    override fun loadFaq(alias: String) =
        rest.getFaq(getCurrentMarathonId(), alias)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                userDataConverter.convertFaqToUIModel(it)
            }

    override fun loadDynamicContent(alias: String) = rest.getPaPage(alias)
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertPaPageToUIModel(processResponse(it)!!)
        }

    override fun generateDynamicContent(rawContent: String) =
        Single.create<SpannableStringBuilder> { emitter ->
            try {
                emitter.onSuccess(
                    userDataConverter.createDescriptionDataSpannableString(
                        rawContent = rawContent
                    )
                )
            } catch (e: Throwable) {
                if (!emitter.isDisposed) {
                    emitter.onError(CommonException().apply {
                        message = "UserRepository.generateDynamicContent"
                    })
                }
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())

    override fun loadReports() = rest.getMarathonReports(getCurrentMarathonId())
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertReportsToUIModel(processResponse(it)!!, dataManager)
        }

    override fun saveReport(
        reports: ReportsUIModel, reportText: String
    ) = rest.saveMarathonReport(
        marathon = getCurrentMarathonId(),
        request = SaveReportRequest(
            week = reports.getSelectedWeek()?.id ?: 0,
            text = reportText
        )
    ).subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            processResponse(it)!!
            reports.getReport()?.reportText = reportText
            reports
        }

    override fun getVoting() = rest.getVoting(getCurrentMarathonId())
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertVotingToUIModel(processResponse(it)!!, dataManager)
        }

    override fun getChat() = rest.getChat(getCurrentMarathonId())
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertChatToUIModel(processResponse(it)!!, dataManager)
        }

    override fun sendMessage(userId: Int, text: String) =
        rest.sendChatMessage(marathon = getCurrentMarathonId(), request = SendMessageRequest(text))
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                processResponse(it)!!.map { message ->
                    userDataConverter.convertChatMessageToUIModel(message, userId, dataManager)
                }
            }


    override fun vote(
        items: List<VotingItem>,
        voteUIModel: VotingItemUIModel
    ) = Single.create<List<VotingItem>> { emitter ->
        try {
            val header = items.first { it is VotingHeaderUIModel } as VotingHeaderUIModel
            val isLike = voteUIModel.isLiked.not()
            if (!header.isActive || (header.availableVotes == 0 && isLike)) {
                emitter.onSuccess(items)
            } else {
                val request = VoteRequest(voteUIModel.id)
                val call = if (isLike) rest.like(getCurrentMarathonId(), header.voteId, request)
                else rest.dislike(getCurrentMarathonId(), header.voteId, request)
                val result = call.execute()
                if (result.isSuccessful && result.body()?.success == true) {
                    val apiResponseModel = result.body()?.result!!
                    header.availableVotes = apiResponseModel.available_votes
                    voteUIModel.isLiked = isLike
                    if (isLike) {
                        header.votes++
                        voteUIModel.votes++
                    } else {
                        header.votes--
                        voteUIModel.votes--
                    }
                    emitter.onSuccess(items.map { item ->
                        if (item is VotingHeaderUIModel) {
                            header
                        } else if (item is VotingItemUIModel && item.id == voteUIModel.id) {
                            voteUIModel
                        } else {
                            item
                        }
                    })
                } else {
                    throw CommonException().apply {
                        message = result.body()?.getErrorContent() ?: Constants.UNKNOWN_ERROR
                    }
                }
            }
        } catch (e: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(e)
            }
        }
    }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.io())

    override fun toggleTaskCompletion(task: TaskUIModel, currentDay: Int): Single<Boolean> {
        val request = TaskToggleRequest(currentDay)
        val marathonId = getCurrentMarathonId()
        return (if (task.checked) rest.completeTask(
            request = request,
            marathon = marathonId,
            task = task.id
        ) else rest.restoreTask(
            request = request,
            marathon = marathonId,
            task = task.id
        )).subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                it.success
            }

    }

    override fun loadMenuItems() = Single.create<List<MenuItemUIModel>> { emitter ->
        try {
            emitter.onSuccess(
                userDataConverter.generateMenuItemsByUserDataCaches(
                    appMenu = dataManager.getMarathonLocalMenu()
                )
            )
        } catch (e: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(CommonException().apply {
                    message = "UserRepository.loadMenuItems == null"
                })
            }
        }
    }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.computation())

    override fun loadProfileDetails() = rest.getProfileDetails()
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            dataManager.saveProfileInfo(it.result?.customer?.profile)
            it.result?.params?.let {
                dataManager.saveProfileParam(it)
            }

            userDataConverter.convertProfileDetailsResponseToUIModel(it)
        }

    override fun loadReferralDetails() = rest.getReferralDetails()
        .subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())
        .map {
            userDataConverter.convertReferralDetailsToUIModel(processResponse(it)!!)
        }

    override fun uploadPhotosToServer(
        type: String,
        week: Int,
        cropped: Bitmap?,
        originalBitmap: Bitmap?
    ) = Single.create<Boolean> { emitter ->
        try {
            emitter.onSuccess(
                uploadBitmap(cropped!!, type, week, false) &&
                        uploadBitmap(originalBitmap!!, type, week, true)

            )
        } catch (e: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(CommonException().apply {
                    message = e.message ?: "UserRepository.uploadPhotosToServer"
                })
            }
        }
    }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.io())

    private fun uploadBitmap(
        bitmap: Bitmap,
        type: String,
        week: Int,
        isOriginal: Boolean
    ): Boolean {
        val reqType = type + if (isOriginal) "_original" else ""
        val file = FileUtils.buildImageBodyPart("photo", reqType, bitmap)
        return processResponse(
            rest.uploadPhoto(
                marathon = getCurrentMarathonId(),
                photo = file,
                type = reqType.toRequestBody(),
                week = week.toString().toRequestBody()
            ).execute().body()!!
        ) != null
    }

    override fun loadProfilePersonalInfo(): Single<ProfilePersonalInfoUIModel> {
        if (dataManager.getProfileInfo() == null || dataManager.getProfileParam() == null) {
            return loadProfileDetailsInternalRx()
        } else {
            return Single.create<ProfilePersonalInfoUIModel> { emitter ->
                try {
                    emitter.onSuccess(
                        userDataConverter.convertProfilePersonalInfoUIModel(
                            profile = dataManager.getProfileInfo()!!
                        )
                    )
                } catch (e: Throwable) {
                    if (!emitter.isDisposed) {
                        emitter.onError(CommonException().apply {
                            message = "UserRepository.loadMenuItems == null"
                        })
                    }
                }
            }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.computation())
        }

    }

    private fun loadProfileDetailsInternalRx(): Single<ProfilePersonalInfoUIModel> =
        rest.getProfileDetails()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                dataManager.saveProfileInfo(it.result?.customer?.profile)

                it.result?.params?.let { params ->
                    dataManager.saveProfileParam(params)
                }

                it.result?.customer?.profile?.let { it1 ->
                    userDataConverter.convertProfilePersonalInfoUIModel(
                        it1
                    )
                }
            }


    override fun clearAppMenuCache() {
        dataManager.saveMarathonLocalMenu(null)
        dataManager.saveCurrentMarathonId(null)
        dataManager.saveCurrentMarathonImageUrl(null)
    }

    override fun requestForgotPwdCode(email: String?): Single<Boolean> {
        return rest.requestForgotPwdCode(ForgotPwdRequest(email))
            .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map {
                userDataConverter.convertRequestPwdCode(it)
            }
    }

    override fun confirmForgotPwdCode(code: String?, email: String?): Single<Boolean> {
        return rest.confirmForgotPwdCode(CheckCodeForgotPwdRequest(code, email))
            .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map {
                userDataConverter.convertRequestPwdCode(it)
            }
    }

    override fun sendNewPwd(
        pwd: String?,
        pwdConfirm: String?,
        email: String?,
        code: String?
    ): Single<Boolean> {
        return rest.resetPwdCode(
            ResetPwdRequest(
                email,
                code,
                pwd,
                pwdConfirm,
                dataManager.getFCMToken()
            )
        )
            .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map {
                userDataConverter.processLoginResponseAndGetLogin(it)?.apply {
                    dataManager.saveAuthToken(this)
                }
                isLoggedIn()
            }

    }

    override fun getProfileParamsByMarathon(marathon: Int?): Single<ProfileBodyParamsUIModel>? {
        val marathonId = marathon ?: dataManager.getCurrentMarathonId() ?: return null
        return rest.getProfileParamsByMarathon(marathonId).subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui()).map { response ->
                userDataConverter.convertProfilePersonalParamUIModel(response)
            }
    }


    override fun updateBodyParamsByMarathon(
        marathon: Int?,
        reqProfileBodyParamsRequest: ProfileBodyParamsRequest
    ): Single<ProfileBodyParamsUIModel>? {
        val marathonId = marathon ?: dataManager.getCurrentMarathonId() ?: return null
        return rest.updateBodyParamsByMarathon(marathonId, reqProfileBodyParamsRequest)
            .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map { responce ->
                userDataConverter.convertProfilePersonalParamUIModel(responce)
            }
    }

    override fun updateProfileDetails(request: UpdateProfileRequest): Single<ProfilePersonalInfoUIModel> {
        return rest.updateProfileDetails(request)
            .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
            .map { response ->
                response.result?.original?.result?.customer?.profile?.let {
                    dataManager.saveProfileInfo(it)
                }
                response.result?.original?.result?.params?.let {
                    dataManager.saveProfileParam(it)
                }
                response.result?.original?.result?.customer?.profile?.let { profile ->
                    val profileUIModel =
                        userDataConverter.convertProfilePersonalInfoUIModel(profile)
                    profileUIModel.familyStatusParams = dataManager.getProfileParam()
                    profileUIModel
                }
            }
    }

    override fun changeCountTracker(
        request: UpdateTrackerRequest,
        type: String
    ): Single<TaskToggleResponse> {
        return when (type) {
            "water_add" -> rest.addCountWaterTracker(getCurrentMarathonId(), request)
                .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
                .map {
                    it
                }
            "steps_add" -> rest.addCountStepTracker(getCurrentMarathonId(), request)
                .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map {
                    it
                }
            "sleep_add" -> rest.addCountDreamTracker(getCurrentMarathonId(), request)
                .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map {
                    it
                }
            else -> rest.deleteCountWaterTracker(
                getCurrentMarathonId(),
                request
            )
                .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
                .map {
                    it
                }
        }
    }

    override fun changeGoalTracker(
        request: UpdateTrackerGoalRequest,
        type: String
    ): Single<TaskToggleResponse> {
        return when (type) {
            "water_set" -> rest.updateGoalWaterTracker(
                getCurrentMarathonId(),
                request
            )
                .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
                .map {
                    it
                }
            "steps_set" -> rest.updateGoalStepTracker(
                getCurrentMarathonId(),
                request
            )
                .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
                .map {
                    it
                }
            "sleep_set" -> rest.updateGoalSleepTracker(
                getCurrentMarathonId(),
                request
            ).subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map {
                it
            }
            "water_set_fit" -> {
                var goal: Int
                val trackers = rest.getTodayTrackers(getCurrentMarathonId()).execute().body()
                trackers?.result?.trackers?.water.let {
                    goal = if (it?.dimension == 1)
                        it.goal ?: 800
                    else it?.goal!! * it.dimension!!
                }
                rest.updateGoalWaterTracker(
                    getCurrentMarathonId(),
                    UpdateTrackerGoalRequest(goal, 1)
                )
            }
            else -> rest.updateGoalWaterTracker(getCurrentMarathonId(), request)
                .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
                .map {
                    it
                }
        }
    }

    override fun setGoogleFitData(request: FitDataRequest): Single<TaskToggleResponse> {
        return rest.setFitUpdate(getCurrentMarathonId(), request)
            .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map { it }
    }


    override fun getCurrentMarathonId() = dataManager.getCurrentMarathonId() ?: -1

    override fun getCurrentMarathonImageUrl() = dataManager.getCurrentMarathonImageUrl()

    override fun getCalorieDetails(): List<CalorieItem> {
        return userDataConverter.createCalorieUIModel(dataManager)
    }

    override fun getProgressForMarathon(marathon: Int?): Single<MarathonProgressUIModel>? {
        val marathonId = marathon ?: dataManager.getCurrentMarathonId() ?: return null
        return rest.getProgressForMarathon(marathonId).subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui()).map {
                it
                return@map userDataConverter.convertMarathonsProgressToUIModel(it, dataManager)
            }
    }

    override fun saveBodyProgressForMarathon(
        marathon: Int?,
        body: Body
    ): Single<TaskToggleResponse>? {
        val marathonId = marathon ?: dataManager.getCurrentMarathonId() ?: return null
        return rest.saveBodyProgressForMarathon(marathonId, body)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                processResponse(it)!!
                it
            }
    }

    override fun getVimeoConfig(videoId: String, h: String?): Single<VimeoModel> {
        return Single.create<VimeoModel> { emitter ->
            try {
                val url = URL("https://player.vimeo.com/video/$videoId/config${h ?: ""}")
                val urlConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
                try {
                    val inputStream: InputStream = BufferedInputStream(urlConnection.inputStream)
                    val buff = BufferedReader(InputStreamReader(inputStream))
                    val jsonObg = buff.readLine()
                    buff.close()
                    inputStream.close()
                    val mainObj = JSONObject(jsonObg)
                    val requestObj = mainObj.getJSONObject("request")
                    val filesObj = requestObj.getJSONObject("files")
                    val progArray = filesObj.getJSONArray("progressive")
                    val posterImg =
                        mainObj.getJSONObject("video").getJSONObject("thumbs").getString("640")
                    var videoUrl = ""
                    for (i in 0 until progArray.length()) {
                        if (progArray.getJSONObject(i).getInt("height") == 720
                        ) {
                            videoUrl = progArray.getJSONObject(i).getString("url")
                        }
                    }
                    emitter.onSuccess(VimeoModel(videoUrl, posterImg))
                } finally {
                    urlConnection.disconnect()
                }
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui())
    }

    override fun getDetailInfoTracker(
        type: String,
        period: String
    ): Single<DetailInfoTrackerUIModel> {
        return rest.getDetailInfoTracker(dataManager.getCurrentMarathonId()!!, type, period)
            .subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.ui()).map {
                processResponse(it)!!
                DetailInfoTrackerUIModel(
                    userDataConverter.getDetailInfoTrackerConvertToList(
                        type,
                        period,
                        it.result!!
                    )
                )
            }
    }

    override fun saveVideoLinkForMarathon(
        marathon: Int?,
        url: String,
        week: Int
    ): Single<TaskToggleResponse> {
        val marathonId = marathon ?: dataManager.getCurrentMarathonId() ?: return Single.just(
            TaskToggleResponse()
        )
        val video = VideoRequest(url, week)
        return rest.saveVideoLinkForMarathon(marathonId, video).subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .map {
                processResponse(it)!!
                it
            }

    }

    override fun updateFCMToken(token: String) {
        thread {
            try {
                rest.updatePushToken(PushRequest(token)).execute().body()?.success?.let {
                    saveFCMToken(token)
                }
            } catch (t: Throwable) {
                t.printStackTrace()
            }
        }
    }

    override fun saveFCMToken(token: String) {
        dataManager.saveFCMToken(token)
    }

    private fun <T> processResponse(it: BaseResponse<T>): T? {
        if (it.success) {
            return it.result
        } else {
            throw CommonException().apply {
                stringCode = it.string_code
                code = it.code ?: 0
                message = it.getErrorContent()
            }
        }
    }
}



