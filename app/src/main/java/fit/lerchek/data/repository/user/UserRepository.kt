package fit.lerchek.data.repository.user

import android.graphics.Bitmap
import android.text.SpannableStringBuilder
import fit.lerchek.data.api.message.marathon.*
import fit.lerchek.data.api.model.Body
import fit.lerchek.data.api.model.VimeoModel
import fit.lerchek.data.domain.uimodel.MenuItemUIModel
import fit.lerchek.data.domain.uimodel.chat.ChatItem
import fit.lerchek.data.domain.uimodel.chat.ChatUIModel
import fit.lerchek.data.domain.uimodel.marathon.*
import fit.lerchek.data.domain.uimodel.profile.ProfileBodyParamsUIModel
import fit.lerchek.data.domain.uimodel.profile.ProfileDetailsUIModel
import fit.lerchek.data.domain.uimodel.profile.ProfilePersonalInfoUIModel
import fit.lerchek.data.domain.uimodel.profile.ReferralDetailsUIModel
import fit.lerchek.ui.feature.marathon.FaqAdapter
import fit.lerchek.ui.feature.marathon.food.RecipeReplacementAdapter
import io.reactivex.Completable
import io.reactivex.Single

interface UserRepository {

    fun login(email: String, password: String): Single<Boolean>

    fun processWithdraw(amount: Int, balance: Int, phone: String): Single<Boolean>

    fun logout(): Single<Boolean>

    fun isLoggedIn(): Boolean

    fun isCurrentProfileMale(): Single<Boolean>

    fun loadMarathons(): Single<List<MarathonUIModel>>

    fun loadTestContentDetails(): Single<List<TestItem>>

    fun updateTestContentDetails(content: List<TestItem>): Single<List<TestItem>>

    fun loadMarathonDetails(marathonId: Int, mapFit: HashMap<String, Int>?): Single<MarathonDetailsUIModel>

    fun loadMarathonActivityCalendar(marathonId: Int): Single<ActivityCalendarUIModel>

    fun loadFoodDetails(
        filterUIModel: FoodFilterUIModel? = null,
        needApiSync: Boolean = false,
        foodDetailsUIModel: FoodDetailsUIModel? = null
    ): Single<FoodDetailsUIModel>

    fun loadFavorites(): Single<FavoriteUIModel>

    fun mealFilter(mealId: Int, list: List<RecipeUIModel>): Single<List<RecipeUIModel>>

    fun loadWorkoutsDetails(
        filterUIModel: WorkoutFilterUIModel? = null,
        needApiSync: Boolean = false,
        workoutDetailsUIModel: WorkoutDetailsUIModel? = null,
        marathonId: Int? = null
    ): Single<WorkoutDetailsUIModel>

    fun loadWorkoutDetails(workoutId: Int): Single<WorkoutUIModel>

    fun loadRecipeDetails(
        recipeId: Int, baseRecipeId: Int, replacementAllowed: Boolean, isExtra: Boolean
    ): Single<RecipeUIModel>

    fun loadRecipeReplacements(
        recipeId: Int,
        baseRecipeId: Int
    ): Single<List<RecipeReplacementAdapter.ReplacementItem>>

    fun loadExtraRecipes(
        filter: ExtraRecipesFilterUIModel? = null,
        cache: ExtraRecipesUIModel? = null
    ): Single<ExtraRecipesUIModel>

    fun loadCdfc(filter: CdfcFilterUIModel? = null, cache: CdfcUIModel? = null): Single<CdfcUIModel>

    fun replaceRecipe(
        recipeId: Int,
        baseRecipeId: Int,
        replacementId: Int,
        nutritionPlanId: Int,
        dayNumber: Int
    ): Completable

    fun loadFoodIngredients(weekId: Int?): Single<FoodIngredientsUIModel>

    fun updateIngredientsList(weekId: Int, ingredients: List<TaskUIModel>): Single<Boolean>

    fun toggleFoodIngredient(weekId: Int, ingredient: TaskUIModel): Single<Boolean>

    fun toggleFavorite(isDelete: Boolean,recipeId: Int): Single<Boolean>

    fun loadFaq(alias: String): Single<List<FaqAdapter.FaqListItem>>

    fun loadDynamicContent(alias: String): Single<DynamicContentUIModel>

    fun generateDynamicContent(rawContent: String): Single<SpannableStringBuilder>

    fun loadMenuItems(): Single<List<MenuItemUIModel>>

    fun toggleTaskCompletion(task: TaskUIModel, currentDay: Int): Single<Boolean>

    fun loadProfileDetails(): Single<ProfileDetailsUIModel>

    fun loadReferralDetails(): Single<ReferralDetailsUIModel>

    fun loadProfilePersonalInfo(): Single<ProfilePersonalInfoUIModel>

    fun uploadPhotosToServer(
        type: String,
        week: Int,
        cropped: Bitmap?,
        originalBitmap: Bitmap?
    ): Single<Boolean>

    fun checkIsInsideMarathon(): Single<Boolean>

    fun clearAppMenuCache()

    fun requestForgotPwdCode(email: String?): Single<Boolean>

    fun confirmForgotPwdCode(code: String?, email: String?): Single<Boolean>

    fun sendNewPwd(
        pwd: String?,
        pwdConfirm: String?,
        email: String?,
        code: String?
    ): Single<Boolean>


    fun getProfileParamsByMarathon(marathon: Int? = null): Single<ProfileBodyParamsUIModel>?

    fun updateBodyParamsByMarathon(
        marathon: Int? = null,
        request: ProfileBodyParamsRequest
    ): Single<ProfileBodyParamsUIModel>?

    fun updateProfileDetails(request: UpdateProfileRequest): Single<ProfilePersonalInfoUIModel>

    fun changeCountTracker(request: UpdateTrackerRequest, type: String): Single<TaskToggleResponse>

    fun changeGoalTracker(request: UpdateTrackerGoalRequest, type: String): Single<TaskToggleResponse>

    fun setGoogleFitData(request: FitDataRequest): Single<TaskToggleResponse>

    fun getCurrentMarathonId(): Int

    fun getCurrentMarathonImageUrl(): String?

    fun loadReports(): Single<ReportsUIModel>

    fun saveReport(reports: ReportsUIModel, reportText: String): Single<ReportsUIModel>

    fun getVoting(): Single<List<VotingItem>>

    fun vote(items: List<VotingItem>, voteUIModel: VotingItemUIModel): Single<List<VotingItem>>

    fun getCalorieDetails(): List<CalorieItem>

    fun getChat(): Single<ChatUIModel>

    fun sendMessage(userId: Int, text: String): Single<List<ChatItem>>

    fun getProgressForMarathon(marathon: Int? = null): Single<MarathonProgressUIModel>?

    fun saveBodyProgressForMarathon(marathon: Int? = null, body: Body): Single<TaskToggleResponse>?

    fun getVimeoConfig(videoId: String, h: String?): Single<VimeoModel>

    fun getDetailInfoTracker(type: String, period: String): Single<DetailInfoTrackerUIModel>

    fun saveVideoLinkForMarathon(
        marathon: Int? = null,
        url: String,
        week: Int
    ): Single<TaskToggleResponse>

    fun updateFCMToken(token: String)

    fun saveFCMToken(token: String)

}

